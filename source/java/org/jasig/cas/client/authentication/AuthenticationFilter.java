/**
 * Licensed to Jasig under one or more contributor license
 * agreements. See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Jasig licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.jasig.cas.client.authentication;

import ee.webmedia.alfresco.common.web.BeanHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.Assertion;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.jasig.cas.client.validation.AbstractTicketValidationFilter.redirectActive;

/**
 * Filter implementation to intercept all requests and attempt to authenticate
 * the user by redirecting them to CAS (unless the user has a ticket).
 * <p>
 * This filter allows you to specify the following parameters (at either the context-level or the filter-level):
 * <ul>
 * <li><code>casServerLoginUrl</code> - the url to log into CAS, i.e. https://cas.rutgers.edu/login</li>
 * <li><code>renew</code> - true/false on whether to use renew or not.</li>
 * <li><code>gateway</code> - true/false on whether to use gateway or not.</li>
 * </ul>
 *
 * <p>Please see AbstractCasFilter for additional properties.</p>
 *
 * @author Scott Battaglia
 * @version $Revision: 11768 $ $Date: 2007-02-07 15:44:16 -0500 (Wed, 07 Feb 2007) $
 * @since 3.0
 */
public class AuthenticationFilter extends AbstractCasFilter {
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * The URL to the CAS Server login.
     */
    private String casServerLoginUrl;
    private boolean useSession = true;

    /**
     * Whether to send the renew request or not.
     */
    private boolean renew = false;

    /**
     * Whether to send the gateway request or not.
     */
    private boolean gateway = false;

    private GatewayResolver gatewayStorage = new DefaultGatewayResolverImpl();

    protected void initInternal(final FilterConfig filterConfig) throws ServletException {
        log.info("org.jasig.cas.client.authentication.AuthenticationFilter: initInternal()...");
        if (!isIgnoreInitConfiguration()) {
            super.initInternal(filterConfig);
            setCasServerLoginUrl(getPropertyFromInitParams(filterConfig, "casServerLoginUrl", null));
            log.trace("org.jasig.cas.client.authentication.AuthenticationFilter: Loaded CasServerLoginUrl parameter: " + this.casServerLoginUrl);
            setRenew(parseBoolean(getPropertyFromInitParams(filterConfig, "renew", "false")));
            log.trace("org.jasig.cas.client.authentication.AuthenticationFilter: Loaded renew parameter: " + this.renew);
            setGateway(parseBoolean(getPropertyFromInitParams(filterConfig, "gateway", "false")));
            log.trace("org.jasig.cas.client.authentication.AuthenticationFilter: Loaded gateway parameter: " + this.gateway);
            setUseSession(this.parseBoolean(this.getPropertyFromInitParams(filterConfig, "useSession", "true")));
            this.log.trace("AbstractTicketValidationFilter: Setting useSession parameter: " + this.useSession);
            final String gatewayStorageClass = getPropertyFromInitParams(filterConfig, "gatewayStorageClass", null);

            if (gatewayStorageClass != null) {
                try {
                    this.gatewayStorage = (GatewayResolver) Class.forName(gatewayStorageClass).newInstance();
                } catch (final Exception e) {
                    log.error(e,e);
                    throw new ServletException(e);
                }
            }
        }
    }

    public void init() {
        super.init();
        CommonUtils.assertNotNull(this.casServerLoginUrl, "casServerLoginUrl cannot be null.");
    }

    public final void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        log.info("org.jasig.cas.client.authentication.AuthenticationFilter: doFilter()...");
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;
        final HttpSession session = request.getSession(false);
        final Assertion assertion = session != null ? (Assertion) session.getAttribute(CONST_CAS_ASSERTION) : null;

        if (assertion != null) {
            log.info("org.jasig.cas.client.authentication.AuthenticationFilter: ASSERTION FOUND! " + (assertion.getPrincipal() != null ? assertion.getPrincipal().getName() : "PRINCIPAL NAME IS NULL!"));
            log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()...");
            filterChain.doFilter(request, response);
            log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()... RETURN!");
            return;
        }

        String urlToRedirectTo = "";

        final String serviceUrl = constructServiceUrl(request, response);
        log.info("org.jasig.cas.client.authentication.AuthenticationFilter: serviceUrl: " + serviceUrl);

        if(BeanHelper.getUaaConfig().isFakeActive()){
            log.info("FAKE AUTHENTICATION IS ACTIVE! USERNAME: " + BeanHelper.getUaaConfig().getFakeAuthenticatedUsername());
            log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: FAKE is ACITVE! USERNAME: " + BeanHelper.getUaaConfig().getFakeAuthenticatedUsername());
            boolean redirect = true;
            if(this.useSession){
                this.log.info("GET attribute from session...");
                redirect = redirectActive(request.getSession().getAttribute(CONST_CAS_ASSERTION));
                this.log.info("GET attribute from session... " + redirect);
            } else {
                this.log.info("GET attribute from request parameter...");
                redirect = redirectActive(request.getParameter(CONST_CAS_ASSERTION));
                this.log.info("GET attribute from request parameter..." + redirect);
            }

            if(redirect){
                log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()...");
                filterChain.doFilter(request, response);
                log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()... RETURN!");
                return;
            } else {
                log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: Constructed service url: " + serviceUrl);
                urlToRedirectTo = CommonUtils.constructRedirectUrl(this.casServerLoginUrl, getServiceParameterName(), serviceUrl, this.renew, this.gateway);

                log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: redirecting to \"" + urlToRedirectTo + "\"");
            }
        } else if(BeanHelper.getUaaConfig().isUaaActive()) {
            log.info("UAA AUTHENTICATION IS ACTIVE!");
            log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: UAA is ACITVE!");
            boolean redirect = true;
            if(this.useSession){
                this.log.info("GET attribute from session...");
                redirect = redirectActive(request.getSession().getAttribute(CONST_CAS_ASSERTION));
                this.log.info("GET attribute from session... " + redirect);
            } else {
                this.log.info("GET attribute from request parameter...");
                redirect = redirectActive(request.getParameter(CONST_CAS_ASSERTION));
                this.log.info("GET attribute from request parameter..." + redirect);
            }

            if(redirect){
                log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()...");
                filterChain.doFilter(request, response);
                log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()... RETURN!");
                return;
            } else {
                log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: Constructed service url: " + serviceUrl);
                urlToRedirectTo = CommonUtils.constructRedirectUrl(this.casServerLoginUrl, getServiceParameterName(), serviceUrl, this.renew, this.gateway);

                log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: redirecting to \"" + urlToRedirectTo + "\"");
            }
        } else if(BeanHelper.getUaaConfig().isCasActive()){
            log.info("CAS AUTHENTICATION IS ACTIVE!");
            final String ticket = CommonUtils.safeGetParameter(request,getArtifactParameterName());
            log.info("org.jasig.cas.client.authentication.AuthenticationFilter: TICKET: " + ticket);
            final boolean wasGatewayed = this.gatewayStorage.hasGatewayedAlready(request, serviceUrl);
            log.info("org.jasig.cas.client.authentication.AuthenticationFilter: Was GATEWAYED: " + wasGatewayed);

            if (CommonUtils.isNotBlank(ticket) || wasGatewayed) {
                log.info("org.jasig.cas.client.authentication.AuthenticationFilter: TICKET OR GATEWAY FOUND!");
                log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()...");
                filterChain.doFilter(request, response);
                log.info("org.jasig.cas.client.authentication.AuthenticationFilter: filterChain.doFilter()... RETURN!");
                return;
            }

            final String modifiedServiceUrl;

            log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: no ticket and no assertion found");
            if (this.gateway) {
                log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: setting gateway attribute in session");
                modifiedServiceUrl = this.gatewayStorage.storeGatewayInformation(request, serviceUrl);
            } else {
                modifiedServiceUrl = serviceUrl;
            }

            log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: Constructed service url: " + modifiedServiceUrl);
            urlToRedirectTo = CommonUtils.constructRedirectUrl(this.casServerLoginUrl, getServiceParameterName(), modifiedServiceUrl, this.renew, this.gateway);

            log.debug("org.jasig.cas.client.authentication.AuthenticationFilter: redirecting to \"" + urlToRedirectTo + "\"");
        } else {
            throw new ServletException("AUTHENTICATION SYSTEM IS NOT DEFINED! CAN'T AUTHENTICATE USER!!!!");
        }

        final String ticket = CommonUtils.safeGetParameter(request,getArtifactParameterName());
        final boolean wasGatewayed = this.gatewayStorage.hasGatewayedAlready(request, serviceUrl);

        if (CommonUtils.isNotBlank(ticket) || wasGatewayed) {
            filterChain.doFilter(request, response);
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("redirecting to \"" + urlToRedirectTo + "\"");
        }

        response.sendRedirect(urlToRedirectTo);
    }

    public final void setRenew(final boolean renew) {
        this.renew = renew;
    }

    public final void setGateway(final boolean gateway) {
        this.gateway = gateway;
    }

    public final void setCasServerLoginUrl(final String casServerLoginUrl) {
        this.casServerLoginUrl = casServerLoginUrl;
    }

    public final void setGatewayStorage(final GatewayResolver gatewayStorage) {
        this.gatewayStorage = gatewayStorage;
    }

    public final void setUseSession(boolean useSession) {
        this.useSession = useSession;
    }
}
