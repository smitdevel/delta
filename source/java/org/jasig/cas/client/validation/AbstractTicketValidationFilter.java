/**
 * Licensed to Jasig under one or more contributor license
 * agreements. See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Jasig licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.jasig.cas.client.validation;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.auth.uaa.model.UaaUserInfo;
import ee.webmedia.alfresco.common.web.BeanHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.client.authentication.AttributePrincipalImpl;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.util.ReflectUtils;

import javax.naming.AuthenticationException;
import javax.net.ssl.HostnameVerifier;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static ee.webmedia.alfresco.user.service.Cas20ProxyReceivingRedirectingTicketValidationFilter.PHONE_NUMBER;

/**
 * The filter that handles all the work of validating ticket requests.
 * <p>
 * This filter can be configured with the following values:
 * <ul>
 * <li><code>redirectAfterValidation</code> - redirect the CAS client to the same URL without the ticket.</li>
 * <li><code>exceptionOnValidationFailure</code> - throw an exception if the validation fails.  Otherwise, continue
 *  processing.</li>
 * <li><code>useSession</code> - store any of the useful information in a session attribute.</li>
 * </ul>
 *
 * @author Scott Battaglia
 * @version $Revision$ $Date$
 * @since 3.1
 */
public abstract class AbstractTicketValidationFilter extends AbstractCasFilter {
    private final static Log log = LogFactory.getLog(AbstractTicketValidationFilter.class);

    /** The TicketValidator we will use to validate tickets. */
    private TicketValidator ticketValidator;

    /**
     * Specify whether the filter should redirect the user agent after a
     * successful validation to remove the ticket parameter from the query
     * string.
     */
    private boolean redirectAfterValidation = false;

    /** Determines whether an exception is thrown when there is a ticket validation failure. */
    private boolean exceptionOnValidationFailure = true;

    private boolean useSession = true;

    /**
     * Template method to return the appropriate validator.
     *
     * @param filterConfig the FilterConfiguration that may be needed to construct a validator.
     * @return the ticket validator.
     */
    protected TicketValidator getTicketValidator(final FilterConfig filterConfig) {
        return this.ticketValidator;
    }

    /**
     * Gets the configured {@link HostnameVerifier} to use for HTTPS connections
     * if one is configured for this filter.
     * @param filterConfig Servlet filter configuration.
     * @return Instance of specified host name verifier or null if none specified.
     */
    protected HostnameVerifier getHostnameVerifier(final FilterConfig filterConfig) {
        final String className = getPropertyFromInitParams(filterConfig, "hostnameVerifier", null);
        log.trace("Using hostnameVerifier parameter: " + className);
        final String config = getPropertyFromInitParams(filterConfig, "hostnameVerifierConfig", null);
        log.trace("Using hostnameVerifierConfig parameter: " + config);
        if (className != null) {
            if (config != null) {
                return ReflectUtils.newInstance(className, config);
            } else {
                return ReflectUtils.newInstance(className);
            }
        }
        return null;
    }

    protected void initInternal(final FilterConfig filterConfig) throws ServletException {
        log.debug("AbstractTicketValidationFilter: initInternal()...");
        setExceptionOnValidationFailure(parseBoolean(getPropertyFromInitParams(filterConfig, "exceptionOnValidationFailure", "true")));
        log.trace("Setting exceptionOnValidationFailure parameter: " + this.exceptionOnValidationFailure);
        setRedirectAfterValidation(parseBoolean(getPropertyFromInitParams(filterConfig, "redirectAfterValidation", "true")));
        log.trace("Setting redirectAfterValidation parameter: " + this.redirectAfterValidation);
        setUseSession(parseBoolean(getPropertyFromInitParams(filterConfig, "useSession", "true")));
        log.trace("Setting useSession parameter: " + this.useSession);
        setTicketValidator(getTicketValidator(filterConfig));
        super.initInternal(filterConfig);
    }

    public void init() {
        super.init();
        CommonUtils.assertNotNull(this.ticketValidator, "ticketValidator cannot be null.");
    }

    /**
     * Pre-process the request before the normal filter process starts.  This could be useful for pre-empting code.
     *
     * @param servletRequest The servlet request.
     * @param servletResponse The servlet response.
     * @param filterChain the filter chain.
     * @return true if processing should continue, false otherwise.
     * @throws IOException if there is an I/O problem
     * @throws ServletException if there is a servlet problem.
     */
    protected boolean preFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        return true;
    }

    /**
     * Template method that gets executed if ticket validation succeeds.  Override if you want additional behavior to occur
     * if ticket validation succeeds.  This method is called after all ValidationFilter processing required for a successful authentication
     * occurs.
     *
     * @param request the HttpServletRequest.
     * @param response the HttpServletResponse.
     * @param assertion the successful Assertion from the server.
     */
    protected void onSuccessfulValidation(final HttpServletRequest request, final HttpServletResponse response, final Assertion assertion) {
        // nothing to do here.
    }

    /**
     * Template method that gets executed if validation fails.  This method is called right after the exception is caught from the ticket validator
     * but before any of the processing of the exception occurs.
     *
     * @param request the HttpServletRequest.
     * @param response the HttpServletResponse.
     */
    protected void onFailedValidation(final HttpServletRequest request, final HttpServletResponse response) {
        // nothing to do here.
    }

    private String removeUrlParam(String url, String param){
        if (url.contains("?")) {
           String[] arr = url.split("\\?");
           if(arr.length >= 2){
               String args = "";
               for(int i = 1 ; i < arr.length; i++){
                   args += arr[i];
                   if(i+1 < arr.length){
                       args += "?";
                   }
               }

               if(args.contains("&")){
                   String[] params = arr[1].toLowerCase().split("&");
                   StringBuilder urlArgs = new StringBuilder("?");
                   for(String p : params){
                       if(!p.startsWith(param)){
                           urlArgs.append(p).append("&");
                       }
                   }
                   return arr[0] + urlArgs;
               } else if(args.contains("=")){
                   if(!args.startsWith(param)){
                       return arr[0] + "?" + args;
                   } else {
                       return arr[0];
                   }
               }
           } else {
               return arr[0];
           }
        }
        return url;
    }
    
    public final void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(BeanHelper.getUaaConfig().isFakeActive()){
            log.debug("FAKE AUTHENTICATION IS ACTIVE! USERNAME: " + BeanHelper.getUaaConfig().getFakeAuthenticatedUsername());
            boolean redirect;
            if(this.useSession){
                log.debug("GET attribute from session...");
                redirect = redirectActive(request.getSession().getAttribute(CONST_CAS_ASSERTION));
                log.debug("GET attribute from session... " + redirect);
            } else {
                log.debug("GET attribute from request parameter...");
                redirect = redirectActive(request.getParameter(CONST_CAS_ASSERTION));
                log.debug("GET attribute from request parameter..." + redirect);
            }

            log.debug("AbstractTicketValidationFilter: FAKE is ACTIVE!");
            if(redirect){
                Assertion assertion = new AssertionImpl(new AttributePrincipalImpl(BeanHelper.getUaaConfig().getFakeAuthenticatedUsername()));
                log.debug("AbstractTicketValidationFilter: USERNAME: " + assertion.getPrincipal().getName());
                request.setAttribute(CONST_CAS_ASSERTION, assertion);
                if (this.useSession) {
                    request.getSession().setAttribute(CONST_CAS_ASSERTION, assertion);
                }

                this.onSuccessfulValidation(request, response, assertion);
                if (this.redirectAfterValidation) {
                    String redirectUrl = this.constructServiceUrl(request, response);
                    log.debug("AbstractTicketValidationFilter: Redirecting after successful ticket validation... " + redirectUrl);
                    response.sendRedirect(redirectUrl);
                    log.debug("AbstractTicketValidationFilter: RETURN!");
                    return;
                }
            }
        } else if(BeanHelper.getUaaConfig().isUaaActive()) {
            log.debug("UAA AUTHENTICATION IS ACTIVE!");
            boolean redirect;
            if(this.useSession){
                log.debug("GET attribute from session...");
                redirect = redirectActive(request.getSession().getAttribute(CONST_CAS_ASSERTION));
                log.debug("GET attribute from session... " + redirect);
            } else {
                log.debug("GET attribute from request parameter...");
                redirect = redirectActive(request.getParameter(CONST_CAS_ASSERTION));
                log.debug("GET attribute from request parameter..." + redirect);
            }

            log.debug("AbstractTicketValidationFilter: UAA is ACTIVE!");
            if(redirect){
                String redirectUrl = removeUrlParam(this.constructServiceUrl(request, response), BeanHelper.getUaaConfig().getCodeString().toLowerCase());
                log.debug("REDIRECT URL : " + redirectUrl);

                String code = request.getParameter(BeanHelper.getUaaConfig().getCodeString());
                log.debug("CODE: " + code);
                String token = (String) request.getSession().getAttribute(BeanHelper.getUaaConfig().getTokenString());
                log.debug("TOKEN: " + token);

                log.debug("SESSION GET ATTRIBUTE: " + CONST_CAS_ASSERTION + "; VALUE: " + request.getSession().getAttribute(CONST_CAS_ASSERTION));

                if (code == null) {
                    log.debug("Redirect");
                    BeanHelper.getUaaConfig().redirectAuthorize(response, redirectUrl);
                    return;
                } else {

                    log.debug("Get token....");
                    Oauth2TokenInfo tokenResponse = null;
                    try {
                        tokenResponse = BeanHelper.getUaaConfig().getUserToken(code, redirectUrl);
                    } catch (AuthenticationException e) {
                        log.warn("Authentication failed!");
                    }

                    if (tokenResponse == null || tokenResponse.getAccess_token() == null) {
                        log.error("Cannot get token from uaa!");
                        BeanHelper.getUaaConfig().redirectLoginError(response, redirectUrl);
                        return;
                    } else {
                        UaaUserInfo uaaUserInfo = BeanHelper.getUaaConfig().getUserInfo(tokenResponse);
                        if (uaaUserInfo != null) {
                            if (uaaUserInfo.getUser_name() == null) {
                                log.error("Cannot get userinfo from uaa: error '" + tokenResponse.getError() + "', error_description '" + tokenResponse.getErrorDescription() + "'");
                                BeanHelper.getUaaConfig().redirectLoginError(response, redirectUrl);
                                return;
                            } else {
                                String currentUsername = uaaUserInfo.getUser_name();
                                log.debug("currentUsername: " + currentUsername);
                                if (StringUtils.isNotBlank(currentUsername)) {
                                    log.debug("User " + uaaUserInfo.getUser_name() + " from UAA logging successful!");

                                    request.getSession().setAttribute(BeanHelper.getUaaConfig().getTokenString(), tokenResponse.getAccess_token());

                                    Assertion assertion = new AssertionImpl(new AttributePrincipalImpl(uaaUserInfo.getUser_name()));
                                    log.debug("AbstractTicketValidationFilter: USERNAME: " + assertion.getPrincipal().getName());

                                    // SET PHONE, if it works
                                    Phone(request, assertion, uaaUserInfo);

                                    request.setAttribute(CONST_CAS_ASSERTION, assertion);
                                    if (this.useSession) {
                                        request.getSession().setAttribute(CONST_CAS_ASSERTION, assertion);
                                    }

                                    this.onSuccessfulValidation(request, response, assertion);
                                    if (this.redirectAfterValidation) {
                                        //String redirectUrl = this.constructServiceUrl(request, response);
                                        log.debug("AbstractTicketValidationFilter: Redirecting after successful ticket validation... " + redirectUrl);
                                        response.sendRedirect(redirectUrl);
                                        log.debug("AbstractTicketValidationFilter: RETURN!");
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if(BeanHelper.getUaaConfig().isCasActive()){
            final String ticket = CommonUtils.safeGetParameter(request, getArtifactParameterName());

            if (CommonUtils.isNotBlank(ticket)) {
                if (log.isDebugEnabled()) {
                    log.debug("Attempting to validate ticket: " + ticket);
                }

                try {
                    final Assertion assertion = this.ticketValidator.validate(ticket, constructServiceUrl(request, response));

                    if (log.isDebugEnabled()) {
                        log.debug("Successfully authenticated user: " + assertion.getPrincipal().getName());
                    }

                    request.setAttribute(CONST_CAS_ASSERTION, assertion);

                    if (this.useSession) {
                        request.getSession().setAttribute(CONST_CAS_ASSERTION, assertion);
                    }
                    onSuccessfulValidation(request, response, assertion);

                    if (this.redirectAfterValidation) {
                        log. debug("Redirecting after successful ticket validation.");
                        response.sendRedirect(constructServiceUrl(request, response));
                        return;
                    }
                } catch (final TicketValidationException e) {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    log.warn(e, e);

                    onFailedValidation(request, response);

                    if (this.exceptionOnValidationFailure) {
                        throw new ServletException(e);
                    }

                    return;
                }
            }
        } else {
            log.error("UNKNOWN AUTHENTICATION! CAN'T AUTHENTICATE ANY USER!!!!");
            throw new ServletException("UNKNOWN AUTHENTICATION! CAN'T AUTHENTICATE ANY USER!!!!");
        }

        filterChain.doFilter(request, response);

    }
    
    private void Phone(HttpServletRequest request, Assertion assertion, UaaUserInfo uaaUserInfo){
        // Propagate attributes if possible
        if (assertion == null || assertion.getPrincipal() == null) {
            return;
        }

        Map<String, Object> attributes = assertion.getPrincipal().getAttributes();
        if (attributes == null || !attributes.containsKey(PHONE_NUMBER)) {
            return;
        }
        if(attributes.containsKey(PHONE_NUMBER)){
            request.getSession().setAttribute(PHONE_NUMBER, attributes.get(PHONE_NUMBER));
        } else {
            request.getSession().setAttribute(PHONE_NUMBER, uaaUserInfo.getPhone_number());
        }

    }

    public static boolean redirectActive(Object obj){
        if(obj == null){
            log.debug("AbstractTicketValidationFilter: ASSERTION param is NULL!");
        } else {
            if(obj instanceof Assertion){
                Assertion assertion = (Assertion) obj;
                log.debug("AbstractTicketValidationFilter: PRINCIPAL: " + (assertion.getPrincipal() != null ? assertion.getPrincipal().getName(): "NULL!"));
                return false;
            }
        }
        return true;
    }

    private void redirectAuthorize(HttpServletResponse response, String redirectUrl) throws IOException {
        log.debug("redirectAuthorize()...use UAA redirect params: " + BeanHelper.getUaaConfig().getRedirectParam(redirectUrl));
        response.sendRedirect(BeanHelper.getUaaConfig().getRedirectParam(redirectUrl));
    }

    public final void setTicketValidator(final TicketValidator ticketValidator) {
        this.ticketValidator = ticketValidator;
    }

    public final void setRedirectAfterValidation(final boolean redirectAfterValidation) {
        this.redirectAfterValidation = redirectAfterValidation;
    }

    public final void setExceptionOnValidationFailure(final boolean exceptionOnValidationFailure) {
        this.exceptionOnValidationFailure = exceptionOnValidationFailure;
    }

    public final void setUseSession(final boolean useSession) {
        this.useSession = useSession;
    }
}
