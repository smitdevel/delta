package ee.webmedia.alfresco.archivals.job;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.parameters.model.Parameters;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.search.impl.lucene.LuceneQueryParser;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.ResultSetRow;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.util.CachingDateFormat;
import org.alfresco.web.bean.repository.Repository;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static ee.webmedia.alfresco.utils.SearchUtil.joinQueryPartsAnd;

public class AutomaticObjectDeletionJob implements StatefulJob {

    private static final Log LOG = LogFactory.getLog(AutomaticObjectDeletionJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Long months = BeanHelper.getParametersService().getLongParameter(Parameters.AUTOMATIC_OBJECT_DELETION);
        LOG.info("Starting AutomaticObjectDeletionJob");
        if (months != null && months > 0 && getArchiveRootRef() != null) {
            SearchParameters sp = getSearchParameters(months);
            ResultSet results = BeanHelper.getSearchService().query(sp);
            LOG.info("Found " + (results != null ? results.length() : 0) + " files to delete");
            if (results != null && results.length() != 0) {
                deleteOldFiles(results);
            }
        } else {
            LOG.info("automaticObjectDeletion parameter not set, job will not start");
        }
        LOG.info("Ending AutomaticObjectDeletionJob");
    }

    private void deleteOldFiles(ResultSet results) {
        int deletedFilesCount = 0;
        List<NodeRef> nodeRefs = new ArrayList<>();
        for (ResultSetRow row : results) {
            NodeRef nodeRef = row.getNodeRef();
            if (BeanHelper.getNodeService().exists(nodeRef)) {
                nodeRefs.add(nodeRef);
                deletedFilesCount++;
            }
        }
        BeanHelper.getNodeArchiveService().purgeArchivedNodes(nodeRefs);
        LOG.info("Deleted " + deletedFilesCount + " files");
    }

    private SearchParameters getSearchParameters(Long months) {
        String query = buildSearchQuery(months);
        SearchParameters sp = new SearchParameters();
        sp.setLanguage(SearchService.LANGUAGE_LUCENE);
        sp.setQuery(query);
        sp.addStore(getArchiveRootRef().getStoreRef());
        return sp;
    }

    private String buildSearchQuery(long months) {
        Date fromDate = new Date(0);
        Date toDate = DateUtils.addMonths(new Date(), -(int) months);
        SimpleDateFormat df = CachingDateFormat.getDateFormat();
        String strFromDate = LuceneQueryParser.escape(df.format(fromDate));
        String strToDate = LuceneQueryParser.escape(df.format(toDate));

        List<String> queryParts = new ArrayList<>();
        String buf = "@" + Repository.escapeQName(ContentModel.PROP_ARCHIVED_DATE) +
                ":" + "[" + strFromDate + " TO " + strToDate + "]";
        queryParts.add(buf);
        queryParts.add("ASPECT:\"" + ContentModel.ASPECT_ARCHIVED + "\"");
        return joinQueryPartsAnd(queryParts);
    }

    private NodeRef getArchiveRootRef() {
        return BeanHelper.getNodeArchiveService().getStoreArchiveNode(Repository.getStoreRef());
    }
}
