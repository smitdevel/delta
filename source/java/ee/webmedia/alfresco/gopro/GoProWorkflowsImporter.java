package ee.webmedia.alfresco.gopro;

import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getPrivilegeService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getUserService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowService;
import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.OWNER_ID;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.repo.transaction.TransactionListenerAdapter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import ee.webmedia.alfresco.addressbook.model.AddressbookModel.Props;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.orgstructure.model.OrganizationStructure;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.privilege.model.Privilege;
import ee.webmedia.alfresco.utils.UserUtil;
import ee.webmedia.alfresco.workflow.model.Comment;
import ee.webmedia.alfresco.workflow.model.CompoundWorkflowType;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;
import ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel;
import ee.webmedia.alfresco.workflow.service.Task;
import ee.webmedia.alfresco.workflow.service.WorkflowUtil;
import ee.webmedia.alfresco.workflow.service.type.WorkflowType;

/**
 * Imports documents and files from GoPro.
 */
public class GoProWorkflowsImporter {

    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(GoProWorkflowsImporter.class);
    
    private static final String GP_MAIN_ELEMENT_FIELD = "field";
    private static final String GP_MAIN_ATTRIBUTE_NAME = "name";
    // doc elements
    private static final String GP_ELEMENT_SUBJECT = "Subject";
    private static final String GP_ELEMENT_DOC_ID = "ParentDocumentId";
    private static final String GP_ELEMENT_WF_OWNER = "RequesterName";
    private static final String GP_ELEMENT_APP_START = "AppStart";
    private static final String GP_ELEMENT_APPROVAL_USER_COMMENTS = "ApprovalUserComments";
    private static final String GP_ELEMENT_APPROVAL_DESCRIPTION = "ApprovalDescription";
    private static final String GP_ELEMENT_APPROVAL_COMMENTS = "ApprovalComments";
    
    
    private static final String TASK_APPROVED = "dokument kooskõlastatud";
    private static final String TASK_NOT_APPROVED = "dokument tagasi lükatud";
    
    private static final String DUMMY_USER_ID = "99999999999";
    

    protected static final char CSV_SEPARATOR = ';';
    private static final String CREATOR_MODIFIER = "DHS";
    private static final FastDateFormat staticDateTimeFormat = FastDateFormat.getInstance("dd.MM.yyyy HH:mm:ss");

    private final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private final DateFormat dateTimeFormatddMMyy = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
    private final DateFormat dateTimeFormatdMMyy = new SimpleDateFormat("d.MM.yy HH:mm:ss");
    private final DateFormat dateFormatSlashed = new SimpleDateFormat("dd/MM/yyyy");
    private final DateFormat dateTimeFormatSlashed = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    protected SAXReader xmlReader = new SAXReader();

    private File dataFolder;
    private File workFolder;
    private int batchSize;
    private String institutionCode;
    private String defaultOwnerId;
    private String defaultOwnerName;
    private String defaultOwnerEmail;
    
    // [SPRING BEANS
    private TransactionService transactionService;
    private NodeService nodeService;
    private BehaviourFilter behaviourFilter;
    private GoProWfMainImporter goProImporter;
    

    public GoProWorkflowsImporter(GoProWfMainImporter goProImporter) {
        
        setTransactionService(BeanHelper.getTransactionService());
        setNodeService(BeanHelper.getNodeService());
        setBehaviourFilter(BeanHelper.getPolicyBehaviourFilter());
        setGoProImporter(goProImporter);

        dateFormat.setLenient(false);
        dateTimeFormat.setLenient(false);
    }

    // INJECTORS
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setBehaviourFilter(BehaviourFilter behaviourFilter) {
        this.behaviourFilter = behaviourFilter;
    }

    public void setGoProImporter(GoProWfMainImporter goProImporter) {
        this.goProImporter = goProImporter;
    }

    /**
     * Runs documents import process
     */
    public void runImport(File dataFolder, File workFolder, int batchSize, String defaultOwnerId, 
    		String institutionCode)
                    throws Exception {
        this.dataFolder = dataFolder;
        this.workFolder = workFolder;
        this.batchSize = batchSize;
        this.institutionCode = institutionCode;
        this.defaultOwnerId = defaultOwnerId;
        Map<QName, Serializable> defaultUserProps = getUserService().getUserProperties(defaultOwnerId);
        this.defaultOwnerName = UserUtil.getPersonFullName1(defaultUserProps);
        this.defaultOwnerEmail = (String)defaultUserProps.get(ContentModel.PROP_EMAIL);
        init();
        // Wf import
       
        	
        loadCompletedDocuments();
        loadWorkflows();
        loadCompletedWorkflows();
        createWorkflows();
        
        
    }

    private void init() {
    	workflowsMap = new TreeMap<String, File>();
    	completedWorkflowsMap = new TreeMap<String, NodeRef>();
    	failedWorkflowsMap = new TreeMap<String, NodeRef>();
    	
        completedDocumentsMap = new TreeMap<String, NodeRef>();
        
        filesToProceed = null;
    }

    protected NavigableMap<String /* workflowId */, File> workflowsMap;
    protected NavigableMap<String /* workflowId */, NodeRef> completedWorkflowsMap;
    protected NavigableMap<String /* workflowId */, NodeRef> failedWorkflowsMap;
    protected NavigableMap<String /* documentId */, NodeRef> completedDocumentsMap;
    private final Map<String, Pair<NodeRef, Map<QName, Serializable>>> userDataByUserName = new HashMap<String, Pair<NodeRef, Map<QName, Serializable>>>();


    protected NavigableSet<Integer> filesToProceed;
    protected File completedWorkflowsFile;
    protected File failedWorkflowsFile;
    protected File completedDocumentsFile;
    

    
    protected void loadWorkflows() {
        workflowsMap = new TreeMap<String, File>();
        log.info("Getting xml entries of " + dataFolder);
        File workflowsFolder = new File(dataFolder, "workflows");
        if (!workflowsFolder.exists()) {
        	log.info("No workflows folder is found, skippinf workflows import");
        	return;
        }
        
        File[] files = workflowsFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return StringUtils.isNotBlank(name) && name.endsWith(".xml");
            }
        });

        log.info("Directory listing contains " + files.length + " xml entries, parsing them to workflows");
        for (File file : files) {
            String name = file.getName();
            if (name.endsWith(".xml")) {
                // Workflow
            	String workflowId = StringUtils.substringBefore(name, ".xml");
                log.debug("Found workflowId=" + workflowId);
                workflowsMap.put(workflowId, file);
                
            }
        }
        log.info("Completed parsing wokrlfows directory listing, got " + workflowsMap.size() + " workflows");
    }
    
    
    private String parseUserFullName(String goProUserName) {
    	if (StringUtils.isBlank(goProUserName)) {
    		return null;
    	}
    	String userFullname = goProUserName;
		if (goProUserName.contains("/")) {
			userFullname = StringUtils.substringBefore(goProUserName, "/").trim();
		}
		if (goProUserName.contains("(")) {
			userFullname = StringUtils.substringBefore(goProUserName, "(").trim();
		}
		
		return userFullname;
    }

    

    private void checkStop() {
        goProImporter.checkStop();
    }

    private abstract class BatchProgress<E> {
        Collection<E> origin;
        List<E> batchList;
        int totalSize;
        int i;
        int completedSize;
        int thisRunCompletedSize;
        long thisRunStartTime;
        long startTime;
        String processName;

        private void init() {
            totalSize = origin.size();
            thisRunStartTime = System.currentTimeMillis();
            startTime = thisRunStartTime;
            i = 0;
            completedSize = 0;
            batchList = new ArrayList<E>(batchSize);
        }

        abstract void executeBatch() throws Exception;

        void executeInTransaction() {
            try {
                getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Object>() {
                    @Override
                    public Object execute() throws Throwable {
                        behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
                        executeBatch();
                        return null;
                    }
                });
            } catch (Exception e) {
                log.error("Import batch of workflows failed, transaction rolled back, continuing with next batch", e);
            }
        }

        private void step() {
            executeInTransaction();
            completedSize += batchList.size();
            thisRunCompletedSize += batchList.size();
            batchList = new ArrayList<E>(batchSize);
            long endTime = System.currentTimeMillis();
            double completedPercent = (completedSize) * 100L / ((double) totalSize);
            double lastWfsPerSec = (i) * 1000L / ((double) (endTime - startTime));
            long thisRunTotalTime = endTime - thisRunStartTime;
            double totalWfsPerSec = (thisRunCompletedSize) * 1000L / ((double) thisRunTotalTime);
            long remainingSize = ((long) totalSize) - ((long) completedSize);
            long divisor = (thisRunCompletedSize) * 60000L;
            int etaMinutes = ((int) (remainingSize * thisRunTotalTime / divisor)) + 1;
            int etaHours = 0;
            if (etaMinutes > 59) {
                etaHours = etaMinutes / 60;
                etaMinutes = etaMinutes % 60;
            }
            String eta = etaMinutes + "m";
            if (etaHours > 0) {
                eta = etaHours + "h " + eta;
            }
            i = 0;
            String info = "%s: %6.2f%% completed - %7d of %7d, %5.1f items per second (last), %5.1f (total), ETA %s";
            log.info(String.format(info, processName, completedPercent, completedSize, totalSize, lastWfsPerSec, totalWfsPerSec, eta));
            startTime = endTime;
        }

        public void run() {
            init();
            checkStop();
            for (E e : origin) {
                batchList.add(e);
                i++;
                if (i >= batchSize) {
                    step();
                    checkStop();
                }
            }
            step();
        }

    }
    
    private static interface CsvWriterClosure {
        void execute(CsvWriter writer) throws IOException;

        String[] getHeaders();
    }

    private static void bindCsvWriteAfterCommit(final File file, final CsvWriterClosure closure) {
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
                try {
                    // Write created documents
                    boolean exists = file.exists();
                    if (!exists) {
                        OutputStream outputStream = new FileOutputStream(file);
                        try {
                            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
                            outputStream.write("\ufeff".getBytes("UTF-8"));
                        } finally {
                            outputStream.close();
                        }
                    }
                    CsvWriter writer = new CsvWriter(new FileWriter(file, true), CSV_SEPARATOR);
                    try {
                        if (!exists) {
                            writer.writeRecord(closure.getHeaders());
                        }
                        closure.execute(writer);
                    } finally {
                        writer.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Error writing file '" + file + "': " + e.getMessage(), e);
                }
            }
        });
    }
    
    
    
    

    protected void loadCompletedDocuments() throws Exception {
        
        completedDocumentsMap = new TreeMap<String, NodeRef>();

        completedDocumentsFile = new File(workFolder, "completed_docs.csv");
        if (!completedDocumentsFile.exists()) {
            log.info("Skipping loading previously completed documentId-s, file does not exist: " + completedDocumentsFile);
            return;
        }

        log.info("Loading previously completed documentId-s from file " + completedDocumentsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedDocumentsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String documentId = reader.get(0);
                String nodeRefString = reader.get(1);
                if (StringUtils.isNotBlank(nodeRefString)) {
                    NodeRef documentRef = new NodeRef(nodeRefString);
                    completedDocumentsMap.put(documentId, documentRef);
                }
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + completedDocumentsMap.size() + " previously completed documentId-s");

       
    }
    
    protected void loadCompletedWorkflows() throws Exception {
        failedWorkflowsFile = new File(workFolder, "failed_workflows.csv");
        OutputStream outputStream = new FileOutputStream(failedWorkflowsFile);
        try {
            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
            outputStream.write("\ufeff".getBytes("UTF-8"));
        } finally {
            outputStream.close();
        }
        CsvWriter writer = new CsvWriter(new FileWriter(failedWorkflowsFile, true), CSV_SEPARATOR);
        try {
            writer.writeRecord(new String[] {
                    "workflowFileName",
                    "errorMsg"
            });
        } finally {
            writer.close();
        }

        completedWorkflowsMap = new TreeMap<String, NodeRef>();

        completedWorkflowsFile = new File(workFolder, "completed_workflows.csv");
        if (!completedWorkflowsFile.exists()) {
            log.info("Skipping loading previously completed workflowId-s, file does not exist: " + completedWorkflowsFile);
            return;
        }

        log.info("Loading previously completed workflowId-s from file " + completedWorkflowsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedWorkflowsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String workflowId = reader.get(0);
                String nodeRefString = reader.get(1);
                if (StringUtils.isNotBlank(nodeRefString)) {
                    NodeRef workflowRef = new NodeRef(nodeRefString);
                    completedWorkflowsMap.put(workflowId, workflowRef);
                }
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + completedWorkflowsMap.size() + " previously completed workflowId-s");

        int previousSize = workflowsMap.size();
        workflowsMap.keySet().removeAll(completedWorkflowsMap.keySet());
        
        log.info("Removed previously completed workflowId-s from current workflow list: " + previousSize + " -> " + workflowsMap.size());
    }

    private class WorkflowsBatchProgress extends BatchProgress<Entry<String, File>> {
        {
            origin = workflowsMap.entrySet();
            processName = "Workflows import";
        }

        @Override
        void executeBatch() throws Exception {
            createWorkflowsBatch(batchList);
        }
    }

    private void createWorkflows() throws Exception {
        if (workflowsMap.isEmpty() || completedDocumentsMap.isEmpty()) {
            log.info("Skipping workflows import, no workflows or completed documents found");
            return;
        }
        log.info("Starting workflows import. Total = " + workflowsMap.size());
        WorkflowsBatchProgress batchProgress = new WorkflowsBatchProgress();
        batchProgress.run();
        log.info("Workflows IMPORT COMPLETE :)");
    }

    static class ImportedWorkflow {
        String workflowId;
        NodeRef nodeRef;
        String documentId;
        NodeRef documentNodeRef;
        

        public ImportedWorkflow(String workflowId, NodeRef nodeRef, String documentId, NodeRef documentNodeRef) {
            this.workflowId = workflowId;
            this.nodeRef = nodeRef;
            this.documentId = documentId;
            this.documentNodeRef = documentNodeRef;
            
        }
    }

    protected void createWorkflowsBatch(final List<Entry<String, File>> batchList) throws Exception {
        final Map<String, ImportedWorkflow> batchCompletedWorkflowsMap = new TreeMap<String, ImportedWorkflow>();
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
            }

            @Override
            public void afterRollback() {
                completedWorkflowsMap.keySet().removeAll(batchCompletedWorkflowsMap.keySet());
            }
        });
        for (Entry<String, File> entry : batchList) {
            String workflowId = entry.getKey();
            if (log.isTraceEnabled()) {
                log.trace("Processing workflowId=" + workflowId);
            }
            File file = entry.getValue();
            try {
                ImportedWorkflow wf = createWorkflow(workflowId, file);
                if (wf.nodeRef != null) {
                	batchCompletedWorkflowsMap.put(workflowId, wf);
                	completedWorkflowsMap.put(workflowId, wf.nodeRef); // Add immediately to completedWorkflowsMap
                	goProImporter.getStatus().incrCount();
                } else {
	                CsvWriter writer = new CsvWriter(new FileWriter(failedWorkflowsFile, true), CSV_SEPARATOR);
	                try {
	                    writer.writeRecord(new String[] { file.getName(), wf.workflowId });
	                } finally {
	                    writer.close();
	                }
	                goProImporter.getStatus().incrFailed();
                }
            } catch (Exception e) {
                CsvWriter writer = new CsvWriter(new FileWriter(failedWorkflowsFile, true), CSV_SEPARATOR);
                try {
                    writer.writeRecord(new String[] { file.getName(), e.getMessage() });
                } finally {
                    writer.close();
                }
                goProImporter.getStatus().incrFailed();
                throw new RuntimeException("Error importing workflow " + file.getName() + ": " + e.getMessage(), e);
            }
        }
        bindCsvWriteAfterCommit(completedWorkflowsFile, new CsvWriterClosure() {

            @Override
            public String[] getHeaders() {
                return new String[] {
                        "workflowId",
                        "nodeRef",
                        "documentId",
                        "documentNodeRef"
                };
            }

            @Override
            public void execute(CsvWriter writer) throws IOException {
                for (Entry<String, ImportedWorkflow> entry : batchCompletedWorkflowsMap.entrySet()) {
                	ImportedWorkflow wf = entry.getValue();
                    writer.writeRecord(new String[] {
                            wf.workflowId,
                            wf.nodeRef == null ? "" : wf.nodeRef.toString(),
                                    wf.documentId == null ? "" : wf.documentId,
                                            wf.documentNodeRef == null ? "" : wf.documentNodeRef.toString()
                    });
                }
            }
        });
    }

    private ImportedWorkflow createWorkflow(String workflowId, File file) throws DocumentException, ParseException {
        return importWf(file, workflowId);
    }
    
    
    
    private ImportedWorkflow importWf(File xml, String workflowId) throws DocumentException,
    ParseException {

    	
        Element root = xmlReader.read(xml).getRootElement();

        
        String documentId = getElementTextByAttribudeName(root, GP_ELEMENT_DOC_ID);
        NodeRef docRef = null;
        if (StringUtils.isBlank(documentId)) {
        	throw new RuntimeException("No documentId is set in the xml element " + GP_ELEMENT_DOC_ID);
        } else {
        	docRef = this.completedDocumentsMap.get(documentId);
        }
        
        if (docRef == null || !nodeService.exists(docRef)) {
        	throw new RuntimeException("No documentRef found in the list of completed documents or in the store: documentId  = " + documentId);
        }
        
        Map<QName, Serializable> docProps = nodeService.getProperties(docRef);
        
        NodeRef cwfRef = createCompoundWorkflow(root, docRef, docProps);
                
        
        return new ImportedWorkflow(workflowId, cwfRef, documentId, docRef);
    }   
    
    

    // HELPER METHODS
    /** RetryingTransactionHelper that only tries to do things once. */
    private RetryingTransactionHelper getTransactionHelper() {
        RetryingTransactionHelper helper = new RetryingTransactionHelper();
        helper.setMaxRetries(1);
        helper.setTransactionService(transactionService);
        return helper;
    }
    
    private String getElementTextByAttribudeName(Element root, String name) {
    	List fields = root.elements(GP_MAIN_ELEMENT_FIELD);
    	for (Object field: fields) {
    		String fieldName = ((Element)field).attributeValue(GP_MAIN_ATTRIBUTE_NAME);
    		if (name.equalsIgnoreCase(fieldName)) {
    			return ((Element)field).getStringValue().trim();
    		}
    	}
    	return null;
    }
    
    private Element getElementByAttribudeName(Element root, String name) {
    	List fields = root.elements(GP_MAIN_ELEMENT_FIELD);
    	for (Object field: fields) {
    		String fieldName = ((Element)field).attributeValue(GP_MAIN_ATTRIBUTE_NAME);
    		if (name.equalsIgnoreCase(fieldName)) {
    			return (Element)field;
    		}
    	}
    	return null;
    }
    
    private List<Element> getElementsByAttribudeName(Element root, String name) {
    	List fields = root.elements(GP_MAIN_ELEMENT_FIELD);
    	List<Element> foundFields = new ArrayList<Element>();
    	for (Object field: fields) {
    		String fieldName = ((Element)field).attributeValue(GP_MAIN_ATTRIBUTE_NAME);
    		if (name.equalsIgnoreCase(fieldName)) {
    			foundFields.add((Element)field);
    		}
    	}
    	return foundFields;
    }
    
    protected NodeRef createCompoundWorkflow(Element root, NodeRef docRef, Map<QName, Serializable> docProps) throws ParseException {
        String approvalDescription = getElementTextByAttribudeName(root, GP_ELEMENT_APPROVAL_DESCRIPTION);
        /*
        if (StringUtils.isBlank(approvalDescription)) {
            return;
        }
        */
        String approvalComments = getElementTextByAttribudeName(root, GP_ELEMENT_APPROVAL_COMMENTS);
        String approvalUserComments = getElementTextByAttribudeName(root, GP_ELEMENT_APPROVAL_USER_COMMENTS);
        String cwfTitle = getElementTextByAttribudeName(root, GP_ELEMENT_SUBJECT);
        String appStart = getElementTextByAttribudeName(root, GP_ELEMENT_APP_START);
        
        String wfOwnerName = parseUserFullName(getElementTextByAttribudeName(root, GP_ELEMENT_WF_OWNER));
        
        String creatorName = (String) docProps.get(DocumentCommonModel.Props.OWNER_NAME);
        String ownerId = (String) docProps.get(DocumentCommonModel.Props.OWNER_ID);
        String ownerName = (String) docProps.get(DocumentCommonModel.Props.OWNER_NAME);
        String ownerEmail = (String) docProps.get(DocumentCommonModel.Props.OWNER_NAME);
        
        
        Node ownerUserNode = getUserNodeByName(wfOwnerName);
        if (ownerUserNode != null) {
        	creatorName = wfOwnerName;
        	ownerName = wfOwnerName;
        	ownerId = (String)ownerUserNode.getProperties().get(ContentModel.PROP_USERNAME);
        	ownerEmail = (String)ownerUserNode.getProperties().get(ContentModel.PROP_EMAIL);
        }
        
        
        Date startedDateTime = null;
        Date finishedDueDateDateTime = null;
        try {
        	startedDateTime = (appStart.length() == 7)?dateTimeFormatdMMyy.parse(appStart + " 00:00:00"):dateTimeFormatddMMyy.parse(appStart + " 00:00:00");
        	finishedDueDateDateTime = (appStart.length() == 7)?dateTimeFormatdMMyy.parse(appStart + " 23:59:59"):dateTimeFormatddMMyy.parse(appStart + " 23:59:59");
        } catch (ParseException e) {
            throw new RuntimeException("Unable to parse APPSTART: " + appStart + ", error: " + e.getMessage(), e);
        }
        
        LogEntry logEntry = new LogEntry();
        logEntry.setComputerIp("127.0.0.1");
        logEntry.setComputerName("localhost");
        logEntry.setLevel(LogObject.DOCUMENT.getLevel());
        logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
        logEntry.setCreatorId("IMPORT");
        String logCreatorName = (String)docProps.get(DocumentCommonModel.Props.OWNER_NAME);
        if (StringUtils.isBlank(logCreatorName)) {
        	logCreatorName = defaultOwnerName;
        }
        logEntry.setCreatorName(logCreatorName);
        logEntry.setEventDescription(approvalComments);
        logEntry.setObjectId(docRef.toString());
        BeanHelper.getLogService().addImportedLogEntry(logEntry, startedDateTime);
        
        
        List<ReviewTask> reviewTasks = parseReviewTasks(approvalComments);
        
        boolean finishedWf = isFinishedWf(reviewTasks);
        Date wfFinishDateTime = (finishedWf)?getMaxTaskFinishDate(reviewTasks):null;
        
        Pair<NodeRef, Map<QName,Serializable>> wfPair = doWorkflow(docRef, docProps, startedDateTime, approvalDescription, wfFinishDateTime, cwfTitle, creatorName, ownerId, ownerName);
        NodeRef wfRef = wfPair.getFirst();
        int taskIndex = 0;
        
        Map<QName, Serializable> taskSearchableProps = wfPair.getSecond();
        
        boolean responsibleActiveSet = false;
        NodeRef firstTaskRef = null;
        String firstTaskOwnerId = null;

        boolean reviewToOtherOrgEnabled = BeanHelper.getWorkflowConstantsBean().isReviewToOtherOrgEnabled();
        String creatorInstitutionCode = institutionCode;
        String creatorInstitutionName = BeanHelper.getParametersService().getStringParameter(Parameters.TASK_OWNER_STRUCT_UNIT);
        
    	Map<QName, Serializable> props = new HashMap<QName, Serializable>();
    	

        
        WorkflowType workflowType = BeanHelper.getWorkflowConstantsBean().getWorkflowTypes().get(WorkflowSpecificModel.Types.REVIEW_WORKFLOW);
        for (ReviewTask reviewTaskInfo : reviewTasks) {
        	if (reviewTaskInfo == null) {
        		reviewTaskInfo = new ReviewTask();
        	}
            
        	String userId = DUMMY_USER_ID;
    		String userName = reviewTaskInfo.getUserName();
    		String email = defaultOwnerEmail;
        	
            String institutionName = null;
    		
        	Node userNode = getUserNodeByName(userName);
    		
        	if (userNode != null) {
        		userId = (String)userNode.getProperties().get(ContentModel.PROP_USERNAME);
        		email = (String)userNode.getProperties().get(ContentModel.PROP_EMAIL);        		
        	}
        	
        	
        	if (!DUMMY_USER_ID.equals(userId)) {
        		OrganizationStructure organizationStructure = getOwnerInstitution(userId);
        		institutionName = organizationStructure != null ? organizationStructure.getName() : null;
        	}
        	
            props = new HashMap<QName, Serializable>();
            props.put(WorkflowSpecificModel.Props.CREATOR_EMAIL, ownerEmail);
            props.put(WorkflowSpecificModel.Props.CREATOR_ID, ownerId);
            props.put(WorkflowCommonModel.Props.CREATOR_NAME, creatorName);
            props.put(WorkflowCommonModel.Props.DOCUMENT_TYPE, docProps.get(DocumentAdminModel.Props.OBJECT_TYPE_ID));
            props.put(WorkflowCommonModel.Props.OWNER_EMAIL, email);
            props.put(WorkflowCommonModel.Props.OWNER_ID, userId);
            props.put(WorkflowCommonModel.Props.OWNER_NAME, userName);            
            
            if (reviewToOtherOrgEnabled) {
            	props.put(WorkflowSpecificModel.Props.CREATOR_INSTITUTION_CODE, creatorInstitutionCode);
            	props.put(WorkflowSpecificModel.Props.CREATOR_INSTITUTION_NAME, creatorInstitutionName);
            	props.put(WorkflowSpecificModel.Props.INSTITUTION_NAME, institutionName);
            
            }
            
            props.put(WorkflowCommonModel.Props.STARTED_DATE_TIME, startedDateTime);
            props.put(WorkflowCommonModel.Props.STOPPED_DATE_TIME, null);
            props.put(WorkflowCommonModel.Props.STATUS, (reviewTaskInfo.getCompletedDateTime() != null)?Status.FINISHED.getName():Status.IN_PROGRESS.getName());
            props.put(WorkflowSpecificModel.Props.DUE_DATE, finishedDueDateDateTime);
            props.put(WorkflowSpecificModel.Props.DUE_DATE_DAYS, null);
            props.put(WorkflowSpecificModel.Props.IS_DUE_DATE_WORKING_DAYS, null);
            props.put(WorkflowSpecificModel.Props.RESOLUTION, approvalDescription);
            props.put(WorkflowSpecificModel.Props.WORKFLOW_RESOLUTION, approvalDescription);
            props.put(WorkflowCommonModel.Props.OUTCOME, reviewTaskInfo.getOutcome());
            props.put(WorkflowCommonModel.Props.COMPLETED_DATE_TIME, reviewTaskInfo.completedDateTime);
            props.put(WorkflowSpecificModel.Props.COMMENT, (StringUtils.isBlank(reviewTaskInfo.comment))?"":reviewTaskInfo.getComment());

            props.putAll(taskSearchableProps);
            Task task = BeanHelper.getWorkflowService().createTaskInMemory(wfRef, workflowType, props);
            Set<QName> aspects = task.getNode().getAspects();
            aspects.add(WorkflowSpecificModel.Aspects.SEARCHABLE);

            if (firstTaskRef == null && reviewTaskInfo.getCompletedDateTime() == null) {
                firstTaskRef = task.getNodeRef();
                firstTaskOwnerId = userId;
            }
            if (!responsibleActiveSet && reviewTaskInfo.getCompletedDateTime() == null && userId.equals(docProps.get(OWNER_ID))) {
                task.getNode().getProperties().put(WorkflowSpecificModel.Props.ACTIVE.toString(), Boolean.TRUE);
                aspects.add(WorkflowSpecificModel.Aspects.RESPONSIBLE);
                responsibleActiveSet = true;
                getPrivilegeService().setPermissions(docRef, userId, Privilege.EDIT_DOCUMENT);
            } else {
                getPrivilegeService().setPermissions(docRef, userId, Privilege.VIEW_DOCUMENT_FILES);
            }
            task.setTaskIndexInWorkflow(taskIndex++);
            BeanHelper.getWorkflowDbService().createTaskEntry(task, wfRef);
            
            if (finishedWf) {
            	docProps.put(DocumentCommonModel.Props.SEARCHABLE_HAS_ALL_FINISHED_COMPOUND_WORKFLOWS, Boolean.TRUE);
            } else {
            	docProps.put(DocumentCommonModel.Props.SEARCHABLE_HAS_STARTED_COMPOUND_WORKFLOWS, Boolean.TRUE);
            }
        }
    
        if (!responsibleActiveSet && firstTaskRef != null) {
            props = new HashMap<QName, Serializable>();
            props.put(WorkflowSpecificModel.Props.ACTIVE, Boolean.TRUE);
            getPrivilegeService().setPermissions(docRef, firstTaskOwnerId, Privilege.EDIT_DOCUMENT);
            BeanHelper.getWorkflowDbService().updateTaskProperties(firstTaskRef, props);
        }
        
        addCwfComments(wfRef, approvalUserComments);
        
        return wfRef;
	}
    
    
    private void addCwfComments(NodeRef wfRef, String commnetInfo) {
    	if (nodeService.exists(wfRef) && StringUtils.isNotBlank(commnetInfo)) {
    		NodeRef cwfRef = nodeService.getPrimaryParent(wfRef).getParentRef();
        	String [] commnetsArr = commnetInfo.split("\u00A5");
        	for (int i = 0; i < commnetsArr.length; i++) {
        		String [] commentParts = commnetsArr[i].split("\u20AE");
        		
        		if (commentParts.length != 3) {
        			log.warn("Invalid comment: " + commnetsArr[i]);
                	continue;
        		}
        		
        		String dateTimeStr = commentParts[0].trim();
        		Date createdDateTime = null;
        		try {
        			if (StringUtils.isNotBlank(dateTimeStr) && dateTimeStr.length() == 10) {
        				createdDateTime = (dateTimeStr.contains("."))?dateFormat.parse(dateTimeStr):dateFormatSlashed.parse(dateTimeStr);
        			} else {
        				createdDateTime = (dateTimeStr.contains("."))?dateTimeFormat.parse(dateTimeStr):dateTimeFormatSlashed.parse(dateTimeStr);;
        			}
                } catch (ParseException e) {
                	log.warn("Unable to parse commnet date: " + dateTimeStr + ", taskInfo: " + commnetInfo + ", error: " + e.getMessage(), e);
                	continue;
                }
        		
        		
        		String userId;
        		String userName = commentParts[1].trim();
        		Node userNode = getUserNodeByName(userName);
        		
            	if (userNode != null) {
            		userId = (String)userNode.getProperties().get(ContentModel.PROP_USERNAME);
            	} else {
            		userId = DUMMY_USER_ID;
            	}
            	
            	String commentText = commentParts[2].trim();
            	
        		Comment comment = new Comment(cwfRef.getId(), createdDateTime, userId, userName, commentText);
        		BeanHelper.getWorkflowDbService().addCompoundWorkfowComment(comment);
        	}
        }
    	
    }
    
    
    private Node getUserNodeByName(String userName) {
    	if (StringUtils.isBlank(userName)) {
    		return null;
    	}
    	List<Node> userNodes = getUserService().searchUsers(userName, false, -1);
		if (userNodes != null && userNodes.size() == 1) {
			return userNodes.get(0);
		}
		return null;
    }
    
    private boolean isFinishedWf(List<ReviewTask> reviewTasks) {
    	
    	for (ReviewTask task: reviewTasks) {
    		if (task.getCompletedDateTime() == null) {
    			return false;
    		}
    	}
    	return true;
    }
    
    private Date getMaxTaskFinishDate(List<ReviewTask> reviewTasks) {
    	Date maxDate = null;
    	for (ReviewTask task: reviewTasks) {
    		if (maxDate == null) {
    			maxDate = task.getCompletedDateTime();
    		} else if (maxDate.before(task.getCompletedDateTime())){
    			maxDate = task.getCompletedDateTime();
    		}
    	}
    	return maxDate;
    }
    
    private Pair<NodeRef, Map<QName, Serializable>>  doWorkflow(NodeRef docRef, Map<QName, Serializable> docProps, Date startDateTime, String resolution, Date finishedDateTime, String cwfTitle, 
    		String creatorName, String ownerId, String ownerName) {
    	NodeRef wfRef = null;
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        Map<QName, Serializable> taskSearchableProps = null;
        
        
        props = new HashMap<QName, Serializable>();
        props.put(WorkflowCommonModel.Props.CREATOR_NAME, creatorName);
        props.put(WorkflowCommonModel.Props.OWNER_ID, ownerId);
        props.put(WorkflowCommonModel.Props.OWNER_NAME, ownerName);
        props.put(WorkflowCommonModel.Props.STARTED_DATE_TIME, startDateTime);
        props.put(WorkflowCommonModel.Props.STATUS, (finishedDateTime != null)?Status.FINISHED.getName():Status.IN_PROGRESS.getName());
        props.put(WorkflowCommonModel.Props.STOPPED_DATE_TIME, null);
        if (finishedDateTime != null) {
        	props.put(WorkflowCommonModel.Props.FINISHED_DATE_TIME, finishedDateTime);
        }
        props.put(WorkflowCommonModel.Props.TYPE, CompoundWorkflowType.INDEPENDENT_WORKFLOW.name());
        props.put(WorkflowCommonModel.Props.TITLE, (StringUtils.isNotBlank(cwfTitle)) ? cwfTitle : docProps.get(DocumentCommonModel.Props.DOC_NAME));
        NodeRef cwfRef = getNodeService().createNode(
                BeanHelper.getConstantNodeRefsBean().getIndependentWorkflowsRoot(),
                WorkflowCommonModel.Assocs.COMPOUND_WORKFLOW,
                WorkflowCommonModel.Assocs.COMPOUND_WORKFLOW,
                WorkflowCommonModel.Types.COMPOUND_WORKFLOW,
                props
                ).getChildRef();
        getNodeService().createAssociation(docRef, cwfRef, DocumentCommonModel.Assocs.WORKFLOW_DOCUMENT);
        getWorkflowService().updateMainDocument(cwfRef, docRef);

        props = new HashMap<QName, Serializable>();
        props.put(WorkflowCommonModel.Props.CREATOR_NAME, creatorName);
        props.put(WorkflowCommonModel.Props.MANDATORY, Boolean.FALSE);
        props.put(WorkflowCommonModel.Props.PARALLEL_TASKS, Boolean.TRUE);
        props.put(WorkflowCommonModel.Props.STARTED_DATE_TIME, startDateTime);
        props.put(WorkflowCommonModel.Props.STOPPED_DATE_TIME, null);
        props.put(WorkflowCommonModel.Props.STATUS, (finishedDateTime != null)?Status.FINISHED.getName():Status.IN_PROGRESS.getName());
        props.put(WorkflowCommonModel.Props.STOP_ON_FINISH, Boolean.FALSE);
        props.put(WorkflowSpecificModel.Props.RESOLUTION, resolution);
        wfRef = getNodeService().createNode(
                cwfRef,
                WorkflowCommonModel.Assocs.WORKFLOW,
                WorkflowCommonModel.Assocs.WORKFLOW,
                WorkflowSpecificModel.Types.REVIEW_WORKFLOW,
                props
                ).getChildRef();
        taskSearchableProps = WorkflowUtil.getTaskSearchableProps(props);
        taskSearchableProps.put(WorkflowSpecificModel.Props.COMPOUND_WORKFLOW_ID, cwfRef.getId());

        return new Pair<NodeRef, Map<QName,Serializable>>(wfRef, taskSearchableProps);
    }
    
        
    
    private List<ReviewTask> parseReviewTasks(String tasksInfo) {
    	List<ReviewTask> reviewTasks = new ArrayList<>();
    	if (StringUtils.isNotBlank(tasksInfo)) {
        	String [] reviewerTasksArr = tasksInfo.split("\u00A5");
        	for (int i = 0; i < reviewerTasksArr.length; i++) {
        		String [] taskInfo = reviewerTasksArr[i].split("\u20AE");
        		
        		if (taskInfo.length != 3) {
        			throw new RuntimeException("Invalid task info: " + reviewerTasksArr[i]);
        		}
        		
        		String dateTimeStr = taskInfo[0].trim();
        		Date completedDateTime = null;
        		try {
        			if (StringUtils.isNotBlank(dateTimeStr) && dateTimeStr.length() == 10) {
        				completedDateTime = (dateTimeStr.contains("."))?dateFormat.parse(dateTimeStr):dateFormatSlashed.parse(dateTimeStr);
        			} else {
        				completedDateTime = (dateTimeStr.contains("."))?dateTimeFormat.parse(dateTimeStr):dateTimeFormatSlashed.parse(dateTimeStr);;
        			}
                } catch (ParseException e) {
                	throw new RuntimeException("Unable to parse approval date: " + dateTimeStr + ", taskInfo: " + taskInfo + ", error: " + e.getMessage(), e);
                }
        		String name = taskInfo[1].trim();
        		String outcomeInfo = taskInfo[2].trim();
        		String comment = (outcomeInfo.contains(":") 
        				&& (outcomeInfo.toLowerCase().contains(TASK_APPROVED) || outcomeInfo.toLowerCase().contains(TASK_NOT_APPROVED))) 
        				? StringUtils.substringAfter(outcomeInfo, ":").trim() 
        						: outcomeInfo;
        		String outcome = null;
        		if (outcomeInfo.toLowerCase().contains(TASK_APPROVED)) {
        			outcome = "Kooskõlastatud";
        		} else if (outcomeInfo.toLowerCase().contains(TASK_NOT_APPROVED)) {
        			outcome =  "Kooskõlastamata";
        		}
        		
        		
        		ReviewTask rTask = new ReviewTask();
        		rTask.setUserName(name);
        		rTask.setCompletedDateTime(completedDateTime);
        		rTask.setOutcome(outcome);
        		rTask.setComment(comment);
        		reviewTasks.add(rTask);
        	}
        }
    	Collections.sort(reviewTasks);
    	return reviewTasks;     
    }
    
    
    private class ReviewTask implements Comparable<ReviewTask> {
    	private String userName;
		private Date completedDateTime;
    	private String comment;
    	private String outcome;
    	
    	public String getOutcome() {
			return outcome;
		}
		public void setOutcome(String outcome) {
			this.outcome = outcome;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public Date getCompletedDateTime() {
			return completedDateTime;
		}
		public void setCompletedDateTime(Date completedDateTime) {
			this.completedDateTime = completedDateTime;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}
    	
		
		@Override
		public int compareTo(ReviewTask o) {
			if (this.getCompletedDateTime() == null && o.getCompletedDateTime() == null) {
				return 0;
			}
			if (this.getCompletedDateTime() == null && o.getCompletedDateTime() != null) {
				return 1;
			}
			if (this.getCompletedDateTime() != null && o.getCompletedDateTime() == null) {
				return -1;
			}
			return this.getCompletedDateTime().compareTo(o.getCompletedDateTime());
		}
    }
    
    
    private OrganizationStructure getOwnerInstitution(String userId) {
    	Node user = BeanHelper.getUserService().getUser(userId);
        if (user == null) {
            return null;
        }
        String organizationId = (String) user.getProperties().get(ContentModel.PROP_ORGID);
        if (StringUtils.isBlank(organizationId)) {
            return null;
        }
        OrganizationStructure organizationStructure = BeanHelper.getOrganizationStructureService().getOrganizationStructure(organizationId);
        while (organizationStructure != null) {
            String institutionRegCode = organizationStructure.getInstitutionRegCode();
            if (StringUtils.isNotBlank(institutionRegCode)) {
                List<Node> institutions = BeanHelper.getAddressbookService().getContactsByRegNumber(institutionRegCode);
                if (!institutions.isEmpty()) {
                    Map<String, Object> orgProps = institutions.get(0).getProperties();
                    if (Boolean.TRUE.equals(orgProps.get(Props.DVK_CAPABLE))) {
                        // dvk capable organization contact found
                        return organizationStructure;
                    }
                    
                    return null;
                }
                return null;
            }
            organizationStructure = BeanHelper.getOrganizationStructureService().getOrganizationStructure(organizationStructure.getSuperUnitId());
        }
        // organization with non-empty reg code is not found
        return null;
    }
    

}
