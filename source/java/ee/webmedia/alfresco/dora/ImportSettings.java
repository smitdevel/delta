package ee.webmedia.alfresco.dora;

import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


public class ImportSettings implements Cloneable {
    private static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(ImportSettings.class);

    private String dataFolder;

    private String workFolder;

    private String defaultOwnerId;
    
    private int batchSize = 50;
    
    private Date docListArchivalsSeparatingDate;

    private String taskOwnerStructUnitAuthority;
    
    private List<String> taskOwnerStructUnitAuthorityPrivileges;
    
    private boolean forRegisteration;


    public boolean isForRegisteration() {
		return forRegisteration;
	}

	public void setForRegisteration(boolean forRegisteration) {
		this.forRegisteration = forRegisteration;
	}

	public String getDataFolder() {
        return dataFolder;
    }

    public void setDataFolder(String dataFolder) {
        this.dataFolder = dataFolder;
    }

    public String getWorkFolder() {
        return workFolder;
    }

    public void setWorkFolder(String workFolder) {
        this.workFolder = workFolder;
    }

    public String getDefaultOwnerId() {
        return defaultOwnerId;
    }

    public void setDefaultOwnerId(String defaultOwnerId) {
        this.defaultOwnerId = defaultOwnerId;
    }

    public String getTaskOwnerStructUnitAuthority() {
        return taskOwnerStructUnitAuthority;
    }

    public void setTaskOwnerStructUnitAuthority(String taskOwnerStructUnitAuthority) {
        this.taskOwnerStructUnitAuthority = taskOwnerStructUnitAuthority;
    }
    
    public List<String> getTaskOwnerStructUnitAuthorityPrivileges() {
        return taskOwnerStructUnitAuthorityPrivileges;
    }

    public void setTaskOwnerStructUnitAuthorityPrivileges(List<String> taskOwnerStructUnitAuthorityPrivileges) {
        this.taskOwnerStructUnitAuthorityPrivileges = taskOwnerStructUnitAuthorityPrivileges;
    }
    
    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
    
    public Date getDocListArchivalsSeparatingDate() {
        return docListArchivalsSeparatingDate;
    }

    public void setDocListArchivalsSeparatingDate(Date docListArchivalsSeparatingDate) {
        this.docListArchivalsSeparatingDate = docListArchivalsSeparatingDate;
    }

    public boolean isValid() {
        
        return isNotBlank(dataFolder) && isNotBlank(workFolder) && isNotBlank(defaultOwnerId) 
        		&& docListArchivalsSeparatingDate != null && batchSize > 0;
    }

    @Override
    public ImportSettings clone() {
        try {
            return (ImportSettings) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public File getDataFolderFile(String file) {
        return new File(dataFolder, file);
    }

    public File getWorkFolderFile(String file) {
        return new File(workFolder, file);
    }
    
    public boolean isVolumeOpen(Date volumeEnded, String volumeStatus) {
    	if (StringUtils.isNotBlank(volumeStatus) && ("suletud".equalsIgnoreCase(volumeStatus) || "hävitatud".equalsIgnoreCase(volumeStatus))) {
    		return false;
    	}
        return (StringUtils.isNotBlank(volumeStatus) && "avatud".equalsIgnoreCase(volumeStatus)) || docListArchivalsSeparatingDate == null || volumeEnded == null || !volumeEnded.before(docListArchivalsSeparatingDate);
    }
}
