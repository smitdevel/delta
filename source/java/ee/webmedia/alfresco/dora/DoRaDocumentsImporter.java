package ee.webmedia.alfresco.dora;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentAdminService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentDynamicService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentListService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNamespaceService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getUserService;
import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.OWNER_ID;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.repo.transaction.TransactionListenerAdapter;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.util.Assert;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.classificator.enums.DocumentStatus;
import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.common.web.WmNode;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel.Props;
import ee.webmedia.alfresco.docadmin.service.Field;
import ee.webmedia.alfresco.docconfig.service.DynamicPropertyDefinition;
import ee.webmedia.alfresco.docdynamic.model.DocumentChildModel;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamic;
import ee.webmedia.alfresco.document.file.model.FileModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.document.model.DocumentSpecificModel;
import ee.webmedia.alfresco.document.service.DocumentService;
import ee.webmedia.alfresco.document.service.DocumentService.AssocType;
import ee.webmedia.alfresco.dora.DoRaDocumentsMapper.Mapping;
import ee.webmedia.alfresco.dora.DoRaDocumentsMapper.PropertyValue;
import ee.webmedia.alfresco.dora.DoRaDocumentsMapper.TypeInfo;
import ee.webmedia.alfresco.functions.model.FunctionsModel;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.orgstructure.model.OrganizationStructureModel;
import ee.webmedia.alfresco.orgstructure.service.OrganizationStructureService;
import ee.webmedia.alfresco.series.model.SeriesModel;
import ee.webmedia.alfresco.utils.FilenameUtil;
import ee.webmedia.alfresco.utils.RepoUtil;
import ee.webmedia.alfresco.utils.UserUtil;
import ee.webmedia.alfresco.volume.model.VolumeModel;

/**
 * Imports documents and files from DoRa.
 */
public class DoRaDocumentsImporter {

    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(DoRaDocumentsImporter.class);
    
    
    // DoRa elements
    private static final String ELEMENT_FIELD = "Field";
    private static final String ATTRIBUTE_NAME = "Name";
    
    private static final String ELEMENT_NAME_DELTA_LOCATION = "DeltaLocation";
    private static final String ELEMENT_NAME_FUNCTION = "Function";
    private static final String ELEMENT_NAME_SERIES = "Series";
    private static final String ELEMENT_NAME_VOLUME = "Volumes";
    private static final String ELEMENT_NAME_DOCUMENT_TYPE = "DocumentType";
    
    private static final String ELEMENT_NAME_META_DATA = "MetaData";
    private static final String ELEMENT_NAME_PARTIES = "Parties";
    private static final String ELEMENT_NAME_PARTY = "Party";
    private static final String ELEMENT_NAME_RECIPIENTS = "Recipients";
    private static final String ELEMENT_NAME_RECIPIENT = "Recipient";
    private static final String ELEMENT_NAME_ADDITIONAL_RECIPIENTS = "AdditionalRecipients";
    private static final String ELEMENT_NAME_ADDITIONAL_RECIPIENT = "AdditionalRecipient";
    private static final String ELEMENT_NAME_DOCUMENT_FILE = "DocumentFile";
    private static final String ELEMENT_NAME_ANNEXES = "Annexes";
    private static final String ELEMENT_NAME_ANNEX = "Annex";
    private static final String ELEMENT_NAME_RELATIONS = "Relations";
    private static final String ELEMENT_NAME_RELATION = "Relation";
    private static final String ELEMENT_NAME_ID = "ID";
    private static final String ELEMENT_NAME_TYPE = "Type";

    private static final String STORE_ACTIVE = "Aktiivne";
        
    
    
    private static final String DUMMY_USER_ID = "99999999999";



    protected static final char CSV_SEPARATOR = ';';
    private static final String CREATOR_MODIFIER = "DHS";
    private static final FastDateFormat staticDateTimeFormat = FastDateFormat.getInstance("dd.MM.yyyy HH:mm:ss");
    private static final FastDateFormat staticDateFormat = FastDateFormat.getInstance("dd.MM.yyyy");

    private final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private final DateFormat dateTimeFormatddMMyy = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
    private final DateFormat dateTimeFormatdMMyy = new SimpleDateFormat("d.MM.yy HH:mm:ss");

    protected SAXReader xmlReader = new SAXReader();

    private File dataFolder;
    private File workFolder;
    private int batchSize;
    private String institutionCode;
    private String defaultOwnerId;
    private String defaultOwnerName;
    private String defaultOwnerEmail;
    private String defaultOwnerJobTitle;
    private String defaultOwnerPhone;
    private List<String> defaultOwnerOrgUnit;
    private String defaultOwnerServiceRank;
    private String defaultOwnerWorkAddress;
    
    private Collection<StoreRef> storeRefs = new LinkedHashSet<StoreRef>();
    
    
    // [SPRING BEANS
    private TransactionService transactionService;
    private DocumentService documentService;
    private GeneralService generalService;
    private FileFolderService fileFolderService;
    private NodeService nodeService;
    private DoRaDocumentsMapper doRaDocumentsMapper;
    private BehaviourFilter behaviourFilter;
    private DoRaImporter doraImporter;
    

    public DoRaDocumentsImporter(DoRaImporter doraImporter) {
        
        
        setDocumentService(BeanHelper.getDocumentService());
        setTransactionService(BeanHelper.getTransactionService());
        setGeneralService(BeanHelper.getGeneralService());
        setFileFolderService(BeanHelper.getFileFolderService());
        setNodeService(BeanHelper.getNodeService());
        doRaDocumentsMapper = new DoRaDocumentsMapper();
        doRaDocumentsMapper.init();
        doRaDocumentsMapper.setNamespaceService(getNamespaceService());
        doRaDocumentsMapper.setDictionaryService(BeanHelper.getDictionaryService());
        doRaDocumentsMapper.setGeneralService(BeanHelper.getGeneralService());
        setBehaviourFilter(BeanHelper.getPolicyBehaviourFilter());
        setDoraImporter(doraImporter);

        dateFormat.setLenient(false);
        dateTimeFormat.setLenient(false);
    }

    // INJECTORS
    public void setGeneralService(GeneralService generalService) {
        this.generalService = generalService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setDoRaDocumentsMapper(DoRaDocumentsMapper doRaDocumentsMapper) {
        this.doRaDocumentsMapper = doRaDocumentsMapper;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setBehaviourFilter(BehaviourFilter behaviourFilter) {
        this.behaviourFilter = behaviourFilter;
    }

    public void setDoraImporter(DoRaImporter doraImporter) {
        this.doraImporter = doraImporter;
    }

    /**
     * Runs documents import process
     */
    public void runImport(File dataFolder, File workFolder, int batchSize, String defaultOwnerId, String institutionCode)
                    throws Exception {
        this.dataFolder = dataFolder;
        this.workFolder = workFolder;
        this.batchSize = batchSize;
        this.institutionCode = institutionCode;
        this.defaultOwnerId = defaultOwnerId;
        Map<QName, Serializable> defaultUserProps = getUserService().getUserProperties(defaultOwnerId);
        this.defaultOwnerName = UserUtil.getPersonFullName1(defaultUserProps);
        this.defaultOwnerEmail = (String)defaultUserProps.get(ContentModel.PROP_EMAIL);
        this.defaultOwnerPhone = (String)defaultUserProps.get(ContentModel.PROP_TELEPHONE);
        this.defaultOwnerJobTitle = (String)defaultUserProps.get(ContentModel.PROP_JOBTITLE);
        this.defaultOwnerOrgUnit = (List<String>)defaultUserProps.get(OrganizationStructureModel.Props.ORGANIZATION_PATH);
        this.defaultOwnerServiceRank = (String)defaultUserProps.get(ContentModel.PROP_SERVICE_RANK);
        this.defaultOwnerWorkAddress = (String)defaultUserProps.get(ContentModel.PROP_STREET_HOUSE);
        init();
        // Doc import
        try {        	
        	File completedStructureFile = new File(workFolder, StructureImporter.COMPLETED_FILENAME);
        	if (!completedStructureFile.exists()) {
                log.info("Skipping documents import, struucture was not imported yet, file does not exist: " + completedStructureFile);
                return;
            }
        	storeRefs.add(generalService.getStore());
        	storeRefs.add(generalService.getArchivalsStoreRef());
        	
            loadCompletedCaseFilesVolumes();
            loadDocuments();
            loadCompletedDocuments();
            loadPostponedAssocs();
            createDocuments();
        } finally {
            writePostponedAssocs();
            documentsMap = null;
            mappings = null;
            postponedAssocs = null;
            completedCaseFiles = null;
            completedVolumes = null;
        }


        // Files indexing
        loadIndexedFiles();
        indexFiles();
    }

    private void init() {
        documentsMap = new TreeMap<String, File>();
        completedDocumentsMap = new TreeMap<String, NodeRef>();
        postponedAssocs = new TreeMap<String, List<PostponedAssoc>>();
        postponedAssocsCommited = new TreeMap<String, List<PostponedAssoc>>();
        completedCaseFiles = new HashMap<String, DoraVolume>();
        completedVolumes = new HashMap<String, List<DoraVolume>>();
        mappings = null;
        seriesMappings = new HashMap<>();
        indexedFiles = null;
        filesToIndex = null;
        filesToProceed = null;
        doRaDocumentsMapper.init();
    }

    protected NavigableMap<String /* documentId */, File> documentsMap;
    protected NavigableMap<String /* documentId */, NodeRef> completedDocumentsMap;
    protected NavigableMap<String /* documentId */, NodeRef> readyToIndexDocumentsMap;
    protected NavigableMap<String /* documentId */, List<PostponedAssoc>> postponedAssocs;
    protected NavigableMap<String /* documentId */, List<PostponedAssoc>> postponedAssocsCommited;
    private final Map<String, Pair<NodeRef, Map<QName, Serializable>>> userDataByUserName = new HashMap<String, Pair<NodeRef, Map<QName, Serializable>>>();
    private Map<String, DoraVolume> completedCaseFiles;
    private Map<String, List<DoraVolume>> completedVolumes;


    protected NavigableSet<Integer> filesToProceed;
    protected File completedDocumentsFile;
    protected File failedDocumentsFile;
    protected File postponedAssocsFile;
    

    private Map<String, Mapping> mappings;
    private Map<String, String> seriesMappings = new HashMap<>();
    
    
    private void loadCompletedCaseFilesVolumes() throws IOException {
    	completedCaseFiles = new HashMap<String, DoraVolume>();
        completedVolumes = new HashMap<String, List<DoraVolume>>();
        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(new File(workFolder, "completed_volumes_cases.csv"))), CSV_SEPARATOR,
                Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String volumeType = reader.get(0);
                String mark = reader.get(1);
                NodeRef nodeRef = new NodeRef(reader.get(4));
                Date validFrom = null;
                try {
                	validFrom = dateFormat.parse(reader.get(5));
                } catch (Throwable t) {}
                Date validTo = null;
                try {
                	validTo = dateFormat.parse(reader.get(6));
                } catch (Throwable e) {}
                String seriesIdentifier = reader.get(8);
                String seriesTitle = reader.get(9);
                String function = reader.get(7);
                DoraVolume volume = new DoraVolume();
                volume.nodeRef = nodeRef;
                volume.mark = mark;
                volume.seriesIdentifier = seriesIdentifier;
                volume.seriesTitle = seriesTitle;
                volume.function = function;
                volume.validFrom = validFrom;
                volume.validTo = validTo;
                if ("caseFile".equals(volumeType)) {
                	completedCaseFiles.put(mark, volume);
                } else {
                	List<DoraVolume> volumes = completedVolumes.get(seriesIdentifier);
                	if (volumes == null) {
                		volumes = new ArrayList<DoraVolume>();
                		completedVolumes.put(seriesIdentifier + " " + seriesTitle, volumes);
                	}
                	volumes.add(volume);
                	
                }
            }
        } finally {
            reader.close();
        }
    }

    protected void loadDocuments() {
        documentsMap = new TreeMap<String, File>();
        log.info("Getting xml entries of " + dataFolder);
        File[] files = dataFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return StringUtils.isNotBlank(name) && name.endsWith(".xml") && !name.toLowerCase().contains("mapping");
            }
        });

        log.info("Directory listing contains " + files.length + " xml entries, parsing them to documents");
        for (File file : files) {
            String name = file.getName();
            if (name.endsWith(".xml") && !name.contains("mappings")) {
                // Document
            	String documentId = StringUtils.substringBefore(name, ".xml");
                log.debug("Found documentId=" + documentId);
                documentsMap.put(documentId, file);
                
            }
        }
        log.info("Completed parsing directory listing, got " + documentsMap.size() + " documents");
    }

    
    
    private File indexedFilesFile;
    private Set<String> indexedFiles;
    private NavigableSet<String> filesToIndex;

    protected void loadIndexedFiles() throws Exception {
        indexedFilesFile = new File(workFolder, "indexed_files.csv");
        indexedFiles = new HashSet<String>();

        if (!indexedFilesFile.exists()) {
            log.info("Skipping loading previously indexed files, file does not exist: " + indexedFilesFile);
        } else {

            log.info("Loading previously indexed files from file " + indexedFilesFile);

            CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(indexedFilesFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
            try {
                reader.readHeaders();
                while (reader.readRecord()) {
                    String documentId = reader.get(0);
                    indexedFiles.add(documentId);
                }
            } finally {
                reader.close();
            }
            log.info("Loaded " + indexedFiles.size() + " documents with previously indexed files");
        }
        
        readyToIndexDocumentsMap = new TreeMap<String, NodeRef>();

        completedDocumentsFile = new File(workFolder, "completed_docs.csv");
        if (!completedDocumentsFile.exists()) {
            log.info("Skipping loading previously completed documentId-s, file does not exist: " + completedDocumentsFile);
            return;
        }

        log.info("Loading previously completed documentId-s- (for indexing) from file " + completedDocumentsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedDocumentsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String documentId = reader.get(0);
                String nodeRefString = reader.get(1);
                if (StringUtils.isNotBlank(nodeRefString)) {
                    NodeRef documentRef = new NodeRef(nodeRefString);
                    readyToIndexDocumentsMap.put(documentId, documentRef);
                }
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + readyToIndexDocumentsMap.size() + " previously completed documentId-s (for indexing)");


        filesToIndex = new TreeSet<String>(readyToIndexDocumentsMap.keySet());
        filesToIndex.removeAll(indexedFiles);

        log.info("Total documents with files to index: " + filesToIndex.size());
    }

    private void indexFiles() {
        if (filesToIndex.isEmpty()) {
            log.info("Skipping files indexing, no files found");
            return;
        }
        log.info("Starting files indexing, total = " + filesToIndex.size());
        FilesIndexBatchProgress batchProgress = new FilesIndexBatchProgress(filesToIndex);
        batchProgress.run();
        log.info("Files INDEXING COMPLETE :)");
    }

    private class FilesIndexBatchProgress extends BatchProgress<String> {

        public FilesIndexBatchProgress(Set<String> origin) {
            this.origin = origin;
            processName = "Files indexing";
        }

        @Override
        void executeBatch() {
            createFilesIndexBatch(batchList);
        }
    }

    protected void createFilesIndexBatch(final List<String> batchList) {
        final Set<String> batchCompletedFiles = new HashSet<String>(batchSize);
        for (String documentId : batchList) {
            if (log.isTraceEnabled()) {
                log.trace("Processing files with docId = " + documentId);
            }
            NodeRef documentRef = completedDocumentsMap.get(documentId);
            if (documentRef == null) {
                continue;
            }
            if (!nodeService.exists(documentRef)) {
                log.error("Skipping indexing files for documentId=" + documentId + ", node does not exist: " + documentRef);
                continue;
            }
            try {
                Map<QName, Serializable> origProps = nodeService.getProperties(documentRef);
                Map<QName, Serializable> setProps = new HashMap<QName, Serializable>();
                setProps.put(ContentModel.PROP_MODIFIER, origProps.get(ContentModel.PROP_MODIFIER));
                setProps.put(ContentModel.PROP_MODIFIED, origProps.get(ContentModel.PROP_MODIFIED));
                documentService.updateSearchableFiles(documentRef, setProps);
            } catch (Exception e) {
                throw new RuntimeException("Error indexing files for document id=" + documentId + ", nodeRef=" + documentRef + ": " + e.getMessage(), e);
            }
            batchCompletedFiles.add(documentId);
        }
        bindCsvWriteAfterCommit(indexedFilesFile, new CsvWriterClosure() {

            @Override
            public void execute(CsvWriter writer) throws IOException {
                for (String documentId : batchCompletedFiles) {
                    writer.writeRecord(new String[] {
                            documentId.toString()
                    });
                    indexedFiles.add(documentId);
                }
            }

            @Override
            public String[] getHeaders() {
                return new String[] { "documentId" };
            }
        });
    }

    private boolean findUserNode(String doraUserName, Map<QName, Serializable> props, QName idQname, QName nameQname, QName emailQname) {
    	Node userNode = null;
    	String userFullname = null;
    	if (StringUtils.isNotBlank(doraUserName)) {
    		userFullname = parseUserFullName(doraUserName);
    		
    		List<Node> userNodes = getUserService().searchUsers(userFullname, false, -1);
    		if (userNodes != null && userNodes.size() == 1) {
    			userNode = userNodes.get(0);
    		}
    	}
    	if (userNode != null) {
    		String userId = (String)userNode.getProperties().get(ContentModel.PROP_USERNAME);
    		String userName = (String)userNode.getProperties().get(ContentModel.PROP_FIRSTNAME) + " " + (String)userNode.getProperties().get(ContentModel.PROP_LASTNAME);
    		if (userFullname == null || !userFullname.equals(userName)) {
    			return false;
    		}
    		String email = (String)userNode.getProperties().get(ContentModel.PROP_EMAIL);
    		if (idQname != null) {
    			props.put(idQname, userId);
    		}
    		if (nameQname != null) {
    			props.put(nameQname, (StringUtils.isNotBlank(userName)?userName:userId));
    		}
    		if (emailQname != null && StringUtils.isNotBlank(email)) {
    			props.put(emailQname, email);
    		}
    		return true;
    	} else {
    		return false;
    	}
	}
    
    private String parseUserFullName(String doraUserName) {
    	String userFullname = doraUserName;
		if (doraUserName.contains("/")) {
			userFullname = StringUtils.substringBefore(doraUserName, "/").trim();
		}
		if (doraUserName.contains("(")) {
			userFullname = StringUtils.substringBefore(doraUserName, "(").trim();
		}
		
		return userFullname.trim();
    }

    private void setOwnerProperties(Map<QName, Serializable> props, TypeInfo typeInfo) {
        String gpOwnerName = StringUtils.stripToNull((String) props.get(DocumentCommonModel.Props.OWNER_NAME));
        String gpSignerName = StringUtils.stripToNull((String) props.get(DocumentCommonModel.Props.SIGNER_NAME));
        
        boolean ownerFound = false;
        boolean signerFound = false;
        if (StringUtils.isNotBlank(gpOwnerName)) {
        	QName emailQname = null;
        	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(DocumentCommonModel.Props.OWNER_EMAIL.getLocalName());
    		if (pair != null) {
    			emailQname = DocumentCommonModel.Props.OWNER_EMAIL;
    		}
        	ownerFound = findUserNode(gpOwnerName, props, OWNER_ID, DocumentCommonModel.Props.OWNER_NAME, emailQname);
        	if (!ownerFound) {
        		addMissingFieldToCommment(DocumentCommonModel.Props.OWNER_NAME.getLocalName() + ":" + gpOwnerName, props, typeInfo);
       		}
        }
        
        if (!ownerFound && StringUtils.isNotBlank(defaultOwnerId)) {
        	props.put(DocumentCommonModel.Props.OWNER_ID, defaultOwnerId);
        	props.put(DocumentCommonModel.Props.OWNER_NAME, defaultOwnerName);
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_EMAIL)) {
        		props.put(DocumentCommonModel.Props.OWNER_EMAIL, defaultOwnerEmail);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_PHONE)) {
        		props.put(DocumentCommonModel.Props.OWNER_PHONE, defaultOwnerPhone);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_JOB_TITLE)) {
        		props.put(DocumentCommonModel.Props.OWNER_JOB_TITLE, defaultOwnerJobTitle);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT)) {
        		props.put(DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT, (ArrayList)defaultOwnerOrgUnit);
        	}
        	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_SERVICE_RANK)) {
        		props.put(DocumentDynamicModel.Props.OWNER_SERVICE_RANK, defaultOwnerServiceRank);
        	}
        	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_WORK_ADDRESS)) {
        		props.put(DocumentDynamicModel.Props.OWNER_WORK_ADDRESS, defaultOwnerWorkAddress);
        	}

        }
        
        if (StringUtils.isNotBlank(gpSignerName)) {
       		signerFound = findUserNode(gpSignerName, props, null, DocumentCommonModel.Props.SIGNER_NAME, null);
        }
        
    }
    
    private boolean hasField(TypeInfo typeInfo, QName fieldQName) {
    	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldQName.getLocalName());
		return pair != null;
    }


    private void checkStop() {
        doraImporter.checkStop();
    }

    private abstract class BatchProgress<E> {
        Collection<E> origin;
        List<E> batchList;
        int totalSize;
        int i;
        int completedSize;
        int thisRunCompletedSize;
        long thisRunStartTime;
        long startTime;
        String processName;

        private void init() {
            totalSize = origin.size();
            thisRunStartTime = System.currentTimeMillis();
            startTime = thisRunStartTime;
            i = 0;
            completedSize = 0;
            batchList = new ArrayList<E>(batchSize);
        }

        abstract void executeBatch() throws Exception;

        void executeInTransaction() {
            try {
                getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Object>() {
                    @Override
                    public Object execute() throws Throwable {
                        behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
                        executeBatch();
                        return null;
                    }
                });
            } catch (Exception e) {
                log.error("Import batch of documents failed, transaction rolled back, continuing with next batch", e);
            }
        }

        private void step() {
            executeInTransaction();
            completedSize += batchList.size();
            thisRunCompletedSize += batchList.size();
            batchList = new ArrayList<E>(batchSize);
            long endTime = System.currentTimeMillis();
            double completedPercent = (completedSize) * 100L / ((double) totalSize);
            double lastDocsPerSec = (i) * 1000L / ((double) (endTime - startTime));
            long thisRunTotalTime = endTime - thisRunStartTime;
            double totalDocsPerSec = (thisRunCompletedSize) * 1000L / ((double) thisRunTotalTime);
            long remainingSize = ((long) totalSize) - ((long) completedSize);
            long divisor = (thisRunCompletedSize) * 60000L;
            int etaMinutes = ((int) (remainingSize * thisRunTotalTime / divisor)) + 1;
            int etaHours = 0;
            if (etaMinutes > 59) {
                etaHours = etaMinutes / 60;
                etaMinutes = etaMinutes % 60;
            }
            String eta = etaMinutes + "m";
            if (etaHours > 0) {
                eta = etaHours + "h " + eta;
            }
            i = 0;
            String info = "%s: %6.2f%% completed - %7d of %7d, %5.1f items per second (last), %5.1f (total), ETA %s";
            log.info(String.format(info, processName, completedPercent, completedSize, totalSize, lastDocsPerSec, totalDocsPerSec, eta));
            startTime = endTime;
        }

        public void run() {
            init();
            checkStop();
            for (E e : origin) {
                batchList.add(e);
                i++;
                if (i >= batchSize) {
                    step();
                    checkStop();
                }
            }
            step();
        }

    }
    
    private static interface CsvWriterClosure {
        void execute(CsvWriter writer) throws IOException;

        String[] getHeaders();
    }

    private static void bindCsvWriteAfterCommit(final File file, final CsvWriterClosure closure) {
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
                try {
                    // Write created documents
                    boolean exists = file.exists();
                    if (!exists) {
                        OutputStream outputStream = new FileOutputStream(file);
                        try {
                            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
                            outputStream.write("\ufeff".getBytes("UTF-8"));
                        } finally {
                            outputStream.close();
                        }
                    }
                    CsvWriter writer = new CsvWriter(new FileWriter(file, true), CSV_SEPARATOR);
                    try {
                        if (!exists) {
                            writer.writeRecord(closure.getHeaders());
                        }
                        closure.execute(writer);
                    } finally {
                        writer.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Error writing file '" + file + "': " + e.getMessage(), e);
                }
            }
        });
    }
    
    

    protected void loadCompletedDocuments() throws Exception {
        failedDocumentsFile = new File(workFolder, "failed_docs.csv");
        OutputStream outputStream = new FileOutputStream(failedDocumentsFile);
        try {
            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
            outputStream.write("\ufeff".getBytes("UTF-8"));
        } finally {
            outputStream.close();
        }
        CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
        try {
            writer.writeRecord(new String[] {
                    "documentFileName",
                    "errorMsg"
            });
        } finally {
            writer.close();
        }

        completedDocumentsMap = new TreeMap<String, NodeRef>();

        completedDocumentsFile = new File(workFolder, "completed_docs.csv");
        if (!completedDocumentsFile.exists()) {
            log.info("Skipping loading previously completed documentId-s, file does not exist: " + completedDocumentsFile);
            return;
        }

        log.info("Loading previously completed documentId-s from file " + completedDocumentsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedDocumentsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String documentId = reader.get(0);
                String nodeRefString = reader.get(1);
                if (StringUtils.isNotBlank(nodeRefString)) {
                    NodeRef documentRef = new NodeRef(nodeRefString);
                    completedDocumentsMap.put(documentId, documentRef);
                }
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + completedDocumentsMap.size() + " previously completed documentId-s");

        int previousSize = documentsMap.size();
        documentsMap.keySet().removeAll(completedDocumentsMap.keySet());
       
        log.info("Removed previously completed documentId-s from current document list: " + previousSize + " -> " + documentsMap.size());
    }

    private class DocumensBatchProgress extends BatchProgress<Entry<String, File>> {
        {
            origin = documentsMap.entrySet();
            processName = "Documents import";
        }

        @Override
        void executeBatch() throws Exception {
            createDocumentsBatch(batchList);
        }
    }

    private void createDocuments() throws Exception {
        if (documentsMap.isEmpty()) {
            log.info("Skipping documents import, no documents found");
            return;
        }
        log.info("Starting documents import. Total = " + documentsMap.size());
        DocumensBatchProgress batchProgress = new DocumensBatchProgress();
        try {
            batchProgress.run();
        } finally {
            getDocumentListService().updateDocCounters();
        }
        log.info("Documents IMPORT COMPLETE :)");
    }

    static class ImportedDocument {
        String documentId;
        NodeRef nodeRef;
        String toimik;
        String regNumber;
        String regDateTime;
        String docName;
        String publishToAdr;
        String docNameAdr;
        String ownerId;
        String ownerName;
        String accessRestriction;
        String accessRestrictionReason;
        String accessRestrictionBeginDate;
        String accessRestrictionEndDate;
        String accessRestrictionEndDesc;
        String docType;
        String documentTypeId;
        String docStatus;
        String storageType;
        String transmittalMode;
        String fileNames;
        String docUrl;

        public ImportedDocument(String documentId, NodeRef nodeRef, String toimik, String docType, String documentTypeId, String fileNames,
                Map<QName, Serializable> props) {
            this.documentId = documentId;
            this.nodeRef = nodeRef;
            this.toimik = toimik;
            this.docType = docType;
            this.documentTypeId = documentTypeId;
            this.fileNames = fileNames;
            this.docUrl = StringEscapeUtils.escapeHtml(BeanHelper.getDocumentTemplateService().getDocumentUrl(nodeRef));
            
            if (props != null) {
                regNumber = (String) props.get(DocumentCommonModel.Props.REG_NUMBER);
                regDateTime = ((Date) props.get(DocumentCommonModel.Props.REG_DATE_TIME) != null)?staticDateTimeFormat.format((Date) props.get(DocumentCommonModel.Props.REG_DATE_TIME)):"";
                docName = (String) props.get(DocumentCommonModel.Props.DOC_NAME);
                publishToAdr = (String) props.get(DocumentDynamicModel.Props.PUBLISH_TO_ADR);
                docNameAdr = (String) props.get(DocumentDynamicModel.Props.DOC_NAME_ADR);
                ownerId = (String) props.get(DocumentCommonModel.Props.OWNER_ID);
                ownerName = (String) props.get(DocumentCommonModel.Props.OWNER_NAME);
                accessRestriction = (String) props.get(DocumentCommonModel.Props.ACCESS_RESTRICTION);
                accessRestrictionReason = (String) props.get(DocumentCommonModel.Props.ACCESS_RESTRICTION_REASON);
                accessRestrictionBeginDate = ((Date) props.get(DocumentCommonModel.Props.ACCESS_RESTRICTION_BEGIN_DATE) != null)?staticDateFormat.format((Date) props.get(DocumentCommonModel.Props.ACCESS_RESTRICTION_BEGIN_DATE)):"";
                accessRestrictionEndDate = ((Date) props.get(DocumentCommonModel.Props.ACCESS_RESTRICTION_END_DATE) != null)?staticDateFormat.format((Date) props.get(DocumentCommonModel.Props.ACCESS_RESTRICTION_END_DATE)):"";
                accessRestrictionEndDesc = (String) props.get(DocumentCommonModel.Props.ACCESS_RESTRICTION_END_DESC);
                docStatus = (String) props.get(DocumentCommonModel.Props.DOC_STATUS);
                storageType = (String) props.get(DocumentCommonModel.Props.STORAGE_TYPE);
                transmittalMode = (String) props.get(DocumentSpecificModel.Props.TRANSMITTAL_MODE);
            }
        }
        
        public ImportedDocument(String documentId, String documentTypeId) {
            this.documentId = documentId;
            this.documentTypeId = documentTypeId;            
        }
    }

    protected void createDocumentsBatch(final List<Entry<String, File>> batchList) throws Exception {
        final Map<String, ImportedDocument> batchCompletedDocumentsMap = new TreeMap<String, ImportedDocument>();
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
                postponedAssocsCommited = new TreeMap<String, List<PostponedAssoc>>(postponedAssocs);
            }

            @Override
            public void afterRollback() {
                postponedAssocs = new TreeMap<String, List<PostponedAssoc>>(postponedAssocsCommited);
                completedDocumentsMap.keySet().removeAll(batchCompletedDocumentsMap.keySet());
            }
        });
        
        for (Entry<String, File> entry : batchList) {
            String documentId = entry.getKey();
            if (log.isTraceEnabled()) {
                log.trace("Processing documentId=" + documentId);
            }
            File file = entry.getValue();
            try {
                ImportedDocument doc = createDocument(documentId, file);
                if (doc.nodeRef != null) {
                	batchCompletedDocumentsMap.put(documentId, doc);
                	completedDocumentsMap.put(documentId, doc.nodeRef); // Add immediately to completedDocumentsMap, because other code wants to access it
                	doraImporter.getStatus().incrCount();
                } else {
	                CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
	                try {
	                    writer.writeRecord(new String[] { file.getName(), doc.documentTypeId });
	                } finally {
	                    writer.close();
	                }
	                doraImporter.getStatus().incrFailed();
                }
            } catch (Throwable e) {
                CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
                try {
                    writer.writeRecord(new String[] { file.getName(), e.getMessage() });
                } finally {
                    writer.close();
                }
                doraImporter.getStatus().incrFailed();
                log.error("Error importing document " + file.getName() + ": " + e.getMessage(), e);
            }
        }
        bindCsvWriteAfterCommit(completedDocumentsFile, new CsvWriterClosure() {

            @Override
            public String[] getHeaders() {
                return new String[] {
                        "documentId",
                        "nodeRef",
                        "ppToimik",
                        "regNumber",
                        "regDateTime",
                        "docName",
                        "publishToAdr",
                        "docNameAdr",
                        "ownerId",
                        "ownerName",
                        "accessRestriction",
                        "accessRestrictionReason",
                        "accessRestrictionBeginDate",
                        "accessRestrictionEndDate",
                        "accessRestrictionEndDesc",
                        "documentTypeId",
                        "docStatus",
                        "storageType",
                        "transmittalMode",
                        "fileNames",
                        "docUrl"
                };
            }

            @Override
            public void execute(CsvWriter writer) throws IOException {
                for (Entry<String, ImportedDocument> entry : batchCompletedDocumentsMap.entrySet()) {
                    ImportedDocument doc = entry.getValue();
                    writer.writeRecord(new String[] {
                            doc.documentId.toString(),
                            doc.nodeRef == null ? "" : doc.nodeRef.toString(),
                                    doc.toimik == null ? "" : doc.toimik,
                                        doc.regNumber == null ? "" : doc.regNumber,
                                                doc.regDateTime == null ? "" : doc.regDateTime,
                                                	doc.docName == null ? "" : doc.docName,
                                                		doc.publishToAdr == null ? "" : doc.publishToAdr,
                                                			doc.docNameAdr == null ? "" : doc.docNameAdr,
                                                                doc.ownerId == null ? "" : doc.ownerId,
                                                                     doc.ownerName == null ? "" : doc.ownerName,
                                                                            doc.accessRestriction == null ? "" : doc.accessRestriction,
                                                                                        doc.accessRestrictionReason == null ? "" : doc.accessRestrictionReason,
                                                                                        		doc.accessRestrictionBeginDate == null ? "" : doc.accessRestrictionBeginDate,
                                                                                        			doc.accessRestrictionEndDate == null ? "" : doc.accessRestrictionEndDate,
                                                                                        				doc.accessRestrictionEndDesc == null ? "" : doc.accessRestrictionEndDesc,
                                                                                                            doc.documentTypeId == null ? "" : doc.documentTypeId,
                                                                                                                doc.docStatus == null ? "" : doc.docStatus,
                                                                                                                	doc.storageType == null ? "" : doc.storageType,
                                                                                                                		doc.transmittalMode == null ? "" : doc.transmittalMode,
                                                                                                                			doc.fileNames == null ? "" : doc.fileNames,
                                                                                                                				doc.docUrl == null ? "" : doc.docUrl
                    });
                }
            }
        });
    }

    private ImportedDocument createDocument(String documentId, File file) throws DocumentException, ParseException {
        return importDoc(file, mappings, documentId);
    }
    
    private String addFiles(String documentId, NodeRef importDocRef, Element root) {
    	StringBuilder fileNames = new StringBuilder();
    	List activeFilesList = root.elements(ELEMENT_NAME_DOCUMENT_FILE);
        if (activeFilesList != null) {
            for (Object file : activeFilesList) {
                String fileName = ((Element)file).attributeValue(ATTRIBUTE_NAME);
                String contentStr = ((Element)file).getText();
                
                NodeRef fileRef = addFile(fileName, contentStr, importDocRef, null, true, true);
                if (fileNames.length() > 1) {
                	fileNames.append("| ");
                }
                fileNames.append(fileName + ":" + fileRef.toString());
            }
        }
        Element inactiveFiles = root.element(ELEMENT_NAME_ANNEXES);
        if (inactiveFiles != null) {
	        List inactiveFilesList = inactiveFiles.elements(ELEMENT_NAME_ANNEX);
	        if (inactiveFilesList != null) {
	            for (Object file : inactiveFilesList) {
	                String fileName = ((Element)file).attributeValue(ATTRIBUTE_NAME);
	                String contentStr = ((Element)file).getText();
	                NodeRef fileRef = addFile(fileName, contentStr, importDocRef, null, false, true);
	                if (fileNames.length() > 1) {
	                	fileNames.append("| ");
	                }
	                fileNames.append(fileName + ":" + fileRef.toString());
	            }
	        }
        }
        
        return fileNames.toString();
    }
    
    private NodeRef addFile(String displayName, String content, NodeRef parentNodeRef, String mimeType, boolean active, boolean isBase64) {
        String fileName = FilenameUtil.makeSafeFilename(displayName);
        fileName = generalService.getUniqueFileName(parentNodeRef, fileName);
        final FileInfo fileInfo = fileFolderService.create(parentNodeRef, fileName, ContentModel.TYPE_CONTENT);
        final NodeRef fileRef = fileInfo.getNodeRef();
        
        OutputStream out = null;
        try {
        	String mimetype = BeanHelper.getMimetypeService().guessMimetype(fileName);
	        final ContentWriter writer = fileFolderService.getWriter(fileRef);
	        writer.setMimetype(mimetype);
	        writer.setEncoding("UTF-8");
	        if (isBase64) {
		        out = writer.getContentOutputStream();
		        byte[] decodedContent = Base64.decode(content);
		        out.write(decodedContent);
		        out.flush();
	        } else {
	        	writer.putContent(content);
	        }
        } catch (IOException e) {
            final String msg = "Outputstream exception while writing file: " + fileName;
            throw new RuntimeException(msg, e);
        } catch (Base64DecodingException e) {
        	final String msg = "Outputstream exception while exporting consolidated docList row to CSV-stream";
            throw new RuntimeException(msg, e);
        } finally {
        	IOUtils.closeQuietly(out);
        }
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        props.put(ContentModel.PROP_CREATOR, CREATOR_MODIFIER);
        props.put(ContentModel.PROP_MODIFIER, CREATOR_MODIFIER);
        props.put(FileModel.Props.DISPLAY_NAME, displayName);
        props.put(FileModel.Props.ACTIVE, Boolean.valueOf(active));
        nodeService.addProperties(fileRef, props);
        return fileRef;
    }

    private Map<QName, Serializable> mapProperties(Element root, TypeInfo typeInfo, Map<QName, Serializable> parentPropsMap) {
        Map<QName, Serializable> propsMap = new HashMap<>();
        
        String comment = "";
        List fields = root.elements(ELEMENT_FIELD);
        for (Object field: fields) {
    		String fieldName = ((Element)field).attributeValue(ATTRIBUTE_NAME);
    		String value = ((Element)field).getStringValue();
    		if (StringUtils.isBlank(value)) {
    			continue;
    		}
    		
    		org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldName);
    		if (pair == null) {
    			comment = StringUtils.isNotBlank(comment)?comment + " " + fieldName + ":" + value + ";" 
    					:fieldName + ":" + value + ";";
    		} else {
    		
    			DynamicPropertyDefinition propDef = pair.getFirst();
    			
		    	ee.webmedia.alfresco.dora.DoRaDocumentsMapper.PropertyValueProvider valueProvider = typeInfo.props.get(fieldName);
		    	if (valueProvider != null) {
		    		PropertyValue propValue = valueProvider.provide();
		            propValue.put(value);
		    		propsMap.put(propDef.getName(), propValue.get());
    			} else {
    				propsMap.put(propDef.getName(), value);
    			}
    		}
    	}
        
        if (StringUtils.isNotBlank(comment)) {
        	addMissingFieldToCommment(comment, (parentPropsMap != null) ? parentPropsMap : propsMap, typeInfo);
        }
        
        return propsMap;
    }

    private ImportedDocument importDoc(File xml, Map<String, Mapping> mappings, String documentId) throws DocumentException,
    ParseException {

        Element root = xmlReader.read(xml).getRootElement();
        
        String storeLocation = getElementTextByName(root, ELEMENT_NAME_DELTA_LOCATION);
        String functionMark = getElementTextByName(root, ELEMENT_NAME_FUNCTION);
        String seriesIdentifier = getElementTextByName(root, ELEMENT_NAME_SERIES);
        String volumeMark = getElementTextByName(root, ELEMENT_NAME_VOLUME);
        String docType = getElementTextByName(root, ELEMENT_NAME_DOCUMENT_TYPE);
        
        if ("Contract".equals(docType)) {
        	docType = "contract";
        }
        
        TypeInfo typeInfo = doRaDocumentsMapper.getTypeInfo(docType);
        if (typeInfo == null) {
        	new ImportedDocument(documentId, "missing document type: " + docType);
        }
        
        NodeRef functionRef = findFunction(functionMark, storeLocation);
        if (functionRef == null) {
        	// Skip document
        	return new ImportedDocument(documentId, "function NodeRef not found: " + functionMark);
        }
        NodeRef seriesRef = findSeries(functionRef, seriesIdentifier);
        if (seriesRef == null) {
        	// Skip document
        	return new ImportedDocument(documentId, "series NodeRef not found: " + seriesIdentifier);
        }
        NodeRef volumeRef = findVolume(seriesRef, volumeMark);
        if (volumeRef == null) {
        	// Skip document
        	return new ImportedDocument(documentId, "volume NodeRef not found: " + volumeMark);
        }
       
		updateSeriesDocumentTypes(volumeRef, docType);

		
		
		Element metaData = root.element(ELEMENT_NAME_META_DATA);
        
		
		Map<QName, Serializable> propsMap = setProps(metaData, volumeRef, typeInfo);

		DocumentDynamic doc = getDocumentDynamicService().createNewDocument(typeInfo.docVer, volumeRef, false)
				.getFirst();
		NodeRef documentRef = doc.getNodeRef();
		nodeService.addAspect(documentRef, DocumentCommonModel.Aspects.SEARCHABLE, null);

		Map<NodeRef, Map<QName, NodeRef>> parentRefsByVolumeRef = new HashMap<NodeRef, Map<QName, NodeRef>>();
		Map<QName, NodeRef> parentRefs = documentService.getDocumentParents(documentRef);
		parentRefsByVolumeRef.put(volumeRef, parentRefs);

		propsMap.putAll(parentRefs);

		checkProps(propsMap, null, typeInfo.propDefs);

		Pair<DynamicPropertyDefinition, Field> docNameAdrPropDefinition = typeInfo.propDefs
				.get(DocumentDynamicModel.Props.DOC_NAME_ADR.getLocalName());
		if (docNameAdrPropDefinition != null && docNameAdrPropDefinition.getSecond() != null) {
			String docNameAdrDefualt = docNameAdrPropDefinition.getSecond().getClassificatorDefaultValue();
			if (StringUtils.isNotBlank(docNameAdrDefualt)) {
				propsMap.put(DocumentDynamicModel.Props.DOC_NAME_ADR, docNameAdrDefualt);
			}
		}
		
		// add contract party
		Element parties = metaData.element(ELEMENT_NAME_PARTIES);
    	if (parties != null) {
    		setParties(parties, typeInfo, doc.getNode(), propsMap);
    	}
		

		//propsMap.put(DocumentCommonModel.Props.DOCUMENT_IS_IMPORTED, Boolean.valueOf(
		//		DocumentStatus.FINISHED.getValueName().equals(propsMap.get(DocumentCommonModel.Props.DOC_STATUS))));
    	propsMap.put(DocumentCommonModel.Props.DOCUMENT_IS_IMPORTED, Boolean.TRUE);
    	
    	addForRegisteration(propsMap, typeInfo, functionRef);
    	
		doc.getNode().getProperties().putAll(RepoUtil.toStringProperties(propsMap));
		doc.getNode().getProperties().put(
				DocumentService.TransientProps.TEMP_LOGGING_DISABLED_DOCUMENT_METADATA_CHANGED.toString(),
				Boolean.TRUE);
		getDocumentDynamicService().updateDocument(doc, Arrays.asList("doraImporter"), false, true);
		
		addAssociations(documentRef, documentId, root);

		LogEntry logEntry = new LogEntry();
		logEntry.setComputerIp("127.0.0.1");
		logEntry.setComputerName("localhost");
		logEntry.setLevel(LogObject.DOCUMENT.getLevel());
		logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
		logEntry.setCreatorId("IMPORT");
		logEntry.setCreatorName("IMPORT");
		logEntry.setEventDescription("Dokumendi importimine");
		logEntry.setObjectId(documentRef.toString());
		BeanHelper.getLogService().addLogEntry(logEntry);

		String fileNames = addFiles(documentId, documentRef, root);
		BeanHelper.getFileService().reorderFiles(documentRef);

		return new ImportedDocument(documentId, documentRef, functionMark + "/" + seriesIdentifier + "/" + volumeMark, docType,
				typeInfo.docVer.getParent().getId(), fileNames, nodeService.getProperties(documentRef));
		
    }
    
    private void addForRegisteration(Map<QName, Serializable> propsMap, TypeInfo docType, NodeRef functionRef) {
    	String status = (String) propsMap.get(DocumentCommonModel.Props.DOC_STATUS);
        if (doraImporter.getData().isForRegisteration()
        		&& canRegister(docType, functionRef)
        		&& (status == null || DocumentStatus.WORKING.getValueName().equals(status))
        		&& propsMap.get(DocumentCommonModel.Props.REG_DATE_TIME) == null
        		&& StringUtils.isBlank((String) propsMap.get(DocumentCommonModel.Props.REG_NUMBER))) {
        	propsMap.put(DocumentCommonModel.Props.SEARCHABLE_HAS_ALL_FINISHED_COMPOUND_WORKFLOWS, true);
        	propsMap.put(DocumentCommonModel.Props.DOC_STATUS, DocumentStatus.WORKING.getValueName());
        }
        
        
		
	}
    
    private boolean canRegister(TypeInfo docType, NodeRef functionRef) {
    	return getDocumentAdminService().getTypeProperty(docType.docVer.getNodeRef(), DocumentAdminModel.Props.REGISTRATION_ON_DOC_FORM_ENABLED, Boolean.class) 
    			&& getDocumentAdminService().getTypeProperty(docType.docVer.getNodeRef(), DocumentAdminModel.Props.REGISTRATION_ENABLED, Boolean.class)
    			&& functionRef != null 
    			&& !BeanHelper.getFunctionsService().getUnmodifiableFunction(functionRef, null).isDocumentActivitiesAreLimited();
    }

	private void setParties(Element root, TypeInfo typeInfo, Node node, Map<QName, Serializable> parentPropsMap) {	
    	List <Map<QName, Serializable>> contractPartiesProps = new ArrayList<Map<QName, Serializable>>();
    	List parties = root.elements(ELEMENT_NAME_PARTY);
    	for (Object party: parties) {
    		Map<QName, Serializable> contractPartyPropsMap = mapProperties((Element)party, typeInfo, parentPropsMap);
    		if (isNotEmptyProps(contractPartyPropsMap)) {
    			contractPartiesProps.add(contractPartyPropsMap);
    		}
    	}
    	
    	QName[] hierarchy = (QName[]) ArrayUtils.add(new QName[] {}, DocumentChildModel.Assocs.CONTRACT_PARTY);
    	List<Node> childNodes = node.getAllChildAssociations(DocumentChildModel.Assocs.CONTRACT_PARTY);
    	int childNodesSize = (childNodes != null)?childNodes.size():0;
    	for (int i = 0; i < contractPartiesProps.size() - childNodesSize; i++) {
    		getDocumentDynamicService().createChildNodesHierarchyAndSetDefaultPropertyValues(node, hierarchy, typeInfo.docVer);
    	}
    	if (contractPartiesProps.size() > 0) {
    		childNodes = node.getAllChildAssociations(DocumentChildModel.Assocs.CONTRACT_PARTY);
        	if (contractPartiesProps.size() != childNodes.size()) {
        		throw new RuntimeException("contractPartiesProps.size:" + contractPartiesProps.size() + " != document childNodes.size:" + childNodes.size());
        	}
        	for (int i = 0; i < contractPartiesProps.size(); i++) {
        		Map<QName, Serializable> contractPartyPropsMap = contractPartiesProps.get(i);
        		Node childNode = childNodes.get(i);
        		checkProps(contractPartyPropsMap, hierarchy, typeInfo.propDefs);
        		childNode.getProperties().putAll(RepoUtil.toStringProperties(contractPartyPropsMap));
        	}
    	}
    }
    
    private void updateSeriesDocumentTypes(NodeRef volumeRef, String docTypeName) {
    	NodeRef seriesRef = (NodeRef)getNodeService().getProperty(volumeRef, DocumentCommonModel.Props.SERIES);
    	List<String> docTypes = (List<String>) getNodeService().getProperty(seriesRef, SeriesModel.Props.DOC_TYPE);
    	if (!docTypes.contains(docTypeName)) {
    		docTypes.add(docTypeName);
    		getNodeService().setProperty(seriesRef, SeriesModel.Props.DOC_TYPE, (Serializable)docTypes);
    	}
    }

    private void checkProps(Map<QName, Serializable> propsMap, QName[] hierarchy, Map<String, org.alfresco.util.Pair<DynamicPropertyDefinition, Field>> propDefs) {
        List<String> errors = new ArrayList<String>();
        for (Entry<QName, Serializable> entry : propsMap.entrySet()) {
            String propNameString = entry.getKey().toPrefixString(getNamespaceService());
            if (!DocumentDynamicModel.URI.equals(entry.getKey().getNamespaceURI())) {
                errors.add("Property " + propNameString + " has wrong namespace");
                continue;
            }
            org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = propDefs.get(entry.getKey().getLocalName());
            if (pair == null) {
                errors.add("Property " + propNameString + " is missing propertyDefinition");
                continue;
            }

            if (hierarchy == null) {
                hierarchy = new QName[] {};
            }
            DynamicPropertyDefinition propDef = pair.getFirst();
            QName[] propHierarchy = propDef.getChildAssocTypeQNameHierarchy();
            if (propHierarchy == null) {
                propHierarchy = new QName[] {};
            }
            if (!Arrays.equals(hierarchy, propHierarchy)) {
                errors.add("Property " + propNameString + " should be on hierarchy " + Arrays.asList(propHierarchy) + " but is on hierarchy " + Arrays.asList(hierarchy));
                continue;
            }

            // PostipoissDocumentsMapper doesn't know about single- and multi-valued properties, so handle it here
            Serializable value = entry.getValue();
            if (propDef.isMultiValued() && value != null && !(value instanceof List)) {
                ArrayList<Serializable> list = new ArrayList<Serializable>();
                list.add(value);
                value = list;
                entry.setValue(value);
            }

            if (value == null) {
                continue;
            }
            String requiredClassName = propDef.getDataType().getJavaClassName();
            if (value instanceof List<?>) {
                if (!("additionalRecipientName".equals(propDef.getName().getLocalName()) || "partyName".equals(propDef.getName().getLocalName()) || "partyContactPerson".equals(propDef.getName().getLocalName())) && !propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should not be a List, but is a List");
                    continue;
                }
                List<Serializable> list = (List<Serializable>) value;
                int i = 0;
                for (Serializable listElement : list) {
                    if (listElement == null) {
                        i++;
                        continue;
                    }
                    String realClassName = listElement.getClass().getName();
                    if (!realClassName.equals(requiredClassName)) {
                        errors.add("Property " + propNameString + " value at index " + i + " should be of type " + requiredClassName + " but is of type " + realClassName + " : "
                                + listElement);
                        continue;
                    }
                    i++;
                }
            } else {
                if (propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should be a List, but is not a List");
                    continue;
                }
                String realClassName = value.getClass().getName();
                if (!realClassName.equals(requiredClassName)) {
                    errors.add("Property " + propNameString + " value should be of type " + requiredClassName + " but is of type " + realClassName + " : " + value);
                    continue;
                }
            }
        }
        if (!errors.isEmpty()) {
            log.info("props=" + WmNode.toString(propsMap, getNamespaceService()));
            throw new RuntimeException("Found " + errors.size() + " errors:\n  * " + StringUtils.join(errors, "\n  * "));
        }
    }

    
    
    private boolean isNotEmptyProps(Map<QName, Serializable> propsMap) {
    	String partyNames = (String)propsMap.get(DocumentSpecificModel.Props.PARTY_NAME);
    	String partyContactPersons = (String)propsMap.get(DocumentSpecificModel.Props.PARTY_CONTACT_PERSON);
    	if (StringUtils.isNotBlank(partyNames) || StringUtils.isNotBlank(partyContactPersons)) {
    		return true;
    	}
    	return false;
    }

    private Map<QName, Serializable> setProps(Element root, NodeRef volumeRef, TypeInfo typeInfo) {
        Map<QName, Serializable> propsMap = mapProperties(root, typeInfo, null);

        String regNumber = (String)propsMap.get(DocumentCommonModel.Props.REG_NUMBER);
        if (StringUtils.isNotBlank(regNumber)) {
        	if (propsMap.get(DocumentCommonModel.Props.REG_DATE_TIME) == null) {
            	throw new RuntimeException("regDateTime must not be null");
            }
        	String shortRegNumber = null;
        	if (StringUtils.isNotBlank(regNumber)) {
        		if (regNumber.contains("/")) {
        			shortRegNumber = StringUtils.substringAfterLast(regNumber, "/");
        		} else if (regNumber.contains("-")) {
            		shortRegNumber = StringUtils.substringBeforeLast(shortRegNumber, "-");
            	} else {
            		shortRegNumber = regNumber;
            	}
        		
        	}
            propsMap.put(DocumentCommonModel.Props.SHORT_REG_NUMBER, shortRegNumber);
            
        }
        
        if (propsMap.get(DocumentCommonModel.Props.REG_DATE_TIME) != null 
        		&& StringUtils.isBlank((String) propsMap.get(DocumentCommonModel.Props.REG_NUMBER))) {
        	throw new RuntimeException("regNumber must not be empty");
        }
        
        /*String status = (String) propsMap.get(DocumentCommonModel.Props.DOC_STATUS);
        if (status != null && !DocumentStatus.WORKING.getValueName().equals(status)
        		&& propsMap.get(DocumentCommonModel.Props.REG_DATE_TIME) == null
        		&& StringUtils.isBlank((String) propsMap.get(DocumentCommonModel.Props.REG_NUMBER))) {
        	throw new RuntimeException("regNumber and regDateTime must not be empty");
        }*/

        setOwnerProperties(propsMap, typeInfo);
        
        Element recipients = root.element(ELEMENT_NAME_RECIPIENTS);
    	if (recipients != null) {
    		setRecipients(recipients, propsMap, typeInfo);
    	}
    	Element additionalRecipients = root.element(ELEMENT_NAME_ADDITIONAL_RECIPIENTS);
    	if (additionalRecipients != null) {
    		setAdditionalRecipients(additionalRecipients, propsMap, typeInfo);
    	}

    	if (StringUtils.isBlank((String) propsMap.get(DocumentCommonModel.Props.DOC_NAME))) {
        	throw new RuntimeException("docName cannot be blank");
        }

        return propsMap;
    }
    
    
    private void setRecipients(Element root, Map<QName, Serializable> propsMap, TypeInfo typeInfo) {
    	List recipients = root.elements(ELEMENT_NAME_RECIPIENT);
    	for (Object recipient: recipients) {
    		handleLists((Element)recipient, propsMap, typeInfo);
    	}
    }
    
    private void setAdditionalRecipients(Element root, Map<QName, Serializable> propsMap, TypeInfo typeInfo) {
    	List additionalRecipients = root.elements(ELEMENT_NAME_ADDITIONAL_RECIPIENT);
    	for (Object additionalRecipient: additionalRecipients) {
    		handleLists((Element)additionalRecipient, propsMap, typeInfo);
    	}
    }
    
    private void handleLists(Element root, Map<QName, Serializable> propsMap, TypeInfo typeInfo) {
    	List fields = root.elements(ELEMENT_FIELD);
    	for (Object field: fields) {
    		String fieldName = ((Element)field).attributeValue(ATTRIBUTE_NAME);
    		String value = ((Element)field).getStringValue();
    		if (StringUtils.isBlank(value)) {
    			value = null;
    		}
    		
    		org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldName);
    		if (pair == null && value != null) {
    			addMissingFieldToCommment(fieldName + ":" + value, propsMap, typeInfo);
    		} else if (pair != null) {
	    		DynamicPropertyDefinition propDef = pair.getFirst();
	    		
	    		List<String> values = (List<String>) propsMap.get(propDef.getName());
	    		if (values == null) {
	    			values = new ArrayList<>();
	    		}
	    		values.add(value);
	    		propsMap.put(propDef.getName(), (Serializable)values);
    		}
    	}
    }
    
    private void addMissingFieldToCommment(String missingFieldNameValue, Map<QName, Serializable> propsMap, TypeInfo typeInfo) {
    	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pairComment = typeInfo.propDefs.get(DocumentCommonModel.Props.COMMENT.getLocalName());
		if (pairComment != null) {
			String propComment =  (String) propsMap.get(DocumentCommonModel.Props.COMMENT);
			propComment = (StringUtils.isNotBlank(propComment)) ? propComment + " " + missingFieldNameValue + ";" : missingFieldNameValue + ";"; 
			propsMap.put(DocumentCommonModel.Props.COMMENT, propComment);
		}
    }

    private NodeRef findCompletedDoc(String id) {
        NodeRef res = completedDocumentsMap.get(id);
        return res;
    }
    
    
    
    
    
    private NodeRef findVolume(NodeRef seriesRef, String volumeMark) {

        for (ChildAssociationRef assoc : nodeService.getChildAssocs(seriesRef, VolumeModel.Associations.VOLUME, VolumeModel.Associations.VOLUME)) {
            if (volumeMark.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.MARK))) {
                return assoc.getChildRef();
            }
        }
        
        for (ChildAssociationRef assoc : nodeService.getChildAssocs(seriesRef, CaseFileModel.Assocs.CASE_FILE, CaseFileModel.Assocs.CASE_FILE)) {
            if (volumeMark.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.MARK))) {
                return assoc.getChildRef();
            }
        }
        
    	return null;
    }
    
    
    private NodeRef findSeries(NodeRef functionRef, String seriesIdentifier) {
        for (ChildAssociationRef assoc : nodeService.getChildAssocs(functionRef, SeriesModel.Associations.SERIES, SeriesModel.Associations.SERIES)) {
            if (seriesIdentifier.equals(nodeService.getProperty(assoc.getChildRef(), SeriesModel.Props.SERIES_IDENTIFIER))) {
                return assoc.getChildRef();
            }
        }
        
    	return null;
    }
    
    
    private NodeRef findFunction(String mark, String storeLocation) {
    	log.info("delta store: " + storeLocation);
    	
        NodeRef docsList = (STORE_ACTIVE.equals(storeLocation))?
        		generalService.getNodeRef(FunctionsModel.Repo.FUNCTIONS_SPACE, generalService.getStore())
        		:generalService.getNodeRef(FunctionsModel.Repo.FUNCTIONS_SPACE, generalService.getArchivalsStoreRef());

        for (ChildAssociationRef assoc : nodeService.getChildAssocs(docsList, FunctionsModel.Associations.FUNCTION, FunctionsModel.Associations.FUNCTION)) {
            if (mark.equals(nodeService.getProperty(assoc.getChildRef(), FunctionsModel.Props.MARK))) {
            	return assoc.getChildRef();
            }
        }

        return null;
    }
    
    // [ASSOCS
    
    private AssocType getAssocTypeFromString(String assocTypeString) {
        if (AssocType.DEFAULT.getValueName().equals(assocTypeString)) {
            return AssocType.DEFAULT;
        } else if (AssocType.FOLLOWUP.getValueName().equals(assocTypeString)) {
            return AssocType.FOLLOWUP;
        } else if (AssocType.REPLY.getValueName().equals(assocTypeString)) {
            return AssocType.REPLY;
        } else {
            throw new RuntimeException("Unknown assocType value: " + assocTypeString);
        }
    }
    protected void loadPostponedAssocs() throws Exception {
        postponedAssocsFile = new File(workFolder, "postponed_assocs.csv");

        postponedAssocs = new TreeMap<String, List<PostponedAssoc>>();

        if (!postponedAssocsFile.exists()) {
            log.info("Skipping loading postponed assocs, file does not exist: " + postponedAssocsFile);
            return;
        }

        log.info("Loading postponed assocs from file " + postponedAssocsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(postponedAssocsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String sourceDocId = reader.get(0);
                String targetDocId = reader.get(1);
                String assocTypeString = reader.get(2);
                if (StringUtils.isNotBlank(assocTypeString)) {
                	assocTypeString = assocTypeString.toLowerCase();
                }
                AssocType assocType = getAssocTypeFromString(assocTypeString);
                
                putPostponedAssoc(sourceDocId, targetDocId, assocType);
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + postponedAssocs.size() + " postponed assocs.");
        postponedAssocsCommited = new TreeMap<String, List<PostponedAssoc>>(postponedAssocs);
    }

    static class PostponedAssoc {
        String sourceDocId;
        String targetDocId;
        AssocType assocType;

        public PostponedAssoc(String sourceDocId, String targetDocId, AssocType assocType) {
            this.sourceDocId = sourceDocId;
            this.targetDocId = targetDocId;
            this.assocType = assocType;
        }
    }

    private void writePostponedAssocs() {
        try {
            // Write created documents
            if (postponedAssocsFile == null) {
                return;
            }
            boolean exists = postponedAssocsFile.exists();
            if (exists) {
                postponedAssocsFile.delete();
            }
            CsvWriter writer = new CsvWriter(new FileWriter(postponedAssocsFile, false), CSV_SEPARATOR);
            try {
                writer.writeRecord(new String[] {
                        "sourceDocumentId",
                        "targetDocumentId",
                        "assocType"
                });
                for (List<PostponedAssoc> list : postponedAssocsCommited.values()) {
                    for (PostponedAssoc postponedAssoc : list) {
                        writer.writeRecord(new String[] {
                                postponedAssoc.sourceDocId.toString(),
                                postponedAssoc.targetDocId.toString(),
                                postponedAssoc.assocType.getValueName()
                        });
                    }
                }
            } finally {
                writer.close();
            }
        } catch (IOException e) {
            throw new RuntimeException("Error writing file '" + postponedAssocsFile + "': " + e.getMessage(), e);
        }
    }

    protected void addAssociations(NodeRef documentRef, String documentId, Element root) {
        
        Element relations = root.element(ELEMENT_NAME_RELATIONS);
        if (relations != null) {
        	List relationList = relations.elements(ELEMENT_NAME_RELATION);
        	for (Object relation: relationList) {
        		String targetDocumentId = getElementTextByName((Element) relation, ELEMENT_NAME_ID);
        		String assocTypeName = getElementTextByName((Element) relation, ELEMENT_NAME_TYPE);
        		if (StringUtils.isNotBlank(assocTypeName)) {
        			assocTypeName = assocTypeName.toLowerCase();
                }
        		
        		if (StringUtils.isBlank(targetDocumentId) || StringUtils.isBlank(assocTypeName) || AssocType.INITIAL.getValueName().equals(assocTypeName)) {
                    continue;
                }
        		
        		AssocType assocType = getAssocTypeFromString(assocTypeName);
        		
                NodeRef targetDocumentRef = findCompletedDoc(targetDocumentId);
                if (targetDocumentRef == null) {
                    if (documentsMap.get(targetDocumentId) != null || completedDocumentsMap.get(targetDocumentId) != null) {
                        log.warn("Association from documentId=" + documentId + " to non-existent documentId=" + targetDocumentId + " assocType=" + assocType);
                    }
                    putPostponedAssoc(documentId, targetDocumentId, assocType);
                } else {
                    log.debug("Creating assoc " + documentId + " [" + documentRef + "] -> " + targetDocumentId + " [" + targetDocumentRef + "], type=" + assocType);
                    createAssoc(documentRef, targetDocumentRef, documentId, targetDocumentId, assocType);
                }
        	}
        }

        
        List<PostponedAssoc> list = postponedAssocs.get(documentId);
        if (list != null) {
            for (PostponedAssoc postponedAssoc : list) {
                NodeRef sourceDocRef = findCompletedDoc(postponedAssoc.sourceDocId);
                log.debug("Creating assoc " + postponedAssoc.sourceDocId + " [" + sourceDocRef + "] -> " + documentId + " [" + documentRef + "], type=" + postponedAssoc.assocType);
                createAssoc(sourceDocRef, documentRef, postponedAssoc.sourceDocId, documentId, postponedAssoc.assocType);
            }
            postponedAssocs.remove(documentId);
        }
    }

    private void createAssoc(NodeRef sourceDocRef, NodeRef targetDocRef, String sourceDocId, String targetDocId, AssocType assocType) {
        QName assocTypeQName;
        switch (assocType) {
	        case REPLY:
	            assocTypeQName = DocumentCommonModel.Assocs.DOCUMENT_REPLY;
	            break;
	        case FOLLOWUP:
	            assocTypeQName = DocumentCommonModel.Assocs.DOCUMENT_FOLLOW_UP;
	            break;
	        case DEFAULT:
	            assocTypeQName = DocumentCommonModel.Assocs.DOCUMENT_2_DOCUMENT;
	            break;
	        default:
	            throw new RuntimeException("Unsupported assocType: " + assocType);
	    }
        if (sourceDocRef == null || !nodeService.exists(sourceDocRef)) {
            log.error("Skipping creating assoc, source does not exist, sourceDocumentId=" + sourceDocId + " targetDocumentId="
                    + targetDocId + " assocType=tavaline sourceDocRef=" + sourceDocRef + " targetDocRef=" + targetDocRef);
            return;
        }
        if (targetDocRef == null || !nodeService.exists(targetDocRef)) {
            log.error("Skipping creating assoc, target does not exist, sourceDocumentId=" + sourceDocId + " targetDocumentId="
                    + targetDocId + " assocType=tavaline sourceDocRef=" + sourceDocRef + " targetDocRef=" + targetDocRef);
            return;
        }
        boolean skip = false;

        // Check that reverse-direction associations are not previously defined, otherwise it is a business rule failure
        List<AssociationRef> targetAssocs = nodeService.getSourceAssocs(sourceDocRef, assocTypeQName);
        for (AssociationRef assocRef : targetAssocs) {
            Assert.isTrue(assocRef.getTargetRef().equals(sourceDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
            // if (sourceDocRef.equals(assocRef.getSourceRef())
            log.debug("Existing target-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
                    log.debug("Skipping this assoc creation");
                    skip = true;
            }
            
        }
        if (!skip) {
	        List<AssociationRef> sourceAssocs = nodeService.getTargetAssocs(targetDocRef, assocTypeQName);
	        for (AssociationRef assocRef : sourceAssocs) {
	            Assert.isTrue(assocRef.getSourceRef().equals(targetDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
	            log.debug("Existing source-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
	            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
	                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
	                    log.debug("Skipping this assoc creation");
	                    skip = true;
	            }
	        }
	        if (!skip && !sourceAssocs.isEmpty() 
	        		&& (AssocType.REPLY == assocType || AssocType.FOLLOWUP == assocType)) {
	        	log.error("Skipping creating assoc, target already exist for another source (" + sourceAssocs.get(0).getSourceRef() + "), sourceDocumentId=" + sourceDocId + " targetDocumentId="
	                    + targetDocId + " assocType=tavaline sourceDocRef=" + sourceDocRef + " targetDocRef=" + targetDocRef);
	        	skip = true;
	        }
        }
        
        if (!skip) {
	        // Check that same-direction associations are not previously defined, otherwise nodeService.createAssociation throws AssociationExistsException
	        targetAssocs = nodeService.getSourceAssocs(targetDocRef, assocTypeQName);
	        for (AssociationRef assocRef : targetAssocs) {
	            Assert.isTrue(assocRef.getTargetRef().equals(targetDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
	            log.debug("Existing target-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
	            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
	                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
	                    log.debug("Skipping this assoc creation");
	                    skip = true;
	            }
	        }
        }
        if (!skip) {
        	List<AssociationRef> sourceAssocs = nodeService.getTargetAssocs(sourceDocRef, assocTypeQName);
	        for (AssociationRef assocRef : sourceAssocs) {
	            Assert.isTrue(assocRef.getSourceRef().equals(sourceDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
	            log.debug("Existing source-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
	            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
	                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
	                    log.debug("Skipping this assoc creation");
	                    skip = true;
	            }
	        }
        }

        if (!skip) {
            nodeService.createAssociation(sourceDocRef, targetDocRef, assocTypeQName);
        }
    }

    private void putPostponedAssoc(String sourceDocId, String targetDocId, AssocType assocType) {
        List<PostponedAssoc> list = postponedAssocs.get(targetDocId);
        if (list == null) {
            list = new ArrayList<PostponedAssoc>();
            postponedAssocs.put(targetDocId, list);
        }
        list.add(new PostponedAssoc(sourceDocId, targetDocId, assocType));
    }

    // ASSOCS]
    
    

    // HELPER METHODS
    /** RetryingTransactionHelper that only tries to do things once. */
    private RetryingTransactionHelper getTransactionHelper() {
        RetryingTransactionHelper helper = new RetryingTransactionHelper();
        helper.setMaxRetries(1);
        helper.setTransactionService(transactionService);
        return helper;
    }
    
    
    private String getElementTextByName(Element root, String name) {
    	Element element = root.element(name);
    	if (element != null) {
    		return element.getStringValue();
    	}
    	return null;
    }
    
    
    
    private class DoraVolume {
    	private NodeRef nodeRef;
		private Date validFrom;
		private Date validTo;
    	private String mark;
    	private String seriesIdentifier;
    	private String seriesTitle;
		private String function;
    	
    	public NodeRef getNodeRef() {
			return nodeRef;
		}
		public void setNodeRef(NodeRef nodeRef) {
			this.nodeRef = nodeRef;
		}
		public Date getValidFrom() {
			return validFrom;
		}
		public void setValidFrom(Date validFrom) {
			this.validFrom = validFrom;
		}
		public Date getValidTo() {
			return validTo;
		}
		public void setValidTo(Date validTo) {
			this.validTo = validTo;
		}
		public String getMark() {
			return mark;
		}
		public void setMark(String mark) {
			this.mark = mark;
		}
		public String getSeriesIdentifier() {
			return seriesIdentifier;
		}
		public void setSeriesIdentifier(String seriesIdentifier) {
			this.seriesIdentifier = seriesIdentifier;
		}
		public String getSeriesTitle() {
			return seriesTitle;
		}
		public void setSeriesTitle(String seriesTitle) {
			this.seriesTitle = seriesTitle;
		}
		public String getFunction() {
			return function;
		}
		public void setFunction(String function) {
			this.function = function;
		}    	
    	
    	
    }

}
