package ee.webmedia.alfresco.log.service;

import java.io.Serializable;

import org.alfresco.web.bean.repository.Node;

import ee.webmedia.alfresco.parameters.model.Parameter;

public interface AdminLogService {

    String BEAN_NAME = "AdminLogService";
    
    void addUserGroupLog(String event, String groupDisplayName);
        
    void addClassificatorLog(Node node, String event, String classificatorName);
    
    void addClassificatorLog(Node node, String event, String classificatorName, String value);
    
    void addClassificatorLog(Node node, String event, String classificatorName, String changedPropName, String oldValue, String newValue);
    
    void addClassificatorLog(Node node, String event, String classificatorName, String classificatorValueName,
    		String changedPropName, String oldValue, String newValue);
    
    void addClassificatorChangeLogIfNeeded(Node classificatorNode, String classificatorName, String valueName,
    		String origValueName, String originalValue, String modValue);

	void addClassificatorChangeLogIfNeeded(Node classificatorNode, String classificatorName, String valueName,
			String origValueName, boolean originalValue, boolean modValue);
    
    void addParameterLog(Parameter<? extends Serializable> parameter);
    
    void addAppLog(String event, String levelName);
    
    void addContactLog(Node node, String event);

    void addContactGroupLog(Node node, String event);
        
    void addContactGroupUserLog(Node node, String event, String userName);
    
    void addTemplateLog(Node node, String event);
    
    void addTemplateChangedLog(Node node, String propNameKey, String oldValue, String newValue);
}
