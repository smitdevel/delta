package ee.webmedia.alfresco.log.service;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentDynamicService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.alfresco.web.bean.repository.Node;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.EqualsHelper;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.util.Assert;

import ee.webmedia.alfresco.addressbook.model.AddressbookModel;
import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.cases.model.CaseModel;
import ee.webmedia.alfresco.classificator.model.ClassificatorValue;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.log.service.LogService;
import ee.webmedia.alfresco.parameters.model.Parameter;
import ee.webmedia.alfresco.user.service.UserService;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.utils.RepoUtil;
import ee.webmedia.alfresco.volume.model.VolumeModel;

public class AdminLogServiceImpl implements AdminLogService {
	
	private final String MSG_KEY_PREFIX = "admin_log";
	
    private UserService userService;
    private LogService logService;
    
    public void addUserGroupLog(String event, String groupDisplayName) {
    	addLog(LogObject.USER_GROUP, event, groupDisplayName);
    }
    
    public void addClassificatorChangeLogIfNeeded(Node classificatorNode, String classificatorName, String valueName, String origValueName, String originalValue, String modValue) {
        if (!EqualsHelper.nullSafeEquals(originalValue, modValue)) {
        	addClassificatorLog(classificatorNode, "classificator_value_changed", classificatorName, origValueName, MessageUtil.getMessage(valueName), originalValue, modValue);
        }
    }
    
    public void addClassificatorChangeLogIfNeeded(Node classificatorNode, String classificatorName, String valueName, String origValueName, boolean originalValue, boolean modValue) {
        if (originalValue != modValue) {
        	addClassificatorLog(classificatorNode, "classificator_value_changed", classificatorName, origValueName, MessageUtil.getMessage(valueName), String.valueOf(originalValue), String.valueOf(modValue));
        }
    }

    public void addClassificatorLog(Node node, String event, String classificatorName) {
    	addLog(node, LogObject.CLASSIFICATOR, event, classificatorName);
    }

    public void addClassificatorLog(Node node, String event, String classificatorName, String value) {
    	addLog(node, LogObject.CLASSIFICATOR, event, classificatorName, value);
    }

    public void addClassificatorLog(Node node, String event, String classificatorName, String changedPropName, String oldValue, String newValue) {
    	addLog(node, LogObject.CLASSIFICATOR, event, classificatorName, changedPropName, oldValue, newValue);
    }

    public void addClassificatorLog(Node node, String event, String classificatorName, String classificatorValueName, String changedPropName, String oldValue, String newValue) {
    	addLog(node, LogObject.CLASSIFICATOR, event, classificatorName, classificatorValueName, changedPropName, oldValue, newValue);
    }

    public void addParameterLog(Parameter<? extends Serializable> parameter) {
    	String oldValue = parameter.getPreviousParamValue() != null ? parameter.getPreviousParamValue().toString() : null;
    	String newValue = parameter.getParamValue() != null ? parameter.getParamValue().toString() : null;
    	if (oldValue == null || newValue == null) {
    		return;
    	}
    	addLog(LogObject.PARAMETER, "parameter_changed", parameter.getParamName(), oldValue, newValue);
    }

    public void addAppLog(String event, String levelName) {
    	addLog(LogObject.APP_LOG, event, levelName);
    }

    public void addContactLog(Node node, String event) {
    	String contactName = BeanHelper.getAddressbookService().getContactName(node);
    	addLog(node, LogObject.CONTACT, event, contactName);
    }

    public void addContactGroupLog(Node node, String event) {
    	String groupName = getGroupName(node);
    	addLog(node, LogObject.CONTACT_GROUP, event, groupName);
    }

    public void addContactGroupUserLog(Node node, String event, String contactName) {
    	String groupName = getGroupName(node);
    	addLog(node, LogObject.CONTACT_GROUP, event, groupName, contactName);
    }

    public void addTemplateLog(Node node, String event) {
        String templateName = getTemplateName(node);
    	addLog(node, LogObject.TEMPLATE, event, templateName);
    }

    public void addTemplateChangedLog(Node node, String propNameKey, String oldValue, String newValue) {
    	String propName = MessageUtil.getMessage(String.format("template_%s", propNameKey.split("}")[1]));
    	addLog(node, LogObject.TEMPLATE, "template_changed", node.getName(), propName, oldValue, newValue);
    }

    private void addLog(Node node, LogObject logObject, String event, String ... logEntryParams) {
    	String msgKey = String.format("%s_%s", MSG_KEY_PREFIX, event);
    	logService.addLogEntry(LogEntry.create(logObject, userService, node.getNodeRef(), msgKey, logEntryParams));
    }

    private void addLog(LogObject logObject, String event, String ... logEntryParams) {
    	String msgKey = String.format("%s_%s", MSG_KEY_PREFIX, event);
    	logService.addLogEntry(LogEntry.create(logObject, userService, msgKey, logEntryParams));
    }

    private String getTemplateName(Node node) {
    	return String.format("%s%s", node.getProperties().get("{temp}fileNameBase"), node.getProperties().get("{temp}fileNameExtension"));
    }

    private String getGroupName(Node node) {
    	return (String) node.getProperties().get(AddressbookModel.Props.GROUP_NAME.toString());
    }

    // START: getters / setters

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

	public void setLogService(LogService logService) {
		this.logService = logService;
	}

    // END: getters / setters
}
