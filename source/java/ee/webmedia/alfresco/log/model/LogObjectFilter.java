package ee.webmedia.alfresco.log.model;

import ee.webmedia.alfresco.common.propertysheet.classificatorselector.EnumSelectorItemFilter;

public class LogObjectFilter implements EnumSelectorItemFilter<LogObject> {

    @Override
    public boolean showItem(LogObject enumItem) {
        return !LogObject.WORKFLOW.equals(enumItem) && !LogObject.LOG_IN_OUT.equals(enumItem) 
        		&& !LogObject.RIGHTS_DOCUMENT.equals(enumItem) && !LogObject.RIGHTS_SERIES.equals(enumItem) && !LogObject.RIGHTS_VOLUME.equals(enumItem);
    }

}
