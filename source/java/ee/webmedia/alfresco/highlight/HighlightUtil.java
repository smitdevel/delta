package ee.webmedia.alfresco.highlight;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.parameters.model.Parameters;

public class HighlightUtil {

	public static String getHighlightColor() {
		return String.format("%s", BeanHelper.getParametersService().getStringParameter(Parameters.LAST_VIEWED_ROW_COLOR).trim());
	}
	
}
