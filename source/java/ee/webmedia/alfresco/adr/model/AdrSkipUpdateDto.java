package ee.webmedia.alfresco.adr.model;

import java.io.Serializable;

public class AdrSkipUpdateDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String adrRef;
	private String archivedRef;
	private String restoredRef;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAdrRef() {
		return adrRef;
	}
	public void setAdrRef(String adrRef) {
		this.adrRef = adrRef;
	}
	public String getArchivedRef() {
		return archivedRef;
	}
	public void setArchivedRef(String archivedRef) {
		this.archivedRef = archivedRef;
	}
	public String getRestoredRef() {
		return restoredRef;
	}
	public void setRestoredRef(String restoredRef) {
		this.restoredRef = restoredRef;
	}
	
	

}
