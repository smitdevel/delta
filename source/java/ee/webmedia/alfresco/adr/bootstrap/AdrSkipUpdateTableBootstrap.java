package ee.webmedia.alfresco.adr.bootstrap;

import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Statement;

import ee.webmedia.alfresco.common.bootstrap.CreateTableBootstrap;

public class AdrSkipUpdateTableBootstrap extends CreateTableBootstrap {

    @Override
    protected InputStream getSqlStatementsInputStream() {
        return getClass().getResourceAsStream("adr_skip_update_table.sql");
    }

    @Override
    protected String getTableToCheck() {
        return "adr_skip_update";
    }

    @Override
    protected void dropTables(Statement stmt) throws SQLException {
        stmt.executeUpdate("DROP TABLE adr_skip_update");
    }

    @Override
    protected String getTablesLogName() {
        return "ADR skip update";
    }

}
