CREATE TABLE adr_skip_update (
  asu_id  BIGSERIAL PRIMARY KEY, -- auto-incremented
  adr_node_ref text NOT NULL,
  archived_node_ref text,
  restored_node_ref text,
  UNIQUE(adr_node_ref, archived_node_ref, restored_node_ref) DEFERRABLE INITIALLY DEFERRED
);
DROP INDEX IF EXISTS adr_skip_update_archived_node_ref;
CREATE INDEX adr_skip_update_archived_node_ref ON adr_skip_update (archived_node_ref);
DROP INDEX IF EXISTS adr_skip_update_restored_node_ref;
CREATE INDEX adr_skip_update_restored_node_ref ON adr_skip_update (restored_node_ref);
