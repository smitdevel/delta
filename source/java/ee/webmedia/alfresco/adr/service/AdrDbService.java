package ee.webmedia.alfresco.adr.service;

import org.alfresco.service.cmr.repository.NodeRef;

import ee.webmedia.alfresco.adr.model.AdrSkipUpdateDto;

public interface AdrDbService {

    String BEAN_NAME = "AdrDbService";

    AdrSkipUpdateDto getByAdrRef(NodeRef nodeRef);
    AdrSkipUpdateDto getByArchivedRef(NodeRef nodeRef);
    AdrSkipUpdateDto getByRestoredRef(NodeRef nodeRef);
    int update(AdrSkipUpdateDto dto);
    int insert(AdrSkipUpdateDto dto);
    int delete(Long id);
}
