package ee.webmedia.alfresco.adr.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ee.webmedia.alfresco.adr.model.AdrSkipUpdateDto;
import ee.webmedia.alfresco.common.service.GeneralService;

/**
 * Main implementation of {@link AdrDbService}. This class does not rely on Alfresco, and exchanges data with the database using JDBC(Template) directly.
 */
public class AdrDbServiceImpl implements AdrDbService {

    
    
    private JdbcTemplate jdbcTemplate;
    private DataSource dataSource;
    private NodeService nodeService;
    private GeneralService generalService;
    
    @Override
    public AdrSkipUpdateDto getByAdrRef(NodeRef nodeRef) {
        String sqlQuery = "SELECT * FROM adr_skip_update where adr_node_ref=?";
        try {
        	return jdbcTemplate.queryForObject(sqlQuery, new AdrSkipUpdateMapper(), nodeRef.toString());
        } catch (Exception e) {
        	return null;
        }
    }
    
    @Override
    public AdrSkipUpdateDto getByArchivedRef(NodeRef nodeRef) {
        String sqlQuery = "SELECT * FROM adr_skip_update where archived_node_ref=?";
        try {
        	return jdbcTemplate.queryForObject(sqlQuery, new AdrSkipUpdateMapper(), nodeRef.toString());
        } catch (Exception e) {
        	return null;
        }
    }
    
    @Override
    public AdrSkipUpdateDto getByRestoredRef(NodeRef nodeRef) {
        String sqlQuery = "SELECT * FROM adr_skip_update where restored_node_ref=?";
        try {
        	return jdbcTemplate.queryForObject(sqlQuery, new AdrSkipUpdateMapper(), nodeRef.toString());
        } catch (Exception e) {
        	return null;
        }
    }
    
    @Override
    public int update(AdrSkipUpdateDto dto) {
        String sqlQuery = "UPDATE adr_skip_update SET adr_node_ref = ?, archived_node_ref = ?, restored_node_ref = ?  WHERE asu_id=?";
        Object[] args = new Object[4];
        
        args[0] = dto.getAdrRef();
        args[1] = dto.getArchivedRef();
        args[2] = dto.getRestoredRef();
        args[3] = dto.getId();
        
        int rowsUpdated = jdbcTemplate.update(sqlQuery, args);
        
        return rowsUpdated;
    }
    
    @Override
    public int delete(Long id) {
        String sqlQuery = "DELETE FROM adr_skip_update WHERE asu_id=?";
        Object[] args = new Object[1];
       
        args[0] = id;
        
        int rowsUpdated = jdbcTemplate.update(sqlQuery, args);
        
        return rowsUpdated;
    }

    @Override
    public int insert(AdrSkipUpdateDto dto) {
        String sqlQuery = "INSERT INTO adr_skip_update (adr_node_ref, archived_node_ref, restored_node_ref) VALUES (?, ?, ?)";
        Object[] args = new Object[3];
        
        args[0] = dto.getAdrRef();
        args[1] = dto.getArchivedRef();
        args[2] = dto.getRestoredRef();
        
        int rowsUpdated = jdbcTemplate.update(sqlQuery, args);
        
        return rowsUpdated;
    }
    
    
    
    private class AdrSkipUpdateMapper implements ParameterizedRowMapper<AdrSkipUpdateDto> {

        @Override
        public AdrSkipUpdateDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	AdrSkipUpdateDto dto = new AdrSkipUpdateDto();
        	dto.setId(rs.getLong("asu_id"));
        	dto.setAdrRef(rs.getString("adr_node_ref"));
        	dto.setArchivedRef(rs.getString("archived_node_ref"));
        	dto.setRestoredRef(rs.getString("restored_node_ref"));
            return dto;
        }

    }

    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setGeneralService(GeneralService generalService) {
        this.generalService = generalService;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
