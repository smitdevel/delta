package ee.webmedia.alfresco.kum.model;

import java.util.ArrayList;
import java.util.List;

public class DocumentTypeMappingModel {
	private String objectTypeId;
	private List<ValueFromModel> valuesFrom = new ArrayList<>();
	private List<DefaultValueModel> defaultValues = new ArrayList<>();
	
	public String getObjectTypeId() {
		return objectTypeId;
	}
	public void setObjectTypeId(String objectTypeId) {
		this.objectTypeId = objectTypeId;
	}
	public List<ValueFromModel> getValuesFrom() {
		return valuesFrom;
	}
	public void setValuesFrom(List<ValueFromModel> valuesFrom) {
		this.valuesFrom = valuesFrom;
	}
	public List<DefaultValueModel> getDefaultValues() {
		return defaultValues;
	}
	public void setDefaultValues(List<DefaultValueModel> defaultValues) {
		this.defaultValues = defaultValues;
	}
}
