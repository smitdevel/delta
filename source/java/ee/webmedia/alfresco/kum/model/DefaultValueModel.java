package ee.webmedia.alfresco.kum.model;

public class DefaultValueModel {
	private String valueTo;
	private String value;
	
	public String getValueTo() {
		return valueTo;
	}
	public void setValueTo(String valueTo) {
		this.valueTo = valueTo;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
