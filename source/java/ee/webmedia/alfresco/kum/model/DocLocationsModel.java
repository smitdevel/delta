package ee.webmedia.alfresco.kum.model;

import java.util.List;

public class DocLocationsModel {
	private List<DocLocationModel> docLocations;

	public List<DocLocationModel> getDocLocations() {
		return docLocations;
	}

	public void setDocLocations(List<DocLocationModel> docLocations) {
		this.docLocations = docLocations;
	}

	
}
