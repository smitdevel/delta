package ee.webmedia.alfresco.kum.model;

import java.util.List;

public class MappingModel {
	private DocumentTypeMappingModel generalType;
	private List<DocumentTypeMappingModel> documentTypes;
	
	public DocumentTypeMappingModel getGeneralType() {
		return generalType;
	}
	public void setGeneralType(DocumentTypeMappingModel generalType) {
		this.generalType = generalType;
	}
	public List<DocumentTypeMappingModel> getDocumentTypes() {
		return documentTypes;
	}
	public void setDocumentTypes(List<DocumentTypeMappingModel> documentTypes) {
		this.documentTypes = documentTypes;
	}

}
