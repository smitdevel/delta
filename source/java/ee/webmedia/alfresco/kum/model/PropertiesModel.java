package ee.webmedia.alfresco.kum.model;

import java.util.Map;

public class PropertiesModel {
	private Map<String, Object> fields;

	public Map<String, Object> getFields() {
		return fields;
	}

	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}
}
