package ee.webmedia.alfresco.kum.model;

import java.util.Map;

public class DocumentModel {
	private String uuid;
	private Long nodeId;
	private String caseUuid;
	private PropertiesModel properties;
	private Map<String, PropertiesModel> childs;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Long getNodeId() {
		return nodeId;
	}
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	public String getCaseUuid() {
		return caseUuid;
	}
	public void setCaseUuid(String caseUuid) {
		this.caseUuid = caseUuid;
	}
	public PropertiesModel getProperties() {
		return properties;
	}
	public void setProperties(PropertiesModel properties) {
		this.properties = properties;
	}
	public Map<String, PropertiesModel> getChilds() {
		return childs;
	}
	public void setChilds(Map<String, PropertiesModel> childs) {
		this.childs = childs;
	}
}
