package ee.webmedia.alfresco.kum;

import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.io.File;


public class ImportSettings implements Cloneable {
    private static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(ImportSettings.class);

    private String dataFolder;

    private String workFolder;

    private String defaultOwnerId;
    
    private boolean docLocationSpecified = true;
    
    private int batchSize = 50;
    
	public String getDataFolder() {
        return dataFolder;
    }

    public void setDataFolder(String dataFolder) {
        this.dataFolder = dataFolder;
    }

    public String getWorkFolder() {
        return workFolder;
    }

    public void setWorkFolder(String workFolder) {
        this.workFolder = workFolder;
    }

    public String getDefaultOwnerId() {
        return defaultOwnerId;
    }

    public void setDefaultOwnerId(String defaultOwnerId) {
        this.defaultOwnerId = defaultOwnerId;
    }

    
    
    public boolean getDocLocationSpecified() {
		return docLocationSpecified;
	}

	public void setDocLocationSpecified(boolean docLocationSpecified) {
		this.docLocationSpecified = docLocationSpecified;
	}

	public boolean isValid() {
        
        return isNotBlank(dataFolder) && isNotBlank(workFolder) && isNotBlank(defaultOwnerId) 
        		&& batchSize > 0;
    }

    @Override
    public ImportSettings clone() {
        try {
            return (ImportSettings) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public File getDataFolderFile(String file) {
        return new File(dataFolder, file);
    }

    public File getWorkFolderFile(String file) {
        return new File(workFolder, file);
    }
    
    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
}
