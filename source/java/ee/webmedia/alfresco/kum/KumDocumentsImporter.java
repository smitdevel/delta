package ee.webmedia.alfresco.kum;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentDynamicService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentListService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNamespaceService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getUserService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.repo.transaction.TransactionListenerAdapter;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.springframework.util.CollectionUtils;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ee.webmedia.alfresco.classificator.constant.FieldType;
import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.common.web.WmNode;
import ee.webmedia.alfresco.docadmin.service.Field;
import ee.webmedia.alfresco.docconfig.service.DynamicPropertyDefinition;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamic;
import ee.webmedia.alfresco.document.file.model.FileModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.document.model.DocumentSpecificModel;
import ee.webmedia.alfresco.document.service.DocumentService;
import ee.webmedia.alfresco.kum.KumDocumentsMapper.Mapping;
import ee.webmedia.alfresco.kum.KumDocumentsMapper.PropertyValue;
import ee.webmedia.alfresco.kum.KumDocumentsMapper.TypeInfo;
import ee.webmedia.alfresco.kum.model.DefaultValueModel;
import ee.webmedia.alfresco.kum.model.DocLocationModel;
import ee.webmedia.alfresco.kum.model.DocLocationsModel;
import ee.webmedia.alfresco.kum.model.DocumentModel;
import ee.webmedia.alfresco.kum.model.DocumentTypeMappingModel;
import ee.webmedia.alfresco.kum.model.MappingModel;
import ee.webmedia.alfresco.kum.model.PropertiesModel;
import ee.webmedia.alfresco.kum.model.ValueFromModel;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.orgstructure.model.OrganizationStructureModel;
import ee.webmedia.alfresco.utils.FilenameUtil;
import ee.webmedia.alfresco.utils.RepoUtil;
import ee.webmedia.alfresco.utils.UserUtil;
/**
 * Imports documents and files from JSON.
 */
public class KumDocumentsImporter {

    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(KumDocumentsImporter.class);
    
    
        
    
    private static final String DUMMY_USER_ID = "99999999999";


    private static final String FILE_NAME_MAPPINGS = "mappings.json";
    private static final String FILE_NAME_DOC_LOCATION = "docLocation.json";

    protected static final char CSV_SEPARATOR = ';';
    private static final String CREATOR_MODIFIER = "DHS";
    private static final FastDateFormat staticDateTimeFormat = FastDateFormat.getInstance("dd.MM.yyyy HH:mm:ss");
    private static final FastDateFormat staticDateFormat = FastDateFormat.getInstance("dd.MM.yyyy");
    
    private static final String NODE_REF_PREFIX = "workspace://SpacesStore/";
    
    private static final String DOC_LOCATION_FROM_OTHER = "other";
    
    private static final String FAKE_VALUE_PREFIX = "rO0A";
    
    
    private static final String FIELD_VOLUME = "volume";
    private static final String FIELD_OBJECT_TYPE_ID = "objectTypeId";
    
    private static final String FILE_FIELD_NAME = "name";
    private static final String FILE_FIELD_DISPLAY_NAME = "displayName";
    private static final String FILE_FIELD_FILE_ORDER_IN_LIST = "fileOrderInList";
    private static final String FILE_FIELD_ACTIVE = "active";
    
    

    private final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private final DateFormat dateTimeFormatddMMyy = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
    private final DateFormat dateTimeFormatdMMyy = new SimpleDateFormat("d.MM.yy HH:mm:ss");

    protected SAXReader xmlReader = new SAXReader();

    private File dataFolder;
    private File workFolder;
    private boolean docLocationSpecified;
    private int batchSize;
    private String institutionCode;
    private String defaultOwnerId;
    private String defaultOwnerName;
    private String defaultOwnerEmail;
    private String defaultOwnerJobTitle;
    private String defaultOwnerPhone;
    private List<String> defaultOwnerOrgUnit;
    private String defaultOwnerServiceRank;
    private String defaultOwnerWorkAddress;
    
    private Collection<StoreRef> storeRefs = new LinkedHashSet<StoreRef>();
    
    
    // [SPRING BEANS
    private TransactionService transactionService;
    private DocumentService documentService;
    private GeneralService generalService;
    private FileFolderService fileFolderService;
    private NodeService nodeService;
    private KumDocumentsMapper kumDocumentsMapper;
    private BehaviourFilter behaviourFilter;
    private KumImporter kumImporter;
    
    private ObjectMapper objectMapper = new ObjectMapper();
    
    private MappingModel mappingModel;
    private DocLocationsModel docLocationsModel;
    private DocLocationModel defaultDocLocation;

    protected int documentsToImportCount;
    protected NavigableMap<String /* documentId */, NodeRef> completedDocumentsMap;
    protected NavigableMap<String /* documentId */, NodeRef> readyToIndexDocumentsMap;
    private final Map<String, Pair<NodeRef, Map<QName, Serializable>>> userDataByUserName = new HashMap<String, Pair<NodeRef, Map<QName, Serializable>>>();
    

    protected NavigableSet<Integer> filesToProceed;
    protected File completedDocumentsFile;
    protected File failedDocumentsFile;
    

    private Map<String, Mapping> mappings;

    
    public KumDocumentsImporter(KumImporter kumImporter) {
        
        
        setDocumentService(BeanHelper.getDocumentService());
        setTransactionService(BeanHelper.getTransactionService());
        setGeneralService(BeanHelper.getGeneralService());
        setFileFolderService(BeanHelper.getFileFolderService());
        setNodeService(BeanHelper.getNodeService());
        kumDocumentsMapper = new KumDocumentsMapper();
        kumDocumentsMapper.init();
        kumDocumentsMapper.setNamespaceService(getNamespaceService());
        kumDocumentsMapper.setDictionaryService(BeanHelper.getDictionaryService());
        kumDocumentsMapper.setGeneralService(BeanHelper.getGeneralService());
        setBehaviourFilter(BeanHelper.getPolicyBehaviourFilter());
        setDoraImporter(kumImporter);

        dateFormat.setLenient(false);
        dateTimeFormat.setLenient(false);
    }

    // INJECTORS
    public void setGeneralService(GeneralService generalService) {
        this.generalService = generalService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setKumDocumentsMapper(KumDocumentsMapper kumDocumentsMapper) {
        this.kumDocumentsMapper = kumDocumentsMapper;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setBehaviourFilter(BehaviourFilter behaviourFilter) {
        this.behaviourFilter = behaviourFilter;
    }

    public void setDoraImporter(KumImporter kumImporter) {
        this.kumImporter = kumImporter;
    }

    /**
     * Runs documents import process
     */
    public void runImport(File dataFolder, File workFolder, boolean docLocationSpecified, int batchSize, String defaultOwnerId, String institutionCode)
                    throws Exception {
        this.dataFolder = dataFolder;
        this.workFolder = workFolder;
        this.docLocationSpecified = docLocationSpecified;
        this.batchSize = batchSize;
        this.institutionCode = institutionCode;
        this.defaultOwnerId = defaultOwnerId;
        Map<QName, Serializable> defaultUserProps = getUserService().getUserProperties(defaultOwnerId);
        this.defaultOwnerName = UserUtil.getPersonFullName1(defaultUserProps);
        this.defaultOwnerEmail = (String)defaultUserProps.get(ContentModel.PROP_EMAIL);
        this.defaultOwnerPhone = (String)defaultUserProps.get(ContentModel.PROP_TELEPHONE);
        this.defaultOwnerJobTitle = (String)defaultUserProps.get(ContentModel.PROP_JOBTITLE);
        this.defaultOwnerOrgUnit = (List<String>)defaultUserProps.get(OrganizationStructureModel.Props.ORGANIZATION_PATH);
        this.defaultOwnerServiceRank = (String)defaultUserProps.get(ContentModel.PROP_SERVICE_RANK);
        this.defaultOwnerWorkAddress = (String)defaultUserProps.get(ContentModel.PROP_STREET_HOUSE);
        init();
        // Doc import
        try {
        	
        	
        	
        	File mappingFile = new File(dataFolder, FILE_NAME_MAPPINGS);
        	File docLocationFile = new File(dataFolder, FILE_NAME_DOC_LOCATION);
        	if (!mappingFile.exists() || !docLocationFile.exists()) {
                log.info("Skipping documents import,  mappings.json or/and docLocation.json do/does not exist in the data folder.");
                return;
            }
        	
        	mappingModel = loadJsonMappings(mappingFile);
        	docLocationsModel = loadJsonDocLocations(docLocationFile);
        	setDefaultDocLocation();
        	
        	storeRefs.add(generalService.getStore());
        	
            countDocuments();
            loadCompletedDocuments();
            createDocuments();
        } finally {
        	documentsToImportCount = 0;
            mappings = null;
            mappingModel = null;
            docLocationsModel = null;
            defaultDocLocation = null;
        }

    }

    private void init() {
        documentsToImportCount = 0;
        completedDocumentsMap = new TreeMap<String, NodeRef>();
        mappings = null;
        filesToProceed = null;
        kumDocumentsMapper.init();
        mappingModel = null;
        docLocationsModel = null;
        defaultDocLocation = null;
    }

        
    protected void countDocuments() throws IOException {
    	final String searchField = "\"docName\":";
    	
    	
    	File[] files = new File(dataFolder, "docs").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return StringUtils.isNotBlank(name) && name.endsWith(".json");
            }
        });
    	
    	for (File file : files) {
    		String content = readFileIntoString(file);
    		documentsToImportCount += StringUtils.countMatches(content, searchField);
		}
    	
    	
        log.info("Completed parsing dokumendid.csv , got " + documentsToImportCount + " documents");
    }
    
    
    private static String readFileIntoString(File file) throws IOException {
    	byte[] bytes = Files.readAllBytes(file.toPath());
		return new String(bytes, "UTF-8");
    }
    
    
    private MappingModel loadJsonMappings(File file) throws IOException {
    	
    	return objectMapper.readValue(new InputStreamReader( new FileInputStream(file), StandardCharsets.UTF_8), MappingModel.class);
    }
    
    private DocLocationsModel loadJsonDocLocations(File file) throws IOException {
    	return objectMapper.readValue(new InputStreamReader( new FileInputStream(file), StandardCharsets.UTF_8), DocLocationsModel.class);
    }
    
    private void setDefaultDocLocation() {
    	for (DocLocationModel docLoc: docLocationsModel.getDocLocations()) {
    		if (DOC_LOCATION_FROM_OTHER.equals(docLoc.getFrom())) {
    			defaultDocLocation = docLoc;
    			break;
    		}
    	}
    }
    

    private boolean findUserNode(String doclogixUserName, Map<QName, Serializable> props, QName idQname, QName nameQname, QName emailQname) {
    	Node userNode = null;
    	String userFullname = null;
    	if (StringUtils.isNotBlank(doclogixUserName)) {
    		userFullname = parseUserFullName(doclogixUserName);
    		
    		List<Node> userNodes = getUserService().searchUsers(userFullname, false, -1);
    		if (userNodes != null && userNodes.size() == 1) {
    			userNode = userNodes.get(0);
    		}
    	}
    	if (userNode != null) {
    		String userId = (String)userNode.getProperties().get(ContentModel.PROP_USERNAME);
    		String userName = (String)userNode.getProperties().get(ContentModel.PROP_FIRSTNAME) + " " + (String)userNode.getProperties().get(ContentModel.PROP_LASTNAME);
    		if (userFullname == null || !userFullname.equals(userName)) {
    			return false;
    		}
    		String email = (String)userNode.getProperties().get(ContentModel.PROP_EMAIL);
    		if (idQname != null) {
    			props.put(idQname, userId);
    		}
    		if (nameQname != null) {
    			props.put(nameQname, (StringUtils.isNotBlank(userName)?userName:userId));
    		}
    		if (emailQname != null && StringUtils.isNotBlank(email)) {
    			props.put(emailQname, email);
    		}
    		return true;
    	} else {
    		return false;
    	}
	}
    
    private String parseUserFullName(String doclogixUserName) {
    	String userFullname = doclogixUserName;
		if (doclogixUserName.contains("/")) {
			userFullname = StringUtils.substringBefore(doclogixUserName, "/").trim();
		}
		if (doclogixUserName.contains("(")) {
			userFullname = StringUtils.substringBefore(doclogixUserName, "(").trim();
		}
		
		return userFullname.trim();
    }

    private void setOwnerProperties(Map<QName, Serializable> props, TypeInfo typeInfo) {
        if (StringUtils.isNotBlank(defaultOwnerId)) {
        	props.put(DocumentCommonModel.Props.OWNER_ID, defaultOwnerId);
        	props.put(DocumentCommonModel.Props.OWNER_NAME, defaultOwnerName);
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_EMAIL)) {
        		props.put(DocumentCommonModel.Props.OWNER_EMAIL, defaultOwnerEmail);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_PHONE)) {
        		props.put(DocumentCommonModel.Props.OWNER_PHONE, defaultOwnerPhone);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_JOB_TITLE)) {
        		props.put(DocumentCommonModel.Props.OWNER_JOB_TITLE, defaultOwnerJobTitle);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT)) {
        		props.put(DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT, (ArrayList)defaultOwnerOrgUnit);
        	}
        	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_SERVICE_RANK)) {
        		props.put(DocumentDynamicModel.Props.OWNER_SERVICE_RANK, defaultOwnerServiceRank);
        	}
        	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_WORK_ADDRESS)) {
        		props.put(DocumentDynamicModel.Props.OWNER_WORK_ADDRESS, defaultOwnerWorkAddress);
        	}

        }
        
        String gpSignerName = StringUtils.stripToNull((String) props.get(DocumentCommonModel.Props.SIGNER_NAME));
        
        if (StringUtils.isNotBlank(gpSignerName)) {
       		findUserNode(gpSignerName, props, null, DocumentCommonModel.Props.SIGNER_NAME, null);
        }
        
    }
    
    private boolean hasField(TypeInfo typeInfo, QName fieldQName) {
    	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldQName.getLocalName());
		return pair != null;
    }


    private void checkStop() {
        kumImporter.checkStop();
    }

    private abstract class BatchProgress {
        int totalSize;
        int completedSize;
        int thisRunCompletedSize;
        long thisRunStartTime;
        long startTime;
        String processName;

        private void init() {
            totalSize = 0;
            thisRunStartTime = System.currentTimeMillis();
            startTime = thisRunStartTime;
            completedSize = 0;
        }

        abstract void executeBatch(File jsonFile) throws Exception;

        void executeInTransaction(final File jsonFile) {
            try {
                
                executeBatch(jsonFile);
        
            } catch (Exception e) {
                log.error("Import batch of documents failed, transaction rolled back, continuing with next batch", e);
            }
        }

        private void step(File jsonFile) {
            executeInTransaction(jsonFile);
            completedSize += batchSize;
            thisRunCompletedSize += batchSize;
            long endTime = System.currentTimeMillis();
            double completedPercent = (completedSize) * 100L / ((double) totalSize);
            double lastDocsPerSec = (batchSize) * 1000L / ((double) (endTime - startTime));
            long thisRunTotalTime = endTime - thisRunStartTime;
            double totalDocsPerSec = (thisRunCompletedSize) * 1000L / ((double) thisRunTotalTime);
            long remainingSize = ((long) totalSize) - ((long) completedSize);
            long divisor = (thisRunCompletedSize) * 60000L;
            int etaMinutes = ((int) (remainingSize * thisRunTotalTime / divisor)) + 1;
            int etaHours = 0;
            if (etaMinutes > 59) {
                etaHours = etaMinutes / 60;
                etaMinutes = etaMinutes % 60;
            }
            String eta = etaMinutes + "m";
            if (etaHours > 0) {
                eta = etaHours + "h " + eta;
            }
            String info = "%s: %6.2f%% completed - %7d of %7d, %5.1f items per second (last), %5.1f (total), ETA %s";
            log.info(String.format(info, processName, completedPercent, completedSize, totalSize, lastDocsPerSec, totalDocsPerSec, eta));
            startTime = endTime;
        }

        public void run() {
        	File[] files = new File(dataFolder, "docs").listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return StringUtils.isNotBlank(name) && name.endsWith(".json");
                }
            });
        	
        	if (files == null || files.length <= 0) {
                log.error("No json docs can be found");
                return;
            }
        	
        	try {
	        	
	            init();
	            checkStop();
	            
	            for (File jsonFile: files) {
	                step(jsonFile);
	                checkStop();
	            }
        	} catch (Throwable t) {
        		log.error("Error importing json docs", t);
        	}
            
        }

    }
    
    private static interface CsvWriterClosure {
        void execute(CsvWriter writer) throws IOException;

        String[] getHeaders();
    }

    private static void bindCsvWriteAfterCommit(final File file, final CsvWriterClosure closure) {
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
                try {
                    // Write created documents
                    boolean exists = file.exists();
                    if (!exists) {
                        OutputStream outputStream = new FileOutputStream(file);
                        try {
                            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
                            outputStream.write("\ufeff".getBytes("UTF-8"));
                        } finally {
                            outputStream.close();
                        }
                    }
                    CsvWriter writer = new CsvWriter(new FileWriter(file, true), CSV_SEPARATOR);
                    try {
                        if (!exists) {
                            writer.writeRecord(closure.getHeaders());
                        }
                        closure.execute(writer);
                    } finally {
                        writer.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Error writing file '" + file + "': " + e.getMessage(), e);
                }
            }
        });
    }
    
    

    protected void loadCompletedDocuments() throws Exception {
        failedDocumentsFile = new File(workFolder, "failed_docs.csv");
        OutputStream outputStream = new FileOutputStream(failedDocumentsFile);
        try {
            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
            outputStream.write("\ufeff".getBytes("UTF-8"));
        } finally {
            outputStream.close();
        }
        CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
        try {
            writer.writeRecord(new String[] {
                    "documentFileName",
                    "errorMsg"
            });
        } finally {
            writer.close();
        }

        completedDocumentsMap = new TreeMap<String, NodeRef>();

        completedDocumentsFile = new File(workFolder, "completed_docs.csv");
        if (!completedDocumentsFile.exists()) {
            log.info("Skipping loading previously completed documentId-s, file does not exist: " + completedDocumentsFile);
            return;
        }

        log.info("Loading previously completed documentId-s from file " + completedDocumentsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedDocumentsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String documentId = reader.get(0);
                String nodeRefString = reader.get(1);
                if (StringUtils.isNotBlank(nodeRefString)) {
                    NodeRef documentRef = new NodeRef(nodeRefString);
                    completedDocumentsMap.put(documentId, documentRef);
                }
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + completedDocumentsMap.size() + " previously completed documentId-s");

        
    }

    private class DocumensBatchProgress extends BatchProgress {
        {
            processName = "Documents import";
        }

        @Override
        void executeBatch(File jsonFile) throws Exception {
            createDocumentsBatch(jsonFile);
        }
    }

    private void createDocuments() throws Exception {
        if (documentsToImportCount == 0) {
            log.info("Skipping documents import, no documents found");
            return;
        }
        log.info("Starting documents import. Total = " + documentsToImportCount);
        DocumensBatchProgress batchProgress = new DocumensBatchProgress();
        try {
            batchProgress.run();
        } finally {
            getDocumentListService().updateDocCounters();
        }
        log.info("Documents IMPORT COMPLETE :)");
    }

    static class ImportedDocument {
        String documentId;
        NodeRef nodeRef;
        String docUrl;
        String msg;

        public ImportedDocument(String documentId, NodeRef nodeRef) {
            this.documentId = documentId;
            this.nodeRef = nodeRef;
            this.docUrl = StringEscapeUtils.escapeHtml(BeanHelper.getDocumentTemplateService().getDocumentUrl(nodeRef));
        }
        
        public ImportedDocument(String documentId, String msg) {
            this.documentId = documentId;
            this.msg = msg;
        }
    }

    protected void createDocumentsBatch(File jsonFile) throws Exception {
        int docsCounter = 0;   
        
        TypeReference<HashMap<String, DocumentModel>> typeRef = new TypeReference<HashMap<String, DocumentModel>>() {};
        
        Reader jsonReader = new InputStreamReader( new FileInputStream(jsonFile), StandardCharsets.UTF_8);
        
        Map<String, DocumentModel> kumDocs = objectMapper.readValue(jsonReader, typeRef);
        
        StoreRef generalStoreRef = generalService.getStore();
        
        for (String kumDocId: kumDocs.keySet()) {
        	docsCounter++;
        	final Map<String, ImportedDocument> batchCompletedDocumentsMap = new TreeMap<String, ImportedDocument>();
        	
        	if (completedDocumentsMap.containsKey(kumDocId)) {
        		log.info("kum doc already imported, kumDocId = " + kumDocId);
        		continue;
        	}
        	try {
        		processKumDoc(kumDocId, kumDocs, generalStoreRef, batchCompletedDocumentsMap);
        	} catch (Exception e) {
        		log.error("Error importing document " + kumDocId + ": " + e.getMessage(), e);
        	}
        }
        
        
        batchSize = docsCounter;
    }
    
    
    private void processKumDoc(final String kumDocId, final Map<String, DocumentModel> kumDocs, final StoreRef generalStoreRef, final Map<String, ImportedDocument> batchCompletedDocumentsMap) {
    	getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Object>() {
            @Override
            public Object execute() throws Throwable {
            	behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
            	
            	 AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
                     @Override
                     public void afterCommit() {
                         
                     }

                     @Override
                     public void afterRollback() {
                         completedDocumentsMap.keySet().removeAll(batchCompletedDocumentsMap.keySet());
                     }
                 });
            	
            	DocumentModel kumDoc = kumDocs.get(kumDocId);
                
                if (log.isTraceEnabled()) {
                    log.trace("Processing documentId=" + kumDocId);
                }
                
                
                
                try {
                    ImportedDocument doc = createDocument(kumDocId, kumDoc, generalStoreRef);
                    if (doc.nodeRef != null) {
                    	batchCompletedDocumentsMap.put(kumDocId, doc);
                    	completedDocumentsMap.put(kumDocId, doc.nodeRef); // Add immediately to completedDocumentsMap, because other code wants to access it
                    	kumImporter.getStatus().incrCount();
                    } else {
    	                CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
    	                try {
    	                    writer.writeRecord(new String[] { kumDocId, doc.msg });
    	                } finally {
    	                    writer.close();
    	                }
    	                kumImporter.getStatus().incrFailed();
                    }
                } catch (Throwable e) {
                    CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
                    try {
                        writer.writeRecord(new String[] { kumDocId, e.getMessage() });
                    } finally {
                        writer.close();
                    }
                    kumImporter.getStatus().incrFailed();
                    throw new RuntimeException("Error importing document " + kumDocId + ": " + e.getMessage(), e);
                }
                
                bindCsvWriteAfterCommit(completedDocumentsFile, new CsvWriterClosure() {

                    @Override
                    public String[] getHeaders() {
                        return new String[] {
                                "documentId",
                                "nodeRef",
                                "docUrl"
                        };
                    }

                    @Override
                    public void execute(CsvWriter writer) throws IOException {
                        for (Entry<String, ImportedDocument> entry : batchCompletedDocumentsMap.entrySet()) {
                            ImportedDocument doc = entry.getValue();
                            writer.writeRecord(new String[] {
                                    doc.documentId.toString(),
                                    	doc.nodeRef == null ? "" : doc.nodeRef.toString(),
                                    		doc.docUrl == null ? "" : doc.docUrl
                            });
                        }
                    }
                });
                return null;
            }
        });
    }
    

    private ImportedDocument createDocument(String documentId, DocumentModel kumDoc, StoreRef generalStoreRef) throws DocumentException, ParseException, IOException {
        return importDoc(kumDoc, documentId, mappings, generalStoreRef);
    }
    

    private Map<QName, Serializable> mapDefaultProperties(DocumentTypeMappingModel docTypeMapping, TypeInfo typeInfo, List<String> comments) {
    	
    	
    	Map<QName, Serializable> propsMap = new HashMap<>();
    	
    	if (CollectionUtils.isEmpty(docTypeMapping.getDefaultValues())) {
    		return propsMap;
    	}
    	
    	for (DefaultValueModel defVal: docTypeMapping.getDefaultValues()) {
    		mapProperty(defVal.getValueTo(), defVal.getValue(), typeInfo, propsMap, comments);
    		
    	}
    	
    	return propsMap;
    }
    
    private void mapKumProperties(DocumentModel kumDoc, DocumentTypeMappingModel docTypeMapping, TypeInfo typeInfo, Map<QName, Serializable> propsMap, List<String> comments) {
    	
    	for  (Entry<String, Object> field: kumDoc.getProperties().getFields().entrySet()) {
    		ValueFromModel valueFromModel = findMappedModel(field.getKey(), docTypeMapping.getValuesFrom());
    		if (valueFromModel == null && isAllowedForComment(field.getValue())) {
    			comments.add(field.getKey() + ":" + field.getValue() + ";");
    		} else if (valueFromModel != null) {
    			mapProperty((StringUtils.isNotBlank(valueFromModel.getValueTo())) ? valueFromModel.getValueTo() : valueFromModel.getValueFrom(), 
    					(field.getValue() == null || StringUtils.isBlank(String.valueOf(field.getValue())) || String.valueOf(field.getValue()).startsWith(FAKE_VALUE_PREFIX)) ? valueFromModel.getDefaultValue() : field.getValue(), 
    					typeInfo, propsMap, comments);
    		}
    	}
    	
    }
    
    private ValueFromModel findMappedModel(String kumFiedlName, List<ValueFromModel> valuesMapping) {
    	for (ValueFromModel valueFromModel: valuesMapping) {
    		if (kumFiedlName.equals(valueFromModel.getValueFrom())) {
    			return valueFromModel;
    		}
    	}
    	
    	return null;
    }
    
    private Map<QName, Serializable> mapProperties(DocumentModel kumDoc, DocumentTypeMappingModel docTypeMapping, TypeInfo typeInfo) throws IOException {
    	List<String> comments = new ArrayList<>();
    	Map<QName, Serializable> propsMap = mapDefaultProperties(docTypeMapping, typeInfo, comments);
        mapKumProperties(kumDoc, docTypeMapping, typeInfo, propsMap, comments);
            
        if (!comments.isEmpty()) {
        	addMissingFieldToCommment(StringUtils.join(comments, " "), propsMap, typeInfo);
        }
        
        return propsMap;
    }
    
    private void mapProperty(String fieldName, String value, TypeInfo typeInfo, Map<QName, Serializable> propsMap) {
		if (StringUtils.isBlank(value)) {
			return;
		}
		
		Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldName);
		if (pair == null) {
			return;
		} else {
			DynamicPropertyDefinition propDef = pair.getFirst();
			
	    	KumDocumentsMapper.PropertyValueProvider valueProvider = typeInfo.props.get(fieldName);
	    	if (valueProvider != null) {
	    		PropertyValue propValue = valueProvider.provide();
	            propValue.put(value);
	    		propsMap.put(propDef.getName(), propValue.get());
			} else {
				propsMap.put(propDef.getName(), value);
			}
		}
    }
    
    
    private void mapProperty(String fieldName, Object value, TypeInfo typeInfo, Map<QName, Serializable> propsMap, List<String> comments) {
		if (value == null || StringUtils.isBlank(String.valueOf(value))) {
			return;
		}
		
		Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldName);
		if (pair == null && isAllowedForComment(value)) {
			comments.add(fieldName + ":" + value + ";");
		} else if (pair != null) {
			String strValue = String.valueOf(value); 
			DynamicPropertyDefinition propDef = pair.getFirst();
			FieldType fieldType = propDef.getFieldType();
			
	    	KumDocumentsMapper.PropertyValueProvider valueProvider = typeInfo.props.get(fieldName);
	    	if (valueProvider != null) {
	    		PropertyValue propValue = valueProvider.provide();
	            propValue.put(strValue);
	    		propsMap.put(propDef.getName(), propValue.get());
	    	} else if (fieldType == FieldType.CHECKBOX && StringUtils.isNotBlank(strValue)) {
	    		propsMap.put(propDef.getName(), ("1".equals(strValue)) ? Boolean.TRUE : Boolean.FALSE);
			} else {
				propsMap.put(propDef.getName(), strValue);
			}
		}
    }
    
    
    private boolean isAllowedForComment(Object value) {
    	if (value == null || StringUtils.isBlank(String.valueOf(value)) || value instanceof Integer || value instanceof Boolean || String.valueOf(value).startsWith(FAKE_VALUE_PREFIX)) {
    		return false;
    	}
    	
    	return true;
    }
    
    private NodeRef getVolumeRef(DocumentModel kumDoc) {
    	
    	
    	String volumeRef = StringUtils.isNotBlank(kumDoc.getCaseUuid()) ? NODE_REF_PREFIX + kumDoc.getCaseUuid() : String.valueOf(kumDoc.getProperties().getFields().get(FIELD_VOLUME));
    	
    	if (StringUtils.isBlank(volumeRef) && defaultDocLocation != null) {
    		return new NodeRef(defaultDocLocation.getTo());
    	}
    	
    	for (DocLocationModel docLocation: docLocationsModel.getDocLocations()) {
    		if (volumeRef.endsWith(docLocation.getFrom())) {
    			return new NodeRef(docLocation.getTo());
    		}
    	}
    	
    	if (defaultDocLocation != null) {
    		return new NodeRef(defaultDocLocation.getTo());
    	}
    	
    	return null;
    	
    }
    
    private DocumentTypeMappingModel getDocTypeMapping(DocumentModel kumDoc) {
    	String docType = String.valueOf(kumDoc.getProperties().getFields().get(FIELD_OBJECT_TYPE_ID));
    	
    	if (StringUtils.isBlank(docType)) {
    		return mappingModel.getGeneralType();
    	}
    	
    	for (DocumentTypeMappingModel docTypeMapping: mappingModel.getDocumentTypes()) {
    		if (docType.equals(docTypeMapping.getObjectTypeId())) {
    			DocumentTypeMappingModel fullDocTypeMapping = new DocumentTypeMappingModel();
    			fullDocTypeMapping.setObjectTypeId(docTypeMapping.getObjectTypeId());
    			fullDocTypeMapping.getDefaultValues().addAll(mappingModel.getGeneralType().getDefaultValues());
    			fullDocTypeMapping.getDefaultValues().addAll(docTypeMapping.getDefaultValues());
    			fullDocTypeMapping.getValuesFrom().addAll(mappingModel.getGeneralType().getValuesFrom());
    			fullDocTypeMapping.getValuesFrom().addAll(docTypeMapping.getValuesFrom());
    			
    			return fullDocTypeMapping;
    		}
    	}
    	
    	return mappingModel.getGeneralType();
    }
    
    

    private ImportedDocument importDoc(DocumentModel kumDoc, String documentId, Map<String, Mapping> mappings, StoreRef archivalStoreRef) throws DocumentException,
    ParseException, IOException {

    	
    	
       
        NodeRef volumeRef = getVolumeRef(kumDoc);
        if (volumeRef == null) {
        	return new ImportedDocument(documentId, "volume NodeRef not found for: " + kumDoc.getProperties().getFields().get(FIELD_VOLUME));
        }
        
        
        DocumentTypeMappingModel docTypeMapping = getDocTypeMapping(kumDoc);
        
       
        TypeInfo typeInfo = kumDocumentsMapper.getTypeInfo(docTypeMapping.getObjectTypeId());
        if (typeInfo == null) {
        	new ImportedDocument(documentId, "missing document type: " + docTypeMapping.getObjectTypeId());
        }
		
		
		Map<QName, Serializable> propsMap = setProps(kumDoc, docTypeMapping, volumeRef, typeInfo);

		DocumentDynamic doc = getDocumentDynamicService().createNewDocument(typeInfo.docVer, volumeRef, false)
				.getFirst();
		NodeRef documentRef = doc.getNodeRef();
		nodeService.addAspect(documentRef, DocumentCommonModel.Aspects.SEARCHABLE, null);
		
		
		Map<NodeRef, Map<QName, NodeRef>> parentRefsByVolumeRef = new HashMap<NodeRef, Map<QName, NodeRef>>();
		Map<QName, NodeRef> parentRefs = documentService.getDocumentParents(documentRef);
		parentRefsByVolumeRef.put(volumeRef, parentRefs);

		propsMap.putAll(parentRefs);

		checkProps(propsMap, null, typeInfo.propDefs);
		

    	propsMap.put(DocumentCommonModel.Props.DOCUMENT_IS_IMPORTED, Boolean.TRUE);
    	
    	checkMandotoryFields(propsMap, typeInfo.propDefs);
    	
		doc.getNode().getProperties().putAll(RepoUtil.toStringProperties(propsMap));
		doc.getNode().getProperties().put(
				DocumentService.TransientProps.TEMP_LOGGING_DISABLED_DOCUMENT_METADATA_CHANGED.toString(),
				Boolean.TRUE);
		getDocumentDynamicService().updateDocument(doc, Arrays.asList("kumImporter"), false, true);
		

		LogEntry logEntry = new LogEntry();
		logEntry.setComputerIp("127.0.0.1");
		logEntry.setComputerName("localhost");
		logEntry.setLevel(LogObject.DOCUMENT.getLevel());
		logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
		logEntry.setCreatorId("IMPORT");
		logEntry.setCreatorName("IMPORT");
		logEntry.setEventDescription("Dokumendi importimine (kum)");
		logEntry.setObjectId(documentRef.toString());
		BeanHelper.getLogService().addLogEntry(logEntry);

		addFiles(kumDoc, documentRef);
		
		return new ImportedDocument(documentId, documentRef);
		
    }
    

    private void checkMandotoryFields(Map<QName, Serializable> propsMap, Map<String, Pair<DynamicPropertyDefinition, Field>> propDefs) {
    	List<String> errors = new ArrayList<String>();
    	for (Entry<String, Pair<DynamicPropertyDefinition, Field>> entry : propDefs.entrySet()) {
    		if (entry.getValue().getSecond() != null && entry.getValue().getFirst().isMandatory() && !containsProp(entry.getKey(), propsMap)) {
    			errors.add("Missing mandatory property " + entry.getKey());
    		}
    	}
    	if (!errors.isEmpty()) {
            log.info("props=" + WmNode.toString(propsMap, getNamespaceService()));
            throw new RuntimeException("Found " + errors.size() + " errors:\n  * " + StringUtils.join(errors, "\n  * "));
        }
    }
    
    private boolean containsProp(String propLocalName, Map<QName, Serializable> propsMap) {
    	for (Entry<QName, Serializable> entry : propsMap.entrySet()) {
    		if (propLocalName.equals(entry.getKey().getLocalName())) {
    			return true;
    		}
    	}
    	return false;
    }
    
    
    private void checkProps(Map<QName, Serializable> propsMap, QName[] hierarchy, Map<String, Pair<DynamicPropertyDefinition, Field>> propDefs) {
        List<String> errors = new ArrayList<String>();
        for (Entry<QName, Serializable> entry : propsMap.entrySet()) {
            String propNameString = entry.getKey().toPrefixString(getNamespaceService());
            if (!DocumentDynamicModel.URI.equals(entry.getKey().getNamespaceURI())) {
                errors.add("Property " + propNameString + " has wrong namespace");
                continue;
            }
            Pair<DynamicPropertyDefinition, Field> pair = propDefs.get(entry.getKey().getLocalName());
            if (pair == null) {
                errors.add("Property " + propNameString + " is missing propertyDefinition");
                continue;
            }

            if (hierarchy == null) {
                hierarchy = new QName[] {};
            }
            DynamicPropertyDefinition propDef = pair.getFirst();
            QName[] propHierarchy = propDef.getChildAssocTypeQNameHierarchy();
            if (propHierarchy == null) {
                propHierarchy = new QName[] {};
            }
            if (!Arrays.equals(hierarchy, propHierarchy)) {
                errors.add("Property " + propNameString + " should be on hierarchy " + Arrays.asList(propHierarchy) + " but is on hierarchy " + Arrays.asList(hierarchy));
                continue;
            }

            // DocumentsMapper doesn't know about single- and multi-valued properties, so handle it here
            Serializable value = entry.getValue();
            if (propDef.isMultiValued() && value != null && !(value instanceof List)) {
                ArrayList<Serializable> list = new ArrayList<Serializable>();
                list.add(value);
                value = list;
                entry.setValue(value);
            }

            if (value == null) {
                continue;
            }
            String requiredClassName = propDef.getDataType().getJavaClassName();
            if (value instanceof List<?>) {
                if (!("additionalRecipientName".equals(propDef.getName().getLocalName()) || "partyName".equals(propDef.getName().getLocalName()) || "partyContactPerson".equals(propDef.getName().getLocalName())) && !propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should not be a List, but is a List");
                    continue;
                }
                List<Serializable> list = (List<Serializable>) value;
                int i = 0;
                for (Serializable listElement : list) {
                    if (listElement == null) {
                        i++;
                        continue;
                    }
                    String realClassName = listElement.getClass().getName();
                    if (!realClassName.equals(requiredClassName)) {
                        errors.add("Property " + propNameString + " value at index " + i + " should be of type " + requiredClassName + " but is of type " + realClassName + " : "
                                + listElement);
                        continue;
                    }
                    i++;
                }
            } else {
                if (propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should be a List, but is not a List");
                    continue;
                }
                String realClassName = value.getClass().getName();
                if (!realClassName.equals(requiredClassName)) {
                    errors.add("Property " + propNameString + " value should be of type " + requiredClassName + " but is of type " + realClassName + " : " + value);
                    continue;
                }
            }
        }
        if (!errors.isEmpty()) {
            log.info("props=" + WmNode.toString(propsMap, getNamespaceService()));
            throw new RuntimeException("Found " + errors.size() + " errors:\n  * " + StringUtils.join(errors, "\n  * "));
        }
    }

    
    
    private boolean isNotEmptyProps(Map<QName, Serializable> propsMap) {
    	String partyNames = (String)propsMap.get(DocumentSpecificModel.Props.PARTY_NAME);
    	String partyContactPersons = (String)propsMap.get(DocumentSpecificModel.Props.PARTY_CONTACT_PERSON);
    	if (StringUtils.isNotBlank(partyNames) || StringUtils.isNotBlank(partyContactPersons)) {
    		return true;
    	}
    	return false;
    }

    private Map<QName, Serializable> setProps(DocumentModel kumDoc, DocumentTypeMappingModel docTypeMapping, NodeRef volumeRef, TypeInfo typeInfo) throws IOException {
        Map<QName, Serializable> propsMap = mapProperties(kumDoc, docTypeMapping, typeInfo);

        String regNumber = (String)propsMap.get(DocumentCommonModel.Props.REG_NUMBER);
        if (StringUtils.isNotBlank(regNumber) && propsMap.get(DocumentCommonModel.Props.REG_DATE_TIME) != null) {
        	
        	String shortRegNumber = null;
        	if (StringUtils.isNotBlank(regNumber)) {
        		if (regNumber.contains("/")) {
        			shortRegNumber = StringUtils.substringAfterLast(regNumber, "/");
        		} else if (regNumber.contains("-")) {
            		shortRegNumber = StringUtils.substringBeforeLast(shortRegNumber, "-");
            	} else {
            		shortRegNumber = regNumber;
            	}
        		
        	}
            propsMap.put(DocumentCommonModel.Props.SHORT_REG_NUMBER, shortRegNumber);
            
        }

        setOwnerProperties(propsMap, typeInfo);
        
    	if (StringUtils.isBlank((String) propsMap.get(DocumentCommonModel.Props.DOC_NAME))) {
        	throw new RuntimeException("docName cannot be blank");
        }

        return propsMap;
    }
    
    
    private void addMissingFieldToCommment(String missingFieldNameValue, Map<QName, Serializable> propsMap, TypeInfo typeInfo) {
    	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pairComment = typeInfo.propDefs.get(DocumentCommonModel.Props.COMMENT.getLocalName());
		if (pairComment != null) {
			String propComment =  (String) propsMap.get(DocumentCommonModel.Props.COMMENT);
			propComment = (StringUtils.isNotBlank(propComment)) ? propComment + " " + missingFieldNameValue + ";" : missingFieldNameValue + ";"; 
			propsMap.put(DocumentCommonModel.Props.COMMENT, propComment);
		}
    }
    
    
    private void addFiles(DocumentModel kumDoc, NodeRef docNodeRef) {
    	
    	if (CollectionUtils.isEmpty(kumDoc.getChilds())) {
    		return;
    	}
    	
    	for (String fileId: kumDoc.getChilds().keySet()) {
    		File file = new File(dataFolder + "/files/" + kumDoc.getUuid(), fileId + ".dat");
    		boolean isZipped = false;
    		if (file == null || !file.exists()) {
    			file = new File(dataFolder + "/files/" + kumDoc.getUuid(), fileId + ".dat.gz");
    			isZipped = true;
    			if (file == null || !file.exists()) {
    				throw new RuntimeException("fileId does not exist: " + fileId);
    			}
    		}
    		PropertiesModel propsModel = kumDoc.getChilds().get(fileId);
    		String fileName = (String) propsModel.getFields().get(FILE_FIELD_NAME);
    		String displayName = (String) propsModel.getFields().get(FILE_FIELD_DISPLAY_NAME);
    		Integer fileOrderInList = (Integer) propsModel.getFields().get(FILE_FIELD_FILE_ORDER_IN_LIST);
    		Boolean active = (Boolean)propsModel.getFields().get(FILE_FIELD_ACTIVE);
    		
    		addFile(fileName,
    				(StringUtils.isBlank(displayName)) ? fileName : displayName,
    				(fileOrderInList != null) ? fileOrderInList : 1, 
    				file, 
    				docNodeRef, 
    				(active != null) ? active : true, isZipped);
    		
    	}
    	
    }
    
    private NodeRef addFile(String fileName, String displayName, Integer fileOrderInList, File file, NodeRef parentNodeRef, boolean active, boolean isZipped) {
		fileName = (StringUtils.isBlank(fileName)) ? FilenameUtil.makeSafeFilename(displayName) : fileName;
		fileName = generalService.getUniqueFileName(parentNodeRef, fileName);
		final FileInfo fileInfo = fileFolderService.create(parentNodeRef, fileName, ContentModel.TYPE_CONTENT);
		final NodeRef fileRef = fileInfo.getNodeRef();

		String mimetype = BeanHelper.getMimetypeService().guessMimetype(fileName);
		final ContentWriter writer = fileFolderService.getWriter(fileRef);
		writer.setMimetype(mimetype);
		writer.setEncoding("UTF-8");

		if (isZipped) {
			try  {
				GZIPInputStream zis = new GZIPInputStream(new FileInputStream(file));
				writer.putContent(zis);
			} catch (IOException e) {
				throw new RuntimeException("Failed to unzip file: " + file.getName());
			}
		} else {
			writer.putContent(file);
		}

		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		props.put(ContentModel.PROP_CREATOR, CREATOR_MODIFIER);
		props.put(ContentModel.PROP_MODIFIER, CREATOR_MODIFIER);
		props.put(FileModel.Props.DISPLAY_NAME, displayName);
		props.put(FileModel.Props.FILE_ORDER_IN_LIST, fileOrderInList);
		props.put(FileModel.Props.ACTIVE, Boolean.valueOf(active));
		nodeService.addProperties(fileRef, props);
		return fileRef;
	}

    private NodeRef findCompletedDoc(String id) {
        NodeRef res = completedDocumentsMap.get(id);
        return res;
    }
    
    

    // HELPER METHODS
    /** RetryingTransactionHelper that only tries to do things once. */
    private RetryingTransactionHelper getTransactionHelper() {
        RetryingTransactionHelper helper = new RetryingTransactionHelper();
        helper.setMaxRetries(1);
        helper.setTransactionService(transactionService);
        return helper;
    }
    
}
