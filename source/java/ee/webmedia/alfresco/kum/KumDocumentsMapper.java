package ee.webmedia.alfresco.kum;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentAdminService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentConfigService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNamespaceService;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docadmin.service.DocumentAdminService;
import ee.webmedia.alfresco.docadmin.service.DocumentType;
import ee.webmedia.alfresco.docadmin.service.DocumentTypeVersion;
import ee.webmedia.alfresco.docadmin.service.Field;
import ee.webmedia.alfresco.docadmin.web.DocAdminUtil;
import ee.webmedia.alfresco.docconfig.service.DynamicPropertyDefinition;
import ee.webmedia.alfresco.utils.TreeNode;

public class KumDocumentsMapper {
    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(KumDocumentsMapper.class);

    private static final DateFormat dateFormat1 = new SimpleDateFormat("dd.MM.yyyy");
    private static final DateFormat dateFormat2 = new SimpleDateFormat("d.M.yyyy");
    private static final DateFormat dateFormat3 = new SimpleDateFormat("dd.M.yyyy");
    private static final DateFormat dateFormat4 = new SimpleDateFormat("d.MM.yyyy");
    private static final DateFormat dateFormat5 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    
    private static final DateFormat dateFormatWithoutYear = new SimpleDateFormat("dd.MM");
    private static final DateFormat dateFormatOnlyDay = new SimpleDateFormat("dd");

    public KumDocumentsMapper() {
        dateFormat1.setLenient(false);
        dateFormat2.setLenient(false);
        dateFormat3.setLenient(false);
        dateFormat4.setLenient(false);
        dateFormat5.setLenient(false);
        dateFormatWithoutYear.setLenient(false);
        dateFormatOnlyDay.setLenient(false);
    }
    
    private static Date getDateFormat5Date(String dateStr) throws Exception {
    	int index = dateStr.lastIndexOf(":");
    	int hoursToAdd = Integer.parseInt(dateStr.substring(index - 1, index));
    	Calendar cal = Calendar.getInstance();
    	
		cal.setTime(dateFormat5.parse(dateStr));
		cal.add(Calendar.HOUR_OF_DAY, hoursToAdd);
		return cal.getTime();
    }

    static class Mapping {
        String from;
        QName to;
        QName assoc;
        List<PropMapping> props;
        Set<Mapping> subMappings = new HashSet<Mapping>();
        TypeInfo typeInfo;
        String defaultVolume;

        @Override
        public String toString() {
            StringBuilder s = new StringBuilder(String.format("[%s -> %s %s :\n",
                    from,
                    to == null ? null : to.toPrefixString(getNamespaceService()),
                    assoc == null ? null : assoc.toPrefixString(getNamespaceService())));
            for (PropMapping m : props) {
                s.append("  ");
                s.append(m);
                s.append("\n");
            }
            s.append("]");
            return s.toString();
        }

        public Mapping(Mapping m, String from, TypeInfo typeInfo) {
            if (m != null) {
                props = new ArrayList<PropMapping>(m.props);
            } else {
                props = new ArrayList<PropMapping>();
            }
            this.from = from;
            this.typeInfo = typeInfo;
            to = typeInfo == null ? null : typeInfo.qname;
        }

        public void add(PropMapping pm) {
            props.add(pm);
        }

        public PropMapping getPropMappingTo(String to) {
            return getPropMappingTo(to, false);
        }

        public PropMapping requirePropMappingTo(String to) {
            return getPropMappingTo(to, true);
        }

        private PropMapping getPropMappingTo(String to, boolean throwExceptionIfNotExists) {
            PropMapping result = null;
            for (PropMapping propMapping : props) {
                if (to.equals(propMapping.to)) {
                    if (result != null) {
                        throw new RuntimeException("Multiple <prop> elements with same to='" + to + "' found under <mapping from='" + from + "' to='" + this.to + "'>");
                    }
                    result = propMapping;
                }
            }
            if (throwExceptionIfNotExists && result == null) {
                throw new RuntimeException("No <prop> elements with to='" + to + "' found under <mapping from='" + from + "' to='" + this.to + "'>");
            }
            return result;
        }
    }

    static class PropMapping {
        String from;
        String to;
        String toFirst;
        String toSecond;
        String prefix;
        Splitter splitter;
        String expression;

        @Override
        public String toString() {
            if (splitter == null) {
                if (prefix == null) {
                    return String.format("[%s -> %s]", from, to);
                } else {
                    return String.format("[%s -> %s : %s]", from, to, prefix);
                }
            }
            return String.format("[%s - %s -> (%s, %s)]", from, splitter, toFirst, toSecond);
        }

        public PropMapping() {
        }

        public PropMapping(String from, String to, String prefix, String expression) {
            this.from = from;
            this.to = to;
            this.prefix = prefix;
            this.expression = expression;
        }

        public PropMapping(String from, String to, String prefix, String toFirst, String toSecond, Splitter splitter, String expression) {
            this.to = to;
            this.prefix = prefix;
            this.from = from;
            this.toFirst = toFirst;
            this.toSecond = toSecond;
            this.splitter = splitter;
            this.expression = expression;
        }

    }

    @SuppressWarnings("serial")
    static class ConvertException extends Exception {
    }

    static interface Splitter {
        Pair split(String s) throws ConvertException;
    }

    class PeriodSplitter implements Splitter {
        @Override
        public Pair split(String s) throws ConvertException {
            Date first = null;
            Date second = null;
            int i = s.indexOf('-');
            if (i != -1) {
                try {
                    String sf = StringUtils.strip(s.substring(0, i), ". ");
                    String ss = StringUtils.strip(s.substring(i + 1), ". ");
                    second = dateFormat1.parse(ss);
                    try {
                        first = dateFormat1.parse(sf);
                    } catch (ParseException e) {
                        try {
                            first = dateFormatWithoutYear.parse(sf);
                            first = combine(first, second, false);
                        } catch (ParseException ee) {
                            first = dateFormatOnlyDay.parse(sf);
                            first = combine(first, second, true);
                        }
                    }
                    return new Pair(first, second);
                } catch (ParseException e) {
                }
            } else {
            	try {
            		first = dateFormat1.parse(s);
                } catch (ParseException e1) {
                	try {
                		first = dateFormat2.parse(s);
                    } catch (ParseException e2) {
                    	try {
                    		first = dateFormat3.parse(s);
                        } catch (ParseException e3) {
                        	try {
                        		first = dateFormat4.parse(s);
                            } catch (ParseException e4) {
                            	try {
                            		first = getDateFormat5Date(s);
                                } catch (Exception e) {
                                    throw new ConvertException();
                                }
                            }
                        }
                    }
                }
                return new Pair(first, first);
            }
            throw new ConvertException();
        }

        private Date combine(Date dayDate, Date yearDate, boolean withMonthes) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(yearDate);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            calendar.setTime(dayDate);
            calendar.set(Calendar.YEAR, year);
            if (withMonthes) {
                calendar.set(Calendar.MONTH, month);
            }
            return calendar.getTime();
        }

        @Override
        public String toString() {
            return "'period' splitter";
        }
    }

    static class Pair {
        Object first;
        Object second;

        public Pair(Object first, Object second) {
            this.first = first;
            this.second = second;
        }

    }

    static class StringValueAppender {
        String s;
        StringBuilder sb;
        String separator = ", ";

        public StringValueAppender() {
        }

        public StringValueAppender(String separator) {
            this.separator = separator;
        }

        public void add(String s) {
            if (StringUtils.isBlank(s)) {
                return;
            }
            if (this.s == null) {
                this.s = s;
            } else {
                if (sb == null) {
                    sb = new StringBuilder(this.s);
                }
                sb.append(separator).append(s);
            }
        }

        public String get() {
            if (sb == null) {
                return s;
            } else {
                return sb.toString();
            }
        }
    }

    static String join(String separator, String... strings) {
        StringValueAppender app = new StringValueAppender(separator);
        for (String s : strings) {
            app.add(s);
        }
        return app.get();
    }

    static abstract class PropertyValueProvider {
        QName qname;

        PropertyValueProvider withQName(QName qname) {
            this.qname = qname;
            return this;
        }

        protected abstract PropertyValue provide();

        @Override
        public String toString() {
            return ObjectUtils.toString(qname);
        }
    }

    static class StringPropertyValueProvider extends PropertyValueProvider {

        @Override
        public PropertyValue provide() {
            return new StringPropertyValue().withQName(qname);
        }

    }

    static class CommentPropertyValueProvider extends PropertyValueProvider {

        @Override
        public PropertyValue provide() {
            return new CommentPropertyValue().withQName(qname);
        }

    }

    class DatePropertyValueProvider extends PropertyValueProvider {

        @Override
        public PropertyValue provide() {
            return new DatePropertyValue().withQName(qname);
        }

    }
    
    class ListPropertyValueProvider extends PropertyValueProvider {

        @Override
        public PropertyValue provide() {
            return new ListPropertyValue().withQName(qname);
        }

    }

    static class LongPropertyValueProvider extends PropertyValueProvider {

        @Override
        public PropertyValue provide() {
            return new LongPropertyValue().withQName(qname);
        }

    }
    
    static class DoublePropertyValueProvider extends PropertyValueProvider {

        @Override
        public PropertyValue provide() {
            return new DoublePropertyValue().withQName(qname);
        }

    }

    static class DocumentValue {
        TypeInfo typeInfo;
        Map<String, PropertyValue> props = new HashMap<String, PropertyValue>();

        public DocumentValue(TypeInfo typeInfo) {
            this.typeInfo = typeInfo;
        }

        void put(String s, String prop, String prefix) {
            if (StringUtils.isNotBlank(s)) {
                getPropertyValue(prop).put(s, prefix);
            }
        }

        void putObject(Object o, String prop) {
            if (o != null) {
                getPropertyValue(prop).putObject(o);
            }
        }

        private PropertyValue getPropertyValue(String prop) {
            PropertyValue value = props.get(prop);
            if (value == null) {
                PropertyValueProvider valueProvider = typeInfo.props.get(prop);
                Assert.notNull(valueProvider);
                value = valueProvider.provide();
                props.put(prop, value);
            }
            return value;
        }
    }

    static abstract class PropertyValue {
        QName qname;

        PropertyValue withQName(QName qname) {
            this.qname = qname;
            return this;
        }

        abstract void put(String s);

        void putWithPrefix(String s, String prefix) {
            throw new UnsupportedOperationException("Could not put " + s + " with prefix " + prefix);
        }

        void put(String s, String prefix) {
            if (prefix == null) {
                put(s);
            } else {
                putWithPrefix(s, prefix);
            }
        }

        void putObject(Object o) {
            throw new UnsupportedOperationException();
        }

        abstract Serializable get();
    }

    static class StringPropertyValue extends PropertyValue {
        StringValueAppender value = new StringValueAppender();

        @Override
        public void put(String s) {
            value.add(s);
        }

        @Override
        public Serializable get() {
            return value.get();
        }
    }

    static class CommentPropertyValue extends StringPropertyValue {
        final static String SPACE_DELIMED = ".spaced";
        List<String> prefixes = new ArrayList<String>();
        Map<String, StringValueAppender> values = new HashMap<String, StringValueAppender>();
        StringValueAppender spaceDelimedValue = new StringValueAppender(" ");
        {
            value = new StringValueAppender("\n");
        }

        @Override
        public void putWithPrefix(String s, String prefix) {
            Assert.notNull(prefix);
            if (SPACE_DELIMED.equals(prefix)) {
                spaceDelimedValue.add(s);
                return;
            }
            if (!StringUtils.isBlank(s)) {
                StringValueAppender app;
                if (!values.containsKey(prefix)) {
                    prefixes.add(prefix);
                    app = new StringValueAppender();
                    values.put(prefix, app);
                } else {
                    app = values.get(prefix);
                }
                app.add(s);
            }
        }

        @Override
        public Serializable get() {
            value.add(spaceDelimedValue.get());
            for (String prefix : prefixes) {
                String prefixValue = values.get(prefix).get();
                if (StringUtils.isNotBlank(prefixValue)) {
                    value.add(prefix + prefixValue);
                }
            }
            String string = value.get();
            return string;
        }
    }

    class DatePropertyValue extends PropertyValue {
        Date value;

        @Override
        public Serializable get() {
            return value;
        }

        @Override
        public void put(String s) {
            try {
                value = dateFormat1.parse(s);
            } catch (ParseException e1) {
            	try {
                    value = dateFormat2.parse(s);
                } catch (ParseException e2) {
                	try {
                        value = dateFormat3.parse(s);
                    } catch (ParseException e3) {
                    	try {
                            value = dateFormat4.parse(s);
                        } catch (ParseException e4) {
                        	try {
                        		value = getDateFormat5Date(s);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void putObject(Object o) {
            if (o == null) {
                return;
            }
            Assert.isInstanceOf(Date.class, o);
            value = (Date) o;
        }

    }
    
    class ListPropertyValue extends PropertyValue {
        List<String> value;

        @Override
        public Serializable get() {
            return (Serializable)value;
        }

        @Override
        public void put(String s) {
        	if (value == null) {
        		value = new ArrayList<String>();
        	}
        	value.add(s);
        }

        @Override
        public void putObject(Object o) {
            if (o == null) {
                return;
            }
            Assert.isInstanceOf(List.class, o);
            value = (List<String>) o;
        }

    }

    static class LongPropertyValue extends PropertyValue {
        Long value;

        @Override
        public Serializable get() {
            return value;
        }

        @Override
        public void put(String s) {
            String number = extractNumber(s);
            try {
            	value = Long.parseLong(number);
            } catch (NumberFormatException e) {
            }
        }

        private static String extractNumber(String s) {
            StringBuilder sb = new StringBuilder();
            for (Character c : s.toCharArray()) {
                if (isGoodNumberCharacter(c)) {
                    sb.append(c);
                }
            }
            return sb.toString();
        }

        private static boolean isGoodNumberCharacter(Character c) {
            return Character.isDigit(c) || '.' == c;
        }
    }
    
    static class DoublePropertyValue extends PropertyValue {
        Double value;

        @Override
        public Serializable get() {
            return value;
        }

        @Override
        public void put(String s) {
            String number = extractNumber(s);
            try {
            	value = Double.parseDouble(number);
            } catch (NumberFormatException e) {
            }
        }

        private static String extractNumber(String s) {
        	s = s.replace(',', '.');
            StringBuilder sb = new StringBuilder();
            for (Character c : s.toCharArray()) {
                if (isGoodNumberCharacter(c)) {
                    sb.append(c);
                }
            }
            return sb.toString();
        }

        private static boolean isGoodNumberCharacter(Character c) {
            return Character.isDigit(c) || '.' == c;
        }
    }

    static class TypeInfo {
        String name;
        QName qname;
        Map<String, PropertyValueProvider> props = new HashMap<String, PropertyValueProvider>();
        DocumentTypeVersion docVer;
        Map<String, org.alfresco.util.Pair<DynamicPropertyDefinition, Field>> propDefs;
        TreeNode<QName> childAssocTypeQNameTree;
        QName[] hierarchy;

        public TypeInfo(String name, QName qname) {
            this.name = name;
            this.qname = qname;
        }

    }

    TypeInfo createTypeInfo(String name, String prefix) {

        QName qname = QName.createQName(prefix, name, namespaceService);
        TypeInfo typeInfo = new TypeInfo(name, qname);

        DocumentType docType = getDocumentAdminService().getDocumentType(name, DocumentAdminService.DOC_TYPE_WITH_OUT_GRAND_CHILDREN_EXEPT_LATEST_DOCTYPE_VER);
        Assert.notNull(docType, "Document type doesn't exist: " + name);
        typeInfo.docVer = docType.getLatestDocumentTypeVersion();
        typeInfo.propDefs = getDocumentConfigService().getPropertyDefinitions(DocAdminUtil.getPropDefCacheKey(DocumentType.class, typeInfo.docVer));
        typeInfo.childAssocTypeQNameTree = getDocumentConfigService().getChildAssocTypeQNameTree(typeInfo.docVer);
        typeInfo.hierarchy = new QName[] {};
    

        for (org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair : typeInfo.propDefs.values()) {
            DynamicPropertyDefinition propDef = pair.getFirst();
            QName[] hierarchy = propDef.getChildAssocTypeQNameHierarchy();
            if (hierarchy == null) {
                hierarchy = new QName[] {};
            }
            if (!Arrays.equals(hierarchy, typeInfo.hierarchy)) {
                continue;
            }

            QName prop = propDef.getName();
            PropertyValueProvider valueProvider = getProvider(prop.getLocalName(), propDef);
            if (valueProvider == null) {
                continue;
            }
            typeInfo.props.put(prop.getLocalName(), valueProvider.withQName(prop));
        }
        return typeInfo;
    }

    PropertyValueProvider getProvider(String name, PropertyDefinition propDef) {
        // SIM "sendDesc".equals(name)
        if ("comment".equals(name) || "errandComment".equals(name) || "content".equals(name) || "price".equals(name) || "expenseType".equals(name)) {
            return new CommentPropertyValueProvider();
        }
        String javaClassName = propDef.getDataType().getJavaClassName();
        if ("java.util.Date".equals(javaClassName)) {
            return new DatePropertyValueProvider();
        }
        if ("java.lang.Double".equals(javaClassName)) {
            return new DoublePropertyValueProvider();
        }
        if ("java.lang.Long".equals(javaClassName)) {
            return new LongPropertyValueProvider();
        }
        
		if (propDef.isMultiValued()) {
            return new ListPropertyValueProvider();
        }
		
        if (!"java.lang.String".equals(javaClassName)) {
            log.info("Data type " + javaClassName + " is not supported for property " + propDef.getName().toPrefixString(BeanHelper.getNamespaceService())
                    + ", not allowing mapping");
            return null;
        }
        return new StringPropertyValueProvider();
    }

    Map<String, TypeInfo> typeInfos = new HashMap<String, TypeInfo>();

    public void init() {
    	typeInfos = new HashMap<String, TypeInfo>();
    }
    
    protected TypeInfo getTypeInfo(String docType) {

        TypeInfo typeInfo = typeInfos.get(docType);
        if (typeInfo == null) {
            typeInfo = createTypeInfo(docType, "docdyn");
            typeInfos.put(docType, typeInfo);
        }

        return typeInfo;
    }

        
    
    private NamespaceService namespaceService;
    private DictionaryService dictionaryService;
    private GeneralService generalService;

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setDictionaryService(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    public void setGeneralService(GeneralService generalService) {
        this.generalService = generalService;
    }
    
    public static void main(String [] args) {
    	String date = "2019-05-08T17:45:43.045+03:00";
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    	
    	try {
			System.out.print("date: " + dateFormat.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
