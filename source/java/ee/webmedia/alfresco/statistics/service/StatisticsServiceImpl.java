package ee.webmedia.alfresco.statistics.service;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentAdminService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;

import org.alfresco.util.Pair;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docadmin.service.DocumentAdminService;
import ee.webmedia.alfresco.docadmin.service.DocumentType;
import ee.webmedia.alfresco.statistics.service.type.Statistic;
import ee.webmedia.alfresco.statistics.service.type.StatisticType;
import ee.webmedia.alfresco.statistics.web.StatisticsListDialog;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.workflow.model.Status;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.service.cmr.repository.StoreRef;

public class StatisticsServiceImpl implements StatisticsService {
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private JdbcTemplate jdbcTemplate;
    private List<DocumentType> docTypes = getDocumentAdminService().getTypes(DocumentType.class, DocumentAdminService.DONT_INCLUDE_CHILDREN);

	public Integer getTaskCount(Date startDate, Date endDate, String orgUnit, StatisticType type, Integer level) {
	    return getStatisticsCount(startDate, endDate, orgUnit, Statistic.TASK, type, level, "wfc_owner_organization_name");
	}
	
	public String getTranslatedDocTypeMessage(String docId) {
		String translatedDocTypeName = MessageUtil.getMessage(String.format("document_type_%s", docId));
		if (translatedDocTypeName.contains("$")) translatedDocTypeName = findDocTypeName(docTypes, docId);
		return translatedDocTypeName;
	}
	
	public Integer getWorkflowCount(Date startDate, Date endDate, String orgUnit, StatisticType type, Integer level) {
	    return getStatisticsCount(startDate, endDate, orgUnit, Statistic.WORKFLOW, type, level, "wfs_searchable_compound_workflow_owner_organization_name");
	}
	
    public List<ArrayList<Object>> searchTaskRefsByDepartment(Date startDate, Date endDate, String orgUnit, StatisticType type, Integer level) {
        String baseQuery = getTaskStatisticsBaseQueryByType(type);
        Pair<String, List<Object>> conditionByArgs = setupQueryConditionAndArguments(type, baseQuery, orgUnit, level, startDate, endDate, "wfc_owner_organization_name");
        Pair<List<ArrayList<Object>>, Boolean> results = BeanHelper.getWorkflowDbService().searchTaskDetailsStatistics(conditionByArgs.getFirst(), null, conditionByArgs.getSecond(), -1);
        return results.getFirst();
    }
    
    public List<ArrayList<Object>> searchWorkflowRefsByDepartment(Date startDate, Date endDate, String orgUnit, StatisticType type, Integer level) {
        String baseQuery = getWorkflowStatisticsBaseQueryByType(type);
        Pair<String, List<Object>> conditionByArgs = setupQueryConditionAndArguments(type, baseQuery, orgUnit, level, startDate, endDate, "wfs_searchable_compound_workflow_owner_organization_name");
        Pair<List<ArrayList<Object>>, Boolean> results = BeanHelper.getWorkflowDbService().searchWorkflowDetailsStatistics(conditionByArgs.getFirst(), conditionByArgs.getSecond(), -1);
        return results.getFirst();
    }
    
    public Map<Integer, StoreRef> storeIdByRefs() {
    	String query = "SELECT id, protocol, identifier FROM public.alf_store";
    	List<List<Object>> params = jdbcTemplate.query(query, new ParameterizedRowMapper<List<Object>>() {

            @Override
            public List<Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
            	List<Object> result = new ArrayList<>();
            	result.add(rs.getInt("id"));
            	result.add(rs.getString("protocol"));
            	result.add(rs.getString("identifier"));
                return result;
            }
        });
    	
    	Map<Integer, StoreRef> storeIdByRefs = new HashMap<>();
    	for (List<Object> param : params) {
    		Integer id = (Integer) param.get(0);
    		StoreRef ref = new StoreRef((String) param.get(1), (String) param.get(2));
    		storeIdByRefs.put(id, ref);
    	}
		return storeIdByRefs;
    }
    
	public Integer getDocumentCount(Date startDate, Date endDate, String orgUnit, Integer level) {
		String baseQuery = "SELECT COUNT(*) FROM (SELECT distinct id, MAX (string_value) FROM public.alf_node JOIN alf_node_properties ON node_id = alf_node.id WHERE qname_id = (SELECT id from alf_qname where local_name = 'ownerOrgStructUnit')	AND alf_node.id IN (SELECT alf_node.id FROM alf_node JOIN alf_node_properties ON node_id = alf_node.id JOIN alf_qname ON qname_id = alf_qname.id where type_qname_id = (SELECT id from alf_qname where local_name = 'document') AND local_name = 'regDateTime' AND ( string_value >= ? ) AND ( string_value <= ? )) GROUP BY id) as orgs";
		if (isOutsideDepartment(orgUnit)) {
			Object[] args = { startDate, endDate };
			String query = baseQuery + " WHERE (( max IS NULL ) OR ( max = '{NULL}' ) OR ( max = '{}'))";
			return jdbcTemplate.queryForObject(query, args, Integer.class);
		} else if (isUnspecifiedDepartment(orgUnit)) {
			Object[] args = { startDate, endDate };
			String query = baseQuery + getSimpleQueryOrgLevelCheck(level);
			return jdbcTemplate.queryForObject(query, args, Integer.class);
		} else {
			Object[] args = { startDate, endDate, "%" + orgUnit + "%" };
			String query = baseQuery + " WHERE max LIKE ?";
			return jdbcTemplate.queryForObject(query, args, Integer.class);
		}
	}
	
	public List<Pair<String, Integer>> searchDocumentTypeByRegDocCount(Date startDate, Date endDate, String departmentName, Integer level) {
		String departmentQueryPart = null;
		List<Object> args = new ArrayList<>();
		args.add(startDate);
		args.add(endDate);
		if (isOutsideDepartment(departmentName)) {
			departmentQueryPart = " WHERE (( max IS NULL ) OR ( max = '{NULL}' ) OR ( max = '{}'))";
		} else if (isUnspecifiedDepartment(departmentName)) {
			departmentQueryPart = getSimpleQueryOrgLevelCheck(level);
		} else {
			departmentQueryPart = " WHERE max LIKE ?";
			args.add("%" + departmentName + "%");
		}
        String sqlQuery = String.format("SELECT string_value as docType, count(*) as count FROM (SELECT distinct id, MAX (string_value) FROM public.alf_node JOIN alf_node_properties ON node_id = alf_node.id WHERE qname_id = (SELECT id from alf_qname where local_name = 'ownerOrgStructUnit')	AND alf_node.id IN (SELECT alf_node.id FROM alf_node JOIN alf_node_properties ON node_id = alf_node.id JOIN alf_qname ON qname_id = alf_qname.id where type_qname_id = (SELECT id from alf_qname where local_name = 'document') AND local_name = 'regDateTime' AND ( string_value >= ? ) AND ( string_value <= ? )) GROUP BY id) as maxOrganization JOIN alf_node_properties ON id = node_id %s AND qname_id = (SELECT id FROM public.alf_qname where local_name = 'objectTypeId') GROUP BY string_value",
        		departmentQueryPart);
        List<Pair<String, Integer>> docTypesCount = jdbcTemplate.query(sqlQuery, new ParameterizedRowMapper<Pair<String, Integer>>() {

            @Override
            public Pair<String, Integer> mapRow(ResultSet rs, int rowNum) throws SQLException {
            	String docType = String.valueOf(rs.getObject("doctype"));
            	Integer count = (int) ((long) rs.getObject("count"));
            	return new Pair<String, Integer>(docType, count);
            }

        }, args.toArray());
        return docTypesCount;
	}
	
	public List<Node> searchDocumentByType(Date startDate, Date endDate, String departmentName, String docType, Integer level) {
		String departmentQueryPart = null;
		List<Object> args = new ArrayList<>();
		args.add(startDate);
		args.add(endDate);
		if (isOutsideDepartment(departmentName)) {
			departmentQueryPart = " WHERE (( max IS NULL ) OR ( max = '{NULL}' ) OR ( max = '{}'))";
		} else if (isUnspecifiedDepartment(departmentName)) {
			departmentQueryPart = getSimpleQueryOrgLevelCheck(level);
		} else {
			departmentQueryPart = " WHERE max LIKE ?";
			args.add("%" + departmentName + "%");
		}
		args.add(docType);
        String sqlQuery = String.format("SELECT store_id, uuid FROM alf_node WHERE id in (SELECT id FROM (SELECT distinct id, MAX (string_value) FROM public.alf_node JOIN alf_node_properties ON node_id = alf_node.id WHERE qname_id = (SELECT id from alf_qname where local_name = 'ownerOrgStructUnit')	AND alf_node.id IN (SELECT alf_node.id FROM alf_node JOIN alf_node_properties ON node_id = alf_node.id JOIN alf_qname ON qname_id = alf_qname.id where type_qname_id = (SELECT id from alf_qname where local_name = 'document') AND local_name = 'regDateTime' AND ( string_value >= ? ) AND ( string_value <= ? )) GROUP BY id) as maxOrganization JOIN alf_node_properties ON id = node_id %s AND qname_id = (SELECT id FROM public.alf_qname where local_name = 'objectTypeId') AND string_value = ? )",
        		departmentQueryPart);
        List<Node> docNodes = jdbcTemplate.query(sqlQuery, new ParameterizedRowMapper<Node>() {

            @Override
            public Node mapRow(ResultSet rs, int rowNum) throws SQLException {
            	return new Node(getNodeRef(rs, "uuid"));
            }

        }, args.toArray());
        return docNodes;
	}
	
    private NodeRef getNodeRef(ResultSet rs, String nodeUuidName) throws SQLException {
        String nodeUuid = rs.getString(nodeUuidName);
        StoreRef storeRef = BeanHelper.getBulkLoadNodeService().getStoreRefByDbId(rs.getLong("store_id"));
        return new NodeRef(storeRef, nodeUuid);
    }

	private boolean isOutsideDepartment(String departmentName) {
		return departmentName.isEmpty() || departmentName.equals(MessageUtil.getMessage("statistics_text_outside"));
	}
	
	private boolean isUnspecifiedDepartment(String departmentName) {
		return departmentName.equals(MessageUtil.getMessage("statistics_text_unspecified"));
	}
	
	private String getSimpleQueryOrgLevelCheck(int level) {
		String result = "";
		if (level > 1) {
			String mark = "%,%";
			for (int i = 2; i < level; i++) {
				mark = mark + ",%";
			}
			result = String.format("%s WHERE max NOT LIKE '%s'", result, mark);
			if (level > 2) result = String.format("%s AND max LIKE '%s'", result, mark.substring(0, mark.length() - 2));
		}
		return result;
	}

	public List<String> getTaskOwnerOrgs(Date from, Date to) {
		String query = "SELECT DISTINCT wfc_owner_organization_name	FROM delta_task WHERE ( wfc_started_date_time >= ? ) AND ( wfc_started_date_time <= ? );";
		Object[] args = {from, to};
		return getFirstStringRowFromQuery(query, args);
	}
	
	public List<String> getWorkflowOwnerOrgs(Date from, Date to) {
		String query = "SELECT DISTINCT wfs_searchable_compound_workflow_owner_organization_name FROM"
				+ " (SELECT DISTINCT wfs_compound_workflow_id, wfs_searchable_compound_workflow_owner_organization_name FROM delta_task"
				+ " WHERE ( wfs_searchable_compound_workflow_started_date_time >= ? ) AND ( wfs_searchable_compound_workflow_started_date_time <= ? )) as workflow;";
		Object[] args = {from, to};
		return getFirstStringRowFromQuery(query, args);
	}
	
	public Date getCurrentDateAtStartOfDay() {
	        Calendar cal = Calendar.getInstance();  
	        cal.setTime(new Date());  
	        cal.set(Calendar.HOUR_OF_DAY, 0);  
	        cal.set(Calendar.MINUTE, 0);  
	        cal.set(Calendar.SECOND, 0);  
	        cal.set(Calendar.MILLISECOND, 0);  
	        return cal.getTime(); 
	}
	
    public String getCurrentTimeZoneId() {
    	Calendar now = Calendar.getInstance();
        TimeZone timeZone = now.getTimeZone();
        return timeZone.getID();
    }
	
	public List<String> getDocumentOwnerOrgs(Date from, Date to) {
		String query = "SELECT DISTINCT MAX(string_value) FROM public.alf_node JOIN alf_node_properties ON node_id = alf_node.id "
				+ "WHERE qname_id = (SELECT id from alf_qname where local_name = 'ownerOrgStructUnit') "
				+ "AND alf_node.id IN (SELECT alf_node.id FROM alf_node JOIN alf_node_properties ON node_id = alf_node.id JOIN alf_qname ON qname_id = alf_qname.id "
				+ "WHERE type_qname_id = (SELECT id from alf_qname where local_name = 'document') "
				+ "AND local_name = 'regDateTime' AND ( string_value >= ? ) AND ( string_value <= ? )) GROUP BY id";
		Object[] args = {from, to};
		List<String> org = jdbcTemplate.query(query, new ParameterizedRowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        }, args);
		return org;
	}

	public Pair<Date, Date> getStatisticsPeriod(String startDateStr, String endDateStr) {
    	Pair<Date, Date> period;
    	try {
    		period = parseStartAndEndDates(startDateStr, endDateStr);
    	} catch (ParseException e) {
    		startDateStr = getDateStrBeforeSevenDays();
    		endDateStr = dateFormat.format(new Date());
    		final String msg = MessageUtil.getMessage(FacesContext.getCurrentInstance(), "validation_date_failed", "Periood");
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    	}
    	return period;
	}
	
	public String formatSimpleOrgName(String org, int unitLevel) {
		final String unspecified = MessageUtil.getMessage("statistics_text_unspecified");
		if (org == null || org.isEmpty()){
			return MessageUtil.getMessage("statistics_text_outside");
		} else if (!org.contains(",")) {
			if (unitLevel == 1) {
				return org;
			} else {
				return unspecified;
			}
		} else {
			int levelIndex = unitLevel - 1;
			String[] splited = org.split(", ");
			if (splited.length > levelIndex) {
				return splited[levelIndex];
			} else {
				return unspecified;
			}
		}
	}

    public String formatTaskOrWorkflowOrgName(String org, int unitLevel) {
    	String result = "";
		int levelIndex = unitLevel - 1;
        if (org == null || org.equals("{}") || org.equals("{NULL}")){
			return MessageUtil.getMessage("statistics_text_outside");
		} else {
			final String unspecified = MessageUtil.getMessage("statistics_text_unspecified");
			String[] splited = org.split(",\"");
			if (splited.length > 1 && splited.length > levelIndex) {
				result = splited[levelIndex];
				splited = result.split(", ");
				if (splited.length > levelIndex) {
					result = splited[levelIndex];
				} else {
					return unspecified;
				}
			} else if (org.startsWith("{") && org.endsWith("}") && org.length() > 2 && !org.contains(",")) {
				if (levelIndex == 0) {
					return org.replace("{", "").replace("}", "").replace("\"", "");
				} else {
					return unspecified;
				}
			} else {
				return unspecified;
			}
			result = result.replace("\"}", "");
			result = result.replace("\"", "");
			result = result.replace("{", "");
			return result;
		}
    }

	public Pair<Date, Date> parseStartAndEndDates(String startDateStr, String endDateStr) throws ParseException{
    	Date from = dateFormat.parse(startDateStr);
    	Date to = getDateWithMaxTime(dateFormat.parse(endDateStr));
        return new Pair<Date, Date>(from, to);
	}

    public Date getDateAfterSevenDays() {
    	Calendar seven = Calendar.getInstance();
        seven.add(Calendar.DAY_OF_MONTH, 7);
        return seven.getTime();
    }

    public Date getDateWithMaxTime(Date date) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	calendar.set(Calendar.HOUR_OF_DAY, 23);
    	calendar.set(Calendar.MINUTE, 59);
    	calendar.set(Calendar.SECOND, 59);
    	return calendar.getTime();
    }

    public Long datesDiffrenceInDays(Date startDate, Date endDate) {
    	long diffInMillies = Math.abs(startDate.getTime() - endDate.getTime());
    	return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }
    
	private List<String> getFirstStringRowFromQuery(String query, Object[] args) {
		List<String> orgNames = jdbcTemplate.query(query, new ParameterizedRowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        }, args);
		return orgNames;
	}
    
	private Integer getStatisticsCount(Date startDate, Date endDate, String orgUnit, Statistic statistic, StatisticType type, Integer level, String ownerNameColumn) {
		String baseQuery = getStatisticCountBaseQuery(statistic, type);
		Pair<String, List<Object>> conditionByArgs = setupQueryConditionAndArguments(type, baseQuery, orgUnit, level, startDate, endDate, ownerNameColumn);
	    if (statistic.equals(Statistic.TASK)) return BeanHelper.getWorkflowDbService().getTaskDetailsStatisticsCount(conditionByArgs.getFirst(), conditionByArgs.getSecond(), -1);
	    if (statistic.equals(Statistic.WORKFLOW)) return BeanHelper.getWorkflowDbService().getWorkflowDetailsStatisticsCount(conditionByArgs.getFirst(), conditionByArgs.getSecond(), -1);
	    return null;
	}
    
    private Pair<String, List<Object>> setupQueryConditionAndArguments(StatisticType type, String baseQuery, String orgUnit, int level, Date startDate, Date endDate, String ownerOrgColumn){
        String queryConditions = String.format("%s AND ( %s::text LIKE ? )", baseQuery, ownerOrgColumn);       
        if (isOutsideDepartment(orgUnit)) {
        	queryConditions = String.format("%s AND (( %s::text = ? ) "
        			+ "OR ( %s IS null ) OR ( %s::text = '{NULL}'))", baseQuery, ownerOrgColumn, ownerOrgColumn, ownerOrgColumn);
        	orgUnit = "{}";
        } else if (isUnspecifiedDepartment(orgUnit)) {
        	if (level >= 2) {
        		queryConditions = String.format("%s AND ( %s::text NOT LIKE ? )", baseQuery, ownerOrgColumn);
        		queryConditions = setupUnspecifiedQuery(queryConditions, level);
        	}
        } else {
        	orgUnit = String.format("%%%s%%", orgUnit);
        }
        List<Object> arguments = getStatisticsQueryArgumentsByType(type, startDate, endDate, orgUnit);
    	return new Pair<String, List<Object>>(queryConditions, arguments);
    }
    
	private String getStatisticCountBaseQuery(Statistic statistic, StatisticType type) {
		String baseQuery = null;
		if (statistic.equals(Statistic.TASK)) {
			baseQuery = getTaskStatisticsBaseQueryByType(type);
		} else if (statistic.equals(Statistic.WORKFLOW)) {
			baseQuery = getWorkflowStatisticsBaseQueryByType(type);
		}
		return baseQuery;
	}
    
    private String getTaskStatisticsBaseQueryByType(StatisticType type) {
    	switch (type) {
        case INCOMING:
            return "( wfc_started_date_time >= ? ) AND ( wfc_started_date_time <= ? ) AND ( wfc_status != ? )";
        case OVERDUE:
            return "( wfc_started_date_time >= ? ) AND ( wfc_started_date_time <= ? ) AND ((( wfc_completed_date_time IS null ) AND ( wfs_due_date <= now() )) OR (( wfc_completed_date_time IS NOT null ) AND ( wfs_due_date <= wfc_completed_date_time )))";
        case FINISHED:
            return "( wfc_started_date_time >= ? ) AND ( wfc_started_date_time <= ? ) AND ( wfc_status=? ) AND ( wfc_completed_date_time IS NOT null) AND ( wfc_completed_date_time <= wfs_due_date )";
        case NEXT_SEVEN_DAYS:
            return "( wfc_started_date_time >= ? ) AND ( wfc_started_date_time <= ? ) AND (( wfc_status=? ) OR ( wfc_status=? )) AND ( wfs_due_date <= ? ) AND ( wfs_due_date >= ? )";
    	default:
            new RuntimeException("Unknown statistic type");
    	}
    	return null;
    }
    
    private String getWorkflowStatisticsBaseQueryByType(StatisticType type) {
    	switch (type) {
        case INCOMING:
            return "( wfs_searchable_compound_workflow_started_date_time >= ? ) AND ( wfs_searchable_compound_workflow_started_date_time <= ? ) AND ( wfs_searchable_compound_workflow_status != ? )";
        case OVERDUE:
            return "( wfs_searchable_compound_workflow_started_date_time >= ? ) AND ( wfs_searchable_compound_workflow_started_date_time <= ? ) AND ((( wfs_searchable_compound_workflow_finished_date_time IS null ) AND ( wfs_due_date <= now() )) OR (( wfs_searchable_compound_workflow_finished_date_time IS NOT null ) AND ( wfs_due_date <= wfs_searchable_compound_workflow_finished_date_time )))";
        case FINISHED:
            return "( wfs_searchable_compound_workflow_started_date_time >= ? ) AND ( wfs_searchable_compound_workflow_started_date_time <= ? ) AND ( wfs_searchable_compound_workflow_status=? ) AND ( wfs_searchable_compound_workflow_finished_date_time IS NOT null) AND ( wfs_searchable_compound_workflow_finished_date_time <= wfs_due_date )";
        case NEXT_SEVEN_DAYS:
            return "( wfs_searchable_compound_workflow_started_date_time >= ? ) AND ( wfs_searchable_compound_workflow_started_date_time <= ? ) AND (( wfs_searchable_compound_workflow_status=? ) OR ( wfs_searchable_compound_workflow_status=? )) AND ( wfs_due_date <= ? ) AND ( wfs_due_date >= ? )";
    	default:
            new RuntimeException("Unknown statistic type");
    	}
    	return null;
    }
    
    private List<Object> getStatisticsQueryArgumentsByType(StatisticType type, Date startDate, Date endDate, String deparmentName) {
        List<Object> arguments = new ArrayList<>();
    	switch (type) {
        case INCOMING:
        	arguments.add(startDate);
        	arguments.add(endDate);
        	arguments.add(Status.NEW.getName());
            break;
        case OVERDUE:
        	arguments.add(startDate);
        	arguments.add(endDate);
            break;
        case FINISHED:
        	arguments.add(startDate);
        	arguments.add(endDate);
        	arguments.add(Status.FINISHED.getName());
            break;
        case NEXT_SEVEN_DAYS:
            arguments.add(startDate);
        	arguments.add(endDate);
        	arguments.add(Status.IN_PROGRESS.getName());
        	arguments.add(Status.STOPPED.getName());
            Calendar seven = Calendar.getInstance();
            seven.add(Calendar.DAY_OF_MONTH, 7);
            arguments.add(seven.getTime());
            arguments.add(new Date());
            break;
    	default:
            new RuntimeException("Unknown statistic type");
    	}
    	arguments.add(deparmentName);
    	return arguments;
    }

    private String getDateStrBeforeSevenDays() {
    	Calendar seven = Calendar.getInstance();
        seven.add(Calendar.DAY_OF_MONTH, -7);
        seven.set(Calendar.HOUR_OF_DAY, 0);
        seven.set(Calendar.MINUTE, 0);
        seven.set(Calendar.SECOND, 0);
        return dateFormat.format(seven.getTime());
    }
    
    private String setupUnspecifiedQuery(String query, int level) {
		query = String.format("%s AND ( ( wfc_owner_organization_name::text LIKE '{%%}' ) AND ( wfc_owner_organization_name::text NOT LIKE '%%,\"%%' )", query);
    	String unspecifiedPart = "( wfc_owner_organization_name::text LIKE '%%%s%%' ) AND ( wfc_owner_organization_name::text NOT LIKE '%%%s%%' )";
    	for (int i = 3; i < level + 1; i++) {
    		query = String.format("%s OR %s", query, String.format(unspecifiedPart, setUpLevelMarker(i - 2), setUpLevelMarker(i - 1)));
    	}
    	return query + ")";
    }
    
	private String findDocTypeName(List<DocumentType> allDocTypes, String docId) {
		for (DocumentType docType : allDocTypes) {
			if (docType.getId().equals(docId)) return docType.getName();
		}
		return null;
	}
	
    private String setUpLevelMarker(int markSize) {
    	String marker = ",\"";
    	String result = marker;
    	for (int i = 1; i < markSize; i++) {
    		result = result + "%%" + marker;
    	}
    	return result;
    }

	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

	public List<DocumentType> getDocTypes() {
		return docTypes;
	}

	public void setDocTypes(List<DocumentType> docTypes) {
		this.docTypes = docTypes;
	}
}
