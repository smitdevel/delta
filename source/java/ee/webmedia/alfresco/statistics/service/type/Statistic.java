package ee.webmedia.alfresco.statistics.service.type;

public enum Statistic {
	TASK, WORKFLOW
}
