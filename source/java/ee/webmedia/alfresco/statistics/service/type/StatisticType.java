package ee.webmedia.alfresco.statistics.service.type;

public enum StatisticType {
	INCOMING, FINISHED, OVERDUE, NEXT_SEVEN_DAYS
}
