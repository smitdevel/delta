package ee.webmedia.alfresco.statistics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.alfresco.util.Pair;

import ee.webmedia.alfresco.statistics.service.type.StatisticType;
import ee.webmedia.alfresco.statistics.web.StatisticsListDialog;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.service.cmr.repository.StoreRef;

public interface StatisticsService {
	String BEAN_NAME = "StatisticsService";
		
	Pair<Date, Date> parseStartAndEndDates(String startDateStr, String endDateStr) throws ParseException;
	
	SimpleDateFormat getDateFormat();
	
    Date getDateAfterSevenDays();
        
    Long datesDiffrenceInDays(Date startDate, Date endDate);
    
    Pair<Date, Date> getStatisticsPeriod(String startDateStr, String endDateStr);
    
    String formatSimpleOrgName(String org, int unitLevel);

	String formatTaskOrWorkflowOrgName(String org, int unitLevel);
	
	List<String> getTaskOwnerOrgs(Date from, Date to);
	
	List<String> getWorkflowOwnerOrgs(Date from, Date to);
	
	List<String> getDocumentOwnerOrgs(Date from, Date to);
	
	Integer getTaskCount(Date startDate, Date endDate, String orgUnit, StatisticType type, Integer level);
	
	Integer getWorkflowCount(Date startDate, Date endDate, String orgUnit, StatisticType type, Integer level);
	
	Integer getDocumentCount(Date startDate, Date endDate, String orgUnit, Integer level);
	
	List<ArrayList<Object>> searchTaskRefsByDepartment(Date startDate, Date endDate, String deparmentName, StatisticType type, Integer level);

	List<ArrayList<Object>> searchWorkflowRefsByDepartment(Date startDate, Date endDate, String departmentName, StatisticType type, Integer level);
	
	Date getCurrentDateAtStartOfDay();
	
	String getCurrentTimeZoneId();
	
	Map<Integer, StoreRef> storeIdByRefs();
	
	List<Pair<String, Integer>> searchDocumentTypeByRegDocCount(Date startDate, Date endDate, String departmentName, Integer level);

	List<Node> searchDocumentByType(Date startDate, Date endDate, String departmentName, String doctype, Integer level);
	
	String getTranslatedDocTypeMessage(String docId);
}
