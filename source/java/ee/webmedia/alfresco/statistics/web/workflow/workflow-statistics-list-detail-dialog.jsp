<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="statistics-details-results-panel" styleClass="panel-100 with-pager" label="#{WorkflowStatisticsListDetailDialog.departmentName}" progressive="true">
   <a:richList id="statisticsDetailList" viewMode="details" pageSize="#{BrowseBean.pageSizeContent}" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt" width="100%"
      value="#{WorkflowStatisticsListDetailDialog.departmentOverview}" var="r" refreshOnBind="true" >

      <a:column id="col1">
         <f:facet name="header">
            <a:sortLink id="col1-sort" label="#{msg.statistics_text_doc_title}" value="title" styleClass="header" />
         </f:facet>
         <a:actionLink id="col1-act" value="#{r.title}" action="dialog:compoundWorkflowDialog" actionListener="#{CompoundWorkflowDialog.setupWorkflowFromList}" >
            <f:param name="nodeRef" value="#{r.wfNodeRef}" />
         </a:actionLink>
      </a:column>
      
      <a:column id="col2">
         <f:facet name="header">
            <a:sortLink id="col2-sort" label="#{msg.statistics_text_owner}" value="ownerName" styleClass="header" />
         </f:facet>
         <h:outputText id="col2-text" value="#{r.ownerName}" />
      </a:column>
      
      <a:column id="col3" primary="true">
         <f:facet name="header">
            <a:sortLink id="col3-sort" label="#{msg.statistics_text_start_date}" value="startedDate" styleClass="header" />
         </f:facet>
         <h:outputText id="col3-text" value="#{r.startedDate}" >
         	<f:convertDateTime pattern="dd.MM.yyyy" timeZone="#{WorkflowStatisticsListDetailDialog.currentTimeZoneId}" />
         </h:outputText>
      </a:column>

      <a:column id="col4">
         <f:facet name="header">
            <a:sortLink id="col4-sort" label="#{msg.statistics_text_due_date}" value="dueDate" styleClass="header" />
         </f:facet>
         <h:outputText id="col4-text" value="#{r.dueDate}" >
         	<f:convertDateTime pattern="dd.MM.yyyy" timeZone="#{WorkflowStatisticsListDetailDialog.currentTimeZoneId}" />
         </h:outputText>
      </a:column>

      <a:column id="col5">
         <f:facet name="header">
            <a:sortLink id="col5-sort" label="#{msg.statistics_text_status}" value="status" styleClass="header" />
         </f:facet>
         <h:outputText id="col5-text" value="#{r.status}" />
      </a:column>
      
      <a:column id="col6">
         <f:facet name="header">
            <a:sortLink id="col6-sort" label="#{msg.statistics_text_doc_count}" value="docCount" styleClass="header" />
         </f:facet>
         <a:actionLink id="col3-text-doc" value="#{r.docCount}" action="#{DocumentDialog.action}" actionListener="#{DocumentDialog.open}" rendered="#{r.documentWorkflow}">
            <f:param name="nodeRef" value="#{r.docRef}" />
         </a:actionLink>
         <h:outputText id="col6-text" value="#{r.docCount}"  rendered="#{!r.documentWorkflow}" />
      </a:column>
    
      <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/page-size.jsp" />
      <a:dataPager id="pager1" styleClass="pager" />
   </a:richList>

   <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/disable-dialog-finish-button.jsp" />
</a:panel>