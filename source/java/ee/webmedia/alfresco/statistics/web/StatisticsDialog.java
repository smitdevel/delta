package ee.webmedia.alfresco.statistics.web;

import java.util.Date;
import java.util.List;

public interface StatisticsDialog {

	public List<String> getOwnerOrgs(Date startDate, Date endDate);
	
	public String formatOrgName(String org, int unitLevel);

}
