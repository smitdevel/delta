package ee.webmedia.alfresco.statistics.web.task;

import static ee.webmedia.alfresco.common.web.BeanHelper.getClassificatorService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getHelpTextService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowConstantsBean;
import static ee.webmedia.alfresco.common.propertysheet.datepicker.DatePickerWithDueDateGenerator.createDueDateDaysSelector;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIOutput;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.el.ValueBinding;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.alfresco.util.Pair;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Node;

import ee.webmedia.alfresco.classificator.enums.DatePeriods;
import ee.webmedia.alfresco.classificator.model.Classificator;
import ee.webmedia.alfresco.classificator.model.ClassificatorValue;
import ee.webmedia.alfresco.common.propertysheet.datepicker.DatePickerGenerator;
import ee.webmedia.alfresco.common.propertysheet.datepicker.DatePickerWithDueDateGenerator;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.help.model.HelpText;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.statistics.model.StatisticModel;
import ee.webmedia.alfresco.statistics.service.StatisticsService;
import ee.webmedia.alfresco.statistics.service.type.StatisticType;
import ee.webmedia.alfresco.statistics.web.StatisticsListDialog;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel;
import ee.webmedia.alfresco.workflow.web.CompoundWorkflowDialog;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.ui.common.ComponentConstants;
import org.alfresco.web.ui.common.Utils;
import org.alfresco.web.ui.common.tag.PanelTag;

public class TaskStatisticsListDialog extends StatisticsListDialog {
        
    public List<StatisticModel> getStatisticsOverview() {
    	Pair<Date, Date> period = statisticsService.getStatisticsPeriod(startDateStr, endDateStr);
        return getStatistics(period.getFirst(), period.getSecond());
    }
    
    private List<StatisticModel> getStatistics(Date from, Date to) {
    	List<String> orgs = getOwnerOrgs(from, to);
    	Set<String> orgsParsedByLevel = parseOrgsByLevel(orgs, getUnitLevel());
    	List<StatisticModel> model = new ArrayList<>();
    	for (String orgUnit : orgsParsedByLevel) {
    		model.add(new StatisticModel(orgUnit,
    				statisticsService.getTaskCount(from, to, orgUnit, StatisticType.INCOMING, unitLevel), 
    				statisticsService.getTaskCount(from, to, orgUnit, StatisticType.FINISHED, unitLevel), 
    				statisticsService.getTaskCount(from, to, orgUnit, StatisticType.OVERDUE, unitLevel), 
    				statisticsService.getTaskCount(from, to, orgUnit, StatisticType.NEXT_SEVEN_DAYS, unitLevel)));
    	}
    	return model;
    }
	
    @Override
    public String getContainerTitle() {
    	return MessageUtil.getMessage("statistics_tasks_overview");
    }

	@Override
	public List<String> getOwnerOrgs(Date startDate, Date endDate) {
		return statisticsService.getTaskOwnerOrgs(startDate, endDate);
	}

	@Override
	public String formatOrgName(String department, int unitLevel) {
		return statisticsService.formatTaskOrWorkflowOrgName(department, unitLevel);
	}
	
}
