package ee.webmedia.alfresco.statistics.web.task;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.event.ActionEvent;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.component.UIActionLink;
import org.apache.commons.lang.StringUtils;

import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.common.service.BulkLoadNodeService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.functions.model.FunctionsModel;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.series.model.SeriesModel;
import ee.webmedia.alfresco.statistics.model.task.TaskStatDetailModel;
import ee.webmedia.alfresco.statistics.service.StatisticsService;
import ee.webmedia.alfresco.statistics.service.type.StatisticType;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;
import ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel;

public class TaskStatisticsListDetailDialog extends BaseDialogBean {
	
	private transient StatisticsService statisticsService;
	
	private String departmentName;
	private StatisticType type;
	private Date startDate;
	private Date endDate;
	private Integer level;

    public List<TaskStatDetailModel> getDepartmentOverview() {
		ArrayList<TaskStatDetailModel> departmentsStatisticsRows = new ArrayList<TaskStatDetailModel>();
		List<ArrayList<Object>> result = statisticsService.searchTaskRefsByDepartment(startDate, endDate, departmentName, type, level);
		for(ArrayList<Object> params : result) {
			NodeRef wfNodeRef = (NodeRef) params.get(0);
			String title = (String) params.get(1);
			String taskType = (String) params.get(2);
			String creatorName = (String) params.get(3);
			String ownerName = (String) params.get(4);
			Date startedDate = (Date) params.get(5);
			Date dueDate = (Date) params.get(6);
			Date completedDate = (Date) params.get(7);
			String status = (String) params.get(8);
			if (taskType.equals(WorkflowSpecificModel.Types.GROUP_ASSIGNMENT_TASK.getLocalName())) {
				ownerName = (String) params.get(9);
			}
			departmentsStatisticsRows.add(new TaskStatDetailModel(wfNodeRef, departmentName, title, taskType,
					creatorName, ownerName, startedDate, dueDate, completedDate, status));
		}
		return departmentsStatisticsRows;
    }
    
	@Override
	protected String finishImpl(FacesContext context, String outcome) throws Throwable {
		return null;
	}
	
    @Override
    public String cancel() {
        return super.cancel();
    }
    
    public String getCurrentTimeZoneId() {
    	return statisticsService.getCurrentTimeZoneId();
    }

    @Override
    public String getCancelButtonLabel() {
        return MessageUtil.getMessage(FacesContext.getCurrentInstance(), "back_button");
    }
    
    public void setupDepartmentView(ActionEvent event) {
    	UIActionLink link = (UIActionLink) event.getComponent();
        Map<String, String> params = link.getParameterMap();
        this.departmentName = (String) params.get("departmentName");
        String type = params.get("type");
        StatisticType.valueOf(type);
        this.type = StatisticType.valueOf(type);
        Pair<Date, Date> period;
    	try {
    		period = statisticsService.parseStartAndEndDates((String) params.get("startDate"), (String) params.get("endDate"));
    	} catch (ParseException e) {
    		final String msg = MessageUtil.getMessage(FacesContext.getCurrentInstance(), "validation_date_failed", "Periood");
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    	}
        this.startDate = period.getFirst();
        this.endDate = period.getSecond();
        this.level = Integer.valueOf(params.get("level"));
    }
    
	@Override
    public String getContainerTitle()
    {
		SimpleDateFormat dateFormat = statisticsService.getDateFormat();
		switch (type) {
        case INCOMING:
        	return MessageUtil.getMessage("statistics_tasks_incoming_overview", dateFormat.format(startDate), dateFormat.format(endDate));
        case FINISHED:
        	return MessageUtil.getMessage("statistics_tasks_answered_overview", dateFormat.format(startDate), dateFormat.format(endDate));
        case OVERDUE:
        	return MessageUtil.getMessage("statistics_tasks_out_of_time_overview", dateFormat.format(startDate), dateFormat.format(endDate));
        case NEXT_SEVEN_DAYS:
        	return MessageUtil.getMessage("statistics_tasks_out_next_seven_days_overview", dateFormat.format(startDate), dateFormat.format(endDate));
    	default:
            new RuntimeException("Unknown statistic type");
    	}
		return null;
    }

	private Integer getDifferenceDays(Date d1, Date d2) {
		if(d2 == null) return null;
		long time = TimeUnit.DAYS.convert(d2.getTime() - d1.getTime(), TimeUnit.MILLISECONDS);
		if(time > 0) return 0;
	    return (int) Math.abs(time);
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public StatisticType getType() {
		return type;
	}

	public void setType(StatisticType type) {
		this.type = type;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public StatisticsService getStatisticsService() {
		return statisticsService;
	}

	public void setStatisticsService(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}
}
