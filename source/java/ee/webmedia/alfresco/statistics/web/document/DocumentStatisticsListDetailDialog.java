package ee.webmedia.alfresco.statistics.web.document;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.event.ActionEvent;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.component.UIActionLink;
import org.apache.commons.lang.StringUtils;

import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.common.service.BulkLoadNodeService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.functions.model.FunctionsModel;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.series.model.SeriesModel;
import ee.webmedia.alfresco.statistics.model.document.DocumentDetailModel;
import ee.webmedia.alfresco.statistics.model.task.TaskStatDetailModel;
import ee.webmedia.alfresco.statistics.service.StatisticsService;
import ee.webmedia.alfresco.statistics.service.type.StatisticType;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;

public class DocumentStatisticsListDetailDialog extends BaseDialogBean {

	private transient StatisticsService statisticsService;
	
	private String departmentName;
	private String docType;
	private Date startDate;
	private Date endDate;
	private Integer level;
	
    public List<DocumentDetailModel> getDepartmentOverview() {
		ArrayList<DocumentDetailModel> departmentsStatisticsRows = new ArrayList<DocumentDetailModel>();
		List<Node> result = statisticsService.searchDocumentByType(startDate, endDate, departmentName, docType, level);
		for (Node node : result) {
			Map<String, Object> properties = node.getProperties();
			String title = (String) properties.get(DocumentCommonModel.Props.DOC_NAME);
			String regNumber = (String) properties.get(DocumentCommonModel.Props.REG_NUMBER);
			Date regDate = (Date) properties.get(DocumentCommonModel.Props.REG_DATE_TIME);
			String ownerName = (String) properties.get(DocumentCommonModel.Props.OWNER_NAME);
			departmentsStatisticsRows.add(new DocumentDetailModel(node.getNodeRef(), title, regNumber, regDate, ownerName));
		}
		return departmentsStatisticsRows;
    }

	@Override
	protected String finishImpl(FacesContext context, String outcome) throws Throwable {
		return null;
	}
	
    @Override
    public String cancel() {
        return super.cancel();
    }

    @Override
    public String getCancelButtonLabel() {
        return MessageUtil.getMessage(FacesContext.getCurrentInstance(), "back_button");
    }
    
    public void setupDepartmentView(ActionEvent event) {
    	UIActionLink link = (UIActionLink) event.getComponent();
        Map<String, String> params = link.getParameterMap();
        this.departmentName = (String) params.get("department");
        this.docType = params.get("type");
        Pair<Date, Date> period;
    	try {
    		period = statisticsService.parseStartAndEndDates((String) params.get("startDate"), (String) params.get("endDate"));
    	} catch (ParseException e) {
    		final String msg = MessageUtil.getMessage(FacesContext.getCurrentInstance(), "validation_date_failed", "Periood");
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    	}
        this.startDate = period.getFirst();
        this.endDate = period.getSecond();
        this.level = Integer.valueOf(params.get("level"));
    }
    
	@Override
    public String getContainerTitle()
    {
		SimpleDateFormat dateFormat = statisticsService.getDateFormat();
		String translatedDocTypeName = statisticsService.getTranslatedDocTypeMessage(docType);
    	return MessageUtil.getMessage("statistics_document_overview_by_type", translatedDocTypeName, departmentName, dateFormat.format(startDate), dateFormat.format(endDate));
    }

	private Integer getDifferenceDays(Date d1, Date d2) {
		if (d2 == null) return null;
		long time = TimeUnit.DAYS.convert(d2.getTime() - d1.getTime(), TimeUnit.MILLISECONDS);
		if (time > 0) return 0;
	    return (int) Math.abs(time);
	}
	
	public void setupDepartmentView() {};

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public StatisticsService getStatisticsService() {
		return statisticsService;
	}

	public void setStatisticsService(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}
	
}
