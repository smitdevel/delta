package ee.webmedia.alfresco.statistics.web.document;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentAdminService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.event.ActionEvent;

import org.alfresco.util.Pair;
import org.alfresco.web.bean.dialog.BaseDialogBean;

import ee.webmedia.alfresco.docadmin.service.DocumentAdminService;
import ee.webmedia.alfresco.docadmin.service.DocumentType;
import ee.webmedia.alfresco.statistics.model.document.DocumentStatisticModel;
import ee.webmedia.alfresco.statistics.model.document.DocumentTypeModel;
import ee.webmedia.alfresco.statistics.model.task.TaskStatDetailModel;
import ee.webmedia.alfresco.statistics.service.StatisticsService;
import ee.webmedia.alfresco.statistics.service.type.StatisticType;
import ee.webmedia.alfresco.utils.MessageUtil;
import org.alfresco.web.ui.common.component.UIActionLink;


public class DocumentStatisticsByTypeDialog extends BaseDialogBean {

	private transient StatisticsService statisticsService;

	private String departmentName;
	private Date startDate;
	private Date endDate;
	private Integer level;
	
    public List<DocumentTypeModel> getDepartmentOverview() {
    	List<DocumentTypeModel> statistics = new ArrayList<>();
		List<Pair<String, Integer>> docTypesByCount = statisticsService.searchDocumentTypeByRegDocCount(startDate, endDate, departmentName, level);
		for (Pair<String, Integer> docTypeByCount: docTypesByCount) {
			String docId = docTypeByCount.getFirst();
			String translatedDocTypeName = statisticsService.getTranslatedDocTypeMessage(docId);
			statistics.add(new DocumentTypeModel(translatedDocTypeName, docId, docTypeByCount.getSecond()));
		}
		return statistics;
    }
	
    public void setupDepartmentView(ActionEvent event) {
    	UIActionLink link = (UIActionLink) event.getComponent();
        Map<String, String> params = link.getParameterMap();
        this.departmentName = (String) params.get("department");
        Pair<Date, Date> period;
    	try {
    		period = statisticsService.parseStartAndEndDates((String) params.get("startDate"), (String) params.get("endDate"));
    	} catch (ParseException e) {
    		final String msg = MessageUtil.getMessage(FacesContext.getCurrentInstance(), "validation_date_failed", "Periood");
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    	}
        this.startDate = period.getFirst();
        this.endDate = period.getSecond();
        this.level = Integer.valueOf(params.get("level"));
    }
    
    @Override
    public String getContainerTitle() {
		SimpleDateFormat dateFormat = statisticsService.getDateFormat();
    	return MessageUtil.getMessage("statistics_document_overview_by_org", departmentName, dateFormat.format(startDate), dateFormat.format(endDate));
    }
    
	@Override
	protected String finishImpl(FacesContext context, String outcome) throws Throwable {
		return null;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public StatisticsService getStatisticsService() {
		return statisticsService;
	}

	public void setStatisticsService(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}

}
