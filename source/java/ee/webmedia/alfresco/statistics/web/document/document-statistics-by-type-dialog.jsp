<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="statistics-details-results-panel" styleClass="panel-100 with-pager" label="#{DocumentStatisticsByTypeDialog.departmentName}" progressive="true">
   <a:richList id="statisticsDetailList" viewMode="details" pageSize="#{BrowseBean.pageSizeContent}" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt" width="100%"
      value="#{DocumentStatisticsByTypeDialog.departmentOverview}" var="r" refreshOnBind="true" >

      <a:column id="col1">
         <f:facet name="header">
            <a:sortLink id="col1-sort" label="#{msg.statistics_text_doc_type}" value="department" styleClass="header" />
         </f:facet>
         <a:actionLink id="col1-act" value="#{r.docTypeName}" action="dialog:documentStatisticsListDetailDialog" actionListener="#{DocumentStatisticsListDetailDialog.setupDepartmentView}" >
            <f:param name="department" value="#{DocumentStatisticsByTypeDialog.departmentName}" />
	        <f:param name="startDate" value="#{DocumentStatisticsListDialog.startDateStr}" />
	        <f:param name="endDate" value="#{DocumentStatisticsListDialog.endDateStr}" />
	        <f:param name="level" value="#{DocumentStatisticsListDialog.unitLevel}" />
	        <f:param name="type" value="#{r.docType}" />
         </a:actionLink>
      </a:column>
      
      <a:column id="col2">
         <f:facet name="header">
            <a:sortLink id="col2-sort" label="#{msg.statistics_text_registred_doc_count}" value="registredDocumentsCount" styleClass="header" />
         </f:facet>
         <a:actionLink id="col2-act" value="#{r.registredDocumentsCount}" action="dialog:documentStatisticsListDetailDialog" actionListener="#{DocumentStatisticsListDetailDialog.setupDepartmentView}" >
            <f:param name="department" value="#{DocumentStatisticsByTypeDialog.departmentName}" />
	        <f:param name="startDate" value="#{DocumentStatisticsListDialog.startDateStr}" />
	        <f:param name="endDate" value="#{DocumentStatisticsListDialog.endDateStr}" />
	        <f:param name="level" value="#{DocumentStatisticsListDialog.unitLevel}" />
	        <f:param name="type" value="#{r.docType}" />
         </a:actionLink>
      </a:column>
      
      <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/page-size.jsp" />
      <a:dataPager id="pager1" styleClass="pager" />
   </a:richList>

   <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/disable-dialog-finish-button.jsp" />
</a:panel>