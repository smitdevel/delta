<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="statistics-settings" styleClass="panel-100" label="Statistika seaded" >
   <h:panelGrid columns="1" style="width: 100%;">   
      <h:panelGroup id="datePickerRow">
	    <a:outputText value="Periood: alates " style="padding: 2px" />
	    <h:inputText id="statistic_dueDate" styleClass="date" value="#{DocumentStatisticsListDialog.startDateStr}" maxlength="10" onchange="processButtonState();" />
	    <a:outputText id="to" value=" kuni " />
	    <h:inputText id="statistic_dueDate_EndDate" styleClass="date endDate" value="#{DocumentStatisticsListDialog.endDateStr}" maxlength="10" onchange="processButtonState();" />
	    <h:selectOneMenu id="statistic_dueDate_DateRangePicker" styleClass="quickDateRangePicker" style="margin-left: 5px;" onchange="setStatisticsDateFromEnumOnChange();">
            <f:selectItems value="#{DocumentStatisticsListDialog.datePeriods}" />
        </h:selectOneMenu>
	    <h:commandButton id="search" value="Otsi" rendered="true" style="margin-left: 5px; margin-top:0.2%;"/>
   </h:panelGroup>
   <h:panelGroup>
		<a:outputText value="Struktuuri üksuse tase: " style="padding: 2px" />
		<a:outputText id="Level" value="#{DocumentStatisticsListDialog.unitLevel}" />
	    <h:commandButton id="level-up" value="Üles" action="#{DocumentStatisticsListDialog.decreaseUnitLevel}" disabled="#{DocumentStatisticsListDialog.unitLevel < 2}" 
	    style="margin-left: 5px; margin-top:0.5%;" styleClass="specificAction" />
	    <h:commandButton id="level-down" value="Alla" action="#{DocumentStatisticsListDialog.increaseUnitLevel}" disabled="#{DocumentStatisticsListDialog.levelDownDisabled}" 
	    style="margin-left: 5px; margin-top:0.5%;" />
   </h:panelGroup>
   </h:panelGrid>
</a:panel>

<a:panel id="statistics-results-panel" styleClass="panel-100 with-pager" label="#{msg.statistics_overview_by_units}" progressive="true">
	<a:richList id="statisticsList" viewMode="details" pageSize="#{BrowseBean.pageSizeContent}" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt" width="100%"
	   	  value="#{DocumentStatisticsListDialog.statisticsOverview}" var="r" refreshOnBind="true">   
		<a:column id="col1" primary="true">
 	       <f:facet name="header">
		   		<a:sortLink id="col1-sort" label="#{msg.statistics_text_unit}" value="department" styleClass="header" />
		   </f:facet>
		   <a:actionLink id="col1-act" value="#{r.department}" action="dialog:documentStatisticsByTypeDialog" actionListener="#{DocumentStatisticsByTypeDialog.setupDepartmentView}" >
	            <f:param name="department" value="#{r.department}" />
	            <f:param name="startDate" value="#{DocumentStatisticsListDialog.startDateStr}" />
	            <f:param name="endDate" value="#{DocumentStatisticsListDialog.endDateStr}" />
	            <f:param name="level" value="#{DocumentStatisticsListDialog.unitLevel}" />  
           </a:actionLink>
	    </a:column>
		<a:column id="col2" primary="true">
		    <f:facet name="header">
                <a:sortLink id="col2-sort" label="#{msg.statistics_text_registred_doc_count}" value="registredDocumentsCount" styleClass="header" />
		    </f:facet>
         <a:actionLink id="col2-act" value="#{r.registredDocumentsCount}" action="dialog:documentStatisticsByTypeDialog" actionListener="#{DocumentStatisticsByTypeDialog.setupDepartmentView}" >
	            <f:param name="department" value="#{r.department}" />
	            <f:param name="startDate" value="#{DocumentStatisticsListDialog.startDateStr}" />
	            <f:param name="endDate" value="#{DocumentStatisticsListDialog.endDateStr}" />
	            <f:param name="level" value="#{DocumentStatisticsListDialog.unitLevel}" />  
           </a:actionLink>
		</a:column>

	      <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/page-size.jsp" />
	      <a:dataPager id="pager1" styleClass="pager" />
	   </a:richList>
	   <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/statistics/web/common/date-picker-script.jsp" />
	   <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/disable-dialog-finish-button.jsp" />
</a:panel>