<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="statistics-details-results-panel" styleClass="panel-100 with-pager" label="#{DocumentStatisticsListDetailDialog.departmentName}" progressive="true">
   <a:richList id="statisticsDetailList" viewMode="details" pageSize="#{BrowseBean.pageSizeContent}" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt" width="100%"
      value="#{DocumentStatisticsListDetailDialog.departmentOverview}" var="r" refreshOnBind="true" >

      <a:column id="col1">
         <f:facet name="header">
            <a:sortLink id="col1-sort" label="#{msg.statistics_text_doc_title}" value="title" styleClass="header" />
         </f:facet>
         <a:actionLink id="col1-act" value="#{r.title}" action="#{DocumentDialog.action}" actionListener="#{DocumentDialog.open}" >
            <f:param name="nodeRef" value="#{r.docNodeRef}" />
         </a:actionLink>
      </a:column>
      
      <a:column id="col2">
         <f:facet name="header">
            <a:sortLink id="col2-sort" label="#{msg.statistics_text_doc_reg_number}" value="viit" styleClass="header" />
         </f:facet>
         <a:actionLink id="col2-act" value="#{r.viit}" action="#{DocumentDialog.action}" actionListener="#{DocumentDialog.open}" >
            <f:param name="nodeRef" value="#{r.docNodeRef}" />
         </a:actionLink>
      </a:column>
      
      <a:column id="col3">
         <f:facet name="header">
            <a:sortLink id="col3-sort" label="#{msg.statistics_text_reg_date}" value="regDate" styleClass="header" />
         </f:facet>
         <a:actionLink id="col3-act" value="#{r.regDate}" action="#{DocumentDialog.action}" actionListener="#{DocumentDialog.open}" >
            <f:param name="nodeRef" value="#{r.docNodeRef}" />
         </a:actionLink>
      </a:column>
      
      <a:column id="col4">
         <f:facet name="header">
            <a:sortLink id="col4-sort" label="#{msg.statistics_text_owner}" value="owner" styleClass="header" />
         </f:facet>
         <a:actionLink id="col4-act" value="#{r.owner}" action="#{DocumentDialog.action}" actionListener="#{DocumentDialog.open}" >
            <f:param name="nodeRef" value="#{r.docNodeRef}" />
         </a:actionLink>
      </a:column>
    
      <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/page-size.jsp" />
      <a:dataPager id="pager1" styleClass="pager" />
   </a:richList>

   <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/disable-dialog-finish-button.jsp" />
</a:panel>