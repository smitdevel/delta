<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<f:verbatim><script>
	function setStatisticsDateFromEnumOnChange(){
	    var selector = jQuery("#" + escapeId4JQ("dialog:dialog-body:statistic_dueDate_DateRangePicker"));
		var selectedEnum = selector.val();
	    if (selectedEnum == "") return;
		var startDate = new Date();
		var finishDate = new Date();
		if (selectedEnum == "YESTERDAY"){
		    startDate.setDate(startDate.getDate()-1);
			finishDate.setDate(finishDate.getDate()-1);
		} else if (selectedEnum == "CURRENT_WEEK") {
		    startDate.setDate(startDate.getDate() - getEstonianWeekday(startDate));
		} else if (selectedEnum == "PREV_WEEK") {
		    startDate.setDate(startDate.getDate() - getEstonianWeekday(startDate) -7);
		    finishDate = new Date(startDate.getFullYear(),startDate.getMonth(),startDate.getDate() + 6,0,0,0,0);
		} else if (selectedEnum == "FROM_PREV_WEEK") {
		    startDate.setDate(startDate.getDate() - getEstonianWeekday(startDate) -7);
		} else if (selectedEnum == "CURRENT_MONTH") {
		    startDate = new Date(startDate.getFullYear(),startDate.getMonth(),1,0,0,0,0);
		} else if (selectedEnum == "PREV_MONTH") {
		    startDate = new Date(startDate.getFullYear(),startDate.getMonth() - 1,1,0,0,0,0);
		    finishDate = new Date(startDate.getFullYear(),startDate.getMonth() + 1,1,0,0,0,0);
		    finishDate.setDate(finishDate.getDate()-1);
		} else if (selectedEnum == "FROM_PREV_MONTH") {
		    startDate = new Date(startDate.getFullYear(),startDate.getMonth() - 1,1,0,0,0,0);
		} else if (selectedEnum == "CURRENT_YEAR"){
		      startDate = new Date(startDate.getFullYear(),0,1,0,0,0,0);
		}
		var beginDate = jQuery("#" + escapeId4JQ("dialog:dialog-body:statistic_dueDate"));
		var endDate = jQuery("#" + escapeId4JQ("dialog:dialog-body:statistic_dueDate_EndDate"));
		beginDate.val(("0" + startDate.getDate()).slice(-2) + "." + ("0" + (startDate.getMonth() + 1)).slice(-2) + "." + startDate.getFullYear());
		endDate.val(("0" + finishDate.getDate()).slice(-2) + "." + ("0" + (finishDate.getMonth() + 1)).slice(-2) + "." + finishDate.getFullYear());
	}
</script></f:verbatim>