package ee.webmedia.alfresco.statistics.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.alfresco.web.bean.dialog.BaseDialogBean;

import ee.webmedia.alfresco.classificator.enums.DatePeriods;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.statistics.service.StatisticsService;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.Pair;

import ee.webmedia.alfresco.utils.MessageUtil;

public abstract class StatisticsListDialog extends BaseDialogBean implements StatisticsDialog {

	protected final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	protected transient StatisticsService statisticsService;

	protected String startDateStr = getDateBeforeSevenDays();
	protected String endDateStr = dateFormat.format(new Date());
	protected List<SelectItem> datePeriods;
	protected Integer unitLevel = 1;
	protected String currentTimeZoneId = getCurrentTimeZoneId();

    public List<SelectItem> getDatePeriods() {
    	if (datePeriods == null) {
    		datePeriods = new ArrayList<>();
    		datePeriods.add(new SelectItem("", MessageUtil.getMessage(FacesContext.getCurrentInstance(), "document_choose")));
    		for (DatePeriods datePeriod : DatePeriods.values()) {
    			String valueName = datePeriod.getValueName();
    			datePeriods.add(new SelectItem(valueName, MessageUtil.getMessage(String.format("constant_ee.webmedia.alfresco.classificator.enums.DatePeriods_%s", valueName))));		
    		}
    	}
    	return datePeriods;
    }
    
    public String getCurrentTimeZoneId() {
    	Calendar now = Calendar.getInstance();
        TimeZone timeZone = now.getTimeZone();
        return timeZone.getID();
    }
    
    public Set<String> parseOrgsByLevel(List<String> orgs, Integer level) {
    	Set<String> orgsParsedByLevel = new HashSet<>();
    	for (String org : orgs) {
    		orgsParsedByLevel.add(formatOrgName(org, level));
    	}
    	return orgsParsedByLevel;
    }

    private String getDateBeforeSevenDays() {
    	Calendar seven = Calendar.getInstance();
        seven.add(Calendar.DAY_OF_MONTH, -7);
        seven.set(Calendar.HOUR_OF_DAY, 0);
        seven.set(Calendar.MINUTE, 0);
        seven.set(Calendar.SECOND, 0);
        return dateFormat.format(seven.getTime());
    }

	private void changeUnitLevel(boolean increase) {
		unitLevel = (increase) ? ++ unitLevel : -- unitLevel;
	}

	public boolean isLevelDownDisabled() {
		Pair<Date, Date> period = statisticsService.getStatisticsPeriod(startDateStr, endDateStr);
		List<String> orgs = getOwnerOrgs(period.getFirst(), period.getSecond());
    	Set<String> orgsParsedByLevel = parseOrgsByLevel(orgs, getUnitLevel() + 1);
    	orgsParsedByLevel.remove(MessageUtil.getMessage("statistics_text_outside"));
    	orgsParsedByLevel.remove(MessageUtil.getMessage("statistics_text_unspecified"));
		return orgsParsedByLevel.isEmpty();
	}

	public void increaseUnitLevel() {
		changeUnitLevel(true);
	}

	public void decreaseUnitLevel() {
		changeUnitLevel(false);
	}

	@Override
    protected String finishImpl(FacesContext context, String outcome) throws Throwable {
        return null;
    }
	
	public int getUnitLevel() {
		return unitLevel;		
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public StatisticsService getStatisticsService() {
		return statisticsService;
	}

	public void setStatisticsService(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}

    public void setupDepartmentView(String deparment) {}

    public void setupDepartmentView(ActionEvent event) {}

}
