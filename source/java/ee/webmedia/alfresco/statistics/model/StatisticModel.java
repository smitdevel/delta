package ee.webmedia.alfresco.statistics.model;

public class StatisticModel {

	private String department;
	private Integer incoming;
	private Integer answered;
	private Integer outOfTime;
	private Integer outOfTimeInNextSevenDays;
	
	public StatisticModel(String department, Integer incoming, Integer answered, Integer outOfTime,
			Integer outOfTimeInNextSevenDays) {
		if (incoming == null) incoming = 0;
		if (answered == null) answered = 0;
		if (outOfTime == null) outOfTime = 0;
		if (outOfTimeInNextSevenDays == null) outOfTimeInNextSevenDays = 0;
		this.department = department;
		this.incoming = incoming;
		this.answered = answered;
		this.outOfTime = outOfTime;
		this.outOfTimeInNextSevenDays = outOfTimeInNextSevenDays;
	}
	
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public Integer getIncoming() {
		return incoming;
	}
	public void setIncoming(Integer incoming) {
		this.incoming = incoming;
	}
	public Integer getAnswered() {
		return answered;
	}
	public void setAnswered(Integer answered) {
		this.answered = answered;
	}
	public Integer getOutOfTime() {
		return outOfTime;
	}
	public void setOutOfTime(Integer outOfTime) {
		this.outOfTime = outOfTime;
	}
	public Integer getOutOfTimeInNextSevenDays() {
		return outOfTimeInNextSevenDays;
	}
	public void setOutOfTimeInNextSevenDays(Integer outOfTimeInNextSevenDays) {
		this.outOfTimeInNextSevenDays = outOfTimeInNextSevenDays;
	}
	
}
