package ee.webmedia.alfresco.statistics.model.workflow;

import java.util.Date;
import org.alfresco.service.cmr.repository.NodeRef;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.utils.MessageUtil;

public class WorkflowStatDetailModel {
	
	private NodeRef wfNodeRef;
	private String departmentName;
	private String title;
	private String ownerName;
	private Date startedDate;
	private Date dueDate;
	private String status;
	private String docCount;
	private NodeRef docRef;

	public WorkflowStatDetailModel(NodeRef wfNodeRef, String departmentName,
			String title, String ownerName, Date startedDate, Date dueDate,
			String status, String docCount, NodeRef docRef) {
		this.departmentName = departmentName;
		this.title = title;
		this.ownerName = ownerName;
		this.startedDate = startedDate;
		this.dueDate = dueDate;
		this.status = status;
		this.wfNodeRef = wfNodeRef;
        this.docCount = docCount;
        this.docRef = docRef;
	}

	public NodeRef getWfNodeRef() {
		return wfNodeRef;
	}

	public void setWfNodeRef(NodeRef wfNodeRef) {
		this.wfNodeRef = wfNodeRef;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public Date getStartedDate() {
		return startedDate;
	}

	public void setStartedDate(Date startedDate) {
		this.startedDate = startedDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDocCount() {
		return docCount;
	}

	public void setDocCount(String docCount) {
		this.docCount = docCount;
	}

	public NodeRef getDocRef() {
		return docRef;
	}

	public void setDocRef(NodeRef docRef) {
		this.docRef = docRef;
	}
	
	public boolean isDocumentWorkflow() {
		return docRef != null;
	}

}
