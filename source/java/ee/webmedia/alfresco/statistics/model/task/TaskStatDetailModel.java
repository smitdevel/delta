package ee.webmedia.alfresco.statistics.model.task;

import java.util.Date;
import org.alfresco.service.cmr.repository.NodeRef;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.statistics.service.StatisticsService;
import ee.webmedia.alfresco.utils.MessageUtil;

public class TaskStatDetailModel {
	
	private NodeRef docNodeRef;
	private String departmentName;
	private String title;
	private String taskType;
	private String creatorName;
	private String ownerName;
	private Date startedDate;
	private Date dueDate;
	private Date completedDate;
	private Long overdueDays;
	private Long workDays;
	private String status;

	
	public TaskStatDetailModel(NodeRef docNodeRef, String departmentName,
			String title, String taskType, String creatorName, String ownerName, Date startedDate, Date dueDate,
			Date completedDate, String status) {
		StatisticsService statisticsService = BeanHelper.getStatisticsService();
		this.docNodeRef = docNodeRef;
		this.departmentName = departmentName;
		this.title = title;
		this.taskType = MessageUtil.getMessage(String.format("wfs_workflowSpecificModel.type.wfs_%s.title", taskType));
		this.creatorName = creatorName;
		this.ownerName = ownerName;
		this.startedDate = startedDate;
		this.dueDate = dueDate;
		this.completedDate = completedDate;
		this.status = status;
		if (dueDate != null) {
			if (completedDate != null) {
				if (completedDate.after(dueDate)) this.overdueDays = statisticsService.datesDiffrenceInDays(completedDate, dueDate);
				this.workDays = statisticsService.datesDiffrenceInDays(completedDate, startedDate);
			} else {
				Date now = statisticsService.getCurrentDateAtStartOfDay();
				if (dueDate.before(now)) this.overdueDays = statisticsService.datesDiffrenceInDays(dueDate, now);
			}
		}
	}

	public NodeRef getDocNodeRef() {
		return docNodeRef;
	}

	public void setDocNodeRef(NodeRef docNodeRef) {
		this.docNodeRef = docNodeRef;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public Date getStartedDate() {
		return startedDate;
	}

	public void setStartedDate(Date startedDate) {
		this.startedDate = startedDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getOverdueDays() {
		return overdueDays;
	}

	public void setOverdueDays(Long overdueDays) {
		this.overdueDays = overdueDays;
	}

	public Long getWorkDays() {
		return workDays;
	}

	public void setWorkDays(Long workDays) {
		this.workDays = workDays;
	}
}
