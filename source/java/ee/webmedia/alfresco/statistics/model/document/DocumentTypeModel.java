package ee.webmedia.alfresco.statistics.model.document;

public class DocumentTypeModel {

	private String docTypeName;
	private String docType;
	private Integer registredDocumentsCount;
		
	public DocumentTypeModel(String docTypeName, String docType, Integer registredDocumentsCount) {
		super();
		this.docTypeName = docTypeName;
		this.docType = docType;
		this.registredDocumentsCount = registredDocumentsCount;
	}
	
	public String getDocTypeName() {
		return docTypeName;
	}
	public void setDocTypeName(String docTypeName) {
		this.docTypeName = docTypeName;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public Integer getRegistredDocumentsCount() {
		return registredDocumentsCount;
	}
	public void setRegistredDocumentsCount(Integer registredDocumentsCount) {
		this.registredDocumentsCount = registredDocumentsCount;
	}


}
