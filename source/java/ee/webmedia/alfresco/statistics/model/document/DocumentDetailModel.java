package ee.webmedia.alfresco.statistics.model.document;

import java.util.Date;
import org.alfresco.service.cmr.repository.NodeRef;

public class DocumentDetailModel {

	private NodeRef docNodeRef;
	private String title;
	private String viit;
	private Date regDate;
	private String owner;

	public DocumentDetailModel(NodeRef docNodeRef, String title,
			String viit, Date regDate, String owner) {
		this.docNodeRef = docNodeRef;
		this.title = title;
		this.viit = viit;
		this.regDate = regDate;
		this.owner = owner;
	}
	
	public NodeRef getDocNodeRef() {
		return docNodeRef;
	}
	public void setDocNodeRef(NodeRef docNodeRef) {
		this.docNodeRef = docNodeRef;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getViit() {
		return viit;
	}
	public void setViit(String viit) {
		this.viit = viit;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
}
