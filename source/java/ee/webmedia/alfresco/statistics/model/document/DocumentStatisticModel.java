package ee.webmedia.alfresco.statistics.model.document;

public class DocumentStatisticModel {

	private String department;
	private Integer registredDocumentsCount;

	public DocumentStatisticModel(String department, Integer registredDocumentsCount) {
		this.department = department;
		this.registredDocumentsCount = registredDocumentsCount;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Integer getRegistredDocumentsCount() {
		return registredDocumentsCount;
	}

	public void setRegistredDocumentsCount(Integer registredDocumentsCount) {
		this.registredDocumentsCount = registredDocumentsCount;
	}

}
