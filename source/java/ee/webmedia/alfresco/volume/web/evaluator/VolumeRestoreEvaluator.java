package ee.webmedia.alfresco.volume.web.evaluator;

import java.util.List;
import java.util.Map;

import org.alfresco.web.bean.repository.Node;

import ee.webmedia.alfresco.classificator.enums.DocListUnitStatus;
import ee.webmedia.alfresco.classificator.enums.VolumeType;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.eventplan.model.EventPlanModel;
import ee.webmedia.alfresco.series.model.SeriesModel;
import ee.webmedia.alfresco.user.web.DocumentManagerEvaluator;

/**
 * Evaluates to true if given series has status "avatud" and volume types contains "ANNUAL_FILE" or "SUBJECT_FILE".
 */
public class VolumeRestoreEvaluator extends DocumentManagerEvaluator {

    private static final long serialVersionUID = 0L;

    @Override
    public boolean evaluate(Node node) {
		Map<String, Object> props = node.getProperties();
		String status = (String) props.get(DocumentDynamicModel.Props.STATUS.toString());
		Object isAppraised = props.get(EventPlanModel.Props.IS_APPRAISED);
		Object nextEvent = props.get(EventPlanModel.Props.NEXT_EVENT);
		boolean isOpenedOrClosed = status.equals("avatud") || status.equals("suletud");
		boolean isArchival = BeanHelper.getGeneralService().isArchivalsStoreRef(node.getNodeRef().getStoreRef());
		boolean inGroup = BeanHelper.getUserService().isAdministrator() || BeanHelper.getUserService().isArchivist();
		return isArchival && inGroup && isOpenedOrClosed 
				&& ((isAppraised == null || ! (Boolean) isAppraised) || (nextEvent != null && nextEvent.toString().equals("REVIEW")));
    }
}
