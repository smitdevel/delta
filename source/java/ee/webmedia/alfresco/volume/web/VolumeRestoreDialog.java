package ee.webmedia.alfresco.volume.web;

import static ee.webmedia.alfresco.common.web.BeanHelper.getCaseFileDialog;
import static ee.webmedia.alfresco.common.web.BeanHelper.getCaseFileService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getFunctionsService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getSeriesService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getVolumeDetailsDialog;
import static ee.webmedia.alfresco.common.web.BeanHelper.getVolumeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.dialog.BaseDialogBean;

import ee.webmedia.alfresco.adr.model.AdrSkipUpdateDto;
import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.cases.model.CaseModel;
import ee.webmedia.alfresco.classificator.enums.DocListUnitStatus;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.functions.model.UnmodifiableFunction;
import ee.webmedia.alfresco.series.model.UnmodifiableSeries;
import ee.webmedia.alfresco.utils.ActionUtil;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.volume.model.VolumeModel;

public class VolumeRestoreDialog extends BaseDialogBean {

    private NodeRef currentEntry;
    private NodeRef function;
    private NodeRef series;

    public void setup(ActionEvent event) {
    	currentEntry = ActionUtil.getParentNodeRefParam(event);
    }
    
	@Override
	protected String finishImpl(FacesContext context, String outcome) throws Throwable {
		if (function == null  || series == null) {
            MessageUtil.addErrorMessage("save_error_location", null);
			return null;
		}
		QName type = getNodeService().getType(currentEntry);
		ChildAssociationRef restoredNodeRef;
		if (type.equals(CaseFileModel.Types.CASE_FILE)) {
			restoredNodeRef = getNodeService().moveNode(currentEntry, series, 
					CaseFileModel.Assocs.CASE_FILE, CaseFileModel.Assocs.CASE_FILE);
			getNodeService().setProperty(restoredNodeRef.getChildRef(), DocumentCommonModel.Props.FUNCTION, function);
			getNodeService().setProperty(restoredNodeRef.getChildRef(), DocumentCommonModel.Props.SERIES, series);
			getCaseFileDialog().setCaseFile(getCaseFileService().getCaseFile(restoredNodeRef.getChildRef()));
		} else {
			restoredNodeRef = getNodeService().moveNode(currentEntry, series, 
					VolumeModel.Associations.VOLUME, VolumeModel.Associations.VOLUME);
			getNodeService().setProperty(restoredNodeRef.getChildRef(), DocumentCommonModel.Props.FUNCTION, function);
			getNodeService().setProperty(restoredNodeRef.getChildRef(), DocumentCommonModel.Props.SERIES, series);
			getVolumeDetailsDialog().setCurrentEntry(getVolumeService().getVolumeByNodeRef(restoredNodeRef.getChildRef(), null));

		}
		if (getNodeService().getProperties(restoredNodeRef.getChildRef()).containsKey(VolumeModel.Props.MARKED_FOR_ARCHIVING)) {
			getNodeService().removeProperty(restoredNodeRef.getChildRef(), VolumeModel.Props.MARKED_FOR_ARCHIVING);
		}
		setAdrSkipUpdateForVolumeDocs(restoredNodeRef.getChildRef());
		if (type.equals(VolumeModel.Types.VOLUME) || type.equals(CaseFileModel.Types.CASE_FILE)) {
			for (ChildAssociationRef child : getNodeService().getChildAssocs(restoredNodeRef.getChildRef())) {
				NodeRef childNodeRef = child.getChildRef();
	            QName nodeType = getNodeService().getType(childNodeRef);
	            if (DocumentCommonModel.Types.DOCUMENT.equals(nodeType)) {
					getNodeService().setProperty(childNodeRef, DocumentCommonModel.Props.FUNCTION, function);
					getNodeService().setProperty(childNodeRef, DocumentCommonModel.Props.SERIES, series);
					getNodeService().setProperty(childNodeRef, DocumentCommonModel.Props.VOLUME, restoredNodeRef.getChildRef());
	            }
			}
		}

        MessageUtil.addInfoMessage("save_success");
		return outcome;
	}
	
	
	private void setAdrSkipUpdateForVolumeDocs(NodeRef volumeNodeRef) {
		StoreRef archivalStore = BeanHelper.getGeneralService().getArchivalsStoreRef();
		List<ChildAssociationRef> volumeChildAssocs = getNodeService().getChildAssocs(volumeNodeRef);
        for (ChildAssociationRef childAssocRef : volumeChildAssocs) {
            NodeRef childNodeRef = childAssocRef.getChildRef();
            QName nodeType = getNodeService().getType(childNodeRef);
            if (CaseModel.Types.CASE.equals(nodeType)) {
            	List<ChildAssociationRef> caseChildAssocs = getNodeService().getChildAssocs(childNodeRef);
            	for (ChildAssociationRef caseChildAssocRef : caseChildAssocs) {
            		NodeRef caseChildNodeRef = caseChildAssocRef.getChildRef();
                    QName caseChildNodeType = getNodeService().getType(childNodeRef);
            		if (DocumentCommonModel.Types.DOCUMENT.equals(caseChildNodeType)) {
                    	setAdrSkipUpdate(archivalStore, caseChildNodeRef);
            		}
            	}
            	
            } else if (DocumentCommonModel.Types.DOCUMENT.equals(nodeType)) {
            	setAdrSkipUpdate(archivalStore, childNodeRef);
            }
        }
	}
	
	
	
	private void setAdrSkipUpdate(StoreRef archivalStore, NodeRef restoredRef) {
		
		NodeRef archivedRef = new NodeRef(archivalStore, restoredRef.getId());
    	AdrSkipUpdateDto dto;
    	if (getNodeService().hasAspect(restoredRef, DocumentCommonModel.Aspects.ADR_SKIP_UPDATE)) {
    		dto = BeanHelper.getAdrDbService().getByArchivedRef(archivedRef);
    		dto.setRestoredRef(restoredRef.toString());
    		BeanHelper.getAdrDbService().update(dto);
    	} else {
    		getNodeService().addAspect(restoredRef, DocumentCommonModel.Aspects.ADR_SKIP_UPDATE, null);
    		dto = new AdrSkipUpdateDto();
    		dto.setAdrRef(archivedRef.toString());
    		dto.setRestoredRef(restoredRef.toString());
    		BeanHelper.getAdrDbService().insert(dto);
    	}
    }
	
    public List<SelectItem> getFunctionList() {
    	List<SelectItem> functions = new ArrayList<>();
    	functions.add(0, new SelectItem("", MessageUtil.getMessage("select_default_label")));
    	for (UnmodifiableFunction unmodifiableFunction : getFunctionsService().getAllSpaceStoreFunctions(DocListUnitStatus.OPEN)) {
    		functions.add(new SelectItem(unmodifiableFunction.getNodeRef().toString(), unmodifiableFunction.getFunctionLabelForModal()));
    	}
    	for (UnmodifiableFunction unmodifiableFunction : getFunctionsService().getAllSpaceStoreFunctions(DocListUnitStatus.CLOSED)) {
    		functions.add(new SelectItem(unmodifiableFunction.getNodeRef().toString(), unmodifiableFunction.getFunctionLabelForModal()));
    	}
    	return functions;
    }
    
    public List<SelectItem> getSeriesList() {
    	if (function == null) {
        	return Collections.EMPTY_LIST;
    	}

    	List<SelectItem> series = new ArrayList<>();
    	series.add(0, new SelectItem("", MessageUtil.getMessage("select_default_label")));
    	for (UnmodifiableSeries unmodifiableSeries : getSeriesService().getAllSeriesByFunction(function, false)) {
    		series.add(new SelectItem(unmodifiableSeries.getNodeRef().toString(), unmodifiableSeries.getSeriesLabelForModal()));
    	}
    	return series;
    }
    
    public void onFunctionChange(ValueChangeEvent event) {
    	Object newValue = event.getNewValue();
    	if (newValue == null || !NodeRef.isNodeRef((String) newValue)) {
    		function = null;
    	} else {
    		function = new NodeRef((String) newValue);
    	}
		series = null;
    }
    
    public void onSeriesChange(ValueChangeEvent event) {
    	Object newValue = event.getNewValue();
    	if (newValue == null || !NodeRef.isNodeRef((String) newValue)) {
    		series = null;
    	} else {
    		series = new NodeRef((String) newValue);
    	}
    }

	public NodeRef getFunction() {
		return function;
	}

	public NodeRef getSeries() {
		return series;
	}

}
