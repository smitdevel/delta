<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="restore-volume-panel" label="#{msg.volume_restore}" styleClass="panel-100" progressive="true">
		<r:propertySheetGrid value="restore-volume_grid" columns='2' labelStyleClass="propertiesLabel">
		    <h:outputText id="function-text" value="#{msg.function}"/>
		    <h:selectOneMenu id="function" immediate="true" valueChangeListener="#{VolumeRestoreDialog.onFunctionChange}" onchange="document.forms['dialog'].submit()">
		         <f:selectItems value="#{VolumeRestoreDialog.functionList}" />
		    </h:selectOneMenu>
		    <h:outputText id="series-text" value="#{msg.series}" />
		    <h:selectOneMenu id="series" disabled="#{VolumeRestoreDialog.function == null}" style="width: 100%;" immediate="true" valueChangeListener="#{VolumeRestoreDialog.onSeriesChange}" onchange="document.forms['dialog'].submit()">
		    	 <f:selectItems value="#{VolumeRestoreDialog.seriesList}" />
		    </h:selectOneMenu>
		</r:propertySheetGrid>
</a:panel>