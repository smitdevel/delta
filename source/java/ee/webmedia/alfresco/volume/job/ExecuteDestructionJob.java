package ee.webmedia.alfresco.volume.job;

import java.io.Serializable;
import java.util.*;

import com.google.common.base.Strings;
import ee.webmedia.alfresco.archivals.model.ActivityStatus;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.volume.model.VolumeModel;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.QNamePattern;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.util.Assert;

import ee.webmedia.alfresco.destruction.model.DestructionModel;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.destruction.model.DestructionJobStatus;
import ee.webmedia.alfresco.archivals.model.ArchivalsModel;
import ee.webmedia.alfresco.archivals.service.ArchivalsService;
import ee.webmedia.alfresco.common.web.BeanHelper;

import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.DOC_NAME;
import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.REG_NUMBER;

public class ExecuteDestructionJob implements StatefulJob {

    private static final Log LOG = LogFactory.getLog(ExecuteDestructionJob.class);
    public static final String SKIPPED_DOC_NAME_TEMP_CODE = "archivals_volume_next_event_csv_destruction_template";

    private ArchivalsService archivalsService;
    private NodeService nodeService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        setServices();

        if (archivalsService.isArchiveJobInProgress()) {
            LOG.debug("DESTRUCTION_JOB :: Archive job is in-progress, exiting from Destruction job.");
            return;
        }

        if(!archivalsService.setDestructionJobInProgress(true)) {
            LOG.debug("DESTRUCTION_JOB :: Set destruction job InProgress. Exit job.");
            return;
        }

        while (archivalsService.isDestructionAllowed()) {

            List<NodeRef> jobList = archivalsService.getAllInQueueJobsForDesruction();
            if (jobList.isEmpty()) {
                LOG.debug("DESTRUCTION_JOB :: Destruction job list is empty!");
                archivalsService.resetDestructionManualActions();
                break;
            }

            Pair<NodeRef, DestructionJobStatus> jobRefAndStatus = getNextDestructingJobWithStatus(jobList);
            NodeRef destructingJobRef = jobRefAndStatus.getFirst();
            DestructionJobStatus jobStatus = jobRefAndStatus.getSecond();
            LOG.warn("DESTRUCTION_JOB :: Start destructingJob jobNodeRef="+destructingJobRef.toString()+", jobStatus="+jobStatus);

            /* DELTA-1704_Skipping documents/files in destruction process:
               Destruction jobs in a partially destroyed status can only be restarted from the "Performed Archiving Activities" view
               using the " Continue Partially Destroyed Jobs" button.
            * */
            if (DestructionJobStatus.PARTIALLY_DESTROYED.equals(jobStatus))
                break;

            if (!DestructionJobStatus.RESUME_PARTIALLY_DESTROYED.equals(jobStatus) && archivalsService.isDestructionPaused())
                break;

            NodeRef activityRef = null;
            try {
                activityRef = archivalsService.getDestructionJobArchivalActivity(destructingJobRef);

                Assert.notNull(activityRef, "DESTRUCTION_JOB :: Reference to activityRef node must be provided in destructingJobRef="  +destructingJobRef.toString());

                final Map<QName, Serializable> jobProps = nodeService.getProperties(destructingJobRef);
                NodeRef volumeNodeRef = (NodeRef) jobProps.get(DestructionModel.Props.VOLUME_REF);

                LOG.warn(String.format("DESTRUCTION_JOB :: Starting ExecuteDestructionJob, job=%s, status=%s, activityRef=%s", destructingJobRef.toString(),
                        jobStatus, activityRef.toString()));

                boolean resumingPaused = DestructionJobStatus.PAUSED.equals(jobStatus);
                if (DestructionJobStatus.IN_PROGRESS.equals(jobStatus) || resumingPaused) {
                    // archiving is executed in this thread so IN_PROGRESS job should only get here if server was restarted in the middle of archiving

                    if (!nodeService.exists(volumeNodeRef)) {
                        LOG.warn("DESTRUCTION_JOB :: Destruction of volume (nodeRef=" + volumeNodeRef + ") was removed from destructing job list.");
                        if (resumingPaused) {
                            LOG.warn("DESTRUCTION_JOB :: Unable to resume paused destructing job");
                        }

                        // Set ActivityStatus of activityRef Finished
                        archivalsService.removeJobNodeFromDestructingList(destructingJobRef);
                        continue;
                    }
                }
                if (DestructionJobStatus.FAILED.equals(jobStatus) || DestructionJobStatus.FINISHED.equals(jobStatus)) {
                    logDestructionResult(destructingJobRef);
                    archivalsService.removeJobNodeFromDestructingList(destructingJobRef);
                    LOG.warn("DESTRUCTION_JOB :: removeJobNodeFromDestructingList destructingJobRef=" + destructingJobRef.toString());
                    continue;
                }

                Assert.notNull(volumeNodeRef, "Reference to volume node must be provided destructingJobRef="+ destructingJobRef.toString());

                // fill single item list for old methods compatibility.
                List<NodeRef> selectedVolumes = new ArrayList<NodeRef>();
                selectedVolumes.add(volumeNodeRef);


                archivalsService.markDestructingJobAsRunning(destructingJobRef);

                if (archivalsService.disposeVolumes(selectedVolumes, new Date(), activityRef,
                        null /*templateRef*/, MessageUtil.getMessage("applog_archivals_volume_disposed"))) {

                    archivalsService.markDestructionJobFinished(destructingJobRef);
                    LOG.warn("DESTRUCTION_JOB :: markDestructionJobFinished destructingJobRef=" + destructingJobRef.toString());
                } else {
                    archivalsService.markDestructingJobAsPaused(destructingJobRef);
                    LOG.warn("DESTRUCTION_JOB :: markDestructingJobAsPaused since can not disposeVolumes of destructingJobRef=" + destructingJobRef.toString());
                }
            } catch (Exception e) {
                LOG.error(String.format("DESTRUCTION_JOB :: Skip the documents and files that cannot be destroyed during the destruction process because of an unexpected Exception of an archivalActivity = %s", activityRef != null ? activityRef.toString() : "null"), e);

                if (activityRef != null || nodeService.exists(activityRef)) {
                    List<NodeRef> archivalActivityVolumes = new ArrayList<NodeRef>();

                    List<NodeRef> jobsForDestruction = archivalsService.getAllInQueueJobsForDesruction();
                    if (jobsForDestruction.isEmpty()) {
                        return;
                    }

                    // if Skipped CSV file exists, remove it
                    archivalsService.removeSkippedCSVFile(activityRef, SKIPPED_DOC_NAME_TEMP_CODE);

                    // Iterate over all jobs in destructionQueue and find jobs of an archivalActivity that cause exception
                    for (NodeRef jobNodeRef : jobsForDestruction) {
                        if (jobNodeRef != null && nodeService.exists(jobNodeRef)) {
                            // Collect all volumes of an archivalActivity which cause exception
                            List<AssociationRef> activitiesList = nodeService.getSourceAssocs(jobNodeRef, DestructionModel.Assocs.ACTIVITY_LINKED_JOBS);
                            for (AssociationRef associationRef : activitiesList) {
                                NodeRef jobActivityRef = associationRef.getSourceRef();

                                if (jobActivityRef != null && jobActivityRef.getId().equals(activityRef.getId())) {
                                    NodeRef jobVolume = (NodeRef) nodeService.getProperties(jobNodeRef).get(DestructionModel.Props.VOLUME_REF);

                                    if (jobVolume != null && nodeService.exists(jobVolume)) {
                                        LOG.warn(String.format("DESTRUCTION_JOB :: Collect all volumes of an archivalActivity=%s of a job=%s selected volume is %s",
                                                jobActivityRef.toString(), jobNodeRef.toString(), jobVolume.toString()));
                                        archivalActivityVolumes.add(jobVolume);

                                        // if at least one file was not destroyed, the status = “osaliselt hävitatud” (partially destroyed) is set on the archivalActivity object
                                        nodeService.setProperty(activityRef, ArchivalsModel.Props.STATUS, ActivityStatus.PARTIALLY_DESTROYED.getValue());
                                        archivalsService.markDestructingJobAsPartiallyDestroyed(jobNodeRef);
                                        LOG.warn("DESTRUCTION_JOB :: markDestructingJobAsPartiallyDestroyed destructingJobRef=" + destructingJobRef.toString());
                                    } else {
                                        LOG.warn(String.format("DESTRUCTION_JOB :: Volume reference is not exist in destructingJobRef=%s of an archivalActivity=%s. RemoveJobNodeFromDestructingList!", jobNodeRef.toString(), jobActivityRef.toString()));
                                        // Set ActivityStatus of activityRef Finished
                                        archivalsService.removeJobNodeFromDestructingList(jobNodeRef);
                                    }
                                }
                            }
                        }
                    }

                    if (archivalActivityVolumes.size() > 0) {
                        List<String[]> docInfos = generateSkippedDocsData(archivalActivityVolumes);
                        archivalsService.addArchivalActivitySkippedCSV(activityRef, SKIPPED_DOC_NAME_TEMP_CODE, docInfos);
                        LOG.warn(String.format("DESTRUCTION_JOB :: Generating a file of skipped documents of an archivalActivityRef=%s", activityRef.toString()));
                    }
                }
            }
        }

        archivalsService.setDestructionJobInProgress(false);
    }

    private List<String[]> generateSkippedDocsData(List<NodeRef> volumes) {
        List<String[]> docInfos = new ArrayList<>();
        if (volumes.size() > 0) {
            for (NodeRef volumeNodeRef : volumes) {
                Map<QName, Serializable> props = BeanHelper.getNodeService().getProperties(volumeNodeRef);

                List<ChildAssociationRef> documents = //nodeService.getChildAssocs(volumeNodeRef, DocumentCommonModel.Assocs.DOCUMENT, DocumentCommonModel.Assocs.DOCUMENT);
                        nodeService.getChildAssocs(volumeNodeRef, new QNamePattern() {
                            @Override
                            public boolean isMatch(QName qname) {
                                if (VolumeModel.Associations.DELETED_DOCUMENT.equals(qname)) {
                                    return false;
                                }
                                else {
                                    return true;
                                }

                            }
                        }, new QNamePattern() {
                            @Override
                            public boolean isMatch(QName qname) {
                                if (VolumeModel.Associations.DELETED_DOCUMENT.equals(qname)) {
                                    return false;
                                }
                                else {
                                    return true;
                                }
                            }
                        });
                // Documents
                if (documents != null && documents.size() > 0) {
                    for (ChildAssociationRef assocRef : documents) {
                        NodeRef docNodeRef = assocRef.getChildRef();
                        if (docNodeRef != null && nodeService.exists(docNodeRef)) {
                            String regNumber = (String) nodeService.getProperty(docNodeRef, REG_NUMBER);
                            String docName = (String) nodeService.getProperty(docNodeRef, DOC_NAME);
                            String docUrl = BeanHelper.getDocumentTemplateService().getDocumentUrl(docNodeRef);

                            docInfos.add(new String[]{
                                    getStringProperty(props, VolumeModel.Props.MARK),
                                    getStringProperty(props, VolumeModel.Props.TITLE),
                                    Strings.isNullOrEmpty(regNumber) ? "" : regNumber,
                                    Strings.isNullOrEmpty(docName) ? "" : docName,
                                    Strings.isNullOrEmpty(docUrl) ? "" : docUrl});

                            LOG.warn(String.format("DESTRUCTION_JOB :: volumeNodeRef=%s, DOCUMENT=%s", volumeNodeRef.toString(), docNodeRef.toString()));
                        }
                    }
                }

                // Case documents
                for (NodeRef casRef : BeanHelper.getCaseService().getCaseRefsByVolume(volumeNodeRef)) {

                    List<ChildAssociationRef> caseChildAssocs = nodeService.getChildAssocs(casRef, new QNamePattern() {
                        @Override
                        public boolean isMatch(QName qname) {
                            if (VolumeModel.Associations.DELETED_DOCUMENT.equals(qname)) {
                                return false;
                            }
                            else {
                                return true;
                            }

                        }
                    }, new QNamePattern() {
                        @Override
                        public boolean isMatch(QName qname) {
                            if (VolumeModel.Associations.DELETED_DOCUMENT.equals(qname)) {
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                    });

                    if (caseChildAssocs != null && caseChildAssocs.size() > 0) {
                        for (ChildAssociationRef assocRef : caseChildAssocs) {
                            NodeRef docNodeRef = assocRef.getChildRef();
                            if (docNodeRef != null && nodeService.exists(docNodeRef)) {
                                String regNumber = (String) nodeService.getProperty(docNodeRef, REG_NUMBER);
                                String docName = (String) nodeService.getProperty(docNodeRef, DOC_NAME);
                                String docUrl = BeanHelper.getDocumentTemplateService().getDocumentUrl(docNodeRef);

                                docInfos.add(new String[]{
                                        getStringProperty(props, VolumeModel.Props.MARK),
                                        getStringProperty(props, VolumeModel.Props.TITLE),
                                        Strings.isNullOrEmpty(regNumber) ? "" : regNumber,
                                        Strings.isNullOrEmpty(docName) ? "" : docName,
                                        Strings.isNullOrEmpty(docUrl) ? "" : docUrl});

                                LOG.warn(String.format("DESTRUCTION_JOB :: volumeNodeRef=%s, CASE=%s, doc=%s", volumeNodeRef.toString(), casRef.toString(), docNodeRef.toString()));
                            }
                        }
                    }
                }
            }
        }
        return docInfos;
    }

    private Pair<NodeRef, DestructionJobStatus> getNextDestructingJobWithStatus(List<NodeRef> jobList) {
        Map<NodeRef, Node> jobs = BeanHelper.getBulkLoadNodeService().loadNodes(jobList, Collections.singleton(DestructionModel.Props.DESTRUCING_JOB_STATUS));
        for (Map.Entry<NodeRef, Node> entry : jobs.entrySet()) {
            DestructionJobStatus status = getStatus(entry.getValue());
            if (DestructionJobStatus.PAUSED.equals(status)) {
                return Pair.newInstance(entry.getKey(), status);
            }
        }
        Node jobNode = jobs.get(jobList.get(0));
        return Pair.newInstance(jobList.get(0), getStatus(jobNode));
    }

    private DestructionJobStatus getStatus(Node entry) {
        Map<String, Object> jobProps = entry.getProperties();
        LOG.debug("DestructionJob :: REF="+ entry.getNodeRef().toString());
        return DestructionJobStatus.valueOf((String) jobProps.get(DestructionModel.Props.DESTRUCING_JOB_STATUS.toString()));
    }

    private void logDestructionResult(NodeRef destructingJobRef) {
        Map<QName, Serializable> props = nodeService.getProperties(destructingJobRef);
        NodeRef volumeRef = (NodeRef) props.get(DestructionModel.Props.VOLUME_REF);
        DestructionJobStatus status = DestructionJobStatus.valueOf((String) props.get(DestructionModel.Props.DESTRUCING_JOB_STATUS));
        switch (status) {
            case FAILED:
                LOG.warn("Destructing of volume (nodeRef=" + volumeRef + ") failed with the following error message:\n" + props.get(DestructionModel.Props.ERROR_MESSAGE));
                break;
            case FINISHED:
                LOG.info("Destructing of volume (nodeRef=" + volumeRef + ") has been completed successfully.\n" +
                        "Destructing started at " + props.get(DestructionModel.Props.DESTRUCTING_START_TIME) + " and ended at " + props.get(DestructionModel.Props.DESTRUCTING_END_TIME));
                break;
        }
    }

    private void setServices() {
        archivalsService = BeanHelper.getArchivalsService();
        nodeService = BeanHelper.getNodeService();
    }

    private String getStringProperty(Map<QName, Serializable> props, QName propertyName) {
        Serializable value = props.get(propertyName);
        return value != null ? value.toString() : "";
    }
}
