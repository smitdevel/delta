package ee.webmedia.alfresco.docconfig.bootstrap;

import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

import java.io.Serializable;
import java.util.List;

public class AccessRestrictionGroupUpdater extends AbstractModuleComponent {

    private static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(AccessRestrictionGroupUpdater.class);

    private GeneralService generalService;
    private NodeService nodeService;

    @Override
    protected void executeInternal() throws Throwable {
        LOG.info("Executing " + getName());
        addFieldsToFieldGroup();
        LOG.info("Finished updating accessRestriction field group definition.");
    }

    private void addFieldsToFieldGroup() {
        NodeRef groupDefinitionRef = generalService.getNodeRef("/docadmin:fieldGroupDefinitions/docadmin:accessRestriction");
        String changeReason = DocumentDynamicModel.Props.ACCESS_RESTRICTION_CHANGE_REASON.getLocalName();
        String changeReasonDesc = DocumentDynamicModel.Props.ACCESS_RESTRICTION_CHANGE_REASON_DESC.getLocalName();
        if (groupDefinitionRef == null) {
            LOG.info("Could not find field group definition. Skipping field group.");
            return;
        }
        @SuppressWarnings("unchecked")
        List<String> existingIds = (List<String>) nodeService.getProperty(groupDefinitionRef, DocumentAdminModel.Props.FIELD_DEFINITIONS_IDS);
        boolean containsChangeReason = existingIds.contains(changeReason);
        boolean containsChangeReasonDesc = existingIds.contains(changeReasonDesc);

        if (containsChangeReason) {
            LOG.info(changeReason + " field already added to 'accessRestriction' group. Skipping field.");
            return;
        } else {
            existingIds.add(changeReason);
            LOG.info("Added '" + changeReason + "' to 'accessRestriction' group.");
        }
        if (containsChangeReasonDesc) {
            LOG.info(changeReasonDesc + " field already added to 'accessRestriction' group. Skipping field.");
            return;
        } else {
            existingIds.add(changeReasonDesc);
            LOG.info("Added '" + changeReasonDesc + "' to 'accessRestriction' group.");
        }
        nodeService.setProperty(groupDefinitionRef, DocumentAdminModel.Props.FIELD_DEFINITIONS_IDS, (Serializable) existingIds);
    }

    public void setGeneralService(GeneralService generalService) {
        this.generalService = generalService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }
}
