package ee.webmedia.alfresco.destruction.model;

public enum DestructionJobStatus {
    IN_QUEUE, IN_PROGRESS, FINISHED, FAILED, PAUSED, PARTIALLY_DESTROYED, RESUME_PARTIALLY_DESTROYED;
}
