package ee.webmedia.alfresco.sharepoint;

import static ee.webmedia.alfresco.utils.SearchUtil.generateStringExactQuery;

import java.io.Serializable;
import java.util.*;

import org.alfresco.i18n.I18NUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import ee.webmedia.alfresco.classificator.enums.DocumentStatus;
import ee.webmedia.alfresco.common.bootstrap.AbstractNodeUpdater;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogFilter;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.utils.SearchUtil;

public class FirstFinishedDateUpdater extends AbstractNodeUpdater {

    @Override
    protected List<ResultSet> getNodeLoadingResultSet() throws Exception {
        String query = generateFinishedDocumentsQuery();
        List<ResultSet> resultSets = new ArrayList<ResultSet>();
        resultSets.add(searchService.query(generalService.getStore(), SearchService.LANGUAGE_LUCENE, query));
        resultSets.add(searchService.query(generalService.getArchivalsStoreRef(), SearchService.LANGUAGE_LUCENE, query));
        return resultSets;
    }

    private String generateFinishedDocumentsQuery() {
        return SearchUtil.joinQueryPartsAnd(SearchUtil.joinQueryPartsOr(
                SearchUtil.generatePropertyUnsetQuery(DocumentCommonModel.Props.DOCUMENT_IS_IMPORTED),
                SearchUtil.generatePropertyBooleanQuery(DocumentCommonModel.Props.DOCUMENT_IS_IMPORTED, false)),
                generateStringExactQuery(DocumentStatus.FINISHED.getValueName(), DocumentCommonModel.Props.DOC_STATUS));
    }

    @Override
    protected String[] updateNode(NodeRef docRef) throws Exception {
        try {
            Map<QName, Serializable> properties = nodeService.getProperties(docRef);
            Date firstFinishedDate = (Date) properties.get(DocumentCommonModel.Props.FIRST_FINISHED_DATE);
            if (firstFinishedDate == null) {
                List<LogEntry> logs = BeanHelper.getLogService().getLogEntries(getFinishedLogFilter(docRef.toString()), "created_date_time", false, -1, -1);
                Date logFinishedDate = CollectionUtils.isNotEmpty(logs) ? logs.get(0).getCreatedDateTime() : null;
                if (logFinishedDate != null) {
                    nodeService.setProperty(docRef, DocumentCommonModel.Props.FIRST_FINISHED_DATE, logFinishedDate);
                }
            }
        } catch (Exception e) {
            log.debug(e.getMessage());
        }

        return new String[]{docRef.toString()};
    }

    @Override
    protected boolean usePreviousState() {
        return false;
    }
    
    private LogFilter getFinishedLogFilter(String stringNodeRef) {
        LogFilter logFilter = new LogFilter();
        logFilter.setObjectId(Collections.singletonList(stringNodeRef));
        logFilter.setExactObjectId(true);
        Set<String> includedDescriptions = new HashSet<String>(3);
        includedDescriptions.add(MessageUtil.getMessage("document_log_status_proceedingFinish"));
        includedDescriptions.add(MessageUtil.getMessage("document_log_status_proceedingCompleted"));
        includedDescriptions.add(MessageUtil.getMessage("document_log_status_statusFinished"));
        logFilter.setIncludedDescriptions(includedDescriptions);
        return logFilter;
    }
}
