package ee.webmedia.alfresco.doclogix;

import static ee.webmedia.alfresco.common.web.BeanHelper.getUserService;
import static ee.webmedia.alfresco.doclogix.ImportUtil.getInteger;
import static ee.webmedia.alfresco.doclogix.ImportUtil.getString;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.repo.transaction.TransactionListenerAdapter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
/**
 * Imports documents and files from DoRa.
 */
public class DocLogixDocumentLogsImporter {

    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(DocLogixDocumentLogsImporter.class);
        
    
    
    protected static final char CSV_SEPARATOR = ';';
    
    private final DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    protected SAXReader xmlReader = new SAXReader();

    private File dataFolder;
    private File workFolder;
    private int batchSize;
     
    // [SPRING BEANS
    private TransactionService transactionService;
    private NodeService nodeService;
    private BehaviourFilter behaviourFilter;
    private DocLogixImporter docLogixImporter;
    

    public DocLogixDocumentLogsImporter(DocLogixImporter docLogixImporter) {
        
        
        setTransactionService(BeanHelper.getTransactionService());
        setNodeService(BeanHelper.getNodeService());
        setBehaviourFilter(BeanHelper.getPolicyBehaviourFilter());
        setDocLogixImporter(docLogixImporter);

        dateTimeFormat.setLenient(false);
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setBehaviourFilter(BehaviourFilter behaviourFilter) {
        this.behaviourFilter = behaviourFilter;
    }

    public void setDocLogixImporter(DocLogixImporter docLogixImporter) {
        this.docLogixImporter = docLogixImporter;
    }

    /**
     * Runs documents import process
     */
    public void runImport(File dataFolder, File workFolder, int batchSize, String defaultOwnerId, String institutionCode)
                    throws Exception {
        this.dataFolder = dataFolder;
        this.workFolder = workFolder;
        this.batchSize = batchSize;
        init();
        // Doc log import
        try {       	
        	
        	loadCompletedDocumentLogs();
            countDocumentLogs();
            createDocumentLogs();
        } finally {
            documentLogsToImportCount = 0;
        }

    }

    private void init() {
        documentLogsToImportCount = 0;
        completedDocumentLogsSet = new HashSet<>();
    }

    protected int documentLogsToImportCount;
    protected Set<Integer> completedDocumentLogsSet;


    protected File completedDocumentLogsFile;
    protected File failedDocumentLogsFile;
    

    protected void loadCompletedDocumentLogs() throws Exception {
        failedDocumentLogsFile = new File(workFolder, "failed_doc_logs.csv");
        OutputStream outputStream = new FileOutputStream(failedDocumentLogsFile);
        try {
            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
            outputStream.write("\ufeff".getBytes("UTF-8"));
        } finally {
            outputStream.close();
        }
        CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentLogsFile, true), CSV_SEPARATOR);
        try {
            writer.writeRecord(new String[] {
                    "logId",
                    "errorMsg"
            });
        } finally {
            writer.close();
        }

        completedDocumentLogsSet = new HashSet<>();

        completedDocumentLogsFile = new File(workFolder, "completed_doc_logs.csv");
        if (!completedDocumentLogsFile.exists()) {
            log.info("Skipping loading previously completed document logs, file does not exist: " + completedDocumentLogsFile);
            return;
        }

        log.info("Loading previously completed document logId-s from file " + completedDocumentLogsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedDocumentLogsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                Integer logId = Integer.parseInt(reader.get(0));
                completedDocumentLogsSet.add(logId);
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + completedDocumentLogsSet.size() + " previously completed document logId-s");

        
    }

    protected void countDocumentLogs() throws IOException {
    	
    	CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(new File(dataFolder, "logid.csv"))), CSV_SEPARATOR,
                Charset.forName("UTF-8"));
    	int docLogCounter = 0;
    	try {
        	
            reader.readHeaders();
            while (reader.readRecord()) {
            	docLogCounter++;
            }
        } finally {
            reader.close();
        }
    	
    	documentLogsToImportCount = docLogCounter;

    	
        log.info("Completed parsing logid.csv , got " + docLogCounter + " document logs");
    }

    private boolean hasUserNode(String doclogixUserName) {
    	if (StringUtils.isBlank(doclogixUserName)) {
    		return false;
    	}
    	Node userNode = null;
    	String userFullname = null;
    	
		userFullname = parseUserFullName(doclogixUserName);
		
		List<Node> userNodes = getUserService().searchUsers(userFullname, false, -1);
		if (userNodes != null && userNodes.size() == 1) {
			userNode = userNodes.get(0);
		}
    	
    		
    	if (userNode != null) {
    		String userId = (String)userNode.getProperties().get(ContentModel.PROP_USERNAME);
    		String userName = (String)userNode.getProperties().get(ContentModel.PROP_FIRSTNAME) + " " + (String)userNode.getProperties().get(ContentModel.PROP_LASTNAME);
    		if (userFullname == null || !userFullname.equals(userName)) {
    			return false;
    		}
    		
    		return true;
    	} else {
    		return false;
    	}
	}
    
    private boolean existsNodeRef(String nodeRefStr) {
    	if (StringUtils.isBlank(nodeRefStr)) {
    		return false;
    	}
    	NodeRef nodeRef = new NodeRef(nodeRefStr);
    	return nodeService.exists(nodeRef);
    }
    
    private String parseUserFullName(String doclogixUserName) {
    	String userFullname = doclogixUserName;
		if (doclogixUserName.contains("/")) {
			userFullname = StringUtils.substringBefore(doclogixUserName, "/").trim();
		}
		if (doclogixUserName.contains("(")) {
			userFullname = StringUtils.substringBefore(doclogixUserName, "(").trim();
		}
		
		return userFullname.trim();
    }


    private void checkStop() {
        docLogixImporter.checkStop();
    }

    private abstract class BatchProgress {
        int totalSize;
        int i;
        int completedSize;
        int thisRunCompletedSize;
        long thisRunStartTime;
        long startTime;
        String processName;

        private void init() {
            totalSize = 0;
            thisRunStartTime = System.currentTimeMillis();
            startTime = thisRunStartTime;
            i = 0;
            completedSize = 0;
        }

        abstract void executeBatch(CsvReader reader) throws Exception;

        void executeInTransaction(final CsvReader reader) {
            try {
                getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Object>() {
                    @Override
                    public Object execute() throws Throwable {
                        behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
                        executeBatch(reader);
                        return null;
                    }
                });
            } catch (Exception e) {
                log.error("Import batch of document logs failed, transaction rolled back, continuing with next batch", e);
            }
        }

        private void step(CsvReader reader) {
            executeInTransaction(reader);
            completedSize += batchSize;
            thisRunCompletedSize += batchSize;
            long endTime = System.currentTimeMillis();
            double completedPercent = (completedSize) * 100L / ((double) totalSize);
            double lastDocLogsPerSec = (i) * 1000L / ((double) (endTime - startTime));
            long thisRunTotalTime = endTime - thisRunStartTime;
            double totalDocLogsPerSec = (thisRunCompletedSize) * 1000L / ((double) thisRunTotalTime);
            long remainingSize = ((long) totalSize) - ((long) completedSize);
            long divisor = (thisRunCompletedSize) * 60000L;
            int etaMinutes = ((int) (remainingSize * thisRunTotalTime / divisor)) + 1;
            int etaHours = 0;
            if (etaMinutes > 59) {
                etaHours = etaMinutes / 60;
                etaMinutes = etaMinutes % 60;
            }
            String eta = etaMinutes + "m";
            if (etaHours > 0) {
                eta = etaHours + "h " + eta;
            }
            i = 0;
            String info = "%s: %6.2f%% completed - %7d of %7d, %5.1f items per second (last), %5.1f (total), ETA %s";
            log.info(String.format(info, processName, completedPercent, completedSize, totalSize, lastDocLogsPerSec, totalDocLogsPerSec, eta));
            startTime = endTime;
        }

        public void run() {
        	File sourceFile = new File(dataFolder, "logid.csv");
        	
        	if (!sourceFile.exists()) {
                log.error("logid.csv cannot be found");
                return;
            }
        	
        	CsvReader reader = null;
        	try {
	        	
        		reader = ImportUtil.createDataReader(sourceFile);
        		
	        	boolean canContinue = reader.readHeaders() && reader.readRecord();
	            init();
	            checkStop();
	            while (canContinue) {
	                i++;
	                if (i <= batchSize) {
	                    step(reader);
	                    checkStop();
	                }
	                canContinue = reader.readRecord();
	            }
        	} catch (Throwable t) {
        		log.error("Error importing from logid.csv", t);
        	} finally {
        		if (reader != null) {
        			reader.close();
        		}
            }

            
        }

    }
    
    private static interface CsvWriterClosure {
        void execute(CsvWriter writer) throws IOException;

        String[] getHeaders();
    }

    private static void bindCsvWriteAfterCommit(final File file, final CsvWriterClosure closure) {
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
                try {
                    // Write created document logs
                    boolean exists = file.exists();
                    if (!exists) {
                        OutputStream outputStream = new FileOutputStream(file);
                        try {
                            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
                            outputStream.write("\ufeff".getBytes("UTF-8"));
                        } finally {
                            outputStream.close();
                        }
                    }
                    CsvWriter writer = new CsvWriter(new FileWriter(file, true), CSV_SEPARATOR);
                    try {
                        if (!exists) {
                            writer.writeRecord(closure.getHeaders());
                        }
                        closure.execute(writer);
                    } finally {
                        writer.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Error writing file '" + file + "': " + e.getMessage(), e);
                }
            }
        });
    }
    

    private class DocumensBatchProgress extends BatchProgress {
        {
            processName = "Document logs import";
        }

        @Override
        void executeBatch(CsvReader reader) throws Exception {
            createDocumentsBatch(reader);
        }
    }

    private void createDocumentLogs() throws Exception {
        if (documentLogsToImportCount == 0) {
            log.info("Skipping document logs import, no document logs found");
            return;
        }
        log.info("Starting document logs import. Total = " + documentLogsToImportCount);
        DocumensBatchProgress batchProgress = new DocumensBatchProgress();
        
        batchProgress.run();
        
        log.info("Document logs IMPORT COMPLETE :)");
    }

    static class ImportedDocLog {
        Integer logId;
        String docNodeRef;
        String msg;
        

        public ImportedDocLog(Integer logId, String docNodeRef) {
            this.logId = logId;
            this.docNodeRef = docNodeRef;
        }
        
        public ImportedDocLog(Integer logId, String docNodeRef, String msg) {
            this.logId = logId;
            this.docNodeRef = docNodeRef;
            this.msg = msg;
        }
    }

    protected void createDocumentsBatch(CsvReader csvReader) throws Exception {
        final Map<Integer, ImportedDocLog> batchCompletedDocumentsMap = new TreeMap<Integer, ImportedDocLog>();
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
            }

            @Override
            public void afterRollback() {
                completedDocumentLogsSet.removeAll(batchCompletedDocumentsMap.keySet());
            }
        });
        
        int docLogsCounter = 0;
        boolean canContinue = true;
        
        while (canContinue && docLogsCounter < batchSize) {
        	docLogsCounter++;
            Integer logId = getInteger(csvReader, 1); // docLogId
            if (log.isTraceEnabled()) {
                log.trace("Processing document log Id=" + logId);
            }
            if (isCompletedDocLog(logId)) {
            	log.trace("Skipping, already added: document log Id=" + logId);
            	continue;
            }
            
            
            try {
                ImportedDocLog docLog = createDocLog(logId, csvReader);
                if (StringUtils.isBlank(docLog.msg)) {
                	batchCompletedDocumentsMap.put(logId, docLog);
                	completedDocumentLogsSet.add(logId); // Add immediately to completedDocumentLogsSet, because other code wants to access it
                	docLogixImporter.getStatus().incrCount();
                } else {
	                CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentLogsFile, true), CSV_SEPARATOR);
	                try {
	                    writer.writeRecord(new String[] { logId.toString(), docLog.msg });
	                } finally {
	                    writer.close();
	                }
	                docLogixImporter.getStatus().incrFailed();
                }
            } catch (Throwable e) {
                CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentLogsFile, true), CSV_SEPARATOR);
                try {
                    writer.writeRecord(new String[] { logId.toString(), e.getMessage() });
                } finally {
                    writer.close();
                }
                docLogixImporter.getStatus().incrFailed();
                log.error("Error importing document log " + logId + ": " + e.getMessage(), e);
            }
            
            if (docLogsCounter < batchSize) {
            	canContinue = csvReader.readRecord();
            }
        }
        bindCsvWriteAfterCommit(completedDocumentLogsFile, new CsvWriterClosure() {

            @Override
            public String[] getHeaders() {
                return new String[] {
                        "logId",
                        "nodeRef"
                };
            }

            @Override
            public void execute(CsvWriter writer) throws IOException {
                for (Entry<Integer, ImportedDocLog> entry : batchCompletedDocumentsMap.entrySet()) {
                    ImportedDocLog docLog = entry.getValue();
                    writer.writeRecord(new String[] {
                    		docLog.logId.toString(),
                    		docLog.docNodeRef == null ? "" : docLog.docNodeRef
                    });
                }
            }
        });
    }

    private ImportedDocLog createDocLog(Integer logId, CsvReader csvReader) throws DocumentException, ParseException, IOException {
        return importDocLog(csvReader, logId);
    }
   

    private ImportedDocLog importDocLog(CsvReader csvReader, Integer logId) throws DocumentException,
    ParseException, IOException {

        String docNodeRef = getString(csvReader, 2);
        String logAction = getString(csvReader, 3);
        String logCreatorName = getString(csvReader, 4);
        Date logCreatedTime = ImportUtil.getDateTime(csvReader, 5, dateTimeFormat);
        
        
        if (!existsNodeRef(docNodeRef)) {
        	return new ImportedDocLog(logId, docNodeRef, "doc nodeRef does not exit");
        }
        
		LogEntry logEntry = new LogEntry();
		logEntry.setComputerIp("127.0.0.1");
		logEntry.setComputerName("localhost");
		logEntry.setLevel(LogObject.DOCUMENT.getLevel());
		logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
		logEntry.setCreatorId("IMPORT");
		logEntry.setCreatorName(hasUserNode(logCreatorName) ? logCreatorName : "System User");
		logEntry.setEventDescription(logAction);
		logEntry.setObjectId(docNodeRef);
		BeanHelper.getLogService().addImportedLogEntry(logEntry, logCreatedTime);

		
		return new ImportedDocLog(logId, docNodeRef);
		
    }

    private boolean isCompletedDocLog(Integer id) {
    	return completedDocumentLogsSet.contains(id);
    }
    
    
    
    

    // HELPER METHODS
    /** RetryingTransactionHelper that only tries to do things once. */
    private RetryingTransactionHelper getTransactionHelper() {
        RetryingTransactionHelper helper = new RetryingTransactionHelper();
        helper.setMaxRetries(1);
        helper.setTransactionService(transactionService);
        return helper;
    }
    

}
