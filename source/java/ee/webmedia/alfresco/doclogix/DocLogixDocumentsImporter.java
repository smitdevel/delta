package ee.webmedia.alfresco.doclogix;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentDynamicService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentListService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNamespaceService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getUserService;
import static ee.webmedia.alfresco.doclogix.ImportUtil.getString;
import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.OWNER_ID;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.TreeMap;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.repo.transaction.TransactionListenerAdapter;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.springframework.util.Assert;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.classificator.constant.FieldType;
import ee.webmedia.alfresco.classificator.enums.DocumentStatus;
import ee.webmedia.alfresco.classificator.enums.PublishToAdr;
import ee.webmedia.alfresco.classificator.enums.StorageType;
import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.common.web.WmNode;
import ee.webmedia.alfresco.docadmin.service.Field;
import ee.webmedia.alfresco.docconfig.service.DynamicPropertyDefinition;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamic;
import ee.webmedia.alfresco.doclogix.DocLogixDocumentsMapper.Mapping;
import ee.webmedia.alfresco.doclogix.DocLogixDocumentsMapper.PropertyValue;
import ee.webmedia.alfresco.doclogix.DocLogixDocumentsMapper.TypeInfo;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.document.model.DocumentSpecificModel;
import ee.webmedia.alfresco.document.service.DocumentService;
import ee.webmedia.alfresco.functions.model.FunctionsModel;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.orgstructure.model.OrganizationStructureModel;
import ee.webmedia.alfresco.series.model.SeriesModel;
import ee.webmedia.alfresco.utils.RepoUtil;
import ee.webmedia.alfresco.utils.UserUtil;
import ee.webmedia.alfresco.volume.model.VolumeModel;
/**
 * Imports documents and files from DocLogix.
 */
public class DocLogixDocumentsImporter {

    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(DocLogixDocumentsImporter.class);
    
    
        
    
    private static final String DUMMY_USER_ID = "99999999999";



    protected static final char CSV_SEPARATOR = ';';
    private static final String CREATOR_MODIFIER = "DHS";
    private static final FastDateFormat staticDateTimeFormat = FastDateFormat.getInstance("dd.MM.yyyy HH:mm:ss");
    private static final FastDateFormat staticDateFormat = FastDateFormat.getInstance("dd.MM.yyyy");

    private final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private final DateFormat dateTimeFormatddMMyy = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
    private final DateFormat dateTimeFormatdMMyy = new SimpleDateFormat("d.MM.yy HH:mm:ss");

    protected SAXReader xmlReader = new SAXReader();

    private File dataFolder;
    private File workFolder;
    private int batchSize;
    private String institutionCode;
    private String defaultOwnerId;
    private String defaultOwnerName;
    private String defaultOwnerEmail;
    private String defaultOwnerJobTitle;
    private String defaultOwnerPhone;
    private List<String> defaultOwnerOrgUnit;
    private String defaultOwnerServiceRank;
    private String defaultOwnerWorkAddress;
    
    private Collection<StoreRef> storeRefs = new LinkedHashSet<StoreRef>();
    
    
    // [SPRING BEANS
    private TransactionService transactionService;
    private DocumentService documentService;
    private GeneralService generalService;
    private FileFolderService fileFolderService;
    private NodeService nodeService;
    private DocLogixDocumentsMapper docLogixDocumentsMapper;
    private BehaviourFilter behaviourFilter;
    private DocLogixImporter docLogixImporter;
    

    public DocLogixDocumentsImporter(DocLogixImporter doraImporter) {
        
        
        setDocumentService(BeanHelper.getDocumentService());
        setTransactionService(BeanHelper.getTransactionService());
        setGeneralService(BeanHelper.getGeneralService());
        setFileFolderService(BeanHelper.getFileFolderService());
        setNodeService(BeanHelper.getNodeService());
        docLogixDocumentsMapper = new DocLogixDocumentsMapper();
        docLogixDocumentsMapper.init();
        docLogixDocumentsMapper.setNamespaceService(getNamespaceService());
        docLogixDocumentsMapper.setDictionaryService(BeanHelper.getDictionaryService());
        docLogixDocumentsMapper.setGeneralService(BeanHelper.getGeneralService());
        setBehaviourFilter(BeanHelper.getPolicyBehaviourFilter());
        setDoraImporter(doraImporter);

        dateFormat.setLenient(false);
        dateTimeFormat.setLenient(false);
    }

    // INJECTORS
    public void setGeneralService(GeneralService generalService) {
        this.generalService = generalService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setDocLogixDocumentsMapper(DocLogixDocumentsMapper docLogixDocumentsMapper) {
        this.docLogixDocumentsMapper = docLogixDocumentsMapper;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setBehaviourFilter(BehaviourFilter behaviourFilter) {
        this.behaviourFilter = behaviourFilter;
    }

    public void setDoraImporter(DocLogixImporter doraImporter) {
        this.docLogixImporter = doraImporter;
    }

    /**
     * Runs documents import process
     */
    public void runImport(File dataFolder, File workFolder, int batchSize, String defaultOwnerId, String institutionCode)
                    throws Exception {
        this.dataFolder = dataFolder;
        this.workFolder = workFolder;
        this.batchSize = batchSize;
        this.institutionCode = institutionCode;
        this.defaultOwnerId = defaultOwnerId;
        Map<QName, Serializable> defaultUserProps = getUserService().getUserProperties(defaultOwnerId);
        this.defaultOwnerName = UserUtil.getPersonFullName1(defaultUserProps);
        this.defaultOwnerEmail = (String)defaultUserProps.get(ContentModel.PROP_EMAIL);
        this.defaultOwnerPhone = (String)defaultUserProps.get(ContentModel.PROP_TELEPHONE);
        this.defaultOwnerJobTitle = (String)defaultUserProps.get(ContentModel.PROP_JOBTITLE);
        this.defaultOwnerOrgUnit = (List<String>)defaultUserProps.get(OrganizationStructureModel.Props.ORGANIZATION_PATH);
        this.defaultOwnerServiceRank = (String)defaultUserProps.get(ContentModel.PROP_SERVICE_RANK);
        this.defaultOwnerWorkAddress = (String)defaultUserProps.get(ContentModel.PROP_STREET_HOUSE);
        init();
        // Doc import
        try {        	
        	File completedStructureFile = new File(workFolder, StructureImporter.COMPLETED_FILENAME);
        	if (!completedStructureFile.exists()) {
                log.info("Skipping documents import, struucture was not imported yet, file does not exist: " + completedStructureFile);
                return;
            }
        	storeRefs.add(generalService.getStore());
        	storeRefs.add(generalService.getArchivalsStoreRef());
        	
            loadCompletedCaseFilesVolumes();
            countDocuments();
            loadCompletedDocuments();
            loadPostponedAssocs();
            createDocuments();
            postProcessPostponedAssoc();
        } finally {
        	writePostponedAssocs();
            documentsToImportCount = 0;
            mappings = null;
            postponedAssocs = null;
            completedCaseFiles = null;
            completedVolumes = null;
        }

    }

    private void init() {
        documentsToImportCount = 0;
        completedDocumentsMap = new TreeMap<String, NodeRef>();
        postponedAssocs = new TreeMap<String, List<PostponedAssoc>>();
        postponedAssocsCommited = new TreeMap<String, List<PostponedAssoc>>();
        completedCaseFiles = new HashMap<String, DocLogixVolume>();
        completedVolumes = new HashMap<String, List<DocLogixVolume>>();
        mappings = null;
        seriesMappings = new HashMap<>();
        filesToProceed = null;
        docLogixDocumentsMapper.init();
    }

    protected int documentsToImportCount;
    protected NavigableMap<String /* documentId */, NodeRef> completedDocumentsMap;
    protected NavigableMap<String /* documentId */, NodeRef> readyToIndexDocumentsMap;
    protected NavigableMap<String /* documentId */, List<PostponedAssoc>> postponedAssocs;
    protected NavigableMap<String /* documentId */, List<PostponedAssoc>> postponedAssocsCommited;
    private final Map<String, Pair<NodeRef, Map<QName, Serializable>>> userDataByUserName = new HashMap<String, Pair<NodeRef, Map<QName, Serializable>>>();
    private Map<String, DocLogixVolume> completedCaseFiles;
    private Map<String, List<DocLogixVolume>> completedVolumes;


    protected NavigableSet<Integer> filesToProceed;
    protected File completedDocumentsFile;
    protected File failedDocumentsFile;
    protected File postponedAssocsFile;
    

    private Map<String, Mapping> mappings;
    private Map<String, String> seriesMappings = new HashMap<>();
    
    
    private void loadCompletedCaseFilesVolumes() throws IOException {
    	completedCaseFiles = new HashMap<String, DocLogixVolume>();
        completedVolumes = new HashMap<String, List<DocLogixVolume>>();
        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(new File(workFolder, "completed_volumes_cases.csv"))), CSV_SEPARATOR,
                Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String volumeType = reader.get(0);
                String mark = reader.get(1);
                NodeRef nodeRef = new NodeRef(reader.get(4));
                Date validFrom = null;
                try {
                	validFrom = dateFormat.parse(reader.get(5));
                } catch (Throwable t) {}
                Date validTo = null;
                try {
                	validTo = dateFormat.parse(reader.get(6));
                } catch (Throwable e) {}
                String seriesIdentifier = reader.get(8);
                String seriesTitle = reader.get(9);
                String function = reader.get(7);
                DocLogixVolume volume = new DocLogixVolume();
                volume.nodeRef = nodeRef;
                volume.mark = mark;
                volume.seriesIdentifier = seriesIdentifier;
                volume.seriesTitle = seriesTitle;
                volume.function = function;
                volume.validFrom = validFrom;
                volume.validTo = validTo;
                if ("caseFile".equals(volumeType)) {
                	completedCaseFiles.put(mark, volume);
                } else {
                	List<DocLogixVolume> volumes = completedVolumes.get(seriesIdentifier);
                	if (volumes == null) {
                		volumes = new ArrayList<DocLogixVolume>();
                		completedVolumes.put(seriesIdentifier + " " + seriesTitle, volumes);
                	}
                	volumes.add(volume);
                	
                }
            }
        } finally {
            reader.close();
        }
    }

    protected void countDocuments() throws IOException {
    	
    	CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(new File(dataFolder, "dokumendid.csv"))), CSV_SEPARATOR,
                Charset.forName("UTF-8"));
    	int docCounter = 0;
    	try {
        	
            reader.readHeaders();
            while (reader.readRecord()) {
            	docCounter++;
            }
        } finally {
            reader.close();
        }
    	
    	documentsToImportCount = docCounter;

    	
        log.info("Completed parsing dokumendid.csv , got " + docCounter + " documents");
    }

    private boolean findUserNode(String doclogixUserName, Map<QName, Serializable> props, QName idQname, QName nameQname, QName emailQname) {
    	Node userNode = null;
    	String userFullname = null;
    	if (StringUtils.isNotBlank(doclogixUserName)) {
    		userFullname = parseUserFullName(doclogixUserName);
    		
    		List<Node> userNodes = getUserService().searchUsers(userFullname, false, -1);
    		if (userNodes != null && userNodes.size() == 1) {
    			userNode = userNodes.get(0);
    		}
    	}
    	if (userNode != null) {
    		String userId = (String)userNode.getProperties().get(ContentModel.PROP_USERNAME);
    		String userName = (String)userNode.getProperties().get(ContentModel.PROP_FIRSTNAME) + " " + (String)userNode.getProperties().get(ContentModel.PROP_LASTNAME);
    		if (userFullname == null || !userFullname.equals(userName)) {
    			return false;
    		}
    		String email = (String)userNode.getProperties().get(ContentModel.PROP_EMAIL);
    		if (idQname != null) {
    			props.put(idQname, userId);
    		}
    		if (nameQname != null) {
    			props.put(nameQname, (StringUtils.isNotBlank(userName)?userName:userId));
    		}
    		if (emailQname != null && StringUtils.isNotBlank(email)) {
    			props.put(emailQname, email);
    		}
    		return true;
    	} else {
    		return false;
    	}
	}
    
    private String parseUserFullName(String doclogixUserName) {
    	String userFullname = doclogixUserName;
		if (doclogixUserName.contains("/")) {
			userFullname = StringUtils.substringBefore(doclogixUserName, "/").trim();
		}
		if (doclogixUserName.contains("(")) {
			userFullname = StringUtils.substringBefore(doclogixUserName, "(").trim();
		}
		
		return userFullname.trim();
    }

    private void setOwnerProperties(Map<QName, Serializable> props, TypeInfo typeInfo) {
        String gpOwnerName = StringUtils.stripToNull((String) props.get(DocumentCommonModel.Props.OWNER_NAME));
        String gpSignerName = StringUtils.stripToNull((String) props.get(DocumentCommonModel.Props.SIGNER_NAME));
        
        boolean ownerFound = false;
        boolean signerFound = false;
        if (StringUtils.isNotBlank(gpOwnerName)) {
        	QName emailQname = null;
        	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(DocumentCommonModel.Props.OWNER_EMAIL.getLocalName());
    		if (pair != null) {
    			emailQname = DocumentCommonModel.Props.OWNER_EMAIL;
    		}
        	ownerFound = findUserNode(gpOwnerName, props, OWNER_ID, DocumentCommonModel.Props.OWNER_NAME, emailQname);
        	if (!ownerFound) {
        		addMissingFieldToCommment(DocumentCommonModel.Props.OWNER_NAME.getLocalName() + ":" + gpOwnerName, props, typeInfo);
       		}
        }
        
        if (!ownerFound && StringUtils.isNotBlank(defaultOwnerId)) {
        	props.put(DocumentCommonModel.Props.OWNER_ID, defaultOwnerId);
        	props.put(DocumentCommonModel.Props.OWNER_NAME, defaultOwnerName);
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_EMAIL)) {
        		props.put(DocumentCommonModel.Props.OWNER_EMAIL, defaultOwnerEmail);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_PHONE)) {
        		props.put(DocumentCommonModel.Props.OWNER_PHONE, defaultOwnerPhone);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_JOB_TITLE)) {
        		props.put(DocumentCommonModel.Props.OWNER_JOB_TITLE, defaultOwnerJobTitle);
        	}
        	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT)) {
        		props.put(DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT, (ArrayList)defaultOwnerOrgUnit);
        	}
        	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_SERVICE_RANK)) {
        		props.put(DocumentDynamicModel.Props.OWNER_SERVICE_RANK, defaultOwnerServiceRank);
        	}
        	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_WORK_ADDRESS)) {
        		props.put(DocumentDynamicModel.Props.OWNER_WORK_ADDRESS, defaultOwnerWorkAddress);
        	}

        }
        
        if (StringUtils.isNotBlank(gpSignerName)) {
       		signerFound = findUserNode(gpSignerName, props, null, DocumentCommonModel.Props.SIGNER_NAME, null);
        }
        
    }
    
    private boolean hasField(TypeInfo typeInfo, QName fieldQName) {
    	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldQName.getLocalName());
		return pair != null;
    }


    private void checkStop() {
        docLogixImporter.checkStop();
    }

    private abstract class BatchProgress {
        int totalSize;
        int completedSize;
        int thisRunCompletedSize;
        long thisRunStartTime;
        long startTime;
        String processName;

        private void init() {
            totalSize = 0;
            thisRunStartTime = System.currentTimeMillis();
            startTime = thisRunStartTime;
            completedSize = 0;
        }

        abstract void executeBatch(CsvReader reader) throws Exception;

        void executeInTransaction(final CsvReader reader) {
            try {
                getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Object>() {
                    @Override
                    public Object execute() throws Throwable {
                        behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
                        executeBatch(reader);
                        return null;
                    }
                });
            } catch (Exception e) {
                log.error("Import batch of documents failed, transaction rolled back, continuing with next batch", e);
            }
        }

        private void step(CsvReader reader) {
            executeInTransaction(reader);
            completedSize += batchSize;
            thisRunCompletedSize += batchSize;
            long endTime = System.currentTimeMillis();
            double completedPercent = (completedSize) * 100L / ((double) totalSize);
            double lastDocsPerSec = (batchSize) * 1000L / ((double) (endTime - startTime));
            long thisRunTotalTime = endTime - thisRunStartTime;
            double totalDocsPerSec = (thisRunCompletedSize) * 1000L / ((double) thisRunTotalTime);
            long remainingSize = ((long) totalSize) - ((long) completedSize);
            long divisor = (thisRunCompletedSize) * 60000L;
            int etaMinutes = ((int) (remainingSize * thisRunTotalTime / divisor)) + 1;
            int etaHours = 0;
            if (etaMinutes > 59) {
                etaHours = etaMinutes / 60;
                etaMinutes = etaMinutes % 60;
            }
            String eta = etaMinutes + "m";
            if (etaHours > 0) {
                eta = etaHours + "h " + eta;
            }
            String info = "%s: %6.2f%% completed - %7d of %7d, %5.1f items per second (last), %5.1f (total), ETA %s";
            log.info(String.format(info, processName, completedPercent, completedSize, totalSize, lastDocsPerSec, totalDocsPerSec, eta));
            startTime = endTime;
        }

        public void run() {
        	File sourceFile = new File(dataFolder, "dokumendid.csv");
        	
        	if (!sourceFile.exists()) {
                log.error("dokumendid.csv cannot be found");
                return;
            }
        	
        	CsvReader reader = null;
        	try {
	        	
        		reader = ImportUtil.createDataReader(sourceFile);
        		
	        	boolean canContinue = reader.readHeaders() && reader.readRecord();
	            init();
	            checkStop();
	            while (canContinue) {
	                step(reader);
	                checkStop();
	                canContinue = reader.readRecord();
	            }
        	} catch (Throwable t) {
        		log.error("Error importing from dokumendid.csv", t);
        	} finally {
        		if (reader != null) {
        			reader.close();
        		}
            }

            
        }

    }
    
    private static interface CsvWriterClosure {
        void execute(CsvWriter writer) throws IOException;

        String[] getHeaders();
    }

    private static void bindCsvWriteAfterCommit(final File file, final CsvWriterClosure closure) {
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
                try {
                    // Write created documents
                    boolean exists = file.exists();
                    if (!exists) {
                        OutputStream outputStream = new FileOutputStream(file);
                        try {
                            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
                            outputStream.write("\ufeff".getBytes("UTF-8"));
                        } finally {
                            outputStream.close();
                        }
                    }
                    CsvWriter writer = new CsvWriter(new FileWriter(file, true), CSV_SEPARATOR);
                    try {
                        if (!exists) {
                            writer.writeRecord(closure.getHeaders());
                        }
                        closure.execute(writer);
                    } finally {
                        writer.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Error writing file '" + file + "': " + e.getMessage(), e);
                }
            }
        });
    }
    
    

    protected void loadCompletedDocuments() throws Exception {
        failedDocumentsFile = new File(workFolder, "failed_docs.csv");
        OutputStream outputStream = new FileOutputStream(failedDocumentsFile);
        try {
            // the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the file in correct encoding
            outputStream.write("\ufeff".getBytes("UTF-8"));
        } finally {
            outputStream.close();
        }
        CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
        try {
            writer.writeRecord(new String[] {
                    "documentFileName",
                    "errorMsg"
            });
        } finally {
            writer.close();
        }

        completedDocumentsMap = new TreeMap<String, NodeRef>();

        completedDocumentsFile = new File(workFolder, "completed_docs.csv");
        if (!completedDocumentsFile.exists()) {
            log.info("Skipping loading previously completed documentId-s, file does not exist: " + completedDocumentsFile);
            return;
        }

        log.info("Loading previously completed documentId-s from file " + completedDocumentsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedDocumentsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String documentId = reader.get(0);
                String nodeRefString = reader.get(1);
                if (StringUtils.isNotBlank(nodeRefString)) {
                    NodeRef documentRef = new NodeRef(nodeRefString);
                    completedDocumentsMap.put(documentId, documentRef);
                }
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + completedDocumentsMap.size() + " previously completed documentId-s");

        
    }

    private class DocumensBatchProgress extends BatchProgress {
        {
            processName = "Documents import";
        }

        @Override
        void executeBatch(CsvReader reader) throws Exception {
            createDocumentsBatch(reader);
        }
    }

    private void createDocuments() throws Exception {
        if (documentsToImportCount == 0) {
            log.info("Skipping documents import, no documents found");
            return;
        }
        log.info("Starting documents import. Total = " + documentsToImportCount);
        DocumensBatchProgress batchProgress = new DocumensBatchProgress();
        try {
            batchProgress.run();
        } finally {
            getDocumentListService().updateDocCounters();
        }
        log.info("Documents IMPORT COMPLETE :)");
    }

    static class ImportedDocument {
        String documentId;
        NodeRef nodeRef;
        String docUrl;
        String msg;

        public ImportedDocument(String documentId, NodeRef nodeRef) {
            this.documentId = documentId;
            this.nodeRef = nodeRef;
            this.docUrl = StringEscapeUtils.escapeHtml(BeanHelper.getDocumentTemplateService().getDocumentUrl(nodeRef));
        }
        
        public ImportedDocument(String documentId, String msg) {
            this.documentId = documentId;
            this.msg = msg;
        }
    }

    protected void createDocumentsBatch(CsvReader csvReader) throws Exception {
        final Map<String, ImportedDocument> batchCompletedDocumentsMap = new TreeMap<String, ImportedDocument>();
        AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
            @Override
            public void afterCommit() {
                postponedAssocsCommited = new TreeMap<String, List<PostponedAssoc>>(postponedAssocs);
            }

            @Override
            public void afterRollback() {
                postponedAssocs = new TreeMap<String, List<PostponedAssoc>>(postponedAssocsCommited);
                completedDocumentsMap.keySet().removeAll(batchCompletedDocumentsMap.keySet());
            }
        });
        
        StoreRef archivalStoreRef =  generalService.getArchivalsStoreRef();
        
        int docsCounter = 0;
        boolean canContinue = true;
        
        while (canContinue) {
        	docsCounter++;
            String documentId = getString(csvReader, 5); // docLogixId
            if (log.isTraceEnabled()) {
                log.trace("Processing documentId=" + documentId);
            }
            
            
            
            try {
                ImportedDocument doc = createDocument(documentId, csvReader, archivalStoreRef);
                if (doc.nodeRef != null) {
                	batchCompletedDocumentsMap.put(documentId, doc);
                	completedDocumentsMap.put(documentId, doc.nodeRef); // Add immediately to completedDocumentsMap, because other code wants to access it
                	docLogixImporter.getStatus().incrCount();
                } else {
	                CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
	                try {
	                    writer.writeRecord(new String[] { documentId, doc.msg });
	                } finally {
	                    writer.close();
	                }
	                docLogixImporter.getStatus().incrFailed();
                }
            } catch (Throwable e) {
                CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentsFile, true), CSV_SEPARATOR);
                try {
                    writer.writeRecord(new String[] { documentId, e.getMessage() });
                } finally {
                    writer.close();
                }
                docLogixImporter.getStatus().incrFailed();
                log.error("Error importing document " + documentId + ": " + e.getMessage(), e);
            }
            
            if (docsCounter >= batchSize) {
            	canContinue = false;
            } else {
            	canContinue = csvReader.readRecord();
            }
        }
        bindCsvWriteAfterCommit(completedDocumentsFile, new CsvWriterClosure() {

            @Override
            public String[] getHeaders() {
                return new String[] {
                        "documentId",
                        "nodeRef",
                        "docUrl"
                };
            }

            @Override
            public void execute(CsvWriter writer) throws IOException {
                for (Entry<String, ImportedDocument> entry : batchCompletedDocumentsMap.entrySet()) {
                    ImportedDocument doc = entry.getValue();
                    writer.writeRecord(new String[] {
                            doc.documentId.toString(),
                            	doc.nodeRef == null ? "" : doc.nodeRef.toString(),
                            		doc.docUrl == null ? "" : doc.docUrl
                    });
                }
            }
        });
    }

    private ImportedDocument createDocument(String documentId, CsvReader csvReader, StoreRef archivalStoreRef) throws DocumentException, ParseException, IOException {
        return importDoc(csvReader, documentId, mappings, archivalStoreRef);
    }
    

    private Map<QName, Serializable> mapProperties(CsvReader csvReader, TypeInfo typeInfo, Map<QName, Serializable> parentPropsMap) throws IOException {
        Map<QName, Serializable> propsMap = new HashMap<>();
        
        List<String> comments = new ArrayList<>();
        
        
        mapProperty("docName", getString(csvReader, 7), typeInfo, propsMap, comments);
        
        mapProperty("accessRestriction", getString(csvReader, 8), typeInfo, propsMap, comments);
        mapProperty("accessRestrictionReason", getString(csvReader, 9), typeInfo, propsMap, comments);
        mapProperty("accessRestrictionBeginDate", getString(csvReader, 10), typeInfo, propsMap, comments);
        mapProperty("accessRestrictionEndDate", getString(csvReader, 11), typeInfo, propsMap, comments);
        mapProperty("ownerName", getString(csvReader, 12), typeInfo, propsMap, comments);
        
        
        String docStatus = getString(csvReader, 13);
        if (!DocumentStatus.FINISHED.getValueName().equalsIgnoreCase(docStatus)) {
        	throw new RuntimeException("Invalid document status: " + docStatus);
        }
        mapProperty("docStatus", docStatus, typeInfo, propsMap, comments);
        
        String storageType = getString(csvReader, 14);
        if (!StorageType.DIGITAL.getValueName().equalsIgnoreCase(storageType)) {
        	throw new RuntimeException("Invalid storage type: " + storageType);
        }
        mapProperty("storageType", storageType, typeInfo, propsMap, comments);
        
        
        mapProperty("regDateTime", getString(csvReader, 15), typeInfo, propsMap, comments);
        mapProperty("regNumber", getString(csvReader, 16), typeInfo, propsMap, comments);
        mapProperty("ownerOrgStructUnit", getString(csvReader, 17), typeInfo, propsMap, comments);
        mapProperty("contractperson", getString(csvReader, 18), typeInfo, propsMap, comments);
        mapProperty("recipientName", getString(csvReader, 19), typeInfo, propsMap, comments);
        mapProperty("senderRegDate", getString(csvReader, 20), typeInfo, propsMap, comments);
        mapProperty("comment", getString(csvReader, 21), typeInfo, propsMap, comments);
        mapProperty("dateone", getString(csvReader, 22), typeInfo, propsMap, comments);
        mapProperty("content", getString(csvReader, 23), typeInfo, propsMap, comments);
        mapProperty("signerName", getString(csvReader, 24), typeInfo, propsMap, comments);
        mapProperty("senderRegNumber", getString(csvReader, 25), typeInfo, propsMap, comments);
        mapProperty("author", getString(csvReader, 26), typeInfo, propsMap, comments);
        mapProperty("dueDate", getString(csvReader, 27), typeInfo, propsMap, comments);
        mapProperty("contracttypes", getString(csvReader, 28), typeInfo, propsMap, comments);
        mapProperty("beginDate", getString(csvReader, 29), typeInfo, propsMap, comments);
        mapProperty("employees", getString(csvReader, 30), typeInfo, propsMap, comments);
        mapProperty("applicantOrgStructUnit", getString(csvReader, 31), typeInfo, propsMap, comments);
        mapProperty("applicant", getString(csvReader, 32), typeInfo, propsMap, comments);
        mapProperty("inforcefromDate", getString(csvReader, 33), typeInfo, propsMap, comments);
        mapProperty("validitydocument", getString(csvReader, 34), typeInfo, propsMap, comments);
        mapProperty("orgstructure", getString(csvReader, 35), typeInfo, propsMap, comments);
        mapProperty("partySigner", getString(csvReader, 36), typeInfo, propsMap, comments);
        mapProperty("transmittalMode", getString(csvReader, 37), typeInfo, propsMap, comments);
        mapProperty("senderName", getString(csvReader, 38), typeInfo, propsMap, comments);
        mapProperty("senderRegNumber", getString(csvReader, 39), typeInfo, propsMap, comments);
        mapProperty("complienceDate", getString(csvReader, 40), typeInfo, propsMap, comments);
        mapProperty("recipientPersonName", getString(csvReader, 41), typeInfo, propsMap, comments);
        mapProperty("connectedperson", getString(csvReader, 42), typeInfo, propsMap, comments);
        mapProperty("leaveType", getString(csvReader, 43), typeInfo, propsMap, comments);
        mapProperty("procurementarrangement", getString(csvReader, 44), typeInfo, propsMap, comments);
        mapProperty("expectedExpenseSum", getString(csvReader, 45), typeInfo, propsMap, comments);
        mapProperty("financingSource", getString(csvReader, 46), typeInfo, propsMap, comments);
        mapProperty("oneemployee", getString(csvReader, 47), typeInfo, propsMap, comments);
        mapProperty("researcharea", getString(csvReader, 48), typeInfo, propsMap, comments);
        mapProperty("programtext", getString(csvReader, 49), typeInfo, propsMap, comments);
        mapProperty("errandSummaryProjectCode", getString(csvReader, 50), typeInfo, propsMap, comments);
        mapProperty("employee", getString(csvReader, 51), typeInfo, propsMap, comments);
        mapProperty("countrie", getString(csvReader, 52), typeInfo, propsMap, comments);
        mapProperty("contractSum", getString(csvReader, 53), typeInfo, propsMap, comments);
        mapProperty("currencyvalusum", getString(csvReader, 54), typeInfo, propsMap, comments);
        mapProperty("errandExpectedExpense", getString(csvReader, 55), typeInfo, propsMap, comments);
        mapProperty("selffinancingSum", getString(csvReader, 56), typeInfo, propsMap, comments);
        mapProperty("startoverhead", getString(csvReader, 57), typeInfo, propsMap, comments);
        mapProperty("Overheadpercent", getString(csvReader, 58), typeInfo, propsMap, comments);
        mapProperty("partyName", getString(csvReader, 59), typeInfo, propsMap, comments);
        mapProperty("termless", getString(csvReader, 60), typeInfo, propsMap, comments);
        mapProperty("registrationnumbermanually", getString(csvReader, 61), typeInfo, propsMap, comments);
        mapProperty("appendixmanually", getString(csvReader, 62), typeInfo, propsMap, comments);
        mapProperty("attestationposition", getString(csvReader, 63), typeInfo, propsMap, comments);
        mapProperty("contractSum", getString(csvReader, 64), typeInfo, propsMap, comments);
        mapProperty("senderPersonName", getString(csvReader, 67), typeInfo, propsMap, comments);
        mapProperty("endDate", getString(csvReader, 68), typeInfo, propsMap, comments);
        
        mapProperty("publishToAdr", PublishToAdr.NOT_TO_ADR.getValueName(), typeInfo, propsMap);
        
        if (!comments.isEmpty()) {
        	addMissingFieldToCommment(StringUtils.join(comments, " "), (parentPropsMap != null) ? parentPropsMap : propsMap, typeInfo);
        }
        
        return propsMap;
    }
    
    private void mapProperty(String fieldName, String value, TypeInfo typeInfo, Map<QName, Serializable> propsMap) {
		if (StringUtils.isBlank(value)) {
			return;
		}
		
		Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldName);
		if (pair == null) {
			return;
		} else {
			DynamicPropertyDefinition propDef = pair.getFirst();
			
	    	DocLogixDocumentsMapper.PropertyValueProvider valueProvider = typeInfo.props.get(fieldName);
	    	if (valueProvider != null) {
	    		PropertyValue propValue = valueProvider.provide();
	            propValue.put(value);
	    		propsMap.put(propDef.getName(), propValue.get());
			} else {
				propsMap.put(propDef.getName(), value);
			}
		}
    }
    
    
    private void mapProperty(String fieldName, String value, TypeInfo typeInfo, Map<QName, Serializable> propsMap, List<String> comments) {
		if (StringUtils.isBlank(value)) {
			return;
		}
		
		Pair<DynamicPropertyDefinition, Field> pair = typeInfo.propDefs.get(fieldName);
		if (pair == null) {
			comments.add(fieldName + ":" + value + ";");
		} else {
			DynamicPropertyDefinition propDef = pair.getFirst();
			FieldType fieldType = propDef.getFieldType();
			
	    	DocLogixDocumentsMapper.PropertyValueProvider valueProvider = typeInfo.props.get(fieldName);
	    	if (valueProvider != null) {
	    		PropertyValue propValue = valueProvider.provide();
	            propValue.put(value);
	    		propsMap.put(propDef.getName(), propValue.get());
	    	} else if (fieldType == FieldType.CHECKBOX && StringUtils.isNotBlank(value)) {
	    		propsMap.put(propDef.getName(), ("1".equals(value)) ? Boolean.TRUE : Boolean.FALSE);
			} else {
				propsMap.put(propDef.getName(), value);
			}
		}
    }
    

    private ImportedDocument importDoc(CsvReader csvReader, String documentId, Map<String, Mapping> mappings, StoreRef archivalStoreRef) throws DocumentException,
    ParseException, IOException {

        String functionMark = getString(csvReader, 1);
        String seriesIdentifier = getString(csvReader, 2);
        String volumeMark = getString(csvReader, 3);
        String volumeName = getString(csvReader, 4);
        
        String docType = getString(csvReader, 6);
        
        
        TypeInfo typeInfo = docLogixDocumentsMapper.getTypeInfo(docType);
        if (typeInfo == null) {
        	new ImportedDocument(documentId, "missing document type: " + docType);
        }
        
        NodeRef functionRef = findFunction(functionMark, archivalStoreRef);
        if (functionRef == null) {
        	// Skip document
        	return new ImportedDocument(documentId, "function NodeRef not found: " + functionMark);
        }
        NodeRef seriesRef = findSeries(functionRef, seriesIdentifier);
        if (seriesRef == null) {
        	// Skip document
        	return new ImportedDocument(documentId, "series NodeRef not found: " + seriesIdentifier);
        }
        NodeRef volumeRef = findVolume(seriesRef, volumeMark, volumeName);
        if (volumeRef == null) {
        	// Skip document
        	return new ImportedDocument(documentId, "volume NodeRef not found: " + volumeMark);
        }
       
		updateSeriesDocumentTypes(volumeRef, docType);

		
		
		Map<QName, Serializable> propsMap = setProps(csvReader, volumeRef, typeInfo);

		DocumentDynamic doc = getDocumentDynamicService().createNewDocument(typeInfo.docVer, volumeRef, false)
				.getFirst();
		NodeRef documentRef = doc.getNodeRef();
		nodeService.addAspect(documentRef, DocumentCommonModel.Aspects.SEARCHABLE, null);
		
		Map<QName, Serializable> docLogixAspectProps = new HashMap<>();
		docLogixAspectProps.put(DocumentCommonModel.Props.DOC_LOGIX_ID, documentId);
		nodeService.addAspect(documentRef, DocumentCommonModel.Aspects.DOC_LOGIX_IMPORTED, docLogixAspectProps);
		
		
		Map<NodeRef, Map<QName, NodeRef>> parentRefsByVolumeRef = new HashMap<NodeRef, Map<QName, NodeRef>>();
		Map<QName, NodeRef> parentRefs = documentService.getDocumentParents(documentRef);
		parentRefsByVolumeRef.put(volumeRef, parentRefs);

		propsMap.putAll(parentRefs);

		propsMap.put(DocumentCommonModel.Props.DOC_STATUS, DocumentStatus.FINISHED.getValueName());
		checkProps(propsMap, null, typeInfo.propDefs);
		

    	propsMap.put(DocumentCommonModel.Props.DOCUMENT_IS_IMPORTED, Boolean.TRUE);
    	
    	
		doc.getNode().getProperties().putAll(RepoUtil.toStringProperties(propsMap));
		doc.getNode().getProperties().put(
				DocumentService.TransientProps.TEMP_LOGGING_DISABLED_DOCUMENT_METADATA_CHANGED.toString(),
				Boolean.TRUE);
		getDocumentDynamicService().updateDocument(doc, Arrays.asList("docLogixImporter"), false, true);
		
		addAssociations(documentRef, documentId, getString(csvReader, 65));

		LogEntry logEntry = new LogEntry();
		logEntry.setComputerIp("127.0.0.1");
		logEntry.setComputerName("localhost");
		logEntry.setLevel(LogObject.DOCUMENT.getLevel());
		logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
		logEntry.setCreatorId("IMPORT");
		logEntry.setCreatorName("IMPORT");
		logEntry.setEventDescription("Dokumendi importimine (docLogix)");
		logEntry.setObjectId(documentRef.toString());
		BeanHelper.getLogService().addLogEntry(logEntry);

		/*
		String fileNames = addFiles(documentId, documentRef, root);
		BeanHelper.getFileService().reorderFiles(documentRef);
		 */
		
		return new ImportedDocument(documentId, documentRef);
		
    }
    
    

    private void updateSeriesDocumentTypes(NodeRef volumeRef, String docTypeName) {
    	NodeRef seriesRef = (NodeRef)getNodeService().getProperty(volumeRef, DocumentCommonModel.Props.SERIES);
    	List<String> docTypes = (List<String>) getNodeService().getProperty(seriesRef, SeriesModel.Props.DOC_TYPE);
    	if (!docTypes.contains(docTypeName)) {
    		docTypes.add(docTypeName);
    		getNodeService().setProperty(seriesRef, SeriesModel.Props.DOC_TYPE, (Serializable)docTypes);
    	}
    }

    private void checkProps(Map<QName, Serializable> propsMap, QName[] hierarchy, Map<String, Pair<DynamicPropertyDefinition, Field>> propDefs) {
        List<String> errors = new ArrayList<String>();
        for (Entry<QName, Serializable> entry : propsMap.entrySet()) {
            String propNameString = entry.getKey().toPrefixString(getNamespaceService());
            if (!DocumentDynamicModel.URI.equals(entry.getKey().getNamespaceURI())) {
                errors.add("Property " + propNameString + " has wrong namespace");
                continue;
            }
            Pair<DynamicPropertyDefinition, Field> pair = propDefs.get(entry.getKey().getLocalName());
            if (pair == null) {
                errors.add("Property " + propNameString + " is missing propertyDefinition");
                continue;
            }

            if (hierarchy == null) {
                hierarchy = new QName[] {};
            }
            DynamicPropertyDefinition propDef = pair.getFirst();
            QName[] propHierarchy = propDef.getChildAssocTypeQNameHierarchy();
            if (propHierarchy == null) {
                propHierarchy = new QName[] {};
            }
            if (!Arrays.equals(hierarchy, propHierarchy)) {
                errors.add("Property " + propNameString + " should be on hierarchy " + Arrays.asList(propHierarchy) + " but is on hierarchy " + Arrays.asList(hierarchy));
                continue;
            }

            // DocumentsMapper doesn't know about single- and multi-valued properties, so handle it here
            Serializable value = entry.getValue();
            if (propDef.isMultiValued() && value != null && !(value instanceof List)) {
                ArrayList<Serializable> list = new ArrayList<Serializable>();
                list.add(value);
                value = list;
                entry.setValue(value);
            }

            if (value == null) {
                continue;
            }
            String requiredClassName = propDef.getDataType().getJavaClassName();
            if (value instanceof List<?>) {
                if (!("additionalRecipientName".equals(propDef.getName().getLocalName()) || "partyName".equals(propDef.getName().getLocalName()) || "partyContactPerson".equals(propDef.getName().getLocalName())) && !propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should not be a List, but is a List");
                    continue;
                }
                List<Serializable> list = (List<Serializable>) value;
                int i = 0;
                for (Serializable listElement : list) {
                    if (listElement == null) {
                        i++;
                        continue;
                    }
                    String realClassName = listElement.getClass().getName();
                    if (!realClassName.equals(requiredClassName)) {
                        errors.add("Property " + propNameString + " value at index " + i + " should be of type " + requiredClassName + " but is of type " + realClassName + " : "
                                + listElement);
                        continue;
                    }
                    i++;
                }
            } else {
                if (propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should be a List, but is not a List");
                    continue;
                }
                String realClassName = value.getClass().getName();
                if (!realClassName.equals(requiredClassName)) {
                    errors.add("Property " + propNameString + " value should be of type " + requiredClassName + " but is of type " + realClassName + " : " + value);
                    continue;
                }
            }
        }
        if (!errors.isEmpty()) {
            log.info("props=" + WmNode.toString(propsMap, getNamespaceService()));
            throw new RuntimeException("Found " + errors.size() + " errors:\n  * " + StringUtils.join(errors, "\n  * "));
        }
    }

    
    
    private boolean isNotEmptyProps(Map<QName, Serializable> propsMap) {
    	String partyNames = (String)propsMap.get(DocumentSpecificModel.Props.PARTY_NAME);
    	String partyContactPersons = (String)propsMap.get(DocumentSpecificModel.Props.PARTY_CONTACT_PERSON);
    	if (StringUtils.isNotBlank(partyNames) || StringUtils.isNotBlank(partyContactPersons)) {
    		return true;
    	}
    	return false;
    }

    private Map<QName, Serializable> setProps(CsvReader csvReader, NodeRef volumeRef, TypeInfo typeInfo) throws IOException {
        Map<QName, Serializable> propsMap = mapProperties(csvReader, typeInfo, null);

        String regNumber = (String)propsMap.get(DocumentCommonModel.Props.REG_NUMBER);
        if (StringUtils.isNotBlank(regNumber) && propsMap.get(DocumentCommonModel.Props.REG_DATE_TIME) != null) {
        	
        	String shortRegNumber = null;
        	if (StringUtils.isNotBlank(regNumber)) {
        		if (regNumber.contains("/")) {
        			shortRegNumber = StringUtils.substringAfterLast(regNumber, "/");
        		} else if (regNumber.contains("-")) {
            		shortRegNumber = StringUtils.substringBeforeLast(shortRegNumber, "-");
            	} else {
            		shortRegNumber = regNumber;
            	}
        		
        	}
            propsMap.put(DocumentCommonModel.Props.SHORT_REG_NUMBER, shortRegNumber);
            
        }

        setOwnerProperties(propsMap, typeInfo);
        
    	if (StringUtils.isBlank((String) propsMap.get(DocumentCommonModel.Props.DOC_NAME))) {
        	throw new RuntimeException("docName cannot be blank");
        }

        return propsMap;
    }
    
    
    private void addMissingFieldToCommment(String missingFieldNameValue, Map<QName, Serializable> propsMap, TypeInfo typeInfo) {
    	org.alfresco.util.Pair<DynamicPropertyDefinition, Field> pairComment = typeInfo.propDefs.get(DocumentCommonModel.Props.COMMENT.getLocalName());
		if (pairComment != null) {
			String propComment =  (String) propsMap.get(DocumentCommonModel.Props.COMMENT);
			propComment = (StringUtils.isNotBlank(propComment)) ? propComment + " " + missingFieldNameValue + ";" : missingFieldNameValue + ";"; 
			propsMap.put(DocumentCommonModel.Props.COMMENT, propComment);
		}
    }

    private NodeRef findCompletedDoc(String id) {
        NodeRef res = completedDocumentsMap.get(id);
        return res;
    }
    
    
    
    
    
    private NodeRef findVolume(NodeRef seriesRef, String volumeMark, String volumeName) {

        for (ChildAssociationRef assoc : nodeService.getChildAssocs(seriesRef, VolumeModel.Associations.VOLUME, VolumeModel.Associations.VOLUME)) {
            if (volumeMark.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.MARK))
            		&& volumeName.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.TITLE))) {
                return assoc.getChildRef();
            }
        }
        
        for (ChildAssociationRef assoc : nodeService.getChildAssocs(seriesRef, CaseFileModel.Assocs.CASE_FILE, CaseFileModel.Assocs.CASE_FILE)) {
            if (volumeMark.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.MARK))
            		&& volumeName.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.TITLE))) {
                return assoc.getChildRef();
            }
        }
        
    	return null;
    }
    
    
    private NodeRef findSeries(NodeRef functionRef, String seriesIdentifier) {
        for (ChildAssociationRef assoc : nodeService.getChildAssocs(functionRef, SeriesModel.Associations.SERIES, SeriesModel.Associations.SERIES)) {
            if (seriesIdentifier.equals(nodeService.getProperty(assoc.getChildRef(), SeriesModel.Props.SERIES_IDENTIFIER))) {
                return assoc.getChildRef();
            }
        }
        
    	return null;
    }
    
    
    private NodeRef findFunction(String mark, StoreRef archivalsStoreRef) {
    	
        NodeRef docsList = generalService.getNodeRef(FunctionsModel.Repo.FUNCTIONS_SPACE, archivalsStoreRef);

        for (ChildAssociationRef assoc : nodeService.getChildAssocs(docsList, FunctionsModel.Associations.FUNCTION, FunctionsModel.Associations.FUNCTION)) {
            if (mark.equals(nodeService.getProperty(assoc.getChildRef(), FunctionsModel.Props.MARK))) {
            	return assoc.getChildRef();
            }
        }

        return null;
    }
    
    // [ASSOCS
    
    protected void loadPostponedAssocs() throws Exception {
        postponedAssocsFile = new File(workFolder, "postponed_assocs.csv");

        postponedAssocs = new TreeMap<String, List<PostponedAssoc>>();

        if (!postponedAssocsFile.exists()) {
            log.info("Skipping loading postponed assocs, file does not exist: " + postponedAssocsFile);
            return;
        }

        log.info("Loading postponed assocs from file " + postponedAssocsFile);

        CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(postponedAssocsFile)), CSV_SEPARATOR, Charset.forName("UTF-8"));
        try {
            reader.readHeaders();
            while (reader.readRecord()) {
                String sourceDocId = reader.get(0);
                String targetDocId = reader.get(1);
                
                putPostponedAssoc(sourceDocId, targetDocId);
            }
        } finally {
            reader.close();
        }
        log.info("Loaded " + postponedAssocs.size() + " postponed assocs.");
        postponedAssocsCommited = new TreeMap<String, List<PostponedAssoc>>(postponedAssocs);
    }

    static class PostponedAssoc {
        String sourceDocId;
        String targetDocId;

        public PostponedAssoc(String sourceDocId, String targetDocId) {
            this.sourceDocId = sourceDocId;
            this.targetDocId = targetDocId;
        }
    }

    private void writePostponedAssocs() {
        try {
            // Write created documents
            if (postponedAssocsFile == null) {
                return;
            }
            boolean exists = postponedAssocsFile.exists();
            if (exists) {
                postponedAssocsFile.delete();
            }
            CsvWriter writer = new CsvWriter(new FileWriter(postponedAssocsFile, false), CSV_SEPARATOR);
            try {
                writer.writeRecord(new String[] {
                        "sourceDocumentId",
                        "targetDocumentId",
                });
                for (List<PostponedAssoc> list : postponedAssocsCommited.values()) {
                    for (PostponedAssoc postponedAssoc : list) {
                        writer.writeRecord(new String[] {
                                postponedAssoc.sourceDocId.toString(),
                                postponedAssoc.targetDocId.toString(),
                        });
                    }
                }
            } finally {
                writer.close();
            }
        } catch (IOException e) {
            throw new RuntimeException("Error writing file '" + postponedAssocsFile + "': " + e.getMessage(), e);
        }
    }

    protected void addAssociations(NodeRef documentRef, String documentId, String relations) {
        if (StringUtils.isBlank(relations)) {
        	return;
        }
    	
        List<String> relationList = Arrays.asList(relations.split(";"));
    	for (String targetDocumentId: relationList) {
    		
    		if (StringUtils.isBlank(targetDocumentId)) {
                continue;
            }
    				
            NodeRef targetDocumentRef = findCompletedDoc(targetDocumentId);
            if (targetDocumentRef == null) {
                putPostponedAssoc(documentId, targetDocumentId);
            } else {
                log.debug("Creating assoc " + documentId + " [" + documentRef + "] -> " + targetDocumentId + " [" + targetDocumentRef + "]");
                createAssoc(documentRef, targetDocumentRef, documentId, targetDocumentId);
            }
    	}

        
        List<PostponedAssoc> list = postponedAssocs.get(documentId);
        if (list != null) {
        	List<PostponedAssoc> updatedList = new ArrayList<>();
            for (PostponedAssoc postponedAssoc : list) {
                NodeRef sourceDocRef = findCompletedDoc(postponedAssoc.sourceDocId);
                log.debug("Creating assoc " + postponedAssoc.sourceDocId + " [" + sourceDocRef + "] -> " + documentId + " [" + documentRef + "]");
                if (!createAssoc(sourceDocRef, documentRef, postponedAssoc.sourceDocId, documentId)) {
                	updatedList.add(postponedAssoc);
                }
            }
            if (updatedList.isEmpty()) {
            	postponedAssocs.remove(documentId);
            } else {
            	postponedAssocs.put(documentId, updatedList);
            }
        }
    }

    private boolean createAssoc(NodeRef sourceDocRef, NodeRef targetDocRef, String sourceDocId, String targetDocId) {
        QName assocTypeQName = DocumentCommonModel.Assocs.DOCUMENT_2_DOCUMENT;
        
        if (sourceDocRef == null || !nodeService.exists(sourceDocRef)) {
            log.error("Skipping creating assoc, source does not exist, sourceDocumentId=" + sourceDocId + " targetDocumentId="
                    + targetDocId + " assocType=tavaline sourceDocRef=" + sourceDocRef + " targetDocRef=" + targetDocRef);
            return false;
        }
        if (targetDocRef == null || !nodeService.exists(targetDocRef)) {
            log.error("Skipping creating assoc, target does not exist, sourceDocumentId=" + sourceDocId + " targetDocumentId="
                    + targetDocId + " assocType=tavaline sourceDocRef=" + sourceDocRef + " targetDocRef=" + targetDocRef);
            return false;
        }
        boolean skip = false;

        // Check that reverse-direction associations are not previously defined, otherwise it is a business rule failure
        List<AssociationRef> targetAssocs = nodeService.getSourceAssocs(sourceDocRef, assocTypeQName);
        for (AssociationRef assocRef : targetAssocs) {
            Assert.isTrue(assocRef.getTargetRef().equals(sourceDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
            log.debug("Existing target-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
                    log.debug("Skipping this assoc creation");
                    skip = true;
            }
            
        }
        if (!skip) {
	        List<AssociationRef> sourceAssocs = nodeService.getTargetAssocs(targetDocRef, assocTypeQName);
	        for (AssociationRef assocRef : sourceAssocs) {
	            Assert.isTrue(assocRef.getSourceRef().equals(targetDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
	            log.debug("Existing source-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
	            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
	                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
	                    log.debug("Skipping this assoc creation");
	                    skip = true;
	            }
	        }
        }
        
        if (!skip) {
	        // Check that same-direction associations are not previously defined, otherwise nodeService.createAssociation throws AssociationExistsException
	        targetAssocs = nodeService.getSourceAssocs(targetDocRef, assocTypeQName);
	        for (AssociationRef assocRef : targetAssocs) {
	            Assert.isTrue(assocRef.getTargetRef().equals(targetDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
	            log.debug("Existing target-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
	            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
	                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
	                    log.debug("Skipping this assoc creation");
	                    skip = true;
	            }
	        }
        }
        if (!skip) {
        	List<AssociationRef> sourceAssocs = nodeService.getTargetAssocs(sourceDocRef, assocTypeQName);
	        for (AssociationRef assocRef : sourceAssocs) {
	            Assert.isTrue(assocRef.getSourceRef().equals(sourceDocRef), "targetDocRef=" + targetDocRef + ", sourceDocRef=" + sourceDocRef + ", assocRef=" + assocRef);
	            log.debug("Existing source-assoc [" + assocRef.getSourceRef() + "] -> [" + assocRef.getTargetRef() + "], type=" + assocRef.getTypeQName());
	            if ((sourceDocRef.equals(assocRef.getSourceRef()) && targetDocRef.equals(assocRef.getTargetRef())) ||
	                    (targetDocRef.equals(assocRef.getSourceRef()) && sourceDocRef.equals(assocRef.getTargetRef()))) {
	                    log.debug("Skipping this assoc creation");
	                    skip = true;
	            }
	        }
        }

        log.info("skipping");
        
        if (!skip) {
            nodeService.createAssociation(sourceDocRef, targetDocRef, assocTypeQName);
        }
        return true;
    }

    private void putPostponedAssoc(String sourceDocId, String targetDocId) {
        List<PostponedAssoc> list = postponedAssocs.get(targetDocId);
        if (list == null) {
            list = new ArrayList<PostponedAssoc>();
            postponedAssocs.put(targetDocId, list);
        }
        list.add(new PostponedAssoc(sourceDocId, targetDocId));
    }
    
    private void postProcessPostponedAssoc() {
    	log.info("postProcessPostponedAssoc size: " + postponedAssocs.size());
    	
    	NavigableMap<String /* documentId */, List<PostponedAssoc>> updatePostponedAssocs = new TreeMap<>() ;
    	
    	for (String documentId: postponedAssocs.keySet()) {
	    	List<PostponedAssoc> list = postponedAssocs.get(documentId);
	    	log.info("postProcessPostponedAssoc docId: " + documentId);
	        if (list != null) {
	        	log.info("postProcessPostponedAssoc list: " + list.size());
	        	List<PostponedAssoc> updatedList = new ArrayList<>();
	            for (PostponedAssoc postponedAssoc : list) {
	                NodeRef sourceDocRef = findCompletedDoc(postponedAssoc.sourceDocId);
	                NodeRef targetDocRef = findCompletedDoc(postponedAssoc.targetDocId);
	                log.info("postProcessPostponedAssoc: Creating assoc " + postponedAssoc.sourceDocId + " [" + sourceDocRef + "] -> " + documentId + " [" + targetDocRef + "]");
	                if (!createAssoc(sourceDocRef, targetDocRef, postponedAssoc.sourceDocId, documentId)) {
	                	updatedList.add(postponedAssoc);
	                }
	            }
	            if (!updatedList.isEmpty()) {
                	updatePostponedAssocs.put(documentId, updatedList);
                }
	        }
    	}
    	postponedAssocs = updatePostponedAssocs;
    	postponedAssocsCommited = new TreeMap<String, List<PostponedAssoc>>(postponedAssocs);
    }

    // ASSOCS]
    
    

    // HELPER METHODS
    /** RetryingTransactionHelper that only tries to do things once. */
    private RetryingTransactionHelper getTransactionHelper() {
        RetryingTransactionHelper helper = new RetryingTransactionHelper();
        helper.setMaxRetries(1);
        helper.setTransactionService(transactionService);
        return helper;
    }
    
    
    private class DocLogixVolume {
    	private NodeRef nodeRef;
		private Date validFrom;
		private Date validTo;
    	private String mark;
    	private String seriesIdentifier;
    	private String seriesTitle;
		private String function;
    	
    	public NodeRef getNodeRef() {
			return nodeRef;
		}
		public void setNodeRef(NodeRef nodeRef) {
			this.nodeRef = nodeRef;
		}
		public Date getValidFrom() {
			return validFrom;
		}
		public void setValidFrom(Date validFrom) {
			this.validFrom = validFrom;
		}
		public Date getValidTo() {
			return validTo;
		}
		public void setValidTo(Date validTo) {
			this.validTo = validTo;
		}
		public String getMark() {
			return mark;
		}
		public void setMark(String mark) {
			this.mark = mark;
		}
		public String getSeriesIdentifier() {
			return seriesIdentifier;
		}
		public void setSeriesIdentifier(String seriesIdentifier) {
			this.seriesIdentifier = seriesIdentifier;
		}
		public String getSeriesTitle() {
			return seriesTitle;
		}
		public void setSeriesTitle(String seriesTitle) {
			this.seriesTitle = seriesTitle;
		}
		public String getFunction() {
			return function;
		}
		public void setFunction(String function) {
			this.function = function;
		}    	
    		
    }
    

}
