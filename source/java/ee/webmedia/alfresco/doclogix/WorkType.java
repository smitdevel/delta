package ee.webmedia.alfresco.doclogix;

public enum WorkType {
	STRUCTURE,
	DOCUMENTS,
	FILES,
	LOGS

}
