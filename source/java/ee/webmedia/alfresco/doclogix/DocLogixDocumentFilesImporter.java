package ee.webmedia.alfresco.doclogix;

import static ee.webmedia.alfresco.doclogix.ImportUtil.getInteger;
import static ee.webmedia.alfresco.doclogix.ImportUtil.getString;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.repo.transaction.TransactionListenerAdapter;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.file.model.FileModel;
import ee.webmedia.alfresco.utils.FilenameUtil;

/**
 * Imports documents and files from DocLogix.
 */
public class DocLogixDocumentFilesImporter {

	private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory
			.getLog(DocLogixDocumentFilesImporter.class);

	protected static final char CSV_SEPARATOR = ';';
	private static final String CREATOR_MODIFIER = "DHS";

	private final DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	protected SAXReader xmlReader = new SAXReader();

	private File dataFolder;
	private File workFolder;
	private int batchSize;

	// [SPRING BEANS
	private TransactionService transactionService;
	private GeneralService generalService;
	private NodeService nodeService;
	private FileFolderService fileFolderService;
	private BehaviourFilter behaviourFilter;
	private DocLogixImporter docLogixImporter;

	public DocLogixDocumentFilesImporter(DocLogixImporter docLogixImporter) {

		setGeneralService(BeanHelper.getGeneralService());
		setTransactionService(BeanHelper.getTransactionService());
		setNodeService(BeanHelper.getNodeService());
		setBehaviourFilter(BeanHelper.getPolicyBehaviourFilter());
		setFileFolderService(BeanHelper.getFileFolderService());
		setDocLogixImporter(docLogixImporter);

		dateTimeFormat.setLenient(false);
	}

	public void setGeneralService(GeneralService generalService) {
		this.generalService = generalService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public void setFileFolderService(FileFolderService fileFolderService) {
		this.fileFolderService = fileFolderService;
	}

	public void setBehaviourFilter(BehaviourFilter behaviourFilter) {
		this.behaviourFilter = behaviourFilter;
	}

	public void setDocLogixImporter(DocLogixImporter docLogixImporter) {
		this.docLogixImporter = docLogixImporter;
	}

	/**
	 * Runs documents import process
	 */
	public void runImport(File dataFolder, File workFolder, int batchSize, String defaultOwnerId,
			String institutionCode) throws Exception {
		this.dataFolder = dataFolder;
		this.workFolder = workFolder;
		this.batchSize = batchSize;
		init();
		// Doc log import
		try {

			loadCompletedDocumentFiles();
			loadMappingFiles();
			countDocumentFiles();
			createDocumentFiles();
		} finally {
			documentFilesToImportCount = 0;
		}

	}

	private void init() {
		documentFilesToImportCount = 0;
		completedDocumentFilesSet = new HashSet<>();
		mappingFilesMap = new HashMap<>();
	}

	protected int documentFilesToImportCount;
	protected Set<Integer> completedDocumentFilesSet;
	protected Map<Integer, NodeRef> mappingFilesMap;

	protected File completedDocumentFilesFile;
	protected File failedDocumentFilesFile;
	protected File mappingFilesFile;

	protected void loadCompletedDocumentFiles() throws Exception {
		failedDocumentFilesFile = new File(workFolder, "failed_doc_files.csv");
		OutputStream outputStream = new FileOutputStream(failedDocumentFilesFile);
		try {
			// the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the
			// file in correct encoding
			outputStream.write("\ufeff".getBytes("UTF-8"));
		} finally {
			outputStream.close();
		}
		CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentFilesFile, true), CSV_SEPARATOR);
		try {
			writer.writeRecord(new String[] { "fileId", "errorMsg" });
		} finally {
			writer.close();
		}

		completedDocumentFilesSet = new HashSet<>();

		completedDocumentFilesFile = new File(workFolder, "completed_doc_files.csv");
		if (!completedDocumentFilesFile.exists()) {
			log.info("Skipping loading previously completed document Files, file does not exist: "
					+ completedDocumentFilesFile);
			return;
		}

		log.info("Loading previously completed document fileId-s from file " + completedDocumentFilesFile);

		CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(completedDocumentFilesFile)),
				CSV_SEPARATOR, Charset.forName("UTF-8"));
		try {
			reader.readHeaders();
			while (reader.readRecord()) {
				Integer fileId = Integer.parseInt(reader.get(0));
				completedDocumentFilesSet.add(fileId);
			}
		} finally {
			reader.close();
		}
		log.info("Loaded " + completedDocumentFilesSet.size() + " previously completed document fileId-s");

	}

	protected void loadMappingFiles() throws Exception {

		completedDocumentFilesSet = new HashSet<>();

		mappingFilesFile = new File(dataFolder, "mapping_files.csv");
		if (!mappingFilesFile.exists()) {
			log.info("Skipping loading mapping Files, file does not exist: " + mappingFilesFile);
			return;
		}

		log.info("Loading mapping fileId-s and doc node refs from file " + mappingFilesFile);

		CsvReader reader = new CsvReader(new BufferedInputStream(new FileInputStream(mappingFilesFile)), CSV_SEPARATOR,
				Charset.forName("UTF-8"));
		try {
			reader.readHeaders();
			while (reader.readRecord()) {
				try {
					String[] fileIds = reader.get(0).split(";");
					NodeRef nodeRef = new NodeRef(reader.get(1));
					for (int i = 0; i < fileIds.length; i++) {
						mappingFilesMap.put(Integer.parseInt(fileIds[i]), nodeRef);
					}
				} catch (Exception e) {
					log.warn("failed to load mapping file: " + reader.getValues(), e);
				}
			}
		} finally {
			reader.close();
		}
		log.info("Loaded " + mappingFilesMap.size() + " mapping document fileId-s and doc nodeRef");

	}

	protected void countDocumentFiles() throws IOException {

		CsvReader reader = new CsvReader(
				new BufferedInputStream(new FileInputStream(new File(dataFolder, "failid.csv"))), CSV_SEPARATOR,
				Charset.forName("UTF-8"));
		int DocFileCounter = 0;
		try {

			reader.readHeaders();
			while (reader.readRecord()) {
				DocFileCounter++;
			}
		} finally {
			reader.close();
		}

		documentFilesToImportCount = DocFileCounter;

		log.info("Completed parsing failid.csv , got " + DocFileCounter + " document Files");
	}

	private NodeRef toNodeRef(String nodeRefStr) {
		if (StringUtils.isBlank(nodeRefStr)) {
			return null;
		}
		return new NodeRef(nodeRefStr);
	}

	private void checkStop() {
		docLogixImporter.checkStop();
	}

	private abstract class BatchProgress {
		int totalSize;
		int i;
		int completedSize;
		int thisRunCompletedSize;
		long thisRunStartTime;
		long startTime;
		String processName;

		private void init() {
			totalSize = 0;
			thisRunStartTime = System.currentTimeMillis();
			startTime = thisRunStartTime;
			i = 0;
			completedSize = 0;
		}

		abstract void executeBatch(CsvReader reader) throws Exception;

		void executeInTransaction(final CsvReader reader) {
			try {
				getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Object>() {
					@Override
					public Object execute() throws Throwable {
						behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
						executeBatch(reader);
						return null;
					}
				});
			} catch (Exception e) {
				log.error("Import batch of document Files failed, transaction rolled back, continuing with next batch",
						e);
			}
		}

		private void step(CsvReader reader) {
			executeInTransaction(reader);
			completedSize += batchSize;
			thisRunCompletedSize += batchSize;
			long endTime = System.currentTimeMillis();
			double completedPercent = (completedSize) * 100L / ((double) totalSize);
			double lastDocFilesPerSec = (i) * 1000L / ((double) (endTime - startTime));
			long thisRunTotalTime = endTime - thisRunStartTime;
			double totalDocFilesPerSec = (thisRunCompletedSize) * 1000L / ((double) thisRunTotalTime);
			long remainingSize = ((long) totalSize) - ((long) completedSize);
			long divisor = (thisRunCompletedSize) * 60000L;
			int etaMinutes = ((int) (remainingSize * thisRunTotalTime / divisor)) + 1;
			int etaHours = 0;
			if (etaMinutes > 59) {
				etaHours = etaMinutes / 60;
				etaMinutes = etaMinutes % 60;
			}
			String eta = etaMinutes + "m";
			if (etaHours > 0) {
				eta = etaHours + "h " + eta;
			}
			i = 0;
			String info = "%s: %6.2f%% completed - %7d of %7d, %5.1f items per second (last), %5.1f (total), ETA %s";
			log.info(String.format(info, processName, completedPercent, completedSize, totalSize, lastDocFilesPerSec,
					totalDocFilesPerSec, eta));
			startTime = endTime;
		}

		public void run() {
			File sourceFile = new File(dataFolder, "failid.csv");

			if (!sourceFile.exists()) {
				log.error("failid.csv cannot be found");
				return;
			}

			CsvReader reader = null;
			try {

				reader = ImportUtil.createDataReader(sourceFile);

				boolean canContinue = reader.readHeaders() && reader.readRecord();
				init();
				checkStop();
				while (canContinue) {
					i++;
					if (i <= batchSize) {
						step(reader);
						checkStop();
					}
					canContinue = reader.readRecord();
				}
			} catch (Throwable t) {
				log.error("Error importing from failid.csv", t);
			} finally {
				if (reader != null) {
					reader.close();
				}
			}

		}

	}

	private static interface CsvWriterClosure {
		void execute(CsvWriter writer) throws IOException;

		String[] getHeaders();
	}

	private static void bindCsvWriteAfterCommit(final File file, final CsvWriterClosure closure) {
		AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
			@Override
			public void afterCommit() {
				try {
					// Write created document Files
					boolean exists = file.exists();
					if (!exists) {
						OutputStream outputStream = new FileOutputStream(file);
						try {
							// the Unicode value for UTF-8 BOM, is needed so that Excel would recognise the
							// file in correct encoding
							outputStream.write("\ufeff".getBytes("UTF-8"));
						} finally {
							outputStream.close();
						}
					}
					CsvWriter writer = new CsvWriter(new FileWriter(file, true), CSV_SEPARATOR);
					try {
						if (!exists) {
							writer.writeRecord(closure.getHeaders());
						}
						closure.execute(writer);
					} finally {
						writer.close();
					}
				} catch (IOException e) {
					throw new RuntimeException("Error writing file '" + file + "': " + e.getMessage(), e);
				}
			}
		});
	}

	private class DocumensBatchProgress extends BatchProgress {
		{
			processName = "Document Files import";
		}

		@Override
		void executeBatch(CsvReader reader) throws Exception {
			createDocumentsBatch(reader);
		}
	}

	private void createDocumentFiles() throws Exception {
		if (documentFilesToImportCount == 0 || mappingFilesMap.isEmpty()) {
			log.info("Skipping document Files import, no document Files found");
			return;
		}
		log.info("Starting document Files import. Total = " + documentFilesToImportCount);
		DocumensBatchProgress batchProgress = new DocumensBatchProgress();

		batchProgress.run();

		log.info("Document Files IMPORT COMPLETE :)");
	}

	static class ImportedDocFile {
		Integer fileId;
		NodeRef nodeRef;
		String msg;

		public ImportedDocFile(Integer fileId, NodeRef nodeRef) {
			this.fileId = fileId;
			this.nodeRef = nodeRef;
		}

		public ImportedDocFile(Integer fileId, String msg) {
			this.fileId = fileId;
			this.msg = msg;
		}
	}

	protected void createDocumentsBatch(CsvReader csvReader) throws Exception {
		final Map<Integer, ImportedDocFile> batchCompletedDocumentsMap = new TreeMap<Integer, ImportedDocFile>();
		AlfrescoTransactionSupport.bindListener(new TransactionListenerAdapter() {
			@Override
			public void afterCommit() {
			}

			@Override
			public void afterRollback() {
				completedDocumentFilesSet.removeAll(batchCompletedDocumentsMap.keySet());
			}
		});

		int docFilesCounter = 0;
		boolean canContinue = true;

		while (canContinue && docFilesCounter < batchSize) {
			docFilesCounter++;
			Integer fileId = getInteger(csvReader, 1); // file index Id
			if (log.isTraceEnabled()) {
				log.trace("Processing document file Id=" + fileId);
			}
			if (isCompletedDocFile(fileId)) {
				log.trace("Skipping, already added: document file Id=" + fileId);
				continue;
			}

			try {
				ImportedDocFile docFile = createDocFile(fileId, csvReader);
				if (StringUtils.isBlank(docFile.msg)) {
					batchCompletedDocumentsMap.put(fileId, docFile);
					completedDocumentFilesSet.add(fileId); // Add immediately to completedDocumentFilesSet, because
															// other code wants to access it
					docLogixImporter.getStatus().incrCount();
				} else {
					CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentFilesFile, true), CSV_SEPARATOR);
					try {
						writer.writeRecord(new String[] { fileId.toString(), docFile.msg });
					} finally {
						writer.close();
					}
					docLogixImporter.getStatus().incrFailed();
				}
			} catch (Throwable e) {
				CsvWriter writer = new CsvWriter(new FileWriter(failedDocumentFilesFile, true), CSV_SEPARATOR);
				try {
					writer.writeRecord(new String[] { fileId.toString(), e.getMessage() });
				} finally {
					writer.close();
				}
				docLogixImporter.getStatus().incrFailed();
				log.error("Error importing document log " + fileId + ": " + e.getMessage(), e);
			}

			if (docFilesCounter < batchSize) {
				canContinue = csvReader.readRecord();
			}
		}
		bindCsvWriteAfterCommit(completedDocumentFilesFile, new CsvWriterClosure() {

			@Override
			public String[] getHeaders() {
				return new String[] { "fileId", "nodeRef" };
			}

			@Override
			public void execute(CsvWriter writer) throws IOException {
				for (Entry<Integer, ImportedDocFile> entry : batchCompletedDocumentsMap.entrySet()) {
					ImportedDocFile docFile = entry.getValue();
					writer.writeRecord(new String[] { docFile.fileId.toString(),
							docFile.nodeRef == null ? "" : docFile.nodeRef.toString() });
				}
			}
		});
	}

	private ImportedDocFile createDocFile(Integer fileId, CsvReader csvReader)
			throws DocumentException, ParseException, IOException {
		return importDocFile(csvReader, fileId);
	}

	private ImportedDocFile importDocFile(CsvReader csvReader, Integer fileId)
			throws DocumentException, ParseException, IOException {

		Integer docLogicId = getInteger(csvReader, 2);
		NodeRef docNodeRef = mappingFilesMap.get(docLogicId);

		String fileName = getString(csvReader, 3);
		String filePath = getString(csvReader, 4);

		if (docNodeRef == null || !nodeService.exists(docNodeRef)) {
			return new ImportedDocFile(fileId, "doc nodeRef does not exit, docNodeRef=" + docNodeRef
					+ ", mappingFilesMap has key=" + docLogicId + " is " + mappingFilesMap.containsKey(docLogicId));
		}
		File file = new File(filePath);
		if (!file.exists()) {
			return new ImportedDocFile(fileId, "file does not exit, path=" + filePath + ", fileName=" + fileName
					+ ", docNodeRef=" + docNodeRef + ", docLogicId=" + docLogicId);
		}
		NodeRef fileNodeRef = addFile(fileName, file, docNodeRef, true);

		BeanHelper.getFileService().reorderFiles(docNodeRef);

		return new ImportedDocFile(fileId, fileNodeRef);

	}

	private NodeRef addFile(String displayName, File file, NodeRef parentNodeRef, boolean active) {
		String fileName = FilenameUtil.makeSafeFilename(displayName);
		fileName = generalService.getUniqueFileName(parentNodeRef, fileName);
		final FileInfo fileInfo = fileFolderService.create(parentNodeRef, fileName, ContentModel.TYPE_CONTENT);
		final NodeRef fileRef = fileInfo.getNodeRef();

		OutputStream out = null;
		try {

			String mimetype = BeanHelper.getMimetypeService().guessMimetype(fileName);
			final ContentWriter writer = fileFolderService.getWriter(fileRef);
			writer.setMimetype(mimetype);
			writer.setEncoding("UTF-8");

			writer.putContent(file);

		} finally {
			IOUtils.closeQuietly(out);
		}
		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		props.put(ContentModel.PROP_CREATOR, CREATOR_MODIFIER);
		props.put(ContentModel.PROP_MODIFIER, CREATOR_MODIFIER);
		props.put(FileModel.Props.DISPLAY_NAME, displayName);
		props.put(FileModel.Props.ACTIVE, Boolean.valueOf(active));
		nodeService.addProperties(fileRef, props);
		return fileRef;
	}

	private boolean isCompletedDocFile(Integer id) {
		return completedDocumentFilesSet.contains(id);
	}

	// HELPER METHODS
	/** RetryingTransactionHelper that only tries to do things once. */
	private RetryingTransactionHelper getTransactionHelper() {
		RetryingTransactionHelper helper = new RetryingTransactionHelper();
		helper.setMaxRetries(1);
		helper.setTransactionService(transactionService);
		return helper;
	}

}
