package ee.webmedia.alfresco.classificator.enums;

/**
 * Enum constants for KNOWN "deliveryType" classificator values
 */
public enum EncryptionType {
    ENCRYPTED("Krüpteeritult");

    private String valueName;

    EncryptionType(String value) {
        valueName = value;
    }

    public String getValueName() {
        return valueName;
    }

    public boolean equals(String value) {
        return valueName.equalsIgnoreCase(value);
    }
}
