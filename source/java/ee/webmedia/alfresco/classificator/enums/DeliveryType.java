package ee.webmedia.alfresco.classificator.enums;

/**
 * Enum constants for KNOWN "deliveryType" classificator values
 */
public enum DeliveryType {
    AUTO_SEND("Automaatne saatmine");

    private String valueName;

    DeliveryType(String value) {
        valueName = value;
    }

    public String getValueName() {
        return valueName;
    }

    public boolean equals(String value) {
        return valueName.equalsIgnoreCase(value);
    }
}
