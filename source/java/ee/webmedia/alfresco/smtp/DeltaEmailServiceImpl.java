package ee.webmedia.alfresco.smtp;

import java.util.Map;

import org.alfresco.email.server.EmailServiceImpl;
import org.alfresco.email.server.handler.EmailMessageHandler;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.node.integrity.IntegrityException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.cmr.email.EmailMessage;
import org.alfresco.service.cmr.email.EmailMessageException;
import org.alfresco.service.cmr.email.EmailMessagePart;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.Assert;

import ee.webmedia.alfresco.common.service.GeneralService;

/**
 * Email service implementation, that selects target folder based on emailPrefixToFolderMap
 */
public class DeltaEmailServiceImpl extends EmailServiceImpl {
    private static final Log log = LogFactory.getLog(DeltaEmailServiceImpl.class);

    private GeneralService generalService;
    /**
     * Mappings for email address local part to repository location(XPath)
     */
    private Map<String, String> emailPrefixToFolderMap;

    @Override
    public void importMessage(final EmailMessage message) {
        importMessage(null, message);
    }

    private void emailMessagePartLog(String loginfo, EmailMessagePart part){
        try{
            log.debug(loginfo + " : Content-type: " + (part.getContentType() == null ? "NULL" : part.getContentType()));
            log.debug(loginfo + " : Encoding: " + (part.getEncoding() == null ? "NULL" : part.getEncoding()));
            log.debug(loginfo + " : filename: " + (part.getFileName() == null ? "NULL" : part.getFileName()));
            log.debug(loginfo + " : Size: " + (part.getSize() == 0 ? "NULL" : part.getSize()));
        } catch (Exception e){
            log.warn(e.getMessage(), e);
        }
    }

    @Override
    public void importMessage(final NodeRef nodeRef, final EmailMessage message) {
        log.debug("importMessage()...");
        if (!emailInboundEnabled) {
            throw new EmailMessageException(ERR_INBOUND_EMAIL_DISABLED);
        }

        if(message != null){
            log.debug("EMAIL MESSAGE ...");
            log.debug("EMAIL FROM: " + message.getFrom());
            log.debug("EMAIL TO: " + message.getTo());
            log.debug("EMAIL subject: " + message.getSubject());

            if(message.getAttachments() != null){
                log.debug("EMAIL attachments: " + message.getAttachments().length);
                for (EmailMessagePart part : message.getAttachments()) {
                    log.debug("-- MESSAGE PART ------------------------------------");
                    emailMessagePartLog("EMAIL attachment", part);
                }
            } else {
                log.debug("EMAIL attachments: NULL!");
            }

            if(message.getBody() != null){
                emailMessagePartLog("EMAIL body", message.getBody());
            } else {
                log.debug("EMAIL body : NULL");
            }
        } else {
            log.debug("EMAIL MESSAGE is NULL!");
        }

        try {
            log.debug("Process the message using the username's account... processMessageCallback");
            // Process the message using the username's account
            final RetryingTransactionCallback<Object> processMessageCallback = new RetryingTransactionCallback<Object>() {
                @Override
                public Object execute() throws Throwable {
                    final NodeRef targetNodeRef;
                    if (nodeRef == null) {
                        log.debug("NODE Ref is NULL...");
                        String recipient = message.getTo();
                        log.debug("RECIPIENT: " + recipient);
                        targetNodeRef = getTargetNodeByRecipient(recipient);
                        log.debug("TARGET NODE Ref: " + targetNodeRef);
                        if (targetNodeRef == null) {
                            log.debug("TARGET NODE Ref: NULL! Use DeltaEmailServiceImpl to import message...");
                            DeltaEmailServiceImpl.super.importMessage(nodeRef, message);
                            return null;
                        }
                    } else {
                        targetNodeRef = nodeRef;
                    }
                    log.debug("Get e-mail message handler...");
                    EmailMessageHandler messageHandler = getMessageHandler(targetNodeRef);
                    log.debug("Process message...");
                    messageHandler.processMessage(targetNodeRef, message);
                    return null;
                }
            };

            log.debug("processMessageRunAsWork...");
            RunAsWork<Object> processMessageRunAsWork = new RunAsWork<Object>() {
                @Override
                public Object doWork() throws Exception {
                    return retryingTransactionHelper.doInTransaction(processMessageCallback, false);
                }
            };
            AuthenticationUtil.runAs(processMessageRunAsWork, AuthenticationUtil.getSystemUserName());
        } catch (EmailMessageException e) {
            // These are email-specific errors
            throw e;
        } catch (AccessDeniedException e) {
            throw new EmailMessageException(ERR_ACCESS_DENIED, message.getFrom(), message.getTo());
        } catch (IntegrityException e) {
            throw new EmailMessageException(ERR_INVALID_SUBJECT);
        } catch (Throwable e) {
            throw new AlfrescoRuntimeException("Email message processing failed", e);
        }
    }

    /**
     * @return Target node based on recipient emailAddress local part or null
     */
    private NodeRef getTargetNodeByRecipient(String recipient) {
        final int atIndex = recipient.indexOf("@");
        final String target = recipient.substring(0, atIndex);
        NodeRef targetRef = null;
        final String targetXPath = emailPrefixToFolderMap.get(target);
        if (targetXPath == null) {
            return null;
        }
        targetRef = generalService.getNodeRef(targetXPath);
        Assert.notNull(targetRef, "Target node reference not found for recipient '" + recipient + "'");
        return targetRef;
    }

    // START: getters / setters
    public void setGeneralService(GeneralService generalService) {
        this.generalService = generalService;
    }

    public void setEmailPrefixToFolderMap(Map<String, String> emailPrefixToFolderMap) {
        this.emailPrefixToFolderMap = emailPrefixToFolderMap;
    }

    // END: getters / setters
}
