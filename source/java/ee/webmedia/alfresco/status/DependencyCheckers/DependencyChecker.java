package ee.webmedia.alfresco.status.DependencyCheckers;

import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.UnknownHostException;

import ee.webmedia.alfresco.common.service.ApplicationConstantsBean;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.alfresco.web.ui.common.renderer.SimpleHtmlElementRenderer;
import ee.webmedia.alfresco.status.model.Unit;




/**
 * 
 * @author viljar.tina
 *
 */
public abstract class DependencyChecker extends Unit implements Timoutable {
	
    protected static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(SimpleHtmlElementRenderer.class);
    protected static ApplicationConstantsBean props = BeanHelper.getApplicationConstantsBean();
	/**
	 * 
	 */
	public Boolean IsFatal;
	
	
	
	/**
	 * 
	 */
	DependencyChecker ( String name, String uri, Boolean isFatal ) {
		super(name);
        this.IsFatal = isFatal;
        if(uri.contains("null")){ uri = uri.replace("null", getHostName()); }
        this.Uri = uri;
    }
	
	
	
	
	/**
	 * 
	 */
	public Boolean Test ( ) {
		Status = STATUS_OK;
	    return true;
    }
	
	
	/**
	 * 
	 */
	public static String getHostName(){
		
		String hostName = "localhost";
		try{
			InetAddress inetAddr = InetAddress.getLocalHost();
			hostName = inetAddr.getCanonicalHostName();
		}catch(UnknownHostException e){
			System.out.println("Host not found: " + e.getMessage());
		}
		

		// ..
		return hostName;
	}
	
	protected void setTimeoutToHttpConnection(HttpURLConnection conn) {
		int timeout = getTimeout();
		timeout = timeout * 1000;
		String parameterName = getTimeoutParamName();
		if (isTimeoutValid(timeout)) {
			conn.setConnectTimeout(timeout);
			conn.setReadTimeout(timeout);
		} else {
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);
			logTimoutPropertyMissing(parameterName);
		}
	}
	
	protected void logTimoutPropertyMissing(String propertyName) {
		LOG.warn(String.format("Timeout property %s value is < 0. Set timeout to 5s", propertyName));
	}
	
	protected boolean isTimeoutValid(int timeout) {
		if (timeout < 0) {
			return false;
		}
		return true;
	}

}