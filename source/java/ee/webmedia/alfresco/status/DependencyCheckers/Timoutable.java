package ee.webmedia.alfresco.status.DependencyCheckers;

public interface Timoutable {
	
	public int getTimeout();
	
	public String getTimeoutParamName();

}
