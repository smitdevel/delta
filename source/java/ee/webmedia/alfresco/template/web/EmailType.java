package ee.webmedia.alfresco.template.web;

public enum EmailType {
    UNIVERSAL("Universaalne"),
    SEND("Saada välja"),
    FORWARD("Edasta infoks");
    
    private String valueName;

    EmailType(String valueName) {
        this.valueName = valueName;
    }
    
    public String getValueName() {
        return valueName;
    }
}
