package ee.webmedia.alfresco.document.assignresponsibility.service;

import static ee.webmedia.alfresco.common.web.BeanHelper.getCaseFileLogService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getLogService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getPrivilegeService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getUserService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowDbService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.RegexQNamePattern;

import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamicService;
import ee.webmedia.alfresco.document.assignresponsibility.model.AssignResponsibilityModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.privilege.model.Privilege;
import ee.webmedia.alfresco.user.service.UserService;
import ee.webmedia.alfresco.workflow.model.CompoundWorkflowType;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.service.Task;
import ee.webmedia.alfresco.workflow.service.WorkflowService;

public class AssignResponsibilityServiceImpl implements AssignResponsibilityService {
    private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(AssignResponsibilityServiceImpl.class);

    private DocumentDynamicService documentDynamicService;
    private WorkflowService workflowService;
    private UserService userService;
    
	private final String MSG_KEY_PREFIX = "responsibility_log_";

    @Override
    public void changeOwnerOfDesignatedObjects(String fromOwnerId, String toOwnerId, boolean isLeaving, List<NodeRef> designatedObjectRefs) {
        if (log.isDebugEnabled()) {
            log.debug("Assigning responsibility of working documents and new tasks from " + fromOwnerId + " to " + toOwnerId);
        }
        String previousOwnerId = (isLeaving) ? fromOwnerId : toOwnerId;
        String newOwnerId = (isLeaving) ? toOwnerId : fromOwnerId;
        for (NodeRef designatedObjectRef : designatedObjectRefs) {
        	boolean isTaskType = !getNodeService().exists(designatedObjectRef);
        	if (isTaskType) {
        		Task task = getWorkflowService().getTask(designatedObjectRef, false);
                NodeRef compoundWorkflowRef = getNodeService().getPrimaryParent(task.getWorkflowNodeRef()).getParentRef();
                previousOwnerId = task.getOwnerId();
                getWorkflowService().setTaskOwner(designatedObjectRef, newOwnerId, true);
                CompoundWorkflow compoundWorkflow = getWorkflowService().getCompoundWorkflow(compoundWorkflowRef);
                if (compoundWorkflow.isDocumentWorkflow()) {
                    NodeRef docRef = compoundWorkflow.getParent();
                    transferDocumentPermissions(docRef, previousOwnerId, newOwnerId, true);
                } else if (compoundWorkflow.isCaseFileWorkflow()) {
                    NodeRef caseFileRef = compoundWorkflow.getParent();
                    transferDocumentPermissions(caseFileRef, previousOwnerId, newOwnerId, true);
                    transferCaseFileAssocPermissions(caseFileRef, previousOwnerId, newOwnerId);
                } else {
                	transferCompoundWorkflowAssocDocumentsPermissions(compoundWorkflowRef, newOwnerId, previousOwnerId);
                }
                getPrivilegeService().reindex(compoundWorkflowRef);
                addTaskResponsibilityChangeLog(designatedObjectRef, isLeaving);
        	} else {
        		if (getNodeService().isType(designatedObjectRef, DocumentCommonModel.Types.DOCUMENT)) {
        			String previousOwnerName = (String) getNodeService().getProperty(designatedObjectRef, DocumentDynamicModel.Props.OWNER_NAME);
                    documentDynamicService.setOwner(designatedObjectRef, newOwnerId, isLeaving);
                    addDocumentResponsibilityChangeLog(designatedObjectRef, isLeaving, previousOwnerName);
            	} else if (getNodeService().isType(designatedObjectRef, WorkflowCommonModel.Types.COMPOUND_WORKFLOW)) {
        			String previousOwnerName = (String) getNodeService().getProperty(designatedObjectRef, WorkflowCommonModel.Props.OWNER_NAME);
        			if (!isLeaving) {
                        previousOwnerId = (String) getNodeService().getProperty(designatedObjectRef, WorkflowCommonModel.Props.OWNER_ID);
                    }
                    workflowService.setCompoundWorkflowOwner(designatedObjectRef, newOwnerId, isLeaving);
                    transferCompoundWorkflowAssocDocumentsPermissions(designatedObjectRef, newOwnerId, previousOwnerId);
                    addCompoundWorkflowResponsibilityChangeLog(designatedObjectRef, isLeaving, previousOwnerName);
            	} else if (getNodeService().isType(designatedObjectRef, CaseFileModel.Types.CASE_FILE)) {
        			String previousOwnerName = (String) getNodeService().getProperty(designatedObjectRef, DocumentDynamicModel.Props.OWNER_NAME);
        			if (!isLeaving) {
                        previousOwnerId = (String) getNodeService().getProperty(designatedObjectRef, DocumentDynamicModel.Props.OWNER_ID);
                    }
                    documentDynamicService.setOwner(designatedObjectRef, newOwnerId, isLeaving);
                    transferCaseFileAssocPermissions(designatedObjectRef, previousOwnerId, newOwnerId);
                    addCaseFileResponsibilityChangeLog(designatedObjectRef, isLeaving, previousOwnerName);
            	}
            	if (isLeaving) {
            		getNodeService().setProperty(designatedObjectRef, AssignResponsibilityModel.Props.RESPONSIBILITY_ASSIGNER_ID, previousOwnerId);
            	} else {
            		getNodeService().removeProperty(designatedObjectRef, AssignResponsibilityModel.Props.RESPONSIBILITY_ASSIGNER_ID);
            	}
                getPrivilegeService().reindex(designatedObjectRef);
        	}
        }
    }
    
    private void transferCompoundWorkflowAssocDocumentsPermissions(NodeRef designatedObjectRef, String newOwnerId, String previousOwnerId) {
        CompoundWorkflowType type = getWorkflowService().getCompoundWorkflowType(designatedObjectRef);
    	if (CompoundWorkflowType.INDEPENDENT_WORKFLOW.equals(type)) {
            List<AssociationRef> workflowDocuments = getNodeService().getSourceAssocs(designatedObjectRef, DocumentCommonModel.Assocs.WORKFLOW_DOCUMENT);
            for (AssociationRef workflowDocument : workflowDocuments) {
            	NodeRef document = workflowDocument.getSourceRef();
            	List<AssociationRef> documentWorkflows = getNodeService().getTargetAssocs(document, DocumentCommonModel.Assocs.WORKFLOW_DOCUMENT);
            	boolean removePreviousOwnerPermissions = true;
            	for (AssociationRef documentWorkflow : documentWorkflows) {
            		if (containsOtherCompoundWorkflowWithSameOwner(designatedObjectRef, documentWorkflow.getTargetRef(), previousOwnerId)) {
            			removePreviousOwnerPermissions = false;
            		}
            	}
                transferDocumentPermissions(document, previousOwnerId, newOwnerId, removePreviousOwnerPermissions);
            }
        } else if (CompoundWorkflowType.DOCUMENT_WORKFLOW.equals(type)) {
            CompoundWorkflow cpWf = workflowService.getCompoundWorkflow(designatedObjectRef);
            NodeRef document = cpWf.getParent();
        	List<ChildAssociationRef> documentWorkflows = getNodeService().getChildAssocs(document, WorkflowCommonModel.Assocs.COMPOUND_WORKFLOW, RegexQNamePattern.MATCH_ALL);
        	boolean removePreviousOwnerPermissions = true;
        	for (ChildAssociationRef documentWorkflow : documentWorkflows) {
        		if (containsOtherCompoundWorkflowWithSameOwner(designatedObjectRef, documentWorkflow.getChildRef(), previousOwnerId)) {
        			removePreviousOwnerPermissions = false;
        		}
        	}
        	transferDocumentPermissions(document, previousOwnerId, newOwnerId, removePreviousOwnerPermissions);
        }
    }
    
    private boolean containsOtherCompoundWorkflowWithSameOwner(NodeRef designatedObjectRef, NodeRef parentCompoundWorkflowNodeRef, String previousOwnerId) {
		CompoundWorkflow childCompoundWorkflow = workflowService.getCompoundWorkflow(parentCompoundWorkflowNodeRef);
		return childCompoundWorkflow.getNodeRef() != designatedObjectRef && childCompoundWorkflow.getOwnerId().equals(previousOwnerId);
    }
    
    private void transferCaseFileAssocPermissions(NodeRef caseFileRef, String previousOwnerId, String newOwnerId) {
    	if (caseFileRef == null) {
    		return;
    	}
    	List<AssociationRef> caseFileDocuments = getNodeService().getTargetAssocs(caseFileRef, CaseFileModel.Assocs.CASE_FILE_DOCUMENT);
        for (AssociationRef caseFileDocument : caseFileDocuments) {
        	transferDocumentPermissions(caseFileDocument.getTargetRef(), previousOwnerId, newOwnerId, true);
        }
        List<ChildAssociationRef> childDocuments = getNodeService().getChildAssocs(caseFileRef, DocumentCommonModel.Types.DOCUMENT, RegexQNamePattern.MATCH_ALL);
        for (ChildAssociationRef childAssociationRef : childDocuments) {
        	transferDocumentPermissions(childAssociationRef.getChildRef(), previousOwnerId, newOwnerId, true);
        }
    }
    
    private void addTaskResponsibilityChangeLog(NodeRef designatedObjectRef, boolean isLeaving) {
    	Task task = getWorkflowService().getTask(designatedObjectRef, false);
    	if (isLeaving) {
        	getWorkflowDbService().updateResponsibilityAssignerId(designatedObjectRef, AuthenticationUtil.getRunAsUser());
        	task = getWorkflowService().getTask(designatedObjectRef, false);
        	getLogService().addLogEntry(LogEntry.create(LogObject.TASK, userService, task.getNodeRef(), MSG_KEY_PREFIX + "assign_task", userService.getCurrentUserFirstAndLastName(), 
        			task.getOwnerName()));
        } else {
        	String previousOwner = task.getOwnerName();
        	getWorkflowDbService().updateResponsibilityAssignerId(designatedObjectRef, null);
        	getLogService().addLogEntry(LogEntry.create(LogObject.TASK, userService, task.getNodeRef(), MSG_KEY_PREFIX + "revert_task", userService.getCurrentUserFirstAndLastName(), 
        			previousOwner));
        }
    }
    
    private void addDocumentResponsibilityChangeLog(NodeRef designatedObjectRef, boolean isLeaving, String previousOwnerName) {
        if (isLeaving) {
			String nextOwner = (String) getNodeService().getProperty(designatedObjectRef, DocumentDynamicModel.Props.OWNER_NAME);
            getLogService().addLogEntry(LogEntry.create(LogObject.DOCUMENT, getUserService(), designatedObjectRef, MSG_KEY_PREFIX + "assign_document", userService.getCurrentUserFirstAndLastName(), nextOwner));
        } else {
            getLogService().addLogEntry(LogEntry.create(LogObject.DOCUMENT, getUserService(), designatedObjectRef, MSG_KEY_PREFIX + "revert_document", userService.getCurrentUserFirstAndLastName(), previousOwnerName));
        }
    }
    
    private void addCompoundWorkflowResponsibilityChangeLog(NodeRef designatedObjectRef, boolean isLeaving, String previousOwnerName) {
    	if (isLeaving) {
			String nextOwnerName = (String) getNodeService().getProperty(designatedObjectRef, WorkflowCommonModel.Props.OWNER_NAME);
            getLogService().addLogEntry(LogEntry.create(LogObject.COMPOUND_WORKFLOW, userService, designatedObjectRef, MSG_KEY_PREFIX + "assign_workflow", 
            		userService.getCurrentUserFirstAndLastName(), nextOwnerName));
        } else {
            getLogService().addLogEntry(LogEntry.create(LogObject.COMPOUND_WORKFLOW, userService, designatedObjectRef, MSG_KEY_PREFIX + "revert_workflow", 
            		userService.getCurrentUserFirstAndLastName(), previousOwnerName));
        }
    }
    
    private void addCaseFileResponsibilityChangeLog(NodeRef designatedObjectRef, boolean isLeaving, String previousOwnerName) {
        if (isLeaving) {
 			String nextOwner = (String) getNodeService().getProperty(designatedObjectRef, DocumentDynamicModel.Props.OWNER_NAME);
             getCaseFileLogService().addCaseFileLog(designatedObjectRef, MSG_KEY_PREFIX + "assign_casefile", userService.getCurrentUserFirstAndLastName(), nextOwner);
         } else {
             getCaseFileLogService().addCaseFileLog(designatedObjectRef, MSG_KEY_PREFIX + "revert_casefile", userService.getCurrentUserFirstAndLastName(), previousOwnerName);
         }
    }
    
    private void transferDocumentPermissions(NodeRef docRef, String previousOwnerId, String newOwnerId, boolean removePreviousOwnerPermissions) {
    	if (docRef != null) {
    		Set<Privilege> transferedPrivileges = new HashSet<>();
            if (getNodeService().isType(docRef, CaseFileModel.Types.CASE_FILE)) {
            	transferedPrivileges.add(Privilege.VIEW_CASE_FILE);
            	transferedPrivileges.add(Privilege.EDIT_CASE_FILE);

        	} else if (getNodeService().isType(docRef, DocumentCommonModel.Types.DOCUMENT)) {
        		transferedPrivileges.add(Privilege.VIEW_DOCUMENT_META_DATA);
        		transferedPrivileges.add(Privilege.VIEW_DOCUMENT_FILES);
        		transferedPrivileges.add(Privilege.EDIT_DOCUMENT);
        	} else {
        		throw new IllegalArgumentException("Document nodeRef is not type of casefile or document!");
        	}
            if (removePreviousOwnerPermissions) {
        		getPrivilegeService().removeAllPermissions(docRef, previousOwnerId);
            }
    		getPrivilegeService().setPermissions(docRef, newOwnerId, transferedPrivileges);
    	}
    }

    // START: getters / setters

    public void setDocumentDynamicService(DocumentDynamicService documentDynamicService) {
        this.documentDynamicService = documentDynamicService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    // END: getters / setters

}
