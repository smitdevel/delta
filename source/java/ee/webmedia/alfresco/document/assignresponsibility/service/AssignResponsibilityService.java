package ee.webmedia.alfresco.document.assignresponsibility.service;

import java.util.List;
import org.alfresco.service.cmr.repository.NodeRef;

public interface AssignResponsibilityService {

    String BEAN_NAME = "AssignResponsibilityService";

	void changeOwnerOfDesignatedObjects(String fromOwnerId, String toOwnerId, boolean isLeaving,
			List<NodeRef> items);

}
