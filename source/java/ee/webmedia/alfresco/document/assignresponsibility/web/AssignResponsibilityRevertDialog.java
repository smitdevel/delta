package ee.webmedia.alfresco.document.assignresponsibility.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.ui.common.component.data.UIRichList;

import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.casefile.service.CaseFile;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.model.Document;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.web.TaskAndDocumentDataProvider;

public class AssignResponsibilityRevertDialog extends BaseDialogBean {

	public static final String BEAN_NAME = "AssignResponsibilityRevertDialog";

    private Map<NodeRef, Boolean> listCheckboxes = new HashMap<>();
    
    protected UIRichList documentsRichList;
    protected UIRichList tasksRichList;
    protected UIRichList compoundWorkflowsRichList;
    protected UIRichList caseFilesRichList;
    private List<Document> documents = new ArrayList<>();
    private List<CompoundWorkflow> compoundWorkflows = new ArrayList<>();
    private List<CaseFile> caseFiles = new ArrayList<>();
    private String ownerId;

	@Override
	protected String finishImpl(FacesContext context, String outcome) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String cancel() {
        BeanHelper.getUserDetailsDialog().setupUser(ownerId);
        return super.cancel();
	}

	public TaskAndDocumentDataProvider getTasks() {
	    	List<NodeRef> userTasksRef = new ArrayList<>();
	        for (Pair<NodeRef, QName> pair : BeanHelper.getWorkflowDbService().searchResponsibilityAssignerTasks(ownerId)) {
	        	userTasksRef.add(pair.getFirst());
	        }
	    	TaskAndDocumentDataProvider tasks = new TaskAndDocumentDataProvider(userTasksRef);
	    	tasks.loadPage(0, userTasksRef.size());
	    	return tasks;
	}

	public Map<NodeRef, Boolean> getListCheckboxes() {
		return listCheckboxes;
	}

	public UIRichList getDocumentsRichList() {
		return documentsRichList;
	}

	public UIRichList getTasksRichList() {
		return tasksRichList;
	}

	public UIRichList getCompoundWorkflowsRichList() {
		return compoundWorkflowsRichList;
	}

	public UIRichList getCaseFilesRichList() {
		return caseFilesRichList;
	}
	
	public boolean isShowRevertButton() {
        ownerId = (String) BeanHelper.getAssignResponsibilityBean().getFromOwnerId();
		int resultsCount = loadEntities().size() + BeanHelper.getWorkflowDbService().searchResponsibilityAssignerTasks(ownerId).size();
		return resultsCount > 0;
	}
	
	public void updateResults(ActionEvent event) {
		updateResults();
	}
	
	public void updateResults() {
		initRichLists();
		documents.clear();
		compoundWorkflows.clear();
		caseFiles.clear();
        List<NodeRef> assignedEntities = loadEntities();
        for (NodeRef nodeRef : assignedEntities) {
        	if (BeanHelper.getNodeService().getType(nodeRef).equals(DocumentCommonModel.Types.DOCUMENT)) {
        		documents.add(new Document(nodeRef));
        	} else if (BeanHelper.getNodeService().getType(nodeRef).equals(WorkflowCommonModel.Types.COMPOUND_WORKFLOW)) {
        		compoundWorkflows.add(BeanHelper.getWorkflowService().getCompoundWorkflow(nodeRef));
        	} else if (BeanHelper.getNodeService().getType(nodeRef).equals(CaseFileModel.Types.CASE_FILE)) {
        		caseFiles.add(BeanHelper.getCaseFileService().getCaseFile(nodeRef));
        	}
        }
		documentsRichList.setValue(documents);
		documentsRichList.bind();
		tasksRichList.setValue(getTasks());
		tasksRichList.bind();
		compoundWorkflowsRichList.setValue(compoundWorkflows);
		compoundWorkflowsRichList.bind();
		caseFilesRichList.setValue(caseFiles);
		caseFilesRichList.bind();
	}
	
	private void initRichLists() {
		if (documentsRichList == null) {
			documentsRichList = createRichList("#{AssignResponsibilityRevertDialog.documents}");
		}
		if (tasksRichList == null) {
			tasksRichList = createRichList("#{AssignResponsibilityRevertDialog.tasks}");
		}
		if (caseFilesRichList == null) {
			caseFilesRichList = createRichList("#{AssignResponsibilityRevertDialog.caseFiles}");
		}
		if (compoundWorkflowsRichList == null) {
			compoundWorkflowsRichList = createRichList("#{AssignResponsibilityRevertDialog.compoundWorkflows}");
		}
	}
	
	@SuppressWarnings("deprecation")
	private UIRichList createRichList(String binding) {
		UIRichList documentsRichList = new UIRichList();
		documentsRichList.setRefreshOnBind(true);
		documentsRichList.setValueBinding("value", FacesContext.getCurrentInstance().getApplication().createValueBinding(binding));
		return documentsRichList;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public List<CompoundWorkflow> getCompoundWorkflows() {
		return compoundWorkflows;
	}

	public List<CaseFile> getCaseFiles() {
		return caseFiles;
	}

	private List<NodeRef> loadEntities() {
		return BeanHelper.getDocumentSearchService().searchNodesFromAllStores(
        		String.format("(@\\{http\\://alfresco.webmedia.ee/model/assignresponsibility/common/1.0\\}responsibilityAssignerId:\"%s\")",
        				ownerId), 
        		"assignerDocuments");
	}

	@Override
	public void init(Map<String, String> parameters) {
        ownerId = (String) BeanHelper.getAssignResponsibilityBean().getFromOwnerId();
		super.init(parameters);
	}
	
	
}
