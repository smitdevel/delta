<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<%@ page import="org.alfresco.web.app.Application" %>

<a:panel id="document-panel" styleClass="panel-100 with-pager" label="#{msg.documents}" progressive="true" rendered="#{ not empty AssignResponsibilityRevertDialog.documents}">
   <h:commandButton value="#{msg.applog_export_csv}" styleClass="rightButton" onclick="document.getElementById('export_document_csv').click()" type="button" />

   <%-- Main List --%>
   <a:richList id="documentList" viewMode="details" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt"
      width="100%" binding="#{AssignResponsibilityRevertDialog.documentsRichList}" var="r" >
         
      <%-- checkbox --%>
	  <a:column id="document-col-checkbox" primary="true" >
	  	<f:facet name="header">
        	<h:selectBooleanCheckbox id="document-col0-header" styleClass="selectAllDocumentHeaders" />
   	  	</f:facet>
        <h:selectBooleanCheckbox id="docuemnt-col1-checkbox" value="#{AssignResponsibilityRevertDialog.listCheckboxes[r.nodeRef]}" styleClass="documentHeaderSelectable" />
      </a:column>
         
      <%-- Title --%>
      <a:column id="doc-col-1" styleClass="#{r.cssStyleClass}" >
         <f:facet name="header">
            <a:sortLink id="doc-col-1-sort" label="#{msg.document_docName}" value="docName" styleClass="header" />
         </f:facet>
         <a:actionLink id="doc-col-1-text" value="#{r.docName}" action="#{DocumentDialog.action}" tooltip="#{r.docName}"
            actionListener="#{DocumentDialog.open}" rendered="#{r.cssStyleClass ne 'case'}" styleClass="tooltip condence100- no-underline highlight" >
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
         <!-- if row item is not document, but case, create link to documents list of case (row item is subclass of Document, to be shown as listItem in document list) -->
         <a:actionLink id="doc-col-1-link2docList" value="#{r.docName}" action="dialog:documentListDialog" tooltip="#{r.docName}"
            actionListener="#{DocumentListDialog.setup}" rendered="#{r.cssStyleClass == 'case'}" styleClass="condence20-" >
            <f:param name="caseNodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
      </a:column>

      <%-- Document type --%>
      <a:column id="doc-col-2" styleClass="#{r.cssStyleClass}" >
         <f:facet name="header">
            <a:sortLink id="doc-col-2-sort" label="#{msg.document_docType}" value="documentTypeName" styleClass="header" />
         </f:facet>
         <a:actionLink id="doc-col-2-text-1" value="#{r.documentTypeName}" action="#{DocumentDialog.action}" tooltip="#{r.documentTypeName}"
            actionListener="#{DocumentDialog.open}" styleClass="no-underline" >
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
      </a:column>
      
      <%-- regNumber --%>
      <a:column id="doc-col-3" primary="true" styleClass="#{r.cssStyleClass}" >
         <f:facet name="header">
            <a:sortLink id="doc-col-3-sort" label="#{msg.document_regNumber}" value="regNumber" styleClass="header" />
         </f:facet>
         <a:actionLink id="assignment_regnumber_1" value="#{r.akString}" style="font-weight: bold;" action="#{r.getAction}" tooltip="#{r.regNumber}"
            actionListener="#{r.open}" >
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
         <a:actionLink id="assignment_regnumber_2" value="#{r.regNumber}" action="#{r.getAction}" tooltip="#{r.regNumber}"
            actionListener="#{r.open}" >
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
      </a:column>
      
      <%-- Registration date --%>
      <a:column id="doc-col-4" primary="true" styleClass="#{r.cssStyleClass}" >
         <f:facet name="header">
            <a:sortLink id="doc-col-4-sort" label="#{msg.document_regDateTime}" value="regDateTime" styleClass="header" />
         </f:facet>
         <a:actionLink id="doc-col-4-text" value="#{r.regDateTimeStr}" action="#{r.getAction}" tooltip="#{r.regDateTimeStr}" styleClass="no-underline"
            actionListener="#{r.open}" >
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
      </a:column>
      
   </a:richList>

</a:panel>

<a:panel id="task-panel" styleClass="panel-100 with-pager" label="#{msg.tasks}" progressive="true" rendered="#{AssignResponsibilityRevertDialog.tasks.listSize > 0}" >
   <f:facet name="header">
      <h:selectBooleanCheckbox id="task-col0-header" value="false"  styleClass="selectAllHeader" />
   </f:facet>
   <h:commandButton value="#{msg.applog_export_csv}" styleClass="rightButton" onclick="document.getElementById('export_task_csv').click()" type="button" />
            
   <%-- Main List --%>
   <a:richList id="taskList" viewMode="details" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt"
      width="100%" binding="#{AssignResponsibilityRevertDialog.tasksRichList}" var="r" >

	  <%-- checkbox --%>
	  <a:column id="task-col-checkbox" primary="true" >
	  	<f:facet name="header">
        	<h:selectBooleanCheckbox id="task-col0-header" styleClass="selectAllTaskHeaders" />
   	  	</f:facet>
        <h:selectBooleanCheckbox id="task-col1-checkbox" value="#{AssignResponsibilityRevertDialog.listCheckboxes[r.task.nodeRef]}" styleClass="taskHeaderSelectable" />
      </a:column>
          
      <%-- Title --%>
      <a:column id="task-col1" styleClass="#{r.task.cssStyleClass}">
         <f:facet name="header">
            <a:sortLink id="task-col1-sort" label="#{msg.document_docName}" value="title" styleClass="header" />
         </f:facet>
         <a:actionLink id="task-col1-text-1" value="#{r.document.docName}" action="dialog:compoundWorkflowDialog" 
         	actionListener="#{CompoundWorkflowDialog.setupWorkflow}" styleClass="workflow-conf highlight" rendered="#{r.compoundWorkflow.documentWorkflow}">
            <f:param name="nodeRef" value="#{r.compoundWorkflow.nodeRef}" />
         </a:actionLink>
         <a:actionLink id="task-col1-text-2" value="#{r.task.compoundWorkflowTitle}" action="dialog:compoundWorkflowDialog" actionListener="#{CompoundWorkflowDialog.setupWorkflow}" styleClass="workflow-conf highlight" rendered="#{!r.compoundWorkflow.documentWorkflow}">
            <f:param name="nodeRef" value="#{r.compoundWorkflow.nodeRef}" />
         </a:actionLink>
      </a:column>

      <%-- Task type --%>
      <a:column id="task-col2" primary="true" styleClass="#{r.task.cssStyleClass}" style="width: 10%;">
         <f:facet name="header">
            <a:sortLink id="task-col2-sort" label="#{msg.statistics_text_type}" value="typeName" styleClass="header" />
         </f:facet>
         <a:actionLink id="task-col2-text" value="#{r.task.typeStr}" action="dialog:compoundWorkflowDialog" actionListener="#{CompoundWorkflowDialog.setupWorkflow}" styleClass="workflow-conf highlight" >
            <f:param name="nodeRef" value="#{r.compoundWorkflow.nodeRef}" />
         </a:actionLink>
     </a:column>
           
      <%-- dueDate --%>
      <a:column id="task-col3" primary="true" styleClass="#{r.task.cssStyleClass}">
         <f:facet name="header">
            <a:sortLink id="col3-sort" label="#{msg.statistics_text_due_date}" value="dueDateTimeStr" styleClass="header" />
         </f:facet>
         <a:actionLink id="assignment_dueDate_1" value="#{r.task.dueDateTimeStr}" action="#{r.action}" tooltip="#{r.task.dueDateTimeStr}" actionListener="#{r.actionListener}" styleClass="no-underline highlight" >
            <f:param name="nodeRef" value="#{r.actionNodeRef}" />
         </a:actionLink>
      </a:column>
      
      <%-- Status --%>
      <a:column id="task-col4" primary="true" styleClass="#{r.task.cssStyleClass}">
         <f:facet name="header">
            <a:sortLink id="col4-sort" label="#{msg.statistics_text_status}" value="status" styleClass="header" />
         </f:facet>
         <a:actionLink id="col4-text" value="#{r.task.status}" action="#{r.action}" tooltip="#{r.task.status}" actionListener="#{r.actionListener}" styleClass="no-underline highlight" >
            <f:param name="nodeRef" value="#{r.actionNodeRef}" />
         </a:actionLink>
      </a:column>
   </a:richList>

</a:panel>

<a:panel id="workflow-panel" styleClass="panel-100 with-pager" label="#{msg.compound_workflows}" progressive="true" rendered="#{ not empty AssignResponsibilityRevertDialog.compoundWorkflows}" >
   <h:commandButton value="#{msg.applog_export_csv}" styleClass="rightButton" onclick="document.getElementById('export_workflow_csv').click()" type="button" />

   <%-- Main List --%>
   <a:richList id="workflowList" viewMode="details" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt"
      width="100%" binding="#{AssignResponsibilityRevertDialog.compoundWorkflowsRichList}" var="r" >
      	  
      <%-- checkbox --%>
	  <a:column id="workflow-col-checkbox" primary="true" >
	    <f:facet name="header">
	        <h:selectBooleanCheckbox id="workflow-col0-header" styleClass="selectAllWorkflowHeaders" />
	    </f:facet>
        <h:selectBooleanCheckbox id="workflow-col1-checkbox" value="#{AssignResponsibilityRevertDialog.listCheckboxes[r.nodeRef]}" styleClass="wrokflowHeaderSelectable" />
      </a:column>
      
      <%-- Title --%>
      <a:column id="workflow-col1">
         <f:facet name="header">
            <a:sortLink id="workflow-col1-sort" label="#{msg.title}" value="title" styleClass="header" />
         </f:facet>
         <a:actionLink id="workflow-col1-text-1" value="#{r.parentDocumentTitle}" action="dialog:compoundWorkflowDialog" tooltip="#{r.parentDocumentTitle}" 
            actionListener="#{CompoundWorkflowDialog.setupWorkflow}" styleClass="tooltip condence20- no-underline highlight" rendered="#{r.documentWorkflow}" >
            <f:param name="nodeRef" value="#{r.nodeRef}" />
         </a:actionLink>
         <a:actionLink id="workflow-col1-text-2" value="#{r.title}" action="dialog:compoundWorkflowDialog" tooltip="#{r.title}" 
            actionListener="#{CompoundWorkflowDialog.setupWorkflow}" styleClass="tooltip condence20- no-underline highlight" rendered="#{!r.documentWorkflow}" >
            <f:param name="nodeRef" value="#{r.nodeRef}" />
         </a:actionLink>
      </a:column>
          
      <%-- Status --%>
      <a:column id="workflow-col2">
         <f:facet name="header">
            <a:sortLink id="workflow-col2-sort" label="#{msg.status}" value="status" styleClass="header" />
         </f:facet>
         <a:actionLink id="workflow-col2-text-1" value="#{r.status}" action="dialog:compoundWorkflowDialog" tooltip="#{r.status}"
            actionListener="#{CompoundWorkflowDialog.setupWorkflow}" styleClass="tooltip condence20- no-underline highlight" >
            <f:param name="nodeRef" value="#{r.nodeRef}" />
         </a:actionLink>
      </a:column>
   </a:richList>

</a:panel>

<a:panel id="casefile-panel" styleClass="panel-100 with-pager" label="#{msg.casefile_list_dialog_title}" progressive="true" rendered="#{ not empty AssignResponsibilityRevertDialog.caseFiles}" >
   	  <h:commandButton value="#{msg.applog_export_csv}" styleClass="rightButton" onclick="document.getElementById('export_casefile_csv').click()" type="button" />
      
      <a:richList id="caseFileList" viewMode="details" pageSize="#{BrowseBean.pageSizeContent}" rowStyleClass="recordSetRow"
      altRowStyleClass="recordSetRowAlt" width="100%" binding="#{AssignResponsibilityRevertDialog.caseFilesRichList}" var="r" initialSortColumn="validFrom" initialSortDescending="true">
      
      <%-- checkbox --%>
	  <a:column id="casfile-col-checkbox" primary="true" >
	  	<f:facet name="header">
        	<h:selectBooleanCheckbox id="document-col0-header" styleClass="selectAllCaseHeaders" />
   	  	</f:facet>
        <h:selectBooleanCheckbox id="docuemnt-col1-checkbox" value="#{AssignResponsibilityRevertDialog.listCheckboxes[r.nodeRef]}" styleClass="caseHeaderSelectable" />
      </a:column>
      
      <a:column id="casfile-col1">
         <f:facet name="header">
            <a:sortLink id="casfile-col1-lbl" label="#{msg.casefile_title_column}" value="title" styleClass="header" />
         </f:facet>
         <a:actionLink id="casfile-col1-act" value="#{r.title}" actionListener="#{CaseFileDialog.openFromDocumentList}" styleClass="tooltip condence100- no-underline highlight">
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
      </a:column>
      
      <a:column id="col2">
         <f:facet name="header">
            <a:sortLink id="casfile-col2-lbl" label="#{msg.casefile_type_column}" value="type" styleClass="header" />
         </f:facet>
         <a:outputText value="#{r.type}" id="casfile-col2-text"/>
      </a:column>
      
      <%-- Valid from --%>
      <a:column id="col5">
         <f:facet name="header">
            <a:sortLink id="col5-lbl" label="#{msg.casefile_volumemark}" value="validFrom" styleClass="header" />
         </f:facet>
         <a:outputText value="#{r.volumeMark}" id="col3-text"/>
      </a:column>

   </a:richList>

</a:panel>

<a:panel id="assign-responsibility-panel" styleClass="panel-100" label="#{msg.assign_responsibility}">
   <a:booleanEvaluator id="instructionSet" value="#{AssignResponsibilityBean.instructionSet}">
      <f:verbatim><div class="message"></f:verbatim>
      <h:outputText value="#{AssignResponsibilityBean.instruction}" styleClass="medium" />
      <f:verbatim></div></f:verbatim>
   </a:booleanEvaluator>
   <h:commandButton id="revertButton" actionListener="#{AssignResponsibilityBean.revert}" value="#{msg.assign_responsibility_revert}" style="margin-top: 5px;" />
</a:panel>

<jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/select-all-headers.jsp" />
<jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/disable-dialog-finish-button.jsp" />