<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="assign-responsibility-panel" styleClass="panel-100" label="#{msg.assign_responsibility}" rendered="#{AssignResponsibilityBean.panelVisible}">
   <a:booleanEvaluator value="#{AssignResponsibilityBean.instructionSet}">
      <f:verbatim><div class="message"></f:verbatim>
      <h:outputText value="#{AssignResponsibilityBean.instruction}" styleClass="medium" />
      <f:verbatim></div></f:verbatim>
   </a:booleanEvaluator>
   <h:commandButton id="perform" action="dialog:assignResponsibilityDetailsDialog" value="#{msg.assign_responsibility_perform}" style="margin-top: 5px;" />
   <h:commandButton id="revert" actionListener="#{AssignResponsibilityRevertDialog.updateResults}" action="dialog:assignResponsibilityRevertDialog" value="#{msg.assign_responsibility_revert}" rendered="#{AssignResponsibilityRevertDialog.showRevertButton}"  style="margin-left: 10px; margin-top: 5px;" />
</a:panel>
