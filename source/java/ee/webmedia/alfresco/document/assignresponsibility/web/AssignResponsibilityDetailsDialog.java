package ee.webmedia.alfresco.document.assignresponsibility.web;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentSearchService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getUserDetailsDialog;
import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowDbService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getAssignResponsibilityBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.ui.common.component.data.UIRichList;
import org.apache.myfaces.application.jsp.JspStateManagerImpl;

import ee.webmedia.alfresco.casefile.service.CaseFile;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.model.Document;
import ee.webmedia.alfresco.simdhs.CSVExporter;
import ee.webmedia.alfresco.simdhs.DataReader;
import ee.webmedia.alfresco.simdhs.RichListDataReader;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.web.TaskAndDocumentDataProvider;

public class AssignResponsibilityDetailsDialog extends BaseDialogBean {

	public static final String BEAN_NAME = "AssignResponsibilityDetailsDialog";

    private Map<NodeRef, Boolean> listCheckboxes = new HashMap<>();
    
    protected UIRichList documentsRichList;
    protected UIRichList tasksRichList;
    protected UIRichList compoundWorkflowsRichList;
    protected UIRichList caseFilesRichList;
    private String ownerId;
    
    @Override
    public void init(Map<String, String> params) {
        super.init(params);
        ownerId = (String) BeanHelper.getAssignResponsibilityBean().getFromOwnerId();
        getUserDetailsDialog().setupUser(ownerId);
		updateResults();
    }
    
	@Override
	protected String finishImpl(FacesContext context, String outcome) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String cancel() {
        getUserDetailsDialog().setupUser(ownerId);
        return super.cancel();
	}
	
    public List<Document> getDocuments() {
    	List<NodeRef> docs = getDocumentSearchService().searchInWorkingStatusOwnerDocuments(ownerId);
        List<Document> documents = new ArrayList<>();
        for (NodeRef docNodeRef : docs) {
            documents.add(new Document(docNodeRef));
        }
        return documents;
    }
    
    public TaskAndDocumentDataProvider getTasks() {
        List<NodeRef> userTasksRef = new ArrayList<>();
        for (Pair<NodeRef, QName> pair : getWorkflowDbService().searchWorkflowOwnerTaskNodeRefs(ownerId)) {
        	userTasksRef.add(pair.getFirst());
        }
        TaskAndDocumentDataProvider tasks = new TaskAndDocumentDataProvider(userTasksRef);
        tasks.loadPage(0, userTasksRef.size());
        return tasks;
    }
    
    public List<CompoundWorkflow> getCompoundWorkflows() {
        List<NodeRef> compoundWorkflowsRefs = getWorkflowService().searchOwnerCompoundWorkflowRefs(ownerId, -1);
        List<CompoundWorkflow> compoundWorkflows = new ArrayList<>();
        for (NodeRef compoundWorkflowsRef : compoundWorkflowsRefs) {
            CompoundWorkflow compoundWorkflow = getWorkflowService().getCompoundWorkflow(compoundWorkflowsRef);
            compoundWorkflows.add(compoundWorkflow);
        }
        return compoundWorkflows;
    }
    
    public List<CaseFile> getCaseFiles() {
    	List<NodeRef> caseFiles = getDocumentSearchService().searchOpenCaseFilesOwnerId(ownerId, false);
        List<CaseFile> result = new ArrayList<CaseFile>(caseFiles.size());
        for (NodeRef caseFileRef : caseFiles) {
            result.add(BeanHelper.getCaseFileService().getCaseFile(caseFileRef));
        }
        return result;
    }
    
    public void setup(ActionEvent event) {
    	event.getComponent();
    }

	public Map<NodeRef, Boolean> getListCheckboxes() {
		return listCheckboxes;
	}

	public UIRichList getDocumentsRichList() {
		updateResults();
		return documentsRichList;
	}
	
	public UIRichList getTasksRichList() {
		return tasksRichList;
	}

	public UIRichList getCompoundWorkflowsRichList() {
		return compoundWorkflowsRichList;
	}

	public UIRichList getCaseFilesRichList() {
		return caseFilesRichList;
	}
	
	public void updateResults() {
		if (documentsRichList == null) {
			documentsRichList = createRichList("#{AssignResponsibilityDetailsDialog.documents}");
		}
		documentsRichList.setValue(getDocuments());
		documentsRichList.bind();
		
		if (tasksRichList == null) {
			tasksRichList = createRichList("#{AssignResponsibilityDetailsDialog.tasks}");
		}
		tasksRichList.setValue(getTasks());
		tasksRichList.bind();
		
		if (compoundWorkflowsRichList == null) {
			compoundWorkflowsRichList = createRichList("#{AssignResponsibilityDetailsDialog.compoundWorkflows}");
		}
		compoundWorkflowsRichList.setValue(getCompoundWorkflows());
		compoundWorkflowsRichList.bind();
		
		if (caseFilesRichList == null) {
			caseFilesRichList = createRichList("#{AssignResponsibilityDetailsDialog.caseFiles}");
		}
		caseFilesRichList.setValue(getCaseFiles());
		caseFilesRichList.bind();
	}
	
	@SuppressWarnings("deprecation")
	private UIRichList createRichList(String binding) {
		UIRichList documentsRichList = new UIRichList();
		documentsRichList.setRefreshOnBind(true);
		documentsRichList.setValueBinding("value", FacesContext.getCurrentInstance().getApplication().createValueBinding(binding));
		return documentsRichList;
	}
	
    public void exportDocumentsAsCsv(ActionEvent event) {
    	exportAsCsv("documentList");
    }
    
    public void exportTasksAsCsv(ActionEvent event) {
    	exportAsCsv("taskList");
    }
    
    public void exportWorkflowsAsCsv(ActionEvent event) {
    	exportAsCsv("workflowList");
    }
    
    public void exportCaseFilesAsCsv(ActionEvent event) {
    	exportAsCsv("caseFileList");
    }

    public void exportAsCsv(String richListName) {
        DataReader dataReader = new RichListDataReader();
        CSVExporter exporter = new CSVExporter(dataReader);
        exporter.export(richListName);

        // hack for incorrect view id in the next request
        JspStateManagerImpl.ignoreCurrentViewSequenceHack();
    }

}
