package ee.webmedia.alfresco.document.assignresponsibility.web;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.assignresponsibility.model.AssignResponsibilityModel;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.user.model.UserModel;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.utils.UserUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.TransientNode;
import org.apache.commons.lang.StringUtils;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ee.webmedia.alfresco.common.web.BeanHelper.*;

public class AssignResponsibilityBean implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String BEAN_NAME = "AssignResponsibilityBean";
    private final String OWNER_ID = "{temp}ownerId";

    private Node node;
    private String fromOwnerId;
    private boolean leaving;

    public Node getNode() {
        if (node == null) {
            node = new TransientNode(AssignResponsibilityModel.Types.ASSIGN_RESPONSIBILITY, null, null);
        }
        return node;
    }

    public void setOwner(String userName) {
        String fullName = getUserService().getUserFullName(userName);
        Map<String, Object> props = getNode().getProperties();
        props.put(AssignResponsibilityModel.Props.OWNER_NAME.toString(), fullName);
        props.put(OWNER_ID, userName);
    }

    public void unsetOwner() {
        node = null;
    }

    public boolean isOwnerUnset() {
        return StringUtils.isBlank((String) getNode().getProperties().get(OWNER_ID));
    }

    public void execute(@SuppressWarnings("unused") ActionEvent event) {
        String toOwnerId = (String) getNode().getProperties().get(OWNER_ID);
        Map<NodeRef, Boolean> checkboxes = BeanHelper.getAssignResponsibilityDetailsDialog().getListCheckboxes();
        List<NodeRef> designatedObjects = new ArrayList<>();
        for (Map.Entry<NodeRef, Boolean> entry : checkboxes.entrySet()) {
        	if (entry.getValue()) {
        		designatedObjects.add(entry.getKey());
        	}
        }

        if (designatedObjects.isEmpty()) {
        	MessageUtil.addErrorMessage("assign_responsibility_empty");
        	return;
        }
        if (fromOwnerId == null || toOwnerId == null) {
        	throw new NullPointerException("Owner is null");
        }
        getAssignResponsibilityService().changeOwnerOfDesignatedObjects(fromOwnerId, toOwnerId, true, designatedObjects);

        BeanHelper.getAssignResponsibilityDetailsDialog().getListCheckboxes().clear();
        BeanHelper.getAssignResponsibilityDetailsDialog().updateResults();
        
        leaving = true;
        MessageUtil.addInfoMessage("assign_responsibility_perform_success", UserUtil.getPersonFullName1(BeanHelper.getUserService().getUserProperties(toOwnerId)));
    }

    public void revert(@SuppressWarnings("unused") ActionEvent event) {
        String toOwnerId = (String) getUserService().getUserProperties(fromOwnerId).get(UserModel.Props.LIABILITY_GIVEN_TO_PERSON_ID);
        NodeRef fromOwnerRef = null;
        if (toOwnerId == null && (fromOwnerRef = getUserService().getPerson(fromOwnerId)) != null) {
            toOwnerId = (String) BeanHelper.getNodeService().getProperty(fromOwnerRef, UserModel.Props.LIABILITY_GIVEN_TO_PERSON_ID);
        }
        Map<NodeRef, Boolean> checkboxes = BeanHelper.getAssignResponsibilityRevertDialog().getListCheckboxes();
        List<NodeRef> designatedObjects = new ArrayList<>();
        for (Map.Entry<NodeRef, Boolean> entry : checkboxes.entrySet()) {
        	if (entry.getValue()) {
        		designatedObjects.add(entry.getKey());
        	}
        }

        if (designatedObjects.isEmpty()) {
        	MessageUtil.addErrorMessage("assign_responsibility_revert_empty");
        	return;
        }
        getAssignResponsibilityService().changeOwnerOfDesignatedObjects(fromOwnerId, toOwnerId, false, designatedObjects);
        BeanHelper.getAssignResponsibilityRevertDialog().getListCheckboxes().clear();
        BeanHelper.getAssignResponsibilityRevertDialog().updateResults();
        leaving = false;
        unsetOwner();
        BeanHelper.getUserDetailsDialog().reloadUser();
        MessageUtil.addInfoMessage("assign_responsibility_revert_success");
    }

    public String getAssingResponsibilityMessage() {
        Node currentUser = getUserService().getUser(AuthenticationUtil.getRunAsUser());

        if (currentUser.hasAspect(UserModel.Aspects.LEAVING)) {
            Map<String, Object> properties = currentUser.getProperties();
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            StringBuilder builder = new StringBuilder();
            builder.append("<div class=\"message message-red\">");
            builder.append(MessageUtil.getMessage(FacesContext.getCurrentInstance(), "assign_responsibility_message",
                    dateFormat.format(properties.get(UserModel.Props.LEAVING_DATE_TIME)),
                    getUserService().getUserFullName((String) properties.get(UserModel.Props.LIABILITY_GIVEN_TO_PERSON_ID)
                            )));
            builder.append("</div>");

            return builder.toString();
        }

        return "";
    }

    public String getInstruction() {
        return getParametersService().getStringParameter(Parameters.ASSIGN_RESPONSIBILITY_INSTRUCTION);
    }

    public boolean isInstructionSet() {
        return StringUtils.isNotBlank(getInstruction());
    }

    public boolean isNotLeaving() {
        return !leaving;
    }

    public boolean isLeavingAndAdmin() {
        return leaving && getUserService().isAdministrator();
    }

    public boolean isPanelVisible() {
        return isNotLeaving() || isLeavingAndAdmin();
    }

    public void updateLiabilityGivenToPerson(Node user) {
        leaving = user.hasAspect(UserModel.Aspects.LEAVING);
        fromOwnerId = (String) user.getProperties().get(ContentModel.PROP_USERNAME);
        if (leaving) {
            String liabilityGivenToPersonId = (String) user.getProperties().get(UserModel.Props.LIABILITY_GIVEN_TO_PERSON_ID);
            String fullName = getUserService().getUserFullName(liabilityGivenToPersonId);
            Map<String, Object> props = getNode().getProperties();
            props.put(AssignResponsibilityModel.Props.OWNER_NAME.toString(), fullName);
            props.put(OWNER_ID, liabilityGivenToPersonId);
        } else {
            unsetOwner();
        }
    }

	public String getFromOwnerId() {
		return fromOwnerId;
	}

}
