package ee.webmedia.alfresco.document.web;

import java.util.*;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamic;
import ee.webmedia.alfresco.document.search.web.DocumentListDataProvider;
import ee.webmedia.alfresco.utils.MessageUtil;
import org.alfresco.service.cmr.repository.NodeRef;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

public class PublishingDocumentListDialog extends BaseDocumentListDialog {
    private static final long serialVersionUID = 1L;
    public static final String BEAN_NAME = "PublishingDocumentListDialog";

    /**
     * @param event from/menu
     */
    public void setup(ActionEvent event) {
        restored();
    }

    @Override
    public void restored() {
        documentProvider = new DocumentListDataProvider(BeanHelper.getAdrService().getAdrChangedDocuments());
    }



    @Override
    public String getListTitle() {
        return MessageUtil.getMessage(FacesContext.getCurrentInstance(), "document_publishing");
    }

    @Override
    public String getColumnsFile() {
        return "/WEB-INF/classes/ee/webmedia/alfresco/document/web/publishing-document-list-dialog-columns.jsp";
    }

}
