<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/wm.tld" prefix="wm" %>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

      <%-- Title --%>
      <a:column id="col1" styleClass="#{r.cssStyleClass}" >
         <f:facet name="header">
            <a:sortLink id="col1-sort" label="#{msg.document_docName}" value="docName" styleClass="header" />
         </f:facet>
         <a:actionLink id="col1-text-1" value="#{r.docName}" action="#{r.getAction}" tooltip="#{r.docName}"
            actionListener="#{r.open}" styleClass="tooltip condence100- no-underline highlight " >
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
         </a:actionLink>
      </a:column>

      <%-- AccessRestriction --%>
      <a:column id="col2" primary="true" styleClass="#{r.cssStyleClass}" >
          <f:facet name="header">
              <a:sortLink id="col2-sort" label="#{msg.document_accessRestriction}" value="accessRestriction" styleClass="header" />
          </f:facet>
          <a:actionLink id="col2-text" value="#{r.accessRestriction}" action="#{r.getAction}" tooltip="#{r.accessRestriction}"
                        actionListener="#{r.open}" styleClass="no-underline" >
              <f:param name="nodeRef" value="#{r.node.nodeRef}" />
          </a:actionLink>
      </a:column>

      <%-- AccessRestrictionReason --%>
      <a:column id="col8" primary="true" styleClass="#{r.cssStyleClass}" >
          <f:facet name="header">
              <a:sortLink id="col8-sort" label="#{msg.document_accessRestrictionReasonFull}" value="accessRestrictionReason" styleClass="header" />
          </f:facet>
          <a:actionLink id="col8-text" value="#{r.accessRestrictionReason}" action="#{r.getAction}" tooltip="#{r.accessRestrictionReason}"
                        actionListener="#{r.open}" styleClass="no-underline" >
              <f:param name="nodeRef" value="#{r.node.nodeRef}" />
          </a:actionLink>
      </a:column>

      <%-- Document type --%>
      <a:column id="col3" primary="true" styleClass="#{r.cssStyleClass}" >
         <f:facet name="header">
            <a:sortLink id="col3-sort" label="#{msg.document_type}" value="documentTypeName" styleClass="header" />
         </f:facet>
         <a:actionLink id="col3-text" value="#{r.documentTypeName}" action="#{r.getAction}" tooltip="#{r.documentTypeName}"
          actionListener="#{r.open}" styleClass="no-underline" >
            <f:param name="nodeRef" value="#{r.node.nodeRef}" />
          </a:actionLink>
      </a:column>

      <%-- regNumber --%>
      <a:column id="col4" primary="true" styleClass="#{r.cssStyleClass}" >
          <f:facet name="header">
              <a:sortLink id="col4-sort" label="#{msg.document_regNumber}" value="regNumber" styleClass="header" />
          </f:facet>
          <a:actionLink id="col4-text1" value="#{r.akString}" style="font-weight: bold;" action="#{r.getAction}" tooltip="#{r.regNumber}"
                      actionListener="#{r.open}" >
              <f:param name="nodeRef" value="#{r.node.nodeRef}" />
          </a:actionLink>
          <a:actionLink id="col4-text2" value="#{r.regNumber}" action="#{r.getAction}" tooltip="#{r.regNumber}"
                      actionListener="#{r.open}" >
              <f:param name="nodeRef" value="#{r.node.nodeRef}" />
          </a:actionLink>
      </a:column>

      <%-- Sender/owner --%>
      <a:column id="col5" primary="true" styleClass="#{r.cssStyleClass}" >
          <f:facet name="header">
              <a:sortLink id="col5-sort" label="#{msg.document_sender_recipient}" value="sender" styleClass="header" />
          </f:facet>
          <a:actionLink id="col5-text" value="#{r.senderOrRecipient}" action="#{r.getAction}" tooltip="#{r.senderOrRecipient}"
                        actionListener="#{r.open}" styleClass="tooltip condence20- no-underline" >
              <f:param name="nodeRef" value="#{r.node.nodeRef}" />
          </a:actionLink>
      </a:column>

      <%-- Registration date --%>
      <a:column id="col6" primary="true" styleClass="#{r.cssStyleClass}" >
          <f:facet name="header">
              <a:sortLink id="col6-sort" label="#{msg.document_regDateTime}" value="regDateTime" styleClass="header" />
          </f:facet>
          <a:actionLink id="col6-text" value="#{r.regDateTimeStr}" action="#{r.getAction}" tooltip="#{r.regDateTimeStr}" styleClass="no-underline"
                        actionListener="#{r.open}" >
              <f:param name="nodeRef" value="#{r.node.nodeRef}" />
          </a:actionLink>
      </a:column>

      <%-- Files --%>
      <a:column id="col7" primary="true" styleClass="doc-list-actions">
         <f:facet name="header">
            <h:outputText id="col7-header" value="#{msg.document_allFiles}" styleClass="header" />
         </f:facet>
          <f:facet name="csvExport">
              <a:param value="false"/>
          </f:facet>
          <wm:customChildrenContainer id="document-list-files" childGenerator="#{DocumentListDialog.documentRowFileGenerator}" parameterList="#{r}"/>
      </a:column>
