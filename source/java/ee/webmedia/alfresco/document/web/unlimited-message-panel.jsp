<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="unlimited-message" styleClass="message" rendered="#{!DialogManager.bean.limited}">
   <h:outputText id="unlimited-message-text" value="#{DialogManager.bean.unlimitedMessage}" />
</a:panel>