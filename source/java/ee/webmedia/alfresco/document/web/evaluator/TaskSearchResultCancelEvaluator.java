package ee.webmedia.alfresco.document.web.evaluator;

import ee.webmedia.alfresco.workflow.search.web.TaskSearchResultsDialog;
import org.alfresco.web.action.evaluator.BaseActionEvaluator;
import org.alfresco.web.app.servlet.FacesHelper;

import javax.faces.context.FacesContext;

public class TaskSearchResultCancelEvaluator extends BaseActionEvaluator {
    private static final long serialVersionUID = 0L;

    @Override
    public boolean evaluate(Object obj) {
        final FacesContext ctx = FacesContext.getCurrentInstance();
        TaskSearchResultsDialog taskSearchResultsDialog = (TaskSearchResultsDialog) FacesHelper.getManagedBean(ctx, TaskSearchResultsDialog.BEAN_NAME);
        return taskSearchResultsDialog.isShowCheckboxes();
    }
}
