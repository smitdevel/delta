package ee.webmedia.alfresco.document.web.evaluator;

import javax.faces.context.FacesContext;

import org.alfresco.web.action.evaluator.BaseActionEvaluator;
import org.alfresco.web.app.servlet.FacesHelper;

import ee.webmedia.alfresco.document.search.web.DocumentSearchResultsDialog;
import ee.webmedia.alfresco.workflow.search.web.TaskSearchResultsDialog;

public class DocumentSearchResultFinishEvaluator extends BaseActionEvaluator {

    @Override
    public boolean evaluate(Object obj) {
        final FacesContext ctx = FacesContext.getCurrentInstance();
        DocumentSearchResultsDialog documentSearchResultsDialog = (DocumentSearchResultsDialog) FacesHelper.getManagedBean(ctx, DocumentSearchResultsDialog.BEAN_NAME);
        return documentSearchResultsDialog.isShowCheckboxes();
    }
}
