package ee.webmedia.alfresco.document.sendout.service;

import com.nortal.jroad.client.dhl.DhlXTeeService.ContentToSend;
import com.nortal.jroad.client.dhl.DhlXTeeService.SendStatus;
import ee.sk.digidoc.SignedDoc;
import ee.smit.digisign.crypt.model.SignCertificate;
import ee.webmedia.alfresco.addressbook.model.AddressbookModel;
import ee.webmedia.alfresco.addressbook.service.AddressbookService;
import ee.webmedia.alfresco.classificator.enums.AccessRestriction;
import ee.webmedia.alfresco.classificator.enums.SendMode;
import ee.webmedia.alfresco.common.search.DbSearchUtil;
import ee.webmedia.alfresco.common.service.CreateObjectCallback;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.docadmin.web.DocTypeDetailsDialog;
import ee.webmedia.alfresco.docconfig.bootstrap.SystematicDocumentType;
import ee.webmedia.alfresco.docdynamic.model.DocumentChildModel;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamicService;
import ee.webmedia.alfresco.document.model.Document;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.document.model.DocumentSpecificModel;
import ee.webmedia.alfresco.document.sendout.model.DocumentSendInfo;
import ee.webmedia.alfresco.document.sendout.model.SendInfo;
import ee.webmedia.alfresco.dvk.model.DvkSendDocuments;
import ee.webmedia.alfresco.dvk.service.DvkService;
import ee.webmedia.alfresco.email.model.EmailAttachment;
import ee.webmedia.alfresco.email.service.EmailService;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.parameters.service.ParametersService;
import ee.webmedia.alfresco.privilege.model.Privilege;
import ee.webmedia.alfresco.privilege.service.PrivilegeService;
import ee.webmedia.alfresco.signature.model.SkLdapCertificate;
import ee.webmedia.alfresco.signature.service.SignatureService;
import ee.webmedia.alfresco.signature.service.SkLdapService;
import ee.webmedia.alfresco.template.model.ProcessedEmailTemplate;
import ee.webmedia.alfresco.user.model.Authority;
import ee.webmedia.alfresco.user.service.UserService;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.utils.UnableToPerformException;
import ee.webmedia.alfresco.utils.WebUtil;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;
import ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.service.Task;
import ee.webmedia.alfresco.workflow.service.Workflow;
import org.alfresco.i18n.I18NUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentLogService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentTemplateService;
import static ee.webmedia.alfresco.utils.UserUtil.getUserFullNameAndId;

public class SendOutServiceImpl implements SendOutService {

    public static final String SYSTEM = "system";
    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(SendOutServiceImpl.class);

    private static final String SAP_ORG_NAME = "SAP";
    
    private static String DIGIDOC_MIMETYPE = "application/digidoc";
    private static String ASICE_DIGIDOC_MIMETYPE = "application/vnd.etsi.asic-e+zip";

    private NodeService nodeService;
    private EmailService emailService;
    private AddressbookService addressbookService;
    private DvkService _dvkService;
    private ParametersService parametersService;
    private SignatureService _signatureService;
    private SkLdapService skLdapService;
    private UserService _userService;

    @Override
    public List<SendInfo> getDocumentSendInfos(NodeRef document) {
        Map<NodeRef, List<SendInfo>> sendInfos = BeanHelper.getBulkLoadNodeService().loadChildNodes(Collections.singletonList(document), null, DocumentCommonModel.Types.SEND_INFO,
                null, new CreateObjectCallback<SendInfo>() {

            @Override
            public SendInfo create(NodeRef nodeRef, Map<QName, Serializable> properties) {
                return new DocumentSendInfo(properties);
            }
        });
        return sendInfos.isEmpty() ? new ArrayList<SendInfo>() : sendInfos.get(document);
    }

    @Override
    public Date getEarliestSendInfoDate(NodeRef docRef) {
        Map<NodeRef, List<Date>> result = BeanHelper.getBulkLoadNodeService().loadChildNodes(Arrays.asList(docRef),
                Collections.singleton(DocumentCommonModel.Props.SEND_INFO_SEND_DATE_TIME),
                DocumentCommonModel.Types.SEND_INFO, null, new CreateObjectCallback<Date>() {

                    @Override
                    public Date create(NodeRef nodeRef, Map<QName, Serializable> properties) {
                        return (Date) properties.get(DocumentCommonModel.Props.SEND_INFO_SEND_DATE_TIME);
                    }
                });
        List<Date> sendInfoDates = result.get(docRef);
        if (CollectionUtils.isNotEmpty(sendInfoDates)) {
            return Collections.min(sendInfoDates);
        }
        return null;
    }

    @Override
    public void sendOutAutomatically(NodeRef docNodeRef) {
        long startTime = new Date().getTime();
        log.info("sendOutAutomatically START " + getTimeDiff(startTime));

        Document document = new Document(docNodeRef);
        String fromEmail = BeanHelper.getParametersService().getStringParameter(Parameters.DOC_SENDER_EMAIL);
        boolean isDocTypeAutoSendAllowed = BooleanUtils.isTrue(BeanHelper.getDocumentAdminService().getDocumentTypeProperty(document.getObjectTypeId(), DocumentAdminModel.Props.DOC_TYPE_AUTO_SEND_ALLOWED, Boolean.class));
        boolean isSendUnregistratedDocEnabled = BooleanUtils.isTrue(BeanHelper.getDocumentAdminService().getDocumentTypeProperty(document.getObjectTypeId(), DocumentAdminModel.Props.SEND_UNREGISTRATED_DOC_ENABLED, Boolean.class));
        NodeRef seriesRef = (NodeRef) document.getProperties().get(DocumentCommonModel.Props.SERIES);
        NodeRef functionRef = (NodeRef) document.getProperties().get(DocumentCommonModel.Props.FUNCTION);

        if (seriesRef != null && functionRef != null
                && StringUtils.isNotBlank(fromEmail)
                && document.isAutoSendOutAllowed()
                && BeanHelper.getApplicationConstantsBean().isDocumentAutoSendingEnabled()
                && StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.equals(docNodeRef.getStoreRef())
                && !document.getFunction().isDocumentActivitiesAreLimited()
                && !document.getSeries().isSeriesAutoSendForbidden()
                && document.isFinished()
                && !document.isImported()
                && !document.isReopened()
                && document.isDigitalStorageType()
                && !document.isEncrypted()
                && document.isAutoSendDelivery()
                && document.isEmailDvkTransmittalMode()
                && !document.hasSendOutInfo()
                && hasFinishedSignatureTasks(document)
                && document.hasSignedFiles()
                && !document.isLocked()) {
            boolean isAutoSendAllowed = document.isAutoSendOutAllowed()
                    && isDocTypeAutoSendAllowed
                    && (isSendUnregistratedDocEnabled
                    || document.getRegNumber() != null && document.getRegDateTime() != null);
            log.debug("AUTO SEND OUT CHECK: " + isAutoSendAllowed);
            List<Pair<String, String>> recipients = getRecipients(document);
            log.debug("AUTO SEND RECIPIENTS: " + recipients.size());
            String content = getTemplateContent(document);
            log.debug("AUTO SEND CONTENT LENGTH: " + (StringUtils.isNotBlank(content) ? content.length() : 0));
            if (isAutoSendAllowed && CollectionUtils.isNotEmpty(recipients) && StringUtils.isNotEmpty(content)) {
                BeanHelper.getDocumentLockHelperBean().lockOrUnlockIfNeeded(BeanHelper.getDocumentLockHelperBean().isLockingAllowed());
                List<NodeRef> fileRefs = BeanHelper.getFileService().getAllFileRefs(docNodeRef, true);
                try {
                    autoSendOut(docNodeRef, recipients, fileRefs, content, fromEmail);
                    addDocumentNodeProperty(docNodeRef, DocumentCommonModel.Props.SENT_OUT_AUTOMATICALLY, Boolean.TRUE);
                    BeanHelper.getDocumentLockHelperBean().lockOrUnlockIfNeeded(false);
                    log.info("sendOutAutomatically END1 " + getTimeDiff(startTime));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    addDocumentNodeProperty(docNodeRef, DocumentCommonModel.Props.SENT_OUT_AUTOMATICALLY, Boolean.TRUE);
                    addDocumentNodeProperty(docNodeRef, DocumentCommonModel.Props.AUTO_SEND_OUT_FAILED, Boolean.TRUE);
                }
                BeanHelper.getDocumentLockHelperBean().lockOrUnlockIfNeeded(false);
            }
        }
        if (seriesRef != null && functionRef != null
                && StringUtils.isNotBlank(fromEmail)
                && document.isAutoSendOutAllowed()
                && BeanHelper.getApplicationConstantsBean().isDocumentAutoSendingEnabled()
                && StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.equals(docNodeRef.getStoreRef())
                && !document.getFunction().isDocumentActivitiesAreLimited()
                && !document.getSeries().isSeriesAutoSendForbidden()
                && !document.isImported()
                && !document.isReopened()
                && document.isDigitalStorageType()
                && !document.isEncrypted()
                && document.isAutoSendDelivery()
                && document.isEmailDvkTransmittalMode()
                && !document.hasSendOutInfo()
                && isDocTypeAutoSendAllowed
                && StringUtils.isNotBlank(getTemplateContent(document))) {
            String documentAutoSend = parametersService.getStringParameter(Parameters.DOCUMENT_AUTO_SEND);
            MessageUtil.addInfoText(documentAutoSend);
        }
        log.info("sendOutAutomatically END2" + getTimeDiff(startTime));
    }

    private long getTimeDiff(long startTime){
        return new Date().getTime() - startTime;
    }

    private boolean hasFinishedSignatureTasks(ee.webmedia.alfresco.document.model.Document document) {
        List<Node> compoundWorkflows = document.getAllChildAssociations(WorkflowCommonModel.Types.COMPOUND_WORKFLOW);
        boolean hasSignatureTasks = false;
        boolean allTasksFinishedAndSigned = true;
        if (CollectionUtils.isNotEmpty(compoundWorkflows)) {
            for (Node item : compoundWorkflows) {
                CompoundWorkflow compoundWorkflow = BeanHelper.getWorkflowService().getCompoundWorkflow(item.getNodeRef());
                for (Workflow workflow : compoundWorkflow.getWorkflows()) {
                    for (Task task : workflow.getTasks()) {
                        QName taskType = task.getNode().getType();
                        if (WorkflowSpecificModel.Types.SIGNATURE_TASK.equals(taskType)) {
                            hasSignatureTasks = true;
                            if (!task.isStatus(Status.FINISHED)
                                    || !I18NUtil.getMessage("task_outcome_signatureTask1").equals(task.getOutcome())) {
                                allTasksFinishedAndSigned = false;
                                log.info("sendOutAutomatically signed task outcome: " + task.getOutcome());
                                log.info("sendOutAutomatically signed task status: " + task.getStatus());
                            }
                        }
                    }
                }
            }
        }
        List<NodeRef> independentWorkflows = getIndependentWorkflows(document.getNodeRef());
        for (NodeRef item : independentWorkflows) {
            List<NodeRef> workflows = getWorkflowsFromIndependentWorkflow(item);
            for (NodeRef workflow : workflows) {
                List<Task> tasks = BeanHelper.getWorkflowService().getTasks(workflow);
                for (Task task : tasks) {
                    QName taskType = task.getNode().getType();
                    if (WorkflowSpecificModel.Types.SIGNATURE_TASK.equals(taskType)) {
                        hasSignatureTasks = true;
                        if (!task.isStatus(Status.FINISHED)
                                || !I18NUtil.getMessage("task_outcome_signatureTask1").equals(task.getOutcome())) {
                            allTasksFinishedAndSigned = false;
                            log.info("sendOutAutomatically signed task outcome: " + task.getOutcome());
                            log.info("sendOutAutomatically signed task status: " + task.getStatus());
                        }
                    }
                }
            }
        }
        log.info("sendOutAutomatically hasSignatureTasks: " + hasSignatureTasks);
        log.info("sendOutAutomatically allTasksFinishedAndSigned: " + allTasksFinishedAndSigned);
        return hasSignatureTasks && allTasksFinishedAndSigned;
    }

    private List<NodeRef> getIndependentWorkflows(NodeRef documentNodeRef) {
        List<NodeRef> independentWorkflows = BeanHelper.getDocumentAssociationsService().getDocumentIndependentWorkflowAssocs(documentNodeRef);
        return CollectionUtils.isNotEmpty(independentWorkflows) ? independentWorkflows : new ArrayList<NodeRef>();
    }

    private List<NodeRef> getWorkflowsFromIndependentWorkflow(NodeRef indipendentWorkflow) {
        List<NodeRef> result = new ArrayList<NodeRef>();
        List<ChildAssociationRef> workflows = nodeService.getChildAssocs(indipendentWorkflow);
        for (ChildAssociationRef workflow : workflows) {
            if (WorkflowCommonModel.Types.WORKFLOW.equals(workflow.getTypeQName())) {
                result.add(workflow.getChildRef());
            }
        }
        return result;
    }

    public List<Pair<String, String>> getRecipients(Document document) {
        List<Pair<String, String>> recipients = new ArrayList<Pair<String, String>>();
        Map<String, Object> props = document.getProperties();
        boolean isContract = SystematicDocumentType.CONTRACT.getId().equals(props.get(DocumentAdminModel.Props.OBJECT_TYPE_ID));
        if (isContract) {
            addContractContact(props, recipients);
        } else {
            addRecipients(document, recipients);
            addContractParty(document, recipients);
        }
        return recipients;
    }

    private void addRecipients(Document document, List<Pair<String, String>> recipients) {
        addRecipientsByProp(document, DocumentCommonModel.Props.RECIPIENT_NAME, DocumentCommonModel.Props.RECIPIENT_EMAIL,
                DocumentDynamicModel.Props.RECIPIENT_PERSON_NAME, DocumentDynamicModel.Props.RECIPIENT_POSTAL_CITY,
                DocumentDynamicModel.Props.RECIPIENT_STREET_HOUSE, recipients);
        addRecipientsByProp(document, DocumentCommonModel.Props.ADDITIONAL_RECIPIENT_NAME,
                DocumentCommonModel.Props.ADDITIONAL_RECIPIENT_EMAIL, DocumentDynamicModel.Props.ADDITIONAL_RECIPIENT_PERSON_NAME,
                DocumentDynamicModel.Props.ADDITIONAL_RECIPIENT_POSTAL_CITY, DocumentDynamicModel.Props.ADDITIONAL_RECIPIENT_STREET_HOUSE, recipients);
    }

    private void addRecipientsByProp(Document document, QName nameProp, QName emailProp, QName personProp,
                                     QName postalProp, QName streetProp, List<Pair<String, String>> recipients) {
        List<String> recipientNames = getListByProperty(document, nameProp);
        List<String> recipientEmails = getListByProperty(document, emailProp);
        List<String> persons = getListByProperty(document, personProp);
        List<String> postals = getListByProperty(document, postalProp);
        List<String> streets = getListByProperty(document, streetProp);
        for (int i = 0; i < recipientNames.size(); i++) {
            String name = recipientNames.get(i);
            String email = recipientEmails.size() > i ? recipientEmails.get(i) : null;
            String person = persons.size() > i ? persons.get(i) : null;
            String postal = postals.size() > i ? postals.get(i) : null;
            String street = streets.size() > i ? streets.get(i) : null;
            if (StringUtils.isNotBlank(name) || StringUtils.isNotBlank(email)
                    || StringUtils.isNotBlank(person) || StringUtils.isNotBlank(postal) || StringUtils.isNotBlank(street)) {
                recipients.add(new Pair<String, String>(name, email));
            }
        }
    }

    private List<String> getListByProperty(ee.webmedia.alfresco.document.model.Document document, QName qName) {
        @SuppressWarnings("unchecked")
        List<String> result = (List<String>) document.getProperties().get(qName);
        return result != null ? result : new ArrayList<String>();
    }

    private void addContractParty(Document document, List<Pair<String, String>> recipients) {
        List<Node> childNodes = document.getAllChildAssociations(DocumentChildModel.Assocs.CONTRACT_PARTY);
        if (CollectionUtils.isNotEmpty(childNodes)) {
            String name = (String) childNodes.get(0).getProperties().get(DocumentSpecificModel.Props.PARTY_NAME);
            String email = (String) childNodes.get(0).getProperties().get(DocumentSpecificModel.Props.PARTY_EMAIL);
            String signer = (String) childNodes.get(0).getProperties().get(DocumentSpecificModel.Props.PARTY_SIGNER);
            String contact = (String) childNodes.get(0).getProperties().get(DocumentSpecificModel.Props.PARTY_CONTACT_PERSON);
            if (StringUtils.isNotBlank(name) || StringUtils.isNotBlank(email)
                    || StringUtils.isNotBlank(signer) || StringUtils.isNotBlank(contact)) {
                recipients.add(new Pair<String, String>(name, email));
            }
        }
    }

    private void addContractContact(Map<String, Object> props, List<Pair<String, String>> recipients) {
        Object partys = props.get(DocumentSpecificModel.Props.PARTY_NAME);
        if (partys instanceof String) {
            String partyName = (String) partys;
            String email = (String) props.get(DocumentSpecificModel.Props.PARTY_EMAIL);
            String signer = (String) props.get(DocumentSpecificModel.Props.PARTY_SIGNER);
            String contact = (String) props.get(DocumentSpecificModel.Props.PARTY_CONTACT_PERSON);
            if (StringUtils.isNotBlank(partyName) || StringUtils.isNotBlank(email)
                    || StringUtils.isNotBlank(signer) || StringUtils.isNotBlank(contact)) {
                recipients.add(new Pair<String, String>(partyName, email));
            }
        } else if (partys instanceof List) {
            @SuppressWarnings("unchecked")
            List<String> partyList = (List<String>) partys;
            @SuppressWarnings("unchecked")
            List<String> emailList = (List<String>) props.get(DocumentSpecificModel.Props.PARTY_EMAIL);
            @SuppressWarnings("unchecked")
            List<String> signerList = (List<String>) props.get(DocumentSpecificModel.Props.PARTY_SIGNER);
            @SuppressWarnings("unchecked")
            List<String> contactList = (List<String>) props.get(DocumentSpecificModel.Props.PARTY_CONTACT_PERSON);
            if (CollectionUtils.isNotEmpty(partyList) && CollectionUtils.isNotEmpty(emailList)) {
                String name = partyList.get(0);
                String email = emailList.get(0);
                String signer = CollectionUtils.isNotEmpty(signerList) ? signerList.get(0) : null;
                String contact = CollectionUtils.isNotEmpty(contactList) ? contactList.get(0) : null;
                if (StringUtils.isNotBlank(name) || StringUtils.isNotBlank(email)
                        || StringUtils.isNotBlank(signer) || StringUtils.isNotBlank(contact)) {
                    recipients.add(new Pair<String, String>(name, email));
                }
            }
        }
    }

    private void autoSendOut(NodeRef document, List<Pair<String, String>> recipients, List<NodeRef> fileRefs, String content, String fromEmail) throws Exception {
        log.info("AUTO SEND E-MAIL OUT....");
        if (log.isDebugEnabled()) {
            log.debug("Names list size: " + recipients.size());
            for (Pair<String, String> recipient : recipients) {
                log.debug("NAME: " + recipient.getFirst());
            }

            log.debug("Emails list size: " + recipients.size());
            for (Pair<String, String> recipient : recipients) {
                log.debug("EMAIL: " + recipient.getSecond());
            }

            if (fileRefs != null) {
                log.debug("FILE REF size: " + fileRefs.size());
                for (NodeRef nodeRef : fileRefs) {
                    log.debug("FILE NODEREF: " + nodeRef);
                }
            } else {
                log.warn("FILE REF size: NULL!");
            }
        }
        Map<QName, Serializable> docProperties = nodeService.getProperties(document);
        List<Map<QName, Serializable>> emailSendInfoProps = new ArrayList<Map<QName, Serializable>>();
        List<Map<QName, Serializable>> dvkSendInfoProps = new ArrayList<Map<QName, Serializable>>();
        List<Map<QName, Serializable>> failedSendInfoProps = new ArrayList<Map<QName, Serializable>>();
        Date now = new Date();

        // Collect DVK data
        Map<String, Map<QName, Serializable>> toRegNums = new HashMap<String, Map<QName, Serializable>>();
        List<String> toDvkOrgNames = new ArrayList<String>();

        // Collect email data
        Map<String, Pair<String, Map<QName, Serializable>>> toEmails = new HashMap<String, Pair<String, Map<QName, Serializable>>>();
        List<String> toNames = new ArrayList<String>();

        String sentFiles = getSentFiles(fileRefs);
        // Loop through all recipients, keep a list for DVK sending, a list for email sending and prepare sendInfo properties
        for (Pair<String, String> recipient : recipients) {
            String name = StringUtils.defaultString(recipient.getFirst());
            String recipientName = StringUtils.defaultString(recipient.getFirst());
            final String email = recipient.getSecond();
            if (StringUtils.isNotBlank(email)) {
                recipientName += " (" + email + ")";
            }
            String recipientRegNr = getDvkOrgRegNumber(name);
            String accessRestriction = (String) docProperties.get(DocumentCommonModel.Props.ACCESS_RESTRICTION);
            boolean publicAccessRestriction = AccessRestriction.AK.getValueName().equalsIgnoreCase(accessRestriction)
                    || AccessRestriction.OPEN.getValueName().equalsIgnoreCase(accessRestriction);
            String sendMode = StringUtils.isNotBlank(recipientRegNr) ? SendMode.DVK.getValueName() : SendMode.EMAIL.getValueName();
            boolean hasEmail = StringUtils.isNotBlank(email);
            boolean hasName = StringUtils.isNotBlank(name);
            if (hasName && StringUtils.isNotBlank(recipientRegNr) && publicAccessRestriction) {
                if (!toRegNums.containsKey(recipientRegNr)) {
                    Map<QName, Serializable> props = getSendInfoProps(content, now, sentFiles, recipientName, recipientRegNr, sendMode);
                    toRegNums.put(recipientRegNr, props);
                    toDvkOrgNames.add(name);
                }
            } else if (!hasName || !hasEmail || StringUtils.isNotBlank(recipientRegNr) && !publicAccessRestriction) {
                Map<QName, Serializable> props = getSendInfoProps(content, now, sentFiles, recipientName, recipientRegNr, sendMode);
                failedSendInfoProps.add(props);
            } else if (!toEmails.containsKey(recipientName)) {
                Map<QName, Serializable> props = getSendInfoProps(content, now, sentFiles, recipientName, recipientRegNr, sendMode);
                toEmails.put(recipientName, new Pair<String, Map<QName, Serializable>>(email, props));
                toNames.add(name);
            }
        }

        boolean sendByDvk = toRegNums.size() > 0;
        List<EmailAttachment> attachments = emailService.getAttachments(fileRefs, false, null, null);

        // Send through DVK
        String dvkId = "";
        if (sendByDvk) {
            try {
                DvkSendDocuments sd = new DvkSendDocuments();
                sd.setSenderOrgName(parametersService.getStringParameter(Parameters.DVK_ORGANIZATION_NAME));
                sd.setSenderEmail(fromEmail);
                sd.setRecipientsRegNrs(new ArrayList<String>(toRegNums.keySet()));
                sd.setOrgNames(toDvkOrgNames);
                sd.setDocumentNodeRef(document);
                sd.setTextContent(StringEscapeUtils.unescapeHtml(WebUtil.removeDoctypeTags(
                        WebUtil.removeHtmlTags(WebUtil.removeAllBeforeBodyTags(content)))));
                List<ContentToSend> contentsToSend = prepareContents(attachments);
                dvkId = getDvkService().sendDocuments(contentsToSend, sd);
                dvkSendInfoProps.addAll(toRegNums.values());
            } catch (Exception e) {
                e.printStackTrace();
                failedSendInfoProps.addAll(toRegNums.values());
            }
        }

        // Send through email
        if (!toEmails.isEmpty()) {
            int index = 0;
            for (String nameEmail : toEmails.keySet()) {
                Pair<String, Map<QName, Serializable>> emailProps = toEmails.get(nameEmail);
                try {
                    emailService.sendEmail(Collections.singletonList(emailProps.getFirst()), Collections.singletonList(toNames.get(index)), Collections.<String>emptyList(), Collections.<String>emptyList(),
                            fromEmail, (String) docProperties.get(DocumentCommonModel.Props.DOC_NAME), content, true, document, attachments);
                    emailSendInfoProps.add(emailProps.getSecond());
                } catch (Exception e) {
                    e.printStackTrace();
                    failedSendInfoProps.add(emailProps.getSecond());
                }
                index++;
            }
        }

        // Create the sendInfo nodes under the document
        for (Map<QName, Serializable> props : dvkSendInfoProps) {
            props.put(DocumentCommonModel.Props.SEND_INFO_DVK_ID, dvkId);
            props.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendStatus.SENT.toString());
            addSendinfoTransactional(document, props);
            addAutoSendOutLog(document, MessageUtil.getMessage("document_log_status_auto_sent"));
        }
        for (Map<QName, Serializable> props : emailSendInfoProps) {
            props.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendStatus.RECEIVED.toString());
            addSendinfoTransactional(document, props);
            addAutoSendOutLog(document, MessageUtil.getMessage("document_log_status_auto_sent"));
        }
        for (Map<QName, Serializable> props : failedSendInfoProps) {
            props.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendInfo.SEND_FAILED);
            addSendinfoTransactional(document, props);
        }

        if (CollectionUtils.isNotEmpty(failedSendInfoProps)) {
            addDocumentNodeProperty(document, DocumentCommonModel.Props.AUTO_SEND_OUT_FAILED, Boolean.TRUE);
            addAutoSendOutLog(document, MessageUtil.getMessage("document_auto_send_failed", failedSendInfoProps.size()));
        }
    }

    public void addDocumentNodeProperty(final NodeRef document, final QName qName, final Serializable value) {
        RetryingTransactionHelper helper = BeanHelper.getTransactionService().getRetryingTransactionHelper();
        helper.doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<Void>() {
            @Override
            public Void execute() throws Throwable {
                BeanHelper.getNodeService().setProperty(document, qName, value);
                return null;
            }
        }, false, true);
    }

    public void addAutoSendOutLog(final NodeRef document, final String message) {
        RetryingTransactionHelper helper = BeanHelper.getTransactionService().getRetryingTransactionHelper();
        helper.doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<Void>() {
            @Override
            public Void execute() throws Throwable {
                getDocumentLogService().addDocumentLog(document, message, "DHS");
                return null;
            }
        }, false, true);
    }
    
    public List<String> convertStringToList(Object obj){
        if(obj instanceof String){
            List<String> list = new ArrayList<>();
            String str = (String) obj;
            list.add(str);
            return list;
        }

        return (List<String>) obj;
    }
    
    public List<String> castListIfString(Object obj) {
    	if (obj == null) {
    		return null;
    	} else if (obj instanceof String) {
    		return convertStringToList(obj);
    	} else {
    		return (List<String>) obj;
    	}
    }

    private Map<QName, Serializable> getSendInfoProps(String content, Date now, String sentFiles, String recipientName, String recipientRegNr, String sendMode) {
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        props.put(DocumentCommonModel.Props.SEND_INFO_RECIPIENT, recipientName);
        props.put(DocumentCommonModel.Props.SEND_INFO_RECIPIENT_REG_NR, recipientRegNr);
        props.put(DocumentCommonModel.Props.SEND_INFO_SEND_DATE_TIME, now);
        props.put(DocumentCommonModel.Props.SEND_INFO_SEND_MODE, sendMode);
        props.put(DocumentCommonModel.Props.SEND_INFO_DVK_ID, StringUtils.EMPTY);
        props.put(DocumentCommonModel.Props.SEND_INFO_RESOLUTION, content);
        if (sentFiles.length() > 0) {
            props.put(DocumentCommonModel.Props.SEND_INFO_SENT_FILES, sentFiles);
        }
        props.put(DocumentCommonModel.Props.SEND_INFO_IS_ENCRYPTED, Boolean.FALSE);
        props.put(DocumentCommonModel.Props.SEND_INFO_SENDER, SYSTEM);
        return props;
    }

    private String getSentFiles(List<NodeRef> fileRefs) {
        StringBuilder sentFiles = new StringBuilder();
        if (fileRefs != null && fileRefs.size() > 0) {
            for (NodeRef fileRef : fileRefs) {
                String fileName = (String) nodeService.getProperty(fileRef, ContentModel.PROP_NAME);
                if (sentFiles.length() > 0) {
                    sentFiles.append("; ");
                }
                sentFiles.append(fileName);
            }
        }
        return sentFiles.toString();
    }

    private String getTemplateContent(Document document) {
        String content = null;
        String nonPublicTemplateRef = BeanHelper.getDocumentAdminService().getDocumentTypeProperty(document.getObjectTypeId(), DocumentAdminModel.Props.AUTO_SEND_TEMPLATE_NAME_NONPUBLIC, String.class);
        String templateRef = document.isPublicAccessRestriction()
                || StringUtils.isBlank(nonPublicTemplateRef)
                || DocTypeDetailsDialog.PARAM_USE_PUBLIC_TEMPLATE.equals(nonPublicTemplateRef)
                ? BeanHelper.getDocumentAdminService().getDocumentTypeProperty(document.getObjectTypeId(), DocumentAdminModel.Props.AUTO_SEND_TEMPLATE_NAME_PUBLIC, String.class) : nonPublicTemplateRef;
        if (StringUtils.isNotBlank(templateRef)) {
            LinkedHashMap<String, NodeRef> nodeRefs = new LinkedHashMap<>();
            nodeRefs.put(null, document.getNodeRef());
            ProcessedEmailTemplate template = getDocumentTemplateService().getProcessedEmailTemplate(nodeRefs, new NodeRef(templateRef));
            content = template.getContent();
        }
        return content;
    }

    private String getDvkOrgRegNumber(String name) {
        for (Node organization : addressbookService.getDvkCapableOrgs()) {
            String orgName = (String) organization.getProperties().get(AddressbookModel.Props.ORGANIZATION_NAME.toString());
            if (name.equalsIgnoreCase(orgName)) {
                return (String) organization.getProperties().get(AddressbookModel.Props.ORGANIZATION_CODE.toString());
            }
        }
        return StringUtils.EMPTY;
    }

    @Override
    public boolean hasDocumentSendInfos(NodeRef document) {
        List<ChildAssociationRef> assocs = nodeService.getChildAssocs(document, RegexQNamePattern.MATCH_ALL, DocumentCommonModel.Assocs.SEND_INFO);
        return !assocs.isEmpty();
    }

    @Override
    public List<Pair<String, String>> forward(NodeRef document, List<String> names, List<String> emails, List<String> modes, String fromEmail, String content,
            List<NodeRef> fileRefs) {
        try {
            return sendOut(document, names, emails, modes, null, null, null, fromEmail, null, content, fileRefs, false, true);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean sendOut(NodeRef document, List<String> names, List<String> emails, List<String> modes, List<String> idCodes, List<String> encryptionIdCodes, List<X509Certificate> allCertificates, 
    		String fromEmail, String subject, String content, List<NodeRef> fileRefs, boolean zipIt) {
        try {
            return sendOut(document, names, emails, modes, idCodes, encryptionIdCodes, allCertificates, fromEmail, subject, content, fileRefs, zipIt, false) != null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    private List<Pair<String, String>> sendOut(NodeRef document, List<String> names, List<String> emails, List<String> modes, List<String> idCodes, List<String> encryptionIdCodes,
    		List<X509Certificate> allCertificates, String fromEmail, String subject, String content, List<NodeRef> fileRefs, boolean zipIt, boolean forward) throws Exception {
        log.info("SEND E-MAIL OUT....");
        if(log.isDebugEnabled()){
            if(names != null){
                log.debug("Names list size: " + names.size());
                for(String name: names){
                    log.debug("NAME: " + name);
                }
            } else {
                log.warn("Names list size: NULL!");
            }

            if(emails != null){
                log.debug("Emails list size: " + emails.size());
                for(String email : emails){
                    log.debug("EMAIL: " + email);
                }
            } else {
                log.warn("Emails list size: NULL!");
            }

            if(modes != null){
                log.debug("Modes list size: " + modes.size());
                for(String mode : modes){
                    log.debug("MODE: " + mode);
                }
            } else {
                log.warn("Modes list size: NULL!");
            }

            if(idCodes != null){
                log.debug("idCodes list size: " + idCodes.size());
                for(String idCode : idCodes){
                    log.debug("ID CODE: " + idCode);
                }

            } else {
                log.warn("idCodes list size: NULL!");
            }

            if(encryptionIdCodes != null){
                log.debug("encryptionIdCodes list size: " + encryptionIdCodes.size());
                for(String encIdCode : encryptionIdCodes){
                    log.debug("ENCRYPTION ID CODE: " + encIdCode);
                }
            } else {
                log.warn("encryptionIdCodes list size: NULL!");
            }

            if(allCertificates != null){
                log.debug("ALL CERTIFICATES list size: " + allCertificates.size());
                for(X509Certificate cert : allCertificates){
                    log.debug("X509Certificate: serialNumber: " + cert.getSerialNumber() + ", SubjectDN: " + cert.getSubjectDN());
                }
            } else {
                log.debug("ALL CERTIFICATES list is NULL!");
            }

            if(subject != null || !subject.isEmpty()){
                log.debug("SUBJECT length: " + subject.length());
            } else {
                log.debug("SUBJECT is NULL!");
            }

            if(content != null || !content.isEmpty()){
                log.debug("CONTENT length: " + content.length());
            } else {
                log.debug("CONTENT is NULL!");
            }

            if(fileRefs != null){
                log.debug("FILE REF size: " + fileRefs.size());
                for(NodeRef nodeRef : fileRefs){
                    log.debug("FILE NODEREF: " + nodeRef);
                }
            } else {
                log.warn("FILE REF size: NULL!");
            }

            if(zipIt){
                log.debug("ZIPIT is TRUE!");
            } else {
                log.debug("ZIPIT is FALSE!");
            }

            if(forward){
                log.debug("FORWARD is TRUE!");
            } else {
                log.debug("FORWARD is FALSE!");
            }
        }

        if (allCertificates == null) {
        	allCertificates = new ArrayList<X509Certificate>();
    	}
        if (encryptionIdCodes != null) {
            Set<String> encryptionIdCodesSet = new HashSet<String>();
            for (int i = 0; i < names.size(); i++) {

                String encryptionIdCode = encryptionIdCodes.get(i) != null ? encryptionIdCodes.get(i).trim(): null;
                log.debug(i + ") ENCRYPTION ID CODE: " + encryptionIdCode);
                if (StringUtils.isBlank(encryptionIdCode) || encryptionIdCodesSet.contains(encryptionIdCode)) {
                    log.debug("Encryption id code is null or already containst set.");
                    continue;
                }

                List<X509Certificate> certificates = new ArrayList<>();

                if(BeanHelper.getDigisignCryptService().isActive()) {
                    log.info("Using DigiSign-service to get certificates... ID-CODE: " + encryptionIdCode);
                    List<SignCertificate> signCertificateList = BeanHelper.getDigisignCryptSearches().getCertificatesFromDigiSignService(encryptionIdCode, "");
                    if(signCertificateList == null || signCertificateList.isEmpty()){
                        log.debug("CERTIFICATE list size: NULL! continue to next");
                    }

                    log.debug("CERTIFICATE list size: " + signCertificateList);
                    for(SignCertificate cert : signCertificateList){
                        try{
                            X509Certificate certX509 = SignedDoc.readCertificate(cert.getData());
                            log.debug("Add certificate to certificates list...");
                            certificates.add(certX509);
                        }catch (Exception e){
                            log.error(e.getMessage(), e);
                        }
                    }

                } else {
                    log.info("Using SK LDAP to get certificates...");
                    List<SkLdapCertificate> skLdapCertificates = skLdapService.getCertificates(encryptionIdCode);

                    if(skLdapCertificates != null){
                        log.debug("SK LDAP certificates list size: " + skLdapCertificates.size());
                        certificates = getSignatureService().getCertificatesForEncryption(skLdapCertificates);
                    } else {
                        log.warn("SK LDAP certificates list size: NULL!");
                    }
                }

                if (certificates.isEmpty()) {
                    throw new UnableToPerformException("document_send_out_encryptionRecipient_notFound", names.get(i), encryptionIdCode);
                }
                log.info("Add certificates to ALL CERTIFICATES LIST: " + certificates.size());
                allCertificates.addAll(certificates);
                encryptionIdCodesSet.add(encryptionIdCode);

            }
        }

        Map<QName, Serializable> docProperties = nodeService.getProperties(document);
        List<Map<QName, Serializable>> emailInfoProps = new ArrayList<Map<QName, Serializable>>();
        List<Map<QName, Serializable>> dvkInfoProps = new ArrayList<Map<QName, Serializable>>();
        List<Map<QName, Serializable>> otherInfoProps = new ArrayList<Map<QName, Serializable>>();
        Date now = new Date();

        // Collect DVK data
        List<String> toRegNums = new ArrayList<String>();

        // Collect email data
        List<String> toEmails = new ArrayList<String>();
        List<String> toNames = new ArrayList<String>();
        List<String> toBccEmails = new ArrayList<String>();
        List<String> toBccNames = new ArrayList<String>();
        List<String> toDvkOrgNames = new ArrayList<String>();
        List<String> toDvkPersonNames = new ArrayList<String>();
        List<String> toDvkIdCodes = new ArrayList<String>();

        List<Pair<String, String>> dvkRecipients = new ArrayList<>();
        final String executingUser = AuthenticationUtil.getFullyAuthenticatedUser();
        String sender = getUserFullNameAndId(getUserService().getUserProperties(executingUser));

        // Loop through all recipients, keep a list for DVK sending, a list for email sending and prepare sendInfo properties
        for (int i = 0; i < names.size(); i++) {
            if (StringUtils.isNotBlank(names.get(i)) && StringUtils.isNotBlank(modes.get(i))) {
                String recipientName = names.get(i);
                String recipient = recipientName;
                final String email = emails != null ? emails.get(i) : null;
                if (StringUtils.isNotBlank(email)) {
                    recipient += " (" + email + ")";
                }
                String recipientRegNr = "";
                String sendMode = modes.get(i);
                SendStatus sendStatus = SendStatus.RECEIVED;

                if (SendMode.EMAIL_DVK.equals(modes.get(i)) || SendMode.DVK.equals(modes.get(i))) {
                    // Check if matches a DVK capable organization entry in addressbook
                    boolean hasDvkContact = false;
                    for (Node organization : addressbookService.getDvkCapableOrgs()) {
                        String orgName = (String) organization.getProperties().get(AddressbookModel.Props.ORGANIZATION_NAME.toString());
                        String orgEmail = (String) organization.getProperties().get(AddressbookModel.Props.EMAIL.toString());
                        if (recipientName.equalsIgnoreCase(orgName) && email.equalsIgnoreCase(orgEmail)) {
                            hasDvkContact = true;
                            recipientRegNr = (String) organization.getProperties().get(AddressbookModel.Props.ORGANIZATION_CODE.toString());
                            toDvkOrgNames.add(recipientName);
                            break;
                        }
                    }

                    if (hasDvkContact) {
                        toRegNums.add(recipientRegNr);
                        sendMode = SendMode.DVK.getValueName();
                        sendStatus = SendStatus.SENT;
                        dvkRecipients.add(new Pair<>(recipientName, recipientRegNr));
                    } else if (!SendMode.DVK.equals(modes.get(i))) {
                        toEmails.add(email);
                        toNames.add(recipientName);
                        sendMode = SendMode.EMAIL.getValueName();
                    }
                } else if (SendMode.EMAIL.equals(modes.get(i))) {
                    toEmails.add(email);
                    toNames.add(recipientName);
                } else if (SendMode.EMAIL_BCC.equals(modes.get(i))) {
                    toBccEmails.add(email);
                    toBccNames.add(recipientName);
                } else if (SendMode.STATE_PORTAL_EESTI_EE.equals(modes.get(i))) {
                    toDvkPersonNames.add(recipientName);
                    recipientRegNr = idCodes.get(i);
                    toDvkIdCodes.add(recipientRegNr);
                    sendStatus = SendStatus.SENT;
                }

                Map<QName, Serializable> props = new HashMap<QName, Serializable>();
                props.put(DocumentCommonModel.Props.SEND_INFO_RECIPIENT, recipient);
                props.put(DocumentCommonModel.Props.SEND_INFO_RECIPIENT_REG_NR, recipientRegNr);
                props.put(DocumentCommonModel.Props.SEND_INFO_SEND_DATE_TIME, now);
                props.put(DocumentCommonModel.Props.SEND_INFO_SEND_MODE, sendMode);
                props.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, sendStatus.toString());
                props.put(DocumentCommonModel.Props.SEND_INFO_DVK_ID, "");
                props.put(DocumentCommonModel.Props.SEND_INFO_RESOLUTION, content);
                if (StringUtils.isNotBlank(sender)) {
                    props.put(DocumentCommonModel.Props.SEND_INFO_SENDER, sender);
                }
                if (SendMode.DVK.getValueName().equalsIgnoreCase(sendMode) || SendMode.STATE_PORTAL_EESTI_EE.getValueName().equalsIgnoreCase(sendMode)) {
                    dvkInfoProps.add(props);
                } else if(SendMode.EMAIL.getValueName().equalsIgnoreCase(sendMode) || SendMode.EMAIL_BCC.getValueName().equalsIgnoreCase(sendMode)){
                    emailInfoProps.add(props);
                } else {
                    otherInfoProps.add(props);
                }
            }
        }

        // Prepare zip file name if needed
        String zipOrEncryptFileTitle = null;
        boolean isEncrypted = false;
        StringBuilder sentFiles = new StringBuilder();
        if (fileRefs != null && fileRefs.size() > 0) {
        	if ((zipIt || !allCertificates.isEmpty())) {
        		zipOrEncryptFileTitle = buildZipAndEncryptFileTitle(docProperties);
        	}
            isEncrypted = !allCertificates.isEmpty();
            for (NodeRef fileRef : fileRefs) {
                String fileName = (String) nodeService.getProperty(fileRef, ContentModel.PROP_NAME);
                if (sentFiles.length() > 0) {
                	sentFiles.append("; ");
                }
                sentFiles.append(fileName);
            }
        }

        boolean sendByDvk = toRegNums.size() > 0 || toDvkIdCodes.size() > 0;
        List<EmailAttachment> attachments = emailService.getAttachments(fileRefs, !sendByDvk && zipIt, allCertificates, zipOrEncryptFileTitle);

        // Send through DVK
        String dvkId = "";
        if (sendByDvk) {
            try {
                // Construct DvkSendDocument
                DvkSendDocuments sd = new DvkSendDocuments();
                sd.setSenderOrgName(parametersService.getStringParameter(Parameters.DVK_ORGANIZATION_NAME));
                sd.setSenderEmail(fromEmail);
                sd.setRecipientsRegNrs(toRegNums);
                sd.setOrgNames(toDvkOrgNames);
                sd.setPersonIdCodes(toDvkIdCodes);
                sd.setPersonNames(toDvkPersonNames);
                sd.setDocumentNodeRef(document);
                sd.setTextContent(StringEscapeUtils.unescapeHtml(WebUtil.removeHtmlTags(content)));

                // Construct content items
                List<ContentToSend> contentsToSend = prepareContents(attachments);

                // Send it out
                dvkId = forward ? getDvkService().forwardDecDocument(contentsToSend, sd) : getDvkService().sendDocuments(contentsToSend, sd);
                addSendInfo(dvkInfoProps, document, zipIt, isEncrypted, sentFiles, dvkId, false);
            } catch (Exception e) {
                for (Map<QName, Serializable> dvkProp : dvkInfoProps){
                    dvkProp.remove(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS);
                    dvkProp.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendInfo.SEND_FAILED);
                }
                addSendInfo(dvkInfoProps, document, zipIt, isEncrypted, sentFiles, dvkId, true);
                throw new RuntimeException("Document DVK sending failed", e);
            }
        }

        // Send through email
        if (!toEmails.isEmpty() || !toBccEmails.isEmpty()) {
            try {
                emailService.sendEmail(toEmails, toNames, toBccEmails, toBccNames, fromEmail, subject, content, true, document, attachments);
                addSendInfo(emailInfoProps, document, zipIt, isEncrypted, sentFiles, dvkId, false);
            } catch (Exception e) {
                for (Map<QName, Serializable> emailProp : emailInfoProps){
                    emailProp.remove(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS);
                    emailProp.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendInfo.SEND_FAILED);
                }
                addSendInfo(emailInfoProps, document, zipIt, isEncrypted, sentFiles, dvkId, true);
                throw new RuntimeException("Document e-mail sending failed", e);
            }
        }
        
        //Send other
        if (otherInfoProps.size() > 0) {
            addSendInfo(otherInfoProps, document, zipIt, isEncrypted, sentFiles, dvkId, false);
        }

        return dvkRecipients;
    }

    private void addSendInfo(List<Map<QName, Serializable>> dvkInfoProps, NodeRef document, boolean zipIt, boolean isEncrypted, StringBuilder sentFiles, String dvkId, boolean transactional) {
        // Create the sendInfo nodes under the document
        for (Map<QName, Serializable> props : dvkInfoProps) {
            String sendMode = (String) props.get(DocumentCommonModel.Props.SEND_INFO_SEND_MODE);
            if (SendMode.DVK.getValueName().equalsIgnoreCase(sendMode) || SendMode.STATE_PORTAL_EESTI_EE.getValueName().equalsIgnoreCase(sendMode)) {
                props.put(DocumentCommonModel.Props.SEND_INFO_DVK_ID, dvkId);
            }
            if (zipIt || isEncrypted) {
                props.put(DocumentCommonModel.Props.SEND_INFO_IS_ZIPPED, Boolean.TRUE);
            }

            if (isEncrypted) {
                props.put(DocumentCommonModel.Props.SEND_INFO_IS_ENCRYPTED, Boolean.TRUE);
            } else {
                props.put(DocumentCommonModel.Props.SEND_INFO_IS_ENCRYPTED, Boolean.FALSE);
            }

            if (sentFiles.length() > 0) {
                props.put(DocumentCommonModel.Props.SEND_INFO_SENT_FILES, sentFiles.toString());
            }

            if (transactional) {
                addSendinfoTransactional(document, props);
            } else {
                addSendinfo(document, props);
            }
        }
    }

    private String buildZipAndEncryptFileTitle(Map<QName, Serializable> docProperties) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        StringBuilder docName = new StringBuilder();
        String regNum = (String) docProperties.get(DocumentCommonModel.Props.REG_NUMBER);
        if (StringUtils.isNotBlank(regNum)) {
            docName.append(regNum);
        }
        Date regDateTime = (Date) docProperties.get(DocumentCommonModel.Props.REG_DATE_TIME);
        if (regDateTime != null) {
            if (docName.length() > 0) {
                docName.append(" ");
            }
            docName.append(format.format(regDateTime));
        }
        if (docName.length() == 0) {
            docName.append("dokument");
        }
        return docName.toString();
    }

    @Override
    public Long sendForInformation(List<String> authorityIds, Node docNode, String emailTemplate, String subject, String content) {
        List<Authority> authorities = new ArrayList<Authority>();
        PrivilegeService privilegeService = BeanHelper.getPrivilegeService();
        UserService userService = BeanHelper.getUserService();
        NodeRef docRef = docNode.getNodeRef();
        Set<Privilege> privilegesToAdd = new HashSet<Privilege>(Arrays.asList(Privilege.VIEW_DOCUMENT_META_DATA,
                Privilege.VIEW_DOCUMENT_FILES));
        for (String authorityId : authorityIds) {
            Authority authority = userService.getAuthorityOrNull(authorityId);
            if (authority == null) {
                continue;
            }
            authorities.add(authority);
            String authorityStr = authority.getAuthority();
            if (!privilegeService.hasPermissionOnAuthority(docRef, authorityStr, Privilege.VIEW_DOCUMENT_META_DATA, Privilege.VIEW_DOCUMENT_FILES)) {
                privilegeService.setPermissions(docRef, authorityStr, privilegesToAdd);
            }
        }
        return BeanHelper.getNotificationService().sendForInformationNotification(authorities, docNode, emailTemplate, subject, content);
    }

    @Override
    public NodeRef addSendinfo(NodeRef document, Map<QName, Serializable> props) {
        return addSendinfo(document, props, true);
    }

    private NodeRef addSendinfoTransactional(NodeRef document, Map<QName, Serializable> props) {
        RetryingTransactionHelper helper = BeanHelper.getTransactionService().getRetryingTransactionHelper();
        return helper.doInTransaction(getAddSendInfoCallback(document, props, true), false, true);
    }

    @Override
    public NodeRef addSendinfo(NodeRef document, Map<QName, Serializable> props, boolean updateSearchableSendInfo) {
        final NodeRef sendInfoRef = nodeService.createNode(document,
                DocumentCommonModel.Assocs.SEND_INFO, DocumentCommonModel.Assocs.SEND_INFO, DocumentCommonModel.Types.SEND_INFO, props).getChildRef();
        log.debug("created new sendInfo '" + sendInfoRef + "' for sent document '" + document + "'");
        if (updateSearchableSendInfo) {
            updateSearchableSendInfo(document);
        }
        return sendInfoRef;
    }

    private RetryingTransactionHelper.RetryingTransactionCallback<NodeRef> getAddSendInfoCallback(final NodeRef document,
                                                                                                  final Map<QName, Serializable> props,
                                                                                                  final boolean updateSearchableSendInfo) {
        return new RetryingTransactionHelper.RetryingTransactionCallback<NodeRef>() {
            @Override
            public NodeRef execute() throws Throwable {
                final NodeRef sendInfoRef = nodeService.createNode(document,
                        DocumentCommonModel.Assocs.SEND_INFO, DocumentCommonModel.Assocs.SEND_INFO, DocumentCommonModel.Types.SEND_INFO, props).getChildRef();
                log.debug("created new sendInfo '" + sendInfoRef + "' for sent document '" + document + "'");
                if (updateSearchableSendInfo) {
                    updateSearchableSendInfo(document);
                }
                return sendInfoRef;
            }
        };
    }

    @Override
    public void updateSearchableSendInfo(NodeRef document) {
        nodeService.addProperties(document, buildSearchableSendInfo(document));
    }

    @Override
    public Map<QName, Serializable> buildSearchableSendInfo(NodeRef document) {
        List<SendInfo> sendInfos = getDocumentSendInfos(document);
        return DbSearchUtil.buildSearchableSendInfos(sendInfos);
    }

    @Override
    public List<ContentToSend> prepareContents(NodeRef document, List<NodeRef> fileRefs, boolean zipIt) throws Exception {
        String zipAndEncryptFileName = null;
        if (fileRefs != null && fileRefs.size() > 0 && zipIt) {
            Map<QName, Serializable> docProperties = nodeService.getProperties(document);
            zipAndEncryptFileName = buildZipAndEncryptFileTitle(docProperties);
        }
        List<EmailAttachment> attachments = emailService.getAttachments(fileRefs, zipIt, null, zipAndEncryptFileName);
        return prepareContents(attachments);
    }

    @Override
    public List<ContentToSend> prepareContents(List<EmailAttachment> attachments) {
        List<ContentToSend> result = new ArrayList<ContentToSend>();
        for (EmailAttachment attachment : attachments) {
            ContentToSend content = new ContentToSend();
            content.setFileName(attachment.getFileName());
            content.setMimeType((DIGIDOC_MIMETYPE.equals(attachment.getMimeType()))?ASICE_DIGIDOC_MIMETYPE:attachment.getMimeType());
            content.setId(attachment.getFileNodeRef().getId());
            try {
                content.setInputStream(attachment.getInputStreamSource().getInputStream());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            result.add(content);
        }
        return result;
    }

    // START: getters / setters
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setAddressbookService(AddressbookService addressbookService) {
        this.addressbookService = addressbookService;
    }

    public void setParametersService(ParametersService parametersService) {
        this.parametersService = parametersService;
    }

    /**
     * Dependency cicle: dvkService -> documentService -> sendOutService -> documentDynamicService -> documentService
     */
    public DocumentDynamicService getDocumentDynamicService() {
        return BeanHelper.getDocumentDynamicService();
    }

    public void setSkLdapService(SkLdapService skLdapService) {
        this.skLdapService = skLdapService;
    }

    @Override
    public void addSapSendInfo(Node document, String dvkId) {
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        props.put(DocumentCommonModel.Props.SEND_INFO_RECIPIENT, SAP_ORG_NAME);
        props.put(DocumentCommonModel.Props.SEND_INFO_RECIPIENT_REG_NR, parametersService.getStringParameter(Parameters.SAP_DVK_CODE));
        props.put(DocumentCommonModel.Props.SEND_INFO_SEND_DATE_TIME, new Date());
        props.put(DocumentCommonModel.Props.SEND_INFO_SEND_MODE, SendMode.DVK.getValueName());
        props.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendStatus.SENT.toString());
        props.put(DocumentCommonModel.Props.SEND_INFO_DVK_ID, dvkId);
        addSendinfo(document.getNodeRef(), props);
    }

    private DvkService getDvkService() {
        if (_dvkService == null) {
            _dvkService = BeanHelper.getDvkService();
        }
        return _dvkService;
    }

    private SignatureService getSignatureService() {
        if (_signatureService == null) {
            _signatureService = BeanHelper.getSignatureService();
        }
        return _signatureService;
    }
    
    private UserService getUserService() {
        if (_userService == null) {
            _userService = BeanHelper.getUserService();
        }
        return _userService;
    }

    // END: getters / setters

}
