package ee.webmedia.alfresco.document.model;

public enum DocumentNumbering {
	NO_INDIVIDUALIZING_NUMBER,
	NEW_NUMBER_FOR_EVERY_DOCUMENT,
	INDIVIDUALIZING_NUMBER
}
