<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<a:panel id="adr-block-panel" label="#{msg.document_adr_block}" styleClass="panel-100" progressive="true" expanded="false">

    <r:propertySheetGrid value="adr" var="adr" columns='2'>
	   <h:outputText value="#{msg.document_adr_status}" style='font-weight: bold; padding: 10px; text-align: right'/>
	   <a:actionLink value="#{AdrBlockBean.status}" href="#{AdrBlockBean.href}" target="_blank" rendered="#{!AdrBlockBean.deleteDisabled}" />
	   <h:outputText value="#{AdrBlockBean.status}" rendered="#{AdrBlockBean.deleteDisabled}" />
	   <h:commandButton id="add-doc-to-adr" value="#{msg.document_adr_add}" style="margin-top:10px;" readonly="true" rendered="#{AdrBlockBean.addEnabled && AdrBlockBean.deleteDisabled}"
	   actionListener="#{AdrBlockBean.adrDocumentAdd}" />
	   <h:commandButton id="delete-doc-from-adr" value="#{msg.document_adr_delete}" style="margin-top:10px;" readonly="true" rendered="#{!AdrBlockBean.deleteDisabled}"
	   actionListener="#{AdrBlockBean.confirmAdrDocumentDelete}" />
	</r:propertySheetGrid>

</a:panel>
