package ee.webmedia.alfresco.document.adr.web;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.webmedia.alfresco.common.rest.AdrRestApiService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docconfig.generator.DialogDataProvider;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamic;
import ee.webmedia.alfresco.docdynamic.web.DocumentDynamicBlock;
import ee.webmedia.alfresco.document.adr.model.AdrDocumentModel;
import ee.webmedia.alfresco.utils.MessageUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;

import static ee.webmedia.alfresco.adr.service.AdrServiceImpl.isDocumentAllowedToAdr;

public class AdrBlockBean implements DocumentDynamicBlock {

    public static final String BEAN_NAME = "AdrBlockBean";
    private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(AdrBlockBean.class);

	private static final long serialVersionUID = 1L;
	private String nodeRefString;
	private String status;
	private String href;
	private boolean deleteDisabled;
	private boolean isChangedForAdr;
	private AdrRestApiService adrApiService = BeanHelper.getAdrRestApiService();

	@Override
	public void resetOrInit(DialogDataProvider provider) {
        if (provider == null) {
            reset();
        } else {
            init(provider.getNode());
        }
	}
	
    public void init(Node node) {
        reset();
        nodeRefString = node.getNodeRef().toString();
		init();
	}
    
    public void init(String nodeRef) {
        reset();
        nodeRefString = nodeRef;
		init();
	}

	private void init() {
		Pair<String, HttpStatus> documnetJSONByStatus = adrApiService.getDocumentFromAdr(nodeRefString);
		status = getDocumentStatus(documnetJSONByStatus);

		if (AdrDocumentStatus.NOT_FOUND.toString().equals(status)) {
			String chngRefStore = nodeRefString;
			if (nodeRefString.contains("ArchivalsStore")) {
				chngRefStore = nodeRefString.replace("ArchivalsStore", "SpacesStore");
			} else if (nodeRefString.contains("SpacesStore")) {
				chngRefStore = nodeRefString.replace("SpacesStore", "ArchivalsStore");
			}
			documnetJSONByStatus = adrApiService.getDocumentFromAdr(chngRefStore);
			status = getDocumentStatus(documnetJSONByStatus);
		}

		href = getDocumentHref(documnetJSONByStatus);
		deleteDisabled = isDeleteDisabled(documnetJSONByStatus.getSecond());
		if (StringUtils.isNotBlank(nodeRefString)) {
			long startTime = System.currentTimeMillis();
			isChangedForAdr = BeanHelper.getAdrService().isAdrChangedDocument(new NodeRef(nodeRefString));
			log.info("ADR changed documents search time " + (System.currentTimeMillis() - startTime) + " ms");
		}
	}

	public void reset() {
    	nodeRefString = null;
    	status = null;
    	href = null;
        deleteDisabled = true;
    }
    
    private String getDocumentStatus(Pair<String, HttpStatus> documnetJSONByStatus) {
    	if (documnetJSONByStatus.getSecond().equals(HttpStatus.OK)) {
    		return AdrDocumentStatus.EXIST.toString();
    	} else if (documnetJSONByStatus.getSecond().equals(HttpStatus.NOT_FOUND)) {
    		return AdrDocumentStatus.NOT_FOUND.toString();
    	} else {
    		return AdrDocumentStatus.ERROR.toString();
    	}
    }
    
    private String getDocumentHref(Pair<String, HttpStatus> documnetJSONByStatus) {
    	if (documnetJSONByStatus.getSecond().equals(HttpStatus.OK)) {
    		ObjectMapper mapper = new ObjectMapper();
        	try {
    			AdrDocumentModel adrDocumentModel = mapper.readValue(documnetJSONByStatus.getFirst(), AdrDocumentModel.class);
    			return adrDocumentModel.getUri();
    		} catch (IOException e) {
    			log.error(e.getMessage());
    		}
    	}
    	return "";
    }
    
    private boolean isDeleteDisabled(HttpStatus status) {
    	return !status.equals(HttpStatus.OK);
    }
    
    public void confirmAdrDocumentDelete(ActionEvent event) {
    	BeanHelper.getUserConfirmHelper().setup("document_adr_delete_confirm", null, "#{AdrBlockBean.deleteDocument}", null);
    }

	public void adrDocumentAdd(ActionEvent event) {
		NodeRef nodeRef = new NodeRef(nodeRefString);
		HttpStatus status = adrApiService.addDocumentToAdr(BeanHelper.getAdrService().convertToDokumentDetailidegaV2(nodeRef));
		if (status.equals(HttpStatus.OK)) {
			log.info(String.format("Dokument %s on saadetud ADRi", nodeRefString));
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.SECOND, 5);
			BeanHelper.getNodeService().setProperty(nodeRef, DocumentDynamicModel.Props.MANUAL_ADR_PUBLISH_DATE, calendar.getTime());
		} else {
			final FacesContext context = FacesContext.getCurrentInstance();
			MessageUtil.addErrorMessage(context, "document_adr_add_error");
		}
		init(nodeRefString);
	}
    
    public void deleteDocument(ActionEvent event) {
    	HttpStatus status = adrApiService.deleteDocumentFromAdr(nodeRefString);

    	if (status != null && HttpStatus.NOT_FOUND.value() == status.value()) {
			String chngRefStore = nodeRefString;
			if (nodeRefString.contains("ArchivalsStore")) {
				chngRefStore = nodeRefString.replace("ArchivalsStore", "SpacesStore");
			} else if (nodeRefString.contains("SpacesStore")) {
				chngRefStore = nodeRefString.replace("SpacesStore", "ArchivalsStore");
			}
			status = adrApiService.deleteDocumentFromAdr(chngRefStore);
		}

    	if (status.equals(HttpStatus.OK)) {
    		Node docNode = BeanHelper.getDocumentService().getDocument(new NodeRef(nodeRefString));
			BeanHelper.getNodeService().setProperty(docNode.getNodeRef(), DocumentDynamicModel.Props.MANUAL_ADR_PUBLISH_DATE, null);
    		log.info(String.format("Dokument %s on eemaldatud ADRist", nodeRefString));
    	}
    	init(nodeRefString);
    }

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getNodeRefString() {
		return nodeRefString;
	}

	public void setNodeRefString(String nodeRefString) {
		this.nodeRefString = nodeRefString;
	}

	public boolean isDeleteDisabled() {
		return deleteDisabled;
	}

	public boolean isAddEnabled() {
		return isChangedForAdr;
	}

	public void setDeleteDisabled(boolean deleteDisabled) {
		this.deleteDisabled = deleteDisabled;
	}

}
