package ee.webmedia.alfresco.document.adr.web;

import ee.webmedia.alfresco.utils.MessageUtil;

public enum AdrDocumentStatus {
	EXIST {
	    @Override
	    public String toString() {
	    	return MessageUtil.getMessage("document_adr_status_exist");
	    }
	}, NOT_FOUND {
	    @Override
	    public String toString() {
	    	return MessageUtil.getMessage("document_adr_status_not_found");
	    }
	}, ERROR {
	    @Override
	    public String toString() {
	    	return MessageUtil.getMessage("document_adr_status_error");
	    }
	}
}
