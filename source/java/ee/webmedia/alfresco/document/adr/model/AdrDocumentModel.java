package ee.webmedia.alfresco.document.adr.model;

import java.util.Date;

public class AdrDocumentModel {

	Long id;
	Long organizationId;
	String noderef;
	Date regDateTime;
	String regNumber;
	String title;
	String accessRestriction;
	String accessRestrictionReason;
	Date accessRestrictionBeginDate;
	Date accessRestrictionEndDate;
	String accessRestrictionEndDesc;
	Date dueDate;
	Date complianceDate;
	String compilator;
	String annex;
	String senderRegNumber;
	String transmittalMode;
	String party;
	Date createdDateTime;
	Date modifiedDateTime;
	Boolean byRequestOnly;
	String accessRestrictionChangeReason;
	Date sendDate;
	String uri;
	String docTypeName;
	String volumeNodeRef;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public String getNoderef() {
		return noderef;
	}
	public void setNoderef(String noderef) {
		this.noderef = noderef;
	}
	public Date getRegDateTime() {
		return regDateTime;
	}
	public void setRegDateTime(Date regDateTime) {
		this.regDateTime = regDateTime;
	}
	public String getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAccessRestriction() {
		return accessRestriction;
	}
	public void setAccessRestriction(String accessRestriction) {
		this.accessRestriction = accessRestriction;
	}
	public String getAccessRestrictionReason() {
		return accessRestrictionReason;
	}
	public void setAccessRestrictionReason(String accessRestrictionReason) {
		this.accessRestrictionReason = accessRestrictionReason;
	}
	public Date getAccessRestrictionBeginDate() {
		return accessRestrictionBeginDate;
	}
	public void setAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
		this.accessRestrictionBeginDate = accessRestrictionBeginDate;
	}
	public Date getAccessRestrictionEndDate() {
		return accessRestrictionEndDate;
	}
	public void setAccessRestrictionEndDate(Date accessRestrictionEndDate) {
		this.accessRestrictionEndDate = accessRestrictionEndDate;
	}
	public String getAccessRestrictionEndDesc() {
		return accessRestrictionEndDesc;
	}
	public void setAccessRestrictionEndDesc(String accessRestrictionEndDesc) {
		this.accessRestrictionEndDesc = accessRestrictionEndDesc;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Date getComplianceDate() {
		return complianceDate;
	}
	public void setComplianceDate(Date complianceDate) {
		this.complianceDate = complianceDate;
	}
	public String getCompilator() {
		return compilator;
	}
	public void setCompilator(String compilator) {
		this.compilator = compilator;
	}
	public String getAnnex() {
		return annex;
	}
	public void setAnnex(String annex) {
		this.annex = annex;
	}
	public String getSenderRegNumber() {
		return senderRegNumber;
	}
	public void setSenderRegNumber(String senderRegNumber) {
		this.senderRegNumber = senderRegNumber;
	}
	public String getTransmittalMode() {
		return transmittalMode;
	}
	public void setTransmittalMode(String transmittalMode) {
		this.transmittalMode = transmittalMode;
	}
	public String getParty() {
		return party;
	}
	public void setParty(String party) {
		this.party = party;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getModifiedDateTime() {
		return modifiedDateTime;
	}
	public void setModifiedDateTime(Date modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}
	public Boolean getByRequestOnly() {
		return byRequestOnly;
	}
	public void setByRequestOnly(Boolean byRequestOnly) {
		this.byRequestOnly = byRequestOnly;
	}
	public String getAccessRestrictionChangeReason() {
		return accessRestrictionChangeReason;
	}
	public void setAccessRestrictionChangeReason(String accessRestrictionChangeReason) {
		this.accessRestrictionChangeReason = accessRestrictionChangeReason;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getDocTypeName() {
		return docTypeName;
	}
	public void setDocTypeName(String docTypeName) {
		this.docTypeName = docTypeName;
	}
	public String getVolumeNodeRef() {
		return volumeNodeRef;
	}
	public void setVolumeNodeRef(String volumeNodeRef) {
		this.volumeNodeRef = volumeNodeRef;
	}

}
