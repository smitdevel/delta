<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ page import="org.alfresco.web.app.Application" %>
<%@ page import="ee.webmedia.alfresco.document.web.BaseDocumentListDialog" %>

<a:booleanEvaluator value="#{DocumentSearchResultsDialog.infoMessageVisible}">
   <a:panel id="info-message" styleClass="message">
      <h:outputText value="#{DocumentSearchResultsDialog.infoMessage}" />
   </a:panel>
</a:booleanEvaluator>
<jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/document/web/limited-message-panel.jsp" />
<jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/document/web/unlimited-message-panel.jsp" />

<a:panel id="document-panel" styleClass="panel-100 with-pager" label="#{DocumentSearchResultsDialog.listTitle}" progressive="true">
	         <f:facet name="header">
	            <h:selectBooleanCheckbox id="col0-header" value="false" styleClass="selectAllHeader" rendered="#{DocumentSearchResultsDialog.showCheckboxes}" />
	         </f:facet>
   <%-- Main List --%>
   <a:richList id="documentList" styleClass="duplicate-header" viewMode="details" pageSize="#{BrowseBean.pageSizeContent}" rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt"
      width="100%" binding="#{DocumentSearchResultsDialog.richList}" refreshOnBind="true" value="#{DocumentSearchResultsDialog.documents}" var="r" >
      	      <%-- checkbox --%>
	      <a:column id="col-checkbox" primary="true" styleClass="#{r.cssStyleClass}" rendered="#{DocumentSearchResultsDialog.showCheckboxes}" >
           <h:selectBooleanCheckbox id="col1-checkbox" styleClass="headerSelectable" value="#{DocumentSearchResultsDialog.listCheckboxes[r.nodeRef]}" disabled="#{r.markedForFinish}"/>
          </a:column>
      <jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/page-size.jsp" />
      <a:dataPager id="pager1" styleClass="pager" />
   </a:richList>

</a:panel>

<jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/disable-dialog-finish-button.jsp" />
<jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/common/web/select-all-header-function.jsp" />