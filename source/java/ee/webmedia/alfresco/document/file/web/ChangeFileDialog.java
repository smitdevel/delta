package ee.webmedia.alfresco.document.file.web;

import static ee.webmedia.alfresco.common.web.BeanHelper.*;

import java.io.*;
import java.io.File;
import java.util.*;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import ee.webmedia.alfresco.classificator.enums.AccessRestriction;
import ee.webmedia.alfresco.classificator.model.ClassificatorValue;
import ee.webmedia.alfresco.document.file.model.*;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.signature.exception.SignatureException;
import ee.webmedia.alfresco.signature.model.DataItem;
import ee.webmedia.alfresco.signature.model.SignatureItemsAndDataItems;
import org.alfresco.i18n.I18NUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.lock.LockStatus;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.FileUploadBean;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.ui.common.component.data.UIColumn;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.privilege.model.Privilege;
import ee.webmedia.alfresco.utils.ActionUtil;
import ee.webmedia.alfresco.utils.FilenameUtil;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.utils.WebUtil;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.BooleanUtils;

public class ChangeFileDialog extends BaseDialogBean {
    public static final String BEAN_NAME = "ChangeFileDialog";
    private static final long serialVersionUID = 1L;

    private NodeRef docRef;
    private List<String> fileNamesWithoutExtension;
    private List<String> fileNames;
    private List<String> fileOrdersInList;
    private List<Long> fileOrderValues;
    private List<FileVO> files;
    private ArrayList<FileAccessRestriction> fileAccessRestrictions;
    private ArrayList<FileAccessRestriction> accessRestrictions;
    private ArrayList<FileAccessRestriction> attachmentsAccessRestrictions;
    private boolean isFileUpload = false;
    private boolean attachmentsOrScannedFilesAdded = false;
    private boolean activeFilesDialog = true;

    @Override
    public String cancel() {
        handleLock(false);
        init();
        return super.cancel();
    }

    private void init() {
        fileNamesWithoutExtension = null;
        fileNames = null;
        fileOrdersInList = null;
        fileOrderValues = null;
        files = null;
        accessRestrictions = new ArrayList<>();
        attachmentsAccessRestrictions = new ArrayList<>();
        fileAccessRestrictions = new ArrayList<>();
        isFileUpload = false;
        attachmentsOrScannedFilesAdded = false;
    }

    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        if (isFileUpload) {
            fileNames.clear();
            fileOrderValues.clear();
            fileOrdersInList.clear();
            fileNamesWithoutExtension.clear();
            accessRestrictions.clear();
            attachmentsAccessRestrictions.clear();

            if (!validate(docRef)) {
                return null;
            }
            setFileValues();
            FileUploadBean fileUploadBean = getFileUploadBean();
            if (fileUploadBean != null) {
                fileUploadBean.setFileName(fileNames);
                fileUploadBean.setFileNameWithoutExtension(fileNamesWithoutExtension);
                fileUploadBean.setOrderNumbers(fileOrderValues);
                fileUploadBean.setAccessRestrictions(accessRestrictions);
            }
            AddFileDialog addFileDialog = BeanHelper.getAddFileDialog();
            if (attachmentsOrScannedFilesAdded) {
                List<NodeRef> tempNodeRefs = new ArrayList<>();
                List<String> tempNames = new ArrayList<>();
                List<String> tempNamesWithoutExtensions = new ArrayList<>();
                List<Long> tempOrdersInList = new ArrayList<>();
                List<Boolean> tempSelectedWithMetadata = new ArrayList<>();

                for (FileVO file : files) {
                    if (file.getFileRef() == null || file.isDigiDocItem()) {
                        continue;
                    }
                    tempNodeRefs.add(file.getFileRef());
                    tempNames.add(file.getFileNameWithExtension());
                    tempNamesWithoutExtensions.add(file.getFileNameWithoutExtension());
                    tempOrdersInList.add(Long.valueOf(file.getFileOrderInList()));
                    tempSelectedWithMetadata.add(AddFileDialog.BOUND_METADATA_EXTENSIONS.contains(file.getExtension()));
                }
                addFileDialog.setSelectedFileNodeRef(tempNodeRefs);
                addFileDialog.setSelectedFileName(tempNames);
                addFileDialog.setSelectedFileNameWithoutExtension(tempNamesWithoutExtensions);
                addFileDialog.setSelectedFileOrderInList(tempOrdersInList);
                addFileDialog.setSelectedAssociatedWithMetaData(tempSelectedWithMetadata);
                addFileDialog.setSelectedFileAccessRestrictions(attachmentsAccessRestrictions);
            }
            addFileDialog.save(context, outcome, false);
            return "dialog:close[2]";
        }
        validatePermission(docRef, Privilege.EDIT_DOCUMENT);
        if (!validate(docRef)) {
            return null;
        }
        Map<NodeRef, Long> originalOrders = new HashMap<>();
        int fileNameIndex = 0;
        for (int i = 0; i < files.size(); i++) {
            if (!files.get(i).isDigiDocItem()) {
                String newDisplayName = files.get(i).getFileNameWithExtension();
                String newOrderNr = files.get(i).getFileOrderInList();
                boolean nameChanged = false;
                if (!StringUtils.equals(newDisplayName, fileNames.get(fileNameIndex))) { // display name changed
                    nameChanged = true;
                }
                Map<QName, Serializable> properties = getNodeService().getProperties(files.get(i).getFileRef());

                if (nameChanged) {
                    Pair<String, String> filenames = FilenameUtil.getFilenameFromDisplayname(docRef, getFileService().getDocumentFileDisplayNames(docRef),
                            newDisplayName, BeanHelper.getGeneralService());
                    properties.put(ContentModel.PROP_NAME, filenames.getFirst());
                    properties.put(FileModel.Props.DISPLAY_NAME, filenames.getSecond());
                }
                Long originalOrderNr = (Long) properties.get(FileModel.Props.FILE_ORDER_IN_LIST);
                originalOrders.put(files.get(i).getFileRef(), originalOrderNr);

                if (files.get(i).isDigiDoc()) {
                    generateInnerAccessRestrictions(properties, files.get(i), files);
                } else {
                    properties.put(FileModel.Props.FILE_ORDER_IN_LIST, Long.valueOf(newOrderNr));
                    String accessRestriction = files.get(i).getAccessRestriction();
                    properties.put(FileModel.Props.ACCESS_RESTRICTION, accessRestriction);
                    if (AccessRestriction.AK.getValueName().equals(accessRestriction)) {
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_REASON, files.get(i).getAccessRestrictionReason());
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_BEGIN_DATE, files.get(i).getAccessRestrictionBeginDate());
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_END_DATE, files.get(i).getAccessRestrictionEndDate());
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_END_DESC, files.get(i).getAccessRestrictionEndDesc());
                    } else {
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_REASON, null);
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_BEGIN_DATE, null);
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_END_DATE, null);
                        properties.put(FileModel.Props.ACCESS_RESTRICTION_END_DESC, null);
                    }
                }
                getNodeService().addProperties(files.get(i).getFileRef(), properties);
                if (nameChanged) {
                    getDocumentLogService().addDocumentLog(docRef, I18NUtil.getMessage("document_log_status_fileNameChanged", fileNames.get(fileNameIndex), newDisplayName));
                }
                logAccessRestrictionChange(i, fileNames.get(fileNameIndex));
                fileNameIndex++;
            } else {
                logAccessRestrictionChange(i, files.get(i).getFileNameWithExtension());
            }
        }
        getFileService().reorderFiles(docRef, originalOrders);
        MessageUtil.addInfoMessage("save_success");
        handleLock(false);
        return "dialog:close#files-panel";
    }

    private void logAccessRestrictionChange(int fileIndex, String fileName) {
        String emptyValue = MessageUtil.getMessage("document_log_status_empty");

        String newAccessRestriction = StringUtils.defaultString(files.get(fileIndex).getAccessRestriction(), emptyValue);
        String oldAccessRestriction = StringUtils.defaultString(fileAccessRestrictions.get(fileIndex).getAccessRestriction(), emptyValue);
        if (!Objects.equals(newAccessRestriction, oldAccessRestriction)) {
            getDocumentLogService().addDocumentLog(docRef, I18NUtil.getMessage("document_log_status_fileAccessChanged",
                    fileName, I18NUtil.getMessage("file_access_restriction"), oldAccessRestriction, newAccessRestriction));
        }

        String newAccessRestrictionReason = StringUtils.defaultString(files.get(fileIndex).getAccessRestrictionReason(), emptyValue);
        String oldAccessRestrictionReason = StringUtils.defaultString(fileAccessRestrictions.get(fileIndex).getAccessRestrictionReason(), emptyValue);
        if (!Objects.equals(newAccessRestrictionReason, oldAccessRestrictionReason)) {
            getDocumentLogService().addDocumentLog(docRef, I18NUtil.getMessage("document_log_status_fileAccessChanged",
                    fileName, I18NUtil.getMessage("file_access_restriction_reason"), oldAccessRestrictionReason, newAccessRestrictionReason));
        }

        String newAccessRestrictionBeginDate = formatLogDate(files.get(fileIndex).getAccessRestrictionBeginDate());
        String oldAccessRestrictionBeginDate = formatLogDate(fileAccessRestrictions.get(fileIndex).getAccessRestrictionBeginDate());
        if (!Objects.equals(newAccessRestrictionBeginDate, oldAccessRestrictionBeginDate)) {
            getDocumentLogService().addDocumentLog(docRef, I18NUtil.getMessage("document_log_status_fileAccessChanged",
                    fileName, I18NUtil.getMessage("file_access_restriction_begin_date"), oldAccessRestrictionBeginDate, newAccessRestrictionBeginDate));
        }

        String newAccessRestrictionEndDate = formatLogDate(files.get(fileIndex).getAccessRestrictionEndDate());
        String oldAccessRestrictionEndDate = formatLogDate(fileAccessRestrictions.get(fileIndex).getAccessRestrictionEndDate());
        if (!Objects.equals(newAccessRestrictionEndDate, oldAccessRestrictionEndDate)) {
            getDocumentLogService().addDocumentLog(docRef, I18NUtil.getMessage("document_log_status_fileAccessChanged",
                    fileName, I18NUtil.getMessage("file_access_restriction_end_date"), oldAccessRestrictionEndDate, newAccessRestrictionEndDate));
        }

        String newAccessRestrictionEndDesc = StringUtils.defaultString(files.get(fileIndex).getAccessRestrictionEndDesc(), emptyValue);
        String oldAccessRestrictionEndDesc = StringUtils.defaultString(fileAccessRestrictions.get(fileIndex).getAccessRestrictionEndDesc(), emptyValue);
        if (!Objects.equals(newAccessRestrictionEndDesc, oldAccessRestrictionEndDesc)) {
            getDocumentLogService().addDocumentLog(docRef, I18NUtil.getMessage("document_log_status_fileAccessChanged",
                    fileName, I18NUtil.getMessage("file_access_restriction_end_date_desc"), oldAccessRestrictionEndDesc, newAccessRestrictionEndDesc));
        }
    }

    private static String formatLogDate(Date date) {
        if (date == null) {
            return MessageUtil.getMessage("document_log_status_empty");
        }
        return FastDateFormat.getInstance("dd.MM.yyyy").format(date);
    }

    private void generateInnerAccessRestrictions(Map<QName, Serializable> properties, FileVO parent, List<FileVO> files) {
        ArrayList<String> accessRestrictions = new ArrayList<>();
        ArrayList<String> accessRestrictionReasons = new ArrayList<>();
        ArrayList<String> accessRestrictionEndDescs = new ArrayList<>();
        ArrayList<Date> accessRestrictionBeginDates = new ArrayList<>();
        ArrayList<Date> accessRestrictionEndDates = new ArrayList<>();
        if (parent.isDigiDoc() && parent.getFileRef() != null) {
            for (FileVO fileVO : files) {
                if (fileVO.isDigiDocItem() && parent.getFileRef().equals(fileVO.getParentRef())) {
                    String accessRestriction = fileVO.getAccessRestriction();
                    accessRestrictions.add(accessRestriction);
                    if (AccessRestriction.AK.getValueName().equals(accessRestriction)) {
                        accessRestrictionReasons.add(fileVO.getAccessRestrictionReason());
                        accessRestrictionEndDescs.add(fileVO.getAccessRestrictionEndDesc());
                        accessRestrictionBeginDates.add(fileVO.getAccessRestrictionBeginDate());
                        accessRestrictionEndDates.add(fileVO.getAccessRestrictionEndDate());
                    } else {
                        accessRestrictionReasons.add(null);
                        accessRestrictionEndDescs.add(null);
                        accessRestrictionBeginDates.add(null);
                        accessRestrictionEndDates.add(null);
                    }
                }
            }
            properties.put(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTIONS, accessRestrictions);
            properties.put(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_REASONS, accessRestrictionReasons);
            properties.put(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DESCS, accessRestrictionEndDescs);
            properties.put(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_BEGIN_DATES, accessRestrictionBeginDates);
            properties.put(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DATES, accessRestrictionEndDates);
        }
    }

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }

    public void open() {
        open(null);
    }

    public FileUploadBean getFileUploadBean() {
        FileUploadBean fileBean = (FileUploadBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
                get(FileUploadBean.FILE_UPLOAD_BEAN_NAME);
        return fileBean;
    }

    public void open(ActionEvent event) {
        init();
        try {
            files = new ArrayList<>();
            docRef = getDocumentDialogHelperBean().getNodeRef();
            if (event == null) { // file upload, must display only newly uploaded files
                AddFileDialog addFileDialog = BeanHelper.getAddFileDialog();
                attachmentsOrScannedFilesAdded = addFileDialog.addAttatchmentsAndScannedFiles();
                isFileUpload = true;
                Boolean isActive = addFileDialog.isActiveFileDialog();
                activeFilesDialog = isActive;
                int maxOrderNr = 0;
                List<SimpleFileWithOrder> docFiles = null;
                if (isActive) {
                	docFiles = BeanHelper.getBulkLoadNodeService().loadActiveFilesWithOrder(docRef);
                } else {
                	docFiles = BeanHelper.getBulkLoadNodeService().loadInactiveFilesWithOrder(docRef);
                }
                if (docFiles != null) {
                	maxOrderNr = docFiles.size();
                }
                FileUploadBean uploadBean = getFileUploadBean();
                if (uploadBean != null) {
                    for (int i = 0; i < uploadBean.getFileNames().size(); i++) {
                        String fileOrderInList = String.valueOf(++maxOrderNr);
                        String fileName = uploadBean.getFileNames().get(i);
                        File file = uploadBean.getFiles().get(i);
                        FileVO fileVO = new FileVO(fileOrderInList, FilenameUtils.removeExtension(fileName), FilenameUtils.getExtension(fileName), null, false);
                        fileVO.setDdocItems(getFileDigiDocData(fileName, file));
                        files.add(fileVO);
                        addDigiDocItems(fileOrderInList, fileVO, files);
                    }
                }
                if (attachmentsOrScannedFilesAdded) {
                    List<String> fileNames = addFileDialog.getSelectedFileName();
                    List<NodeRef> fileRefs = addFileDialog.getSelectedFileNodeRef();
                    for (int i = 0; i < fileNames.size(); i++) {
                        String fileName = fileNames.get(i);
                        String fileOrderInList = String.valueOf(++maxOrderNr);
                        FileVO fileVO = new FileVO(fileOrderInList, FilenameUtils.removeExtension(fileName), FilenameUtils.getExtension(fileName), fileRefs.get(i), false);
                        fileVO.setDdocItems(getDigiDocData(fileName, fileRefs.get(i)));
                        files.add(fileVO);
                        addDigiDocItems(fileOrderInList, fileVO, files);
                    }
                }
            } else { // started from document dialog
                String type = ActionUtil.getParam(event, "type");
                activeFilesDialog = "active".equals(type);
                List<SimpleFileWithOrder> docFiles = null;
                if (activeFilesDialog) {
                    docFiles = BeanHelper.getBulkLoadNodeService().loadActiveFilesWithOrder(docRef);
                    for (SimpleFileWithOrder f : docFiles) {
                        String orderInList = String.valueOf(f.getFileOrderInList());
                        FileVO fileVO = new FileVO(orderInList, FilenameUtils.removeExtension(f.getDisplayName()), FilenameUtils.getExtension(f.getDisplayName()),
                                f.getFileRef(), f.isGenerated());
                        fileVO.setAccessRestriction(f.getAccessRestriction());
                        fileVO.setAccessRestrictionReason(f.getAccessRestrictionReason());
                        fileVO.setAccessRestrictionEndDesc(f.getAccessRestrictionEndDesc());
                        fileVO.setAccessRestrictionBeginDate(f.getAccessRestrictionBeginDate());
                        fileVO.setAccessRestrictionEndDate(f.getAccessRestrictionEndDate());
                        fileVO.setDdocItems(getDigiDocData(f.getDisplayName(), f.getFileRef()));
                        files.add(fileVO);
                        addDigiDocItems(orderInList, fileVO, files);
                    }
                } else {
                    docFiles = BeanHelper.getBulkLoadNodeService().loadInactiveFilesWithOrder(docRef);
                    for (SimpleFileWithOrder f : docFiles) {
                        String orderInList = String.valueOf(f.getFileOrderInList());
                        FileVO fileVO = new FileVO(orderInList, FilenameUtils.removeExtension(f.getDisplayName()), FilenameUtils.getExtension(f.getDisplayName()),
                                f.getFileRef(), f.isGenerated());
                        files.add(fileVO);
                    }
                }
            }

            Collections.sort(files);

            fileNames = new ArrayList<>();
            fileOrderValues = new ArrayList<>();
            fileOrdersInList = new ArrayList<>();
            fileNamesWithoutExtension = new ArrayList<>();
            setFileValues();
            handleLock(true);
            WebUtil.navigateTo("dialog:changeFile", FacesContext.getCurrentInstance());
        } catch (RuntimeException e) {
            if (e instanceof NodeLockedException) {
                BeanHelper.getDocumentLockHelperBean().handleLockedNode("file_change_file_validation_alreadyLocked", ((NodeLockedException) e).getNodeRef());
            } else {
                MessageUtil.addErrorMessage("file_open_for_changing_failed");
            }
        }
    }

    private SignatureItemsAndDataItems getDigiDocData(String fileName, NodeRef fileRef) {
        boolean isDigiDoc = FilenameUtil.isDigiDocFile(fileName);
        if (isDigiDoc) {
            try {
                return BeanHelper.getDigiDoc4JSignatureService().getDataItemsAndSignatureItems(fileRef, true);
            } catch (SignatureException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private SignatureItemsAndDataItems getFileDigiDocData(String fileName, File file) {
        boolean isDigiDoc = FilenameUtil.isDigiDocFile(fileName);
        if (isDigiDoc) {
            try {
                return BeanHelper.getDigiDoc4JSignatureService().getDataItemsAndSignatureItems(new FileInputStream(file), true);
            } catch (SignatureException | FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void addDigiDocItems(String orderInList, FileVO file, List<FileVO> files) {
        if (file.isDigiDoc()) {
            Map<QName, Serializable> props = file.getFileRef() != null ? BeanHelper.getNodeService().getProperties(file.getFileRef()) : new HashMap<QName, Serializable>();
            ArrayList<String> accessRestrictions = (ArrayList<String>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTIONS);
            ArrayList<String> accessRestrictionReasons = (ArrayList<String>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_REASONS);
            ArrayList<String> accessRestrictionEndDescs = (ArrayList<String>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DESCS);
            ArrayList<Date> accessRestrictionBeginDates = (ArrayList<Date>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_BEGIN_DATES);
            ArrayList<Date> accessRestrictionEndDates = (ArrayList<Date>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DATES);
            int index = 0;
            for (DataItem item : file.getDataItems()) {
                FileVO subFile = new FileVO(orderInList, FilenameUtils.removeExtension(item.getName()), FilenameUtils.getExtension(item.getName()), null, false);
                subFile.setAccessRestriction(CollectionUtils.isNotEmpty(accessRestrictions) && accessRestrictions.size() > index ? accessRestrictions.get(index) : null);
                subFile.setAccessRestrictionReason(CollectionUtils.isNotEmpty(accessRestrictionReasons) && accessRestrictionReasons.size() > index ? accessRestrictionReasons.get(index) : null);
                subFile.setAccessRestrictionEndDesc(CollectionUtils.isNotEmpty(accessRestrictionEndDescs) && accessRestrictionEndDescs.size() > index ? accessRestrictionEndDescs.get(index) : null);
                subFile.setAccessRestrictionBeginDate(CollectionUtils.isNotEmpty(accessRestrictionBeginDates) && accessRestrictionBeginDates.size() > index ? accessRestrictionBeginDates.get(index) : null);
                subFile.setAccessRestrictionEndDate(CollectionUtils.isNotEmpty(accessRestrictionEndDates) && accessRestrictionEndDates.size() > index ? accessRestrictionEndDates.get(index) : null);
                subFile.setDigiDocItem(true);
                subFile.setParentRef(file.getFileRef());
                subFile.setParentOrderInList(file.getParentOrderInList());
                subFile.setDigiDocItemUrl(item.getDownloadUrl());
                subFile.setParentOrderInList(file.getFileOrderInList());
                files.add(subFile);
                index++;
            }
        }
    }

    private void setFileValues() {
        for (FileVO sf : files) {
            if (!sf.isDigiDocItem()) {
                fileNames.add(sf.getFileNameWithExtension());
                fileOrderValues.add(Long.valueOf(sf.getFileOrderInList()));
                fileOrdersInList.add(sf.getFileOrderInList());
                fileNamesWithoutExtension.add(sf.getFileNameWithoutExtension());
                List<FileVO> innerFiles = getInnerFilesByOrder(sf.getFileOrderInList(), files);
                if (sf.getFileRef() == null) {
                    accessRestrictions.add(convert(sf, innerFiles));
                } else {
                    attachmentsAccessRestrictions.add(convert(sf, innerFiles));
                }
            }
            fileAccessRestrictions.add(convert(sf));
        }
    }

    private FileAccessRestriction convert(FileVO file, List<FileVO> subFiles) {
        FileAccessRestriction item = new FileAccessRestriction();
        item.setAccessRestriction(file.getAccessRestriction());
        item.setAccessRestrictionReason(file.getAccessRestrictionReason());
        item.setAccessRestrictionBeginDate(file.getAccessRestrictionBeginDate());
        item.setAccessRestrictionEndDate(file.getAccessRestrictionEndDate());
        item.setAccessRestrictionEndDesc(file.getAccessRestrictionEndDesc());
        for (FileVO vo : subFiles) {
            item.addInnerFileAccessRestriction(vo.getAccessRestriction());
            item.addInnerFileAccessRestrictionBeginDate(vo.getAccessRestrictionBeginDate());
            item.addInnerFileAccessRestrictionEndDate(vo.getAccessRestrictionEndDate());
            item.addInnerFileAccessRestrictionReason(vo.getAccessRestrictionReason());
            item.addInnerFileAccessRestrictionEndDesc(vo.getAccessRestrictionEndDesc());
        }
        return item;
    }

    private FileAccessRestriction convert(FileVO file) {
        FileAccessRestriction item = new FileAccessRestriction();
        item.setAccessRestriction(file.getAccessRestriction());
        item.setAccessRestrictionReason(file.getAccessRestrictionReason());
        item.setAccessRestrictionBeginDate(file.getAccessRestrictionBeginDate());
        item.setAccessRestrictionEndDate(file.getAccessRestrictionEndDate());
        item.setAccessRestrictionEndDesc(file.getAccessRestrictionEndDesc());
        return item;
    }

    private ArrayList<FileVO> getInnerFilesByOrder(String fileOrderInList, List<FileVO> files) {
        ArrayList<FileVO> result = new ArrayList<>();
        for (FileVO fileVO : files) {
            if (fileVO.isDigiDocItem() && Objects.equals(fileOrderInList, fileVO.getParentOrderInList())) {
                result.add(fileVO);
            }
        }
        return result;
    }

    private boolean handleLock(boolean lock4Edit) {
        if (isFileUpload) {
            return true;
        }
        List<Pair<NodeRef, Boolean>> fileRefsWithGeneratedProp = getFileRefsWithGeneratedProp();
        if (lock4Edit) {
            for (Pair<NodeRef, Boolean> fileRef : fileRefsWithGeneratedProp) {
                try {
                    if (getDocLockService().setLockIfFree(fileRef.getFirst()) == LockStatus.LOCKED) {
                        getDocLockService().unlockFiles(fileRefsWithGeneratedProp, docRef);
                    }
                } catch (RuntimeException e) {
                    getDocLockService().unlockFiles(fileRefsWithGeneratedProp, docRef);
                    throw e;
                }
            }
            return true;
        }
        getDocLockService().unlockFiles(fileRefsWithGeneratedProp, docRef);
        return false;
    }

    private List<Pair<NodeRef, Boolean>> getFileRefsWithGeneratedProp() {
        if (files == null) {
            return Collections.emptyList();
        }
        List<Pair<NodeRef, Boolean>> result = new ArrayList<>(files.size());
        for (FileVO f : files) {
            if (!f.isDigiDocItem()) {
                result.add(Pair.newInstance(f.getFileRef(), f.isGenerated()));
            }
        }
        return result;
    }

    private boolean validate(NodeRef docRef) {
        boolean isValid = true;
        for (int i = 0; i < files.size(); i++) {
            if (!files.get(i).isDigiDocItem()) {
                if (StringUtils.isEmpty(files.get(i).getFileNameWithoutExtension())) {
                    MessageUtil.addErrorMessage("common_propertysheet_validator_mandatory", MessageUtil.getMessage("name"));
                    isValid = false;
                    break;
                }
                if (isValid && StringUtils.isEmpty(files.get(i).getFileOrderInList())) {
                    MessageUtil.addErrorMessage("common_propertysheet_validator_mandatory", MessageUtil.getMessage("file_order"));
                    isValid = false;
                    break;
                }
                if (isValid && !files.get(i).getFileNameWithoutExtension().equals(FilenameUtil.stripForbiddenWindowsCharacters(files.get(i).getFileNameWithoutExtension()))) {
                    MessageUtil.addErrorMessage("add_file_invalid_file_name");
                    isValid = false;
                    break;
                }
                if (isValid) {
                    try {
                        fileOrderValues.add(Long.parseLong(files.get(i).getFileOrderInList()));
                    } catch (Exception e) {
                        MessageUtil.addErrorMessage("file_order_invalid", MessageUtil.getMessage("file_order"));
                        isValid = false;
                        break;
                    }
                }
            }
        }
        if (isFileAccessRestrictionMandatory() && activeFilesDialog) {
            String documentAccessRestriction = getDocumentService().getDocumentAccessRestriction(docRef);
            boolean isAkAccessRestriction = AccessRestriction.AK.getValueName().equals(documentAccessRestriction);
            boolean hasOpenAccessRestrictionFiles = hasOpenAccessRestrictionFiles(files);
            if (hasOpenAccessRestrictionFiles && isAkAccessRestriction) {
                MessageUtil.addWarningMessage("file_document_ak_access_restriction");
            }
            for (int i = 0; i < files.size(); i++) {
                FileVO fileVO = files.get(i);
                if (!fileVO.isDigiDoc() && StringUtils.isEmpty(fileVO.getAccessRestriction())) {
                    MessageUtil.addErrorMessage("common_propertysheet_validator_mandatory", MessageUtil.getMessage("file_access_restriction"));
                    isValid = false;
                    break;
                }
                boolean isAkAccess = AccessRestriction.AK.getValueName().equals(fileVO.getAccessRestriction());
                if (!fileVO.isDigiDoc() && isAkAccess && StringUtils.isEmpty(fileVO.getAccessRestrictionReason())) {
                    MessageUtil.addErrorMessage("common_propertysheet_validator_mandatory", MessageUtil.getMessage("file_access_restriction_reason"));
                    isValid = false;
                    break;
                }
                if (!fileVO.isDigiDoc() && isAkAccess && fileVO.getAccessRestrictionBeginDate() == null) {
                    MessageUtil.addErrorMessage("common_propertysheet_validator_mandatory", MessageUtil.getMessage("file_access_restriction_begin_date"));
                    isValid = false;
                    break;
                }
                if (!fileVO.isDigiDoc() && isAkAccess && fileVO.getAccessRestrictionEndDate() == null) {
                    MessageUtil.addErrorMessage("common_propertysheet_validator_mandatory", MessageUtil.getMessage("file_access_restriction_end_date"));
                    isValid = false;
                    break;
                }
            }
        }
        return isValid;
    }

    private boolean hasOpenAccessRestrictionFiles(List<FileVO> files) {
        if (CollectionUtils.isNotEmpty(files)) {
            for (FileVO fileVO : files) {
                if (!fileVO.isDigiDoc() && (fileVO.getAccessRestriction() == null
                        || AccessRestriction.OPEN.getValueName().equals(fileVO.getAccessRestriction()))) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<String> getFileOrderInList() {
        return fileOrdersInList;
    }

    public List<FileVO> getFiles() {
        return files;
    }

    public List<SelectItem> getAccessResctrictions() {
        List<SelectItem> accessRestrictions = getSelectItems("accessRestriction");
        if (!isFileAccessRestrictionMandatory() && hasAccessRestrictionInFiles()) {
            List<SelectItem> accessRestrictionsWithBlankOption = new ArrayList<>();
            accessRestrictionsWithBlankOption.add(new SelectItem(StringUtils.EMPTY));
            accessRestrictionsWithBlankOption.addAll(accessRestrictions);
            return accessRestrictionsWithBlankOption;
        }
        return accessRestrictions;
    }

    public List<SelectItem> getAccessResctrictionReasons() {
        List<SelectItem> accessRestrictionReasons = getSelectItems("accessRestrictionReason");
        List<SelectItem> accessRestrictionReasonsWithBlankOption = new ArrayList<>();
        accessRestrictionReasonsWithBlankOption.add(new SelectItem(StringUtils.EMPTY));
        accessRestrictionReasonsWithBlankOption.addAll(accessRestrictionReasons);
        return accessRestrictionReasonsWithBlankOption;
    }

    private List<SelectItem> getSelectItems(String classifierName) {
        List<ClassificatorValue> values = BeanHelper.getClassificatorService().getOrderedClassificatorValuesByName(classifierName);
        List<SelectItem> accessRestrictions = new ArrayList<>();
        for (ClassificatorValue value : values) {
            if (!value.getSelectorValueName().equals(AccessRestriction.INTERNAL.getValueName())
                    && !value.getSelectorValueName().equals(AccessRestriction.LIMITED.getValueName())) {
                accessRestrictions.add(new SelectItem(value.getSelectorValueName(), value.getSelectorValueName(), value.getClassificatorDescription()));
            }
        }
        return accessRestrictions;
    }

    public boolean isAllowFileAccessRestriction(){
        String parameter = getParametersService().getStringParameter(Parameters.ALLOW_FILE_ACCESS_RESTRICTION);
        return (BooleanUtils.toBoolean(parameter) || hasAccessRestrictionInFiles()) && activeFilesDialog;
    }

    public boolean isFileAccessRestrictionMandatory(){
        String parameter = getParametersService().getStringParameter(Parameters.ALLOW_FILE_ACCESS_RESTRICTION);
        return BooleanUtils.toBoolean(parameter);
    }

    public boolean hasAccessRestrictionInFiles() {
        if (CollectionUtils.isNotEmpty(files)) {
            for (FileVO file : files) {
                if (file.isAccessRestrictionSet()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void accessRestrictionChanged(ValueChangeEvent event) {
        String newValue = (String) event.getNewValue();
        UIComponent component = event.getComponent();
        List<UIColumn> columns = getColumns(component);
        if (AccessRestriction.AK.getValueName().equals(newValue)) {
            for (UIColumn col : columns) {
                if ("fileAccessRestrictionBeginDate".equals(col.getId())) {
                    HtmlInputText inputText = (HtmlInputText) col.getChildren().get(0);
                    inputText.setValue(new Date());
                }
            }
        } else {
            for (UIColumn col : columns) {
                if ("fileAccessRestrictionReason".equals(col.getId())
                        || "fileAccessRestrictionBeginDate".equals(col.getId())
                        || "fileAccessRestrictionEndDate".equals(col.getId())
                        || "fileAccessRestrictionEndDesc".equals(col.getId())) {
                    HtmlInputText inputText = (HtmlInputText) col.getChildren().get(0);
                    inputText.setValue(null);
                }
            }
        }
    }

    private List<UIColumn> getColumns(UIComponent component) {
        if (component != null && component.getParent() != null
                && component.getParent().getParent() != null
                && CollectionUtils.isNotEmpty(component.getParent().getParent().getChildren())) {
            return (List<UIColumn>) component.getParent().getParent().getChildren();
        }
        return Collections.emptyList();
    }

    public void accessRestrictionReasonChanged(ValueChangeEvent event) {
        String newValue = (String) event.getNewValue();
        UIComponent component = event.getComponent();
        if (StringUtils.isNotBlank(newValue) && component != null && component.getParent() != null && component.getParent().getParent() != null) {
            List<UIColumn> columns = getColumns(component);
            calculateAccessRestrictionEndDateOrDesc(newValue, columns);
            for (UIColumn col : columns) {
                if ("fileAccessRestrictionReason".equals(col.getId())) {
                    HtmlInputText inputText = (HtmlInputText) col.getChildren().get(0);
                    String newAccessRestrictionChangeReason = StringUtils.isBlank((String) inputText.getValue())
                            ? newValue : inputText.getValue() + ", " + newValue;
                    inputText.setValue(newAccessRestrictionChangeReason);
                }
            }
        }
    }

    private static Date getBeginDate(List<UIColumn> columns) {
        for (UIColumn col : columns) {
            if ("fileAccessRestrictionBeginDate".equals(col.getId())) {
                HtmlInputText inputText = (HtmlInputText) col.getChildren().get(0);
                return (Date) inputText.getValue();
            }
        }
        return null;
    }

    private static void setAccessRestrictionEndDate(List<UIColumn> columns, Date endDate) {
        for (UIColumn col : columns) {
            if ("fileAccessRestrictionEndDate".equals(col.getId())) {
                HtmlInputText inputText = (HtmlInputText) col.getChildren().get(0);
                inputText.setValue(endDate);
            }
        }
    }

    private static void setAccessRestrictionEndDateDesc(List<UIColumn> columns, String newAccessRestrictionEndDescData) {
        for (UIColumn col : columns) {
            if ("fileAccessRestrictionEndDesc".equals(col.getId())) {
                HtmlInputText inputText = (HtmlInputText) col.getChildren().get(0);
                String accessRestrictionEndDesc = (String) inputText.getValue();
                String newAccessRestrictionEndDesc = StringUtils.isBlank(accessRestrictionEndDesc) ? newAccessRestrictionEndDescData : accessRestrictionEndDesc + ", "
                        + newAccessRestrictionEndDescData;
                inputText.setValue(newAccessRestrictionEndDesc);
            }
        }
    }

    private static void calculateAccessRestrictionEndDateOrDesc(final String accessRestrictionReason, List<UIColumn> columns) {
        String valueData = BeanHelper.getClassificatorService().getClassificatorValuesValueData("accessRestrictionReason", accessRestrictionReason);
        if (valueData == null) {
            return;
        }
        Integer monthsToAdd = null;
        String newAccessRestrictionEndDescData = "";

        for (String valueDataPart : valueData.split(";")) {
            monthsToAdd = null;
            valueDataPart = valueDataPart.trim();
            try {
                monthsToAdd = Integer.parseInt(valueDataPart);
            } catch (NumberFormatException e) {
                // no need to add date
                newAccessRestrictionEndDescData += valueDataPart;
            }
            if (monthsToAdd != null) {
                Date restrictionBeginDate = getBeginDate(columns);
                if (restrictionBeginDate != null) {
                    Date endDate = DateUtils.addMonths(restrictionBeginDate, monthsToAdd);
                    setAccessRestrictionEndDate(columns, endDate);
                }
            } else if (StringUtils.isNotBlank(newAccessRestrictionEndDescData)) {
                setAccessRestrictionEndDateDesc(columns, newAccessRestrictionEndDescData);
            }
        }
    }

    public static class FileVO implements Serializable, Comparable<FileVO> {

        private static final long serialVersionUID = 1L;
        private String fileOrderInList;
        private String fileNameWithoutExtension;
        private final String extension;
        private final NodeRef fileRef;
        private final boolean generated;
        private String accessRestriction;
        private String accessRestrictionReason;
        private String accessRestrictionSelected;
        private String accessRestrictionEndDesc;
        private Date accessRestrictionBeginDate;
        private Date accessRestrictionEndDate;
        private SignatureItemsAndDataItems ddocItems;
        private boolean digiDocItem;
        private String digiDocItemUrl;
        private NodeRef parentRef;
        private String parentOrderInList;

        public FileVO(String fileOrderInList, String fileNameWithoutExtension, String extension, NodeRef fileRef, boolean generated) {
            this.fileOrderInList = fileOrderInList;
            this.fileNameWithoutExtension = fileNameWithoutExtension;
            this.extension = extension;
            this.fileRef = fileRef;
            this.generated = generated;
        }

        public String getFileOrderInList() {
            return fileOrderInList;
        }

        public void setFileOrderInList(String fileOrderInList) {
            this.fileOrderInList = fileOrderInList;
        }

        public String getFileNameWithoutExtension() {
            return fileNameWithoutExtension;
        }

        public void setFileNameWithoutExtension(String fileNameWithoutExtension) {
            this.fileNameWithoutExtension = fileNameWithoutExtension;
        }

        public String getExtension() {
            return extension;
        }

        public String getFileNameWithExtension() {
            return getFileNameWithoutExtension() + (StringUtils.isNotBlank(extension) ? ("." + extension) : "");
        }

        public NodeRef getFileRef() {
            return fileRef;
        }

        public boolean isGenerated() {
            return generated;
        }

        public String getAccessRestriction() {
            return accessRestriction;
        }

        public void setAccessRestriction(String accessRestriction) {
            this.accessRestriction = accessRestriction;
        }

        public String getAccessRestrictionReason() {
            return accessRestrictionReason;
        }

        public void setAccessRestrictionReason(String accessRestrictionReason) {
            this.accessRestrictionReason = accessRestrictionReason;
        }

        public String getAccessRestrictionEndDesc() {
            return accessRestrictionEndDesc;
        }

        public void setAccessRestrictionEndDesc(String accessRestrictionEndDesc) {
            this.accessRestrictionEndDesc = accessRestrictionEndDesc;
        }

        public Date getAccessRestrictionBeginDate() {
            return accessRestrictionBeginDate;
        }

        public void setAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
            this.accessRestrictionBeginDate = accessRestrictionBeginDate;
        }

        public Date getAccessRestrictionEndDate() {
            return accessRestrictionEndDate;
        }

        public void setAccessRestrictionEndDate(Date accessRestrictionEndDate) {
            this.accessRestrictionEndDate = accessRestrictionEndDate;
        }

        public boolean isPublicAccess() {
            return !AccessRestriction.AK.getValueName().equals(getAccessRestriction());
        }

        public List<DataItem> getDataItems() {
            return ddocItems != null ? ddocItems.getDataItems() : Collections.<DataItem>emptyList();
        }

        public boolean isDigiDoc(){
            return FilenameUtil.isDigiDocFile(getFileNameWithExtension()) && parentRef == null && !isDigiDocItem();
        }

        public boolean isAccessRestrictionSet() {
            if (CollectionUtils.isNotEmpty(getDataItems())) {
                for (DataItem item : getDataItems()) {
                    if (StringUtils.isNotBlank(item.getAccessRestriction())) {
                        return true;
                    }
                }
            }
            return StringUtils.isNotBlank(getAccessRestriction());
        }

        @Override
        public int compareTo(FileVO o) {
            Long long1 = Long.valueOf(fileOrderInList);
            Long long2 = Long.valueOf(o.fileOrderInList);
            if (long1 < long2) {
                return -1;
            } else if (long1 > long2) {
                return 1;
            }
            return 0;
        }

        public SignatureItemsAndDataItems getDdocItems() {
            return ddocItems;
        }

        public void setDdocItems(SignatureItemsAndDataItems ddocItems) {
            this.ddocItems = ddocItems;
        }

        public boolean isDigiDocItem() {
            return digiDocItem;
        }

        public void setDigiDocItem(boolean digiDocItem) {
            this.digiDocItem = digiDocItem;
        }

        public String getDigiDocItemUrl() {
            return isDigiDocItem() ? digiDocItemUrl : null;
        }

        public void setDigiDocItemUrl(String digiDocItemUrl) {
            this.digiDocItemUrl = digiDocItemUrl;
        }

        public NodeRef getParentRef() {
            return parentRef;
        }

        public void setParentRef(NodeRef parentRef) {
            this.parentRef = parentRef;
        }

        public String getAccessRestrictionSelected() {
            return accessRestrictionSelected;
        }

        public void setAccessRestrictionSelected(String accessRestrictionSelected) {
            this.accessRestrictionSelected = accessRestrictionSelected;
        }

        public String getParentOrderInList() {
            return parentOrderInList;
        }

        public void setParentOrderInList(String parentOrderInList) {
            this.parentOrderInList = parentOrderInList;
        }
    }

}
