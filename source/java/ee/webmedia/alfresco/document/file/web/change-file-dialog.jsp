<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="/WEB-INF/alfresco.tld" prefix="a"%>
<%@ taglib uri="/WEB-INF/repo.tld" prefix="r"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/wm.tld" prefix="wm"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>


<jsp:include page="/WEB-INF/classes/ee/webmedia/alfresco/document/file/web/file-block.jsp" />


<a:panel styleClass="column panel-100" id="file-change" label="#{msg.file_change_blockname}">

   <a:richList id="fileChangeList" viewMode="details" value="#{ChangeFileDialog.files}" var="r" 
      rowStyleClass="recordSetRow" altRowStyleClass="recordSetRowAlt" width="100%">

      <a:column id="fileOrder" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col0-header" value="#{msg.file_order}" styleClass="header" />
         </f:facet>
         <h:inputText id="order" value="#{r.fileOrderInList}" styleClass="tiny" rendered="#{!r.digiDocItem}"/>
      </a:column>

      <a:column id="fileName" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col1-header" value="#{msg.name}" styleClass="header" />
         </f:facet>
         <h:inputTextarea id="name" value="#{r.fileNameWithoutExtension}" styleClass="focus expand19-200" rendered="#{!r.digiDocItem}"/>
         <h:outputText id="name-text" value="#{r.fileNameWithExtension}" styleClass="expand19-200" rendered="#{r.digiDocItem}"/>
      </a:column>

      <a:column id="fileAccessRestriction" rendered="#{ChangeFileDialog.allowFileAccessRestriction}" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col2-header" value="#{msg.file_access_restriction}" styleClass="header" />
         </f:facet>
         <h:selectOneMenu id="accessRestriction-select" value="#{r.accessRestriction}" rendered="#{!r.digiDoc}"
                          valueChangeListener="#{ChangeFileDialog.accessRestrictionChanged}" onchange="submit()">
            <f:selectItems id="accessRestriction-select-items" value="#{ChangeFileDialog.accessResctrictions}" />
         </h:selectOneMenu>
      </a:column>

      <a:column id="fileAccessRestrictionReasonSelect" rendered="#{ChangeFileDialog.allowFileAccessRestriction}" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col3-header" value="#{msg.file_access_restriction_reason}" styleClass="header" />
         </f:facet>
         <h:selectOneMenu id="accessRestrictionReason-select" styleClass="limited-width" value="#{r.accessRestrictionSelected}" rendered="#{!r.publicAccess}"
                          valueChangeListener="#{ChangeFileDialog.accessRestrictionReasonChanged}" onchange="submit()">
            <f:selectItems id="accessRestrictionReason-select-items" value="#{ChangeFileDialog.accessResctrictionReasons}" />
         </h:selectOneMenu>
      </a:column>

      <a:column id="fileAccessRestrictionReason" rendered="#{ChangeFileDialog.allowFileAccessRestriction}" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col4-header" value="" styleClass="header" />
         </f:facet>
         <h:inputText id="accessRestrictionReason" value="#{r.accessRestrictionReason}" styleClass="focus limited-width-120" rendered="#{!r.publicAccess}"/>
      </a:column>

      <a:column id="fileAccessRestrictionBeginDate" rendered="#{ChangeFileDialog.allowFileAccessRestriction}" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col5-header" value="#{msg.file_access_restriction_begin_date}" styleClass="header" />
         </f:facet>
         <h:inputText id="accessRestrictionBeginDate" value="#{r.accessRestrictionBeginDate}" styleClass="beginDate date" rendered="#{!r.publicAccess}">
            <f:converter converterId="ee.webmedia.alfresco.common.propertysheet.datepicker.DatePickerConverter"/>
         </h:inputText>
      </a:column>

      <a:column id="fileAccessRestrictionEndDate" rendered="#{ChangeFileDialog.allowFileAccessRestriction}" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col6-header" value="#{msg.file_access_restriction_end_date}" styleClass="header" />
         </f:facet>
         <h:inputText id="accessRestrictionEndDate" value="#{r.accessRestrictionEndDate}" styleClass="date endDate" rendered="#{!r.publicAccess}">
            <f:converter converterId="ee.webmedia.alfresco.common.propertysheet.datepicker.DatePickerConverter"/>
         </h:inputText>
      </a:column>

      <a:column id="fileAccessRestrictionEndDesc" rendered="#{ChangeFileDialog.allowFileAccessRestriction}" styleClass="limited-width">
         <f:facet name="header">
            <h:outputText id="col7-header" value="#{msg.file_access_restriction_end_date_desc}" styleClass="header" />
         </f:facet>
         <h:inputText id="accessRestrictionEndDesc" value="#{r.accessRestrictionEndDesc}" styleClass="focus limited-width-120" rendered="#{!r.publicAccess}"/>
      </a:column>

   </a:richList>

</a:panel>
