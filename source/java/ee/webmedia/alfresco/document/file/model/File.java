package ee.webmedia.alfresco.document.file.model;

import static ee.webmedia.alfresco.common.web.BeanHelper.getParametersService;
import static ee.webmedia.alfresco.document.file.model.FileModel.Props.ACTIVE;
import static ee.webmedia.alfresco.document.file.model.FileModel.Props.DISPLAY_NAME;

import java.io.Serializable;
import java.util.*;

import ee.smit.digisign.sign.model.certificates.timestamp.TimeStampTokenShort;
import ee.webmedia.alfresco.classificator.enums.AccessRestriction;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.utils.FilenameUtil;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.mobile.alfresco.workflow.model.SignatureData;
import ee.webmedia.mobile.alfresco.workflow.model.SignatureInfo;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.web.scripts.FileTypeImageUtils;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.FastDateFormat;

import ee.webmedia.alfresco.common.service.IClonable;
import ee.webmedia.alfresco.dvk.model.DvkModel;
import ee.webmedia.alfresco.privilege.model.Privilege;
import ee.webmedia.alfresco.signature.model.DataItem;
import ee.webmedia.alfresco.signature.model.SignatureItem;
import ee.webmedia.alfresco.signature.model.SignatureItemsAndDataItems;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

public class File implements Serializable, IClonable<File>, Comparable<File> {
    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(File.class);

    private static final long serialVersionUID = 1L;

    private String name;
    private String displayName;
    private String downloadUrl;
    private String readOnlyUrl;
    private String creator;
    private String modifier;
    private String encoding;
    private String mimeType;
    private String comment;
    private long size;
    private Date created;
    private Date modified;
    private NodeRef nodeRef;
    private Node node;
    private boolean digiDocItem;
    private boolean digiDocContainer;
    private boolean bdoc;
    private boolean versionable;
    private SignatureItemsAndDataItems ddocItems;
    private boolean generated;
    private boolean active;
    private boolean isTransformableToPdf;
    private boolean convertToPdfIfSigned; //
    private long nrOfChildren; // used to show childCount if the file represents folder
    private boolean isPdf;
    private String activeLockOwner;
    private boolean decContainer;
    private Long fileOrderInList;
    public static FastDateFormat dateFormat = FastDateFormat.getInstance("dd.MM.yyyy HH:mm");
    public static FastDateFormat simpleDateFormat = FastDateFormat.getInstance("dd.MM.yyyy");
    public Boolean viewDocumentFilesPermission;
    private String accessRestriction;
    private String accessRestrictionReason;
    private String accessRestrictionEndDesc;
    private Date accessRestrictionBeginDate;
    private Date accessRestrictionEndDate;
    private ArrayList<String> digidocAccessRestrictions = new ArrayList<>();
    private ArrayList<String> digidocAccessRestrictionReasons = new ArrayList<>();
    private ArrayList<String> digidocAccessRestrictionEndDescs = new ArrayList<>();
    private ArrayList<Date> digidocAccessRestrictionBeginDates = new ArrayList<>();
    private ArrayList<Date> digidocAccessRestrictionEndDates = new ArrayList<>();
    private String fileType;
    private List<SignatureInfo> signatureInfos;
    private List<SignatureData> signatureData;

    public File() {
    }

    public File(FileInfo fileInfo) {
        name = fileInfo.getName();
        log.debug("Filename: " + name);

        Map<QName, Serializable> fileProps = fileInfo.getProperties();
        displayName = (fileProps.get(DISPLAY_NAME) == null) ? name : fileProps.get(DISPLAY_NAME).toString();
        log.debug("Filename: displayName: " + displayName);

        fileType = displayName.substring(displayName.lastIndexOf(".") + 1, displayName.length()); // FilenameUtils.getExtension(name).toLowerCase();
        log.debug("Filename: filetype: " + fileType);
        if (fileType != null) {
            if (ee.smit.common.files.FilenameUtil.isDigiDocFile(displayName)) {
                fileType = "digidoc";
            }
        }



        created = fileInfo.getCreatedDate();
        modified = fileInfo.getModifiedDate();
        // fileInfo.getContentData() != null is here for testing purposes only; normally fileInfo.getContentData() shouldn't be null
        if (!fileInfo.isFolder() && fileInfo.getContentData() != null) {
            encoding = fileInfo.getContentData().getEncoding();
            mimeType = fileInfo.getContentData().getMimetype();
            size = fileInfo.getContentData().getSize();
        }
        nodeRef = fileInfo.getNodeRef();
        node = new Node(nodeRef);
        digiDocItem = false;
        versionable = fileProps.get(ContentModel.PROP_VERSION_LABEL) != null;
        creator = "";
        modifier = "";
        generated = fileProps.get(FileModel.Props.GENERATED_FROM_TEMPLATE) != null || fileProps.get(FileModel.Props.GENERATION_TYPE) != null;
        active = (fileProps.get(ACTIVE) == null) ? true : Boolean.parseBoolean(fileProps.get(ACTIVE).toString());
        convertToPdfIfSigned = Boolean.TRUE.equals(fileProps.get(FileModel.Props.CONVERT_TO_PDF_IF_SIGNED));
        decContainer = fileProps.containsKey(DvkModel.Props.DVK_ID);
        fileOrderInList = (Long) fileProps.get(FileModel.Props.FILE_ORDER_IN_LIST);
        accessRestriction = (String) fileProps.get(FileModel.Props.ACCESS_RESTRICTION);
        accessRestrictionReason = (String) fileProps.get(FileModel.Props.ACCESS_RESTRICTION_REASON);
        accessRestrictionEndDesc = (String) fileProps.get(FileModel.Props.ACCESS_RESTRICTION_END_DESC);
        accessRestrictionBeginDate = (Date) fileProps.get(FileModel.Props.ACCESS_RESTRICTION_BEGIN_DATE);
        accessRestrictionEndDate = (Date) fileProps.get(FileModel.Props.ACCESS_RESTRICTION_END_DATE);
        digidocAccessRestrictions = (ArrayList<String>) fileProps.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTIONS);
        digidocAccessRestrictionReasons = (ArrayList<String>) fileProps.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_REASONS);
        digidocAccessRestrictionEndDescs = (ArrayList<String>) fileProps.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DESCS);
        digidocAccessRestrictionBeginDates = (ArrayList<Date>) fileProps.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_BEGIN_DATES);
        digidocAccessRestrictionEndDates = (ArrayList<Date>) fileProps.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DATES);
    }

    public boolean isViewDocumentFilesPermission() {
        if (viewDocumentFilesPermission == null) {
            viewDocumentFilesPermission = node != null && node.hasPermission(Privilege.VIEW_DOCUMENT_FILES);
        }
        return viewDocumentFilesPermission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String url) {
        downloadUrl = url;
    }

    public String getReadOnlyUrl() {
        return readOnlyUrl;
    }

    public void setReadOnlyUrl(String readOnlyUrl) {
        this.readOnlyUrl = readOnlyUrl;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedTimeStr() {
        return getCreated() != null ? dateFormat.format(getCreated()) : "";
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public NodeRef getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(NodeRef nodeRef) {
        this.nodeRef = nodeRef;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * @return true if this item/file is contained in digiDoc container
     */
    public boolean isDigiDocItem() {
        return digiDocItem;
    }

    public void setDigiDocItem(boolean digiDocItem) {
        this.digiDocItem = digiDocItem;
    }

    /**
     * @return true, if this document is in digiDocItem format and may contain other documents
     */
    public boolean isDigiDocContainer() {
        return digiDocContainer;
    }

    public void setDigiDocContainer(boolean digiDocContainer) {
        this.digiDocContainer = digiDocContainer;
    }

    public NodeRef getCompoundWorkflowRef() {
        return (NodeRef) getNode().getProperties().get(FileModel.Props.COMPOUND_WORKFLOW);
    }

    public NodeRef getGeneratedFileRef() {
        return (NodeRef) getNode().getProperties().get(FileModel.Props.GENERATED_FILE);
    }

    public boolean isVersionable() {
        return versionable;
    }

    public void setVersionable(boolean versionable) {
        this.versionable = versionable;
    }

    public void setDdocItems(SignatureItemsAndDataItems ddocItems) {
        this.ddocItems = ddocItems;
    }

    public List<SignatureItem> getSignatureItems() {
        return ddocItems != null ? ddocItems.getSignatureItems() : Collections.<SignatureItem>emptyList();
    }

    public List<DataItem> getDataItems() {
        return ddocItems != null ? ddocItems.getDataItems() : null;
    }

    public List<TimeStampTokenShort> getTimestampTokens(){
        return ddocItems.getTimeStampTokens();
    }
    /**
     * @return true if file has been generated by system and mark has been set
     */
    public boolean isGenerated() {
        return generated;
    }

    public void setGenerated(boolean generated) {
        this.generated = generated;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isTransformableToPdf() {
        return isTransformableToPdf;
    }

    public void setTransformableToPdf(boolean isTransformableToPdf) {
        this.isTransformableToPdf = isTransformableToPdf;
    }

    public void setNrOfChildren(long nrOfChildren) {
        this.nrOfChildren = nrOfChildren;
    }

    public long getNrOfChildren() {
        return nrOfChildren;
    }

    /**
     * Used to specify icon.
     */
    public String getFileType16() {
        return FileTypeImageUtils.getFileTypeImage(getName(), true);
    }

    /**
     * Used as a parameter.
     */
    public String getId() {
        return nodeRef.getId();
    }

    /* JSP is evil! */
    public boolean isActiveAndNotDigiDoc() {
        return isActive() && !isDigiDocItem();
    }

    public boolean isActiveDigiDoc() {
        return isActive() && isDigiDocItem();
    }

    public boolean isDigidocSignatureItems(){
        return isActiveDigiDoc() && ddocItems != null && ddocItems.getSignatureItems() != null && ddocItems.getSignatureItems().size() > 0;
    }

    public boolean isDigidocTimestampTokenItems(){
        return isActiveDigiDoc() && ddocItems != null && ddocItems.getTimeStampTokens().size() > 0 ;
    }

    public boolean isDigidocTimestampTokenItemsNullSafe(){
        return isActiveDigiDoc() && ddocItems != null && ddocItems.getTimeStampTokens() != null && ddocItems.getTimeStampTokens().size() > 0 ;
    }


    public boolean isNotActiveAndDigiDoc() {
        return !isActive() && isDigiDocItem();
    }

    public boolean isNotActiveAndNotDigiDoc() {
        return !isActive() && !isDigiDocItem();
    }

    public void setPdf(boolean isPdf) {
        this.isPdf = isPdf;
    }

    public boolean isPdf() {
        return isPdf;
    }

    public boolean isDecContainer() {
        return decContainer;
    }

    public String getActiveLockOwner() {
        return activeLockOwner;
    }

    public void setActiveLockOwner(String activeLockOwner) {
        this.activeLockOwner = activeLockOwner;
    }

    @Override
    public File clone() {
        File file;
        try {
            file = (File) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        file.setCreated((Date) created.clone());
        file.setModified((Date) modified.clone());
        file.setNode(new Node(nodeRef));
        return file;
    }

    public void setConvertToPdfIfSigned(boolean isConvertToPdfIfSigned) {
        convertToPdfIfSigned = isConvertToPdfIfSigned;
    }

    public boolean isConvertToPdfIfSigned() {
        return convertToPdfIfSigned;
    }

    public boolean getConvertToPdfIfSignedFromProps() {
        Boolean convertToPdf = node != null ? Boolean.TRUE.equals(node.getProperties().get(FileModel.Props.CONVERT_TO_PDF_IF_SIGNED)) : false;
        return convertToPdf != null ? convertToPdf : false;
    }

    public boolean isBdoc() {
        return bdoc;
    }

    public void setBdoc(boolean isBdoc) {
        bdoc = isBdoc;
    }

    public Long getFileOrderInList() {
        return fileOrderInList;
    }

    public void setFileOrderInList(Long fileOrderInList) {
        this.fileOrderInList = fileOrderInList;
    }

    @Override
    public int compareTo(File o) {
        if (fileOrderInList < o.fileOrderInList) {
            return -1;
        } else if (fileOrderInList > o.fileOrderInList) {
            return 1;
        }
        return orderDigidocSubitemAndParentContainer(o);
    }

    private int orderDigidocSubitemAndParentContainer(File o) {
        if (digiDocItem == o.digiDocItem) {
            return 0;
        }
        if (Boolean.TRUE.equals(digiDocItem)) {
            return 1;
        }
        return -1;
    }

    public boolean isOpenAccessRestriction() {
        return StringUtils.isBlank(accessRestriction) && !isDigiDoc()
                || AccessRestriction.OPEN.getValueName().equals(getAccessRestriction())
                || hasOpenAccessRestrictionDataItem();
    }

    public boolean hasOpenAccessRestrictionDataItem() {
        if (CollectionUtils.isNotEmpty(getDataItems())) {
            for (DataItem item : getDataItems()) {
                if (StringUtils.isBlank(accessRestriction)
                        || AccessRestriction.OPEN.getValueName().equals(item.getAccessRestriction())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isDigiDoc(){
        return !isDigiDocItem() && FilenameUtil.isDigiDocFile(getName());
    }

    public boolean isAccessRestrictionSet() {
        if (ddocItems != null && CollectionUtils.isNotEmpty(getDataItems())) {
            for (DataItem item : getDataItems()) {
                if (StringUtils.isNotBlank(item.getAccessRestriction())) {
                    return true;
                }
            }
        }
        return StringUtils.isNotBlank(getAccessRestriction());
    }

    public String getAccessRestriction() {
        return accessRestriction;
    }

    public void setAccessRestriction(String accessRestriction) {
        this.accessRestriction = accessRestriction;
    }

    public String getAccessRestrictionReason() {
        return accessRestrictionReason;
    }

    public void setAccessRestrictionReason(String accessRestrictionReason) {
        this.accessRestrictionReason = accessRestrictionReason;
    }

    public String getAccessRestrictionEndDesc() {
        return accessRestrictionEndDesc;
    }

    public void setAccessRestrictionEndDesc(String accessRestrictionEndDesc) {
        this.accessRestrictionEndDesc = accessRestrictionEndDesc;
    }

    public Date getAccessRestrictionBeginDate() {
        return accessRestrictionBeginDate;
    }

    public void setAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
        this.accessRestrictionBeginDate = accessRestrictionBeginDate;
    }

    public Date getAccessRestrictionEndDate() {
        return accessRestrictionEndDate;
    }

    public void setAccessRestrictionEndDate(Date accessRestrictionEndDate) {
        this.accessRestrictionEndDate = accessRestrictionEndDate;
    }

    public String getTitleString() {
        List<String> values = new ArrayList<>();
        if (StringUtils.isNotBlank(getAccessRestrictionReason())) {
            values.add(getAccessRestrictionReason());
        }
        if (getAccessRestrictionEndDate() != null) {
            values.add(simpleDateFormat.format(getAccessRestrictionEndDate()));
        }
        if (StringUtils.isNotBlank(getAccessRestrictionEndDesc())) {
            values.add(getAccessRestrictionEndDesc());
        }
        return StringUtils.join(values, ", ");
    }

    public ArrayList<String> getDigidocAccessRestrictions() {
        return digidocAccessRestrictions;
    }

    public void setDigidocAccessRestrictions(ArrayList<String> digidocAccessRestrictions) {
        this.digidocAccessRestrictions = digidocAccessRestrictions;
    }

    public ArrayList<String> getDigidocAccessRestrictionReasons() {
        return digidocAccessRestrictionReasons;
    }

    public void setDigidocAccessRestrictionReasons(ArrayList<String> digidocAccessRestrictionReasons) {
        this.digidocAccessRestrictionReasons = digidocAccessRestrictionReasons;
    }

    public ArrayList<String> getDigidocAccessRestrictionEndDescs() {
        return digidocAccessRestrictionEndDescs;
    }

    public void setDigidocAccessRestrictionEndDescs(ArrayList<String> digidocAccessRestrictionEndDescs) {
        this.digidocAccessRestrictionEndDescs = digidocAccessRestrictionEndDescs;
    }

    public ArrayList<Date> getDigidocAccessRestrictionBeginDates() {
        return digidocAccessRestrictionBeginDates;
    }

    public void setDigidocAccessRestrictionBeginDates(ArrayList<Date> digidocAccessRestrictionBeginDates) {
        this.digidocAccessRestrictionBeginDates = digidocAccessRestrictionBeginDates;
    }

    public ArrayList<Date> getDigidocAccessRestrictionEndDates() {
        return digidocAccessRestrictionEndDates;
    }

    public void setDigidocAccessRestrictionEndDates(ArrayList<Date> digidocAccessRestrictionEndDates) {
        this.digidocAccessRestrictionEndDates = digidocAccessRestrictionEndDates;
    }

    public String getAdrAccessRestrictionString() {
        return isDigiDoc() && !isDigiDocItem() ? getDigidocAdrAccessRestriction() : getAdrAccessRestriction();
    }

    private String getAdrAccessRestriction() {
        String accessRestriction = StringUtils.defaultString(!isAllowFileAccessRestriction() || StringUtils.isNotBlank(getAccessRestriction()) ? getAccessRestriction() : AccessRestriction.OPEN.getValueName());
        StringBuilder result = new StringBuilder(accessRestriction);
        if (AccessRestriction.AK.getValueName().equals(getAccessRestriction())) {
            result.append(" (")
                    .append(getAccessRestrictionReason())
                    .append(", ")
                    .append(simpleDateFormat.format(getAccessRestrictionBeginDate()))
                    .append(" - ")
                    .append(simpleDateFormat.format(getAccessRestrictionEndDate()))
                    .append(")");
        }
        return result.toString();
    }

    private String getDigidocAdrAccessRestriction() {
        Date signatureTime = null;
        if (CollectionUtils.isNotEmpty(getSignatureItems())) {
            for (SignatureItem signatureItem : getSignatureItems()) {
                Date time = signatureItem.getSigningTime();
                if (time != null && (signatureTime == null || time.compareTo(signatureTime) > 0)) {
                    signatureTime = time;
                }
            }
        }
        String time = signatureTime != null ? simpleDateFormat.format(signatureTime) : StringUtils.EMPTY;
        return MessageUtil.getMessage("file_adr_signed", time);
    }

    public boolean isAkAccessRestriction() {
        return AccessRestriction.AK.getValueName().equals(getAccessRestriction());
    }

    private boolean isAllowFileAccessRestriction(){
        String parameter = getParametersService().getStringParameter(Parameters.ALLOW_FILE_ACCESS_RESTRICTION);
        return BooleanUtils.toBoolean(parameter);
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public List<SignatureInfo> getSignatureInfos() {
        return signatureInfos;
    }

    public List<SignatureData> getSignatureData() {
        return signatureData;
    }

    public void setSignatureInfos(List<SignatureInfo> signatureInfos) {
        this.signatureInfos = signatureInfos;
    }

    public void setSignatureData(List<SignatureData> signatureData) {
        this.signatureData = signatureData;
    }
}
