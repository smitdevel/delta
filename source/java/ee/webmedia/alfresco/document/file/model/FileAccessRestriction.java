package ee.webmedia.alfresco.document.file.model;

import java.util.*;

public class FileAccessRestriction {

    private String accessRestriction;
    private String accessRestrictionReason;
    private String accessRestrictionEndDesc;
    private Date accessRestrictionBeginDate;
    private Date accessRestrictionEndDate;
    private ArrayList<String> innerFilesAccessRestrictions;
    private ArrayList<String> innerFilesAccessRestrictionReasons;
    private ArrayList<String> innerFilesAccessRestrictionEndDescs;
    private ArrayList<Date> innerFilesAccessRestrictionBeginDates;
    private ArrayList<Date> innerFilesAccessRestrictionEndDates;

    public FileAccessRestriction() {
        innerFilesAccessRestrictions = new ArrayList<>();
        innerFilesAccessRestrictionReasons = new ArrayList<>();
        innerFilesAccessRestrictionEndDescs = new ArrayList<>();
        innerFilesAccessRestrictionBeginDates = new ArrayList<>();
        innerFilesAccessRestrictionEndDates = new ArrayList<>();
    }

    public String getAccessRestriction() {
        return accessRestriction;
    }

    public void setAccessRestriction(String accessRestriction) {
        this.accessRestriction = accessRestriction;
    }

    public String getAccessRestrictionReason() {
        return accessRestrictionReason;
    }

    public void setAccessRestrictionReason(String accessRestrictionReason) {
        this.accessRestrictionReason = accessRestrictionReason;
    }

    public String getAccessRestrictionEndDesc() {
        return accessRestrictionEndDesc;
    }

    public void setAccessRestrictionEndDesc(String accessRestrictionEndDesc) {
        this.accessRestrictionEndDesc = accessRestrictionEndDesc;
    }

    public Date getAccessRestrictionBeginDate() {
        return accessRestrictionBeginDate;
    }

    public void setAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
        this.accessRestrictionBeginDate = accessRestrictionBeginDate;
    }

    public Date getAccessRestrictionEndDate() {
        return accessRestrictionEndDate;
    }

    public void setAccessRestrictionEndDate(Date accessRestrictionEndDate) {
        this.accessRestrictionEndDate = accessRestrictionEndDate;
    }

    public void addInnerFileAccessRestriction(String accessRestriction) {
        innerFilesAccessRestrictions.add(accessRestriction);
    }

    public void addInnerFileAccessRestrictionReason(String accessRestrictionReason) {
        innerFilesAccessRestrictionReasons.add(accessRestrictionReason);
    }


    public void addInnerFileAccessRestrictionEndDesc(String accessRestrictionEndDesc) {
        innerFilesAccessRestrictionEndDescs.add(accessRestrictionEndDesc);
    }

    public void addInnerFileAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
        innerFilesAccessRestrictionBeginDates.add(accessRestrictionBeginDate);
    }


    public void addInnerFileAccessRestrictionEndDate(Date accessRestrictionEndDate) {
        innerFilesAccessRestrictionEndDates.add(accessRestrictionEndDate);
    }

    public ArrayList<String> getInnerFilesAccessRestrictions() {
        return innerFilesAccessRestrictions;
    }

    public ArrayList<String> getInnerFilesAccessRestrictionReasons() {
        return innerFilesAccessRestrictionReasons;
    }

    public ArrayList<String> getInnerFilesAccessRestrictionEndDescs() {
        return innerFilesAccessRestrictionEndDescs;
    }

    public ArrayList<Date> getInnerFilesAccessRestrictionBeginDates() {
        return innerFilesAccessRestrictionBeginDates;
    }

    public ArrayList<Date> getInnerFilesAccessRestrictionEndDates() {
        return innerFilesAccessRestrictionEndDates;
    }
}
