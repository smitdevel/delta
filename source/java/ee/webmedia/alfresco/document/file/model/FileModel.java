package ee.webmedia.alfresco.document.file.model;

import org.alfresco.service.namespace.QName;

public interface FileModel {
    String URI = "http://alfresco.webmedia.ee/model/file/1.0";

    /**
     * Properties described in alfresco model
     */
    public interface Props {
        QName GENERATED_FROM_TEMPLATE = QName.createQName(URI, "generatedFromTemplate");
        /**
         * property for distinguishing pdf files generated during signing from other generated files that at the moment use {@link #GENERATED} property <br>
         * known values are defined with: {@link GeneratedFileType}
         */
        QName GENERATION_TYPE = QName.createQName(URI, "generationType");
        QName ACTIVE = QName.createQName(URI, "active");
        QName DISPLAY_NAME = QName.createQName(URI, "displayName");
        QName NEW_VERSION_ON_NEXT_SAVE = QName.createQName(URI, "newVersionOnNextSave");
        QName CONVERT_TO_PDF_IF_SIGNED = QName.createQName(URI, "convertToPdfIfSigned");
        QName COMMENT = QName.createQName(URI, "comment");
        QName COMPOUND_WORKFLOW = QName.createQName(URI, "compoundWorkflow");
        /** NodeRef of pdf file that has been generated from this file */
        QName GENERATED_FILE = QName.createQName(URI, "generatedFile");
        QName UPDATE_METADATA_IN_FILES = QName.createQName(URI, "updateMetadataInFiles");
        QName PDF_GENERATED_FROM_FILE = QName.createQName(URI, "pdfGeneratedFromFile");
        QName LOCKED_FILE_NODEREF = QName.createQName(URI, "lockedFileNodeRef");
        QName ACTIVITY_FILE_TYPE = QName.createQName(URI, "activityFileType");
        QName MANUAL_LOCK = QName.createQName(URI, "manualLock");
        QName PREVIOUS_FILE_PARENT = QName.createQName(URI, "previousFileParent");
        QName FILE_ORDER_IN_LIST = QName.createQName(URI, "fileOrderInList");
        QName ACCESS_RESTRICTION = QName.createQName(URI, "accessRestriction");
        QName ACCESS_RESTRICTION_REASON = QName.createQName(URI, "accessRestrictionReason");
        QName ACCESS_RESTRICTION_BEGIN_DATE = QName.createQName(URI, "accessRestrictionBeginDate");
        QName ACCESS_RESTRICTION_END_DATE = QName.createQName(URI, "accessRestrictionEndDate");
        QName ACCESS_RESTRICTION_END_DESC = QName.createQName(URI, "accessRestrictionEndDesc");
        QName DIGIDOC_FILES_ACCESS_RESTRICTIONS= QName.createQName(URI, "digidocFilesAccessRestrictions");
        QName DIGIDOC_FILES_ACCESS_RESTRICTION_REASONS= QName.createQName(URI, "digidocFilesAccessRestrictionReasons");
        QName DIGIDOC_FILES_ACCESS_RESTRICTION_BEGIN_DATES= QName.createQName(URI, "digidocFilesAccessRestrictionBeginDates");
        QName DIGIDOC_FILES_ACCESS_RESTRICTION_END_DATES= QName.createQName(URI, "digidocFilesAccessRestrictionEndDates");
        QName DIGIDOC_FILES_ACCESS_RESTRICTION_END_DESCS= QName.createQName(URI, "digidocFilesAccessRestrictionEndDescs");
    }
}
