package ee.webmedia.alfresco.document.file.model;

import org.alfresco.service.cmr.repository.NodeRef;

import java.util.Date;

public class SimpleFileWithOrder extends SimpleFile {

    private static final long serialVersionUID = 1L;

    private final Long fileOrderInList;
    private final NodeRef fileRef;
    private final boolean generated;
    private boolean active;
    private String accessRestriction;
    private String accessRestrictionReason;
    private String accessRestrictionEndDesc;
    private Date accessRestrictionBeginDate;
    private Date accessRestrictionEndDate;


    public SimpleFileWithOrder(String displayName, String readOnlyUrl, Long fileOrderInList, NodeRef fileRef, boolean generated, File file) {
        super(displayName, readOnlyUrl, fileRef, file);
        this.fileOrderInList = fileOrderInList;
        this.fileRef = fileRef;
        this.generated = generated;
    }

    public Long getFileOrderInList() {
        return fileOrderInList;
    }

    public NodeRef getFileRef() {
        return fileRef;
    }

    public boolean isGenerated() {
        return generated;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAccessRestriction() {
        return accessRestriction;
    }

    public void setAccessRestriction(String accessRestriction) {
        this.accessRestriction = accessRestriction;
    }

    public String getAccessRestrictionReason() {
        return accessRestrictionReason;
    }

    public void setAccessRestrictionReason(String accessRestrictionReason) {
        this.accessRestrictionReason = accessRestrictionReason;
    }

    public String getAccessRestrictionEndDesc() {
        return accessRestrictionEndDesc;
    }

    public void setAccessRestrictionEndDesc(String accessRestrictionEndDesc) {
        this.accessRestrictionEndDesc = accessRestrictionEndDesc;
    }

    public Date getAccessRestrictionBeginDate() {
        return accessRestrictionBeginDate;
    }

    public void setAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
        this.accessRestrictionBeginDate = accessRestrictionBeginDate;
    }

    public Date getAccessRestrictionEndDate() {
        return accessRestrictionEndDate;
    }

    public void setAccessRestrictionEndDate(Date accessRestrictionEndDate) {
        this.accessRestrictionEndDate = accessRestrictionEndDate;
    }
}
