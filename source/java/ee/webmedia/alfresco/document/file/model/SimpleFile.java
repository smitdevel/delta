package ee.webmedia.alfresco.document.file.model;

import java.io.Serializable;
import java.util.List;

import ee.webmedia.alfresco.utils.FilenameUtil;
import ee.webmedia.mobile.alfresco.workflow.model.SignatureData;
import ee.webmedia.mobile.alfresco.workflow.model.SignatureInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.io.FilenameUtils;

public class SimpleFile implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String DIGIDOC_IMAGE_PATH = "/images/icons/ddoc_sign_small.gif";
    public static final String NON_DIGIDOC_IMAGE_PATH = "/images/icons/attachment.gif";
    public static final String NON_DIGIDOC_IMAGE_PATH_RED = "/images/icons/attachment-red.png";

    protected final String displayName;
    protected final String imagePath;
    protected final String readOnlyUrl;
    protected String fileType;
    protected final NodeRef nodeRef;
    protected final File file;

    private long size;
    private Boolean viewDocumentFilesPermission;

    private List<SignatureInfo> signatureInfos;
    private List<SignatureData> signatureData;

    public SimpleFile(String displayName, String readOnlyUrl, NodeRef nodeRef, File file) {
        this.displayName = displayName;
        imagePath = FilenameUtil.isDigiDocFile(displayName) ? DIGIDOC_IMAGE_PATH : NON_DIGIDOC_IMAGE_PATH;
        this.readOnlyUrl = readOnlyUrl;
        this.nodeRef = nodeRef;

        fileType = FilenameUtils.getExtension(displayName).toLowerCase();
        if (fileType != null) {
            if (ee.smit.common.files.FilenameUtil.isDigiDocFile(displayName)) {
                fileType = "digidoc";
            }
        }

        this.file = file;
        this.size = file.getSize();
        this.viewDocumentFilesPermission = file.isViewDocumentFilesPermission();
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getReadOnlyUrl() {
        return readOnlyUrl;
    }

    public File getFile() {
        return file;
    }

    public long getSize() {
        return size;
    }

    public Boolean getViewDocumentFilesPermission() {
        return viewDocumentFilesPermission;
    }

    public List<SignatureInfo> getSignatureInfos() {
        return signatureInfos;
    }

    public List<SignatureData> getSignatureData() {
        return signatureData;
    }

    public void setSignatureInfos(List<SignatureInfo> signatureInfos) {
        this.signatureInfos = signatureInfos;
    }

    public void setSignatureData(List<SignatureData> signatureData) {
        this.signatureData = signatureData;
    }

    public NodeRef getNodeRef() {
        return nodeRef;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setViewDocumentFilesPermission(Boolean viewDocumentFilesPermission) {
        this.viewDocumentFilesPermission = viewDocumentFilesPermission;
    }
}
