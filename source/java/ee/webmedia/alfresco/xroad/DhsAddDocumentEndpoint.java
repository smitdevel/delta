package ee.webmedia.alfresco.xroad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nortal.jroad.annotation.XTeeService;
import com.nortal.jroad.model.XTeeMessage;

import ee.webmedia.alfresco.dhsxroad.ws.DhsAddDocument;
import ee.webmedia.alfresco.dhsxroad.ws.DhsAddDocumentResponse;
import ee.webmedia.alfresco.dhsxroad.ws.DhsAddDocumentResponseType;
import ee.webmedia.alfresco.xroad.core.AbstractXRoadBaseEndpoint;
import ee.webmedia.alfresco.xroad.core.XRoadHeader;

@Component
@XTeeService(name = "DhsAddDocument", version = "v1", title = "DHS add document service")
public class DhsAddDocumentEndpoint extends AbstractXRoadBaseEndpoint<DhsAddDocument> {
	
	@Autowired
	private DhsAddDocumentService dhsAddDocumentService;
	
	@Override
	public Class<DhsAddDocument> getParingKehaClass() {
		return DhsAddDocument.class;
	}

	@Override
	public void invoke(final XTeeMessage<DhsAddDocument> request, final XTeeMessage<Object> response,
			XRoadHeader header) {
		DhsAddDocument requestBean = request.getContent();
		DhsAddDocumentResponse responseContent;
		
		responseContent = invokeInternal(requestBean, header);
		response.setContent(responseContent);
	}

	private DhsAddDocumentResponse invokeInternal(DhsAddDocument requestBean, XRoadHeader header) {
		DhsAddDocumentResponseType responseType = dhsAddDocumentService.executeSaveDocumentInTransaction(requestBean.getRequest());

		return createResponse(responseType);
	}

	

	
	
	private static DhsAddDocumentResponse createResponse(DhsAddDocumentResponseType responseType) {
		DhsAddDocumentResponse response = new DhsAddDocumentResponse();
		response.setResponse(responseType);
		return response;
	}


	

}
