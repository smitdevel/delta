package ee.webmedia.alfresco.xroad.core;

import org.apache.commons.collections.CollectionUtils;

import javax.xml.namespace.QName;
import java.io.Serializable;
import java.util.*;

@SuppressWarnings("serial")
public class XRoadHeader implements Serializable {

  public static final String XROAD_NS_URI = "http://x-road.eu/xsd/xroad.xsd";
  public static final String XROAD_ID_NS_URI = "http://x-road.eu/xsd/identifiers";
  public static final String XROAD_REPR_NS_URI = "http://x-road.eu/xsd/representation.xsd";

  public static final QName CLIENT = new QName(XROAD_NS_URI, "client");
  public static final QName SERVICE = new QName(XROAD_NS_URI, "service");
  public static final QName USER_ID = new QName(XROAD_NS_URI, "userId");
  public static final QName ID = new QName(XROAD_NS_URI, "id");
  public static final QName REPRESENTED_PARTY = new QName(XROAD_REPR_NS_URI, "representedParty");
  public static final QName PROTOCOL_VERSION = new QName(XROAD_NS_URI, "protocolVersion");

  private List<XRoadHeaderElement> elements = new ArrayList<>();

  /**
   * Get Header element by QNames, the ordering of the QNames is important 
   * as the levels are searched in the provided order
   *
   * e.g.
   *   <xroad:client id:objectType="SUBSYSTEM">
   *        <id:xRoadInstance>ee-dev</id:xRoadInstance>
   *        <id:memberClass>GOV</id:memberClass>
   *        <id:memberCode>10117341</id:memberCode>
   *        <id:userId>43009090226</id:userId>
   *        <id:subsystemCode>generic-consumer</id:subsystemCode>
   *    </xroad:client>
   *
   *   Getting memberCode would require the path with 2 QNames
   *   First one for the xroad:client element and second one for id:memberCode element
   *
   * @param names
   * @return
   */
  public XRoadHeaderElement getByQName(QName...names) {
    return getByQName(this.elements, names);
  }

  private XRoadHeaderElement getByQName(List<XRoadHeaderElement> elements, QName...names) {
    for (XRoadHeaderElement element : elements) {
      if (names == null) {
        return element;
      }
      if (element.getQName().equals(names[0])) {
        if (element.getChildren() == null || element.getChildren().isEmpty() || names.length == 1) {
          return element;
        }
        return getByQName(element.getChildren(), Arrays.copyOfRange(names, 1, names.length));
      }
    }
    return null;
  }
  
  public String getFirstValue(QName name) {
    return getAnyFirstValue(this.elements, name);
  }
  
  public String getAnyFirstValue(List<XRoadHeaderElement> elements, QName name) {
    for (XRoadHeaderElement element : elements) {
      if (element.getQName().equals(name)) {
        return element.getValue();
      } else if (CollectionUtils.isNotEmpty(element.getChildren())) {
        String value =  getAnyFirstValue(element.getChildren(), name);
        if (value != null) {
          return value;
        }
      }
    }
    return null;
  }

  public void addHeaderElement(XRoadHeaderElement element) {
    elements.add(element);
  }


  /**
   * Gets all the elements with values.
   * @return
   */
  @Deprecated
  public Map<QName, String> getElemendid() {
    Map<QName, String> oldElementList = new HashMap<>();
    for (XRoadHeaderElement he: elements) {
    	oldElementList.putAll(getElementValues(he));
    }
    return oldElementList;
  }

  private Map<QName, String> getElementValues(XRoadHeaderElement headerElement) {
    Map<QName, String> elementList = new HashMap<>();
    if (headerElement.hasChildren()) {
		for (XRoadHeaderElement he: headerElement.getChildren()) {
			elementList.putAll(getElementValues(he));
	    }
    } else {
      elementList.put(headerElement.getQName(), headerElement.getValue());
    }
    return elementList;
  }

  @Deprecated
  public void setElemendid(Map<QName, String> elements) {
	for (QName key: elements.keySet()) {
		String value = elements.get(key);
		this.elements.add(new XRoadHeaderElement(key, value));
    }
  }

  @Deprecated
  public void addElement(QName qname, String value) {
    this.elements.add(new XRoadHeaderElement(qname, value));
  }
}
