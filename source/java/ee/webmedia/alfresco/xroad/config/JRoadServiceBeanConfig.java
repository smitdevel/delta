package ee.webmedia.alfresco.xroad.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;

@Configuration
@ComponentScan(basePackages = {"com.nortal.jroad","ee.webmedia.alfresco.xroad"})
public class JRoadServiceBeanConfig {

  @Bean
  public String contextPath() {
    return "ee.webmedia.alfresco.dhsxroad.ws";
  }

  @Bean
  public String xteeDatabase() {
    return "dhs";
  }

  @Bean
  public Wsdl11Definition dhs() {
    SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
    wsdl11Definition.setWsdl(new ClassPathResource("wsdl/dhsAddDocument.wsdl"));

    return wsdl11Definition;
  }
}
