package ee.webmedia.alfresco.xroad;

public class ImportValidationException extends Exception {

    private static final long serialVersionUID = 1L;

    private DhsAddDocumentError errorCode;

    public ImportValidationException(String errorMsg, DhsAddDocumentError errorCode) {
        super(errorMsg);
        this.errorCode = errorCode;
    }
    
    public DhsAddDocumentError getErrorCode() {
    	return errorCode;
    }

 
}
