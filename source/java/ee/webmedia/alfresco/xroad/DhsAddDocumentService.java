package ee.webmedia.alfresco.xroad;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentDynamicService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNamespaceService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;
import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.ACCESS_RESTRICTION;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.Pair;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.springframework.stereotype.Service;

import com.ibm.icu.util.Calendar;

import ee.webmedia.alfresco.casefile.model.CaseFileModel;
import ee.webmedia.alfresco.casefile.service.CaseFile;
import ee.webmedia.alfresco.casefile.service.CaseFileService;
import ee.webmedia.alfresco.classificator.enums.AccessRestriction;
import ee.webmedia.alfresco.classificator.enums.DocumentStatus;
import ee.webmedia.alfresco.classificator.enums.VolumeType;
import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.common.web.WmNode;
import ee.webmedia.alfresco.dhsxroad.ws.AnnexType;
import ee.webmedia.alfresco.dhsxroad.ws.AnnexType.Annex;
import ee.webmedia.alfresco.dhsxroad.ws.DhsAddDocumentRequestType;
import ee.webmedia.alfresco.dhsxroad.ws.DhsAddDocumentResponseType;
import ee.webmedia.alfresco.dhsxroad.ws.DocumentFileType;
import ee.webmedia.alfresco.dhsxroad.ws.DocumentFileType.DocumentFile;
import ee.webmedia.alfresco.dhsxroad.ws.ErrorType;
import ee.webmedia.alfresco.dhsxroad.ws.MetadataType.Field;
import ee.webmedia.alfresco.dhsxroad.ws.WorkflowType;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.docadmin.service.CaseFileType;
import ee.webmedia.alfresco.docadmin.service.DocumentAdminService;
import ee.webmedia.alfresco.docadmin.service.DocumentTypeVersion;
import ee.webmedia.alfresco.docconfig.service.DynamicPropertyDefinition;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.docdynamic.service.DocumentDynamic;
import ee.webmedia.alfresco.document.file.model.FileModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.document.model.DocumentSpecificModel;
import ee.webmedia.alfresco.document.service.DocumentService;
import ee.webmedia.alfresco.functions.model.Function;
import ee.webmedia.alfresco.functions.model.FunctionsModel;
import ee.webmedia.alfresco.functions.service.FunctionsService;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.parameters.service.ParametersService;
import ee.webmedia.alfresco.register.model.Register;
import ee.webmedia.alfresco.register.service.RegisterService;
import ee.webmedia.alfresco.series.model.Series;
import ee.webmedia.alfresco.series.model.SeriesModel;
import ee.webmedia.alfresco.series.service.SeriesService;
import ee.webmedia.alfresco.user.service.UserService;
import ee.webmedia.alfresco.utils.FilenameUtil;
import ee.webmedia.alfresco.utils.RepoUtil;
import ee.webmedia.alfresco.utils.UserUtil;
import ee.webmedia.alfresco.volume.model.VolumeModel;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.service.Task;
import ee.webmedia.alfresco.workflow.service.Workflow;
import ee.webmedia.alfresco.workflow.service.WorkflowService;
import ee.webmedia.alfresco.xroad.DocumentsMapper.PropertyValue;
import ee.webmedia.alfresco.xroad.DocumentsMapper.TypeInfo;

/**
 * Imports structure (functions, series, volumes and cases).
 */
@Service
public class DhsAddDocumentService {

    private static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(DhsAddDocumentService.class);


    private static final String FIELD_OWNER_ID = "ownerId";
   
    private static final FastDateFormat staticDateTimeFormat = FastDateFormat.getInstance("dd.MM.yyyy HH:mm:ss");

    private final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private final DateFormat yearYYFormat = new SimpleDateFormat("yy");

    private static final String STATUS_OPEN = "avatud";
    private static final String STORAGE_TYPE_DIGITAL = "digitaalne";
    private static final String TRANSMITTAL_MODE_EMAIL = "e-mail";
    private static final String VALUE_UNKNOWN = "TUNDMATU";
    
    private static final String[] RESTRICTIONS = { "", AccessRestriction.AK.getValueName(), AccessRestriction.OPEN.getValueName(), AccessRestriction.INTERNAL.getValueName(),
        AccessRestriction.LIMITED.getValueName() };

    static {
        // These arrays are used for binary search; they must be sorted to be useful.
        Arrays.sort(RESTRICTIONS);
    }

    private NodeService nodeService;
    private GeneralService generalService;
    private FunctionsService functionsService;
    private DocumentAdminService documentAdminService;
    private CaseFileService caseFileService;
    private UserService userService;
    private TransactionService transactionService;
    private ParametersService parametersService;
    private FileFolderService fileFolderService;
    private DocumentService documentService;
    private DocumentsMapper documentsMapper;
    private BehaviourFilter behaviourFilter;
    private WorkflowService workflowService;
    private SeriesService seriesService;
    private RegisterService registerService;
    
    private String docComment = ""; 
    private String defaultOwnerId = null;
    private String ownerId = null;
    private Map<QName, Serializable> userProps = null;
    private String docName = VALUE_UNKNOWN;
    
    public DhsAddDocumentService() {
    	nodeService = BeanHelper.getNodeService();
    	generalService = BeanHelper.getGeneralService();
    	functionsService = BeanHelper.getFunctionsService();
    	documentAdminService = BeanHelper.getDocumentAdminService();
    	caseFileService = BeanHelper.getCaseFileService();
    	userService = BeanHelper.getUserService();
    	transactionService = BeanHelper.getTransactionService();
    	parametersService = BeanHelper.getParametersService();
    	fileFolderService = BeanHelper.getFileFolderService();
    	
    	behaviourFilter = BeanHelper.getPolicyBehaviourFilter();
    	documentService = BeanHelper.getDocumentService();
    	workflowService = BeanHelper.getWorkflowService();
    	seriesService = BeanHelper.getSeriesService();
    	registerService = BeanHelper.getRegisterService();
    	
        documentsMapper = new DocumentsMapper();
        documentsMapper.init();
        documentsMapper.setNamespaceService(getNamespaceService());
        documentsMapper.setDictionaryService(BeanHelper.getDictionaryService());
        documentsMapper.setGeneralService(BeanHelper.getGeneralService());

        dateFormat.setLenient(false);
        dateTimeFormat.setLenient(false);
        yearYYFormat.setLenient(false);
    	
    }
    
    public DhsAddDocumentResponseType executeSaveDocumentInTransaction(final DhsAddDocumentRequestType request) {
        NodeRef docRef = null;
        DhsAddDocumentResponseType responseType = new DhsAddDocumentResponseType();
        
        try {
    		docRef = saveDocument(request);
            
    		String regNumber = (String)nodeService.getProperty(docRef, DocumentCommonModel.Props.REG_NUMBER);
    		Date regDate = (Date)nodeService.getProperty(docRef, DocumentCommonModel.Props.REG_DATE_TIME);
    		responseType.setDocRegNumber(regNumber);
    		responseType.setRegDateTime(dateToXmlDate(regDate));
    		return responseType;
        } catch (Exception e) {
            LOG.error("Import of document failed, transaction rolled back. Failed document type: " + request.getDocumentType(), e);
            if (e.getCause() instanceof ImportValidationException) {
            	return createErrorResponse(((ImportValidationException)e.getCause()).getErrorCode(), e.getCause().getMessage());
            } else {
            	return createErrorResponse(DhsAddDocumentError.VB00, "General server error.");
            }
            
        }
        
    }
    
    private static DhsAddDocumentResponseType createErrorResponse(DhsAddDocumentError responseCode, String message) {
		ErrorType.Error error = new ErrorType.Error();
		error.setCode(responseCode.name());
		error.setValue(message);
		ErrorType errorType = new ErrorType();
		errorType.getError().add(error);
		DhsAddDocumentResponseType responseType = new DhsAddDocumentResponseType();
		responseType.setErrors(errorType);
		return responseType;
	}

    private NodeRef saveDocument(DhsAddDocumentRequestType request) throws ImportValidationException {
        
        
        
        TypeInfo typeInfo = documentsMapper.getTypeInfo(request.getDocumentType());
        boolean isDefaultTypeInfo = false;
        String defaultDocType = parametersService.getStringParameter(Parameters.DHS_ADD_DOCUMENT_XROAD_DEFAULT_DOC_TYPE);
        if (typeInfo == null) {
        	typeInfo = (StringUtils.isNotBlank(defaultDocType)) ? documentsMapper.getTypeInfo(defaultDocType) : null;
        	isDefaultTypeInfo = true;
        }
        if (typeInfo == null) {
        	throw new ImportValidationException("Missing typeInfo.", DhsAddDocumentError.VB03);
        }
        
        defaultOwnerId = parametersService.getStringParameter(Parameters.DHS_ADD_DOCUMENT_XROAD_DEFAULT_OWNER);
        if (StringUtils.isBlank(defaultOwnerId)) {
        	throw new ImportValidationException("Missing default owner id.", DhsAddDocumentError.VB04);
        }
        ownerId = findOwnerId(request.getMetadata().getField());
        docComment = "";
        
        
    	if (StringUtils.isNotBlank(ownerId) && userService.getPerson(ownerId) == null) {
    		docComment = "ownerId: " + ownerId;
    	}
        if ((StringUtils.isBlank(ownerId) || StringUtils.isNotBlank(docComment)) && userService.getPerson(defaultOwnerId) == null) {
        	throw new ImportValidationException("No person found for default ownerId: " + defaultOwnerId, DhsAddDocumentError.VB04);
        }
        if (StringUtils.isBlank(ownerId) || StringUtils.isNotBlank(docComment)) {
        	ownerId = defaultOwnerId;
        }
        
        userProps = userService.getUserProperties(ownerId);
        docName = VALUE_UNKNOWN;
        
        NodeRef docRef = processDoc(request, defaultDocType, isDefaultTypeInfo, typeInfo);
        
        LOG.info("Document import successfully finished: " + docRef);
        
        return docRef;
    }
    
    
    private NodeRef processDoc(final DhsAddDocumentRequestType request,
    		final String defaultDocType,
    		final boolean isDefaultTypeInfo,
    		final TypeInfo typeInfo) {
    	return AuthenticationUtil.runAs(new RunAsWork<NodeRef>() {

            @Override
            public NodeRef doWork() throws Exception {
            	NodeRef mainDocsList = generalService.getNodeRef(FunctionsModel.Repo.FUNCTIONS_SPACE, generalService.getStore());
            	Map<Long, QName> propertyTypes = new HashMap<Long, QName>();
            	final NodeRef function = findFunction(mainDocsList, request.getFunction(), propertyTypes);
            	final NodeRef defaultFunction = findFunction(mainDocsList, BeanHelper.getParametersService().getStringParameter(Parameters.DHS_ADD_DOCUMENT_XROAD_DEFAULT_FUNCTION), propertyTypes);
            	final NodeRef docRef = getTransactionHelper().doInTransaction(new RetryingTransactionCallback<NodeRef>() {
                    @Override
                    public NodeRef execute() throws Throwable {
                        behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
                        
                        NodeRef series = findSeries(function, request.getSeries(), true);
                        NodeRef defaultSeries = (defaultFunction != null) ? findSeries(defaultFunction, BeanHelper.getParametersService().getStringParameter(Parameters.DHS_ADD_DOCUMENT_XROAD_DEFAULT_SERIES), false) : null;
                        NodeRef volume = findOrCreateVolume(series, defaultSeries, request.getCaseFileType(), request.getVolumeName());
                        
                        updateSeriesDocumentTypes(volume, (isDefaultTypeInfo) ? defaultDocType : request.getDocumentType());
                        
                        Date regDateTime = new Date();
                        
                        NodeRef docRef = createDoc(typeInfo, volume, request, regDateTime);
                        documentService.registerDocumentIfNotRegistered(docRef, false);
                        
                        return docRef;
                    }
                });
            	
            	documentService.updateParentNodesContainingDocsCount(docRef, true);
            	
            	try {
            	getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Void>() {
                    @Override
                    public Void execute() throws Throwable {
                        behaviourFilter.disableBehaviour(ContentModel.ASPECT_AUDITABLE);
                        
                        createCompoundWorkFlow(request.getWorkflow(), docRef);
                        
                        return null;
                    }
                });
            	} catch (Exception e) {
            		LOG.warn("Could not create compound workflow for docRef: " + docRef, e);
            	}
            	
            	return docRef;
            	
            }
        }, ownerId);
    }
    
    
    private void updateSeriesDocumentTypes(NodeRef volumeRef, String docTypeName) {
    	NodeRef seriesRef = (NodeRef)nodeService.getProperty(volumeRef, DocumentCommonModel.Props.SERIES);
    	List<String> docTypes = (List<String>) nodeService.getProperty(seriesRef, SeriesModel.Props.DOC_TYPE);
    	if (!docTypes.contains(docTypeName)) {
    		docTypes.add(docTypeName);
    		nodeService.setProperty(seriesRef, SeriesModel.Props.DOC_TYPE, (Serializable)docTypes);
    	}
    }

    private NodeRef findFunction(NodeRef docsList, String mark, Map<Long, QName> propertyTypes) throws ImportValidationException {
    	NodeRef function = null;
    	int foundCount = 0;
    	for (ChildAssociationRef assoc : nodeService.getChildAssocs(docsList, FunctionsModel.Associations.FUNCTION, FunctionsModel.Associations.FUNCTION)) {
            if (STATUS_OPEN.equals(nodeService.getProperty(assoc.getChildRef(), FunctionsModel.Props.STATUS)) && mark.equals(nodeService.getProperty(assoc.getChildRef(), FunctionsModel.Props.MARK))) {
                function = assoc.getChildRef();
                

                Function f = functionsService.getFunction(function, propertyTypes);
                if ("suletud".equals(f.getStatus())) {
                    function = null;
                    continue;
                }
                foundCount++;
            }
        }

        if (function == null || foundCount != 1) {
        	throw new ImportValidationException("Could not map function:" + mark, DhsAddDocumentError.VB01);
        }

        return function;
    }

    private NodeRef findSeries(final NodeRef function, String seriesId, boolean strictSearch) throws ImportValidationException {
    	NodeRef series = null;
    	int foundCount = 0;
    	
        for (ChildAssociationRef assoc : nodeService.getChildAssocs(function, SeriesModel.Associations.SERIES, SeriesModel.Associations.SERIES)) {
            if (STATUS_OPEN.equals(nodeService.getProperty(assoc.getChildRef(), SeriesModel.Props.STATUS)) && seriesId.equals(nodeService.getProperty(assoc.getChildRef(), SeriesModel.Props.SERIES_IDENTIFIER))) {
                series = assoc.getChildRef();
                foundCount++;
            }
        }
        
        if (series == null || foundCount != 1) {
        	if (strictSearch) {
        		throw new ImportValidationException("Could not map series: " + seriesId, DhsAddDocumentError.VB02);
        	}
        	return null;
        }

        return series;
    }
    
    private NodeRef findOrCreateVolume(final NodeRef series, NodeRef defaultSeries, String caseFileTypeName, String volumeName) throws ImportValidationException {
    	
    	DocumentTypeVersion caseFileTypeVersion = null;
    	boolean isCaseFile = false;
    	boolean isAnualVolume = false;
        List<ChildAssociationRef> existingVolumes = new ArrayList<ChildAssociationRef>();
        if (StringUtils.isNotBlank(caseFileTypeName) && StringUtils.isNotBlank(volumeName)) {
        	caseFileTypeVersion = getCaseFileTypeVersion(caseFileTypeName);
        	existingVolumes.addAll(nodeService.getChildAssocs(series, CaseFileModel.Assocs.CASE_FILE, CaseFileModel.Assocs.CASE_FILE));
        	isCaseFile = true;
        } else {
        	existingVolumes.addAll(nodeService.getChildAssocs(series, VolumeModel.Associations.VOLUME, VolumeModel.Associations.VOLUME));
        }
        if (StringUtils.isBlank(caseFileTypeName) && StringUtils.isBlank(volumeName)) {
        	isAnualVolume = true;
        }
        
        
        List<NodeRef> foundVolumes = new ArrayList<>(); 
        for (ChildAssociationRef assoc : existingVolumes) {
            if (STATUS_OPEN.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.STATUS))
            	&& (isCaseFile && volumeName.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.TITLE)) 
            		&& caseFileTypeName.equals(nodeService.getProperty(assoc.getChildRef(), DocumentAdminModel.Props.OBJECT_TYPE_ID)))
            	|| (StringUtils.isNotBlank(volumeName) && volumeName.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.TITLE))
                		&& VolumeType.SUBJECT_FILE.name().equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.VOLUME_TYPE)))) {
            	foundVolumes.add(assoc.getChildRef());
            }
        }
        
        if (foundVolumes.isEmpty() && !isCaseFile) {
        	if (!isAnualVolume && defaultSeries == null) {
        		throw new ImportValidationException("Missing default series or function.", DhsAddDocumentError.VB02);
        	}
        	String seriesIdent = (String)nodeService.getProperty(((isAnualVolume)?series:defaultSeries), SeriesModel.Props.SERIES_IDENTIFIER);
        	// search for annual files
        	String volumeMark = seriesIdent + "/" + Calendar.getInstance().get(Calendar.YEAR);
        	existingVolumes = nodeService.getChildAssocs((isAnualVolume)?series:defaultSeries, VolumeModel.Associations.VOLUME, VolumeModel.Associations.VOLUME);
        	for (ChildAssociationRef assoc : existingVolumes) {
                if (STATUS_OPEN.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.STATUS)) && volumeMark.equals(nodeService.getProperty(assoc.getChildRef(), VolumeModel.Props.VOLUME_MARK))) {
                	foundVolumes.add(assoc.getChildRef());
                }
            }
        	if (foundVolumes.isEmpty() && isAnualVolume) {
        		throw new ImportValidationException("Could not map annual volume: " + volumeMark, DhsAddDocumentError.VB06);
        	}
        }
        
        NodeRef volume = findOldestVolume(foundVolumes);
        
        if (volume == null) {
            String volumeType = (isCaseFile) ? VolumeType.CASE_FILE.name() : VolumeType.SUBJECT_FILE.name();
            
            Map<QName, Serializable> props = new HashMap<QName, Serializable>();
            props.put(DocumentCommonModel.Props.FUNCTION, nodeService.getPrimaryParent(series).getParentRef());
            props.put(DocumentCommonModel.Props.SERIES, series);
            setVolumeMark(isCaseFile, series, props);
            props.put(VolumeModel.Props.TITLE, volumeName);
            props.put(VolumeModel.Props.VOLUME_TYPE, volumeType);
            props.put(VolumeModel.Props.VALID_FROM, new Date());
            props.put(VolumeModel.Props.STATUS, STATUS_OPEN);
            props.put(VolumeModel.Props.CONTAINS_CASES, Boolean.FALSE);

            if (!isCaseFile) {
            	
                volume = nodeService.createNode(series, VolumeModel.Associations.VOLUME, VolumeModel.Associations.VOLUME, VolumeModel.Types.VOLUME, props).getChildRef();
            } else {
            	
                props.put(DocumentCommonModel.Props.OWNER_ID, ownerId);
                props.put(DocumentCommonModel.Props.OWNER_NAME, UserUtil.getPersonFullName1(userProps));
                props.put(DocumentCommonModel.Props.OWNER_EMAIL, userProps.get(ContentModel.PROP_EMAIL));
                props.put(DocumentCommonModel.Props.OWNER_PHONE, userProps.get(ContentModel.PROP_TELEPHONE));
                props.put(DocumentCommonModel.Props.OWNER_JOB_TITLE, userProps.get(ContentModel.PROP_JOBTITLE));
                props.put(DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT, userProps.get(ContentModel.PROP_ORGANIZATION_PATH));
                props.put(DocumentDynamicModel.Props.OWNER_SERVICE_RANK, userProps.get(ContentModel.PROP_SERVICE_RANK));
                props.put(DocumentDynamicModel.Props.OWNER_WORK_ADDRESS, userProps.get(ContentModel.PROP_STREET_HOUSE));

               
                CaseFile cf = caseFileService.createNewCaseFile(caseFileTypeVersion, series, false).getFirst();
                for (Entry<QName, Serializable> entry : props.entrySet()) {
                    cf.setProp(entry.getKey(), entry.getValue());
                }
                caseFileService.update(cf, null);
                volume = cf.getNodeRef();
            }

        }

        return volume;
    }
    
    private NodeRef findOldestVolume(List<NodeRef> volumeRefs) {
    	if (CollectionUtils.isEmpty(volumeRefs)) {
    		return null;
    	}
    	NodeRef volumeRef = volumeRefs.get(0); 
    	Date validFrom = (Date) nodeService.getProperty(volumeRef, VolumeModel.Props.VALID_FROM);
    	
    	for (NodeRef volRef: volumeRefs) {
    		Date volRefValidFrom = (Date) nodeService.getProperty(volRef, VolumeModel.Props.VALID_FROM);
    		if (validFrom == null || (volRefValidFrom != null && validFrom.after(volRefValidFrom))) {
    			validFrom = volRefValidFrom;
    			volumeRef = volRef;
    		}
    	}
    	
    	return volumeRef;
    }
    
    private DocumentTypeVersion getCaseFileTypeVersion(String caseFileTypeName) throws ImportValidationException {
    	CaseFileType caseFileType = documentAdminService.getCaseFileType(caseFileTypeName,
                DocumentAdminService.DOC_TYPE_WITH_OUT_GRAND_CHILDREN_EXEPT_LATEST_DOCTYPE_VER); 
        if (caseFileType != null) {
            return caseFileType.getLatestDocumentTypeVersion();
        }
        throw new ImportValidationException("Missing case file type:" + caseFileTypeName, DhsAddDocumentError.VB05);
    }
    
    private void setVolumeMark(boolean isCaseFile, NodeRef seriesNodeRef, Map<QName, Serializable> volumeProps) throws ImportValidationException {
    	String volumeMark = (String) nodeService.getProperty(seriesNodeRef, SeriesModel.Props.SERIES_IDENTIFIER);
    	if (isCaseFile) {
    		Series series = seriesService.getSeriesByNodeRef(seriesNodeRef);
            if (series.getVolRegister() == null) {
                throw new ImportValidationException("caseFile volume register missing", DhsAddDocumentError.VB07);
            }
            
            Register volRegister = registerService.getRegister(series.getVolRegister());

            int counterVal = registerService.increaseCount(volRegister.getId());
            volumeMark += "/" + yearYYFormat.format(new Date()) + "/" + counterVal;
            volumeProps.put(VolumeModel.Props.VOL_SHORT_REG_NUMBER, counterVal);
    		
    		
    	}
    	volumeProps.put(VolumeModel.Props.VOLUME_MARK, volumeMark);
    	
    }

    private String findOwnerId(List<Field> fields) {
    	for (Field field: fields) {
    		if (FIELD_OWNER_ID.equals(field.getName()) && StringUtils.isNotBlank(field.getValue())) {
    			return field.getValue();
    		}
    	}
    	
    	return null;
    }
    
    private String addFiles(NodeRef docRef, DhsAddDocumentRequestType request) throws ImportValidationException {
    	StringBuilder fileNames = new StringBuilder();
    	DocumentFileType activeFiles = request.getDocumentFiles();
        if (activeFiles != null && activeFiles.getDocumentFile() != null) {
            for (DocumentFile file : activeFiles.getDocumentFile()) {
                String fileName = file.getName();
                String contentStr = file.getValue();
                
                NodeRef fileRef = addFile(fileName, contentStr, docRef, null, true, true);
                if (fileNames.length() > 1) {
                	fileNames.append("| ");
                }
                fileNames.append(fileName + ":" + fileRef.toString());
            }
        }
        AnnexType inactiveFiles = request.getAnnexes();
        if (inactiveFiles != null) {
            for (Annex file : inactiveFiles.getAnnex()) {
                String fileName = file.getName();
                String contentStr = file.getValue();
                NodeRef fileRef = addFile(fileName, contentStr, docRef, null, false, true);
                if (fileNames.length() > 1) {
                	fileNames.append("| ");
                }
                fileNames.append(fileName + ":" + fileRef.toString());
            }
        }
        
        return fileNames.toString();
    }
    
    private NodeRef addFile(String displayName, String content, NodeRef parentNodeRef, String mimeType, boolean active, boolean isBase64) throws ImportValidationException {
        String fileName = FilenameUtil.makeSafeFilename(displayName);
        fileName = generalService.getUniqueFileName(parentNodeRef, fileName);
        final FileInfo fileInfo = fileFolderService.create(parentNodeRef, fileName, ContentModel.TYPE_CONTENT);
        final NodeRef fileRef = fileInfo.getNodeRef();
        
        OutputStream out = null;
        try {
        	String mimetype = BeanHelper.getMimetypeService().guessMimetype(fileName);
	        final ContentWriter writer = fileFolderService.getWriter(fileRef);
	        writer.setMimetype(mimetype);
	        writer.setEncoding("UTF-8");
	        if (isBase64) {
		        out = writer.getContentOutputStream();
		        byte[] decodedContent = Base64.decode(content);
		        out.write(decodedContent);
		        out.flush();
	        } else {
	        	writer.putContent(content);
	        }
        } catch (IOException e) {
            final String msg = "Outputstream exception while writing file: " + fileName;
            throw new ImportValidationException(msg, DhsAddDocumentError.VS01);
        } catch (Base64DecodingException e) {
        	final String msg = "Outputstream exception while exporting consolidated docList row to CSV-stream";
        	throw new ImportValidationException(msg, DhsAddDocumentError.VS01);
        } finally {
        	IOUtils.closeQuietly(out);
        }
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        props.put(ContentModel.PROP_CREATOR, defaultOwnerId);
        props.put(ContentModel.PROP_MODIFIER, defaultOwnerId);
        props.put(FileModel.Props.DISPLAY_NAME, displayName);
        props.put(FileModel.Props.ACTIVE, Boolean.valueOf(active));
        nodeService.addProperties(fileRef, props);
        return fileRef;
    }
    
    private NodeRef createDoc(TypeInfo typeInfo, NodeRef volume, DhsAddDocumentRequestType request, Date regDateTime) throws ImportValidationException {
    	
    	Map<QName, Serializable> propsMap = setProps(typeInfo, volume, request);

		DocumentDynamic doc = getDocumentDynamicService().createNewDocument(typeInfo.docVer, volume, false)
				.getFirst();
		NodeRef documentRef = doc.getNodeRef();
		nodeService.addAspect(documentRef, DocumentCommonModel.Aspects.SEARCHABLE, null);

		Map<QName, NodeRef> parentRefs = documentService.getDocumentParents(documentRef);

		propsMap.putAll(parentRefs);

		checkProps(propsMap, null, typeInfo.propDefs);

		Pair<DynamicPropertyDefinition, ee.webmedia.alfresco.docadmin.service.Field> docNameAdrPropDefinition = typeInfo.propDefs
				.get(DocumentDynamicModel.Props.DOC_NAME_ADR.getLocalName());
		if (docNameAdrPropDefinition != null && docNameAdrPropDefinition.getSecond() != null) {
			String docNameAdrDefault = docNameAdrPropDefinition.getSecond().getClassificatorDefaultValue();
			if (StringUtils.isNotBlank(docNameAdrDefault)) {
				propsMap.put(DocumentDynamicModel.Props.DOC_NAME_ADR, docNameAdrDefault);
			}
		}

    	propsMap.put(DocumentCommonModel.Props.DOCUMENT_IS_IMPORTED, Boolean.TRUE);
    	propsMap.put(DocumentCommonModel.Props.SEARCHABLE_HAS_ALL_FINISHED_COMPOUND_WORKFLOWS, true);
    	propsMap.put(DocumentCommonModel.Props.DOC_STATUS, DocumentStatus.WORKING.getValueName());
    	propsMap.put(DocumentCommonModel.Props.REG_DATE_TIME, regDateTime);
    	
    	setAccessRestrictionIfNeeded(propsMap);
    	
    	
		doc.getNode().getProperties().putAll(RepoUtil.toStringProperties(propsMap));
		doc.getNode().getProperties().put(
				DocumentService.TransientProps.TEMP_LOGGING_DISABLED_DOCUMENT_METADATA_CHANGED.toString(),
				Boolean.TRUE);
		getDocumentDynamicService().updateDocument(doc, null, false, true);
		

		addLogEntry(documentRef);		

		addFiles(documentRef, request);
		BeanHelper.getFileService().reorderFiles(documentRef);
		
		docName = (String)propsMap.get(DocumentCommonModel.Props.DOC_NAME);
		
    	return documentRef;
    }
    
    
    private Map<QName, Serializable> setProps(TypeInfo typeInfo, NodeRef volume, DhsAddDocumentRequestType request) {
        Map<QName, Serializable> propsMap = mapProperties(request.getMetadata().getField(), typeInfo, null);

        String regNumber = (String)propsMap.get(DocumentCommonModel.Props.REG_NUMBER);
        if (StringUtils.isNotBlank(regNumber)) {
        	String shortRegNumber = null;
        	if (StringUtils.isNotBlank(regNumber)) {
        		if (regNumber.contains("/")) {
        			shortRegNumber = StringUtils.substringAfterLast(regNumber, "/");
        		} else if (regNumber.contains("-")) {
            		shortRegNumber = StringUtils.substringBeforeLast(shortRegNumber, "-");
            	} else {
            		shortRegNumber = regNumber;
            	}
        		
        	}
            propsMap.put(DocumentCommonModel.Props.SHORT_REG_NUMBER, shortRegNumber);
            
        }
        
        
        setOwnerProperties(typeInfo, propsMap);
        
        return propsMap;
    }
    
    private Map<QName, Serializable> mapProperties(List<Field> fields, TypeInfo typeInfo, Map<QName, Serializable> parentPropsMap) {
        Map<QName, Serializable> propsMap = new HashMap<>();
        addFieldsWithDefaultValues(typeInfo, propsMap);
        
        for (Field field: fields) {
    		String fieldName = field.getName();
    		String value = field.getValue();
    		if (StringUtils.isBlank(value)) {
    			continue;
    		}
    		
    		org.alfresco.util.Pair<DynamicPropertyDefinition, ee.webmedia.alfresco.docadmin.service.Field> pair = typeInfo.propDefs.get(fieldName);
    		if (pair == null) {
    			docComment = StringUtils.isNotBlank(docComment)?docComment + " " + fieldName + ":" + value + ";" 
    					:fieldName + ":" + value + ";";
    		} else {
    		
    			DynamicPropertyDefinition propDef = pair.getFirst();
    			
		    	DocumentsMapper.PropertyValueProvider valueProvider = typeInfo.props.get(fieldName);
		    	if (valueProvider != null) {
		    		PropertyValue propValue = valueProvider.provide();
		            propValue.put(value);
		    		propsMap.put(propDef.getName(), propValue.get());
    			} else {
    				propsMap.put(propDef.getName(), value);
    			}
    		}
    	}
        
        if (StringUtils.isNotBlank(docComment)) {
        	addMissingFieldToCommment(docComment, (parentPropsMap != null) ? parentPropsMap : propsMap, typeInfo);
        }
        
        return propsMap;
    }
    
    private void addMissingFieldToCommment(String missingFieldNameValue, Map<QName, Serializable> propsMap, TypeInfo typeInfo) {
    	Pair<DynamicPropertyDefinition, ee.webmedia.alfresco.docadmin.service.Field> pairComment =
    			typeInfo.propDefs.get(DocumentCommonModel.Props.COMMENT.getLocalName());
		if (pairComment != null) {
			String propComment =  (String) propsMap.get(DocumentCommonModel.Props.COMMENT);
			propComment = (StringUtils.isNotBlank(propComment)) ? propComment + " " + missingFieldNameValue + ";" : missingFieldNameValue + ";"; 
			propsMap.put(DocumentCommonModel.Props.COMMENT, propComment);
		}
    }
    
    private void addFieldsWithDefaultValues(TypeInfo typeInfo, Map<QName, Serializable> props) {
    	if (hasField(typeInfo, DocumentCommonModel.Props.STORAGE_TYPE)) {
   		 props.put(DocumentCommonModel.Props.STORAGE_TYPE, STORAGE_TYPE_DIGITAL); 
    	}
    	if (hasField(typeInfo, DocumentSpecificModel.Props.TRANSMITTAL_MODE)) {
      		 props.put(DocumentSpecificModel.Props.TRANSMITTAL_MODE, TRANSMITTAL_MODE_EMAIL); 
       	}
    	if (hasField(typeInfo, DocumentCommonModel.Props.DOC_NAME)) {
      		 props.put(DocumentCommonModel.Props.DOC_NAME, VALUE_UNKNOWN); 
       	}
    	if (hasField(typeInfo, DocumentDynamicModel.Props.DOC_NAME_ADR)) {
      		 props.put(DocumentDynamicModel.Props.DOC_NAME_ADR, VALUE_UNKNOWN); 
       	}
    	if (hasField(typeInfo, DocumentDynamicModel.Props.SENDER_ID)) {
     		 props.put(DocumentDynamicModel.Props.SENDER_ID, VALUE_UNKNOWN); 
      	}
    	if (hasField(typeInfo, DocumentDynamicModel.Props.SENDER_PERSON_NAME)) {
     		 props.put(DocumentDynamicModel.Props.SENDER_PERSON_NAME, VALUE_UNKNOWN); 
      	}
    	if (hasField(typeInfo, DocumentSpecificModel.Props.SENDER_DETAILS_EMAIL)) {
     		 props.put(DocumentSpecificModel.Props.SENDER_DETAILS_EMAIL, VALUE_UNKNOWN); 
      	}
    }
    
    
    
    private void setOwnerProperties(TypeInfo typeInfo, Map<QName, Serializable> props) {
        
    	props.put(DocumentCommonModel.Props.OWNER_ID, ownerId);
    	props.put(DocumentCommonModel.Props.OWNER_NAME, UserUtil.getPersonFullName1(userProps));
    	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_EMAIL)) {
    		props.put(DocumentCommonModel.Props.OWNER_EMAIL, userProps.get(ContentModel.PROP_EMAIL));
    	}
    	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_PHONE)) {
    		 props.put(DocumentCommonModel.Props.OWNER_PHONE, userProps.get(ContentModel.PROP_TELEPHONE));
    	}
    	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_JOB_TITLE)) {
    		props.put(DocumentCommonModel.Props.OWNER_JOB_TITLE, userProps.get(ContentModel.PROP_JOBTITLE));
    	}
    	if (hasField(typeInfo, DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT)) {
    		props.put(DocumentCommonModel.Props.OWNER_ORG_STRUCT_UNIT, userProps.get(ContentModel.PROP_ORGANIZATION_PATH));
    	}
    	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_SERVICE_RANK)) {
    		props.put(DocumentDynamicModel.Props.OWNER_SERVICE_RANK, userProps.get(ContentModel.PROP_SERVICE_RANK));
    	}
    	if (hasField(typeInfo, DocumentDynamicModel.Props.OWNER_WORK_ADDRESS)) {
    		props.put(DocumentDynamicModel.Props.OWNER_WORK_ADDRESS, userProps.get(ContentModel.PROP_STREET_HOUSE));
    	}
        
    }
    
    private boolean hasField(TypeInfo typeInfo, QName fieldQName) {
    	Pair<DynamicPropertyDefinition, ee.webmedia.alfresco.docadmin.service.Field> pair = typeInfo.propDefs.get(fieldQName.getLocalName());
		return pair != null;
    }
    
    private void checkProps(Map<QName, Serializable> propsMap, QName[] hierarchy, Map<String, Pair<DynamicPropertyDefinition, ee.webmedia.alfresco.docadmin.service.Field>> propDefs) {
        List<String> errors = new ArrayList<String>();
        for (Entry<QName, Serializable> entry : propsMap.entrySet()) {
            String propNameString = entry.getKey().toPrefixString(getNamespaceService());
            if (!DocumentDynamicModel.URI.equals(entry.getKey().getNamespaceURI())) {
                errors.add("Property " + propNameString + " has wrong namespace");
                continue;
            }
            org.alfresco.util.Pair<DynamicPropertyDefinition, ee.webmedia.alfresco.docadmin.service.Field> pair = propDefs.get(entry.getKey().getLocalName());
            if (pair == null) {
                errors.add("Property " + propNameString + " is missing propertyDefinition");
                continue;
            }

            if (hierarchy == null) {
                hierarchy = new QName[] {};
            }
            DynamicPropertyDefinition propDef = pair.getFirst();
            QName[] propHierarchy = propDef.getChildAssocTypeQNameHierarchy();
            if (propHierarchy == null) {
                propHierarchy = new QName[] {};
            }
            if (!Arrays.equals(hierarchy, propHierarchy)) {
                errors.add("Property " + propNameString + " should be on hierarchy " + Arrays.asList(propHierarchy) + " but is on hierarchy " + Arrays.asList(hierarchy));
                continue;
            }

            // DocumentsMapper doesn't know about single- and multi-valued properties, so handle it here
            Serializable value = entry.getValue();
            if (propDef.isMultiValued() && value != null && !(value instanceof List)) {
                ArrayList<Serializable> list = new ArrayList<Serializable>();
                list.add(value);
                value = list;
                entry.setValue(value);
            }

            if (value == null) {
                continue;
            }
            String requiredClassName = propDef.getDataType().getJavaClassName();
            if (value instanceof List<?>) {
                if (!("additionalRecipientName".equals(propDef.getName().getLocalName()) || "partyName".equals(propDef.getName().getLocalName()) || "partyContactPerson".equals(propDef.getName().getLocalName())) && !propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should not be a List, but is a List");
                    continue;
                }
                List<Serializable> list = (List<Serializable>) value;
                int i = 0;
                for (Serializable listElement : list) {
                    if (listElement == null) {
                        i++;
                        continue;
                    }
                    String realClassName = listElement.getClass().getName();
                    if (!realClassName.equals(requiredClassName)) {
                        errors.add("Property " + propNameString + " value at index " + i + " should be of type " + requiredClassName + " but is of type " + realClassName + " : "
                                + listElement);
                        continue;
                    }
                    i++;
                }
            } else {
                if (propDef.isMultiValued()) {
                    errors.add("Property " + propNameString + " should be a List, but is not a List");
                    continue;
                }
                String realClassName = value.getClass().getName();
                if (!realClassName.equals(requiredClassName)) {
                    errors.add("Property " + propNameString + " value should be of type " + requiredClassName + " but is of type " + realClassName + " : " + value);
                    continue;
                }
            }
        }
        if (!errors.isEmpty()) {
            LOG.error("props=" + WmNode.toString(propsMap, getNamespaceService()));
            throw new RuntimeException("Found " + errors.size() + " errors:\n  * " + StringUtils.join(errors, "\n  * "));
        }
    }
    
    private void setAccessRestrictionIfNeeded(Map<QName, Serializable> props) {
        String accessRestriction = (String) props.get(ACCESS_RESTRICTION);
        if (StringUtils.isBlank(accessRestriction)) {
        
            props.put(ACCESS_RESTRICTION, AccessRestriction.AK.getValueName());
            props.put(DocumentCommonModel.Props.ACCESS_RESTRICTION_REASON, "AvTS § 35 lg 1 p 12");
            props.put(DocumentCommonModel.Props.ACCESS_RESTRICTION_BEGIN_DATE, new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, 75);
            props.put(DocumentCommonModel.Props.ACCESS_RESTRICTION_END_DATE, cal.getTime());
        }
    }
    
    
    private void createCompoundWorkFlow(WorkflowType reqWorkflowType, NodeRef docRef) {
    	
		if (reqWorkflowType == null || reqWorkflowType.getDueDate() == null) {
			return;
		}
		if (StringUtils.isBlank((String) userProps.get(ContentModel.PROP_EMAIL))) {
    		LOG.warn("Skip compound workflow creation. Owner email is missing for ownerId: " + ownerId);
    		return;
    	}
		
		boolean isCwDefault = false;
		String cwName = reqWorkflowType.getName();
		String cwDefaultName = parametersService.getStringParameter(Parameters.DHS_ADD_DOCUMENT_XROAD_DEFAULT_WORKFLOW);
		if (StringUtils.isBlank(cwName) && StringUtils.isBlank(cwDefaultName)) {
			LOG.warn("At least defualt compount workflow name (dhsAddDocumentXroadDefaultWorkflow)  should be specified");
			return; 
		}
		if (StringUtils.isBlank(cwName)) {
			cwName = cwDefaultName;
			isCwDefault = true;
		}
		
		NodeRef cwDefNodeRef = workflowService.getCompoundWorkflowDefinitionByName(cwName, ownerId, true);
		if (cwDefNodeRef == null && !isCwDefault) {
			cwDefNodeRef = workflowService.getCompoundWorkflowDefinitionByName(cwDefaultName, ownerId, true);
		}
		
		if (cwDefNodeRef == null) {
			LOG.warn("No compound workflow definition was found for names: (from request) " + cwName + ", (default) " + cwDefaultName);
			return;
		}
		
		
		NodeRef parentRef = BeanHelper.getConstantNodeRefsBean().getIndependentWorkflowsRoot();
		
		
	
		CompoundWorkflow compoundWorkflow = workflowService.getNewCompoundWorkflow(cwDefNodeRef, parentRef);
		compoundWorkflow.setOwnerId(ownerId);
        compoundWorkflow.setOwnerName(UserUtil.getPersonFullName1(userProps));
        compoundWorkflow.setProp(WorkflowCommonModel.Props.OWNER_JOB_TITLE, userProps.get(ContentModel.PROP_JOBTITLE));
        compoundWorkflow.setProp(WorkflowCommonModel.Props.OWNER_ORGANIZATION_NAME, (Serializable) userService.getUserOrgPathOrOrgName(userProps));
        compoundWorkflow.setMainDocument(docRef);
        compoundWorkflow.setTitle(docName);
        
        setWorkflowProps(compoundWorkflow.getWorkflows(), xmlDateToDate(reqWorkflowType.getDueDate()));
        
        compoundWorkflow = workflowService.saveCompoundWorkflow(compoundWorkflow);
        
        getNodeService().createAssociation(docRef, compoundWorkflow.getNodeRef(), DocumentCommonModel.Assocs.WORKFLOW_DOCUMENT);
        startWorkflow(compoundWorkflow);
	 
	}
    
    
    private void setWorkflowProps(List<Workflow> workflows, Date dueDate) {
    	for (Workflow workflow: workflows) {
        	workflow.setOwnerId(ownerId);
        	workflow.addTask();
        	setTaskProps(workflow.getTasks(), dueDate);
        }
    }
    
    private void setTaskProps(List<Task> tasks, Date dueDate) {
    	for (Task task: tasks) {
        	task.setOwnerId(ownerId);
        	task.setOwnerName(UserUtil.getPersonFullName1(userProps));
        	task.setOwnerEmail((String)userProps.get(ContentModel.PROP_EMAIL));
        	task.setOwnerJobTitle((String)userProps.get(ContentModel.PROP_JOBTITLE));
        	task.setDueDate(dueDate);
        	
        }
    }
    
	public static Date xmlDateToDate(XMLGregorianCalendar value) {
		if (value != null) {
			return value.toGregorianCalendar().getTime();
		}
		return null;
	}
	
	public static XMLGregorianCalendar dateToXmlDate(Date value) throws DatatypeConfigurationException {
		GregorianCalendar gregory = new GregorianCalendar();
		gregory.setTime(value);

		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
	}
	
	
 
    public void startWorkflow(CompoundWorkflow compoundWorkflow) {
        LOG.debug("startWorkflow");
        workflowService.startCompoundWorkflow(compoundWorkflow);
    }
   
    private void addLogEntry(NodeRef documentRef) {
    	LogEntry logEntry = new LogEntry();
		logEntry.setComputerIp("127.0.0.1");
		logEntry.setComputerName("localhost");
		logEntry.setLevel(LogObject.DOCUMENT.getLevel());
		logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
		logEntry.setCreatorId("XTEE: " + ownerId);
		logEntry.setCreatorName("XTEE: " + UserUtil.getPersonFullName1(userProps));
		logEntry.setEventDescription("DhsAddDocument xtee");
		logEntry.setObjectId(documentRef.toString());
		BeanHelper.getLogService().addLogEntry(logEntry);
    }
    
    
    /** RetryingTransactionHelper that only tries to do things once. */
    private RetryingTransactionHelper getTransactionHelper() {
        RetryingTransactionHelper helper = new RetryingTransactionHelper();
        helper.setMaxRetries(1);
        helper.setTransactionService(transactionService);
        return helper;
    }
}

