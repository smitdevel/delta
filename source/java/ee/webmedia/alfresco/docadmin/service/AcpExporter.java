package ee.webmedia.alfresco.docadmin.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.alfresco.repo.exporter.ACPExportPackageHandler;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.view.ExporterCrawlerParameters;
import org.alfresco.service.cmr.view.Location;
import org.alfresco.util.GUID;
import org.apache.commons.lang3.StringUtils;

import ee.webmedia.alfresco.common.bootstrap.AbstractNodeUpdater;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.parameters.model.Parameters;

public class AcpExporter extends AbstractNodeUpdater {
	
	private String acpNodeRef;
	
	private String acpFileName;
	
    @Override
    protected void executeUpdater() throws Exception {
    	NodeRef nodeRef = new NodeRef(acpNodeRef);
    	if (StringUtils.isBlank(acpNodeRef) || StringUtils.isBlank(acpFileName)) {
    		log.info("node ref and file name must be provided ");
            return;
    	}
        if (!nodeService.exists(nodeRef)) {
            log.info("Node does not exist: nodeRef = " + acpNodeRef);
            return;
        }
        log.info("Starting to export node: " + acpNodeRef);
        
        try {
			Location location = new Location(nodeRef);
			ExporterCrawlerParameters parameters = new ExporterCrawlerParameters();
			parameters.setExportFrom(location);
			parameters.setCrawlChildNodes(true);
			parameters.setCrawlSelf(true);
			parameters.setCrawlContent(true);


			// setup an ACP Package Handler to export to an ACP file format
			File dataFile = new File(GUID.generate());
	        File contentDir = new File(GUID.generate());
	        
	        
	        String acpExportFolder = BeanHelper.getParametersService().getStringParameter(Parameters.ACP_EXPORT_FOLDER);
	        if (StringUtils.isBlank(acpExportFolder)) {
	        	log.warn("acpExportFolder is not defind, skip export");
	        	return;
	        }
	        
	        // setup export package handler
	        File acpFile = new File( acpExportFolder + acpFileName);
	        ACPExportPackageHandler handler = new ACPExportPackageHandler(new FileOutputStream(acpFile), 
	             dataFile, contentDir, BeanHelper.getMimetypeService());

			// now export (note: we're not interested in progress in the example)
			BeanHelper.getExporterService().exportView(handler, parameters, null);

		} catch (Exception e) {
			log.error("Error exporting node: " + acpNodeRef, e);
			return;
		}
        
        log.info("Completed node export: file name = " + acpFileName + ", node ref = " + acpNodeRef);
    }

	@Override
	protected List<ResultSet> getNodeLoadingResultSet() throws Exception {
		return null;
	}

	@Override
	protected String[] updateNode(NodeRef nodeRef) throws Exception {
		return null;
	}

	
	public String getAcpNodeRef() {
		return acpNodeRef;
	}

	public String getAcpFileName() {
		return acpFileName;
	}

	public void setAcpNodeRef(String acpNodeRef) {
		this.acpNodeRef = acpNodeRef;
	}

	public void setAcpFileName(String acpFileName) {
		this.acpFileName = acpFileName;
	}
	
	
}
