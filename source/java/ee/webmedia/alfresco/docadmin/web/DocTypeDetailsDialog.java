package ee.webmedia.alfresco.docadmin.web;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.template.model.UnmodifiableDocumentTemplate;
import ee.webmedia.alfresco.utils.ComponentUtil;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.volume.model.VolumeModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.app.AlfrescoNavigationHandler;
import org.apache.commons.collections.Closure;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import ee.webmedia.alfresco.docadmin.service.DocumentType;
import org.apache.commons.lang3.BooleanUtils;

import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentTemplateService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowConstantsBean;

import static ee.webmedia.alfresco.common.web.BeanHelper.getWorkflowConstantsBean;

/**
 * Details of document type.
 * To open this dialog just call actionListener. You must not set action attribute on actionLink that opens this dialog nor any other way perform navigation, as actionListener
 * handles navigation
 */
public class DocTypeDetailsDialog extends DynamicTypeDetailsDialog<DocumentType, DocTypeDetailsDialog.DocTypeDialogSnapshot> {
    private static final long serialVersionUID = 1L;
    public static final String BEAN_NAME = "DocTypeDetailsDialog";
    public static final String PARAM_USE_PUBLIC_TEMPLATE = "PARAM_USE_PUBLIC_TEMPLATE";

    // START: Block beans
    private FollowupAssocsListBean followupAssocsListBean;
    private ReplyAssocsListBean replyAssocsListBean;
    private DefaultLocationBean defaultLocationBean;

    public DocTypeDetailsDialog() {
        super(DocumentType.class);
    }

    /**
     * Contains fields that contain state to be used when restoring dialog
     */
    static class DocTypeDialogSnapshot extends DynamicTypeDetailsDialog.DynTypeDialogSnapshot<DocumentType> {
        private static final long serialVersionUID = 1L;

        @Override
        public String getOpenDialogNavigationOutcome() {
            return AlfrescoNavigationHandler.DIALOG_PREFIX + "docTypeDetailsDialog";
        }
    }

    @Override
    protected DocTypeDialogSnapshot newSnapshot() {
        return new DocTypeDialogSnapshot();
    }

    @Override
    protected void resetFields() {
        super.resetFields();
        followupAssocsListBean.resetOrInit(null);
        replyAssocsListBean.resetOrInit(null);
        defaultLocationBean.resetOrInit(null);
        // don't assign null to injected beans
    }

    // START: jsf actions/accessors

    @Override
    protected void updateDialogState(DocumentType documentType, DocTypeDialogSnapshot currentSnapshot, NodeRef docTypeVersionRef) {
        super.updateDialogState(documentType, currentSnapshot, docTypeVersionRef);
        followupAssocsListBean.resetOrInit(this);
        replyAssocsListBean.resetOrInit(this);
        defaultLocationBean.resetOrInit(this);
    }

    /** used by other property sheet */
    public List<SelectItem> getPublicEmailTemplates(FacesContext context, UIInput selectComponent) {
        return getEmailTemplates("document_choose", "");
    }

    /** used by other property sheet */
    public List<SelectItem> getNonpublicEmailTemplates(FacesContext context, UIInput selectComponent) {
        return getEmailTemplates("document_use_public_template", PARAM_USE_PUBLIC_TEMPLATE);
    }

    private List<SelectItem> getEmailTemplates(String defaultValueLabel, String defaultValue) {
        List<SelectItem> emailTemplates = new ArrayList<>();
        emailTemplates.add(new SelectItem(defaultValue, MessageUtil.getMessage(FacesContext.getCurrentInstance(), defaultValueLabel)));
        for (UnmodifiableDocumentTemplate template : getDocumentTemplateService().getEmailTemplates()) {
            String templateName = FilenameUtils.getBaseName(template.getName());
            emailTemplates.add(new SelectItem(template.getNodeRef().toString(), templateName));
        }
        return emailTemplates;
    }

    /** used by jsp */
    public boolean isShowSystematicComment() {
        return StringUtils.isNotBlank(getCurrentSnapshot().getDynType().getSystematicComment());
    }

    /** used by jsp */
    public boolean isDocumentWorkflowEnabled() {
        return getWorkflowConstantsBean().isDocumentWorkflowEnabled();
    }

    /** used by jsp */
    public FollowupAssocsListBean getFollowupAssocsListBean() {
        return followupAssocsListBean;
    }

    /** injected by spring */
    public void setFollowupAssocsListBean(FollowupAssocsListBean followupAssocsListBean) {
        this.followupAssocsListBean = followupAssocsListBean;
    }

    /** used by jsp */
    public ReplyAssocsListBean getReplyAssocsListBean() {
        return replyAssocsListBean;
    }

    /** injected by spring */
    public void setReplyAssocsListBean(ReplyAssocsListBean replyAssocsListBean) {
        this.replyAssocsListBean = replyAssocsListBean;
    }

    /** used by jsp */
    public DefaultLocationBean getDefaultLocationBean() {
        return defaultLocationBean;
    }

    /** injected by spring */
    public void setDefaultLocationBean(DefaultLocationBean defaultLocationBean) {
        this.defaultLocationBean = defaultLocationBean;
    }

    /** JSP */
    @Override
    public boolean isAddFieldVisible() {
        return true;
    }

    // END: jsf actions/accessors

    public boolean isAdministrator() {
        return BeanHelper.getUserService().isAdministrator();
    }

    public boolean isDocumentAutoSendingEnabled() {
        return BeanHelper.getUserService().isAdministrator() && BeanHelper.getApplicationConstantsBean().isDocumentAutoSendingEnabled();
    }

    public void changeDocTypeAutoSendAllowed(final ValueChangeEvent event) {
        ComponentUtil.executeLater(PhaseId.INVOKE_APPLICATION, getPropertySheet(), new Closure() {
            @Override
            public void execute(Object input) {
                getCurrentNode().getProperties().put(DocumentAdminModel.Props.DOC_TYPE_AUTO_SEND_ALLOWED.toString(), BooleanUtils.isTrue((Boolean) event.getNewValue()));
                if (getPropertySheet() != null) {
                    getPropertySheet().getChildren().clear();
                    getPropertySheet().getClientValidations().clear();
                    getPropertySheet().setMode(null);
                    getPropertySheet().setNode(null);
                }
            }
        });
    }

    public boolean isDocTypeAutoSendAllowed() {
        return isAdministrator() && BooleanUtils.isTrue((Boolean) getCurrentNode().getProperties().get(DocumentAdminModel.Props.DOC_TYPE_AUTO_SEND_ALLOWED));
    }

    public boolean isTemplateChangeDisabled() {
        return !isDocTypeAutoSendAllowed();
    }
}
