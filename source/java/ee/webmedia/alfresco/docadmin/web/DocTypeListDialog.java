package ee.webmedia.alfresco.docadmin.web;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentAdminService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import ee.webmedia.alfresco.classificator.enums.DocListUnitStatus;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.functions.model.UnmodifiableFunction;
import ee.webmedia.alfresco.series.model.UnmodifiableSeries;
import org.alfresco.web.ui.common.component.PickerSearchParams;

import ee.webmedia.alfresco.docadmin.service.DocumentAdminService;
import ee.webmedia.alfresco.docadmin.service.DocumentType;
import ee.webmedia.alfresco.docadmin.service.DynamicType;
import ee.webmedia.alfresco.utils.MessageUtil;
import org.apache.commons.collections4.CollectionUtils;

public class DocTypeListDialog extends DynamicTypeListDialog<DocumentType> {
    private static final long serialVersionUID = 1L;

    private List<UnmodifiableSeries> allowedSeries = new ArrayList<UnmodifiableSeries>();

    protected DocTypeListDialog() {
        super(DocumentType.class);
    }

    @Override
    public void init(Map<String, String> params) {
        super.init(params);
        allowedSeries = new ArrayList<UnmodifiableSeries>();
        List<UnmodifiableFunction> functions = BeanHelper.getFunctionsService().getAllFunctions();
        for (UnmodifiableFunction function : functions) {
            List<UnmodifiableSeries> allSeries = BeanHelper.getSeriesService().getAllSeriesByFunction(function.getNodeRef(), true);
            for (UnmodifiableSeries series : allSeries) {
                if (!series.isSeriesAutoSendForbidden() && DocListUnitStatus.OPEN.equals(series.getStatus())) {
                    allowedSeries.add(series);
                }
            }
        }
    }

    /**
     * Query callback method executed by the Generic Picker component.
     * This method is part of the contract to the Generic Picker, it is up to the backing bean
     * to execute whatever query is appropriate and return the results.
     * 
     * @param params Search parameters
     * @return An array of SelectItem objects containing the results to display in the picker.
     */
    public SelectItem[] searchUsedDocTypes(PickerSearchParams params) {
        return searchUsedTypes(params, false);
    }

    /**
     * Used by the property sheet as a callback.
     */
    public List<SelectItem> getUsedDocTypes(@SuppressWarnings("unused") FacesContext context, @SuppressWarnings("unused") UIInput selectComponent) {
        return Arrays.asList(searchUsedTypes(null, true));
    }

    @Override
    protected List<? extends DynamicType> loadUsedTypes() {
        return getDocumentAdminService().getDocumentTypes(DocumentAdminService.DONT_INCLUDE_CHILDREN, true);
    }

    public boolean isPanelRendered() {
        return CollectionUtils.isNotEmpty(allowedSeries)
                && BeanHelper.getApplicationConstantsBean().isDocumentAutoSendingEnabled();
    }

    @Override
    protected String getExportFileName() {
        return "documentTypes.xml";
    }

    @Override
    public String getListTitle() {
        return MessageUtil.getMessage("doc_types_list");
    }

    public List<UnmodifiableSeries> getAllowedSeries() {
        return allowedSeries;
    }
}
