package ee.webmedia.alfresco.workflow.model;

import org.alfresco.service.namespace.QName;

public class Signer {


    private QName signerName;
    private QName signerServiceRank;
    private QName signerJobTitle;
    private QName signerOrgStructUnit;
    private QName signerWorkAddress;
    private QName signerEmail;
    private QName signerPhone;

    public QName getSignerName() {
        return signerName;
    }

    public void setSignerName(QName signerName) {
        this.signerName = signerName;
    }

    public QName getSignerServiceRank() {
        return signerServiceRank;
    }

    public void setSignerServiceRank(QName signerServiceRank) {
        this.signerServiceRank = signerServiceRank;
    }

    public QName getSignerJobTitle() {
        return signerJobTitle;
    }

    public void setSignerJobTitle(QName signerJobTitle) {
        this.signerJobTitle = signerJobTitle;
    }

    public QName getSignerOrgStructUnit() {
        return signerOrgStructUnit;
    }

    public void setSignerOrgStructUnit(QName signerOrgStructUnit) {
        this.signerOrgStructUnit = signerOrgStructUnit;
    }

    public QName getSignerWorkAddress() {
        return signerWorkAddress;
    }

    public void setSignerWorkAddress(QName signerWorkAddress) {
        this.signerWorkAddress = signerWorkAddress;
    }

    public QName getSignerEmail() {
        return signerEmail;
    }

    public void setSignerEmail(QName signerEmail) {
        this.signerEmail = signerEmail;
    }

    public QName getSignerPhone() {
        return signerPhone;
    }

    public void setSignerPhone(QName signerPhone) {
        this.signerPhone = signerPhone;
    }


}
