package ee.webmedia.alfresco.workflow.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.model.WorkflowBlockItem;

public class SummaryBlockGroup {

	private WorkflowBlockItem groupMainItem;
	private List<WorkflowBlockItem> groupItems = new ArrayList<>();
	
	public SummaryBlockGroup(WorkflowBlockItem groupMainItem) {
		this.groupMainItem = groupMainItem;
	}

	public WorkflowBlockItem getGroupMainItem() {
		return groupMainItem;
	}

	public List<WorkflowBlockItem> getGroupItems() {
		return groupItems;
	}
	
	public void setupMainItemComment() {
		for (WorkflowBlockItem item : groupItems) {
			if (item.getTaskComment() != null && item.getTaskComment().equals(MessageUtil.getMessage("task_comment_finished_by_group_task"))) {
				continue;
			}
			String outcomeFinishedManualy = MessageUtil.getMessage("task_outcome_finished_manually");
			if (!groupMainItem.getTaskStatus().equals(Status.UNFINISHED.getName())
					&& item.getTaskOutcome() != null && item.getTaskOutcome().contains(outcomeFinishedManualy)) {
				if (groupMainItem.getTaskOutcome().trim().contains(outcomeFinishedManualy)) {
					groupMainItem.setTaskOutcome("");
					groupMainItem.refreshTaskOutcomeMessage();
				}
				continue;
			}
			if (item.getTaskComment() != null && !item.getTaskComment().isEmpty()) {
				groupMainItem.setTaskComment(item.getTaskComment());
				groupMainItem.refreshTaskOutcomeMessage();
				return;
			}
		}
	}
	
}
