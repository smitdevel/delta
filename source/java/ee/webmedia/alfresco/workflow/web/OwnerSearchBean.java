package ee.webmedia.alfresco.workflow.web;

import java.io.Serializable;

import javax.faces.model.SelectItem;

import ee.webmedia.alfresco.common.web.UserContactGroupSearchFilters;
import ee.webmedia.alfresco.utils.MessageUtil;

/**
 * Bean that can be used to search owners(users, -groups, contacts, -groups)
 */
public class OwnerSearchBean implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String BEAN_NAME = "OwnerSearchBean";
    private SelectItem[] ownerSearchFilters;
    private SelectItem[] responsibleOwnerSearchFilters;
    private SelectItem[] reviewOwnerSearchFilters;

    public void init() {
    }

    /**
     * Getter for the task owner search picker filter.
     */
    public SelectItem[] getResponsibleOwnerSearchFilters() {
        if (responsibleOwnerSearchFilters == null) {
            responsibleOwnerSearchFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.USERS_FILTER.getId(), MessageUtil.getMessage("task_owner_users")),
                    new SelectItem(UserContactGroupSearchFilters.CONTACTS_FILTER.getId(), MessageUtil.getMessage("task_owner_contacts")),
            };
        }
        return responsibleOwnerSearchFilters;
    }

    public SelectItem[] getReviewOwnerSearchFilters() {
        if (reviewOwnerSearchFilters == null) {
            reviewOwnerSearchFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.USERS_FILTER.getId(), MessageUtil.getMessage("task_owner_users")),
                    new SelectItem(UserContactGroupSearchFilters.USER_GROUPS_FILTER.getId(), MessageUtil.getMessage("task_owner_usergroups"))
            };
        }
        return reviewOwnerSearchFilters;
    }

    public SelectItem[] getOwnerSearchFilters() {
        if (ownerSearchFilters == null) {
            ownerSearchFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.USERS_FILTER.getId(), MessageUtil.getMessage("task_owner_users")),
                    new SelectItem(UserContactGroupSearchFilters.USER_GROUPS_FILTER.getId(), MessageUtil.getMessage("task_owner_usergroups")),
                    new SelectItem(UserContactGroupSearchFilters.CONTACTS_FILTER.getId(), MessageUtil.getMessage("task_owner_contacts")),
                    new SelectItem(UserContactGroupSearchFilters.CONTACT_GROUPS_FILTER.getId(), MessageUtil.getMessage("task_owner_contactgroups"))
            };
        }
        return ownerSearchFilters;
    }

}
