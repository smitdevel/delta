package ee.webmedia.alfresco.workflow.web.evaluator;

import ee.webmedia.alfresco.workflow.web.MyTasksBean;
import org.alfresco.web.action.evaluator.BaseActionEvaluator;
import org.alfresco.web.app.servlet.FacesHelper;

import javax.faces.context.FacesContext;

public class TaskConfirmationEvaluator extends BaseActionEvaluator {
    private static final long serialVersionUID = 0L;

    @Override
    public boolean evaluate(Object obj) {
        final FacesContext ctx = FacesContext.getCurrentInstance();
        MyTasksBean bean = (MyTasksBean) FacesHelper.getManagedBean(ctx, MyTasksBean.BEAN_NAME);
        return bean.isShowCheckboxes();
    }
}
