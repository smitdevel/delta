package ee.webmedia.alfresco.workflow.search.web;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentSearchService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getJsfBindingHelper;
import static ee.webmedia.alfresco.workflow.model.Status.IN_PROGRESS;
import static ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel.Types.INFORMATION_TASK;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import ee.webmedia.alfresco.document.model.DocumentSpecificModel;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.volume.model.Volume;
import ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel;
import ee.webmedia.alfresco.workflow.search.model.TaskSearchModel;
import ee.webmedia.alfresco.workflow.service.Task;
import ee.webmedia.alfresco.workflow.service.TaskUpdateInfo;
import ee.webmedia.alfresco.workflow.service.WorkflowServiceImpl;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.component.data.UIRichList;
import org.apache.abdera.model.Collection;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.application.jsp.JspStateManagerImpl;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.web.BaseLimitedListDialog;
import ee.webmedia.alfresco.simdhs.CSVExporter;
import ee.webmedia.alfresco.simdhs.DataReader;
import ee.webmedia.alfresco.simdhs.RichListDataReader;
import ee.webmedia.alfresco.workflow.web.TaskInfoListDataProvider;

import java.util.*;

/**
 * Task search results dialog bean.
 */
public class TaskSearchResultsDialog extends BaseLimitedListDialog {

    private static final long serialVersionUID = 1L;
    public static final String BEAN_NAME = "TaskSearchResultsDialog";

    private TaskInfoListDataProvider tasks;
    private Map<NodeRef, Boolean> listCheckboxes;
    private boolean showCheckboxes;
    private Node filter;

    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Throwable {
        // finish button is not used
        return null; // but in case someone clicks finish button twice on the previous dialog,
        // then silently ignore it and stay on the same page
    }

    private void clearRichList() {
        if (getRichList() != null) {
            getRichList().setValue(null);
        }
    }

    @Override
    public String cancel() {
        tasks = null;
        return super.cancel();
    }

    @Override
    public Object getActionsContext() {
        return null;
    }

    @Override
    protected void limitChangedEvent() {
        updateSearchResults();
    }

    private void updateSearchResults() {
        BeanHelper.getTransactionService().getRetryingTransactionHelper().doInTransaction(new RetryingTransactionCallback<Void>() {

            @Override
            public Void execute() throws Throwable {
                doInitialSearch();
                return null;
            }
        }, false);
    }

    @Override
    public void clean() {
        tasks = null;
        filter = null;
    }

    private void doInitialSearch() {
    	Pair<List<NodeRef>, Boolean> results = getDocumentSearchService().searchTaskRefs(filter, AuthenticationUtil.getRunAsUser(), getLimit());
    	results.setSecond(results.getFirst().size() < super.getListCount());
        tasks = new TaskInfoListDataProvider(setLimited(results));
        setShowCheckboxes();
        listCheckboxes = new HashMap<NodeRef, Boolean>();
        for (NodeRef taskRef : tasks.getObjectKeys()) {
            listCheckboxes.put(taskRef, Boolean.FALSE);
        }
        clearRichList();
    }

    private void setShowCheckboxes() {
        Map<String, Object> props = filter.getProperties();
        List<QName> types = (List<QName>) props.get(TaskSearchModel.Props.TASK_TYPE);
        List<String> statuses = (List<String>) props.get(TaskSearchModel.Props.STATUS);
        showCheckboxes = isAdministratorOrDocumentManager()
                && isOwnerNameSelected(props)
                && CollectionUtils.isNotEmpty(types) && types.size() == 1 && INFORMATION_TASK.equals(types.get(0))
                && CollectionUtils.isNotEmpty(statuses) && statuses.size() == 1 && IN_PROGRESS.equals(statuses.get(0));
    }

    private boolean isOwnerNameSelected(Map<String, Object> props) {
        List<String> ownerNames = (List<String>) props.get(TaskSearchModel.Props.OWNER_NAME);
        if (CollectionUtils.isNotEmpty(ownerNames)) {
            for (String ownerName : ownerNames) {
                if (StringUtils.isNotBlank(ownerName)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isAdministratorOrDocumentManager() {
        return BeanHelper.getUserService().isAdministrator() || BeanHelper.getUserService().isDocumentManager();
    }

    public Map<NodeRef, Boolean> getListCheckboxes() {
        return listCheckboxes;
    }

    protected boolean checkTasksSelected() {
        if (!getListCheckboxes().containsValue(Boolean.TRUE)) {
            MessageUtil.addErrorMessage("task_search_cancel_no_task_selected");
            return false;
        }
        return true;
    }

    protected List<NodeRef> getSelectedTasks() {
        final List<NodeRef> tasksToCancel = new ArrayList<NodeRef>();
        for (Map.Entry<NodeRef, Boolean> entry : getListCheckboxes().entrySet()) {
            if (entry.getValue()) {
                tasksToCancel.add(entry.getKey());
            }
        }
        return tasksToCancel;
    }

    public void cancelTasks(@SuppressWarnings("unused") ActionEvent event) {
        if (checkTasksSelected()) {
            BeanHelper.getWorkflowDbService().markForCancellation(getSelectedTasks(), true);
            updateSearchResults();
        }

        // hack for incorrect view id in the next request
        JspStateManagerImpl.ignoreCurrentViewSequenceHack();
    }

    public void exportAsCsv(@SuppressWarnings("unused") ActionEvent event) {
        DataReader dataReader = new RichListDataReader();
        CSVExporter exporter = new CSVExporter(dataReader);
        exporter.export("taskList");

        // hack for incorrect view id in the next request
        JspStateManagerImpl.ignoreCurrentViewSequenceHack();
    }

    public void setup(Node filter) {
        this.filter = filter;
        listCheckboxes = new HashMap<NodeRef, Boolean>();
        resetLimit(true);
        doInitialSearch();
    }

    public TaskInfoListDataProvider getTasks() {
        return tasks;
    }

    public void setRichList(UIRichList richList) {
        getJsfBindingHelper().addBinding(getRichListBindingName(), richList);
    }

    public UIRichList getRichList() {
        return (UIRichList) getJsfBindingHelper().getComponentBinding(getRichListBindingName());
    }

    public boolean isShowCheckboxes() {
        return showCheckboxes;
    }

}
