package ee.webmedia.alfresco.workflow.search.job;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel;
import ee.webmedia.alfresco.workflow.service.Task;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskCancelJob implements StatefulJob {

    private static final Log log = LogFactory.getLog(TaskCancelJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.debug("TaskCancelJob started");
        if (!BeanHelper.getWorkflowService().isTaskCancellationJobInProgress()) {
            log.debug("TaskCancelJob is not active, starting job");
            try {
                cancelTasks();
            } catch (Exception e) {
                log.error("TaskCancelJob :: exception while calling cancelTasks method.", e);
            }
            BeanHelper.getWorkflowService().setTaskCancellationJobInProgress(false);
        } else {
            log.debug("TaskCancelJob is in progress, waiting");
        }
        log.debug("TaskCancelJob ended");
    }

    private void cancelTasks() {
        List<Task> tasks = BeanHelper.getWorkflowDbService().getMarkedForCancellationTasks();
        if (CollectionUtils.isNotEmpty(tasks)) {
            log.info("Tasks for cancellation found: " + tasks.size());
            for (Task task : tasks) {
                try {
                	unfinishTask(task);
                } catch (Exception exp) {
                    log.error(String.format("TaskCancelJob :: exception occurs in task=%s", task.toString()), exp);
                    Map<QName, Serializable> props = new HashMap();
                    props.put(WorkflowSpecificModel.Props.MARKED_FOR_CANCELLATION, false);
                    BeanHelper.getWorkflowDbService().updateTaskProperties(task.getNodeRef(), props);
                }
            }
        }
    }

    private void unfinishTask(Task task) {
        BeanHelper.getWorkflowService().setTasksUnfinished(task.getNodeRef());
    }
}
