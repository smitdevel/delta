package ee.webmedia.alfresco.workflow.search.job;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.workflow.service.Task;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import java.util.List;

public class TaskConfirmJob implements StatefulJob {

    private static final Log log = LogFactory.getLog(TaskConfirmJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.debug("TaskConfirmJob started");
        if (!BeanHelper.getWorkflowService().isTaskConfirmationJobInProgress()) {
            log.debug("TaskConfirmJob is not active, starting job");
            try {
                finishTasks();
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
            BeanHelper.getWorkflowService().setTaskConfirmationJobInProgress(false);
        } else {
            log.debug("TaskConfirmJob is in progress, waiting");
        }
        log.debug("TaskConfirmJob ended");
    }

    private void finishTasks() {
        List<Task> tasks = BeanHelper.getWorkflowDbService().getMarkedForConfirmationTasks();
        if (CollectionUtils.isNotEmpty(tasks)) {
            log.info("Tasks for confirmation found: " + tasks.size());
            for (Task task : tasks) {
                finishTask(task);
            }
        }
    }

    private void finishTask(Task task) {
        BeanHelper.getWorkflowService().setTasksFinished(task.getNodeRef());
    }
}
