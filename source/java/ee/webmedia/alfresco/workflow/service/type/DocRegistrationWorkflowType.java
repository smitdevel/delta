package ee.webmedia.alfresco.workflow.service.type;

import static ee.webmedia.alfresco.common.web.BeanHelper.getFileService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.service.cmr.model.FileInfo;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.file.model.File;
import ee.webmedia.alfresco.document.file.model.FileModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.document.service.DocumentService;
import ee.webmedia.alfresco.document.web.evaluator.RegisterDocumentEvaluator;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.service.Workflow;
import ee.webmedia.alfresco.workflow.service.WorkflowUtil;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEvent;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEventListener;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEventQueue;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEventType;

public class DocRegistrationWorkflowType extends BaseWorkflowType implements WorkflowEventListener {
    private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(DocRegistrationWorkflowType.class);

    private DocumentService documentService;

    @Override
    public void handle(WorkflowEvent event, WorkflowEventQueue queue) {
        if (event.getType() == WorkflowEventType.STATUS_CHANGED && event.getObject() instanceof Workflow
                && WorkflowUtil.isStatus(event.getObject(), Status.IN_PROGRESS)) {
            if (log.isDebugEnabled()) {
                log.debug("Received event STATUS_CHANGED to IN_PROGRESS, registering document");
            }
            final Workflow workflow = (Workflow) event.getObject();
            AuthenticationUtil.runAs(new RunAsWork<Object>() {
                @Override
                public Object doWork() throws Exception {
                    registerDocuments(workflow);
                    return null;
                }

            }, AuthenticationUtil.getSystemUserName());
        }
    }

    private void registerDocuments(final Workflow workflow) {
        CompoundWorkflow compoundWorkflow = workflow.getParent();
        boolean foundDocuments = false;
        if (compoundWorkflow.isDocumentWorkflow()) {
        	NodeRef docRef = compoundWorkflow.getParent();
            if (RegisterDocumentEvaluator.canRegister(new Node(docRef), false)) {
                registerDocument(docRef);
                transformToPdfAndMoveOriginals(docRef);
            } else {
            	String message = MessageUtil.getMessage("workflow_document_already_registred");
                BeanHelper.getUserConfirmHelper().setupAlert(message, null, "#{UserConfirmHelper.reset}", null);
            }
            foundDocuments = true;
            BeanHelper.getMenuBean().processTaskItems();
        } else if (compoundWorkflow.isIndependentWorkflow()) {
        	String docNames = "";
        	Set<NodeRef> docRefs = workflowService.getCompoundWorkflowDocumentRefs(compoundWorkflow);
            for (NodeRef docRef : docRefs) {
                if (RegisterDocumentEvaluator.canRegister(new Node(docRef), false)) {
                    registerDocument(docRef);
                    transformToPdfAndMoveOriginals(docRef);
                } else {
                	docNames += appendDocNames(docRef, docNames);
                }
                foundDocuments = true;
            }
            if (docNames != "") {
                String message = MessageUtil.getMessage("workflow_documents_already_registred", docNames);
            	if (docRefs.size() == 1) {
                	message = MessageUtil.getMessage("workflow_document_already_registred");
            	}
                BeanHelper.getUserConfirmHelper().setupAlert(message, null, "#{UserConfirmHelper.reset}", null);
            }
        } else if (compoundWorkflow.isCaseFileWorkflow()) {
        	String docNames = "";
        	List<NodeRef> docRefs = documentService.getAllDocumentRefsByParentRefWithoutRestrictedAccess(compoundWorkflow.getParent());
            for (NodeRef docRef : docRefs) {
                if (RegisterDocumentEvaluator.canRegister(new Node(docRef))) {
                    registerDocument(docRef);
                } else {
                	docNames += appendDocNames(docRef, docNames);
                }
                foundDocuments = true;
            }
            if (docNames != "") {
                String message = MessageUtil.getMessage("workflow_documents_already_registred", docNames);
            	if (docRefs.size() == 1) {
                	message = MessageUtil.getMessage("workflow_document_already_registred");
            	}
                BeanHelper.getUserConfirmHelper().setupAlert(message, null, "#{UserConfirmHelper.reset}", null);
            }
        }
        if (foundDocuments) {
            BeanHelper.getMenuBean().processTaskItems();
        }
    }
    
    private String appendDocNames(NodeRef docRef, String docNames) {
    	String prefix = ",";
    	if (docNames.equals("")) {
    		prefix = "";
    	}
    	return prefix + BeanHelper.getNodeService().getProperty(docRef, DocumentCommonModel.Props.DOC_NAME);
    }

    private void registerDocument(NodeRef docRef) {
        if (documentService.registerDocumentIfNotRegistered(docRef, true)) {
            BeanHelper.getDocumentTemplateService().updateGeneratedFiles(docRef, true);
        }
    }
    
    private void transformToPdfAndMoveOriginals(NodeRef docRef) {
    	for (File file : getFileService().getAllActiveFiles(docRef)) {
    		if (file.isConvertToPdfIfSigned()) {
    			FileInfo pdf = getFileService().transformToPdf(docRef, file.getNodeRef(), true);
    			pdf.getProperties().put(FileModel.Props.FILE_ORDER_IN_LIST, file.getFileOrderInList());
    			BeanHelper.getNodeService().setProperty(pdf.getNodeRef(), FileModel.Props.FILE_ORDER_IN_LIST, file.getFileOrderInList());
            	getFileService().toggleActive(file.getNodeRef());
    		}
    	}
    }

    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }

}
