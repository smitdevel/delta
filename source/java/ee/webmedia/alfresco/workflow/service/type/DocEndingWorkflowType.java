package ee.webmedia.alfresco.workflow.service.type;

import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.DOC_STATUS;

import java.util.List;
import java.util.Map;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;

import ee.webmedia.alfresco.document.type.model.DocumentType;
import ee.webmedia.alfresco.document.type.model.DocumentTypeModel;
import ee.webmedia.alfresco.classificator.enums.DocumentStatus;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.service.DocumentService;
import ee.webmedia.alfresco.document.web.evaluator.RegisterDocumentEvaluator;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.service.Workflow;
import ee.webmedia.alfresco.workflow.service.WorkflowUtil;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEvent;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEventListener;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEventQueue;
import ee.webmedia.alfresco.workflow.service.event.WorkflowEventType;

public class DocEndingWorkflowType extends BaseWorkflowType implements WorkflowEventListener {
	private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory
			.getLog(DocEndingWorkflowType.class);

	private DocumentService documentService;

	@Override
	public void handle(WorkflowEvent event, WorkflowEventQueue queue) {
		if (event.getType() == WorkflowEventType.STATUS_CHANGED && event.getObject() instanceof Workflow
				&& WorkflowUtil.isStatus(event.getObject(), Status.IN_PROGRESS)) {
			if (log.isDebugEnabled()) {
				log.debug("Received event STATUS_CHANGED to IN_PROGRESS, end document");
			}
			final Workflow workflow = (Workflow) event.getObject();
			AuthenticationUtil.runAs(new RunAsWork<Object>() {
				@Override
				public Object doWork() throws Exception {
					endDocument(workflow);
					return null;
				}

			}, AuthenticationUtil.getSystemUserName());
		}
	}

	private void endDocument(Workflow workflow) {
		CompoundWorkflow compoundWorkflow = workflow.getParent();
		if (compoundWorkflow.isDocumentWorkflow()) {
			NodeRef docRef = compoundWorkflow.getParent();
			finishDocumentIfNeeded(docRef);
		} else if (compoundWorkflow.isIndependentWorkflow()) {
			for (NodeRef docRef : workflowService.getCompoundWorkflowDocumentRefs(compoundWorkflow)) {
				finishDocumentIfNeeded(docRef);
			}
		}
	}
	
	private void finishDocumentIfNeeded(NodeRef docRef) {
		Node document = documentService.getDocument(docRef);
        Map<String, Object> props = document.getProperties();
        String typeId = (String) props.get(DocumentAdminModel.Props.OBJECT_TYPE_ID);
		DocumentType doctype = BeanHelper.getDocumentTypeService().getDocumentType(typeId);
		Boolean finishDocByRegistration = (Boolean) doctype.getProp(DocumentTypeModel.Props.FINISH_DOC_BY_REGISTRATION);
		if (finishDocByRegistration == null || finishDocByRegistration.equals(Boolean.FALSE)) {
			String documentStatus = (String) document.getProperties().get(DocumentDynamicModel.Props.DOC_STATUS);
			if (documentStatus != null && DocumentStatus.WORKING.getValueName().equals(documentStatus)) {
                BeanHelper.getNodeService().setProperty(document.getNodeRef(), DOC_STATUS, DocumentStatus.FINISHED.getValueName());
			}
		}
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}
}
