package ee.webmedia.mobile.alfresco.document;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docadmin.service.DocumentAdminService;
import ee.webmedia.alfresco.docadmin.service.DocumentType;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.utils.WebUtil;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.service.Task;
import ee.webmedia.alfresco.workflow.service.WorkflowService;
import ee.webmedia.mobile.alfresco.common.AbstractBaseController;
import ee.webmedia.mobile.alfresco.document.model.DocumentWorkflowItem;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static ee.webmedia.alfresco.common.web.BeanHelper.getDocumentAdminService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("/document")
public class DocumentController extends AbstractBaseController {
    private static org.apache.commons.logging.Log logger = org.apache.commons.logging.LogFactory.getLog(DocumentController.class);
    private static final String SIGNING_FLOW_ID_ATTR = "signingFlowId";
    public static final String DOCUMENT_MAPPING = "document";
    private static final String DOCUMENT_VIEW = "document";

    @Resource(name = "WmWorkflowService")
    protected WorkflowService workflowService;

    private static final long serialVersionUID = 1L;

    @RequestMapping(method = RequestMethod.GET)
    public String document(Model model) {
        setup(model);
        setPageTitle(model, translate("document.home"));

        return "document/document";
    }

    @RequestMapping(value = "/{documentNodeId}", method = GET)
    public String setupDocument(@PathVariable String documentNodeId, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,
                                        HttpSession session) {
        super.setup(model, request);
        logger.debug("setupDocument()...");
        if(documentNodeId == null){
            logger.debug("documentNodeId is NULL! Return to Document home...");
            return document(model);
        }

        Long signingFlowId = (Long) model.asMap().get(SIGNING_FLOW_ID_ATTR);
        return initDocument(documentNodeId, model, signingFlowId, redirectAttributes, session, request);
    }

    private String initDocument(String documentNodeId, Model model, Long signingFlowId, RedirectAttributes redirectAttributes, HttpSession session,
                                        HttpServletRequest request) {
        logger.info("initDocument()...");
        setup(model);
        setPageTitle(model, translate("document.home"));

        String ownerId = request.getRemoteUser();
        if(ownerId != null){
            logger.debug("SESSION USERNAME: " + ownerId);
        } else {
            logger.error("USERNAME: NULL!");
            addRedirectErrorMsg(redirectAttributes, "document_user_not_found");
            return "redirect:/m/";
        }

        NodeRef docNodeRef = WebUtil.getNodeRefFromNodeId(documentNodeId);

        logger.info("documentNodeRef: " + docNodeRef);
        if (docNodeRef == null) {
            logger.info("documentNodeRef is NULL! Redirect to m/tasks");
            addRedirectErrorMsg(redirectAttributes, "document_error_noderef");
            return "redirect:/m/tasks";
        }
        //Node document = getDocumentService().getDocument(docNodeRef);
        //        //Map<String, Object> props = document.getProperties();



        List<CompoundWorkflow> cwnfs = BeanHelper.getDocumentService().getAllWorkflowNodeRefs(docNodeRef, ownerId);
        if(cwnfs == null){
            logger.info("compoundWorkflowNodeRef is NULL! Redirect to m/tasks");
            addRedirectErrorMsg(redirectAttributes, "document_no_compoundWorkflow_error");
            return "redirect:/m/tasks";
        }

        int total = cwnfs.size();
        logger.debug("CompoundWorkflow found: " + total);
        int inProgress = 0;
        List<NodeRef> activeNodeRefs = new ArrayList<>();

        for(CompoundWorkflow cwnf : cwnfs){
            //logger.debug("CompoundWorkflow: " + cwnf.toString());
            if(cwnf.getStatus().equals(Status.IN_PROGRESS.getName())){
                inProgress++;
                activeNodeRefs.add(cwnf.getNodeRef());
            }
        }

        if(inProgress == 0){
            addRedirectErrorMsg(redirectAttributes, "document_no_compoundWorkflow_error");
            return "redirect:/m/tasks";
        } else if(inProgress == 1){
            logger.debug("compound-workflow node id: " + activeNodeRefs.get(0).getId() + "... REDIRECT...");
            return "redirect:/m/compound-workflow/details/" + activeNodeRefs.get(0).getId();
        } else {

            List<ee.webmedia.mobile.alfresco.workflow.model.Task> activeContainerTasks = new ArrayList<>();

            for (Task task : workflowService.getTasksInProgress(docNodeRef)) {
                ee.webmedia.mobile.alfresco.workflow.model.Task containerTask = new ee.webmedia.mobile.alfresco.workflow.model.Task();
                // task.title
                // task.kind
                // task.overDue
                // task.dueDate
                // task.resolution
                // task.creatorName
                // task.senderName
                // task.compoundWorkflowRef.id

                containerTask.setId(task.getNodeRef().getId());
                containerTask.setTitle((String) getNodeService().getProperty(docNodeRef, DocumentCommonModel.Props.DOC_NAME));
                containerTask.setKind(getDocTypeName(task.getType().getLocalName()));
                containerTask.setType(task.getType());
                containerTask.setResolution(task.getResolution());
                containerTask.setCreatorName(task.getCreatorName());
                containerTask.setSenderName(task.getOwnerName());
                containerTask.setDueDate(task.getDueDate());
                containerTask.setViewedByOwner(task.getViewedByOwner());
                containerTask.setNodeRef(task.getNodeRef());
                containerTask.setCompoundWorkflowRefId(task.getCompoundWorkflowId());
                if(task.getCompoundWorkflowNodeRef() != null){
                    logger.debug("getCompoundWorkflowNodeRef found!");
                    containerTask.setCompoundWorkflowRef(task.getCompoundWorkflowNodeRef());
                } else {
                    logger.debug("getCompoundWorkflowNodeRef is NULL!");
                    if(task.getCompoundWorkflowId() != null){
                        logger.debug("getCompoundWorkflowNodeRefId found!");
                        NodeRef cfNodeRef = WebUtil.getNodeRefFromNodeId(task.getCompoundWorkflowId());
                        containerTask.setCompoundWorkflowRef(cfNodeRef);
                    } else {
                        logger.debug("getCompoundWorkflowNodeRefId is NULL!");
                    }
                }
                containerTask.setComment(task.getComment());
                //containerTask.setActions(task.get);

                // property from parent workflow
                //private boolean isSignTogether(task.is);
                //containerTask.setReviewTaskOutcome(task.getOut);
                //containerTask.setCommentLabel();
                //containerTask.setTypeStr();
                //containerTask.setFiles(task.getFiles());
                //containerTask.setCompletedDateTime();
                //containerTask.setOwnerNameWithSubstitute();
                //containerTask.setCommentAndLinks();


                logger.debug("----------------------------------------------------");
                logger.debug("TASK getNodeRef.id: " +task.getNodeRef().getId());
                logger.debug("getCategory: " + task.getCategory());
                logger.debug("getNodeRef: " + task.getNodeRef());
                logger.debug("getComment: " + task.getComment());
                logger.debug("getCommentAndLinks: " + task.getCommentAndLinks());
                logger.debug("getCompletedDateTime: " + task.getCompletedDateTime());
                logger.debug("getCompoundWorkflowId: " + task.getCompoundWorkflowId());
                logger.debug("getCompoundWorkflowNodeRef: " + task.getCompoundWorkflowNodeRef());
                logger.debug("getCompoundWorkflowTitle: " + task.getCompoundWorkflowTitle());
                logger.debug("getConfirmedDueDate: " + task.getConfirmedDueDate());
                logger.debug("getCreatorEmail: " + task.getCreatorEmail());
                logger.debug("getCreatorId: " + task.getCreatorId());
                logger.debug("getCreatorInstitutionCode: " + task.getCreatorInstitutionCode());
                logger.debug("getCreatorInstitutionName: " + task.getCreatorInstitutionName());
                logger.debug("getCssStyleClass: " + task.getCssStyleClass());
                logger.debug("getDueDate: " + task.getDueDate());
                logger.debug("getDueDateDays: " + task.getDueDateDays());
                logger.debug("getDueDateStr: " + task.getDueDateStr());
                logger.debug("getOutcome: " + task.getOutcome());
                logger.debug("getOutcomeAndComments: " + task.getOutcomeAndComments());
                logger.debug("----------------------------------------------------");

                String taskOwnerId = task.getOwnerId();

                if(taskOwnerId != null && taskOwnerId.equals(ownerId)){
                    logger.debug("-- TASK OWNER: TRUE");
                    activeContainerTasks.add(containerTask);
                } else {
                    logger.debug("-- TASK OWNER: FALSE");
                }
            }


            model.addAttribute("tasks", activeContainerTasks);
            // Load document.jsp
            return "document/document";
        }

        /*
        NodeRef cwnf = BeanHelper.getDocumentService().getWorkflowNodeRef(docNodeRef, ownerId);

        if (cwnf == null) {
            logger.info("compoundWorkflowNodeRef is NULL! Redirect to m/tasks");
            addRedirectErrorMsg(redirectAttributes, "document_no_compoundWorkflow_error");
            return "redirect:/m/tasks";
        }
        */
    }

    private DocumentWorkflowItem getWorkFlowItem(NodeRef docNodeRef, String docHeader, CompoundWorkflow compoundWorkflow){
        DocumentWorkflowItem item = new DocumentWorkflowItem();
        item.setDocumentNodeRef(docNodeRef);
        item.setDocumentHeader(docHeader);
        item.setCompundWorkflowNodeRef(compoundWorkflow.getNodeRef());
        item.setStatus(compoundWorkflow.getStatus());
        item.setCreatedDateTime(compoundWorkflow.getCreatedDateTime());
        item.setEndedDateTime(compoundWorkflow.getEndedDateTime());
        item.setStartedDateStr(compoundWorkflow.getStartedDateTime());

        return item;
    }

    private String getDocTypeName(String docType) {
        if (StringUtils.isBlank(docType)) {
            return "";
        }

        DocumentType documentType = getDocumentAdminService().getDocumentType(docType, DocumentAdminService.DONT_INCLUDE_CHILDREN);
        if (documentType == null) {
            return "";
        }

        return documentType.getName();
    }
}
