package ee.webmedia.mobile.alfresco.document.model;

import ee.webmedia.alfresco.workflow.model.Status;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.Date;

public class DocumentWorkflowItem {
    NodeRef documentNodeRef;
    String documentHeader;
    NodeRef compundWorkflowNodeRef;
    String status;
    Date createdDateTime;
    Date endedDateTime;
    Date startedDateStr;

    public NodeRef getDocumentNodeRef() {
        return documentNodeRef;
    }

    public void setDocumentNodeRef(NodeRef documentNodeRef) {
        this.documentNodeRef = documentNodeRef;
    }

    public String getDocumentHeader() {
        return documentHeader;
    }

    public void setDocumentHeader(String documentHeader) {
        this.documentHeader = documentHeader;
    }

    public NodeRef getCompundWorkflowNodeRef() {
        return compundWorkflowNodeRef;
    }

    public void setCompundWorkflowNodeRef(NodeRef compundWorkflowNodeRef) {
        this.compundWorkflowNodeRef = compundWorkflowNodeRef;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getEndedDateTime() {
        return endedDateTime;
    }

    public void setEndedDateTime(Date endedDateTime) {
        this.endedDateTime = endedDateTime;
    }

    public Date getStartedDateStr() {
        return startedDateStr;
    }

    public void setStartedDateStr(Date startedDateStr) {
        this.startedDateStr = startedDateStr;
    }
}
