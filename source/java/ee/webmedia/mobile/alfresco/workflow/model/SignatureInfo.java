package ee.webmedia.mobile.alfresco.workflow.model;

import java.util.Date;

public class SignatureInfo {
    private String signerName;
    private Date signDate;

    public String getSignerName() {
        return signerName;
    }

    public void setSignerName(String signerName) {
        this.signerName = signerName;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }
}
