package ee.webmedia.mobile.alfresco.workflow.model;

import ee.smit.common.files.FilenameUtil;
import org.alfresco.service.cmr.repository.NodeRef;

import ee.webmedia.alfresco.document.file.model.File;
import org.apache.commons.io.FilenameUtils;

import java.util.List;

public class TaskFile {

    private String name;
    private NodeRef nodeRef;
    private boolean deleted;
    private String deleteUrl;
    private long size;
    private String displayName;
    private String readOnlyUrl;
    private Boolean viewDocumentFilesPermission;
    private String fileType;

    private List<SignatureInfo> signatureInfos;
    private List<SignatureData> signatureData;

    public TaskFile(String name, NodeRef nodeRef) {
        this.name = name;
        this.nodeRef = nodeRef;
        fileType = name.substring(name.lastIndexOf(".") + 1, name.length()); // FilenameUtils.getExtension(name).toLowerCase();

        if (fileType != null) {
            if (ee.smit.common.files.FilenameUtil.isDigiDocFile(displayName)) {
                fileType = "digidoc";
            }
        }
    }

    public TaskFile(File file) {
        nodeRef = file.getNodeRef();
        size = file.getSize();
        displayName = file.getDisplayName();
        readOnlyUrl = file.getReadOnlyUrl();
        viewDocumentFilesPermission = file.isViewDocumentFilesPermission();

        fileType = FilenameUtils.getExtension(file.getDisplayName()).toLowerCase();
        if (fileType != null) {
            if (ee.smit.common.files.FilenameUtil.isDigiDocFile(displayName)) {
                fileType = "digidoc";
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NodeRef getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(NodeRef nodeRef) {
        this.nodeRef = nodeRef;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setDeleteUrl(String deleteUrl) {
        this.deleteUrl = deleteUrl;
    }

    public String getDeleteUrl() {
        return deleteUrl;
    }

    public long getSize() {
        return size;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getReadOnlyUrl() {
        return readOnlyUrl;
    }

    public Boolean getViewDocumentFilesPermission() {
        return viewDocumentFilesPermission;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public List<SignatureInfo> getSignatureInfos() {
        return signatureInfos;
    }

    public void setSignatureInfos(List<SignatureInfo> signatureInfos) {
        this.signatureInfos = signatureInfos;
    }

    public List<SignatureData> getSignatureData() {
        return signatureData;
    }

    public void setSignatureData(List<SignatureData> signatureData) {
        this.signatureData = signatureData;
    }
}
