package ee.webmedia.mobile.alfresco.workflow;

import static ee.webmedia.alfresco.common.web.BeanHelper.*;
import static ee.webmedia.alfresco.document.model.DocumentCommonModel.Props.*;
import static ee.webmedia.alfresco.workflow.web.WorkflowBlockBean.isMobileIdOutcome;
import static ee.webmedia.alfresco.workflow.web.WorkflowBlockBean.isMobileIdOutcomeAndMobileIdDisabled;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.text.ParseException;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ee.webmedia.alfresco.classificator.enums.AccessRestriction;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.cmr.lock.LockStatus;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.app.servlet.UploadFileBaseServlet;
import org.alfresco.web.bean.FileUploadBean;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ee.webmedia.alfresco.common.propertysheet.upload.UploadFileInput.FileWithContentType;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.web.DocumentLockHelperBean;
import ee.webmedia.alfresco.document.file.model.File;
import ee.webmedia.alfresco.document.model.Document;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.template.service.DocumentTemplateService;
import ee.webmedia.alfresco.utils.CalendarUtil;
import ee.webmedia.alfresco.utils.WebUtil;
import ee.webmedia.alfresco.workflow.exception.WorkflowActiveResponsibleTaskException;
import ee.webmedia.alfresco.workflow.exception.WorkflowChangedException;
import ee.webmedia.alfresco.workflow.model.Comment;
import ee.webmedia.alfresco.workflow.model.CompoundWorkflowType;
import ee.webmedia.alfresco.workflow.model.RelatedUrl;
import ee.webmedia.alfresco.workflow.model.Status;
import ee.webmedia.alfresco.workflow.model.WorkflowBlockItem;
import ee.webmedia.alfresco.workflow.model.WorkflowCommonModel;
import ee.webmedia.alfresco.workflow.model.WorkflowSpecificModel;
import ee.webmedia.alfresco.workflow.service.CompoundWorkflow;
import ee.webmedia.alfresco.workflow.service.SignatureTask;
import ee.webmedia.alfresco.workflow.service.Workflow;
import ee.webmedia.alfresco.workflow.service.WorkflowConstantsBean;
import ee.webmedia.alfresco.workflow.service.WorkflowDbService;
import ee.webmedia.alfresco.workflow.service.WorkflowUtil;
import ee.webmedia.alfresco.workflow.web.CompoundWorkflowDialog;
import ee.webmedia.alfresco.workflow.web.SigningFlowContainer;
import ee.webmedia.alfresco.workflow.web.WorkflowBlockBean;
import ee.webmedia.mobile.alfresco.common.AbstractBaseController;
import ee.webmedia.mobile.alfresco.workflow.model.DueDateExtensionForm;
import ee.webmedia.mobile.alfresco.workflow.model.InProgressTasksForm;
import ee.webmedia.mobile.alfresco.workflow.model.LockMessage;
import ee.webmedia.mobile.alfresco.workflow.model.MobileIdSignatureAjaxRequest;
import ee.webmedia.mobile.alfresco.workflow.model.Task;
import ee.webmedia.mobile.alfresco.workflow.model.TaskFile;

@Controller
public class CompundWorkflowDetailsController extends AbstractCompoundWorkflowController {

    private static final String TASK_COUNT_ATTR = "taskCount";

    private static final String WORKFLOW_BLOCK_ITEMS_ATTR = "workflowBlockItems";

    private static org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(CompundWorkflowDetailsController.class);

    private static final String ACTION_SAVE = "save";

    private static final String DELEGATE_TASK = "delegate";
    private static final String EXTEND_DUE_DATE = "extendDueDate";
    private static final String SIGNING_FLOW_ID_ATTR = "signingFlowId";
    private static final long serialVersionUID = 1L;
    public static final String COMPOUND_WORKFLOW_NODE_ID = "compoundWorkflowNodeId";
    public static final String COMPOUND_WORKFLOW_DETAILS_MAPPING = "compound-workflow/details";
    private static final List<QName> SUPPORTED_TASK_TYPES = new ArrayList<QName>(Arrays.asList(AbstractBaseController.TASK_TYPES));
    private static final String TITLE_SUFFIX = ".title";
    private static Map<Integer, String> reviewTaskOutcomes;
    // private static Map<Integer, String> reviewTaskOutcomesSelection;

    @Resource
    private SigningFlowHolder signingFlowHolder;
    @Resource
    private WorkflowDbService workflowDbService;
    @Resource
    private DocumentTemplateService documentTemplateService;
    @Resource
    private WorkflowConstantsBean workflowConstantsBean;

    @RequestMapping(value = COMPOUND_WORKFLOW_DETAILS_MAPPING + "/{compoundWorkflowNodeId}", method = GET)
    public String setupCompoundWorkflow(@PathVariable String compoundWorkflowNodeId, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,
                                        HttpSession session) {
        super.setup(model, request);
        LOG.info("setupCompoundWorkflow()...");
        Long signingFlowId = (Long) model.asMap().get(SIGNING_FLOW_ID_ATTR);
        return initCompoundWorkflow(compoundWorkflowNodeId, model, signingFlowId, redirectAttributes, session, request);
    }

    // compoundWorkflow is initialized here, not in @ModelAttribute method, because it may be null
    private String initCompoundWorkflow(String compoundWorkflowNodeId, Model model, Long signingFlowId, RedirectAttributes redirectAttributes, HttpSession session,
                                        HttpServletRequest request) {
        NodeRef compoundWorkflowNodeRef = WebUtil.getNodeRefFromNodeId(compoundWorkflowNodeId);
        if (compoundWorkflowNodeRef == null) {
            LOG.info("compoundWorkflowNodeRef is NULL! Redirect to m/tasks");
            addRedirectErrorMsg(redirectAttributes, "workflow_compound_edit_error_docDeleted");
            return "redirect:/m/tasks";
        }
        LOG.info("compoundWorkflowNodeRef: " + compoundWorkflowNodeRef);
        // TODO: optimize loading compound workflow if possible (all data may not be needed in mobile version)
        CompoundWorkflow compoundWorkflow = workflowService.getCompoundWorkflow(compoundWorkflowNodeRef);
        /*
        if (!(compoundWorkflow.isIndependentWorkflow() && WorkflowUtil.hasInProgressTaskOfType(compoundWorkflow, AuthenticationUtil.getRunAsUser(), SUPPORTED_TASK_TYPES))) {
            addRedirectErrorMsg(redirectAttributes, "redirect.unavailable." + ExternalAccessPhaseListener.OUTCOME_COMPOUND_WORKFLOW_NODEREF);
            return "redirect:/m/tasks";
        }
        */
        List<ee.webmedia.alfresco.workflow.service.Task> myTasks = workflowService.getMyTasksInProgress(Arrays.asList(compoundWorkflow.getNodeRef()));
        if(myTasks == null){
            LOG.info("MY TASKS found: NULL");
        } else {
            LOG.info("MY TASKS found: " + myTasks.size());
        }
        Map<String, Task> myTasksMap = new LinkedHashMap<>(); // Must preserve insertion order
        Map<NodeRef, List<Pair<String, String>>> taskOutcomeButtons = new HashMap<>();
        SigningFlowContainer signingFlow = null;
        SignatureTask signatureTask = null;
        if (signingFlowId != null) {
            signingFlow = signingFlowHolder.getSigningFlow(signingFlowId);
            signatureTask = signingFlow.getSignatureTask();
        }
        String buttonLabelPrefix = "workflow.task.type.";
        List<ee.webmedia.alfresco.workflow.service.Task> delegableTasks = new ArrayList<>();
        for (ee.webmedia.alfresco.workflow.service.Task task : myTasks) {
            ee.webmedia.mobile.alfresco.workflow.model.Task formTask = new ee.webmedia.mobile.alfresco.workflow.model.Task(task);
            formTask.setSignTogether(isSignTogether(task));
            QName taskType = task.getType();
            if (!SUPPORTED_TASK_TYPES.contains(taskType)) {
                continue;
            }
            formTask.setTypeStr(TASK_TYPE_TO_KEY_MAPPING.get(taskType));
            formTask.setType(taskType);
            if (!Boolean.TRUE.equals(task.getViewedByOwner())) {
                getWorkflowDbService().updateTaskSingleProperty(task, WorkflowCommonModel.Props.VIEWED_BY_OWNER, Boolean.TRUE, task.getWorkflowNodeRef());
            }
            myTasksMap.put(task.getNodeRef().toString(), formTask);
            if (signatureTask != null && task.getNodeRef().equals(signatureTask.getNodeRef())) {
                formTask.setComment(signatureTask.getComment());
            }
            String saveLabel = buttonLabelPrefix + taskType.getLocalName() + ".save.title";
            String label = buttonLabelPrefix + taskType.getLocalName() + ".outcome.";
            List<Pair<String, String>> taskOutcomeBtnLabels = new ArrayList<>();
            if (task.isType(WorkflowSpecificModel.Types.OPINION_TASK,
                    WorkflowSpecificModel.Types.REVIEW_TASK,
                    WorkflowSpecificModel.Types.EXTERNAL_REVIEW_TASK,
                    WorkflowSpecificModel.Types.CONFIRMATION_TASK)) {
                taskOutcomeBtnLabels.add(Pair.newInstance(ACTION_SAVE, saveLabel));
            }
            if (task.isType(WorkflowSpecificModel.Types.ASSIGNMENT_TASK, WorkflowSpecificModel.Types.REVIEW_TASK, WorkflowSpecificModel.Types.OPINION_TASK, WorkflowSpecificModel.Types.CONFIRMATION_TASK, WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK, WorkflowSpecificModel.Types.GROUP_ASSIGNMENT_TASK)) {
                formTask.setCommentLabel("workflow.task." + taskType.getLocalName() + ".prop.comment");
            } else {
                formTask.setCommentLabel("workflow.task.prop.comment");
            }
            if (task.isType(WorkflowSpecificModel.Types.OPINION_TASK) || task.isType(WorkflowSpecificModel.Types.REVIEW_TASK) 
            		|| task.isType(WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK)) {
                addTaskFiles(session, request, task, formTask);
            }
            
            if (task.isType(WorkflowSpecificModel.Types.DUE_DATE_EXTENSION_TASK)) {
                formTask.setProposedDueDate(task.getProposedDueDate());
            }
            
            if (task.isType(WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK)) {
                formTask.setSendOrderAssignmentCompletedEmail(task.getPropBoolean(WorkflowSpecificModel.Props.SEND_ORDER_ASSIGNMENT_COMPLETED_EMAIL));
            }

            for (int outcomeIndex = 0; outcomeIndex < task.getOutcomes(); outcomeIndex++) {
                if (isIdCardOutcome(taskType, outcomeIndex) || isMobileIdOutcomeAndMobileIdDisabled(taskType, outcomeIndex)) {
                    continue;
                }
                if (WorkflowSpecificModel.Types.REVIEW_TASK.equals(taskType)
                        || WorkflowSpecificModel.Types.EXTERNAL_REVIEW_TASK.equals(taskType)) {
                    taskOutcomeBtnLabels.add(Pair.newInstance(Integer.valueOf(outcomeIndex).toString(), "workflow.task.type.reviewTask.button.label"));
                    break;
                }
                taskOutcomeBtnLabels.add(Pair.newInstance(Integer.valueOf(outcomeIndex).toString(), label + outcomeIndex + TITLE_SUFFIX));
            }
            if (isDelegatableTask(task)) {
                String buttonKey = task.isType(WorkflowSpecificModel.Types.INFORMATION_TASK) ? "workflow.task.delegation.button.delegate.informationTask"
                        : "workflow.task.delegation.button.delegate";
                taskOutcomeBtnLabels.add(Pair.newInstance(DELEGATE_TASK, buttonKey));
            }
            if (task.isType(WorkflowSpecificModel.Types.ASSIGNMENT_TASK) || task.isType(WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK)) {
                delegableTasks.add(task);
                if (BeanHelper.getMDeltaUIParams().buttonDueDateExtensionIsActive()) {
                    taskOutcomeBtnLabels.add(Pair.newInstance(EXTEND_DUE_DATE, "workflow.task.type.assignmentTask.dueDateExtension"));
                }
            }
            taskOutcomeButtons.put(task.getNodeRef(), taskOutcomeBtnLabels);
        }
        InProgressTasksForm inProgressTasksForm = new InProgressTasksForm(myTasksMap, compoundWorkflowNodeRef, BeanHelper.getGeneralService()
                .getPrimaryParent(compoundWorkflowNodeRef).getNodeRef());
        if (signingFlow != null) {
            inProgressTasksForm.setSigningFlowId(signingFlowId);
            inProgressTasksForm.setSigningFlowView(signingFlow.getSigningFlowView().name());
            inProgressTasksForm.setMobileIdChallengeId(signingFlow.getChallengeId());
            inProgressTasksForm.setPhoneNumber(signingFlow.getPhoneNumber());
            inProgressTasksForm.setDefaultSigningNumber(signingFlow.isDefaultTelephoneForSigning());
            if (BeanHelper.getApplicationConstantsBean().isInsertSignatureResolution()) {
            	inProgressTasksForm.setResolution(signingFlow.getResolution());
            }
        }
        model.addAttribute("inProgressTasksForm", inProgressTasksForm);
        model.addAttribute("taskOutcomeButtons", taskOutcomeButtons);
        model.addAttribute("compoundWorkflow", compoundWorkflow);
        model.addAttribute("reviewTaskOutcomes", getReviewTaskOutcomes());  // pass Map of outcomes, with selected excluded



        // model.addAttribute("tempOutcome", getSelectedOutcome());        // pass selected outcome

        model.addAttribute("workflowOwnerName", compoundWorkflow.getOwnerName());
        model.addAttribute("workflowStatus", compoundWorkflow.getStatus());
        model.addAttribute("isDocumentWorkflowType", Boolean.FALSE);

        if (workflowConstantsBean.isWorkflowTitleEnabled()) {
            model.addAttribute("compoundWorkflowTitle", compoundWorkflow.getTitle());
        }

        if (CompoundWorkflowType.INDEPENDENT_WORKFLOW.equals(CompoundWorkflowType.valueOf( (String) compoundWorkflow.getProp(WorkflowCommonModel.Props.TYPE)))) {
            setupComments(model, compoundWorkflowNodeId);
            setupObjects(model, compoundWorkflow);
            model.addAttribute("isIndependent", true);
        } else {
            model.addAttribute("isIndependent", false);
            NodeRef containerRef = compoundWorkflow.getParent();
            LOG.debug("Container REF: " + containerRef);
            model.addAttribute("containerRef", containerRef);
            //List<File> files = getFileService().getAllActiveFilesIncludingDigiDocSubItems(containerRef);
            List<File> files = getFileService().getAllActiveAndInactiveFiles(containerRef);
            List<TaskFile> activeFiles = new ArrayList<>();
            List<TaskFile> inActiveFiles = new ArrayList<>();
            for (File file: files) {
                LOG.debug("File name: " + file.getName());
                LOG.debug("File display name: " + file.getDisplayName());
                LOG.debug("File mimetype: " + file.getMimeType());
                LOG.debug("File size: " + file.getSize());
                LOG.debug("File read only URL: " + file.getReadOnlyUrl());
                LOG.debug("File download URL: " + file.getDownloadUrl());
                LOG.debug("File isActive: " + file.isActive());
                LOG.debug("File isActiveAndNotDigiDoc: " + file.isActiveAndNotDigiDoc());
                LOG.debug("File isActiveDigiDoc: " + file.isActiveDigiDoc());
                if(file.isActive()){
                    activeFiles.add(new TaskFile(file));
                } else {
                    inActiveFiles.add(new TaskFile(file));
                }

            }

            model.addAttribute("files", activeFiles);
            model.addAttribute("inActiveFiles", inActiveFiles);

            if (CompoundWorkflowType.DOCUMENT_WORKFLOW.equals(CompoundWorkflowType.valueOf( (String) compoundWorkflow.getProp(WorkflowCommonModel.Props.TYPE)))) {
                String accessRestrictionDate = "", regInfo = "";
                NodeRef compoundWorkflowParent = compoundWorkflow.getParent();
                model.addAttribute("isDocumentWorkflowType", Boolean.TRUE);

                Document document = new Document(compoundWorkflowParent);
                Map<String, Object> docProps = document.getNode().getProperties();

                model.addAttribute("workflowOwnerName", document.getOwnerName());
                model.addAttribute("workflowStatus", docProps.get(DOC_STATUS));

                String accessRestriction = document.getAccessRestriction();
                model.addAttribute("document", document);

                if (accessRestriction != null && !(AccessRestriction.OPEN.getValueName().equals(accessRestriction) &&
                        AccessRestriction.INTERNAL.getValueName().equals(accessRestriction))) {

                    if (document.getAccessRestrictionBeginDate() != null) {
                        accessRestrictionDate = new StringBuilder().append(document.getAccessRestrictionBeginDateStr())
                                .append(document.getAccessRestrictionEndDate() != null ? (" - " + document.getAccessRestrictionEndDateStr()) : "").toString();
                    }
                    model.addAttribute("accessRestrictionDate", accessRestrictionDate);
                }
                if (document.getRegDateTime() != null)
                    regInfo = new StringBuilder().append(document.getRegDateTimeStr())
                            .append(document.getRegNumber() != null ? (" " + StringUtils.trim(document.getRegNumber().trim())) : "").toString();
                model.addAttribute("regInfo", regInfo);
                model.addAttribute("additionalInfo", docProps.get(COMMENT));
            }
        }

        setupOpinions(model, compoundWorkflow);
        setupRelatedUrls(model, compoundWorkflowNodeRef);
        setupWorkflowBlock(model, compoundWorkflow);
        if (!delegableTasks.isEmpty()) {
            setupDelegationHistoryBlock(model, delegableTasks);
        }
        BeanHelper.getLogService().addLogEntry(LogEntry.create(LogObject.COMPOUND_WORKFLOW, getUserService(), compoundWorkflowNodeRef, "applog_compoundWorkflow_view"));
        return COMPOUND_WORKFLOW_DETAILS_MAPPING;
    }

    private boolean isDelegatableTask(ee.webmedia.alfresco.workflow.service.Task task) {
        return task.isType(WorkflowSpecificModel.Types.ASSIGNMENT_TASK) 
        		|| task.isType(WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK)
                || task.isType(WorkflowSpecificModel.Types.INFORMATION_TASK) && workflowConstantsBean.isInformationWorkflowDelegationEnabled()
                || task.isType(WorkflowSpecificModel.Types.OPINION_TASK) && workflowConstantsBean.isOpinionWorkflowDelegationEnabled()
                || task.isType(WorkflowSpecificModel.Types.REVIEW_TASK) && workflowConstantsBean.isReviewWorkflowDelegationEnabled();
    }


    private void addTaskFiles(HttpSession session, HttpServletRequest request, ee.webmedia.alfresco.workflow.service.Task task, Task formTask) {
        workflowService.loadTaskFiles(task);
        List<TaskFile> files = new ArrayList<>();
        formTask.setFiles(files);
        for (Object file : task.getFiles()) {
            if (file instanceof File) {
                File f = ((File) file);
                files.add(new TaskFile(f.getName(), f.getNodeRef()));
            }
        }
        FileUploadBean uploadBean = getFileUploadBean(session);
        if (uploadBean != null && CollectionUtils.isNotEmpty(uploadBean.getTaskRefs())) {
            NodeRef taskRef = task.getNodeRef();
            List<NodeRef> taskRefs = uploadBean.getTaskRefs();
            for (int i = 0; i < taskRefs.size(); i++) {
                NodeRef ref = taskRefs.get(i);
                if (taskRef.equals(ref)) {
                    TaskFile f = new TaskFile(uploadBean.getFileNames().get(i), null);
                    String deleteUrl = UploadFileBaseServlet.generateDeleteUrl(request, uploadBean.getFiles().get(i).getName());
                    f.setDeleteUrl(deleteUrl);
                    files.add(f);
                }
            }
        }
    }

    private void setupWorkflowBlock(Model model, CompoundWorkflow compoundWorkflow) {
        List<WorkflowBlockItem> groupedWorkflowBlockItems = getWorkflowDbService().getWorkflowBlockItems(Arrays.asList(compoundWorkflow.getNodeRef()), null, null);
        setMessageSource(groupedWorkflowBlockItems);
        model.addAttribute(WORKFLOW_BLOCK_ITEMS_ATTR, groupedWorkflowBlockItems);
        model.addAttribute(TASK_COUNT_ATTR, WorkflowUtil.getTaskCount(compoundWorkflow));
    }

    private void setMessageSource(List<WorkflowBlockItem> groupedWorkflowBlockItems) {
        for (WorkflowBlockItem workflowBlockItem : groupedWorkflowBlockItems) {
            workflowBlockItem.setMessageSource(messageSource);
        }
    }


    private Map<Integer, String> getReviewTaskOutcomes() {
        if (reviewTaskOutcomes == null) {
            reviewTaskOutcomes = new HashMap<Integer, String>();
            int outcomes = workflowConstantsBean.getWorkflowTypes().get(WorkflowSpecificModel.Types.REVIEW_WORKFLOW).getTaskOutcomes();
            for (int i = 0; i < outcomes; i++) {
                reviewTaskOutcomes.put(i, translate("workflow.task.type.reviewTask.outcome." + i + TITLE_SUFFIX));
            }
        }
        return reviewTaskOutcomes;
    }

    /* Could be used to pass default selection into reviewTask outcomes dropdown. Needs a way to get the current selection outcome index and there is a problem with confirming getReviewTaskOutcomesSelection() result.

    private Map<Integer, String> getReviewTaskOutcomes() {
        if (reviewTaskOutcomes == null) {
            reviewTaskOutcomes = new HashMap<Integer, String>();
            int outcomes = workflowConstantsBean.getWorkflowTypes().get(WorkflowSpecificModel.Types.REVIEW_WORKFLOW).getTaskOutcomes();
            int selectedOutcomeIndex = getOutcomeIndex();
            for (int i = 0; i < outcomes; i++) {
            if (i != selectedOutcomeIndex) {
                    reviewTaskOutcomes.put(i, translate("workflow.task.type.reviewTask.outcome." + i + TITLE_SUFFIX));
                }

            }
        }
        return reviewTaskOutcomes;
    }

    private Map<Integer, String> getReviewTaskOutcomesSelection() {
        if (reviewTaskOutcomesSelection == null) {
            reviewTaskOutcomesSelection = new HashMap<Integer, String>();
            int outcomes = workflowConstantsBean.getWorkflowTypes().get(WorkflowSpecificModel.Types.REVIEW_WORKFLOW).getTaskOutcomes();
            for (int i = 0; i < outcomes; i++) {
                reviewTaskOutcomesSelection.put(i, translate("workflow.task.type.reviewTask.outcome." + i + TITLE_SUFFIX));
            }
        }
        return reviewTaskOutcomesSelection;
    }

    private String getSelectedOutcome() {
        String tempOutcome = getReviewTaskOutcomes().get(getOutcomeIndex());
        if (tempOutcome == null) {
            return getReviewTaskOutcomesSelection().get(getOutcomeIndex());
        } else {
            return "no such key";
        }
    }

    private Integer getOutcomeIndex() {
        int tempOutcome = 1;             // should return 0, 1, or 2, currently hardcode
        return tempOutcome;
    }
	*/

    private void setupComments(Model model, String compundWorkflowId) {
        List<Comment> compoundWorkflowComments = workflowDbService.getCompoundWorkflowComments(compundWorkflowId);
        if (!compoundWorkflowComments.isEmpty()) {
            model.addAttribute("comments", compoundWorkflowComments);
        }
    }

    private void setupObjects(Model model, CompoundWorkflow compoundWorkflow) {
        List<Document> compoundWorkflowDocuments = workflowService.getCompoundWorkflowDocuments(compoundWorkflow.getNodeRef());

        if (!compoundWorkflowDocuments.isEmpty()) {
            // Set up object properties
            List<String> documentsToSignRefId = compoundWorkflow.getDocumentsToSignNodeRefIds();
            for (Document document : compoundWorkflowDocuments) {
                String nodeRefId = document.getNodeRef().getId();
                if (documentsToSignRefId.contains(nodeRefId)) {
                    document.setDocumentToSign(Boolean.TRUE);
                }
                // TODO: After implementing document details view, add permission check.
                // if (document.getNode().hasPermission(DocumentCommonModel.Privileges.VIEW_DOCUMENT_META_DATA)) {
                // document.setShowLink(true);
                // }
            }

            model.addAttribute("objects", compoundWorkflowDocuments);
        }
    }

    private void setupOpinions(Model model, CompoundWorkflow compoundWorkflow) {
        List<Task> finishedOpinionTasks = new ArrayList<>();
        Set<NodeRef> taskRefs = new HashSet<>();
        for (Workflow wf : compoundWorkflow.getWorkflows()) {
            if (!wf.isType(WorkflowSpecificModel.Types.OPINION_WORKFLOW)) {
                continue;
            }
            for (ee.webmedia.alfresco.workflow.service.Task task : wf.getTasks()) {
                if (task.isStatus(Status.FINISHED)) {
                    taskRefs.add(task.getNodeRef());
                    Task t = new Task(task);
                    t.setCompletedDateTime(task.getCompletedDateTime());
                    t.setOwnerNameWithSubstitute(task.getOwnerNameWithSubstitute());
                    t.setCommentAndLinks(task.getCommentAndLinks());
                    finishedOpinionTasks.add(t);
                }
            }
        }
        if (CollectionUtils.isNotEmpty(finishedOpinionTasks)) {
            Map<NodeRef, List<File>> taskToFiles = workflowService.loadTaskFilesFromCompoundWorkflow(taskRefs, compoundWorkflow.getNodeRef());
            for (Task task : finishedOpinionTasks) {
                if (taskToFiles.containsKey(task.getNodeRef())) {
                    List<TaskFile> taskFiles = new ArrayList<>();
                    for (File f : taskToFiles.get(task.getNodeRef())) {
                        taskFiles.add(new TaskFile(f));
                    }
                    task.setFiles(taskFiles);
                }
            }
            Collections.sort(finishedOpinionTasks, new TaskDueDateComparator());
        }
        model.addAttribute("opinions", finishedOpinionTasks);
    }

    private void setupRelatedUrls(Model model, NodeRef compoundWorkflowNodeRef) {
        List<RelatedUrl> relatedUrls = workflowService.getRelatedUrls(compoundWorkflowNodeRef);
        if (!relatedUrls.isEmpty()) {
            WebUtil.toggleSystemUrlTarget(documentTemplateService.getServerUrl(), relatedUrls);

            model.addAttribute("relatedUrls", relatedUrls);
        }
    }

    private boolean isIdCardOutcome(QName taskType, int outcomeIndex) {
        return WorkflowSpecificModel.Types.SIGNATURE_TASK.equals(taskType)
                && WorkflowSpecificModel.SignatureTaskOutcome.SIGNED_IDCARD.equals(outcomeIndex);
    }

    /** If given task belongs to some task group in workflow block, show all tasks in this group. Otherwise redirect to compound workflow details view */
    @RequestMapping(value = "compound-workflow/task-group-details/{compoundWorkflowId}/{taskId}")
    public String getTaskGroupDetails(@PathVariable String compoundWorkflowId, @PathVariable String taskId, Model model) {
        NodeRef compoundWorkflowNodeRef = WebUtil.getNodeRefFromNodeId(compoundWorkflowId);
        if (compoundWorkflowNodeRef == null) {
            addErrorMessage("workflow_compound_edit_error_docDeleted");
            return "home";
        }

        List<WorkflowBlockItem> groupedWorkflowBlockItems = getWorkflowDbService().getWorkflowBlockItems(Arrays.asList(compoundWorkflowNodeRef), null, null);
        List<WorkflowBlockItem> groupedWorkflowBlockItem = new ArrayList<WorkflowBlockItem>();
        WorkflowBlockItem currentItem = null;
        OUTER: for (WorkflowBlockItem workflowBlockItem : groupedWorkflowBlockItems) {
            if (!workflowBlockItem.isGroupBlockItem()) {
                String workflowTaskId = workflowBlockItem.getTaskNodeRef().getId();
                if (workflowTaskId.equals(taskId)) {
                    return redirectToCompoundWorkflow(compoundWorkflowId);
                }
            }
            else {
                final List<WorkflowBlockItem> groupItems = getWorkflowDbService().getWorkflowBlockItemGroup(workflowBlockItem);
                for (WorkflowBlockItem taskItem : groupItems) {
                    String workflowTaskId = taskItem.getTaskNodeRef().getId();
                    if (workflowTaskId.equals(taskId)) {
                        currentItem = workflowBlockItem;
                        groupedWorkflowBlockItem.addAll(groupItems);
                        break OUTER;
                    }
                }

            }
        }
        if (groupedWorkflowBlockItem.isEmpty()) {
            return redirectToCompoundWorkflow(compoundWorkflowId);
        }
        setMessageSource(groupedWorkflowBlockItem);
        model.addAttribute(WORKFLOW_BLOCK_ITEMS_ATTR, groupedWorkflowBlockItem);
        model.addAttribute("groupName", currentItem.getGroupName());
        return "compound-workflow/task-group-details";
    }

    @RequestMapping(value = "/ajax/cwf/locktask", method = POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public LockMessage setCompoundWorkflowLockTask(@RequestBody LockMessage message) throws ParseException {

        final NodeRef cwfRef = message.getCompoundWorkflowRef();
        List<String> messages = new ArrayList<>(2);
        message.setMessages(messages);
        RetryingTransactionCallback<String> callback = new RetryingTransactionCallback<String>()
        {
            public String execute() throws Throwable
            {

                LockStatus lockStatus = getDocLockService().setLockIfFree(cwfRef);
                String result;

                if (lockStatus == LockStatus.LOCK_OWNER) {
                    result = null;
                } else {
                    String lockOwner = StringUtils.substringBefore(getDocLockService().getLockOwnerIfLocked(cwfRef), "_");
                    String lockOwnerName = getUserService().getUserFullNameAndId(lockOwner);
                    result = translate("workflow_compond.locked", lockOwnerName);
                }
                return result;
            }
        };

        String lockError = txnHelper.doInTransaction(callback, false, true);
        if (StringUtils.isNotBlank(lockError)) {
            messages.add(lockError);
        }
        return message;
    }

    @RequestMapping(value = COMPOUND_WORKFLOW_DETAILS_MAPPING + "/{compoundWorkflowNodeId}/{taskId}/extension", method = GET)
    public String dueDateExtension(@PathVariable String compoundWorkflowNodeId, @PathVariable String taskId, Model model, HttpServletRequest request) {
        super.setupWithoutSidebarMenu(model, request);
        DueDateExtensionForm form = new DueDateExtensionForm();
        NodeRef cwfRef = WebUtil.getNodeRefFromNodeId(compoundWorkflowNodeId);
        form.setCompoundWorkflowRef(cwfRef);
        NodeRef taskRef = new NodeRef(cwfRef.getStoreRef(), taskId);
        String taskCreatorId = (String) getWorkflowDbService().getTaskProperty(taskRef, WorkflowSpecificModel.Props.CREATOR_ID);
        String currentUser = AuthenticationUtil.getRunAsUser();
        if (currentUser != null && !currentUser.equals(taskCreatorId)) {
            form.setUserId(taskCreatorId);
            String taskCreatorName = getUserService().getUserFullName(taskCreatorId);
            form.setUserName(taskCreatorName);
        }
        Date date = CalendarUtil.addWorkingDaysToDate(new LocalDate(), 2).toDateTimeAtCurrentTime().toDate();
        form.setInitialExtensionDueDate(DATE_FORMAT.format(date));
        model.addAttribute("dueDateExtensionForm", form);

        return "compound-workflow/due-date-extension";
    }

    @RequestMapping(value = COMPOUND_WORKFLOW_DETAILS_MAPPING + "/{compoundWorkflowNodeId}/{taskId}/extension", method = POST)
    public String processDueDateExtensionSubmit(@PathVariable String compoundWorkflowNodeId, @PathVariable String taskId, @ModelAttribute DueDateExtensionForm form,
                                                RedirectAttributes redirectAttributes) {
        NodeRef cwfRef = WebUtil.getNodeRefFromNodeId(compoundWorkflowNodeId);
        CompoundWorkflowType type = workflowService.getCompoundWorkflowType(cwfRef);
        LOG.debug("Get workflow type by compound workflow ref: Node ID: " + compoundWorkflowNodeId + ", WORKFLOW type: " + type.name());

        NodeRef taskRef = new NodeRef(cwfRef.getStoreRef(), taskId);
        LOG.debug("Task ref: " + taskRef);
        String userId = form.getUserId();
        LOG.debug("USERID from form field: [" + userId + "]");
        if(userId.contains("¤¤")){
            String[] userIds = userId.split("¤¤");
            if(userIds.length >= 2){
                LOG.debug("Task initial userId: " + userIds[0]);
                LOG.debug("Task extenstion ask from userId: " + userIds[1]);
                userId = userIds[1];
            } else {
                addRedirectInfoMsg(redirectAttributes, "workflow.task.dueDate.extension.submitted");
                return redirectToDueDateExtensionView(compoundWorkflowNodeId, taskId);
            }

        }
        String extenderUserFullname = getUserService().getUserFullName(userId);
        LOG.debug("USER fullname: [" + extenderUserFullname + "]");
        ee.webmedia.alfresco.workflow.service.Task initiatingTask = workflowService.getTaskWithParents(taskRef);
        String extenderEmail = getUserService().getUserEmail(userId);
        LOG.debug("Extender e-mail: [" + extenderEmail + "]");

        if (!CompoundWorkflowType.INDEPENDENT_WORKFLOW.equals(type)) {
            CompoundWorkflow compoundWorkflow = workflowService.getCompoundWorkflow(cwfRef);
            cwfRef = compoundWorkflow.getParent();
        }

        // set lock
        boolean locked = (compoundWorkflowNodeId != null)?setLock(new NodeRef(cwfRef.getStoreRef(), compoundWorkflowNodeId), "workflow_compond.locked", redirectAttributes):false;
        if (locked) {
            try {
                workflowService.createDueDateExtension(
                        form.getReason(),
                        form.getNewDueDate(),
                        form.getExtensionDueDate(),
                        initiatingTask,
                        cwfRef,
                        userId,
                        extenderUserFullname,
                        extenderEmail);

                addRedirectInfoMsg(redirectAttributes, "workflow.task.dueDate.extension.submitted");
            } finally {
                // unlock
                if (compoundWorkflowNodeId != null) {
                    getDocLockService().unlockIfOwner(new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, compoundWorkflowNodeId));
                }
            }

            return redirectToCompoundWorkflow(compoundWorkflowNodeId);
        } else {
            return redirectToDueDateExtensionView(compoundWorkflowNodeId, taskId);
        }

    }

    @RequestMapping(value = COMPOUND_WORKFLOW_DETAILS_MAPPING + "/{compoundWorkflowNodeId}", method = POST)
    public String processSubmit(@PathVariable String compoundWorkflowNodeId, @ModelAttribute InProgressTasksForm inProgressTasksForm, RedirectAttributes redirectAttributes,
                                HttpSession session) {
        if (inProgressTasksForm == null) {
            return redirectToCompoundWorkflow(compoundWorkflowNodeId);
        }
        NodeRef cwfRef = WebUtil.getNodeRefFromNodeId(compoundWorkflowNodeId);
        // set lock
        boolean locked = (compoundWorkflowNodeId != null)?setLock(new NodeRef(cwfRef.getStoreRef(), compoundWorkflowNodeId), "workflow_compond.locked", redirectAttributes):false;
        if (locked) {
            try {
                Map<String, String> formActions = inProgressTasksForm.getActions();
                if (!formActions.isEmpty()) {
                    boolean redirectToTaskList = continueCurrentSigning(inProgressTasksForm, redirectAttributes, formActions, session);
                    if (redirectToTaskList) {
                        return redirectToTaskList(WorkflowSpecificModel.Types.SIGNATURE_TASK);
                    }
                    return redirectToCompoundWorkflow(compoundWorkflowNodeId);
                }
                Task taskToFinish = null;
                int outcomeIndex = -1;
                boolean saveOnly = false;
                NodeRef extensionTaskRef = null;
                for (Task task : inProgressTasksForm.getInProgressTasks().values()) {
                    for (Map.Entry<String, String> entry : task.getActions().entrySet()) {
                        if (StringUtils.isNotBlank(entry.getValue())) {
                            taskToFinish = task;
                            String taskAction = entry.getKey();
                            if (ACTION_SAVE.equals(taskAction)) {
                                saveOnly = true;
                                break;
                            }
                            if (DELEGATE_TASK.equals(taskAction)) {
                                return redirectToTaskDelegation(compoundWorkflowNodeId, task);
                            }
                            if (EXTEND_DUE_DATE.equals(taskAction)) {
                                extensionTaskRef = task.getNodeRef();
                                break;
                            }
                            outcomeIndex = Integer.valueOf(taskAction);
                            break;
                        }
                    }
                }

                if (taskToFinish == null || (outcomeIndex < 0 && !saveOnly && extensionTaskRef == null)) {
                    addRedirectErrorMsg(redirectAttributes, "workflow.task.finish.error.workflow.task.save.failed");
                } else if (saveOnly) {
                    saveTask(taskToFinish, redirectAttributes, session);
                } else if (extensionTaskRef != null) {
                    return redirectToDueDateExtensionView(compoundWorkflowNodeId, extensionTaskRef.getId());
                }
                else {
                    QName taskType = workflowService.getNodeRefType(taskToFinish.getNodeRef());
                    if (isMobileIdOutcome(taskType, outcomeIndex)) {
                        LOG.warn("OUTCOME IS MOBILE-ID.....");

                        if(inProgressTasksForm == null){
                            LOG.error("-- ERROR -- inProgressTasksForm IS NULL!!!!!");
                        } else {
                            inProgressTasksFormLogs(inProgressTasksForm);
                        }

                        if(redirectAttributes == null){
                            LOG.error("-- ERROR -- redirectAttributes IS NULL!!!!!");
                        } else {
                            redirectAttributesLogs(redirectAttributes);
                        }

                        if(taskToFinish == null){
                            LOG.error("-- ERROR -- taskToFinish IS NULL!!!!!");
                        } else {
                            taskToFinishLogs(taskToFinish);
                        }

                        if(session == null){
                            LOG.error("-- ERROR -- WorkflowServiceImplsession IS NULL!!!!!");
                        } else {
                            sessionLogs(session);
                        }

                        startSigning(inProgressTasksForm, redirectAttributes, taskToFinish, session);
                    } else {
                        boolean finishSuccess = finishTask(taskToFinish, taskType, outcomeIndex, redirectAttributes, session);
                        if (finishSuccess) {
                            return redirectToTaskList(taskType);
                        }
                    }
                }

            } finally {
                // unlock
                if (compoundWorkflowNodeId != null) {
                    getDocLockService().unlockIfOwner(new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, compoundWorkflowNodeId));
                }
            }
        }
        return redirectToCompoundWorkflow(compoundWorkflowNodeId);
    }

    private void sessionLogs(HttpSession session){
        LOG.debug("-- HTTP SESSION INFO ----------------------------------------");
        LOG.debug("HttpSession: getAttributeNames: getCreationTime: " + session.getCreationTime());
        LOG.debug("HttpSession: getAttributeNames: getId: " + session.getId());
        LOG.debug("HttpSession: getAttributeNames: getLastAccessedTime: " + session.getLastAccessedTime());
        LOG.debug("HttpSession: getAttributeNames: getMaxInactiveInterval: " + session.getMaxInactiveInterval());
        // ENUM
        Enumeration names = session.getAttributeNames();
        if(names == null){
            LOG.debug("HttpSession: getAttributeNames: getAttributeNames: NULL!");
        } else {
            while(names.hasMoreElements()){
                Object object = names.nextElement();
                if(object == null){
                    LOG.debug("HttpSession: getAttributeNames: getAttributeNames: element object: NULL!");
                } else {
                    if(object instanceof String){
                        LOG.debug("HttpSession: getAttributeNames: getAttributeNames: element object [String]: " + object);
                    } else if(object instanceof Integer){
                        LOG.debug("HttpSession: getAttributeNames: getAttributeNames: element object [Integer]: " + object);
                    } else {
                        LOG.debug("HttpSession: getAttributeNames: getAttributeNames: element object: class: " + object.getClass());
                    }
                }
            }
        }

    }

    private void taskToFinishLogs(Task taskToFinish){
        LOG.debug("-- TASK TO FINISH INFO ----------------------------------------");
        taskInfoLogs(taskToFinish);
    }

    private void redirectAttributesLogs(RedirectAttributes redirectAttributes){
        LOG.debug("-- REDIRECT ATTRIBUTES INFO ----------------------------------------");
        Map<String, ?> flashAttr = redirectAttributes.getFlashAttributes();
        for (Map.Entry<String, ?> entry : flashAttr.entrySet()) {
            LOG.debug("redirectAttributes: KEY: " + entry.getKey());
            Object obj = entry.getValue();
            if(obj == null){
                LOG.debug("redirectAttributes: VALUE: OBJECT is NULL!");
            } else {
                LOG.debug("redirectAttributes: VALUE: OBJECT class: " + obj.getClass());
            }
        }
    }

    private void inProgressTasksFormLogs(InProgressTasksForm inProgressTasksForm){
        LOG.debug("-- IN PROGRESS TASK FORM INFO ----------------------------------------");
        if(inProgressTasksForm == null){
            LOG.error("inProgressTasksForm is NULL!");
        } else {
            LOG.debug("inProgressTasksForm: getMobileIdChallengeId: " + inProgressTasksForm.getMobileIdChallengeId());
            LOG.debug("inProgressTasksForm: getCompoundWorkflowRef: " + inProgressTasksForm.getCompoundWorkflowRef());
            LOG.debug("inProgressTasksForm: getContainerRef: " + inProgressTasksForm.getContainerRef());
            LOG.debug("inProgressTasksForm: getPhoneNumber: " + inProgressTasksForm.getPhoneNumber());
            LOG.debug("inProgressTasksForm: getSigningFlowView: " + inProgressTasksForm.getSigningFlowView());
            LOG.debug("inProgressTasksForm: getSigningFlowId: " + inProgressTasksForm.getSigningFlowId());
            LOG.debug("inProgressTasksForm: isDefaultSigningNumber: " + inProgressTasksForm.isDefaultSigningNumber());

            Map<String, String> actionsMap = inProgressTasksForm.getActions();
            if(actionsMap == null){
                LOG.error("inProgressTasksForm: actionsMap is NULL!");
            } else {
                if(actionsMap.size() == 0){
                    LOG.warn("inProgressTasksForm: actionsMap is empty (0)!");
                } else {
                    int i = 0;
                    for (Map.Entry<String, String> entry : actionsMap.entrySet()) {
                        i++;
                        LOG.debug("inProgressTasksForm: actionsMap entry: key: " + i + ")" + entry.getKey() + " ==> " + entry.getValue());
                    }
                }
            }

            Map<String, Task> inProgressTasks = inProgressTasksForm.getInProgressTasks();
            if(inProgressTasks == null){
                LOG.error("inProgressTasksForm: inProgressTask is NULL!");
            } else {
                int tasksCount = inProgressTasks.size();
                if(tasksCount == 0){
                    LOG.warn("inProgressTasksForm: inProgressTask is empty (0)!");
                } else {
                    int i = 0;
                    for (Map.Entry<String, Task> entry : inProgressTasks.entrySet()) {
                        i++;
                        LOG.debug("-- (" + 1 + "/" + tasksCount + ") -- TASK --------------------------------------------------------------");
                        LOG.debug("inProgressTasksForm: inProgressTask: entry: key: " + entry.getKey());
                        Task task = entry.getValue();
                        taskInfoLogs(task);
                    }
                    LOG.debug("------------------------------------------------------------------");
                }
            }
        }
    }



    private String redirectToDueDateExtensionView(String compoundWorkflowNodeId, String taskId) {
        return redirectToCompoundWorkflow(compoundWorkflowNodeId) + "/" + taskId + "/extension";
    }

    private String redirectToTaskDelegation(String compoundWorkflowNodeId, Task task) {
        String typeStr = task.getTypeStr();
        if (typeStr == null || !TASK_TYPE_MAPPING.containsKey(typeStr)) {
            return redirectToTaskList();
        }
        // String string = redirectToCompoundWorkflow(compoundWorkflowNodeId) + "/" + task.getNodeRef().getId() + "/delegation/" + typeStr;
        return redirectToCompoundWorkflow(compoundWorkflowNodeId) + String.format("/%s/delegation/%s", task.getNodeRef().getId(), typeStr);
        // return string;
    }

    public void startSigning(InProgressTasksForm inProgressTasksForm, RedirectAttributes redirectAttributes, Task taskToFinish, HttpSession session) {
        ee.webmedia.alfresco.workflow.service.Task task = workflowService.getTask(taskToFinish.getNodeRef(), false);
        if (task == null || !task.isStatus(Status.IN_PROGRESS)) {
            addRedirectErrorMsg(redirectAttributes, "workflow.task.finish.error.workflow.task.save.failed");
            return;
        }
        task.setComment(taskToFinish.getComment());

        CompoundWorkflowType type = workflowService.getCompoundWorkflowType(inProgressTasksForm.getCompoundWorkflowRef());
        LOG.debug("Get workflow type by compound workflow ref: Node ID: " + inProgressTasksForm.getCompoundWorkflowRef().getId() + ", WORKFLOW type: " + type.name());

        MobileSigningFlowContainer signingFlow = new MobileSigningFlowContainer((SignatureTask) task, isSignTogether(task), inProgressTasksForm,
                inProgressTasksForm.getCompoundWorkflowRef(), inProgressTasksForm.getContainerRef(), type);
        boolean signingPrepared = signingFlow.prepareSigning(this, redirectAttributes);
        if (!signingPrepared) {
            return;
        }
        long signingFlowId = signingFlowHolder.addSigningFlow(signingFlow, session);
        redirectAttributes.addFlashAttribute(SIGNING_FLOW_ID_ATTR, signingFlowId);
    }

    private boolean isSignTogether(ee.webmedia.alfresco.workflow.service.Task task) {
        NodeRef worklowRef = task.getWorkflowNodeRef();
        return WorkflowUtil.isSignTogetherType((String) BeanHelper.getNodeService().getProperty(worklowRef, WorkflowSpecificModel.Props.SIGNING_TYPE));
    }

    public boolean continueCurrentSigning(InProgressTasksForm inProgressTasksForm, RedirectAttributes redirectAttributes, Map<String, String> formActions, HttpSession session) {
        Long signingFlowId = inProgressTasksForm.getSigningFlowId();
        MobileSigningFlowContainer signingFlow = getSigningFlow(inProgressTasksForm, signingFlowId);
        if (formActions.containsKey("mobileNumberInserted")) {
            if (signingFlow != null) {
                String phoneNumber = inProgressTasksForm.getPhoneNumber();
                signingFlow.setPhoneNumber(phoneNumber);
                session.setAttribute(SigningFlowContainer.LAST_USED_MOBILE_ID_NUMBER, phoneNumber);
                signingFlow.setDefaultTelephoneForSigning(inProgressTasksForm.isDefaultSigningNumber());
                signingFlow.setResolution(inProgressTasksForm.getResolution());
                boolean signingStarted = signingFlow.startMobileIdSigning(this, redirectAttributes);
                if (!signingStarted) {
                    signingFlowHolder.removeSigningFlow(signingFlowId);
                } else {
                    redirectAttributes.addFlashAttribute(SIGNING_FLOW_ID_ATTR, signingFlowId);
                }
            }
        } else if (formActions.containsKey("finishMobileIdSigning")) {
            if (signingFlow != null) {
                boolean signingFinished = signingFlow.finishMobileIdSigning(this, redirectAttributes);
                if (!signingFinished || signingFlow.isSigningQueueEmpty()) {
                    signingFlowHolder.removeSigningFlow(signingFlowId);
                    return true;
                }
                redirectAttributes.addFlashAttribute(SIGNING_FLOW_ID_ATTR, signingFlowId);
            }
        } else if (formActions.containsKey("signingCancelled")) {
            if (signingFlow != null) {
                signingFlowHolder.removeSigningFlow(signingFlowId);
            }
        } else {
            LOG.warn("Unknown compound workflow action");
        }
        return false;
    }

    private void saveTask(Task taskToFinish, RedirectAttributes redirectAttributes, HttpSession session) {
        ee.webmedia.alfresco.workflow.service.Task task = workflowService.getTaskWithParents(taskToFinish.getNodeRef());
        if (!task.isStatus(Status.IN_PROGRESS)) {
            addRedirectErrorMsg(redirectAttributes, "workflow.task.finish.error.workflow.task.save.failed");
            return;
        }
        task.setComment(taskToFinish.getComment());
        List<Integer> uploadedFileIndexes = addUploadedFilesAndRemoveDeletedFiles(taskToFinish, session, task);
        try {
            workflowService.saveInProgressTask(task);
            addRedirectInfoMsg(redirectAttributes, "save.success");
            if (CollectionUtils.isNotEmpty(uploadedFileIndexes) && task.isType(WorkflowSpecificModel.Types.OPINION_TASK)) {
                getFileUploadBean(session).removeFiles(uploadedFileIndexes);
                addRedirectInfoMsg(redirectAttributes, "workflow.task.type.opinionTask.files.uploaded");
            }
            if (CollectionUtils.isNotEmpty(uploadedFileIndexes) && task.isType(WorkflowSpecificModel.Types.REVIEW_TASK)) {
                getFileUploadBean(session).removeFiles(uploadedFileIndexes);
                addRedirectInfoMsg(redirectAttributes, "workflow.task.type.reviewTask.files.uploaded");
            }
        } catch (WorkflowChangedException e) {
            addRedirectErrorMsg(redirectAttributes, "workflow.task.finish.error.workflow.task.save.failed");
        }
    }

    private List<Integer> addUploadedFilesAndRemoveDeletedFiles(Task taskToFinish, HttpSession session, ee.webmedia.alfresco.workflow.service.Task task) {
        List<Integer> uploadedFileIndexes = null;
        if (task.isType(WorkflowSpecificModel.Types.OPINION_TASK) || task.isType(WorkflowSpecificModel.Types.REVIEW_TASK) 
        		|| task.isType(WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK)) {
            uploadedFileIndexes = addUploadedFiles(session, task);
            removeDeletedFiles(taskToFinish.getFiles(), task);
        }
        return uploadedFileIndexes;
    }

    private void removeDeletedFiles(List<TaskFile> files, ee.webmedia.alfresco.workflow.service.Task task) {
        if (CollectionUtils.isEmpty(files)) {
            return;
        }
        for (TaskFile file : files) {
            if (file.isDeleted()) {
                task.getRemovedFiles().add(file.getNodeRef());
            }
        }
    }

    private List<Integer> addUploadedFiles(HttpSession session, ee.webmedia.alfresco.workflow.service.Task task) {
        FileUploadBean fileBean = getFileUploadBean(session);
        if (fileBean != null && CollectionUtils.isNotEmpty(fileBean.getFiles())) {
            List<java.io.File> files = fileBean.getFiles();
            List<String> contentTypes = fileBean.getContentTypes();
            List<String> names = fileBean.getFileNames();
            List<NodeRef> taskRefs = fileBean.getTaskRefs();
            NodeRef originalTaskRef = task.getNodeRef();
            List<Integer> addedFilesIndexes = new ArrayList<>();
            for (int i = 0; i < files.size(); i++) {
                NodeRef taskRef = taskRefs.get(i);
                if (!originalTaskRef.equals(taskRef)) {
                    continue;
                }
                FileWithContentType file = new FileWithContentType(files.get(i), contentTypes.get(i), names.get(i));
                task.addUploadedFile(file);
                addedFilesIndexes.add(i);
            }
            return addedFilesIndexes;
        }
        return Collections.emptyList();
    }

    private FileUploadBean getFileUploadBean(HttpSession session) {
        return (FileUploadBean) session.getAttribute(FileUploadBean.FILE_UPLOAD_BEAN_NAME);
    }

    private boolean finishTask(Task taskToFinish, QName taskType, int outcomeIndex, RedirectAttributes redirectAttributes, HttpSession session) {
        ee.webmedia.alfresco.workflow.service.Task task = workflowService.getTaskWithParents(taskToFinish.getNodeRef());
        if (task == null || !task.isStatus(Status.IN_PROGRESS)) {
            addRedirectErrorMsg(redirectAttributes, "workflow.task.finish.error.workflow.task.save.failed");
            return false;
        }
        task.setComment(taskToFinish.getComment());

        if (WorkflowSpecificModel.Types.REVIEW_TASK.equals(taskType) || WorkflowSpecificModel.Types.EXTERNAL_REVIEW_TASK.equals(taskType)) {
            Integer nodeOutcome = taskToFinish.getReviewTaskOutcome();
            if (nodeOutcome != null) {
                outcomeIndex = nodeOutcome;
                task.setProp(WorkflowSpecificModel.Props.TEMP_OUTCOME, outcomeIndex);
            }
        }

        List<Integer> uploadedFileIndexes = addUploadedFilesAndRemoveDeletedFiles(taskToFinish, session, task);
        if (WorkflowSpecificModel.Types.OPINION_TASK.equals(taskType) || 
        		WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK.equals(taskType) || task.isType(WorkflowSpecificModel.Types.REVIEW_TASK) && CollectionUtils.isEmpty(uploadedFileIndexes)) {
            workflowService.loadTaskFiles(task);
        }
        
        if (WorkflowSpecificModel.Types.DUE_DATE_EXTENSION_TASK.equals(taskType) && task.getConfirmedDueDate() == null) {
        	task.setConfirmedDueDate(task.getProposedDueDate());
        }

        List<Pair<String, String>> validationMsgs = null;
        if ((validationMsgs = WorkflowBlockBean.validate(task, outcomeIndex)) != null) {
            for (Pair<String, String> validationMsg : validationMsgs) {
                if (validationMsg.getSecond() == null) {
                    addRedirectErrorMsg(redirectAttributes, validationMsg.getFirst());
                } else {
                    addRedirectErrorMsg(redirectAttributes, validationMsg.getFirst(), validationMsg.getSecond());
                }

            }
            return false;
        }
        // finish the task
        try {
            workflowService.finishInProgressTask(task, outcomeIndex);
            addRedirectInfoMsg(redirectAttributes, "workflow.task.finish.success");
            List files = task.getFiles();
            if ((task.isType(WorkflowSpecificModel.Types.OPINION_TASK) 
            		|| task.isType(WorkflowSpecificModel.Types.REVIEW_TASK) 
            		|| task.isType(WorkflowSpecificModel.Types.ORDER_ASSIGNMENT_TASK)) && CollectionUtils.isNotEmpty(files)) {
                session.removeAttribute(FileUploadBean.FILE_UPLOAD_BEAN_NAME);
                addRedirectInfoMsg(redirectAttributes, "workflow.task.type.opinionTask.files.uploaded.and.visible");
            }
            return true;
        } catch (InvalidNodeRefException e) {
            addRedirectErrorMsg(redirectAttributes, "workflow.task.finish.error.docDeleted");
        } catch (NodeLockedException e) {
            LOG.error("Finishing task failed", e);
            Pair<String, Object[]> messageKeyAndValueHolders = DocumentLockHelperBean.getErrorMessageKeyAndValueHolders(
                    "workflow.task.finish.error.document.registerDoc.docLocked",
                    e.getNodeRef());
            addRedirectErrorMsg(redirectAttributes, messageKeyAndValueHolders.getFirst(), messageKeyAndValueHolders.getSecond());
        } catch (WorkflowChangedException e) {
            CompoundWorkflowDialog.logWorkflowChangedException(e, "Finishing task failed", LOG);
            addRedirectErrorMsg(redirectAttributes, "workflow.task.finish.error.workflow.task.save.failed");
        } catch (WorkflowActiveResponsibleTaskException e) {
            LOG.debug("Finishing task failed: more than one active responsible task!", e);
            addRedirectErrorMsg(redirectAttributes, "workflow.compound.save.failed.responsible");
        }
        return false;
    }

    private MobileSigningFlowContainer getSigningFlow(InProgressTasksForm inProgressTasksForm, Long signingFlowId) {
        if (inProgressTasksForm != null) {
            return signingFlowHolder.getSigningFlow(signingFlowId);
        }
        return null;
    }

    @RequestMapping(value = COMPOUND_WORKFLOW_DETAILS_MAPPING + "/ajax/get-signature", method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getMobileIdSignature(HttpServletResponse response, @RequestBody MobileIdSignatureAjaxRequest requestParams) {
        SigningFlowContainer signingFlow = signingFlowHolder.getSigningFlow(requestParams.getSigningFlowId());
        if (signingFlow == null) {
            return SigningFlowContainer.handleInvalidSigantureState();
        }
        return signingFlow.getMobileIdSignature(requestParams.getMobileIdChallengeId());
    }

    private class TaskDueDateComparator implements Comparator<Task> {

        @Override
        public int compare(Task t1, Task t2) {
            if (t1.getCompletedDateTime().before(t2.getCompletedDateTime())) {
                return -1;
            } else if (t2.getCompletedDateTime().before(t1.getCompletedDateTime())) {
                return 1;
            }
            return 0;
        }

    }
}