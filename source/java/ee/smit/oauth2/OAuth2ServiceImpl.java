package ee.smit.oauth2;

import ee.smit.amr.model.InlineObject;
import ee.smit.amr.model.InlineResponseDefault;
import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.common.JsonUtils;
import org.alfresco.repo.cache.SimpleCache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.auth.AuthenticationException;

import java.util.Date;

import static ee.smit.common.http.HttpUtils.restPostReq;

public class OAuth2ServiceImpl implements OAuth2Service{
    protected final static Log log = LogFactory.getLog(OAuth2ServiceImpl.class);

    private SimpleCache<String, Oauth2TokenInfo> tokenInfoSimpleCache;

    private final int retryLimit = 3;

    public static void getSleepInSeconds(int sec) {
        log.info("Sleep in " + sec + " seconds...");
        try {
            Thread.sleep(sec * 1000L);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        log.info("Sleep in " + sec + " seconds... DONE!");
    }

    private String getTokenIdentifier(String uri, String user){
        return uri + ":::" + user;
    }

    @Override
    public void storeToken(String clientId, Oauth2TokenInfo token) {
        log.info("Store token: clientId: " + clientId + "; token: " + token);
        if(tokenInfoSimpleCache == null){
//            tokenInfoSimpleCache.
        }
        tokenInfoSimpleCache.put(clientId, token);
        if(tokenInfoSimpleCache == null){
            log.warn("Tokens in map: TOKEN MAP IS NULL!!!");
        } else {
            log.info("Tokens in map: " + tokenInfoSimpleCache.getKeys().size());
        }
    }

    @Override
    public Oauth2TokenInfo getToken(String clientId) {
        return tokenInfoSimpleCache.get(clientId);
    }

    @Override
    public Oauth2TokenInfo getToken(String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException {
        log.info("Get Token... serviceUri: " + serviceUri + "; clientId: " + clientId + "; username: " + username);
        Oauth2TokenInfo tokenInfo = getToken(getTokenIdentifier(serviceUri, clientId));

        if (tokenInfo == null) {
            log.warn("Token is null!");
            return getNewToken(serviceUri, clientId, clientSecret, username, password);
        }
        if (tokenInfo.isExpired2(new Date())) {
            log.warn("Token is Expired!");
            return refreshToken(tokenInfo.getRefresh_token(), serviceUri, clientId, clientSecret, username, password);
        }

        return getNewToken(serviceUri, clientId, clientSecret, username, password);
    }

    @Override
    public Oauth2TokenInfo getUaaToken(String serviceUri, String svcUser, String svcPass) throws AuthenticationException {
        log.info("Get Token... serviceUri: " + serviceUri + "; svcUser: " + svcUser);
        Oauth2TokenInfo tokenInfo = getToken(getTokenIdentifier(serviceUri, svcUser));

        if (tokenInfo == null) {
            log.warn("Token is null!");
            return getNewUaaToken(serviceUri, svcUser, svcPass);
        }
        if (tokenInfo.isExpired2(new Date())) {
            log.warn("Token is Expired!");
            return refreshUaaToken(tokenInfo.getRefresh_token(), serviceUri, svcUser, svcPass);
        }

        return getNewUaaToken(serviceUri, svcUser, svcPass);
    }

    private Oauth2TokenInfo getNewUaaToken(String uri, String svcUser, String svcPass) throws AuthenticationException {
        log.info("getToken()... URI: " + uri);

        InlineObject body = new InlineObject().username(svcUser.trim());
        body.setPassword(svcPass.trim());

        int i = 0;
        while (true) {
            try {
                log.trace("AMR TOKEN REQUEST JSON: " + JsonUtils.convertObjectToJson(body));
                InlineResponseDefault resp = restPostReq(uri, body, InlineResponseDefault.class);
                if (resp == null) {
                    log.error("RESPONSE IS NULL!");
                    throw new AuthenticationException("RESPONSE IS NULL!");
                }
                log.trace("AMR TOKEN RESPONSE JSON: " + JsonUtils.convertObjectToJson(resp));

                if (resp.getTokenType() == null) {
                    log.error("RESPONSE IS NULL!");
                    throw new AuthenticationException("TOKENTYPE IS NULL!");
                }

                Oauth2TokenInfo info = new Oauth2TokenInfo();
                info.setToken_type(resp.getTokenType());
                log.trace("TOKEN TYPE: " + info.getToken_type());
                info.setAccess_token(resp.getAccessToken());
                log.trace("ACCESS TOKEN: " + info.getAccess_token());
                info.setJti("AmrRestService");
                log.trace("JTI: " + info.getJti());
                info.setExpires_in(resp.getExpiresIn());
                log.trace("EXPIRES IN: " + info.getExpires_in());

                log.trace("USERNAME: " + resp.getUsername());
                log.trace("ROLES: " + resp.getRoles());
                log.info("Token received successfully!");
                storeToken(getTokenIdentifier(uri, svcUser), info);
                return info;

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                getSleepInSeconds(1);
                log.info("Try after again, if retry count is lower than retry limit... " + i + " of " + retryLimit);
                i++;

                log.info("GET TOKEN... RETRY: " + i);
                if (i >= retryLimit) {
                    throw new AuthenticationException("GETTING AMR TOKEN FAILED! Authorization Failed! :: " + e.getMessage(), e);
                }
            }
        }
    }

    private Oauth2TokenInfo refreshUaaToken(String refreshToken, String serviceUri, String svcUser, String svcPass) throws AuthenticationException {
        log.info("Get new UAA Token based on Refresh UAA Token... serviceUri: " + serviceUri + "; svcUser: " + svcUser);

        if (refreshToken == null) {
            log.warn("UAA Refresh Token is NULL! serviceUri: " + serviceUri + "; svcPass: " + svcPass);
            getNewUaaToken(serviceUri, svcUser, svcPass);
        }

        // get new token based by refresh token
        Oauth2TokenInfo tokenInfo = null;
        if (tokenInfo == null) {
            log.warn("UAA Token is null! ClientId: " + svcUser);
            tokenInfo = getNewUaaToken(serviceUri, svcUser, svcPass);
        }
        storeToken(getTokenIdentifier(serviceUri, svcUser), tokenInfo);
        return tokenInfo;
    }

    private Oauth2TokenInfo refreshToken(String refreshToken, String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException {
        log.info("Get new Token based on Refresh Token... serviceUri: " + serviceUri + "; clientId: " + clientId + "; username: " + username);

        if (refreshToken == null) {
            log.warn("Refresh Token is NULL! serviceUri: " + serviceUri + "; clientId: " + clientId + "; username: " + username);
            getNewToken(serviceUri, clientId, clientSecret, username, password);
        }

        // get new token based by refresh token
        Oauth2TokenInfo tokenInfo = Oauth2Client.sendTokenRequestAsBasicAuth(serviceUri, clientId, clientSecret, refreshToken);
        if (tokenInfo == null) {
            log.warn("Refresh Token is null! ClientId: " + clientId);
            tokenInfo = getNewToken(serviceUri, clientId, clientSecret, username, password);
        }
        storeToken(getTokenIdentifier(serviceUri, clientId), tokenInfo);
        return tokenInfo;
    }

    private Oauth2TokenInfo getNewToken(String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException {
        log.info("Get new Token... serviceUri: " + serviceUri + "; clientId: " + clientId + "; username: " + username);
        Oauth2TokenInfo tokenInfo = Oauth2Client.sendTokenRequestAsBasicAuth(serviceUri, clientId, clientSecret, username, password);
        storeToken(clientId, tokenInfo);
        return tokenInfo;
    }
}
