package ee.smit.oauth2;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import org.apache.http.auth.AuthenticationException;

public interface OAuth2Service {
    void storeToken(String clientId, Oauth2TokenInfo token);
    Oauth2TokenInfo getToken(String clientId);
    Oauth2TokenInfo getToken(String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException;

    Oauth2TokenInfo getUaaToken(String serviceUri, String svcUser, String svcPass) throws AuthenticationException;
}
