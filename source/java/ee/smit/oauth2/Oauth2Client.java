package ee.smit.oauth2;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.webmedia.alfresco.signature.exception.SignatureException;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.auth.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Oauth2Client {
    public static final String AUTH_SERVER_URI = "/oauth/token";
    private static final Logger logger = LoggerFactory.getLogger(Oauth2Client.class);
    private static final String TOKEN_NAME = "Delta-Token";

    /**
     * Get http headers with json media type
     *
     * @return
     */
    public static HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));
        return headers;
    }

    public static HttpHeaders getJsonHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public static HttpHeaders getMultipartFormDataHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));
        return headers;
    }

    /**
     * Set token info to http headers
     *
     * @param headers
     * @param token
     * @return
     */
    public static HttpHeaders getHeadersWithToken(HttpHeaders headers, Oauth2TokenInfo token) {
        logger.debug("getHeadersWithToken()...");
        headers.add(TOKEN_NAME, token.getJti());
        logger.debug("TOKEN: Authorization: " + token.getToken_type() + " " + token.getAccess_token());
        headers.add("Authorization", token.getToken_type() + " " + token.getAccess_token());
        return headers;
    }

    public static HttpURLConnection getHttpURLConnectionWithToken(HttpURLConnection huc, Oauth2TokenInfo token) {
        huc.setRequestProperty(TOKEN_NAME, token.getJti());
        huc.setRequestProperty("Authorization", token.getToken_type() + " " + token.getAccess_token());
        return huc;
    }

    /**
     * Get authorization headers for getting token
     *
     * @param clientId
     * @param clientSecret
     * @return
     */
    public static HttpHeaders getHeadersWithClientCredentials(String clientId, String clientSecret) {
        String plainClientCredentials = clientId + ":" + clientSecret;
        String base64ClientCredentials = new String(Base64.encodeBase64(plainClientCredentials.getBytes()));
        String authorization = "Basic " + base64ClientCredentials;

        logger.trace("Authorization: " + authorization);

        HttpHeaders headers = getJsonHeaders();
        headers.add("Authorization", authorization);
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        return headers;
    }

    /**
     * Get authorization request data
     *
     * @param grantType
     * @param scope
     * @param username
     * @param password
     * @return
     */
    public static String getAuthData(String grantType, String scope, String username, String password) {
        return "grant_type=" + grantType + "&scope=" + scope + "&username=" + username + "&password=" + password;
    }

    /**
     * Get authorization renew data by refresh token
     *
     * @param grantType
     * @param refreshToken
     * @return
     */
    public static String getTokenByRefreshToken(String grantType, String refreshToken) {
        return "grant_type=" + grantType + "&refresh_token=" + refreshToken;
    }


    /**
     * Oauth2 token re-request by resfresh token
     *
     * @param serviceUri
     * @param clientId
     * @param clientSecret
     * @param refreshToken
     * @return
     * @throws AuthenticationException
     */
    public static Oauth2TokenInfo sendTokenRequestAsBasicAuth(String serviceUri, String clientId, String clientSecret, String refreshToken) throws AuthenticationException {
        String authData = getTokenByRefreshToken("refresh_token", refreshToken);
        return getTokenRequestAsBasicAuth(serviceUri, authData, clientId, clientSecret);
    }



    /**
     * Oauth2 token request by username and password
     *
     * @param serviceUri
     * @param clientId
     * @param clientSecret
     * @param username
     * @param password
     * @return
     * @throws AuthenticationException
     */
    public static Oauth2TokenInfo sendTokenRequestAsBasicAuth(String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException {
        // Check that SVC service token exist
        String authData = getAuthData("password", "write", username, password);
        logger.trace("AUTH DATA: " + authData);

        // GET new token
        return getTokenRequestAsBasicAuth(serviceUri, authData, clientId, clientSecret);
    }

    /**
     * Get token with authData
     *
     * @param serviceUri
     * @param authData
     * @param clientId
     * @param clientSecret
     * @return
     * @throws AuthenticationException
     */
    private static Oauth2TokenInfo getTokenRequestAsBasicAuth(String serviceUri, String authData, String clientId, String clientSecret) throws AuthenticationException {
        try {
            String authUri = getAuthUri(serviceUri);

            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<String> request = new HttpEntity<>(authData, getHeadersWithClientCredentials(clientId, clientSecret));

            ResponseEntity<Oauth2TokenInfo> response = restTemplate.exchange(authUri, HttpMethod.POST, request, Oauth2TokenInfo.class);

            if (response == null | response.getBody() == null) {
                throw new AuthenticationException("Authorization Failed!");
            }
            return response.getBody();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException("Authorization Failed! " + e.getMessage());
        }
    }

    /**
     * Get token info with UrlEncodedFormEntity
     *
     * @param serviceTokenUri
     * @param request
     * @return
     * @throws AuthenticationException
     */
    public static Oauth2TokenInfo getTokenRequestInBody(String serviceTokenUri, HttpEntity<?> request) throws AuthenticationException {
        logger.debug("getTokenRequestInBody()...");
        //ResponseEntity<Oauth2TokenInfo> response = new RestTemplate().exchange(serviceTokenUri, HttpMethod.POST, request, Oauth2TokenInfo.class);
        try {
            ResponseEntity<Object> response = new RestTemplate().exchange(serviceTokenUri, HttpMethod.POST, request, Object.class);
            if (response == null || response.getBody() == null) {
                throw new AuthenticationException("Authorization Failed!");
            }

            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) response.getBody();

            if (logger.isTraceEnabled()) {
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    logger.trace("MAP Entry: name: " + entry.getKey() + "; VALUE: class: " + entry.getValue().getClass());
                }
            }
            Oauth2TokenInfo tokenInfo = new Oauth2TokenInfo();

            if (map != null) {
                tokenInfo.setError((String) map.get("error"));
                tokenInfo.setAccess_token((String) map.get("access_token"));
                tokenInfo.setToken_type((String) map.get("token_type"));
                tokenInfo.setRefresh_token((String) map.get("refresh_token"));
                tokenInfo.setExpires_in((int) map.get("expires_in"));
                tokenInfo.setScope((String) map.get("scope"));
                tokenInfo.setJti((String) map.get("jti"));
                logger.debug("TOKEN TYPE: " + map.get("token_type"));
                logger.debug("TOKEN REQUEST SUCCESSFUL! jti: " + tokenInfo.getJti() + "; Access token: " + tokenInfo.getAccess_token());
            }

            return tokenInfo;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException("Authorization Failed! " + e.getMessage());
        }
    }

    public static <T> T getMultipartPostRequestWithToken(MultiValueMap<String, Object> body, String serviceUrl, Class<T> type, Oauth2TokenInfo tokenInfo) throws SignatureException {
        logger.debug("getIdCardDigest()... serviceUrl: " + serviceUrl);
        logger.debug("SERVICE URL: " + serviceUrl);

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(getMultipartFormDataHeaders(), tokenInfo));

        String msg = "";
        try {
            ResponseEntity<T> response = new RestTemplate().postForEntity(serviceUrl, requestEntity, type);
            if (response != null) {
                logger.debug("REQEUST STATUS CODE: " + response.getStatusCode());
                if (response.hasBody()) {
                    logger.debug("BODY class: " + response.getBody().getClass());
                    return type.cast(response.getBody());
                }
            }

        } catch (Exception e) {
            msg = e.getMessage();
            logger.error(e.getMessage(), e);
        }
        throw new SignatureException("NO RESPONSE BODY! " + msg);
    }

    public static <T> T getMultipartPostRequest(MultiValueMap<String, Object> body, String serviceUrl, Class<T> type) throws SignatureException {
        logger.debug("getIdCardDigest()... serviceUrl: " + serviceUrl);
        logger.debug("SERVICE URL: " + serviceUrl);

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, getMultipartFormDataHeaders());

        String msg = "";
        try {
            ResponseEntity<T> response = new RestTemplate().postForEntity(serviceUrl, requestEntity, type);
            if (response != null) {
                logger.debug("REQEUST STATUS CODE: " + response.getStatusCode());
                if (response.hasBody()) {
                    logger.debug("BODY class: " + response.getBody().getClass());
                    return type.cast(response.getBody());
                }
            }

        } catch (Exception e) {
            msg = e.getMessage();
            logger.error(e.getMessage(), e);
        }
        throw new SignatureException("NO RESPONSE BODY ! " + msg);
    }

    /**
     * Get authorization uri
     *
     * @param serviceUri
     * @return
     */
    public static String getAuthUri(String serviceUri) {
        return serviceUri + AUTH_SERVER_URI;
    }
}
