package ee.smit.digisigncrypt.model.crypt;

import ee.smit.digisigncrypt.model.DefaultResponse;

public class CryptResponse extends DefaultResponse {
    private String file;

    // -- GETTERS, SETTERS -----------------------------------------

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

}
