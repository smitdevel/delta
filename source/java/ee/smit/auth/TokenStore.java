package ee.smit.auth;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import org.apache.http.auth.AuthenticationException;

public interface TokenStore {
    String BEAN_NAME = "TokenStore";

    void storeToken(String clientId, Oauth2TokenInfo token);

    Oauth2TokenInfo getToken(String uri, String clientId, String clientSecret, String appname, String apppass) throws AuthenticationException;
    Oauth2TokenInfo getToken(String serviceUri, String svcUser, String svcPass) throws AuthenticationException;

    String getTokenIdentifier(String uri, String user);
}
