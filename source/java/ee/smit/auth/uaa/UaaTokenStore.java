package ee.smit.auth.uaa;

import ee.smit.amr.model.InlineObject;
import ee.smit.amr.model.InlineResponseDefault;
import ee.smit.utils.cache.MemoryCache;
import ee.smit.auth.TokenStore;
import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.common.JsonUtils;
import ee.smit.oauth2.Oauth2Client;
import org.alfresco.repo.cache.SimpleCache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.auth.AuthenticationException;

import static ee.smit.common.http.HttpUtils.restPostReq;

public class UaaTokenStore implements TokenStore {
    protected final static Log log = LogFactory.getLog(UaaTokenStore.class);

    //private final ConcurrentHashMap<String, Oauth2TokenInfo> clientIdToAccessTokenStore = new ConcurrentHashMap<>();
    private SimpleCache<String, Object> tokenInfoSimpleCache;

    private MemoryCache tokenCache = new MemoryCache();
    private final int retryLimit = 3;

    public static void getSleepInSeconds(int sec) {
        log.info("Sleep in " + sec + " seconds...");
        try {
            Thread.sleep(sec * 1000L);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        log.info("Sleep in " + sec + " seconds... DONE!");
    }

    public String getTokenIdentifier(String uri, String username){
        return uri + "|" + username;
    }

    public String getTokenIdentifier(String uri, String clientId, String username){
        return uri + "|" + clientId + "|" + username;
    }

    @Override
    public void storeToken(String clientId, Oauth2TokenInfo token) {
        log.trace("storeToken()... clientId: " + clientId + "; TOKEN: " + (token == null ? "NULL" : "TOKEN EXISTS!!!"));
        tokenInfoSimpleCache.put(clientId, token);

        //tokenCache.putTokenToCache(clientId, token);
    }

    private void getTokensInfoToLog(String tokenClientId){
        try{
            if(tokenInfoSimpleCache == null){
                log.warn("TOKEN CACHE: TOKEN CACHE IS NULL!!!");
            } else {
                log.info("TOKEN CACHE: Tokens found:" + tokenInfoSimpleCache.getKeys().size());

                if(log.isTraceEnabled()){
                    for (String key : tokenInfoSimpleCache.getKeys()) {
                        log.trace("TOKEN CACHE: CLIENT ID: [" + key + "]; tokenClientID: " + tokenClientId + "; TOKEN: " + (tokenInfoSimpleCache.get(key) == null ? "NULL!" : "EXISTS!")) ;
                        if(key.equals(tokenClientId)){
                            log.trace("TOKEN FOUND FROM CACHE!!! - " + tokenClientId);
                            Oauth2TokenInfo token = (Oauth2TokenInfo) tokenInfoSimpleCache.get(key);
                            if(token == null){
                                log.error("TOKEN IS NULL!!!");
                                //getSleepInSeconds(1);
                            } else {
                                log.info("TOKEN NAME: " + token.getToken_name() + "; IS EXPIRED: " + token.isExpired() + "; EXPIRATION DATE: " + token.getExpiration());
                            }
                        }
                    }
                }
            }
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
    }

    public Oauth2TokenInfo getToken(String clientId) {
        //log.info("getToken()... clientId: " + clientId);
        //Oauth2TokenInfo ti = (Oauth2TokenInfo) tokenCache.getTokenFromCache(clientId);
        //if(ti == null){
        //    log.warn("AARE: TokenCache MAP does not contain TOKEN INFO! - " + clientId);
        //}
        if(tokenInfoSimpleCache == null){
            log.warn("TOKEN CACHE: CACHE IS NULL! Return...");
            return null;
        }

        getTokensInfoToLog(clientId);

        Oauth2TokenInfo tokenInfo = (Oauth2TokenInfo) tokenInfoSimpleCache.get(clientId);
        if(tokenInfo == null){
            log.info("TOKEN CACHE: NO TOKEN FOUND! " + clientId);
        } else {
            log.info("TOKEN CACHE: TOKEN FOUND! " + clientId);
        }
        return tokenInfo;
    }

    @Override
    public Oauth2TokenInfo getToken(String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException {
        String tokenClientId = getTokenIdentifier(serviceUri, clientId, username);
        Oauth2TokenInfo tokenInfo = getToken(tokenClientId);

        if (tokenInfo == null) {
            return getNewToken(serviceUri, clientId, clientSecret, username, password);
        }
        if (tokenInfo.isExpired()) {
            log.info("TOKEN CACHE: TOKEN IS EXPIRED! Get refresh TOKEN: [" + tokenClientId + "]");
            return refreshToken(tokenInfo.getRefresh_token(), serviceUri, clientId, clientSecret, username, password);
        }

        //return getNewToken(serviceUri, clientId, clientSecret, username, password);
        return tokenInfo;
    }

    @Override
    public Oauth2TokenInfo getToken(String serviceUri, String svcUser, String svcPass) throws AuthenticationException {
        //log.info("getToken()... serviceUri: " + serviceUri);
        Oauth2TokenInfo tokenInfo = getToken(getTokenIdentifier(serviceUri, svcUser));

        if (tokenInfo == null) {
            return getNewUaaToken(serviceUri, svcUser, svcPass);
        }
        if (tokenInfo.isExpired()) {
            return refreshUaaToken(tokenInfo.getRefresh_token(), serviceUri, svcUser, svcPass);
        }

        // return getNewUaaToken(serviceUri, svcUser, svcPass);
        return tokenInfo;
    }

    private Oauth2TokenInfo getNewUaaToken(String uri, String svcUser, String svcPass) throws AuthenticationException {
        InlineObject body = new InlineObject().username(svcUser.trim());
        body.setPassword(svcPass.trim());

        int i = 0;
        while (true) {
            try {
                if(log.isTraceEnabled()){
                    log.trace("AMR TOKEN REQUEST JSON: " + JsonUtils.convertObjectToJson(body));
                }

                InlineResponseDefault resp = restPostReq(uri, body, InlineResponseDefault.class);
                if (resp == null) {
                    log.error("RESPONSE IS NULL!");
                    throw new AuthenticationException("RESPONSE IS NULL!");
                }

                if(log.isTraceEnabled()){
                    log.trace("AMR TOKEN RESPONSE JSON: " + JsonUtils.convertObjectToJson(resp));
                }

                if (resp.getTokenType() == null) {
                    log.error("RESPONSE IS NULL!");
                    throw new AuthenticationException("TOKENTYPE IS NULL!");
                }

                Oauth2TokenInfo info = new Oauth2TokenInfo();
                info.setToken_type(resp.getTokenType());
                log.trace("TOKEN TYPE: " + info.getToken_type());
                info.setAccess_token(resp.getAccessToken());
                //log.trace("ACCESS TOKEN: " + info.getAccess_token());
                info.setJti("AmrRestService");
                info.setToken_name("AmrRestService");
                //log.trace("JTI: " + info.getJti());
                info.setExpires_in(resp.getExpiresIn());
                log.trace("EXPIRES IN: " + info.getExpires_in());

                log.trace("USERNAME: " + resp.getUsername());
                log.trace("ROLES: " + resp.getRoles());
                log.info("Token received successfully!");
                storeToken(getTokenIdentifier(uri, svcUser), info);
                return info;

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                getSleepInSeconds(1);
                log.info("Try after again, if retry count is lower than retry limit... " + i + " of " + retryLimit);
                i++;

                log.info("GET TOKEN... RETRY: " + i);
                if (i >= retryLimit) {
                    throw new AuthenticationException("GETTING AMR TOKEN FAILED! Authorization Failed! :: " + e.getMessage(), e);
                }
            }
        }
    }

    private Oauth2TokenInfo refreshUaaToken(String refreshToken, String serviceUri, String svcUser, String svcPass) throws AuthenticationException {
        Oauth2TokenInfo tokenInfo = null;

        if (refreshToken == null) {
            log.warn("UAA Refresh Token is NULL! serviceUri: " + serviceUri + "; svcPass: " + svcPass);
            return getNewUaaToken(serviceUri, svcUser, svcPass);
        } else {
            // get new token based by refresh token
            // TODO: implement refresh token usage!
            log.info("OAUTH2 REFRESH TOKEN NOT IMPLEMENTED!!");
        }


        if (tokenInfo == null) {
            log.warn("UAA Token is null! ClientId: " + svcUser);
            tokenInfo = getNewUaaToken(serviceUri, svcUser, svcPass);
        }
        storeToken(getTokenIdentifier(serviceUri, svcUser), tokenInfo);
        return tokenInfo;
    }

    private Oauth2TokenInfo refreshToken(String refreshToken, String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException {
        Oauth2TokenInfo tokenInfo;

        if (refreshToken == null) {
            log.warn("Refresh Token is NULL! serviceUri: " + serviceUri);
            return getNewToken(serviceUri, clientId, clientSecret, username, password);
        } else {
            // get new token based by refresh token
            log.info("GET OAuth2 TOKEN  BY REFRESH-TOKEN....");
            tokenInfo = Oauth2Client.sendTokenRequestAsBasicAuth(serviceUri, clientId, clientSecret, refreshToken);
        }

        if (tokenInfo == null) {
            log.warn("Refresh Token is NULL! serviceUri: " + serviceUri);
            tokenInfo = getNewToken(serviceUri, clientId, clientSecret, username, password);
        }
        storeToken(getTokenIdentifier(serviceUri, clientId, username), tokenInfo);
        return tokenInfo;
    }

    private Oauth2TokenInfo getNewToken(String serviceUri, String clientId, String clientSecret, String username, String password) throws AuthenticationException {
        Oauth2TokenInfo tokenInfo = Oauth2Client.sendTokenRequestAsBasicAuth(serviceUri, clientId, clientSecret, username, password);
        storeToken(getTokenIdentifier(serviceUri, clientId, username), tokenInfo);
        return tokenInfo;
    }

    public void setTokenInfoSimpleCache(SimpleCache<String, Object> tokenInfoSimpleCache) {
        this.tokenInfoSimpleCache = tokenInfoSimpleCache;
    }
}
