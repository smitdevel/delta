package ee.smit.auth.uaa.exceptions;

public class TokenExpiredException extends Throwable {
    private static final long serialVersionUID = 2256477558314496009L;

    /**
     * Constructs an {@code TokenExpiredException} with no detail
     * message.
     */
    public TokenExpiredException() {
        super();
    }

    /**
     * Constructs an {@code TokenExpiredException} with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public TokenExpiredException(String s) {
        super(s);
    }
}
