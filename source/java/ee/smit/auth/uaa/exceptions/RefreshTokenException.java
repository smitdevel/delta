package ee.smit.auth.uaa.exceptions;

public class RefreshTokenException extends Throwable {
    private static final long serialVersionUID = 2256477558314496009L;

    /**
     * Constructs an {@code RefreshTokenException} with no detail
     * message.
     */
    public RefreshTokenException() {
        super();
    }

    /**
     * Constructs an {@code RefreshTokenException} with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public RefreshTokenException(String s) {
        super(s);
    }
}
