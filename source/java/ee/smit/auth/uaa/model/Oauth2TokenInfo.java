package ee.smit.auth.uaa.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.io.Serializable;
import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Oauth2TokenInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String token_name;
    private String access_token;
    private String token_type;
    private String id_token;
    private String refresh_token;
    private Integer expires_in;
    private Date expiration;
    private String scope;
    private String jti;
    private byte[] content;

    private String error;
    private String errorDescription;

    public String getToken_name() {
        return token_name;
    }

    public void setToken_name(String token_name) {
        this.token_name = token_name;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getId_token() {
        return id_token;
    }

    public void setId_token(String id_token) {
        this.id_token = id_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;

        if (expires_in != null) {
            setExpiration(new Date(System.currentTimeMillis() + (expires_in * 1000L)));
        }
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
    
    public boolean isExpired2(Date date) {
        if(expires_in < date.getTime()){
            return true;
        }
        return false;
    }
    
    public boolean isExpired() {

        return expiration != null && expiration.before(new Date());
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Oauth2TokenInfo {\n");
        sb.append("    token_name: ").append(toIndentedString(token_name)).append("\n");
        sb.append("    access_token: ").append(toIndentedString(access_token)).append("\n");
        sb.append("    token_type: ").append(toIndentedString(token_type)).append("\n");
        sb.append("    id_token: ").append(toIndentedString(id_token)).append("\n");
        sb.append("    refresh_token: ").append(toIndentedString(refresh_token)).append("\n");
        sb.append("    expires_in: ").append(toIndentedString(expires_in)).append("\n");
        sb.append("    expiration: ").append(toIndentedString(expiration)).append("\n");
        sb.append("    scope: ").append(toIndentedString(scope)).append("\n");
        sb.append("    jti: ").append(toIndentedString(jti)).append("\n");
        sb.append("    access_token: ").append(toIndentedString(access_token)).append("\n");
        sb.append("    content: ").append(toIndentedString(content)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
