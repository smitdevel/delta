package ee.smit.auth.uaa.model;

public enum Oauth2GrantType {
    authorization_code,
    password,
    client_credentials,
    refresh_token,
    device_code;

}
