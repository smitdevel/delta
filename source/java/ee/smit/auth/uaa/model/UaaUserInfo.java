package ee.smit.auth.uaa.model;

import java.security.Timestamp;

/**
 * {
 *     "user_id": "b5cbdb23-4b60-4759-beb0-2f4d487dd2f9",
 *     "user_name": "37503294229",
 *     "given_name": "Aare",
 *     "family_name": "Puusepp",
 *     "phone_number": "6126293",
 *     "email": "Aare.Puusepp@smit.ee",
 *     "email_verified": false,
 *     "previous_logon_time": 1602595610756,
 *     "name": "Aare Puusepp",
 *     "sub": "b5cbdb23-4b60-4759-beb0-2f4d487dd2f9"
 * }
 */

public class UaaUserInfo {

    private String user_id;
    private String user_name;
    private String given_name;
    private String family_name;
    private String phone_number;
    private String email;
    private boolean email_verified;
    private Long previous_logon_time;
    private String name;
    private String sub;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getGiven_name() {
        return given_name;
    }

    public void setGiven_name(String given_name) {
        this.given_name = given_name;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmail_verified() {
        return email_verified;
    }

    public void setEmail_verified(boolean email_verified) {
        this.email_verified = email_verified;
    }

    public Long getPrevious_logon_time() {
        return previous_logon_time;
    }

    public void setPrevious_logon_time(Long previous_logon_time) {
        this.previous_logon_time = previous_logon_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }
}
