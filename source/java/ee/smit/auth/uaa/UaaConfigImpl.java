package ee.smit.auth.uaa;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.auth.uaa.model.UaaIntrospectInfo;
import ee.smit.auth.uaa.model.UaaUserInfo;
import ee.smit.oauth2.Oauth2Client;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.naming.AuthenticationException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class UaaConfigImpl implements UaaConfig {
    private final static Log log = LogFactory.getLog(UaaConfigImpl.class);

    private final String CLIENT_ID = "client_id";
    private final String CLIENT_SECRET = "client_secret";
    private final String GRANT_TYPE = "grant_type";
    private final String GRANT_TYPE_VALUE = "authorization_code";
    //protected final String GRANT_TYPE_VALUE = "client_credentials";
    private final String CODE = "code";
    private final String REDIRECT_URI = "redirect_uri";
    private final String RESPONSE_TYPE = "response_type";
    private final String RESPONSE_TYPE_CODE = "code";
    private final String TOKEN = "token";
    private final String SCOPE = "scope";
    private final String SCOPE_VALUE = "openid";
    private final String[] CLIENT_AUTHENTICATION_TYPES = {"basic_auth_header", "client_credentials_in_body"};

    private final String USERINFO_PATH = "/userinfo";
    private final String ACCESS_TOKEN_PATH = "/oauth/token";
    private final String AUTHORIZE_PATH = "/oauth/authorize";
    private final String LOGOUT_PATH = "logout.do";
    private final String LOGOUT_REDIRECT = "redirect";

    private final String UAA_USER_ATTRIBUTE = "_uaa_user_attribute_";
    private final String CONST_UAA_ASSERTION = "_const_uaa_assertion_";

    private final String AUTHENTICATE_SYSTEM_TYPE_UAA = "UAA";
    private final String AUTHENTICATE_SYSTEM_TYPE_CAS = "CAS";
    private final String AUTHENTICATE_SYSTEM_TYPE_FAKE = "FAKE";

    private String authenticateSystem;
    private String fakeAuthenticatedUsername;

    private String callbackUrl;
    private String authUrl;

    private String clientId;
    private String clientSecret;

    public String getRedirectParam(String redirectUrl){

        String params = authUrl + AUTHORIZE_PATH + "?"
                + RESPONSE_TYPE + "=" + RESPONSE_TYPE_CODE + "&"
                + CLIENT_ID + "=" + clientId + "&"
                + SCOPE + "=" + SCOPE_VALUE + "&"
                + REDIRECT_URI+"=" + (redirectUrl.isEmpty() ? callbackUrl : redirectUrl);
        log.debug("getRedirectParam(): " +params);
        return params;
    }

    public String getUserinfoPath(){
        return authUrl + USERINFO_PATH;
    }

    public String getAccessTokenUrl(){
        return authUrl + ACCESS_TOKEN_PATH;
    }

    public String getClientAuthenticationType(){
        return CLIENT_AUTHENTICATION_TYPES[0];
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public String getLogoutUrl() {
        return this.authUrl + LOGOUT_PATH + "?" + LOGOUT_REDIRECT + "=" + callbackUrl;
    }


    public void redirectAuthorize(ServletResponse servletResponse) throws IOException {
        log.debug("redirectAuthorize()...use UAA redirect params: " + getRedirectParam(null));
        ((HttpServletResponse) servletResponse).sendRedirect(getRedirectParam(null));
    }

    public void redirectAuthorize(ServletResponse servletResponse , String redirectUrl) throws IOException {
        log.debug("redirectAuthorize()...use UAA redirect params: " + getRedirectParam(redirectUrl));
        ((HttpServletResponse) servletResponse).sendRedirect(getRedirectParam(redirectUrl));
    }

    public void redirectAuthorize(HttpServletResponse response, String redirectUrl) throws IOException {
        log.debug("redirectAuthorize()...use UAA redirect params: " + getRedirectParam(redirectUrl));
        response.sendRedirect(getRedirectParam(redirectUrl));
    }

    @Override
    public void redirectLoginError(HttpServletResponse servletResponse) throws IOException {
        log.debug("redirectLoginError()... ");
        servletResponse.sendRedirect(getCallbackUrl(""));
    }

    public void redirectLoginError(HttpServletResponse servletResponse, String redirectUrl) throws IOException {
        log.debug("redirectLoginError()... user callback url: " + getCallbackUrl(redirectUrl));
        servletResponse.sendRedirect(getCallbackUrl(redirectUrl));
    }

    private UrlEncodedFormEntity getUrlEncodedAuthorizationCode(String username) throws UnsupportedEncodingException {
        return getUrlEncodedAuthorizationCode(username, null);
    }

    private UrlEncodedFormEntity getUrlEncodedAuthorizationCode(String username, String redirectUrl) throws UnsupportedEncodingException {
        log.debug("getUrlEncodedAuthorizationCode()... username: " + username);
        List<BasicNameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair(GRANT_TYPE, "authorization_code"));
        list.add(new BasicNameValuePair(CLIENT_ID, getClientId()));
        list.add(new BasicNameValuePair(CLIENT_SECRET, getClientSecret()));
        list.add(new BasicNameValuePair(CODE, username));
        list.add(new BasicNameValuePair(SCOPE, SCOPE_VALUE));
        list.add(new BasicNameValuePair(REDIRECT_URI, getCallbackUrl(redirectUrl)));

        return new UrlEncodedFormEntity(list);
    }

    private String getStringAuthorizationCode(String username, String redirectUrl) {
        log.debug("getStringAuthorizationCode()... username: " + username);
        String authString = GRANT_TYPE + "=" + GRANT_TYPE_VALUE + "&"
                + CLIENT_ID + "=" + getClientId() + "&"
                + CLIENT_SECRET + "=" + getClientSecret() + "&"
                + CODE + "=" + username + "&"
                + SCOPE + "=" + SCOPE_VALUE + "&"
                + REDIRECT_URI + "=" + getCallbackUrl(redirectUrl);

        log.trace("AUTH STRING: " + authString);
        return authString;
    }

    public Oauth2TokenInfo getUserToken(String code, String redirectUrl) throws AuthenticationException {
        log.debug("getUserToken()... code: " + code);
        Oauth2TokenInfo tokenInfo = null;

        try{
            log.debug("GET TOKEN: PART 3....");
            tokenInfo = Oauth2Client.getTokenRequestInBody(getAccessTokenUrl(), new HttpEntity<>(getStringAuthorizationCode(code, redirectUrl), Oauth2Client.getHeadersWithClientCredentials(getClientId(), getClientSecret())));
            if(tokenInfo == null){
                log.warn("TOKEN INFO IS NULL!");
            } else {
                log.debug("TOKEN FOUND!");
            }
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }

        return tokenInfo;
    }

    public UaaUserInfo getUserInfo(Oauth2TokenInfo accessToken) {
        log.debug("getUserInfo()...");

        HttpEntity requestEntity = new HttpEntity(Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(), accessToken));

        ResponseEntity<UaaUserInfo> response = new RestTemplate().exchange(getUserinfoPath(), HttpMethod.GET, requestEntity, UaaUserInfo.class);
        return response.getBody();
    }

    private UrlEncodedFormEntity getIntrospectBody(Oauth2TokenInfo accessToken) throws UnsupportedEncodingException {
        List<BasicNameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("token", accessToken.getAccess_token()));
        return new UrlEncodedFormEntity(list);
    }

    private boolean isTokenValid(Oauth2TokenInfo accessToken) throws IOException {
        log.debug("isTokenValid()...");

        ResponseEntity<UaaIntrospectInfo> response = new RestTemplate().postForEntity(getAccessTokenUrl(), getIntrospectBody(accessToken), UaaIntrospectInfo.class);
        return response.getBody().isActive();
    }

    public boolean isUaaActive(){
        if(authenticateSystem.equals(AUTHENTICATE_SYSTEM_TYPE_UAA))
            return true;
        else
            return false;
    }

    public boolean isCasActive(){
        if(authenticateSystem.equals(AUTHENTICATE_SYSTEM_TYPE_CAS))
            return true;
        else
            return false;
    }

    public boolean isFakeActive(){
        if(authenticateSystem.equals(AUTHENTICATE_SYSTEM_TYPE_FAKE)) {
            log.info("FAKE AUTH IS ACTIVE!");
            return true;
        } else
            return false;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getCallbackUrl(String redirectUrl) {
        return redirectUrl.isEmpty() ? callbackUrl : redirectUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCodeString() {
        return CODE;
    }

    public String getTokenString(){
        return TOKEN;
    }

    public void setAuthenticationSystem(String authenticationSystem) {
        this.authenticateSystem = authenticationSystem;
    }

    public void setFakeAuthenticatedUsername(String fakeAuthenticatedUsername) {
        this.fakeAuthenticatedUsername = fakeAuthenticatedUsername;
    }

    public String getFakeAuthenticatedUsername() {
        return fakeAuthenticatedUsername;
    }
}
