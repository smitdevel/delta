package ee.smit.auth.uaa;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.auth.uaa.model.UaaUserInfo;

import javax.naming.AuthenticationException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface UaaConfig {
    String BEAN_NAME = "UaaConfig";

    String getRedirectParam(String redirecUrl);

    String getUserinfoPath();

    String getAccessTokenUrl();

    String getClientAuthenticationType();

    String getAuthUrl();

    String getLogoutUrl();

    void redirectAuthorize(ServletResponse servletResponse) throws IOException;
    void redirectAuthorize(ServletResponse servletResponse, String redirectUrl) throws IOException;

    void redirectAuthorize(HttpServletResponse response, String redirectUrl) throws IOException;

    void redirectLoginError(HttpServletResponse servletResponse) throws IOException;
    void redirectLoginError(HttpServletResponse servletResponse, String redirectUrl) throws IOException;

    Oauth2TokenInfo getUserToken(String code, String redirectUrl) throws AuthenticationException;

    UaaUserInfo getUserInfo(Oauth2TokenInfo accessToken);

    boolean isUaaActive();

    boolean isCasActive();

    boolean isFakeActive();

    String getCodeString();

    String getTokenString();

    String getFakeAuthenticatedUsername();

}
