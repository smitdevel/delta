package ee.smit.mobile.ui;

public class MDeltaUIParamsImpl implements MDeltaUIParams {
    private boolean buttonDueDateExtensionIsActive;

    @Override
    public boolean buttonDueDateExtensionIsActive() {
        return buttonDueDateExtensionIsActive;
    }

    public void setButtonDueDateExtensionIsActive(boolean buttonDueDateExtensionIsActive) {
        this.buttonDueDateExtensionIsActive = buttonDueDateExtensionIsActive;
    }
}
