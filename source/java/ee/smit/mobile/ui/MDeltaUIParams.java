package ee.smit.mobile.ui;

public interface MDeltaUIParams {
    String BEAN_NAME = "MDeltaUIParams";

    boolean buttonDueDateExtensionIsActive();
}
