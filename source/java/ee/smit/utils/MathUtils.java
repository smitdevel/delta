package ee.smit.utils;

import java.math.BigInteger;

public class MathUtils {

    public static Long fromBigInteger(BigInteger bi){
        if(bi == null){
            return null;
        }

        return bi.longValue();
    }

    public static BigInteger fromLong(Long l){
        if(l == null){
            return null;
        }
        return BigInteger.valueOf(l);
    }
}
