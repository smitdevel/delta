package ee.smit.utils.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Using: call in service class: private MemoryCache memCache = new MemoryCache();
 *
 */
public class MemoryCache {
    protected final static Log log = LogFactory.getLog(MemoryCache.class);

    private final Map<String, Object> map = new ConcurrentHashMap<>();

    public MemoryCache(){}

    public MemoryCache(String key, Object value){
        this.map.put(key, value);
    }

    /**
     * Using: Oauth2TokenInfo ti = (Oauth2TokenInfo) memCache.getTokenFromCache(clientId);
     * @param key
     * @return
     */
    public Object getTokenFromCache(String key) {
        Object value;
        synchronized (map) {
            value = map.get(key);
        }
        return value;
    }

    /**
     * Using: memCache.putTokenToCache(clientId, token);
     * @param key
     * @param value
     */
    public void putTokenToCache(String key, Object value){
        synchronized (map) {
            if (map.get(key) == null) {
                map.put(key, value);
            }
        }
    }
}
