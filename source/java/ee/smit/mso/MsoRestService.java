package ee.smit.mso;

public interface MsoRestService {
    String BEAN_NAME = "MsoRestService";

    String getServiceUrl();

    String getClientId();

    String getClientSecret();

    String getUsername();

    String getPassword();

    boolean isActive();

}
