package ee.smit.mso;

import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public interface MsoRestSearches {
    String BEAN_NAME = "MsoRestSearches";

    String MIMETYPE_DOC = MimetypeMap.MIMETYPE_WORD;
    String MIMETYPE_DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    String MIMETYPE_DOT = "application/dot";
    String MIMETYPE_DOTX = "application/vnd.openxmlformats-officedocument.wordprocessingml.template";

    Set<String> supportedSourceMimetypesForPdf = new HashSet<>(Arrays.asList(
            // Word files
            "text/plain", // .txt
            "text/html", // .html, .htm

            "application/vnd.oasis.opendocument.text", // .odt
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .docx
            "application/msword", // .doc
            "application/vnd.openxmlformats-officedocument.wordprocessingml.template", // .dotx
            "application/dot", //.dot
            "application/rtf", // .rtf
            // Excel files
            "application/vnd.oasis.opendocument.spreadsheet", // .ods
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xlsx
            "application/vnd.ms-excel", // .xls
            "application/vnd.excel", // .xls
            "application/vnd.openxmlformats-officedocument.spreadsheetml.template", // xltx
            // PowerPoint files
            "application/vnd.oasis.opendocument.presentation", // .odp
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow", // .ppsx
            "application/powerpoint", // .ppt
            "application/vnd.ms-powerpoint", // .ppt
            "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .pptx
            "application/vnd.openxmlformats-officedocument.presentationml.slide" // .sldx
    ));

    Map<String, String> supportedSourceMimetypesForPdfWithExt = new HashMap<String, String>() {{
        // Word files
        put("text/plain", "txt");
        put("text/html", "html");

        put("application/vnd.oasis.opendocument.text", "odt");
        put("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx");
        put("application/msword", "doc");
        put("application/vnd.openxmlformats-officedocument.wordprocessingml.template", "dotx");
        put("application/dot", "dot");
        put("application/rtf", "rtf");
        // Excel files
        put("application/vnd.oasis.opendocument.spreadsheet", "ods");
        put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx");
        put("application/vnd.ms-excel", "xls");
        put("application/vnd.excel", "xls");
        put("application/vnd.openxmlformats-officedocument.spreadsheetml.template", "xltx");
        // PowerPoint files
        put("application/vnd.oasis.opendocument.presentation", "odp");
        put("application/vnd.openxmlformats-officedocument.presentationml.slideshow", "ppsx");
        put("application/powerpoint", "ppt");
        put("application/vnd.ms-powerpoint", "ppt");
        put("application/vnd.openxmlformats-officedocument.presentationml.presentation", "pptx");
        put("application/vnd.openxmlformats-officedocument.presentationml.slide", "sldx");

    }};

    Set<String> TEXT_EXTS = new HashSet<>(Arrays.asList(
            "txt",
            "text"
    ));

    Set<String> WORD_EXTS = new HashSet<>(Arrays.asList(
            "docx",
            "docm",
            "dotx",
            "dotm",
            "doc",
            "dot",
            "htm",
            "html",
            "rtf",
            "mht",
            "mhtml",
            "xml",
            "wpd",
            "odt"
    ));

    Set<String> EXCEL_EXTS = new HashSet<>(Arrays.asList(
            "xlsx",
            "xlsm",
            "xlsb",
            "xlam",
            "xltx",
            "xltm",
            "xls",
            "xlt",
            "xla",
            "xlw",
            "odc",
            "ods"
    ));

    Set<String> POWERPOINT_EXTS = new HashSet<>(Arrays.asList(
            "odp",
            "pot",
            "potm",
            "potx",
            "ppa",
            "ppam",
            "pps",
            "ppsm",
            "ppsx",
            "ppt",
            "pptm",
            "pptx",
            "sldm",
            "sldx"
    ));

    Set<String> MSG_EXTS = new HashSet<>(Arrays.asList(
            "msg",
            "eml"
    ));

    boolean isPlainTextFile(String fname);

    boolean isWordSupportedFile(String fname);

    boolean isExcelSupportedFile(String fname);

    boolean isPowerpointSupportedFile(String fname);

    boolean isOutlookMessageSupportedFile(String fname);

    boolean isPdfSupportedFile(String fname);

    String parse(MultipartFile multipartFile) throws IOException;

    String parse(String filename, byte[] bytes) throws IOException;

    String convertExcelToPlainText(String filename, InputStream inputStream) throws IOException;

    String convertExcelToPlainText(String filename, byte[] filedata) throws IOException;

    String convertWordToPlainText(String filename, InputStream inputStream) throws IOException;

    String convertWordToPlainText(String filename, byte[] filedata) throws IOException;

    boolean isTransformableToPdf(String sourceMimetype);

    boolean isFormulasReplaceable(String sourceMimetype);

    void transformToPdf(ContentReader documentReader, ContentWriter pdfWriter, String fileUuid) throws Exception;

    boolean replaceFormulas(Map<String, String> formulas, ContentReader documentReader, ContentWriter documentWriter, boolean dontSaveIfUnmodified) throws Exception;

    void replaceFormulasAndTransformToPdf(Map<String, String> formulas, ContentReader documentReader, ContentWriter documentWriter, ContentWriter pdfWriter) throws Exception;

    Map<String, String> modifiedFormulas(ContentReader documentReader, String fileUuid) throws Exception;

}
