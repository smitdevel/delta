package ee.smit.mso;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import ee.smit.common.PdfExtractor;
import ee.smit.common.files.FileUtil;
import ee.smit.common.http.HttpUtils;
import ee.smit.mso.client.DocumentAndFormulasInput;
import ee.smit.mso.client.DocumentInput;
import ee.smit.mso.client.DocumentOutput;
import ee.smit.mso.client.FormulaOutput;
import ee.smit.mso.client.MSOFormula;
import ee.smit.mso.client.PlainTextDataModel;
import ee.webmedia.alfresco.common.listener.StatisticsPhaseListener;
import ee.webmedia.alfresco.common.listener.StatisticsPhaseListenerLogColumn;
import ee.webmedia.alfresco.utils.CalendarUtil;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static ee.smit.common.files.FileUtil.InputStreamToBytes;
import static ee.smit.common.http.HttpClientConfig.trustSSL;
import static ee.smit.common.http.RestUtil.makeObjectPostRequest;

public class MsoRestSearchesImpl implements MsoRestSearches {
    private static final Log log = LogFactory.getLog(MsoRestSearchesImpl.class);

    private final String EXCEL_URIPATH = "/api/Mso/convert/excel/text";
    private final String WORD_URIPATH = "/api/Mso/convert/word/text";
    private final String POWERPOINT_URIPATH = "/api/Mso/convert/powerpoint/text";
    private final String POWERPOINT_PDF_URIPATH = "/api/Mso/convert/powerpoint/pdf";

    private MsoRestService msoRestService;

    public boolean isPlainTextFile(String fname) {
        return TEXT_EXTS.contains(StringUtils.lowerCase(FileUtil.getFileExt(fname)));
    }

    public boolean isWordSupportedFile(String fname) {
        return WORD_EXTS.contains(StringUtils.lowerCase(FileUtil.getFileExt(fname)));
    }

    public boolean isExcelSupportedFile(String fname) {
        return EXCEL_EXTS.contains(StringUtils.lowerCase(FileUtil.getFileExt(fname)));
    }

    public boolean isPowerpointSupportedFile(String fname){
        return POWERPOINT_EXTS.contains(StringUtils.lowerCase(FileUtil.getFileExt(fname)));
    }

    public boolean isOutlookMessageSupportedFile(String fname){
        return MSG_EXTS.contains(StringUtils.lowerCase(FileUtil.getFileExt(fname)));
    }

    public boolean isPdfSupportedFile(String fname) {
        return FileUtil.getFileExt(fname).equalsIgnoreCase("pdf");
    }

    public void setMsoRestService(MsoRestService msoRestService) {
        this.msoRestService = msoRestService;
    }

    public String parse(MultipartFile multipartFile) throws IOException {
        return parse(multipartFile.getOriginalFilename(), multipartFile.getBytes());
    }

    public String parse(String filename, byte[] bytes) throws IOException {
        log.debug("parse()... filename: " + filename + "; file bytes: " + bytes.length);
        // Not to use in production env
        trustSSL();

        String plainText;

        if (isPlainTextFile(filename)) {
            log.debug("FILE (" + filename + ") is in TEXT format!");
            plainText = new String(bytes, StandardCharsets.UTF_8);
            return plainText;
        } else if (isPdfSupportedFile(filename)) {
            log.debug("FILE (" + filename + ") is in PDF format!");
            plainText = PdfExtractor.getText(bytes);
            return plainText;
        } else if (isExcelSupportedFile(filename)) {
            log.debug("FILE (" + filename + ") is in EXCEL format!");
            return convertExcelToPlainText(filename, bytes);
        } else if (isWordSupportedFile(filename)) {
            log.debug("FILE (" + filename + ") is in WORD format!");
            return convertWordToPlainText(filename, bytes);
        } else if (isPowerpointSupportedFile(filename)) {
            log.debug("FILE (" + filename + ") is in POWERFOINT format!");
            return convertPowerPointToPlainText(filename, bytes);
        } else {
            log.debug("FILE (" + filename + ") is in NOT SUPPORTED format!");
        }

        return null;
    }

    private PlainTextDataModel convertToPlainText(String filename, InputStream inputStream, String localVarPath) throws IOException {
        return convertToPlainText(filename, InputStreamToBytes(inputStream), localVarPath);
    }

    public String convertExcelToPlainText(String filename, InputStream inputStream) throws IOException {
        log.info("convertExcelToPlainText()... filename: " + filename + "; Inputstream");
        return convertWordToPlainText(filename, InputStreamToBytes(inputStream));
    }

    public String convertExcelToPlainText(String filename, byte[] filedata) throws IOException {
        log.info("convertExcelToPlainText()... filename: " + filename + "; bytes: " + filedata.length);
        PlainTextDataModel plainTextDataModel = convertToPlainText(filename, filedata, EXCEL_URIPATH);
        log.debug("plainTextDataModel.getInputFilename(): " + plainTextDataModel.getInputFilename());
        log.debug("plainTextDataModel.getInputContentType(): " + plainTextDataModel.getInputContentType());
        log.debug("plainTextDataModel.getFiledata(): " + plainTextDataModel.getFiledata());
        return plainTextDataModel.getFiledata();
    }

    public String convertWordToPlainText(String filename, InputStream inputStream) throws IOException {
        log.info("convertWordToPlainText()... filename: " + filename + "; Inputstream");
        return convertWordToPlainText(filename, InputStreamToBytes(inputStream));
    }

    public String convertWordToPlainText(String filename, byte[] filedata) throws IOException {
        log.info("convertWordToPlainText()... filename: " + filename + "; bytes: " + filedata.length);
        PlainTextDataModel plainTextDataModel = convertToPlainText(filename, filedata, WORD_URIPATH);
        log.debug("plainTextDataModel.getInputFilename(): " + plainTextDataModel.getInputFilename());
        log.debug("plainTextDataModel.getInputContentType(): " + plainTextDataModel.getInputContentType());
        log.debug("plainTextDataModel.getFiledata(): " + plainTextDataModel.getFiledata());

        return plainTextDataModel.getFiledata();
    }

    public String convertPowerPointToPlainText(String filename, InputStream inputStream) throws IOException {
        log.info("convertPowerPointToPlainText()... filename: " + filename + "; Inputstream");
        return convertPowerPointToPlainText(filename, InputStreamToBytes(inputStream));
    }

    public String convertPowerPointToPlainText(String filename, byte[] filedata) throws IOException {
        log.info("convertPowerPointToPlainText()... filename: " + filename + "; bytes: " + filedata.length);
        PlainTextDataModel plainTextDataModel = convertToPlainText(filename, filedata, POWERPOINT_URIPATH);
        log.debug("plainTextDataModel.getInputFilename(): " + plainTextDataModel.getInputFilename());
        log.debug("plainTextDataModel.getInputContentType(): " + plainTextDataModel.getInputContentType());
        log.debug("plainTextDataModel.getFiledata(): " + plainTextDataModel.getFiledata());

        return plainTextDataModel.getFiledata();
    }
    @Override
    public boolean isTransformableToPdf(String sourceMimetype) {
        log.info("isTransformableToPdf()... sourceMimetype: " + sourceMimetype);
        return supportedSourceMimetypesForPdf.contains(StringUtils.lowerCase(sourceMimetype));
    }

    public String getFileExtByMimetype(String mimeType){
        log.info("getFileExtByMimetype()... mimeType: " + mimeType);
        if(supportedSourceMimetypesForPdfWithExt.containsKey(StringUtils.lowerCase(mimeType))){
            return supportedSourceMimetypesForPdfWithExt.get(StringUtils.lowerCase(mimeType));
        }
        log.debug("Return UNK file extension!");
        return "unk";
    }

    @Override
    public boolean isFormulasReplaceable(String sourceMimetype) {
        log.info("isFormulasReplaceable()... sourceMimetype: " + sourceMimetype);
        // Basically the same check is performed in DocumentTemplateServiceImpl#replaceFormulas, but with file extensions (.doc .docx .dot .dotx)
        return MIMETYPE_DOC.equalsIgnoreCase(sourceMimetype) || MIMETYPE_DOCX.equalsIgnoreCase(sourceMimetype)
                || MIMETYPE_DOT.equalsIgnoreCase(sourceMimetype) || MIMETYPE_DOTX.equalsIgnoreCase(sourceMimetype);
    }

    @Override
    public void transformToPdf(ContentReader documentReader, ContentWriter pdfWriter, String fileUuid) throws Exception {
        log.info("transformToPdf()... fileUuid: " + fileUuid);
        try {
            if (!MimetypeMap.MIMETYPE_PDF.equalsIgnoreCase(pdfWriter.getMimetype())) {
                throw new IllegalArgumentException("Only target mime type " + MimetypeMap.MIMETYPE_PDF + " is supported");
            }
            if (documentReader == null) {
                return;
            }
            String mimetype = documentReader.getMimetype().toLowerCase();
            log.debug("MIME TYPE: " + mimetype);

            if (!isTransformableToPdf(mimetype)) {
                throw new IllegalArgumentException("Source mime type is not supported for transformToPdf: " + mimetype);
            }

            long duration = -1;
            try {
                long startTime = System.nanoTime();
                byte[] output;
                try {
                    output = convertToPdf("transformable file." + getFileExtByMimetype(mimetype), getDocumentBytes(documentReader));
                } finally {
                    StatisticsPhaseListener.addTimingNano(StatisticsPhaseListenerLogColumn.SRV_MSO, startTime);
                }
                duration = CalendarUtil.duration(startTime);

                pdfWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                pdfWriter.setEncoding("UTF-8");
                ByteArrayInputStream bis = new ByteArrayInputStream(output);
                pdfWriter.putContent(bis);

            } finally {
                log.info("PERFORMANCE: query mso2.convertToPdf - " + duration + " ms|" + documentReader.getSize() + "|" + documentReader.getMimetype() + "|"
                        + documentReader.getEncoding() + "|" + pdfWriter.getSize());
            }
        } catch (Exception e) {
            log.error("Error in transformToPdf! | ContentURL: " + documentReader.getContentUrl() + " | Input mimetype: " + documentReader.getMimetype().toLowerCase(), e);
            throw e;
        }
    }

    public static String convertObjectToJson(Object object) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(object);
    }

    @Override
    public boolean replaceFormulas(Map<String, String> formulas, ContentReader documentReader, ContentWriter documentWriter, boolean dontSaveIfUnmodified) throws Exception {
        log.debug("replaceFormulas()...");
        try {
            DocumentAndFormulasInput input = replaceFormulasPrepare(formulas, documentReader);
            if (input == null) {
                return false;
            }
            input.setDontSaveIfUnmodified(dontSaveIfUnmodified);
            long duration = -1;
            try {
                log.info("Sending request to perform Mso2.replaceFormulas, formulas=[" + formulas.size() + "] documentReader=" + documentReader);

                long startTime = System.nanoTime();
                DocumentOutput output;
                try {
                    output = replaceFormulasReq(input);
                } finally {
                    StatisticsPhaseListener.addTimingNano(StatisticsPhaseListenerLogColumn.SRV_MSO, startTime);
                }
                if (dontSaveIfUnmodified && output.getDocumentFile() == null) {
                    log.info("MSO-REST service didn't modify file, file is not updated.");
                    return false;
                }
                duration = CalendarUtil.duration(startTime);

                String inputMimeType = documentReader.getMimetype();
                String outputMimeType;
                if (inputMimeType.equalsIgnoreCase(MIMETYPE_DOT) || inputMimeType.equalsIgnoreCase(MIMETYPE_DOC)) {
                    outputMimeType = MIMETYPE_DOC;
                } else if (inputMimeType.equalsIgnoreCase(MIMETYPE_DOTX) || inputMimeType.equalsIgnoreCase(MIMETYPE_DOCX)) {
                    outputMimeType = MIMETYPE_DOCX;
                } else {
                    throw new IllegalArgumentException("Unsupported input mimeType " + inputMimeType);
                }
                documentWriter.setMimetype(outputMimeType);
                documentWriter.setEncoding("UTF-8");
                ByteArrayInputStream bis = new ByteArrayInputStream(output.getDocumentFile());
                documentWriter.putContent(bis);
                return true;
            } finally {
                log.info("PERFORMANCE: query MSO.REST.replaceFormulas - " + duration + " ms|" + documentReader.getSize() + "|" + documentReader.getMimetype() + "|"
                        + documentReader.getEncoding() + "|" + documentWriter.getSize() + "|" + formulas.size());
            }
        } catch (Exception e) {
            log.error("Error in replaceFormulas", e);
            throw e;
        }
    }

    @Override
    public void replaceFormulasAndTransformToPdf(Map<String, String> formulas, ContentReader documentReader, ContentWriter documentWriter, ContentWriter pdfWriter)
            throws Exception {
        log.info("replaceFormulasAndTransformToPdf()...");
        // TODO SMIT's new MSO service didn't return document file in the interface as of 13.04.2012
// @formatter:off
/*
        try {
            MsoDocumentAndFormulasInput input = replaceFormulasPrepare(formulas, documentReader);
            if (input == null) {
                return;
            }

            long duration = -1;
            try {
                log.info("Sending request to perform Mso2.replaceFormulasAndTransformToPdf, formulas=[" + formulas.size() + "] documentReader=" + documentReader);

                long startTime = System.nanoTime();
                MsoDocumentAndPdfOutput output;
                try {
                    output = mso.replaceFormulasAndConvertToPdf(input);
                } finally {
                    StatisticsPhaseListener.addTimingNano(StatisticsPhaseListenerLogColumn.SRV_MSO, startTime);
                }
                duration = CalendarUtil.duration(startTime);

                String inputMimeType = documentReader.getMimetype();
                String outputMimeType;
                if (inputMimeType.equalsIgnoreCase(MIMETYPE_DOT) || inputMimeType.equalsIgnoreCase(MIMETYPE_DOC)) {
                    outputMimeType = MIMETYPE_DOC;
                } else if (inputMimeType.equalsIgnoreCase(MIMETYPE_DOTX) || inputMimeType.equalsIgnoreCase(MIMETYPE_DOCX)) {
                    outputMimeType = MIMETYPE_DOCX;
                } else {
                    throw new IllegalArgumentException("Unsupported input mimeType " + inputMimeType);
                }
                documentWriter.setMimetype(outputMimeType);
                documentWriter.setEncoding("UTF-8");
                ByteArrayInputStream bis = new ByteArrayInputStream(output.getDocumentFile().getValue());
                documentWriter.putContent(bis);

                pdfWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                pdfWriter.setEncoding("UTF-8");
                bis = new ByteArrayInputStream(output.getPdfFile().getValue());
                pdfWriter.putContent(bis);

            } finally {
                log.info("PERFORMANCE: query mso2.replaceFormulasAndConvertToPdf - " + duration + " ms|" + documentReader.getSize() + "|" + documentReader.getMimetype() + "|"
                        + documentReader.getEncoding() + "|" + documentWriter.getSize() + "|" + pdfWriter.getSize() + "|" + formulas.size());
            }
        } catch (Exception e) {
            log.error("Error in replaceFormulasAndTransformToPdf", e);
            throw e;
        }
 */
// @formatter:on
    }

    @Override
    public Map<String, String> modifiedFormulas(ContentReader documentReader, String fileUuid) throws Exception {
        log.info("modifiedFormulas()... fileUuid: " + fileUuid);
        try {
            if (documentReader == null) {
                return null;
            }

            Map<String, String> formulas = new LinkedHashMap<String, String>();
            long duration = -1;
            try {

                DocumentInput input = getDocumentInput(documentReader, fileUuid);
                log.info("Sending request to perform MSO.REST.modifiedFormulas, documentReader=" + documentReader);

                long startTime = System.nanoTime();
                FormulaOutput output;
                try {
                    output = getDocumentFormulas(input);
                } finally {
                    StatisticsPhaseListener.addTimingNano(StatisticsPhaseListenerLogColumn.SRV_MSO, startTime);
                }
                duration = CalendarUtil.duration(startTime);

                if (output.getFormulas() == null || output.getFormulas().size() == 0) {
                    log.info("MSO getModifiedFormulas is NULL!");
                    return null;
                }

                for (MSOFormula formula : output.getFormulas()) {
                    if (formula.getKey() == null || StringUtils.isBlank(formula.getKey())) {
                        log.debug("Formula key or value is null or blanck...");
                        continue;
                    }

                    String formulaValue = formula.getValue() == null ? null : formula.getValue();
                    log.debug("formula key: " + formula.getKey() + " ==> value: [" + formulaValue + "]");

                    formulas.put(formula.getKey(), formulaValue);
                }
                return formulas;

            } finally {
                log.info("PERFORMANCE: query mso2.modifiedFormulas - " + duration + " ms|" + documentReader.getSize() + "|" + documentReader.getMimetype() + "|"
                        + documentReader.getEncoding() + "|" + formulas.size());
            }
        } catch (Exception e) {
            log.error("Error in getModifiedFormulas", e);
            throw e;
        }
    }

    /**
     * String localVarPath = "/api/Mso/convert/word/text";
     * String localVarPath = "/api/Mso/convert/excel/text";
     *
     * @param filename
     * @param filedata
     * @param localVarPath
     * @return
     */
    private PlainTextDataModel convertToPlainText(String filename, byte[] filedata, String localVarPath) {
        log.info("convertToPlainText()... filename: " + filename + "; bytes: " + filedata.length);
        trustSSL();
        String URI = msoRestService.getServiceUrl() + localVarPath;
        log.debug("convertToPlainText(): URI: " + URI);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        HttpUtils.addFilesAsMultiPartFiles("file", body, filedata, filename);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, HttpUtils.getMultipartFormDataHeaders());

        return HttpUtils.exchangeObjectPostRequest(URI, requestEntity, PlainTextDataModel.class);
    }

    private DocumentInput getDocumentInput(ContentReader documentReader, String fileUuid) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream((int) documentReader.getSize());
        documentReader.getContent(bos);

        final DocumentInput input = new DocumentInput();
        input.setDocumentFile(bos.toByteArray());
        input.setFileEncoding(documentReader.getEncoding());
        input.setFileType(documentReader.getMimetype());
        input.setUid(fileUuid);
        input.setIdCode("NOT_IMPLEMENTED");
        return input;
    }

    private byte[] getDocumentBytes(ContentReader documentReader) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream((int) documentReader.getSize());
        documentReader.getContent(bos);

        return bos.toByteArray();
    }

    private DocumentAndFormulasInput replaceFormulasPrepare(Map<String, String> formulas, ContentReader documentReader) {
        if (documentReader == null) {
            return null;
        }

        String mimetype = documentReader.getMimetype().toLowerCase();
        if (!isFormulasReplaceable(mimetype)) {
            throw new IllegalArgumentException("Source mime type is not supported for replaceFormulas: " + mimetype);
        }

        return getDocumentAndFormulasInput(formulas, documentReader);
    }

    private DocumentAndFormulasInput getDocumentAndFormulasInput(Map<String, String> formulas, ContentReader documentReader) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream((int) documentReader.getSize());
        documentReader.getContent(bos);

        List<MSOFormula> formulaList = new ArrayList<>();
        for (Map.Entry<String, String> entry : formulas.entrySet()) {
            MSOFormula f = new MSOFormula();
            f.setKey(entry.getKey());
            f.setValue(entry.getValue());
            formulaList.add(f);
        }

        DocumentAndFormulasInput input = new DocumentAndFormulasInput();
        input.setDocumentFile(bos.toByteArray());
        input.setFileEncoding(documentReader.getEncoding());
        input.setFileType(documentReader.getMimetype());
        input.setFormulas(formulaList);
        return input;
    }

    private byte[] convertToPdf(String filename, byte[] filedata){
        log.info("convertToPdf()... filename: " + filename + "; filebytes: " + filedata.length);
        trustSSL();
        String localVarPath = "/api/Mso/convert/pdf/file";
        String URI = msoRestService.getServiceUrl() + localVarPath;
        log.info("MSO-REST REQUEST URI: " + URI);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        HttpUtils.addFilesAsMultiPartFiles("file", body, filedata, filename);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, HttpUtils.getMultipartFormDataHeaders());

        return HttpUtils.exchangeObjectPostRequest(URI, requestEntity, byte[].class);
    }

    private FormulaOutput getDocumentFormulas(DocumentInput input) {
        log.info("getDocumentFormulas()... ");
        trustSSL();
        String localVarPath = "/api/Mso/ModifiedFormulas";
        String URI = msoRestService.getServiceUrl() + localVarPath;
        log.info("MSO-REST REQUEST URI: " + URI);

        try {
            return (FormulaOutput) makeObjectPostRequest(FormulaOutput.class, input, URI);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private DocumentOutput replaceFormulasReq(DocumentAndFormulasInput input) {
        log.info("replaceFormulasReq()...");
        trustSSL();
        String localVarPath = "/api/Mso/ReplaceFormulas";
        String URI = msoRestService.getServiceUrl() + localVarPath;
        log.info("MSO-REST REQUEST URI: " + URI);

        try {
            return (DocumentOutput) makeObjectPostRequest(DocumentOutput.class, input, URI);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;

    }
}
