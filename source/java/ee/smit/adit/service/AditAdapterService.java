package ee.smit.adit.service;

public interface AditAdapterService {
    String BEAN_NAME = "AditAdapterService";

    String getUri();

    boolean isAditAdapterActive();

    boolean isAditCheckActive();

    String getRegCode();
}
