package ee.smit.amr;

import ee.smit.amr.model.AmetnikListV2Resp;
import ee.smit.amr.model.AmetnikV2Resp;
import ee.smit.amr.model.YksusListV2Resp;
import ee.smit.amr.model.YksusV2Resp;
import ee.smit.users.UnitInfo;
import ee.smit.users.UserInfo;

import java.util.List;

public interface AmrRestService {
    String BEAN_NAME = "AmrRestService";

    YksusListV2Resp getUnitsByCompanyId();

    AmetnikListV2Resp getJobsByCompanyId();

    UserInfo getJobBySocialSecNr(String socialSecurityNr);

    UserInfo convertAmetnikV2Resp(AmetnikV2Resp ametnik);

    UserInfo[] convertAmetnikListV2Resp(AmetnikListV2Resp resp);

    UnitInfo convertYksusV2Resp(YksusV2Resp resp);

    UnitInfo[] convertYksusListV2Resp(YksusListV2Resp resp);

    List<String> getRS();

    boolean hasRs(String socialSecNr);

    boolean getRemoveGroupsEmail();

    boolean isActive();

}
