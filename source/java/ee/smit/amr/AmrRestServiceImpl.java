package ee.smit.amr;

import ee.smit.amr.model.AmetikohtV2Resp;
import ee.smit.amr.model.AmetnikListBody;
import ee.smit.amr.model.AmetnikListV2Resp;
import ee.smit.amr.model.AmetnikV2Resp;
import ee.smit.amr.model.PuudumineV2Resp;
import ee.smit.amr.model.RsLubaBody;
import ee.smit.amr.model.RsLubaByIsikukoodBody;
import ee.smit.amr.model.RsLubaListResp;
import ee.smit.amr.model.RsLubaResp;
import ee.smit.amr.model.RsResp;
import ee.smit.amr.model.YksusListBody;
import ee.smit.amr.model.YksusListV2Resp;
import ee.smit.amr.model.YksusV2Resp;
import ee.smit.auth.TokenStore;
import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.common.JsonUtils;
import ee.smit.users.UnitInfo;
import ee.smit.users.UserInfo;
import ee.webmedia.alfresco.utils.UnableToPerformException;
import org.apache.http.auth.AuthenticationException;
import org.joda.time.Interval;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ee.smit.common.http.HttpUtils.restPostReq;

public class AmrRestServiceImpl implements AmrRestService {
    private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(AmrRestServiceImpl.class);

    private Long asutusId;
    private Boolean removeGroupsEmail = false;
    private boolean active;
    private String svcUser;
    private String svcPass;
    private String serviceUrl;
    private String amrUserAwayCodes;
    protected TokenStore tokenStore;

    private Oauth2TokenInfo getToken() throws AuthenticationException {
        String URI = serviceUrl + "/login";
        log.info("getToken()... URI: " + URI);
        return tokenStore.getToken(URI, svcUser.trim(), svcPass.trim());
    }

    /**
     * "asutusId": 1,
     * "yksusId": null,
     * "aktiivsedOnly": true,
     * "kasAK": false,
     * "muudetudAlates": null
     *
     * @return
     */
    @Override
    public YksusListV2Resp getUnitsByCompanyId() {
        String URI = serviceUrl + "/api/v3/yksusList";
        log.info("getUnitsByCompanyId()... URI: " + URI);

        YksusListBody body = new YksusListBody().asutusId(asutusId);
        //body.setKasAK(true);
        body.setAktiivsedOnly(true);

        try {
            if(log.isTraceEnabled()){
                log.trace("REQUEST JSON: " + JsonUtils.convertObjectToJson(body));
            }
            YksusListV2Resp resp = restPostReq(URI, body, YksusListV2Resp.class, getToken());
            if (resp.getError() != null) {
                throw new UnableToPerformException(resp.getError());
            }
            return resp;

        } catch (Exception e) {
            throw new UnableToPerformException(e.getMessage());
        }
    }

    private void setAmetnikListRequestParams(AmetnikListBody body){
        body.setKasAmetikohad(true);
        body.setKasHaridus(false);
        body.setKasPuudumised(true);
        body.setAktiivsedPuudumisedOnly(true);
        body.setKasAsendused(true);
        body.setAktiivsedAsendusedOnly(true);
        body.setKasKvalifikatsioonid(false);
        body.setKasKontaktid(true);
        body.setKasPseudoYksused(false);
        body.setKasKoolitused(false);
        body.setKasAmetVV(false);
        body.setKasAmetSV(false);

    }

    @Override
    public AmetnikListV2Resp getJobsByCompanyId() {
        String URI = serviceUrl + "/api/v3/ametnikList";
        log.info("getJobsByCompanyId()... URI: " + URI);

        AmetnikListBody body = new AmetnikListBody().asutusId(asutusId);
        setAmetnikListRequestParams(body);

        try {
            if(log.isTraceEnabled()){
                log.trace("REQUEST JSON: " + JsonUtils.convertObjectToJson(body));
            }
            AmetnikListV2Resp resp = restPostReq(URI, body, AmetnikListV2Resp.class, getToken());

            if (resp.getError() != null) {
                throw new UnableToPerformException(resp.getError());
            }
            return resp;

        } catch (Exception e) {
            throw new UnableToPerformException(e.getMessage());
        }
    }

    @Override
    public UserInfo getJobBySocialSecNr(String socialSecurityNr) {
        String URI = serviceUrl + "/api/v3/ametnikList";
        log.info("getJobBySocialSecNr()... URI: " + URI + "; asutusId: " + asutusId + "; socialSecurityNr: " + socialSecurityNr);

        AmetnikListBody body = new AmetnikListBody().asutusIdAndIsikukood(asutusId, socialSecurityNr);
        setAmetnikListRequestParams(body);

        try {
            if(log.isTraceEnabled()){
                log.trace("REQUEST JSON: " + JsonUtils.convertObjectToJson(body));
            }
            AmetnikListV2Resp resp = restPostReq(URI, body, AmetnikListV2Resp.class, getToken());
            UserInfo[] infos = convertAmetnikListV2Resp(resp);
            if (infos == null || infos.length == 0) {
                return null;
            }
            if (infos.length > 1) {
                log.warn("Found more than 1 user!");
                for (UserInfo info : infos) {
                    log.info("Name: [" + info.getFirstname() + " " + info.getLastname() + "], socialSecNr: [" + info.getSocialSecNr() + "]");
                }
            } else {
                log.info("USER INFO FOUND FROM AMR-REST - Name: [" + infos[0].getFirstname() + " " + infos[0].getLastname() + "], socialSecNr: [" + infos[0].getSocialSecNr() + "]");
            }

            return infos[0];
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new UnableToPerformException(e.getMessage());
        }
    }

    @Override
    public UserInfo convertAmetnikV2Resp(AmetnikV2Resp ametnik) {
        if (ametnik == null || ametnik.getAmetikohad() == null || ametnik.getAmetikohad().size() == 0)
            return new UserInfo();

        AmetikohtV2Resp job = findByBiggestWorkload(ametnik.getAmetikohad());

        UserInfo info = new UserInfo();
        info.setSocialSecNr(ametnik.getIsikukood());
        info.setFirstname(ametnik.getEesnimi());
        info.setLastname(ametnik.getPerekonnanimi());
        info.setSapCode(ametnik.getSapKood());

        if (job != null) {
            if(job.getPuudumised() != null && job.getPuudumised().size() > 0){

                if(isUserAway(job.getPuudumised())){
                    log.info("USER: "+ ametnik.getIsikukood() + " is in AWAY status! Remove from list....");
                    return null;
                }
            }
            info.setJobTitle(job.getAmetikohaPikkTekst());
            info.setServiceRank(job.getTeenistusaste());
            info.setOrgId(job.getAsutusId());
            info.setUnitId(job.getYksusId());
            info.setUnitPath(job.getYksusRada());
            // TODO: kas kasutada ametikoha kontakte või ametikohaga seotud kontaktide objekti kontakte?
            if (job.getKontakt() != null) {
                info.setEmail(job.getKontakt().getEmail());

                // TODO: kas kasutada mobiil või laua telefoni?
                info.setPhoneNr(job.getKontakt().getLauatelefon());

                info.setCounty(job.getKontakt().getMaakond());
                info.setMunicipality(job.getKontakt().getLinn());
                info.setCityRegion(job.getKontakt().getLinnaosa());
                info.setStreet(getSteetWithNr(job.getKontakt().getTanav(), job.getKontakt().getHausn()));
                info.setPostalCode(job.getSihtkood());
            }

        }
        return info;
    }

    private boolean isUserAway(List<PuudumineV2Resp> puudumised){
        log.info("isUserAway()...puudumised.size(): " + puudumised.size());

        for(PuudumineV2Resp puudumine : puudumised){
            log.info("PUUDUMINE ACTIVE: " + puudumine.getAktiivne() + "; LiikKood: " + puudumine.getPuudumineLiikKood() + "; StartDate: " + puudumine.getAlgusKuupaev() + "; EndDate: " + puudumine.getLoppKuupaev());
            if(puudumine.getAktiivne()){
                if(isDateBetween(convertAmrDate(puudumine.getAlgusKuupaev()), convertAmrDate(puudumine.getLoppKuupaev()), new Date())){
                    if(getUserAwayCodes().contains(puudumine.getPuudumineLiikKood())){
                        log.info("PUUDUMINE Away is TRUE!");
                        return true;
                    }
                }
            }
        }
        log.info("PUUDUMINE Away is FALSE!");
        return false;
    }

    private Date convertAmrDate(String date){
        if(date == null){
            return null;
        }

        if(date.contains("T") || date.contains(" ")){
            String[] partsT = date.split("[T ]+");
            if(partsT.length > 0){
                String dateFirstPart = partsT[0];
                return convertToDate(dateFirstPart, "yyyy-MM-dd");
            }
        }
        return null;
    }

    private boolean isValid(String dateStr, String dateFormat) {
        return convertToDate(dateStr, dateFormat) != null;
    }

    private Date convertToDate(String dateStr, String dateFormat){
        DateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        try {
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private boolean isDateBetween(Date startDate, Date endDate, Date dateToCheck){
        Interval interval = new Interval(startDate.getTime(), endDate.getTime());
        return interval.contains(dateToCheck.getTime());
    }

    private String getSteetWithNr(String street, String nr) {
        if (nr == null) {
            if (street == null) {
                return "";
            }
            return street;
        }
        return street + " " + nr;
    }

    private AmetikohtV2Resp findByBiggestWorkload(List<AmetikohtV2Resp> jobs) {
        if (jobs.size() == 1) {
            return jobs.get(0);
        }

        Float biggestWorkLoadValue = null;
        Integer jobId = null;

        for (int i = 0; i < jobs.size(); i++) {
            AmetikohtV2Resp job = jobs.get(i);
            if (job.getAsutusId() == null || !job.getAsutusId().equals(asutusId)) {
                continue;
            }

            if (job.getKoormus() == null) {
                continue;
            }
            if (biggestWorkLoadValue == null) {
                biggestWorkLoadValue = job.getKoormus();
                jobId = i;
                continue;
            }

            if (biggestWorkLoadValue < job.getKoormus()) {
                biggestWorkLoadValue = job.getKoormus();
                jobId = i;
            }
        }

        if (jobId == null) {
            return jobs.get(0);
        }

        return jobs.get(jobId);
    }

    @Override
    public UserInfo[] convertAmetnikListV2Resp(AmetnikListV2Resp resp) {
        if (resp == null || resp.getAmetnikud() == null || resp.getAmetnikud().size() == 0) {
            return new UserInfo[0];
        }
        List<UserInfo> infos = new ArrayList<>();

        for (AmetnikV2Resp user : resp.getAmetnikud()) {
            UserInfo userInfo = convertAmetnikV2Resp(user);
            if(userInfo != null){
                infos.add(userInfo);
            } else {
                log.error("Failed to convert AmetnikV2Resp user object to UserInfo object!!! username: " + user.getIsikukood() + "; " + user.getEesnimi() + "; " + user.getPerekonnanimi());
            }
        }

        return infos.toArray(new UserInfo[0]);
    }

    @Override
    public UnitInfo convertYksusV2Resp(YksusV2Resp resp) {
        if (resp == null) {
            log.warn("YksusV2Resp object is null! return new UnitInfo()...");
            return new UnitInfo();
        }

        UnitInfo info = new UnitInfo();
        info.setOrgId(resp.getAsutusId());
        if (resp.getYlemYksusId() != null) {
            info.setSuperUnitId(resp.getYlemYksusId());
        }
        info.setUnitId(resp.getId());
        info.setName(resp.getNimetus());
        info.setEmail(resp.getEmail());
        info.setUnitPath(resp.getTaisNimetus());
        return info;
    }

    @Override
    public UnitInfo[] convertYksusListV2Resp(YksusListV2Resp resp) {
        log.info("convertYksusListV2Resp()...");
        if (resp == null || resp.getYksused() == null) {
            return new UnitInfo[0];
        }

        int i = 0;
        UnitInfo[] infos = new UnitInfo[resp.getYksused().size()];
        log.trace("UnitInfo[] infos length: " + infos.length);
        for (YksusV2Resp unit : resp.getYksused()) {
            infos[i] = convertYksusV2Resp(unit);
            i++;
        }
        return infos;
    }

    @Override
    public List<String> getRS() {
        RsLubaListResp resp = getRsList();

        if (resp == null || resp.getRsLoad() == null || resp.getRsLoad().size() == 0) {
            return null;
        }

        List<String> rsList = new ArrayList<>();

        for (RsResp rs : resp.getRsLoad()) {
            if (rs.isOnLuba()) {
                rsList.add(rs.getIsikukood());
            }
        }

        return rsList;
    }

    private RsLubaListResp getRsList() {
        String URI = serviceUrl + "/api/v2/allRSLoad";
        log.info("getRsList()... URI: " + URI);

        RsLubaBody body = new RsLubaBody();
        body.asutusId(asutusId);

        try {
            if(log.isTraceEnabled()){
                log.trace("REQUEST JSON: " + JsonUtils.convertObjectToJson(body));
            }
            RsLubaListResp resp = restPostReq(URI, body, RsLubaListResp.class, getToken());
            if (resp.getError() != null) {
                throw new UnableToPerformException(resp.getError());
            }
            return resp;
        } catch (Exception e) {
            throw new UnableToPerformException(e.getMessage());
        }
    }

    @Override
    public boolean hasRs(String socialSecNr) {
        String URI = serviceUrl + "/api/v2/rsLubaByIsikukood";
        log.info("hasRs()... URI: " + URI);

        RsLubaByIsikukoodBody body = new RsLubaByIsikukoodBody().isikukood(socialSecNr);
        try {
            if(log.isTraceEnabled()){
                log.trace("REQUEST JSON: " + JsonUtils.convertObjectToJson(body));
            }
            RsLubaResp resp = restPostReq(URI, body, RsLubaResp.class, getToken());
            if (resp == null || resp.getRsLuba() == null) {
                log.error("No response from AMR-REST service! Can't check RS status!");
                return false;
            }

            if (resp.getError() != null) {
                log.error("AMR-REST service response has ERRORS! " + resp.getError());
                return false;
            }

            return resp.getRsLuba().isOnLuba();

        } catch (Exception e) {
            log.error("AMR-REST service has ERRORS! " + e.getMessage(), e);
            //throw new UnableToPerformException(e.getMessage());
            return false;
        }
    }

    // -- SETTER, GETTER --------------------------------
    public void setAsutusId(Long asutusId) {
        log.info("---------------------------------------------------------------------------");
        log.info("setAsutusId(): value = [" + asutusId+ "]");
        log.info("---------------------------------------------------------------------------");
        this.asutusId = asutusId;
    }

    public void setSvcUser(String svcUser) {
        log.info("---------------------------------------------------------------------------");
        log.info("setSvcUser(): value = [" + svcUser+ "]");
        log.info("---------------------------------------------------------------------------");
        this.svcUser = svcUser;
    }

    public void setSvcPass(String svcPass) {
        log.trace("---------------------------------------------------------------------------");
        log.trace("setSvcPass(): value.length() = [" + svcPass.length()+ "]");
        log.trace("---------------------------------------------------------------------------");
        this.svcPass = svcPass;
    }

    public void setServiceUrl(String serviceUrl) {
        log.info("---------------------------------------------------------------------------");
        log.info("setServiceUrl(): value = [" + serviceUrl+ "]");
        log.info("---------------------------------------------------------------------------");
        this.serviceUrl = serviceUrl;
    }

    @Override
    public boolean isActive() {
        log.info("---------------------------------------------------------------------------");
        log.info("isActive(): value = [" + active + "]");
        log.info("---------------------------------------------------------------------------");

        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getRemoveGroupsEmail() {
        log.info("---------------------------------------------------------------------------");
        log.info("getRemoveGroupsEmail(): value = [" + removeGroupsEmail + "]");
        log.info("---------------------------------------------------------------------------");
        return this.removeGroupsEmail;
    }

    public void setRemoveGroupsEmail(boolean removeGroupsEmail) {
        log.info("---------------------------------------------------------------------------");
        log.info("setRemoveGroupsEmail(): value = [" + removeGroupsEmail + "]");
        log.info("---------------------------------------------------------------------------");

        this.removeGroupsEmail = removeGroupsEmail;
    }

    public List<String> getUserAwayCodes() {
        List<String> strings = new ArrayList<>();
        if(amrUserAwayCodes == null) {
            return strings;
        }
        String splitter = ",";
        if(amrUserAwayCodes.contains(splitter)){
            strings = convertStringToList(amrUserAwayCodes, splitter);
        } else {
            strings.add(amrUserAwayCodes.trim());
        }
        log.info("getUserAwayCodes(): [" + listToString(strings) + "]");
        return strings;
    }

    private List<String> convertStringToList(String str, String splitter){
        List<String> list = new ArrayList<>();
        String[] parts = str.split(splitter);
        for(String part : parts){
            list.add(part.trim());
        }

        return list;
    }

    private String listToString(List<?> objects){
        if(objects == null){
            return null;
        }
        StringBuilder str = new StringBuilder();
        for (Object obj : objects) {
            str.append(", ").append(obj);
        }
        return "List{" + str + "}";
    }

    public void setAmrUserAwayCodes(String amrUserAwayCodes) {
        log.info("---------------------------------------------------------------------------");
        log.info("setAmrUserAwayCodes(): value = [" + amrUserAwayCodes + "]");
        log.info("---------------------------------------------------------------------------");

        this.amrUserAwayCodes = amrUserAwayCodes;
    }

    public void setTokenStore(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }
}
