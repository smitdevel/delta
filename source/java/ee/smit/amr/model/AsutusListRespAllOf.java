/*
 * Ametnikservices
 * Ametnikservices kasutamiseks pead endale kõigepealt tellima SVC konto. Seejärel telli soovitud päringutele ligipääs ning ulatus (asutus või struktuuriüksus). Peale ligipääsu saamist on võimalik neid alljärgnevalt ka ise katsetada. Palume mitte kasutada `deprecated` päringuid. Päringute tegemisel on soovituslik teha ennem `/login` et saada `access_token` ja lisada saadud token päisesse `Authorization: Bearer access_token` edasiste päringute tegemisel. Võimalus on ka iga päringuga panna kaasa username ja password, aga see teeb päringut aeglasemaks.
 *
 * The version of the OpenAPI document: 1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package ee.smit.amr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * AsutusListRespAllOf
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AsutusListRespAllOf {
  private List<AsutusResp> asutused = null;

  public AsutusListRespAllOf() { 
  }

  public AsutusListRespAllOf asutused(List<AsutusResp> asutused) {
    
    this.asutused = asutused;
    return this;
  }

  public AsutusListRespAllOf addAsutusedItem(AsutusResp asutusedItem) {
    if (this.asutused == null) {
      this.asutused = new ArrayList<AsutusResp>();
    }
    this.asutused.add(asutusedItem);
    return this;
  }

   /**
   * Päringuga leitud asutuste list.
   * @return asutused
  **/
  public List<AsutusResp> getAsutused() {
    return asutused;
  }


  public void setAsutused(List<AsutusResp> asutused) {
    this.asutused = asutused;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AsutusListRespAllOf asutusListRespAllOf = (AsutusListRespAllOf) o;
    return Objects.equals(this.asutused, asutusListRespAllOf.asutused);
  }

  @Override
  public int hashCode() {
    return Objects.hash(asutused);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AsutusListRespAllOf {\n");
    sb.append("    asutused: ").append(toIndentedString(asutused)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

