/*
 * Ametnikservices
 * Ametnikservices kasutamiseks pead endale kõigepealt tellima SVC konto. Seejärel telli soovitud päringutele ligipääs ning ulatus (asutus või struktuuriüksus). Peale ligipääsu saamist on võimalik neid alljärgnevalt ka ise katsetada. Palume mitte kasutada `deprecated` päringuid. Päringute tegemisel on soovituslik teha ennem `/login` et saada `access_token` ja lisada saadud token päisesse `Authorization: Bearer access_token` edasiste päringute tegemisel. Võimalus on ka iga päringuga panna kaasa username ja password, aga see teeb päringut aeglasemaks.
 *
 * The version of the OpenAPI document: 1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package ee.smit.amr.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

/**
 * S3 info
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class S3Url {
  private String url;

  private String expiration;

  public S3Url() { 
  }

  public S3Url url(String url) {
    
    this.url = url;
    return this;
  }

   /**
   * S3 ajutine url faili allalaadimiseks
   * @return url
  **/
  public String getUrl() {
    return url;
  }


  public void setUrl(String url) {
    this.url = url;
  }


  public S3Url expiration(String expiration) {
    
    this.expiration = expiration;
    return this;
  }

   /**
   * S3 urli kehtivuse lõpp
   * @return expiration
  **/
  public String getExpiration() {
    return expiration;
  }


  public void setExpiration(String expiration) {
    this.expiration = expiration;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    S3Url s3Url = (S3Url) o;
    return Objects.equals(this.url, s3Url.url) &&
        Objects.equals(this.expiration, s3Url.expiration);
  }

  @Override
  public int hashCode() {
    return Objects.hash(url, expiration);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class S3Url {\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    expiration: ").append(toIndentedString(expiration)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

