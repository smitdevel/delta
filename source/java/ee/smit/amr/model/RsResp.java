/*
 * Ametnikservices
 * Ametnikservices kasutamiseks pead endale kõigepealt tellima SVC konto. Seejärel telli soovitud päringutele ligipääs ning ulatus (asutus või struktuuriüksus). Peale ligipääsu saamist on võimalik neid alljärgnevalt ka ise katsetada. Palume mitte kasutada `deprecated` päringuid. Päringute tegemisel on soovituslik teha ennem `/login` et saada `access_token` ja lisada saadud token päisesse `Authorization: Bearer access_token` edasiste päringute tegemisel. Võimalus on ka iga päringuga panna kaasa username ja password, aga see teeb päringut aeglasemaks.
 *
 * The version of the OpenAPI document: 1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package ee.smit.amr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

/**
 * Päringuga leitud ametniku RS luba.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RsResp {
  private String nimi;

  private String isikukood;

  private Boolean onLuba;

  public RsResp() { 
  }

  public RsResp nimi(String nimi) {
    
    this.nimi = nimi;
    return this;
  }

   /**
   * Ametniku nimi.
   * @return nimi
  **/
  public String getNimi() {
    return nimi;
  }


  public void setNimi(String nimi) {
    this.nimi = nimi;
  }


  public RsResp isikukood(String isikukood) {
    
    this.isikukood = isikukood;
    return this;
  }

   /**
   * Ametniku isikukood.
   * @return isikukood
  **/
  public String getIsikukood() {
    return isikukood;
  }


  public void setIsikukood(String isikukood) {
    this.isikukood = isikukood;
  }


  public RsResp onLuba(Boolean onLuba) {
    
    this.onLuba = onLuba;
    return this;
  }

   /**
   * Kas ametnik omab RS luba.
   * @return onLuba
  **/
  public Boolean getOnLuba() {
    return onLuba;
  }


  public void setOnLuba(Boolean onLuba) {
    this.onLuba = onLuba;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RsResp rsResp = (RsResp) o;
    return Objects.equals(this.nimi, rsResp.nimi) &&
        Objects.equals(this.isikukood, rsResp.isikukood) &&
        Objects.equals(this.onLuba, rsResp.onLuba);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nimi, isikukood, onLuba);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RsResp {\n");
    sb.append("    nimi: ").append(toIndentedString(nimi)).append("\n");
    sb.append("    isikukood: ").append(toIndentedString(isikukood)).append("\n");
    sb.append("    onLuba: ").append(toIndentedString(onLuba)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public boolean isOnLuba() {
    return onLuba;
  }
}

