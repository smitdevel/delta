/*
 * Ametnikservices
 * Ametnikservices kasutamiseks pead endale kõigepealt tellima SVC konto. Seejärel telli soovitud päringutele ligipääs ning ulatus (asutus või struktuuriüksus). Peale ligipääsu saamist on võimalik neid alljärgnevalt ka ise katsetada. Palume mitte kasutada `deprecated` päringuid. Päringute tegemisel on soovituslik teha ennem `/login` et saada `access_token` ja lisada saadud token päisesse `Authorization: Bearer access_token` edasiste päringute tegemisel. Võimalus on ka iga päringuga panna kaasa username ja password, aga see teeb päringut aeglasemaks.
 *
 * The version of the OpenAPI document: 1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package ee.smit.amr.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

/**
 * Tagastatav asutuse objekt.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AsutusResp {
  private Long id;

  private String nimetus;

  private String registrikood;

  private String sapKood;

  private String kompaniiKood;

  private String muudetud;

  public AsutusResp() { 
  }

  public AsutusResp id(Long id) {
    
    this.id = id;
    return this;
  }

   /**
   * Asutuse id.
   * @return id
  **/
  public Long getId() {
    return id;
  }


  public void setId(Long id) {
    this.id = id;
  }


  public AsutusResp nimetus(String nimetus) {
    
    this.nimetus = nimetus;
    return this;
  }

   /**
   * Asutuse nimetus.
   * @return nimetus
  **/
  public String getNimetus() {
    return nimetus;
  }


  public void setNimetus(String nimetus) {
    this.nimetus = nimetus;
  }


  public AsutusResp registrikood(String registrikood) {
    
    this.registrikood = registrikood;
    return this;
  }

   /**
   * Asutuse registrikood.
   * @return registrikood
  **/
  public String getRegistrikood() {
    return registrikood;
  }


  public void setRegistrikood(String registrikood) {
    this.registrikood = registrikood;
  }


  public AsutusResp sapKood(String sapKood) {
    
    this.sapKood = sapKood;
    return this;
  }

   /**
   * Asutuse sap kood.
   * @return sapKood
  **/
  public String getSapKood() {
    return sapKood;
  }


  public void setSapKood(String sapKood) {
    this.sapKood = sapKood;
  }


  public AsutusResp kompaniiKood(String kompaniiKood) {
    
    this.kompaniiKood = kompaniiKood;
    return this;
  }

   /**
   * Asutuse kompanii kood.
   * @return kompaniiKood
  **/
  public String getKompaniiKood() {
    return kompaniiKood;
  }


  public void setKompaniiKood(String kompaniiKood) {
    this.kompaniiKood = kompaniiKood;
  }


  public AsutusResp muudetud(String muudetud) {
    
    this.muudetud = muudetud;
    return this;
  }

   /**
   * Muutmise kuupäev.
   * @return muudetud
  **/
  public String getMuudetud() {
    return muudetud;
  }


  public void setMuudetud(String muudetud) {
    this.muudetud = muudetud;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AsutusResp asutusResp = (AsutusResp) o;
    return Objects.equals(this.id, asutusResp.id) &&
        Objects.equals(this.nimetus, asutusResp.nimetus) &&
        Objects.equals(this.registrikood, asutusResp.registrikood) &&
        Objects.equals(this.sapKood, asutusResp.sapKood) &&
        Objects.equals(this.kompaniiKood, asutusResp.kompaniiKood) &&
        Objects.equals(this.muudetud, asutusResp.muudetud);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nimetus, registrikood, sapKood, kompaniiKood, muudetud);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AsutusResp {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    nimetus: ").append(toIndentedString(nimetus)).append("\n");
    sb.append("    registrikood: ").append(toIndentedString(registrikood)).append("\n");
    sb.append("    sapKood: ").append(toIndentedString(sapKood)).append("\n");
    sb.append("    kompaniiKood: ").append(toIndentedString(kompaniiKood)).append("\n");
    sb.append("    muudetud: ").append(toIndentedString(muudetud)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

