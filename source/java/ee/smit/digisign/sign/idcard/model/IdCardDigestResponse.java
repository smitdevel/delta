package ee.smit.digisign.sign.idcard.model;

import ee.smit.digisign.sign.model.digidoc.requests.DefaultResponse;

public class IdCardDigestResponse extends DefaultResponse {
    String uid;
    String digest;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }
}
