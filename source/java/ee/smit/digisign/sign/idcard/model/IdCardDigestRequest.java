package ee.smit.digisign.sign.idcard.model;

public class IdCardDigestRequest {
    String regCode;
    String certPem;
    Boolean bash64enc = false;

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getCertPem() {
        return certPem;
    }

    public void setCertPem(String certPem) {
        this.certPem = certPem;
    }

    public Boolean getBash64enc() {
        return bash64enc;
    }

    public void setBash64enc(Boolean bash64enc) {
        this.bash64enc = bash64enc;
    }
}
