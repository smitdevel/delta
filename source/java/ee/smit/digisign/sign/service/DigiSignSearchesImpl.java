package ee.smit.digisign.sign.service;

import com.google.common.io.ByteStreams;
import ee.smit.auth.TokenStore;
import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.common.http.RestUtil;
import ee.smit.digisign.sign.asics.model.AsicsSignRequest;
import ee.smit.digisign.sign.etamp.model.EStampSignRequestWithAttr;
import ee.smit.digisign.sign.idcard.model.IdCardDigestRequestWithAttr;
import ee.smit.digisign.sign.idcard.model.IdCardDigestResponse;
import ee.smit.digisign.sign.idcard.model.IdCardSignRequest;
import ee.smit.digisign.sign.mobileid.model.MIDContainerRequest;
import ee.smit.digisign.sign.mobileid.model.MIDSignRequestWithAttr;
import ee.smit.digisign.sign.mobileid.model.MIDSignResponse;
import ee.smit.digisign.sign.mobileid.model.MIDSignStatusResponse;
import ee.smit.digisign.sign.model.certificates.X509CertInfo;
import ee.smit.digisign.sign.model.certificates.principal.X509CertPrincipal;
import ee.smit.digisign.sign.model.certificates.timestamp.TimeStampTokenShort;
import ee.smit.digisign.sign.model.digidoc.file.RequestFiles;
import ee.smit.digisign.sign.model.digidoc.parse.v4.DigiDocParseEntity;
import ee.smit.digisign.sign.model.digidoc.parse.v4.DigidocDataEntity;
import ee.smit.digisign.sign.model.digidoc.parse.v4.DigidocSignatureEntity;
import ee.smit.digisign.sign.model.digidoc.requests.DigidocParseRequest;
import ee.smit.digisign.sign.model.digidoc.requests.SignAttr;
import ee.smit.oauth2.Oauth2Client;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.signature.exception.SignatureException;
import ee.webmedia.alfresco.signature.exception.SignatureRuntimeException;
import ee.webmedia.alfresco.signature.model.*;
import ee.webmedia.alfresco.utils.FilenameUtil;
import ee.webmedia.alfresco.utils.UnableToPerformException;
import ee.webmedia.alfresco.utils.UserUtil;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.auth.AuthenticationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static ee.smit.common.DateUtils.parseStringToDate;
import static ee.smit.common.http.RestUtil.addFileAsMultiPartFile;
import static ee.smit.common.http.RestUtil.addFilesAsMultiPartFiles;
import static ee.smit.common.http.RestUtil.makePostRequest;
import static ee.webmedia.alfresco.common.web.BeanHelper.getDigiDoc4JSignatureService;
import static ee.webmedia.alfresco.common.web.BeanHelper.getMimetypeService;

public class DigiSignSearchesImpl implements DigiSignSearches {
    protected final static Log log = LogFactory.getLog(DigiSignSearchesImpl.class);
    private final String ESTAMP_SIGN_URI = "/api/estamp/sign";

    private final String IDCARD_DIGEST_URI = "/api/idcard/v2/digest";
    private final String IDCARD_SIGN_URI = "/api/idcard/v2/sign";
    private final String IDCARD_SIGN_STREAM_URI = "/api/idcard/v2/signStream";

    private final String MID_SIGN_URI = "/api/mid/v2/sign";
    private final String MID_STATUS_URI = "/api/mid/v2/status";
    private final String MID_CONTAINTER_URI = "/api/mid/v2/container";
    
    private final String API_ASICS_CHECK_CRYPTO = "/api/asics/checkCrypto";
    private final String API_ASICS_CHECK_CRYPTO_AND_MAKE = "/api/asics/checkCryptoAndMake";
    private final String API_ASICS_CREATE = "/api/asics/create";
    private final String API_ASICS_SIGN = "/api/asics/sign";
    private final String API_DIGIDOC_PARSE = "/api/digidoc/parse";
    private final String API_DIGIDOC_PARSE_V4 = "/api/digidoc/parse/v4";

    private final String SIGN_OU_TYPE_DIGITAL_SIGNATURE = "digital signature";
    private final String SIGN_OU_TYPE_CORP_SIGNATURE = "corporate signature";

    protected DigiSignService digiSignService;
    protected TokenStore tokenStore;
    private String defaultRootDir;

    public static byte[] getBytesFromFilePath(String Path) throws IOException {
        Path path = Paths.get(Path);
        return Files.readAllBytes(path);
    }

    /**
     * Check that ASICS service exists and is active
     * @return
     */
    public boolean isAsicsActive(){
        String serverUrl = digiSignService.getUri() + API_ASICS_SIGN;
        try {
            return RestUtil.checkUrlExists(serverUrl, getToken());
        } catch (AuthenticationException e) {
            log.warn("Getting TOKEN error: " + e.getMessage());
        }
        return false;
    }

    public static Resource getFileResource(String fileName, String path) throws IOException {
        String fileExt = FilenameUtil.getDigiDocExt(fileName).toLowerCase();
        String fileBaseName = FilenameUtils.getBaseName(fileName);
        //todo replace tempFile with a real file
        Path tempFile = Files.createTempFile(fileBaseName, "." + fileExt);

        Files.write(tempFile, getBytesFromFilePath(path));

        log.debug("uploading: " + tempFile);
        File file = tempFile.toFile();
        //to upload in-memory bytes use ByteArrayResource instead
        return new FileSystemResource(file);
    }

    /**
     * @param ref
     * @return
     */
    private FileInfo getFileinfoByRef(NodeRef ref) {
        return BeanHelper.getFileFolderService().getFileInfo(ref);
    }

    /**
     * POST request
     *
     * @param files
     * @param requestJson
     * @return
     */
    @Override
    public byte[] signWithEStamp(MultipartFile[] files, EStampSignRequestWithAttr requestJson) throws AuthenticationException {
        String serverUrl = digiSignService.getUri() + ESTAMP_SIGN_URI;

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("files", files);
        body.add("digest", requestJson);

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(), getToken()));

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<byte[]> response = restTemplate.postForEntity(serverUrl, requestEntity, byte[].class);
        //response.getHeaders().
        return response.getBody();
    }

    private SignAttr setSignAttrValues(String state, String postalCode, String country, String city, String role) {
        SignAttr signAttr = new SignAttr();
        signAttr.setState(state);
        signAttr.setPostalCode(postalCode);
        signAttr.setCountry(country);
        signAttr.setCity(city);
        signAttr.setRole(role);
        return signAttr;
    }

    private IdCardDigestRequestWithAttr getIDCardDigestRequestWithAttr(String certPem, SignAttr signAttr, RequestFiles files) {
        IdCardDigestRequestWithAttr requestJson = new IdCardDigestRequestWithAttr();

        requestJson.setSignAttr(signAttr);
        requestJson.setFiles(files);
        requestJson.setCertPem(certPem);
        requestJson.setBash64enc(true);
        requestJson.setRegCode(digiSignService.getRegCode());
        return requestJson;
    }

    public SignatureDigest prepareDocumentDigest(List<NodeRef> nodeRefs, String certHex, String role) throws SignatureException {
        log.debug("prepareDocumentDigest()...");
        try {
            RequestFiles files = new RequestFiles();

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            addFilesAsMultiPartFiles("files", body, nodeRefs, files, "");
            body.add("digest", getIDCardDigestRequestWithAttr(certHex, setSignAttrValues("", "", "", "", role), files));
            //IdCardDigestResponse response = getIdCardDigest(body);
            IdCardDigestResponse response = Oauth2Client.getMultipartPostRequestWithToken(body,digiSignService.getUri() + IDCARD_DIGEST_URI, IdCardDigestResponse.class, getToken());

            return new SignatureDigest(response.getDigest(), certHex, new Date(), null, response.getUid(), digiSignService.getRegCode());
        } catch (Exception e) {
            throw new SignatureException("Failed to create ID-CARD digest request: ERROR MESSAGE: " + e.getMessage(), e);
        }
    }

    private IdCardDigestResponse getIdCardDigest(MultiValueMap<String, Object> body) throws AuthenticationException, SignatureException {
        log.debug("getIdCardDigest()...");
        String serverUrl = digiSignService.getUri() + IDCARD_DIGEST_URI;
        log.debug("SERVICE URL: " + serverUrl);

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(), getToken()));

        String msg = "";
        try {
            ResponseEntity<IdCardDigestResponse> response = new RestTemplate().postForEntity(serverUrl, requestEntity, IdCardDigestResponse.class);
            if (response != null) {
                log.debug("REQEUST STATUS CODE: " + response.getStatusCode());
                if (response.hasBody()) {
                    log.debug("BODY class: " + response.getBody().getClass());
                    return response.getBody();
                }
            }

        } catch (Exception e) {
            msg = e.getMessage();
            log.error(e.getMessage(), e);
        }
        throw new SignatureException("NO RESPONSE BODY FROM Digisign service! " + msg);
    }

    public void UpdateExistingContainerWithSignature(NodeRef nodeRef, String signatureHex, String requestUid) throws SignatureRuntimeException {
        try {
            log.debug("UpdateExistingContainerWithSignature()...");
            IdCardSignRequest request = new IdCardSignRequest();
            request.setDigest(signatureHex);
            request.setRegCode(digiSignService.getRegCode());
            request.setUid(requestUid);

            //InputStream is = signStreamWithIdCard(request);
            InputStream is = new ByteArrayInputStream(signWithIdCard(request));
            log.debug("WriteSignedContainer...");
            getDigiDoc4JSignatureService().writeSignedContainer(nodeRef, is);
        } catch (Exception e) {
            throw new SignatureRuntimeException("Failed to add signature and write request UID = " + requestUid + ", signatureHex = " + signatureHex + ", ERROR: " + e.getMessage(), e);
        }
    }

    public NodeRef createAndSignContainer(NodeRef parentRef, String filename, String signatureHex, String requestUid) throws SignatureRuntimeException {
        try {
            log.debug("createAndSignContainer()...");
            IdCardSignRequest request = new IdCardSignRequest();
            request.setDigest(signatureHex);
            request.setRegCode(digiSignService.getRegCode());
            request.setUid(requestUid);

            InputStream is = new ByteArrayInputStream(signWithIdCard(request));
            log.debug("Create content node... parentRef: " + parentRef + "; filename: " + filename);
            NodeRef newNodeRef = getDigiDoc4JSignatureService().createContentNode(parentRef, filename);
            log.debug("New nodeRef: " + newNodeRef);

            log.debug("WriteSignedContainer...");
            getDigiDoc4JSignatureService().writeSignedContainer(newNodeRef, is);
            return newNodeRef;
        } catch (Exception e) {
            throw new SignatureRuntimeException("Failed to add signature and write file " + filename + ", signatureHex = " + signatureHex + ", ERROR: " + e.getMessage(), e);
        }
    }

    private byte[] signWithIdCard(IdCardSignRequest requestJson) throws AuthenticationException, SignatureException {
        log.debug("signWithIdCard()...");
        String serverUrl = digiSignService.getUri() + IDCARD_SIGN_URI;
        log.debug("SERVICE URL: " + serverUrl);

        log.debug("REQUEST: UID: " + requestJson.getUid());
        log.debug("REQUEST: DIGEST: " + requestJson.getDigest());
        log.debug("REQUEST: REG CODE: " + requestJson.getRegCode());

        HttpEntity<IdCardSignRequest> requestEntity
                = new HttpEntity<>(requestJson, Oauth2Client.getHeadersWithToken(Oauth2Client.getHeaders(), getToken()));
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<byte[]> response = restTemplate.postForEntity(serverUrl, requestEntity, byte[].class);
        log.debug("RESPOSE: STATUS CODE: " + response.getStatusCode());

        HttpHeaders headers = response.getHeaders();
        boolean signStatus = false;
        if (headers.containsKey("Response-Status-Code")) {
            log.debug("HEADERS: Response-Status-Code: " + headers.get("Response-Status-Code"));
            log.debug("HEADERS: Response-Status-Text: " + headers.get("Response-Status-Text"));
            List<String> codes = headers.get("Response-Status-Code");
            log.debug("CODES: size: " + codes.size());
            for (String code : codes) {
                if (code.equals("201")) {
                    log.debug("CODE == 201");
                    signStatus = true;
                    break;
                } else {
                    log.debug("NOT CORRECT CODE: " + code);
                }
            }
        } else {
            log.debug("HEADERS: Response-Status-Text: " + "FIELD NOT FOUND!");
        }

        if (response.hasBody()) {
            log.debug("BODY size: " + response.getBody().length);
        }

        if (signStatus) {
            log.debug("SIGN STATUS is TRUE!");
            return response.getBody();
        } else {
            log.debug("SIGN STATUS is FALSE!");
            throw new SignatureException(headers.get("Response-Status-Text").toString());
        }
    }


    private InputStream signStreamWithIdCard(IdCardSignRequest requestJson) throws AuthenticationException, SignatureException {
        log.debug("signStreamWithIdCard()...");
        String serverUrl = digiSignService.getUri() + IDCARD_SIGN_STREAM_URI;
        log.debug("SERVICE URL: " + serverUrl);

        log.debug("REQUEST: UID: " + requestJson.getUid());
        log.debug("REQUEST: DIGEST: " + requestJson.getDigest());
        log.debug("REQUEST: REG CODE: " + requestJson.getRegCode());

        HttpEntity<IdCardSignRequest> requestEntity
                = new HttpEntity<>(requestJson, Oauth2Client.getHeadersWithToken(Oauth2Client.getHeaders(), getToken()));
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<InputStream> response = restTemplate.postForEntity(serverUrl, requestEntity, InputStream.class);
        log.debug("RESPONSE STATUSCODE: " + response.getStatusCode());
        HttpHeaders headers = response.getHeaders();
        boolean signStatus = false;
        if (headers.containsKey("Response-Status-Code")) {
            log.debug("HEADERS: Response-Status-Code: " + headers.get("Response-Status-Code"));
            log.debug("HEADERS: Response-Status-Text: " + headers.get("Response-Status-Text"));
            List<String> codes = headers.get("Response-Status-Code");
            log.debug("CODES: size: " + codes.size());
            for (String code : codes) {
                if (code.equals("201")) {
                    log.debug("CODE == 201");
                    signStatus = true;
                } else {
                    log.debug("NOT CORRECT CODE: " + code);
                }
            }
        } else {
            log.debug("HEADERS: Response-Status-Text: " + "FIELD NOT FOUND!");
        }

        if (signStatus) {
            log.debug("SIGN STATUS is TRUE!");
            return response.getBody();
        } else {
            log.debug("SIGN STATUS is FALSE!");
            throw new SignatureException(headers.get("Response-Status-Text").toString());
        }
    }

    private MIDSignResponse prepareSignWithMid(MultiValueMap<String, Object> body) throws AuthenticationException {
        String serverUrl = digiSignService.getUri() + MID_SIGN_URI;

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(), getToken()));

        ResponseEntity<MIDSignResponse> response = new RestTemplate().postForEntity(serverUrl, requestEntity, MIDSignResponse.class);
        return response.getBody();
    }

    private Object makePostReqest(MultiValueMap<String, Object> body, String serverUrl, Class responseClass) throws AuthenticationException {

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(), getToken()));

        ResponseEntity<Object> response = new RestTemplate().postForEntity(serverUrl, requestEntity, responseClass);
        return response;
    }

    /**
     * @param requestJson
     * @return
     * @throws AuthenticationException
     */
    @Override
    public MIDSignStatusResponse getMidSignStatus(MIDContainerRequest requestJson) throws UnableToPerformException {
        String serverUrl = digiSignService.getUri() + MID_STATUS_URI;
        try {
            HttpEntity<MIDContainerRequest> requestEntity = new HttpEntity<>(requestJson, Oauth2Client.getHeadersWithToken(Oauth2Client.getHeaders(), getToken()));
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<MIDSignStatusResponse> response = restTemplate.postForEntity(serverUrl, requestEntity, MIDSignStatusResponse.class);
            return response.getBody();
        } catch (AuthenticationException e) {
            throw new UnableToPerformException(e.getMessage());
        }
    }

    public MIDSignStatusResponse getMidSignStatus(SignatureChallenge signatureChallenge) throws UnableToPerformException {
        MIDContainerRequest request = new MIDContainerRequest();
        request.setRegCode(digiSignService.getRegCode());
        request.setUid(signatureChallenge.getChallengeId());
        return getMidSignStatus(request);
    }

    /**
     * @param requestJson
     * @return
     * @throws AuthenticationException
     */
    @Override
    public byte[] getMidSignedContainer(MIDContainerRequest requestJson) throws AuthenticationException, SignatureException {
        log.debug("getMidSignedContainer()...");
        String serverUrl = digiSignService.getUri() + MID_CONTAINTER_URI;
        log.debug("SERVICE URL: " + serverUrl);

        HttpEntity<MIDContainerRequest> requestEntity
                = new HttpEntity<>(requestJson, Oauth2Client.getHeadersWithToken(Oauth2Client.getHeaders(), getToken()));
        RestTemplate restTemplate = new RestTemplate();
        log.debug("Make container request...");
        ResponseEntity<byte[]> response = restTemplate.postForEntity(serverUrl, requestEntity, byte[].class);
        String msg = "";
        HttpHeaders headers = response.getHeaders();
        boolean signStatus = false;
        if (headers.containsKey("Response-Status-Code")) {
            log.debug("HEADERS: Response-Status-Code: " + headers.get("Response-Status-Code"));
            log.debug("HEADERS: Response-Status-Text: " + headers.get("Response-Status-Text"));
            List<String> codes = headers.get("Response-Status-Code");
            log.debug("CODES: size: " + codes.size());
            for (String code : codes) {
                if (code.equals("201")) {
                    log.debug("CODE == 201");
                    signStatus = true;
                } else {
                    log.debug("NOT CORRECT CODE: " + code);
                }
            }
        } else {
            log.debug("HEADERS: Response-Status-Text: " + "FIELD NOT FOUND!");
        }

        if (signStatus) {
            log.debug("SIGN STATUS is TRUE!");
            return response.getBody();
        } else {
            log.debug("SIGN STATUS is FALSE!");
            throw new SignatureException(headers.get("Response-Status-Text").toString());
        }
    }


    public void updateMidSignedContainer(String requestUid, String regCode, NodeRef nodeRef) throws UnableToPerformException, SignatureException, IOException {
        MIDContainerRequest requestJson = new MIDContainerRequest();
        requestJson.setUid(requestUid);
        requestJson.setRegCode(regCode);

        try {
            byte[] data = getMidSignedContainer(requestJson);
            if (data != null) {
                getDigiDoc4JSignatureService().writeSignedContainer(nodeRef, new ByteArrayInputStream(data));
            } else {
                throw new SignatureException("Signed container is missing!");
            }
        } catch (AuthenticationException e) {
            throw new UnableToPerformException(e.getMessage());
        } catch (SignatureException e) {
            throw new SignatureException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public NodeRef createNewMidSignedContainer(String requestUid, String regCode, NodeRef document, String filename) throws UnableToPerformException, SignatureException, IOException {
        log.debug("createNewMidSignedContainer()...");
        log.debug("REQUEST UID: " + requestUid);
        log.debug("REG CODE: " + regCode);
        log.debug("DOCUMENT NodeRef: " + document);
        log.debug("FILENAME: " + filename);
        MIDContainerRequest requestJson = new MIDContainerRequest();
        requestJson.setUid(requestUid);
        requestJson.setRegCode(regCode);

        try {
            log.debug("Get MID signed container...");
            byte[] data = getMidSignedContainer(requestJson);
            log.debug("CHECK DATA....");
            if (data != null) {
                log.debug("DATA file length: " + data.length);
                log.debug("Create content node...");
                NodeRef newNodeRef = getDigiDoc4JSignatureService().createContentNode(document, filename);
                log.debug("New nodeRef: " + newNodeRef);
                log.debug("WriteSignedContainer...");
                getDigiDoc4JSignatureService().writeSignedContainer(newNodeRef, new ByteArrayInputStream(data));

                return newNodeRef;
            } else {
                log.debug("DATA is NULL! Signed container is missing!");
                throw new SignatureException("Signed container is missing!");
            }
        } catch (AuthenticationException e) {
            throw new UnableToPerformException(e.getMessage());
        } catch (SignatureException e) {
            throw new SignatureException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    //@Override
    public SignatureChallenge getSignatureChallenge(List<NodeRef> nodeRefs, String phoneNo, String idCode, String role) throws SignatureException, UnableToPerformException {
        log.debug("getSignatureChallenge(): Phone nr: " + phoneNo + "; id-code: " + idCode + "; nodeRefs: size:" + nodeRefs.size());
        try {

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

            RequestFiles files = new RequestFiles();

            addFilesAsMultiPartFiles("files", body, nodeRefs, files, "");
            body.add("sign", getMIDSignRequestWithAttr(phoneNo, idCode, setSignAttrValues("", "", "", "", role), files));

            MIDSignResponse response = prepareSignWithMid(body);

            SignatureChallenge signatureChallenge = new SignatureChallenge(response.getUid(), response.getCode(), null, response.getUid(), digiSignService.getRegCode());
            log.debug("SignatureChallenge: " + signatureChallenge);
            return signatureChallenge;
        } catch (UnableToPerformException e) {
            throw e;
        } catch (Exception e) {
            throw new SignatureException("Failed to create Mobile-ID signing request: phoneNo = " + phoneNo + "; ERROR MESSAGE: " + e.getMessage(), e);
        }
    }

    //, String role, String city, String country, String postalCode, String state
    private MIDSignRequestWithAttr getMIDSignRequestWithAttr(String phoneNo, String idCode, SignAttr signAttr, RequestFiles files) {
        MIDSignRequestWithAttr requestJson = new MIDSignRequestWithAttr();

        requestJson.setIdCode(idCode);
        requestJson.setPhoneNr(phoneNo);
        requestJson.setRegCode(digiSignService.getRegCode());
        requestJson.setServiceName(digiSignService.getServiceName());

        requestJson.setSignAttr(signAttr);
        requestJson.setFiles(files);

        return requestJson;
    }

    private AsicsSignRequest getAsicsSignRequest(RequestFiles files){
        AsicsSignRequest request = new AsicsSignRequest();
        request.setFiles(files);
        request.setRegCode(digiSignService.getRegCode());
        return request;
    }

    @Override
    public String getMobileIdSignature(SignatureChallenge signatureChallenge) throws UnableToPerformException {
        log.debug("getMobileIdSignature()...");
        MIDContainerRequest request = new MIDContainerRequest();
        request.setRegCode(digiSignService.getRegCode());
        log.debug("getMobileIdSignature()... REG CODE: " + request.getRegCode());
        request.setUid(signatureChallenge.getRequestUid());
        log.debug("getMobileIdSignature()... REQUEST UID: " + request.getUid());
        log.debug("getMobileIdSignature()... CHALLENGE ID: " + signatureChallenge.getChallengeId());
        log.debug("getMobileIdSignature()... SESS CODE: " + signatureChallenge.getSesscode());

        MIDSignStatusResponse response = getMidSignStatus(request);
        if (response != null) {
            log.debug("RESPONSE STATUS CODE: " + response.getStatusCode());

            //TODO: Here update statuscode handling for errors

            if (response.getStatusCode() == 400) {
                return "SIGNED";
            }
        }
        log.debug("RESPONSE IS MISSING! Return NULL!");
        return null;
    }

    public Oauth2TokenInfo getToken() throws AuthenticationException {
        //return Oauth2Client.sendTokenRequestAsBasicAuth(digiSignService.getUri(), digiSignService.getClientId(), digiSignService.getClientSecret(), digiSignService.getAppname(), digiSignService.getApppass());
        return tokenStore.getToken(digiSignService.getUri(), digiSignService.getClientId(), digiSignService.getClientSecret(), digiSignService.getAppname(), digiSignService.getApppass());
    }

    public byte[] getContent(NodeRef nodeRef) {
        try {
            InputStream contentInputStream = getContentInputStream(nodeRef);
            if (contentInputStream != null) {
                byte[] filedata = IOUtils.toByteArray(contentInputStream);
                IOUtils.closeQuietly(contentInputStream);
                return filedata;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public InputStream getContentInputStream(NodeRef nodeRef) {
        try {

            String rootLocation = getDefaultRootDir();
            ContentReader reader = BeanHelper.getFileFolderService().getReader(nodeRef);
            if (reader == null) {
                return null;
            }
            InputStream contentInputStream = reader.getContentInputStream();
            return contentInputStream;

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public SignatureItemsAndDataItems getDataItemsAndSignatureItems(InputStream is, boolean includeData, String filext) throws SignatureException {
        log.debug("getDataItemsAndSignatureItems()... Inputstream...");
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        try {

            DigidocParseRequest requestJson = new DigidocParseRequest();
            requestJson.setRegCode(digiSignService.getRegCode());
            requestJson.setShortFormat(true);
            body.add("params", requestJson);

            addFilesAsMultiPartFiles("file", body, IOUtils.toByteArray(is), "digidoc." + filext);
            return getDataAndSignature(body, includeData, null);
        } catch (IOException e) {
            throw new SignatureException("Digidoc file parse failed! ERROR: " + e.getMessage(), e);
        }
    }

    public SignatureItemsAndDataItems getDataItemsAndSignatureItems(NodeRef nodeRef, boolean includeData) throws SignatureException {
        log.debug("getDataItemsAndSignatureItems()... NodeRef...");
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        DigidocParseRequest requestJson = new DigidocParseRequest();
        requestJson.setRegCode(digiSignService.getRegCode());
        requestJson.setShortFormat(true);
        body.add("params", requestJson);

        RequestFiles files = new RequestFiles();

        addFilesAsMultiPartFiles("file", body, new ArrayList<>(Arrays.asList(nodeRef)), files, "");
        log.debug("Get data and signatures from DIGISIGN-SIGN service...");
        return getDataAndSignature(body, includeData, nodeRef);
    }

    @Override
    public SignatureItemsAndDataItems getDataItemsAndSignatureItems(InputStream inputStream, boolean includeData) throws SignatureException {
        log.debug("getDataItemsAndSignatureItems(): inputStream... includeData" + includeData);
        return getDataItemsAndSignatureItems(inputStream, includeData, null);
    }

    private SignatureItemsAndDataItems getDataAndSignature(MultiValueMap<String, Object> body, boolean includeData, NodeRef nodeRef) throws SignatureException {
        log.debug("getDataAndSignature()...");
        try {
            List<DataItem> dataItems = new ArrayList<>();
            List<SignatureItem> signatureItems = new ArrayList<>();

            DigiDocParseEntity entity = parseDigidocFile(body);

            log.debug("Digidoc file(s) parsed...");
            if (entity != null) {
                int orederId = 0;

                log.debug("DATAFILES: Found: " + (entity.getDataFiles() != null ? entity.getDataFiles().size() : "NULL") + " FILES!");

                if(entity.getDataFiles() != null && entity.getDataFiles().size() > 0){
                    for (DigidocDataEntity dataEntity : entity.getDataFiles()) {
                        String mimeType = dataEntity.getMediaType();
                        String guessedMimetype = getMimetypeService().guessMimetype(mimeType);
                        if (MimetypeMap.MIMETYPE_BINARY.equals(guessedMimetype) && org.apache.commons.lang.StringUtils.isNotBlank(mimeType)) {
                            guessedMimetype = mimeType;
                        }
                        log.debug("Data item: orderId: " + orederId + "; nodeRef: " + nodeRef + "; id: " + dataEntity.getId() + "; filename: " + dataEntity.getFilename() + "; mime: " + guessedMimetype + "; size: " + dataEntity.getFilesize() + "; data length: " + dataEntity.getFiledata().length);
                        if (includeData) {
                            dataItems.add(new DataItem(nodeRef, dataEntity.getId(), dataEntity.getFilename(), guessedMimetype, null, dataEntity.getFilesize(), null, orederId, dataEntity.getFiledata()));
                        } else
                            dataItems.add(new DataItem(nodeRef, dataEntity.getId(), dataEntity.getFilename(), guessedMimetype, dataEntity.getFilesize(), orederId));

                        orederId++;
                    }
                } else {
                    log.warn("NO DATA FILES INCLUDED IN DIGIDOC CONTAINER!!!");
                }

                log.debug("SIGNATURES: Found: " + (entity.getSignatures() != null ? entity.getSignatures().size() : "0") + " SIGNATURE!");

                for (DigidocSignatureEntity signatureEntity : entity.getSignatures()) {
                    X509CertInfo certValue = signatureEntity.getSigningCertificate();

                    String subjectDNName = certValue.getX509Certificate().getSubjectDNName();
                    X509CertPrincipal x509CertPrincipal = getPrincipal(subjectDNName);
                    //String subjectName = certValue.getSubjectName();


                    String cn = certValue.getX509Certificate().getSubjectDNCommonName();
                    log.debug("X509Cert subject (CN) common name: " + cn);

                    String subjectFirstName = x509CertPrincipal.getGIVENNAME();
                    log.debug("X509Cert subject firstname: " + subjectFirstName);

                    String subjectLastName = x509CertPrincipal.getSURNAME();
                    log.debug("X509Cert subject lastname: " + subjectLastName);

                    String legalCode = x509CertPrincipal.getSERIALNUMBER();
                    if (legalCode.startsWith("PNOEE-")) {
                        log.debug("X509Cert subject (NEW: ESTEID 2018) serialnumber: " + legalCode);
                        String[] idParts = legalCode.split("-");
                        legalCode = idParts[1];
                        log.debug("X509Cert subject (NEW: ESTEID 2018) serialnumber: AFTER: " + legalCode);
                    } else {
                        log.debug("X509Cert subject (OLD STYLE) serialnumber: " + legalCode);
                    }

                    String name = UserUtil.getPersonFullName(subjectFirstName, subjectLastName);

                    // If certificate is not for person then it is for e-stamping and we use Common Name as a name.
                    if ((subjectLastName == null || subjectLastName.isEmpty()) && (subjectFirstName == null || subjectFirstName.isEmpty())) {
                        log.debug("USE NAME value from CN: ");
                        name = x509CertPrincipal.getCN();
                    }

                    Date signingTime = signatureEntity.getTrustedSigningTime();

                    String address = getSignatureAddress(signatureEntity.getCity(), signatureEntity.getPostalCode(), signatureEntity.getCountryName());
                    log.debug("Signature data...");

                    log.debug("name: " + name);
                    log.debug("legalCode: " + legalCode);
                    log.debug("signingTime: " + signingTime);
                    log.debug("Signer ROLES....");
                    if(signatureEntity.getSignerRoles() != null){
                        for(String role: signatureEntity.getSignerRoles()){
                            log.debug("ROLE: " + role);
                        }

                    }
                    log.debug("address: " + address);
                    Boolean valid = signatureEntity.isValid();
                    log.trace("-- Signature element isValid - START ------------------------------------------------------------------------------");
                    if(valid == null){
                        log.warn("Signature element VALID status is NULL!!! use container valid status.....");
                        if(entity.isContainerValid() == null){
                            log.warn("Container element VALID status is NULL!!! use value: FALSE.....");
                            valid = false;
                        } else {
                            valid = entity.isContainerValid();
                        }
                    }
                    log.debug("signature isValid: " + valid);
                    log.trace("-- Signature element isValid - END ------------------------------------------------------------------------------");

                    log.debug("getCertEncType(certValue): " + getCertEncType(certValue));
                    log.debug("signType: " + getSignType(x509CertPrincipal));

                    log.debug("Add signature data to Signature Items list");

                    SignatureItem signatureItem = new SignatureItem(name, legalCode, signingTime, signatureEntity.getSignerRoles(), address, valid, getCertEncType(certValue), getSignType(x509CertPrincipal));

                    signatureItems.add(signatureItem);
                    log.debug("signatureItems list size: " + signatureItems.size());
                }
            } else {
                log.warn("No Signature entity to parse!");
            }

            // If timestamp exists, then add to list.
            List<TimeStampTokenShort> tokenInfos = new ArrayList<>();
            if(entity != null && entity.getTimeStampToken() != null){
                TimeStampTokenShort stampTokenShort = new TimeStampTokenShort();

                if(entity.getTimeStampToken().getTimeStampInfo() != null){
                    stampTokenShort.setGenTime(entity.getTimeStampToken().getTimeStampInfo().getGenTime());

                    if(entity.getTimeStampToken().getTimeStampInfo().getTsaName() != null)
                        stampTokenShort.setTsaNameCommonName(entity.getTimeStampToken().getTimeStampInfo().getTsaName().getCommonName());
                }

                tokenInfos.add(stampTokenShort);
            }

            log.debug("Return " + signatureItems.size() + " signature, " + dataItems.size() + " dataitems and " + tokenInfos.size() + " timestamps...");

            return new SignatureItemsAndDataItems(signatureItems, dataItems, tokenInfos);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new SignatureException("Digidoc file parse failed! ERROR: " + e.getMessage(), e);
        }
    }

    private String getSignType(X509CertPrincipal x509CertPrincipal){
        String signType = null;
        String signOU = x509CertPrincipal.getOU();
        String signO = x509CertPrincipal.getO();
        log.debug("OU: " + signOU + "; O: " + signO);
        if(signOU != null){
            log.debug("OU is " + signOU);
            if(signOU.toLowerCase().equals(SIGN_OU_TYPE_CORP_SIGNATURE)) {
                signType = signOU;
            }
            if(signOU.toLowerCase().equals(SIGN_OU_TYPE_DIGITAL_SIGNATURE)) {
                signType = signO;
            }
            log.debug("SIGN TYPE is: " + signType);
        }

        return signType;
    }

    private String getCertEncType(X509CertInfo certValue) {
        try{
            String sigAlgName = certValue.getX509Certificate().getSigAlgName();
            String encryptionType = "";
            if (sigAlgName.toLowerCase().equals("sha1withrsa")) {
                encryptionType = "SHA-1";
            } else if (sigAlgName.toLowerCase().equals("sha256withrsa")) {
                encryptionType = "SHA-256";
            } else {
                encryptionType = sigAlgName;
            }
            return encryptionType;
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return "UNKNOWN!";
    }

    private String getSignatureAddress(String city, String postalCode, String countryName) {
        StringBuilder address = new StringBuilder();
        if (StringUtils.isNotBlank(city)) {
            address.append(city);
        }
        if (StringUtils.isNotBlank(postalCode)) {
            if (address.length() > 0) {
                address.append(",");
            }
            address.append(postalCode);
        }
        if (StringUtils.isNotBlank(countryName)) {
            if (address.length() > 0) {
                address.append(",");
            }
            address.append(countryName);
        }
        if (address.length() > 0) {
            return address.toString();
        }

        return "";
    }

    private X509CertPrincipal getPrincipal(String DN) {
        // DN: C=EE,O=ESTEID (MOBIIL-ID),OU=digital signature,CN=PUUSEPP\,AARE\,37503294229,SURNAME=PUUSEPP,GIVENNAME=AARE,SERIALNUMBER=37503294229
        log.debug("DN: " + DN);
        try {
            String C = null;
            String CN = null;
            String SURNAME = null;
            String SERIALNUMBER = null;
            String GIVENNAME = null;
            String O = null;
            String OU = null;

            String[] split = DN.split(",");
            for (String x : split) {
                log.debug("DN part: [" + x + "]");
                if (x.contains("C=")) {
                    log.debug("DN contains " + x);
                    C = x.split("=")[1].trim();
                    log.debug("C value: " + C);
                }
                if (x.contains("CN=")) {
                    log.debug("DN contains " + x);
                    CN = x.split("=")[1].trim();
                    log.debug("CN value: " + CN);
                }
                if (x.contains("O=")) {
                    log.debug("DN contains " + x);
                    O = x.split("=")[1].trim();
                    log.debug("O value: " + O);
                }
                if (x.contains("OU=")) {
                    log.debug("DN contains " + x);
                    OU = x.split("=")[1].trim();
                    log.debug("OU value: " + OU);
                }

                if (x.contains("SURNAME=")) {
                    log.debug("DN contains " + x);
                    SURNAME = x.split("=")[1].trim();
                    log.debug("SURNAME value: " + SURNAME);
                }
                if (x.contains("SERIALNUMBER=")) {
                    log.debug("DN contains " + x);
                    SERIALNUMBER = x.split("=")[1].trim();
                    log.debug("SERIALNUMBER value: " + SERIALNUMBER);
                }
                if (x.contains("GIVENNAME=")) {
                    log.debug("DN contains " + x);
                    GIVENNAME = x.split("=")[1].trim();
                    log.debug("GIVENNAME value: " + GIVENNAME);
                }
            }
            return new X509CertPrincipal(C, CN, O, OU, SURNAME, GIVENNAME, SERIALNUMBER);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * @param body
     * @return
     * @throws AuthenticationException
     */
    private DigiDocParseEntity parseDigidocFile(MultiValueMap<String, Object> body) throws AuthenticationException {
        log.debug("parseDigidocFile()...");
        String serviceUrl = digiSignService.getUri() + API_DIGIDOC_PARSE_V4;
        log.debug("SERVICE URL: " + serviceUrl);
        Oauth2TokenInfo token = getToken();
        if(!RestUtil.checkUrlExists(serviceUrl, token)){
            log.warn("SERVICE URL is not ACTIVE: " + serviceUrl);
            return null;
        }

        log.debug("SERVICE URL FOUND! " + serviceUrl);

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(), token));

        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<DigiDocParseEntity> response = restTemplate.postForEntity(serviceUrl, requestEntity, DigiDocParseEntity.class);
            if (response != null) {
                log.debug("RESPONSE STATUS CODE: " + response.getStatusCode());
                return response.getBody();
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * @param base64FileData
     * @return
     * @throws JSONException
     */
    public JSONObject checkDigidocCrypto(String base64FileData) throws JSONException {
        String serviceUrl = digiSignService.getUri() + API_ASICS_CHECK_CRYPTO;

        JSONObject request = new JSONObject();
        request.put("file", base64FileData);

        return makePostRequest(request, serviceUrl);

    }

    /**
     * @param files
     * @return
     * @throws JSONException
     */
    public JSONObject checkDigidocCryptoAndTimestamp(List<JSONObject> files) throws JSONException {
        String serviceUrl = digiSignService.getUri() + API_ASICS_CHECK_CRYPTO_AND_MAKE;

        JSONObject request = new JSONObject();
        request.put("files", files);

        return makePostRequest(request, serviceUrl);

    }

    public byte[] makeAsicS(File file) throws IOException, AuthenticationException{
        log.debug("makeAsicS()...");
        return makeAsicS(file.getName(), FileUtils.readFileToByteArray(file));
    }

    public byte[] makeAsicS(String filename, byte[] fileBytes) throws AuthenticationException {
        String serviceUrl = digiSignService.getUri() + API_ASICS_SIGN;
        log.info("REST service url: " + serviceUrl + " - upload filesize: " + (fileBytes != null ? fileBytes.length : 0) + " bytes, filename: " + filename);
        return createAsicsRequestByFilePath(filename, fileBytes, serviceUrl);
    }

    private byte[] createAsicsRequestByFilePath(String filename, byte[] fileData, String serviceUrl) throws AuthenticationException {
        log.info("Create ASICS request... filename: " + filename + ", filesize: " + (fileData == null ? "NULL" : fileData.length));
        RequestFiles files = new RequestFiles();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        addFileAsMultiPartFile("file", body, 1, filename, fileData, files);
        body.add("sign", getAsicsSignRequest(files));
        log.debug("Request entity....");
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getHeaders(), getToken()));
        log.debug("Make container request...");
        final ResponseEntity<byte[]> response = new RestTemplate().postForEntity(serviceUrl, requestEntity, byte[].class);
        if(log.isDebugEnabled()){
            logReponse(response);
        }
        if (response.getStatusCode().equals(HttpStatus.CREATED)) {
            log.debug("Status CODE is " + response.getStatusCode());
            if (response.hasBody()) {
                log.debug("Return response body..");
                return response.getBody();
            }
            log.warn("Response has no body. Return NULL!");
        }
        return null;
    }

    private void logReponse(ResponseEntity<byte[]> response) {
        try {
            log.debug("Response: statuscode: " + response.getStatusCode());
            log.debug("Response: hasbody: " + response.hasBody());
            log.debug("Response: headers: contentType" + response.getHeaders().getContentType());

        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private String getDefaultRootDir() {
        return defaultRootDir;
    }

    public void setDefaultRootDir(String defaultRootDir) {
        this.defaultRootDir = defaultRootDir;
    }

    private Date stringToDate(String dateValue) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return parseStringToDate(dateValue, format);
    }

    public void setDigiSignService(DigiSignServiceImpl digiSignService) {
        this.digiSignService = digiSignService;
    }

    public void setTokenStore(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    private class BinaryFileExtractor implements ResponseExtractor<byte[]> {
        @Override
        public byte[] extractData(ClientHttpResponse response) throws IOException {
            return ByteStreams.toByteArray(response.getBody());
        }
    }
}