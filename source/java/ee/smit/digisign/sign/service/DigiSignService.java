package ee.smit.digisign.sign.service;

public interface DigiSignService {
    String BEAN_NAME = "DigiSignService";

    String getUri();

    String getAppname();

    String getApppass();

    String getClientId();

    String getClientSecret();

    boolean isActive();

    int getMaxThreads();

    int getBatchSize();

    String getRegCode();

    String getServiceName();

    boolean isIdCardEnabled();

    boolean isMobileIdEnabled();

    boolean isTimestamptingEnabled();

    boolean isSmartIdEnabled();

    boolean isFirmSignCertEnabled();

}
