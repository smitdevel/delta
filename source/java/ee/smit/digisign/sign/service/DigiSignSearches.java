package ee.smit.digisign.sign.service;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.digisign.sign.etamp.model.EStampSignRequestWithAttr;
import ee.smit.digisign.sign.mobileid.model.MIDContainerRequest;
import ee.smit.digisign.sign.mobileid.model.MIDSignStatusResponse;
import ee.webmedia.alfresco.signature.exception.SignatureException;
import ee.webmedia.alfresco.signature.exception.SignatureRuntimeException;
import ee.webmedia.alfresco.signature.model.SignatureChallenge;
import ee.webmedia.alfresco.signature.model.SignatureDigest;
import ee.webmedia.alfresco.signature.model.SignatureItemsAndDataItems;
import ee.webmedia.alfresco.utils.UnableToPerformException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.http.auth.AuthenticationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface DigiSignSearches {
    String BEAN_NAME = "DigiSignSearches";

    String DIGIDOC_FORMAT_BDOC_TM = "bdoc-tm";
    String DIGIDOC_FORMAT_BDOC_TS = "bdoc-ts";
    String DIGIDOC_FORMAT_ASICE = "asice";
    String BDOC_CONTAINER_TYPE = "BDOC";
    String DDOC_CONTAINER_TYPE = "DDOC";


    boolean isAsicsActive();

    Oauth2TokenInfo getToken() throws AuthenticationException;

    JSONObject checkDigidocCrypto(String base64FileData) throws JSONException;

    JSONObject checkDigidocCryptoAndTimestamp(List<JSONObject> files) throws JSONException;

    byte[] makeAsicS(File file) throws IOException, AuthenticationException;

    byte[] makeAsicS(String filename, byte[] fileBytes) throws AuthenticationException;

    byte[] getContent(NodeRef nodeRef);

    InputStream getContentInputStream(NodeRef nodeRef);

    SignatureItemsAndDataItems getDataItemsAndSignatureItems(InputStream is, boolean includeData, String filext) throws SignatureException;

    SignatureItemsAndDataItems getDataItemsAndSignatureItems(NodeRef nodeRef, boolean includeData) throws SignatureException;

    SignatureItemsAndDataItems getDataItemsAndSignatureItems(InputStream inputStream, boolean includeData) throws SignatureException;
    // E-STAMP sign
    byte[] signWithEStamp(MultipartFile[] files, EStampSignRequestWithAttr requestJson) throws AuthenticationException;

    // ID-card sign - PART 1
    SignatureDigest prepareDocumentDigest(List<NodeRef> nodeRefs, String certHex, String role) throws SignatureException;
    //IdCardDigestResponse getIdCardDigest(MultipartFile[] files, IdCardDigestRequestWithAttr requestJson) throws AuthenticationException;

    // ID-card sign - PART 2
    void UpdateExistingContainerWithSignature(NodeRef nodeRef, String signatureHex, String requestUid) throws SignatureRuntimeException;

    NodeRef createAndSignContainer(NodeRef parentRef, String filename, String signatureHex, String requestUid) throws SignatureRuntimeException;
    //byte[] signWithIdCard(IdCardSignRequest requestJson) throws AuthenticationException, URISyntaxException;

    // MID sign - PART 1
    //MIDSignResponse prepareSignWithMid(MultipartFile[] files, MIDSignRequestWithAttr requestJson) throws AuthenticationException;

    // MID sign - PART 1
    SignatureChallenge getSignatureChallenge(List<NodeRef> nodeRefs, String phoneNo, String idCode, String role) throws SignatureException, UnableToPerformException;

    // MID sign - PART 2 (optional)
    MIDSignStatusResponse getMidSignStatus(MIDContainerRequest requestJson) throws UnableToPerformException;

    MIDSignStatusResponse getMidSignStatus(SignatureChallenge signatureChallenge) throws UnableToPerformException;

    String getMobileIdSignature(SignatureChallenge signatureChallenge) throws UnableToPerformException;

    // MID sign - PART 3
    byte[] getMidSignedContainer(MIDContainerRequest requestJson) throws AuthenticationException, SignatureException;

    void updateMidSignedContainer(String requestUid, String regCode, NodeRef nodeRef) throws UnableToPerformException, SignatureException, IOException;

    NodeRef createNewMidSignedContainer(String requestUid, String regCode, NodeRef document, String filename) throws UnableToPerformException, SignatureException, IOException;
}
