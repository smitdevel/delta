package ee.smit.digisign.sign.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DigiSignServiceImpl implements DigiSignService {
    protected final Log log = LogFactory.getLog(getClass());

    private String appname;
    private String apppass;

    private String defaultUri;

    private String clientId;
    private String clientSecret;

    private boolean active;
    private int batchSize;

    private String regCode;
    private String serviceName;

    private boolean idCardEnabled;
    private boolean mobileIdEnabled;
    private boolean timestamptingEnabled;
    private boolean smartIdEnabled;
    private boolean firmSignCertEnabled;

    public int getMaxThreads() {
        return maxThreads;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    private int maxThreads = 0;

    // Getters, Setters ------------------------
    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppname() {
        return appname;
    }

    public void setApppass(String apppass) {
        this.apppass = apppass;
    }

    public String getApppass() {
        return apppass;
    }

    public void setDefaultUri(String defaultUri) {
        this.defaultUri = defaultUri;
    }

    public String getUri() {
        return defaultUri;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getRegCode() {
        return regCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public boolean isIdCardEnabled() {
        return idCardEnabled;
    }

    public void setIdCardEnabled(boolean idCardEnabled) {
        this.idCardEnabled = idCardEnabled;
    }

    public boolean isMobileIdEnabled() {
        return mobileIdEnabled;
    }

    public void setMobileIdEnabled(boolean mobileIdEnabled) {
        this.mobileIdEnabled = mobileIdEnabled;
    }

    public boolean isTimestamptingEnabled() {
        return timestamptingEnabled;
    }

    public void setTimestamptingEnabled(boolean timestamptingEnabled) {
        this.timestamptingEnabled = timestamptingEnabled;
    }

    public boolean isSmartIdEnabled() {
        return smartIdEnabled;
    }

    public void setSmartIdEnabled(boolean smartIdEnabled) {
        this.smartIdEnabled = smartIdEnabled;
    }

    public boolean isFirmSignCertEnabled() {
        return firmSignCertEnabled;
    }

    public void setFirmSignCertEnabled(boolean firmSignCertEnabled) {
        this.firmSignCertEnabled = firmSignCertEnabled;
    }
}
