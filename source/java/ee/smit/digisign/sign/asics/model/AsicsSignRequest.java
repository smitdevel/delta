package ee.smit.digisign.sign.asics.model;

import ee.smit.digisign.sign.model.digidoc.file.RequestFiles;

public class AsicsSignRequest extends AsicsRequest {
    RequestFiles files;

    public RequestFiles getFiles() {
        return files;
    }

    public void setFiles(RequestFiles files) {
        this.files = files;
    }
}
