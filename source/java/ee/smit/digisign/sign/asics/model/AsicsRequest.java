package ee.smit.digisign.sign.asics.model;

public class AsicsRequest {
    String regCode;

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }
}
