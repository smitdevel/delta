package ee.smit.digisign.sign.asics.model;

import ee.smit.digisign.sign.model.digidoc.file.RequestFiles;
import ee.smit.digisign.sign.model.digidoc.requests.SignAttr;

public class AsicsSignRequestWithAttr extends AsicsRequest {
    SignAttr signAttr;
    RequestFiles files;

    public SignAttr getSignAttr() {
        return signAttr;
    }

    public void setSignAttr(SignAttr signAttr) {
        this.signAttr = signAttr;
    }

    public RequestFiles getFiles() {
        return files;
    }

    public void setFiles(RequestFiles files) {
        this.files = files;
    }
}
