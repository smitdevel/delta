package ee.smit.digisign.sign.mobileid.model;


import ee.webmedia.alfresco.signature.model.SignatureChallenge;

public class MIDChallengeResponse {
    SignatureChallenge challenge;
    String uid;
    String error;

    public SignatureChallenge getChallenge() {
        return challenge;
    }

    public void setChallenge(SignatureChallenge challenge) {
        this.challenge = challenge;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
