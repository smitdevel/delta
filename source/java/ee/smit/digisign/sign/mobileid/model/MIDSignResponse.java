package ee.smit.digisign.sign.mobileid.model;

import ee.smit.digisign.sign.model.digidoc.requests.DefaultResponse;

public class MIDSignResponse extends DefaultResponse {
    String code;
    String uid;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
