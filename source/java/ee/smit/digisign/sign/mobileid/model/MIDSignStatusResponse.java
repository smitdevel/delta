package ee.smit.digisign.sign.mobileid.model;

import ee.smit.digisign.sign.model.digidoc.requests.DefaultResponse;

public class MIDSignStatusResponse extends DefaultResponse {
    String proccesStatus;
    String signedUid;
    byte[] signedFileData;

    public String getProccesStatus() {
        return proccesStatus;
    }

    public void setProccesStatus(String proccesStatus) {
        this.proccesStatus = proccesStatus;
    }

    public String getSignedUid() {
        return signedUid;
    }

    public void setSignedUid(String signedUid) {
        this.signedUid = signedUid;
    }

    public byte[] getSignedFileData() {
        return signedFileData;
    }

    public void setSignedFileData(byte[] signedFileData) {
        this.signedFileData = signedFileData;
    }
}
