package ee.smit.digisign.sign.model.certificates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties
public class X509CertificateInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    int version;
    BigInteger serialNumber;

    Principal issuerDN;
    String issuerDNCommonName;
    String issuerDNName;
    X500Principal issuerX500Principal;

    Principal subjectDN;
    String subjectDNCommonName;
    String subjectDNName;
    X500Principal subjectX500Principal;

    Date notBefore;
    Date notAfter;

    //byte[] tbsCertificate;
    //byte[] signature;
    String sigAlgName;
    String sigAlgOID;
    //byte[] sigAlgParams;
    boolean[] issuerUniqueID;
    boolean[] subjectUniqueID;
    boolean[] keyUsage;
    List<String> extendedKeyUsage;
    int basicConstraints;
    Collection<List<?>> subjectAlternativeNames;
    Collection<List<?>> IssuerAlternativeNames;
    PublicKeyInfo publicKey;
    Set<String> criticalExtensionOIDs;
    Set<String> nonCriticalExtensionOIDs;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(BigInteger serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Principal getIssuerDN() {
        return issuerDN;
    }

    public void setIssuerDN(Principal issuerDN) {
        this.issuerDN = issuerDN;
    }

    public String getIssuerDNCommonName() {
        return issuerDNCommonName;
    }

    public void setIssuerDNCommonName(String issuerDNCommonName) {
        this.issuerDNCommonName = issuerDNCommonName;
    }

    public String getIssuerDNName() {
        return issuerDNName;
    }

    public void setIssuerDNName(String issuerDNName) {
        this.issuerDNName = issuerDNName;
    }

    public X500Principal getIssuerX500Principal() {
        return issuerX500Principal;
    }

    public void setIssuerX500Principal(X500Principal issuerX500Principal) {
        this.issuerX500Principal = issuerX500Principal;
    }

    public Principal getSubjectDN() {
        return subjectDN;
    }

    public void setSubjectDN(Principal subjectDN) {
        this.subjectDN = subjectDN;
    }

    public String getSubjectDNCommonName() {
        return subjectDNCommonName;
    }

    public void setSubjectDNCommonName(String subjectDNCommonName) {
        this.subjectDNCommonName = subjectDNCommonName;
    }

    public String getSubjectDNName() {
        return subjectDNName;
    }

    public void setSubjectDNName(String subjectDNName) {
        this.subjectDNName = subjectDNName;
    }

    public X500Principal getSubjectX500Principal() {
        return subjectX500Principal;
    }

    public void setSubjectX500Principal(X500Principal subjectX500Principal) {
        this.subjectX500Principal = subjectX500Principal;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = notBefore;
    }

    public Date getNotAfter() {
        return notAfter;
    }

    public void setNotAfter(Date notAfter) {
        this.notAfter = notAfter;
    }

    public String getSigAlgName() {
        return sigAlgName;
    }

    public void setSigAlgName(String sigAlgName) {
        this.sigAlgName = sigAlgName;
    }

    public String getSigAlgOID() {
        return sigAlgOID;
    }

    public void setSigAlgOID(String sigAlgOID) {
        this.sigAlgOID = sigAlgOID;
    }

    public boolean[] getIssuerUniqueID() {
        return issuerUniqueID;
    }

    public void setIssuerUniqueID(boolean[] issuerUniqueID) {
        this.issuerUniqueID = issuerUniqueID;
    }

    public boolean[] getSubjectUniqueID() {
        return subjectUniqueID;
    }

    public void setSubjectUniqueID(boolean[] subjectUniqueID) {
        this.subjectUniqueID = subjectUniqueID;
    }

    public boolean[] getKeyUsage() {
        return keyUsage;
    }

    public void setKeyUsage(boolean[] keyUsage) {
        this.keyUsage = keyUsage;
    }

    public List<String> getExtendedKeyUsage() {
        return extendedKeyUsage;
    }

    public void setExtendedKeyUsage(List<String> extendedKeyUsage) {
        this.extendedKeyUsage = extendedKeyUsage;
    }

    public int getBasicConstraints() {
        return basicConstraints;
    }

    public void setBasicConstraints(int basicConstraints) {
        this.basicConstraints = basicConstraints;
    }

    public Collection<List<?>> getSubjectAlternativeNames() {
        return subjectAlternativeNames;
    }

    public void setSubjectAlternativeNames(Collection<List<?>> subjectAlternativeNames) {
        this.subjectAlternativeNames = subjectAlternativeNames;
    }

    public Collection<List<?>> getIssuerAlternativeNames() {
        return IssuerAlternativeNames;
    }

    public void setIssuerAlternativeNames(Collection<List<?>> issuerAlternativeNames) {
        IssuerAlternativeNames = issuerAlternativeNames;
    }

    public PublicKeyInfo getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKeyInfo publicKey) {
        this.publicKey = publicKey;
    }

    public Set<String> getCriticalExtensionOIDs() {
        return criticalExtensionOIDs;
    }

    public void setCriticalExtensionOIDs(Set<String> criticalExtensionOIDs) {
        this.criticalExtensionOIDs = criticalExtensionOIDs;
    }

    public Set<String> getNonCriticalExtensionOIDs() {
        return nonCriticalExtensionOIDs;
    }

    public void setNonCriticalExtensionOIDs(Set<String> nonCriticalExtensionOIDs) {
        this.nonCriticalExtensionOIDs = nonCriticalExtensionOIDs;
    }
}
