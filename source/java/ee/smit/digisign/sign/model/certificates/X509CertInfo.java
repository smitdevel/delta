package ee.smit.digisign.sign.model.certificates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties
public class X509CertInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    X509CertificateInfo x509Certificate;
    String subjectName;
    String serial;
    String serialPrefix;
    String serialNumber;
    String name;
    String signOuType;
    List<String> keyUsages;
    List<String> certificatePolicies;

    public X509CertificateInfo getX509Certificate() {
        return x509Certificate;
    }

    public void setX509Certificate(X509CertificateInfo x509Certificate) {
        this.x509Certificate = x509Certificate;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSignOuType() {
        return signOuType;
    }

    public void setSignOuType(String signOuType) {
        this.signOuType = signOuType;
    }

    public List<String> getKeyUsages() {
        return keyUsages;
    }

    public void setKeyUsages(List<String> keyUsages) {
        this.keyUsages = keyUsages;
    }

    public List<String> getCertificatePolicies() {
        return certificatePolicies;
    }

    public void setCertificatePolicies(List<String> certificatePolicies) {
        this.certificatePolicies = certificatePolicies;
    }

    public String getSerialPrefix() {
        return serialPrefix;
    }

    public void setSerialPrefix(String serialPrefix) {
        this.serialPrefix = serialPrefix;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
