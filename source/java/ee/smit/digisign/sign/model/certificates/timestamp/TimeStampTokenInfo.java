package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.bouncycastle.asn1.cms.AttributeTable;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties
public class TimeStampTokenInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    List<X509CertificateHolder> attributeCertificates;
    List<X509CertificateHolder> certificates;
    List<X509CertificateHolder> cRLs;
    SignerId sID;
    AttributeTable signedAttributes;
    AttributeTable unsignedAttributes;
    TimeStampToken timeStampInfo;

    public List<X509CertificateHolder> getAttributeCertificates() {
        return attributeCertificates;
    }

    public void setAttributeCertificates(List<X509CertificateHolder> attributeCertificates) {
        this.attributeCertificates = attributeCertificates;
    }

    public List<X509CertificateHolder> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<X509CertificateHolder> certificates) {
        this.certificates = certificates;
    }

    public List<X509CertificateHolder> getcRLs() {
        return cRLs;
    }

    public void setcRLs(List<X509CertificateHolder> cRLs) {
        this.cRLs = cRLs;
    }

    public SignerId getsID() {
        return sID;
    }

    public void setsID(SignerId sID) {
        this.sID = sID;
    }

    public AttributeTable getSignedAttributes() {
        return signedAttributes;
    }

    public void setSignedAttributes(AttributeTable signedAttributes) {
        this.signedAttributes = signedAttributes;
    }

    public AttributeTable getUnsignedAttributes() {
        return unsignedAttributes;
    }

    public void setUnsignedAttributes(AttributeTable unsignedAttributes) {
        this.unsignedAttributes = unsignedAttributes;
    }

    public TimeStampToken getTimeStampInfo() {
        return timeStampInfo;
    }

    public void setTimeStampInfo(TimeStampToken timeStampInfo) {
        this.timeStampInfo = timeStampInfo;
    }
}
