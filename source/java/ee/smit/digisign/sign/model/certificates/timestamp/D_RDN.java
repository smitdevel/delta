package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.smit.digisign.sign.model.certificates.enums.X500AttributeTypes;

import java.io.Serializable;

@JsonIgnoreProperties
public class D_RDN implements Serializable {
    private static final long serialVersionUID = 1L;

    String typeId;
    String typeName;
    String valueString;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
        this.typeName = X500AttributeTypes.fromId(typeId);
    }

    public String getTypeName() {
        return typeName;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }
}
