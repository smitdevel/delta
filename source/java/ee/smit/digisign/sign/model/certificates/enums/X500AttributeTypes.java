package ee.smit.digisign.sign.model.certificates.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum X500AttributeTypes {
    objectClass("2.5.4.0", "objectClass"),
    aliasedEntryName("2.5.4.1", "aliasedEntryName"),
    knowldgeinformation("2.5.4.2", "knowldgeinformation"),
    commonName("2.5.4.3", "commonName"),
    surname("2.5.4.4", "surname"),
    serialNumber("2.5.4.5", "serialNumber"),
    countryName("2.5.4.6", "countryName"),
    localityName("2.5.4.7", "localityName"),
    stateOrProvinceName("2.5.4.8", "stateOrProvinceName"),
    streetAddress("2.5.4.9", "streetAddress"),
    organizationName("2.5.4.10", "organizationName"),
    organizationalUnitName("2.5.4.11", "organizationalUnitName"),
    title("2.5.4.12", "title"),
    description("2.5.4.13", "description"),
    searchGuide("2.5.4.14", "searchGuide"),
    businessCategory("2.5.4.15", "businessCategory"),
    postalAddress("2.5.4.16", "postalAddress"),
    postalCode("2.5.4.17", "postalCode"),
    postOfficeBox("2.5.4.18", "postOfficeBox"),
    physicalDeliveryOfficeName("2.5.4.19", "physicalDeliveryOfficeName"),
    telephoneNumber("2.5.4.20", "telephoneNumber"),
    telexNumber("2.5.4.21", "telexNumber"),
    teletexTerminalIdentifier("2.5.4.22", "teletexTerminalIdentifier"),
    facsimileTelephoneNumber("2.5.4.23", "facsimileTelephoneNumber"),
    x121Address("2.5.4.24", "x121Address"),
    internationalISDNNumber("2.5.4.25", "internationalISDNNumber"),
    registeredAddress("2.5.4.26", "registeredAddress"),
    destinationIndicator("2.5.4.27", "destinationIndicator"),
    preferredDeliveryMethod("2.5.4.28", "preferredDeliveryMethod"),
    presentationAddress("2.5.4.29", "presentationAddress"),
    supportedApplicationContext("2.5.4.30", "supportedApplicationContext"),
    member("2.5.4.31", "member"),
    owner("2.5.4.32", "owner"),
    roleOccupant("2.5.4.33", "roleOccupant"),
    seeAlso("2.5.4.34", "seeAlso"),
    userPassword("2.5.4.35", "userPassword"),
    userCertificate("2.5.4.36", "userCertificate"),
    cACertificate("2.5.4.37", "cACertificate"),
    authorityRevocationList("2.5.4.38", "authorityRevocationList"),
    certificateRevocationList("2.5.4.39", "certificateRevocationList"),
    crossCertificatePair("2.5.4.40", "crossCertificatePair"),
    name("2.5.4.41", "name"),
    givenName("2.5.4.42", "givenName"),
    initials("2.5.4.43", "initials"),
    generationQualifier("2.5.4.44", "generationQualifier"),
    uniqueIdentifier("2.5.4.45", "uniqueIdentifier"),
    dnQualifier("2.5.4.46", "dnQualifier"),
    enhancedSearchGuide("2.5.4.47", "enhancedSearchGuide"),
    protocolInformation("2.5.4.48", "protocolInformation"),
    distinguishedName("2.5.4.49", "distinguishedName"),
    uniqueMember("2.5.4.50", "uniqueMember"),
    houseIdentifier("2.5.4.51", "houseIdentifier"),
    supportedAlgorithms("2.5.4.52", "supportedAlgorithms"),
    deltaRevocationList("2.5.4.53", "deltaRevocationList"),
    attributeCertificate("2.5.4.58", "Attribute Certificate attribute (attributeCertificate)"),
    pseudonym("2.5.4.65", "pseudonym");

    private static final Logger logger = LoggerFactory.getLogger(X500AttributeTypes.class);

    private final String codeValue;
    private final String nameValue;

    X500AttributeTypes(String codeValue, String nameValue) {
        this.codeValue = codeValue;
        this.nameValue = nameValue;
    }

    @Nullable
    public static String fromId(String codeValue) {
        for (X500AttributeTypes at : X500AttributeTypes.values()) {
            if (at.getCodeValue().equals(codeValue)) {
                return at.nameValue;
            }
        }
        return null;
    }

    public static String getNameByCode(String codeValue) {
        logger.debug("getNameByCode()... typeId: " + codeValue);
        try {
            return X500AttributeTypes.valueOf(codeValue).getNameValue();
        } catch (Exception e) {
            logger.error("ENUM: " + e.getMessage());
        }
        return null;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public String getNameValue() {
        return nameValue;
    }
}
