package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@JsonIgnoreProperties
public class SignerId implements Serializable {
    private static final long serialVersionUID = 1L;

    BigInteger serialNumber;
    List<D_RDN> rdns;

    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(BigInteger serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<D_RDN> getRdns() {
        return rdns;
    }

    public void setRdns(List<D_RDN> rdns) {
        this.rdns = rdns;
    }
}
