package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties
public class B_X500Name implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<D_RDN> rdns;
    private String value;
    private String commonName;

    public List<D_RDN> getRdns() {
        return rdns;
    }

    public void setRdns(List<D_RDN> rdns) {
        this.rdns = rdns;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }
}
