package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

@JsonIgnoreProperties
public class X509CertificateHolder implements Serializable {
    private static final long serialVersionUID = 1L;

    private B_X500Name issuer;
    private B_X500Name subject;
    private Date notAfter;
    private Date notBefore;
    private BigInteger serialNumber;
    private Integer versionNumber;

    public B_X500Name getIssuer() {
        return issuer;
    }

    public void setIssuer(B_X500Name issuer) {
        this.issuer = issuer;
    }

    public B_X500Name getSubject() {
        return subject;
    }

    public void setSubject(B_X500Name subject) {
        this.subject = subject;
    }

    public Date getNotAfter() {
        return notAfter;
    }

    public void setNotAfter(Date notAfter) {
        this.notAfter = notAfter;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = notBefore;
    }

    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(BigInteger serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Integer versionNumber) {
        this.versionNumber = versionNumber;
    }
}
