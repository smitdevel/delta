package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties
public class CertStore implements Serializable {
    private static final long serialVersionUID = 1L;

    List<X509CertificateHolder> certList = new ArrayList<>();

    public List<X509CertificateHolder> getCertList() {
        return certList;
    }

    public void setCertList(List<X509CertificateHolder> certList) {
        this.certList = certList;
    }
}
