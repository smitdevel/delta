package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

@JsonIgnoreProperties
public class TimeStampToken implements Serializable {
    private static final long serialVersionUID = 1L;

    private Date genTime;
    private String ImprintAlgOID;
    private String policy;
    private BigInteger serialNumber;
    private B_X500Name tsaName;
    private String hashAlgorithmId;

    public Date getGenTime() {
        return genTime;
    }

    public void setGenTime(Date genTime) {
        this.genTime = genTime;
    }

    public String getImprintAlgOID() {
        return ImprintAlgOID;
    }

    public void setImprintAlgOID(String imprintAlgOID) {
        ImprintAlgOID = imprintAlgOID;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(BigInteger serialNumber) {
        this.serialNumber = serialNumber;
    }

    public B_X500Name getTsaName() {
        return tsaName;
    }

    public void setTsaName(B_X500Name tsaName) {
        this.tsaName = tsaName;
    }

    public String getHashAlgorithmId() {
        return hashAlgorithmId;
    }

    public void setHashAlgorithmId(String hashAlgorithmId) {
        this.hashAlgorithmId = hashAlgorithmId;
    }
}
