package ee.smit.digisign.sign.model.certificates.timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties
public class D_RDNS implements Serializable {
    private static final long serialVersionUID = 1L;

    List<D_RDN> rdns;

    public List<D_RDN> getRdns() {
        return rdns;
    }

    public void setRdns(List<D_RDN> rdns) {
        this.rdns = rdns;
    }
}
