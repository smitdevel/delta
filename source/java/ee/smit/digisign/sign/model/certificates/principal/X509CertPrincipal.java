package ee.smit.digisign.sign.model.certificates.principal;

public class X509CertPrincipal {
    String C;
    String CN;
    String O;
    String OU;
    String SURNAME;
    String GIVENNAME;
    String SERIALNUMBER;

    public X509CertPrincipal(String C, String CN, String O, String OU, String SURNAME, String GIVENNAME, String SERIALNUMBER){
        this.C = C;
        this.CN = CN;
        this.O = O;
        this.OU = OU;
        this.SURNAME = SURNAME;
        this.GIVENNAME = GIVENNAME;
        this.SERIALNUMBER = SERIALNUMBER;
    }

    public X509CertPrincipal(String SURNAME, String GIVENNAME, String SERIALNUMBER){
        this(null, null, null, null, SURNAME, GIVENNAME, SERIALNUMBER);
    }

    public X509CertPrincipal(String CN, String SURNAME, String GIVENNAME, String SERIALNUMBER){
        this(null, CN, null, null, SURNAME, GIVENNAME, SERIALNUMBER);
    }

    public X509CertPrincipal(String CN){
        this(null, CN, null, null, null, null, null);
    }
    public String getC() {
        return C;
    }

    public String getCN() {
        return CN;
    }

    public String getSURNAME() {
        return SURNAME;
    }

    public String getGIVENNAME() {
        return GIVENNAME;
    }

    public String getSERIALNUMBER() {
        return SERIALNUMBER;
    }

    public String getO() {
        return O;
    }

    public String getOU() {
        return OU;
    }
}
