package ee.smit.digisign.sign.model.certificates.timestamp;

import java.util.Date;

public class TimeStampTokenShort {
    private Date genTime;
    private String tsaNameCommonName;

    public Date getGenTime() {
        return genTime;
    }

    public void setGenTime(Date genTime) {
        this.genTime = genTime;
    }

    public String getTsaNameCommonName() {
        return tsaNameCommonName;
    }

    public void setTsaNameCommonName(String tsaNameCommonName) {
        this.tsaNameCommonName = tsaNameCommonName;
    }
}
