package ee.smit.digisign.sign.model.digidoc.siging;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.digidoc4j.DataToSign;

import java.io.Serializable;

/**
 * Holds intermediate data for Mobile-ID signing.
 */
@JsonIgnoreProperties
public class SignatureChallenge implements Serializable {
    private static final long serialVersionUID = 1L;

    private final String regCode;
    private final String sesscode;
    private final String challengeId;
    private final String serviceUid;
    private final String serviceName;
    private final String documentNr;
    private final DataToSign dataToSign;
    private final Object object;
    private final String displayText60;
    private final String displayText200;

    public SignatureChallenge(String regCode, String sesscode, String challengeId, String serviceUid, String serviceName, DataToSign dataToSign, String documentNr, Object object, String displayText60, String displayText200) {
        this.regCode = regCode;
        this.sesscode = sesscode;
        this.challengeId = challengeId;
        this.serviceUid = serviceUid;
        this.serviceName = serviceName;
        this.dataToSign = dataToSign;
        this.documentNr = documentNr;
        this.object = object;
        this.displayText60 = displayText60;
        this.displayText200 = displayText200;
    }

    public String getSesscode() {
        return sesscode;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public DataToSign getDataToSign() {
        return dataToSign;
    }

    public String getServiceUid() {
        return serviceUid;
    }

    public String getServiceName() {
        return serviceName;
    }

    public Object getObject() {
        return object;
    }

    public String getRegCode() {
        return regCode;
    }

    public String getDocumentNr() {
        return documentNr;
    }

    public String getDisplayText60() {
        return displayText60;
    }

    public String getDisplayText200() {
        return displayText200;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("SignatureChallenge[");
        sb.append("regCode=").append(regCode);
        sb.append(", sesscode=").append(sesscode);
        sb.append(", challengeId=").append(challengeId);
        sb.append(", serviceUid=").append(serviceUid);
        sb.append(", serviceName=").append(serviceName);
        //sb.append(", dataToSign=").append(dataToSign);
        sb.append(", documentNr=").append(documentNr);
        //sb.append(", object=").append(object);
        sb.append(", displayText60=").append(displayText60);
        sb.append(", displayText200=").append(displayText200);
        sb.append("]");
        return sb.toString();
    }


}