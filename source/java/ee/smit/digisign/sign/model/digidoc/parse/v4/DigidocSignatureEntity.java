package ee.smit.digisign.sign.model.digidoc.parse.v4;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import ee.smit.digisign.sign.model.certificates.X509CertInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties
public class DigidocSignatureEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    String serial;

    String socialSecNr;
    String name;
    byte[] adESSignature;
    String city;
    Date claimedSigningTime;
    String countryName;
    String id;
    X509CertInfo oCSPCertificate;
    byte[] oCSPNonce;
    Date oCSPResponseCreationTime;
    String postalCode;
    String profile;
    String signatureMethod;
    List<String> signerRoles;
    X509CertInfo signingCertificate;
    String stateOrProvince;
    Date timeStampCreationTime;
    X509CertInfo timeStampTokenCertificate;
    Date trustedSigningTime;
    List<DigidocExceptionEntity> digidocErrorsList;
    List<DigidocExceptionEntity> digidocWarningsList;
    SigninedPrincipal principal;
    Boolean valid = null;

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
        if(serial.contains("-") && serial.toUpperCase().startsWith("PNO")){
            String[] parts = serial.split("-", 1);
            socialSecNr = parts[1];
        }
    }

    public String getSocialSecNr() {
        return socialSecNr;
    }

    public void setSocialSecNr(String socialSecNr) {
        this.socialSecNr = socialSecNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SigninedPrincipal getPrincipal() {
        return principal;
    }

    public void setPrincipal(SigninedPrincipal principal) {
        this.principal = principal;
    }

    public byte[] getAdESSignature() {
        return adESSignature;
    }

    public void setAdESSignature(byte[] adESSignature) {
        this.adESSignature = adESSignature;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getClaimedSigningTime() {
        return claimedSigningTime;
    }

    public void setClaimedSigningTime(Date claimedSigningTime) {
        this.claimedSigningTime = claimedSigningTime;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public X509CertInfo getoCSPCertificate() {
        return oCSPCertificate;
    }

    public void setoCSPCertificate(X509CertInfo oCSPCertificate) {
        this.oCSPCertificate = oCSPCertificate;
    }

    public byte[] getoCSPNonce() {
        return oCSPNonce;
    }

    public void setoCSPNonce(byte[] oCSPNonce) {
        this.oCSPNonce = oCSPNonce;
    }

    public Date getoCSPResponseCreationTime() {
        return oCSPResponseCreationTime;
    }

    public void setoCSPResponseCreationTime(Date oCSPResponseCreationTime) {
        this.oCSPResponseCreationTime = oCSPResponseCreationTime;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getSignatureMethod() {
        return signatureMethod;
    }

    public void setSignatureMethod(String signatureMethod) {
        this.signatureMethod = signatureMethod;
    }

    public List<String> getSignerRoles() {
        return signerRoles;
    }

    public void setSignerRoles(List<String> signerRoles) {
        this.signerRoles = signerRoles;
    }

    public X509CertInfo getSigningCertificate() {
        return signingCertificate;
    }

    public void setSigningCertificate(X509CertInfo signingCertificate) {
        this.signingCertificate = signingCertificate;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public Date getTimeStampCreationTime() {
        return timeStampCreationTime;
    }

    public void setTimeStampCreationTime(Date timeStampCreationTime) {
        this.timeStampCreationTime = timeStampCreationTime;
    }

    public List<DigidocExceptionEntity> getDigidocErrorsList() {
        return digidocErrorsList;
    }

    public void setDigidocErrorsList(List<DigidocExceptionEntity> digidocErrorsList) {
        this.digidocErrorsList = digidocErrorsList;
    }

    public List<DigidocExceptionEntity> getDigidocWarningsList() {
        return digidocWarningsList;
    }

    public void setDigidocWarningsList(List<DigidocExceptionEntity> digidocWarningsList) {
        this.digidocWarningsList = digidocWarningsList;
    }

    public X509CertInfo getTimeStampTokenCertificate() {
        return timeStampTokenCertificate;
    }

    public void setTimeStampTokenCertificate(X509CertInfo timeStampTokenCertificate) {
        this.timeStampTokenCertificate = timeStampTokenCertificate;
    }

    public Date getTrustedSigningTime() {
        return trustedSigningTime;
    }

    public void setTrustedSigningTime(Date trustedSigningTime) {
        this.trustedSigningTime = trustedSigningTime;
    }

    public Boolean isValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Boolean getValid() {
        return valid;
    }
}
