package ee.smit.digisign.sign.model.digidoc.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class DigidocParseRequest {
    String regCode;
    Boolean shortFormat;
    Boolean cache;
    Boolean datafiles;
    Boolean signatures;
    Boolean validate;

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public Boolean getShortFormat() {
        return shortFormat;
    }

    public void setShortFormat(Boolean shortFormat) {
        this.shortFormat = shortFormat;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getDatafiles() {
        return datafiles;
    }

    public void setDatafiles(Boolean datafiles) {
        this.datafiles = datafiles;
    }

    public Boolean getSignatures() {
        return signatures;
    }

    public void setSignatures(Boolean signatures) {
        this.signatures = signatures;
    }

    public Boolean getValidate() {
        return validate;
    }

    public void setValidate(Boolean validate) {
        this.validate = validate;
    }
}
