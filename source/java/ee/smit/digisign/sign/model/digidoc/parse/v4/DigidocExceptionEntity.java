package ee.smit.digisign.sign.model.digidoc.parse.v4;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties
public class DigidocExceptionEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    int errorCodeId;
    String signatureId;
    String message;

    public int getErrorCodeId() {
        return errorCodeId;
    }

    public void setErrorCodeId(int errorCodeId) {
        this.errorCodeId = errorCodeId;
    }

    public String getSignatureId() {
        return signatureId;
    }

    public void setSignatureId(String signatureId) {
        this.signatureId = signatureId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
