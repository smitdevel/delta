package ee.smit.digisign.sign.model.digidoc;

public class DataToSign {

    private byte[] dataToBeSigned;

    public byte[] getDataToBeSigned() {
        return dataToBeSigned;
    }

    public void setDataToBeSigned(byte[] dataToBeSigned) {
        this.dataToBeSigned = dataToBeSigned;
    }
}
