package ee.smit.digisign.sign.model.digidoc.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class DefaultResponse {
    private int statusCode;
    private String statusText;

    public int getStatusCode() {
        return statusCode;
    }

    // -- GETTERS, SETTERS -------------------------------
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

}
