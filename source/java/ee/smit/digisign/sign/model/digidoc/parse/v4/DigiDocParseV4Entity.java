package ee.smit.digisign.sign.model.digidoc.parse.v4;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import ee.smit.digisign.sign.model.certificates.timestamp.TimeStampTokenInfo;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties
public class DigiDocParseV4Entity implements Serializable {
    private static final long serialVersionUID = 1L;

    Boolean containerValid = null;
    List<DigidocDataEntity> dataFiles;
    List<DigidocSignatureEntity> signatures;
    TimeStampTokenInfo timeStampToken;
    List<DigidocExceptionEntity> errors;
    List<DigidocExceptionEntity> warnings;
    List<DigidocExceptionEntity> containerErrors;
    String validationReport;

    public TimeStampTokenInfo getTimeStampToken() {
        return timeStampToken;
    }

    public void setTimeStampToken(TimeStampTokenInfo timeStampToken) {
        this.timeStampToken = timeStampToken;
    }

    public Boolean isContainerValid() {
        return containerValid;
    }

    public void setContainerValid(Boolean containerValid) {
        this.containerValid = containerValid;
    }

    public List<DigidocDataEntity> getDataFiles() {
        return dataFiles;
    }

    public void setDataFiles(List<DigidocDataEntity> dataFiles) {
        this.dataFiles = dataFiles;
    }

    public List<DigidocSignatureEntity> getSignatures() {
        return signatures;
    }

    public void setSignatures(List<DigidocSignatureEntity> signatures) {
        this.signatures = signatures;
    }

    public List<DigidocExceptionEntity> getErrors() {
        return errors;
    }

    public void setErrors(List<DigidocExceptionEntity> errors) {
        this.errors = errors;
    }

    public List<DigidocExceptionEntity> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<DigidocExceptionEntity> warnings) {
        this.warnings = warnings;
    }

    public List<DigidocExceptionEntity> getContainerErrors() {
        return containerErrors;
    }

    public void setContainerErrors(List<DigidocExceptionEntity> containerErrors) {
        this.containerErrors = containerErrors;
    }

    public String getValidationReport() {
        return validationReport;
    }

    public void setValidationReport(String validationReport) {
        this.validationReport = validationReport;
    }
}
