package ee.smit.digisign.sign.model.digidoc.file;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties
public class RequestFileInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String filename;
    private Integer size;
    private String encoding;
    private String mimetype;
    private String bash64EncodedName;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }


    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getBash64EncodedName() {
        return bash64EncodedName;
    }

    public void setBash64EncodedName(String bash64EncodedName) {
        this.bash64EncodedName = bash64EncodedName;
    }
}
