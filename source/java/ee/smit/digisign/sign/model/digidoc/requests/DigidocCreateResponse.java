package ee.smit.digisign.sign.model.digidoc.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class DigidocCreateResponse {
    String uid;
    String dataToSignHex;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDataToSignHex() {
        return dataToSignHex;
    }

    public void setDataToSignHex(String dataToSignHex) {
        this.dataToSignHex = dataToSignHex;
    }
}
