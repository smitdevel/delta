package ee.smit.digisign.sign.model.digidoc.parse.v4;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties
public class SigninedPrincipal implements Serializable {
    private static final long serialVersionUID = 1L;

    String SURNAME;
    String GIVENNAME;
    String SERIALNUMBER;

    public SigninedPrincipal(String SURNAME, String GIVENNAME, String SERIALNUMBER) {
        this.SURNAME = SURNAME;
        this.GIVENNAME = GIVENNAME;
        this.SERIALNUMBER = SERIALNUMBER;
    }

    public String getSURNAME() {
        return SURNAME;
    }

    public String getGIVENNAME() {
        return GIVENNAME;
    }

    public String getSERIALNUMBER() {
        return SERIALNUMBER;
    }
}
