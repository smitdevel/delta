package ee.smit.digisign.sign.model.digidoc.file;

import java.util.Map;

public class RequestFiles {

    Map<String, RequestFileInfo> filesInfos;

    public Map<String, RequestFileInfo> getFilesInfos() {
        return filesInfos;
    }

    public void setFilesInfos(Map<String, RequestFileInfo> filesInfos) {
        this.filesInfos = filesInfos;
    }
}
