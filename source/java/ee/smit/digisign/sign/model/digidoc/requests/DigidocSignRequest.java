package ee.smit.digisign.sign.model.digidoc.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class DigidocSignRequest {
    String uid;
    String signatureHex;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSignatureHex() {
        return signatureHex;
    }

    public void setSignatureHex(String signatureHex) {
        this.signatureHex = signatureHex;
    }
}
