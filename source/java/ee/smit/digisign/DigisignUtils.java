package ee.smit.digisign;

import org.alfresco.service.cmr.repository.NodeRef;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

public interface DigisignUtils {
    String BEAN_NAME = "DigisignUtils";

    JSONObject checkDigidocCrypto(String base64FileData) throws JSONException;

    JSONObject checkDigidocCryptoAndTimestamp(List<JSONObject> files) throws JSONException;


    Date stringToDate(String dateValue);

    Date digiSignStringToDate(String dateValue);

    byte[] getContent(NodeRef nodeRef);

    InputStream getContentInputStream(NodeRef nodeRef);

    List<JSONObject> getFilesByFileRefList(List<NodeRef> fileRefs) throws Exception;
}
