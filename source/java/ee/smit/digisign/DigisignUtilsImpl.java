package ee.smit.digisign;

import org.alfresco.service.cmr.repository.NodeRef;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

public class DigisignUtilsImpl implements DigisignUtils {

    @Override
    public JSONObject checkDigidocCrypto(String base64FileData) throws JSONException {
        return null;
    }

    @Override
    public JSONObject checkDigidocCryptoAndTimestamp(List<JSONObject> files) throws JSONException {
        return null;
    }

    @Override
    public Date stringToDate(String dateValue) {
        return null;
    }

    @Override
    public Date digiSignStringToDate(String dateValue) {
        return null;
    }

    @Override
    public byte[] getContent(NodeRef nodeRef) {
        return new byte[0];
    }

    @Override
    public InputStream getContentInputStream(NodeRef nodeRef) {
        return null;
    }

    @Override
    public List<JSONObject> getFilesByFileRefList(List<NodeRef> fileRefs) throws Exception {
        return null;
    }
}
