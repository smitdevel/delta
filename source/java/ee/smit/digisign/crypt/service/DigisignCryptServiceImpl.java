package ee.smit.digisign.crypt.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DigisignCryptServiceImpl implements DigisignCryptService {
    protected final Log log = LogFactory.getLog(getClass());

    private String appname;
    private String apppass;
    private String defaultUri;
    private boolean active;
    private String clientId;
    private String clientSecret;
    private String regCode;

    // Getters, Setters ------------------------
    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppname() {
        return appname;
    }

    public void setApppass(String apppass) {
        this.apppass = apppass;
    }

    public String getApppass() {
        return apppass;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setDefaultUri(String defaultUri) {
        this.defaultUri = defaultUri;
    }

    public String getUri() {
        return defaultUri;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getRegCode() {
        return regCode;
    }
}
