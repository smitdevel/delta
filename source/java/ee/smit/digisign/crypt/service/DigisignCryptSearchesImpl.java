package ee.smit.digisign.crypt.service;

import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.common.http.RestUtil;
import ee.smit.digisign.crypt.model.*;
import ee.smit.digisign.sign.model.digidoc.file.RequestFiles;
import ee.smit.digisigncrypt.model.crypt.CryptResponse;
import ee.smit.oauth2.Oauth2Client;
import ee.webmedia.alfresco.signature.exception.SignatureException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import org.apache.http.auth.AuthenticationException;

import java.io.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ee.smit.common.CertUtils.convertX509CertListToCertInfoList;
import static ee.smit.common.http.RestUtil.addFilesAsMultiPartFiles;
import static ee.smit.common.http.RestUtil.digiSignStringToDate;

public class DigisignCryptSearchesImpl implements DigisignCryptSearches {
    protected final static Log log = LogFactory.getLog(DigisignCryptSearchesImpl.class);
    protected DigisignCryptService digiSignCryptService;
    private String API_CDOC_DECRYPT = "/api/decrypt/decrypt";

    /**
     *
     * @param cn
     * @return
     * @throws Exception
     */
    public JSONObject getCertificatesByCn(String cn)  throws JSONException {
        String seachUri = digiSignCryptService.getUri() + "/api/certificate/searchByCn/" + cn;
        return RestUtil.searchByGet(seachUri);
    }
    public JSONObject getCertificatesByCnAndSerialNumber(String cn, String serialnumber)  throws JSONException{
        if(cn == null){
            cn="";
        }
        if(serialnumber == null){
            serialnumber = "";
        }

        String seachUri = digiSignCryptService.getUri() + "/api/certificate/search?cn=" + cn + "&serialnumber="+serialnumber;
        return RestUtil.searchByGet(seachUri);
    }

    /**
     *
     * @param serialNumber
     * @return
     * @throws Exception
     */
    public JSONObject getCertificatesBySerialNumber(String serialNumber)  throws JSONException{
        String seachUri = digiSignCryptService.getUri() + "/api/certificate/searchBySerialNumber/" + serialNumber;
        log.debug("getCertificatesBySerialNumber: uri: " + seachUri);
        return RestUtil.searchByGet(seachUri);
    }

    private void getOutputStreamCdocResponse(OutputStream out, CryptResponse response) throws IOException {
        log.debug("RESPONSE status: " + response.getStatus());
        log.debug("RESPONSE status code: " + response.getStatusCode());

        String cdocFileBase64Encoded = response.getFile();
        log.debug("CDOC string length: " + (cdocFileBase64Encoded == null ? "NULL" : cdocFileBase64Encoded.length()));
        if(cdocFileBase64Encoded != null || !cdocFileBase64Encoded.isEmpty()){
            log.debug("CDOC string length: " + cdocFileBase64Encoded.length());
            byte[] file = Base64.decodeBase64(cdocFileBase64Encoded);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            out.write(file);
        }
        IOUtils.closeQuietly(out);

    }

    /**
     *
     * @param certs (base64 encoded input)
     * @param fileRefs List of nodeRefs
     * @param out Outputstream
     */
    public void makeCdoc(List<X509Certificate> certs, List<NodeRef> fileRefs, OutputStream out) throws Exception {
        String serviceUri = digiSignCryptService.getUri() + "/api/crypt/v2/make";

        try {
            RequestFiles files = new RequestFiles();

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            addFilesAsMultiPartFiles("files", body, fileRefs, files, "");
            body.add("cryptJson", getCryptFiltesRequest(certs, files));
            CryptResponse response = Oauth2Client.getMultipartPostRequest(body, serviceUri, CryptResponse.class);
            getOutputStreamCdocResponse(out, response);
        } catch (Exception e) {
            throw new SignatureException("Failed to create ID-CARD digest request: ERROR MESSAGE: " + e.getMessage(), e);
        }
    }

    CryptFilesRequest getCryptFiltesRequest(List<X509Certificate> certs, RequestFiles files) throws CertificateEncodingException {
        CryptFilesRequest request = new CryptFilesRequest();
        request.setCertificates(convertX509CertListToCertInfoList(certs));
        request.setFiles(files);
        return request;
    }


    public List<SignCertificate> getCertificatesFromDigiSignService(String serialNumber, String cn){
        List<SignCertificate> signCertificateList = new ArrayList<>();
        log.debug("Get certificates by cn name: " + cn);

        if(!cn.isEmpty() && !serialNumber.isEmpty()){
            signCertificateList.addAll(getCertsByCnandSerialNr(cn, serialNumber));
        } else {
            if(!cn.isEmpty()) {
                signCertificateList.addAll(getCertsByCn(cn));
            }

            if(!serialNumber.isEmpty()){
                signCertificateList.addAll(getCertsBySerialNumber(serialNumber));
            }
        }
        log.debug("Certificate list size: " + signCertificateList.size());
        return signCertificateList;
    }

    private List<SignCertificate> getSignCertificates(JSONObject jsonByCn) {
        log.debug("Check CN JSON Response...");
        List<SignCertificate> certListCn = checkJsonResponse(jsonByCn);
        if(certListCn != null && !certListCn.isEmpty()){
            log.debug("Found crypt supported certificates by cn (name)..");
        } else {
            log.trace("JSON response is NULL!");
        }
        return certListCn;
    }

    private List<SignCertificate> getCertsByCnandSerialNr(String cn, String serialNumber){
        JSONObject jsonByCn = getCertByCnandSerialNr(cn, serialNumber);
        return getSignCertificates(jsonByCn);
    }

    private List<SignCertificate> getCertsByCn(String cn){
        JSONObject jsonByCn = getCertByCn(cn);
        return getSignCertificates(jsonByCn);
    }

    private List<SignCertificate> getCertsBySerialNumber(String serialNumber){
        log.debug("Get certificates by serialNumber: " + serialNumber);
        JSONObject jsonBySerialNumber = getCertBySerialNumber(serialNumber);
        log.debug("Check SerialNumber JSON Response...");
        List<SignCertificate> certListNr = checkJsonResponse(jsonBySerialNumber);
        if(certListNr != null && !certListNr.isEmpty()){
            log.debug("Found crypt supported certificates by serialNumber (id-code, organization reg code)..");
        } else {
            log.trace("JSON response is NULL!");
        }
        return certListNr;
    }

    public List<SignCertificate> checkJsonResponse(JSONObject json){
        if(json == null){
            return new ArrayList<>();
        }
        List<SignCertificate> certList = new ArrayList<>();

        try{
            log.debug("DigiSign response status: " + json.getString("status"));
            if(json.getString("status").equals("NOT_FOUND")){
                return certList;
            }
            log.debug("-- GET certificates -----------------------------");

            log.debug("Try to get get(certificates)....");
            Object o = json.get("certificates");
            if(o == null){
                return certList;
            }
            log.debug("Object class: " + o.getClass());
            if(o instanceof ArrayList){

                @SuppressWarnings("unchecked")
                List<Object> objectList = (ArrayList<Object>) o;

                for (Object o1 : objectList){
                    log.debug("ObjectList o1 class: " + o1.getClass());
                    if(o1 instanceof Map){
                        log.debug("It's a MAP!");
                        SignCertificate digiSignCert = new SignCertificate();

                        @SuppressWarnings("unchecked")
                        Map<String, Object> certObject = (Map<String, Object>) o1;

                        String cn = (String) certObject.get("cn");
                        log.debug("CN: " + cn);
                        digiSignCert.setCn(cn);

                        String serialNumber = (String )certObject.get("serialNumber");
                        log.debug("serialNumber: " + serialNumber);
                        digiSignCert.setSerialNumber(serialNumber);

                        String certDataBase64Encoded = (String) certObject.get("userEncryptionCertificate");
                        log.debug("Certificate data: " + certDataBase64Encoded);
                        digiSignCert.setData(Base64.decodeBase64(certDataBase64Encoded));

                        String issuerCn = (String)certObject.get("issuerCn");
                        log.debug("issuerCn: " + issuerCn);
                        digiSignCert.setIssuerCn(issuerCn);

                        String notAfter = (String)certObject.get("notAfter");
                        log.debug("notAfter date: " + notAfter);
                        digiSignCert.setNotAfter(digiSignStringToDate(notAfter));

                        String notBefore = (String)certObject.get("notBefore");
                        log.debug("notBefore date: " + notBefore);
                        digiSignCert.setNotBefore(digiSignStringToDate(notBefore));

                        boolean encryptionSupported = (boolean) certObject.get("encryptionSupported");
                        log.debug("encryptionSupported: " + encryptionSupported);
                        digiSignCert.setEncryptionSupported(encryptionSupported);

                        certList.add(digiSignCert);
                    }
                }
            }

        } catch (Exception e){
            log.error(e.getMessage(), e);
        }

        return certList;
    }

    private JSONObject getCertBySerialNumber(String orgCode){
        JSONObject json = null;
        if (StringUtils.isNotBlank(orgCode)) {
            try{
                json = getCertificatesBySerialNumber(orgCode);
            } catch (JSONException e){
                log.error("DigiSign certificate search by org code failed!" + e.getMessage(), e);
            }
        }
        return json;
    }

    private JSONObject getCertByCn(String orgName){
        JSONObject json = null;
        if (StringUtils.isNotBlank(orgName)) {
            try{
                json = getCertificatesByCn(orgName);
            } catch (JSONException e){
                log.error("DigiSign certificate search by org code failed!" + e.getMessage(), e);
            }
        }
        return json;
    }

    private JSONObject getCertByCnandSerialNr(String orgName, String orgCode){
        JSONObject json = null;
        if (StringUtils.isNotBlank(orgName) && StringUtils.isNotBlank(orgCode)) {
            try{
                json = getCertificatesByCnAndSerialNumber(orgName, orgCode);
            } catch (JSONException e){
                log.error("DigiSign certificate search by org code failed!" + e.getMessage(), e);
            }
        }
        return json;
    }

    public List<DataFile> decryptCDOC(InputStream filedata, String filename) throws AuthenticationException, IOException {
        byte[] fileBytes = IOUtils.toByteArray(filedata);
        return decryptCDOC(fileBytes, filename);
    }
    public List<DataFile> decryptCDOC(byte[] filedata, String filename) throws AuthenticationException {
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        addFilesAsMultiPartFiles("file", body, filedata, filename);
        body.add("json", new DeCryptRequest(digiSignCryptService.getRegCode()));

        DecryptResponse response = makeDecryptRequest(body);
        return response.getDataFiles();
    }

    public DecryptResponse makeDecryptRequest(MultiValueMap<String, Object> body) throws AuthenticationException {
        String serverUrl = digiSignCryptService.getUri() + API_CDOC_DECRYPT;

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(),  getToken()));

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<DecryptResponse> response = restTemplate.postForEntity(serverUrl, requestEntity, DecryptResponse.class);
        return response.getBody();
    }

    public Oauth2TokenInfo getToken() throws AuthenticationException {
        return Oauth2Client.sendTokenRequestAsBasicAuth(digiSignCryptService.getUri(), digiSignCryptService.getClientId(), digiSignCryptService.getClientSecret(), digiSignCryptService.getAppname(), digiSignCryptService.getApppass());
    }

    public void setDigisignCryptService(DigisignCryptService digiSignCryptService) {
        this.digiSignCryptService = digiSignCryptService;
    }
}
