package ee.smit.digisign.crypt.service;

public interface DigisignCryptService {
    String BEAN_NAME = "DigisignCryptService";

    String getUri();

    String getAppname();

    String getApppass();

    boolean isActive();

    String getClientId();

    String getClientSecret();

    String getRegCode();
}
