package ee.smit.digisign.crypt.service;

import ee.smit.digisign.crypt.model.DataFile;
import ee.smit.digisign.crypt.model.SignCertificate;
import org.alfresco.service.cmr.repository.NodeRef;
import org.json.JSONException;
import org.json.JSONObject;

import org.apache.http.auth.AuthenticationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.X509Certificate;
import java.util.List;

public interface DigisignCryptSearches {
    String BEAN_NAME = "DigisignCryptSearches";

    JSONObject getCertificatesByCn(String cn) throws JSONException;

    JSONObject getCertificatesByCnAndSerialNumber(String cn, String serialnumber)  throws JSONException;

    JSONObject getCertificatesBySerialNumber(String serialNumber) throws JSONException;

    void makeCdoc(List<X509Certificate> certs, List<NodeRef> fileRefs, OutputStream tmpOutput) throws Exception;

    List<SignCertificate> getCertificatesFromDigiSignService(String orgCode, String orgName);

    List<SignCertificate> checkJsonResponse(JSONObject json);

    List<DataFile> decryptCDOC(byte[] filedata, String filename) throws AuthenticationException;
    List<DataFile> decryptCDOC(InputStream filedata, String filename) throws AuthenticationException, IOException;

}
