package ee.smit.digisign.crypt.model;

public class CertInfo {
    private String certificate;

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    @Override
    public String toString() {
        return "CERTIFICATE length: " + (certificate == null ? "NULL" : certificate.length());
    }
}
