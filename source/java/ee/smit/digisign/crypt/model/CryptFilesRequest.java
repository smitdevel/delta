package ee.smit.digisign.crypt.model;

import ee.smit.digisign.sign.model.digidoc.file.RequestFiles;

import java.util.List;

public class CryptFilesRequest {
    private List<CertInfo> certificates;
    RequestFiles files;

    public List<CertInfo> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<CertInfo> certificates) {
        this.certificates = certificates;
    }

    public RequestFiles getFiles() {
        return files;
    }

    public void setFiles(RequestFiles files) {
        this.files = files;
    }
}
