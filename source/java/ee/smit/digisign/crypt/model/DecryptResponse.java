package ee.smit.digisign.crypt.model;

import java.util.List;

public class DecryptResponse {
    String status;
    Integer statusCode;
    List<String> recipents;
    List<DataFile> dataFiles;

    public List<DataFile> getDataFiles() {
        return dataFiles;
    }

    public void setDataFiles(List<DataFile> dataFiles) {
        this.dataFiles = dataFiles;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getRecipents() {
        return recipents;
    }

    public void setRecipents(List<String> recipents) {
        this.recipents = recipents;
    }
}