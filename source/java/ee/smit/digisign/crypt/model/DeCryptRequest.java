package ee.smit.digisign.crypt.model;

public class DeCryptRequest {
    String asutusCode;

    public DeCryptRequest(String asutusCode){
        this.asutusCode = asutusCode;
    }
    public String getAsutusCode() {
        return asutusCode;
    }

    public void setAsutusCode(String asutusCode) {
        this.asutusCode = asutusCode;
    }
}
