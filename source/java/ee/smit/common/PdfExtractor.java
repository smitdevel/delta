package ee.smit.common;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.IOException;
import java.io.InputStream;

public class PdfExtractor {

    public static String getText(InputStream is) throws IOException {
        PDDocument doc = PDDocument.load(is);
        return new PDFTextStripper().getText(doc);
    }

    public static String getText(byte[] bytes) throws IOException {
        PDDocument doc = PDDocument.load(bytes);
        return new PDFTextStripper().getText(doc);
    }

}
