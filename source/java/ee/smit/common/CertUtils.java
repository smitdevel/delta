package ee.smit.common;

import ee.smit.digisign.crypt.model.CertInfo;
import ee.smit.digisign.sign.service.DigiSignSearches;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class CertUtils {
    private static final Logger log = Logger.getLogger(CertUtils.class);

    protected DigiSignSearches digiSignSearches;

    public static List<JSONObject> convertX509CertListToBase64List(List<X509Certificate> certs) throws CertificateEncodingException, JSONException {
        List<JSONObject> certList = new ArrayList<>();
        for(X509Certificate cert: certs){
            byte[] certBytes = cert.getEncoded();
            JSONObject j = new JSONObject();
            String certdata = Base64.encodeBase64String(certBytes);
            j.put("certificate", certdata);
            certList.add(j);

        }
        return certList;
    }

    public static List<CertInfo> convertX509CertListToCertInfoList(List<X509Certificate> certs) throws CertificateEncodingException {
        List<CertInfo> certList = new ArrayList<>();
        for(X509Certificate cert: certs){
            byte[] certBytes = cert.getEncoded();
            String certdata = Base64.encodeBase64String(certBytes);

            CertInfo certInfo = new CertInfo();
            certInfo.setCertificate(certdata);

            certList.add(certInfo);
        }
        return certList;
    }

}
