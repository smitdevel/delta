package ee.smit.common;

import java.util.ArrayList;
import java.util.List;

public class SqlUtils {

    public static List<String> parseStrListToSql(String strList) {
        List<String> strings = new ArrayList<>();
        if (strList.contains(",")) {
            String[] strArr = strList.split(",");
            for (String str : strArr) {
                strings.add(str.trim());
            }
        } else {
            strings.add(strList.trim());
        }
        return strings;
    }

    public static String strToSqlStr(String str) {
        return "'" + str.trim() + "'";
    }
}
