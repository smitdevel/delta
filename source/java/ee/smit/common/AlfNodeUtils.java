package ee.smit.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

public class AlfNodeUtils {
    private static final Logger logger = LoggerFactory.getLogger(AlfNodeUtils.class);

    private static final String REGEX_01 = "[a-zA-Z]{1,}\\:\\/\\/[a-zA-Z]{1,}\\/";
    private static final String REGEX_02 = "([a-zA-Z]{1,})\\:\\/\\/([a-zA-Z]{1,})\\/([a-z\\-0-9]*)";

    public static boolean isStringReference(String reference) {
        return isStringReference(reference, REGEX_01);
    }

    public static boolean isStringReferenceWithUuid(String reference) {
        return isStringReference(reference, REGEX_02);
    }

    public static boolean isStringReference(String reference, String regex) {
        if (reference != null && reference.matches(regex)) {
            logger.debug("MATCH FOUND! " + reference);
            return true;
        } else {
            logger.debug("NO MATCH FOUND! " + reference);
        }
        return false;
    }

    public static String[] getRefParts(String ref) {
        return getRefParts(ref, REGEX_01);
    }

    public static String[] getRefParts(String ref, String regex) {
        if (!isStringReference(ref)) {
            return null;
        }
        return getParts(ref, regex);
    }

    public static String[] getRefPartsWithUuid(String ref) {
        return getRefPartsWithUuid(ref, REGEX_02);
    }

    public static String[] getRefPartsWithUuid(String ref, String regex) {
        if (!isStringReferenceWithUuid(ref)) {
            return null;
        }
        return getParts(ref, regex);
    }

    public static String[] getParts(String ref, String regex) {
        java.util.regex.Matcher m = Pattern.compile(regex).matcher(ref);
        if (m.matches()) {
            String[] refs = new String[m.groupCount()];
            for (int i = 1; i <= m.groupCount(); i++) {
                logger.debug(i + " PATTERN PART: " + m.group(i));
                refs[i - 1] = m.group(i);
            }
            return refs;
        }
        return new String[0];
    }
}
