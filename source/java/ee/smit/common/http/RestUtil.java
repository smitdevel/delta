package ee.smit.common.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.common.files.FileMessageResource;
import ee.smit.digisign.sign.model.digidoc.file.RequestFileInfo;
import ee.smit.digisign.sign.model.digidoc.file.RequestFiles;
import ee.smit.oauth2.Oauth2Client;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.file.model.FileModel;
import ee.webmedia.alfresco.utils.FilenameUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RestUtil {

    protected final static Log log = LogFactory.getLog(RestUtil.class);

    private static final RestTemplate restTemplate = new RestTemplate();

    /**
     * @param searchUri
     * @return
     * @throws Exception
     */
    public static JSONObject searchByGet(String searchUri) throws JSONException {
        log.debug("SEARCH URI: " + searchUri);
        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> result = restTemplate.getForEntity(searchUri, Map.class);
        JSONObject responseJson = new JSONObject(result.getBody());

        logResponseEntityMap(result, responseJson);

        return responseJson;
    }

    private static HttpHeaders getNewHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public static Object makeObjectPostRequest(Class<?> responseObject, Object obj, String postUri) throws URISyntaxException {
        log.debug("Request post uri: " + postUri);
        URI uri = new URI(postUri);
        return restTemplate.postForObject(uri, obj, responseObject);
    }

    public static JSONObject makePostRequest(JSONObject request, String postUri) throws JSONException {
        log.debug("Request post uri: " + postUri);
        HttpEntity<String> entity = new HttpEntity<>(request.toString(), getNewHeaders());
        ResponseEntity<String> result = restTemplate.postForEntity(postUri, entity, String.class);
        log.debug("POST REQUEST status: " + result.getStatusCode());
        log.debug("Has body: " + result.hasBody());

        String body = result.getBody();
        log.debug("Body length: " + body.length());

        return new JSONObject(body);
    }

    @SuppressWarnings("rawtypes")
    public static void logResponseEntityMap(ResponseEntity<Map> result, JSONObject json) {
        if (result != null) {
            if (result.getStatusCode() != null) {
                log.debug("RESULT STATUS CODE: name(): " + result.getStatusCode().name());
            }
            if (result.getStatusCode() != null) {
                log.debug("RESULT STATUS CODE: getReasonPhrase(): " + result.getStatusCode().getReasonPhrase());
                log.debug("RESULT STATUS CODE: series().name(): " + result.getStatusCode().series().name());
            }
            if (json != null) {
                log.debug("JSON FOUND.....check objeckts...");
                try {
                    JSONArray jsonArray = json.names();
                    int length = jsonArray.length();
                    log.debug("Found: " + length);
                } catch (Exception ex) {
                    {
                        log.error(ex.getMessage(), ex);
                    }
                }
            } else {
                log.debug("JSON RESPONSE IS NULL!");
            }
        } else {
            log.debug("RESULT IS NULL!");
        }
    }

    public static List<JSONObject> getFilesByFileRefList(List<NodeRef> fileRefs) throws Exception {
        List<JSONObject> filesList = new ArrayList<>();
        for (NodeRef ref : fileRefs) {
            JSONObject file = new JSONObject();
            byte[] fileBytes = BeanHelper.getDigiSignSearches().getContent(ref);
            String filedata = Base64.encodeBase64String(fileBytes);
            file.put("filedata", filedata);
            file.put("filename", getFilenameByNodeRef(ref));
            filesList.add(file);
        }
        return filesList;
    }

    public static void addFileAsMultiPartFile(String partname, MultiValueMap<String, Object> body, int fileIndex, File file, RequestFiles files) throws IOException {
        Map<String, RequestFileInfo> filesInfos = new HashMap<>();
        addFileAsMultiPartFile(partname, filesInfos, body, fileIndex, file.getName(), FileUtils.readFileToByteArray(file));
        files.setFilesInfos(filesInfos);
    }

    public static void addFileAsMultiPartFile(String partname, MultiValueMap<String, Object> body, int fileIndex, String filename, byte[] fileData, RequestFiles files) {
        log.debug("addFileAsMultiPartFile()... partname: " + partname + "; fileIndex: " + fileIndex + "; filename: " + filename + "; FILE data length: " + (fileData != null ? fileData.length : 0));
        Map<String, RequestFileInfo> filesInfos = new HashMap<>();
        addFileAsMultiPartFile(partname, filesInfos, body, fileIndex, filename, fileData);
        files.setFilesInfos(filesInfos);
    }

    public static void addFileAsMultiPartFile(String partname, Map<String, RequestFileInfo> filesInfos, MultiValueMap<String, Object> body, int fileIndex, String filename, byte[] fileData) {

        String fileName = convertFilenameToDigidocSupported(filename);
        String extension = FilenameUtils.getExtension(fileName);
        String newFileName = fileIndex + "." + extension;
        addFilesAsMultiPartFiles(partname, body, fileData, newFileName);
        filesInfos.put(newFileName, setRequestFileInfo(fileName, fileData));
    }

    public static void addFilesAsMultiPartFiles(String partname, MultiValueMap<String, Object> body, List<NodeRef> nodeRefs, RequestFiles files, String displayText) {
        log.debug("addFilesAsMultiPartFiles()...");
        Map<String, RequestFileInfo> filesInfos = new HashMap<>();

        int fileIndex = 0;
        StringBuilder displayTextBuilder = new StringBuilder(displayText);
        for (NodeRef nodeRef : nodeRefs) {
            log.debug("NODE REF: " + nodeRef);
            fileIndex++;

            byte[] fileData = BeanHelper.getDigiSignSearches().getContent(nodeRef);
            String filename = getFileName(nodeRef);
            log.debug("FILENAME: " + filename);
            displayTextBuilder.append(filename);
            if(nodeRefs.size() >= fileIndex){
                displayTextBuilder.append("\n");
            }
            addFileAsMultiPartFile(partname, filesInfos, body, fileIndex, filename, fileData);
        }
        displayText = displayTextBuilder.toString();
        files.setFilesInfos(filesInfos);
    }

    public static void addFilesAsMultiPartFiles(String partname, MultiValueMap<String, Object> body, byte[] fileData, String fileName) {
        body.add(partname, new FileMessageResource(fileData, fileName));
    }

    /**
     * @param fileName
     * @return
     */
    private static String convertFilenameToDigidocSupported(String fileName) {
        if (fileName != null && fileName.length() > 220) {
            String baseName = FilenameUtils.removeExtension(fileName);
            String extension = FilenameUtils.getExtension(fileName);
            fileName = baseName.substring(0, 210) + FilenameUtils.EXTENSION_SEPARATOR + extension;
        }
        return fileName;
    }

    /**
     * @param fileName
     * @param fileData
     * @return
     */
    private static RequestFileInfo setRequestFileInfo(String fileName, byte[] fileData) {
        RequestFileInfo fileInfo = new RequestFileInfo();
        fileInfo.setFilename(fileName);
        fileInfo.setBash64EncodedName(Base64.encodeBase64String(fileName.getBytes(StandardCharsets.UTF_8)));
        fileInfo.setSize(fileData != null ? fileData.length : 0);
        return fileInfo;
    }

    private static String covertObjectToJson(Object object) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(object);
    }

    private static String getFilenameByNodeRef(NodeRef ref) {
        return getFileName(ref);
        //FileInfo fileInfo = BeanHelper.getFileFolderService().getFileInfo(ref);
        //return FilenameUtil.makeSafeFilename(fileInfo.getName());
    }

    protected static String getFileName(NodeRef fileRef) {
        log.debug("getFileName(): nodeRef: " + fileRef);
        String name = (String) BeanHelper.getNodeService().getProperty(fileRef, FileModel.Props.DISPLAY_NAME);
        log.debug("Filename: (Display name): " + name);
        if (StringUtils.isNotBlank(name)) {
            name = FilenameUtil.stripForbiddenWindowsCharactersAndRedundantWhitespaces(name);
        }
        return StringUtils.isNotBlank(name) ? name : (String) BeanHelper.getNodeService().getProperty(fileRef, ContentModel.PROP_NAME);
    }

    public static Date digiSignStringToDate(String dateValue) {
        String DATE_FORMAT_00 = "yyyy-MM-dd'T'HH:mm:ss";

        if (dateValue == null) {
            return null;
        }

        Locale locale = Locale.getDefault();
        log.info("LOCALE: Country: " + locale.getCountry() + "; Language: " + locale.getLanguage());
        try {
            SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_00, locale);
            return parseStringToDate(dateValue, format);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public static Date parseStringToDate(String dateValue, SimpleDateFormat format) {
        try {
            Date date = format.parse(dateValue);
            return date;
        } catch (ParseException e) {
            log.error("String to Date convert failed! " + e.getMessage(), e);
        }
        return null;
    }

    public static boolean checkUrlExistsInPUT(String urltoCheck, Oauth2TokenInfo token) {
        return checkUrlExists(urltoCheck, token, "PUT");
    }

    public static boolean checkUrlExistsInGET(String urltoCheck, Oauth2TokenInfo token) {
        return checkUrlExists(urltoCheck, token, "GET");
    }

    public static boolean checkUrlExistsInPOST(String urltoCheck, Oauth2TokenInfo token) {
        return checkUrlExists(urltoCheck, token, "POST");
    }

    /**
     * Default urlChecking method is POST
     *
     * @param urltoCheck
     * @param token
     * @return
     */
    public static boolean checkUrlExists(String urltoCheck, Oauth2TokenInfo token) {
        return checkUrlExistsInPOST(urltoCheck, token);
    }

    /**
     * Check, if url exists or is connection error...
     *
     * @param urltoCheck
     * @return
     */
    public static boolean checkUrlExists(String urltoCheck, Oauth2TokenInfo token, String method) {
        log.debug("checkUrlExists()... url: " + urltoCheck);
        try {
            URL url = new URL(urltoCheck);
            HttpURLConnection.setFollowRedirects(true);

            HttpURLConnection huc = Oauth2Client.getHttpURLConnectionWithToken((HttpURLConnection) url.openConnection(), token);
            huc.setRequestMethod(method);
            log.debug("huc response code: " + huc.getResponseCode());
            if (huc.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
                return false;
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return true;
    }
}
