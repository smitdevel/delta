package ee.smit.common.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import ee.smit.auth.uaa.model.Oauth2TokenInfo;
import ee.smit.common.JsonUtils;
import ee.smit.common.files.FileMessageResource;
import ee.smit.oauth2.Oauth2Client;
import org.apache.http.auth.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;

public class HttpUtils {
    private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    public static <T> T restGetReq(String URI, Class<T> type) {
        ResponseEntity<T> response = new RestTemplate().getForEntity(URI, type);
        logger.debug("HTTP response STATUS: " + response.getStatusCode());
        if (response.hasBody()) {
            logger.debug("BODY FOUND!");
            return type.cast(response.getBody());
        }
        return null;
    }

    /*
    private Object makePostReqest(MultiValueMap<String, Object> body, String serverUrl, Class responseClass) throws AuthenticationException {

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, Oauth2Client.getHeadersWithToken(Oauth2Client.getMultipartFormDataHeaders(), getToken()));

        ResponseEntity<Object> response = new RestTemplate().postForEntity(serverUrl, requestEntity, responseClass);
        return response;
    }
    */

    public static <T> T restPostReq(String URI, Object obj, Class<T> responseType, Oauth2TokenInfo info) {
            return restPostReq(URI, new HttpEntity<>(obj, Oauth2Client.getHeadersWithToken(Oauth2Client.getHeaders(), info)), responseType);
    }

    public static <T> T restPostReq(String URI, Object obj, Class<T> type) {
        logger.info("restPostReq()...");
        ResponseEntity<T> response = null;
        try {
            response = new RestTemplate().postForEntity(URI, obj, type);
        } catch (Exception e){
            logger.error(e.getMessage(), e);
        }

        if(response == null){
            logger.info("RESPONSE (CLASS: " + type.getName() + ") IS NULL ");
            return null;
        }

        logger.debug("HTTP response STATUS: " + (response != null ? response.getStatusCode() : null));
        if(response.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR){
            if(response.hasBody()){
                if(response.getBody() instanceof String){
                    throw new RuntimeException("REQUEST 500 ERROR: " + response.getBody());
                }
            }
        }

        if (response.hasBody()) {
            logger.debug("BODY FOUND!");
            try {
                if(logger.isTraceEnabled()){
                    logger.trace("BODY JSON: " + JsonUtils.convertObjectToJson(type.cast(response.getBody())));
                }
            } catch (JsonProcessingException e) {
                logger.error("----------------------------------------------------------------------------------");
                logger.error("JSON CONVERSION PROBLEM: " + e.getMessage());
                logger.error("----------------------------------------------------------------------------------");
            }
            return type.cast(response.getBody());
        }
        return null;
    }

    public static <T> T exchangeObjectPostRequest(String URI, HttpEntity<MultiValueMap<String, Object>> requestEntity, Class<T> type) {
        ResponseEntity<T> response = new RestTemplate().exchange(URI, HttpMethod.POST, requestEntity, type);
        logger.debug("HTTP response STATUS: " + response.getStatusCode());
        if (response.hasBody()) {
            logger.debug("BODY FOUND!");
            return type.cast(response.getBody());
        }
        return null;
    }

    public static HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(java.util.Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));
        return headers;
    }

    public static HttpHeaders getJsonHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public static HttpHeaders getMultipartFormDataHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        return headers;
    }

    public static HttpHeaders getHeadersWithToken(HttpHeaders headers, Oauth2TokenInfo token) {
        logger.debug("getHeadersWithToken()...");
        logger.debug("TOKEN: Authorization: " + token.getToken_type() + " " + token.getAccess_token());
        headers.add("Authorization", token.getToken_type() + " " + token.getAccess_token());
        return headers;
    }

    public static HttpEntity<MultiValueMap<String, Object>> getMultiValueMap(Map<String, Object> map, HttpHeaders httpHeaders) {
        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            data.add(entry.getKey(), entry.getValue());
        }

        return new HttpEntity<>(data, httpHeaders);
    }

    public static void addFilesAsMultiPartFiles(String partname, MultiValueMap<String, Object> body, byte[] fileData, String fileName) {
        body.add(partname, new FileMessageResource(fileData, fileName));
    }
}
