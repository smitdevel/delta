package ee.smit.common.files;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class FileUtil {
    private static final Log log = LogFactory.getLog(FileUtil.class);

    public static String removePathSpecialChars(String name) {
        return name.replace("/", " of ")

                .replace(":", "")
                .replaceAll("[\\<><:/*?|\"']+", " ")
                .replaceAll("[ ]+", " ")
                .replaceAll("[. ]+$", "");
        //.replace("\\", " ")
    }

    /**
     * @param fileName
     * @return
     */
    public static String getFileExt(String fileName) {
        String fn = fileName.toUpperCase();
        return FilenameUtils.getExtension(fn);
    }

    public static byte[] InputStreamToBytes(InputStream is) throws java.io.IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = is.read(buffer)) > -1 ) {
            baos.write(buffer, 0, len);
        }
        baos.flush();

        log.debug("InputStreamToBytes()... bytes: " + baos.toByteArray().length);
        return baos.toByteArray();
    }
}
