package ee.smit.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

    public static Date plusDays(Date date, int days) {
        return addDays(date, days);
    }

    public static Date minusDays(Date date, int days) {
        return addDays(date, -days);
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -days); //minus number would decrement the days
        return cal.getTime();
    }

    public static Date timestampToDate(Long timestamp) {
        Timestamp ts = new Timestamp(timestamp);
        Date date = new Date(ts.getTime());
        return date;
    }

    /**
     * @param dateValue
     * @param DATE_FORMAT_00 "yyyy-MM-dd'T'HH:mm:ss"
     * @return
     */
    public static Date stringToDate(String dateValue, String DATE_FORMAT_00) {
        if (dateValue == null) {
            return null;
        }

        java.util.Locale locale = java.util.Locale.getDefault();
        logger.trace("LOCALE: Country: " + locale.getCountry() + "; Language: " + locale.getLanguage() + "; DATE: " + dateValue + "; FORMAT: " + DATE_FORMAT_00);
        try {
            SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_00, locale);
            return parseStringToDate(dateValue, format);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static Date parseStringToDate(String dateValue, SimpleDateFormat format) {
        if (dateValue == null) {
            return null;
        }

        try {
            return format.parse(dateValue);
        } catch (ParseException e) {
            logger.error("String to Date convert failed! " + e.getMessage(), e);
        }
        return null;
    }
}
