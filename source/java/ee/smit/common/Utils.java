package ee.smit.common;

public final class Utils {
    private Utils() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T castToAnything(Object obj) {
        return (T) obj;
    }
}
