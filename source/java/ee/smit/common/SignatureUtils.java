package ee.smit.common;

import ee.webmedia.alfresco.signature.model.SignatureItem;
import ee.webmedia.alfresco.signature.model.SignatureItemsAndDataItems;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMParser;
import org.apache.commons.codec.binary.Base64;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;

public class SignatureUtils {
    protected final static Log log = LogFactory.getLog(SignatureUtils.class);

    public static boolean isDigidocWithWeakSignatureChain(SignatureItemsAndDataItems items) {
        List<SignatureItem> signatureItems = items.getSignatureItems();

        for (SignatureItem signatureItem : signatureItems) {
            String encrytionType = signatureItem.getEncrytionType();
            log.debug("Signature encryption type: " + encrytionType);
            if (encrytionType.startsWith("SHA-1")) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param certificateInPem
     * @return
     */
    public static X509Certificate getCertificateFromPem(String certificateInPem) throws RuntimeException {
        log.debug("getCertificateFromPem()...");
        if (certificateInPem == null) {
            throw new RuntimeException("Certificate not found!");
        }
        log.debug("CERT length: " + certificateInPem.length());
        X509Certificate cert;
        StringReader reader = null;
        PEMParser pr = null;
        try {
            reader = new StringReader(certificateInPem);
            pr = new PEMParser(reader);
            X509CertificateHolder holder = (X509CertificateHolder) pr.readObject();
            JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
            cert = certConverter.getCertificate(holder);
        } catch (IOException e) {
            log.error("Error reading certificate: " + e.getMessage());
            throw new RuntimeException(e);
        } catch (CertificateException e) {
            log.error("Error reading certificate from holder: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(pr);
            IOUtils.closeQuietly(reader);
        }
        return cert;
    }

    /**
     * @param certificateInHex
     * @return
     */
    public static X509Certificate getCertificateFromHex(String certificateInHex) throws RuntimeException {
        return getCertFromBytes(DatatypeConverter.parseHexBinary(certificateInHex));
    }

    public static X509Certificate getCertFromBash64String(String certificate) throws RuntimeException {
        return getCertFromBytes(Base64.decodeBase64(certificate));
    }

    public static X509Certificate readCertificate(byte[] certificateBytes) throws RuntimeException {
        return getCertFromBytes(certificateBytes);
    }
    public static X509Certificate getCertFromBytes(byte[] certificateBytes) throws RuntimeException {
        InputStream inStream = null;
        try {
            inStream = new ByteArrayInputStream(certificateBytes);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate) cf.generateCertificate(inStream);
            return certificate;
        } catch (CertificateException e) {
            log.error("Error reading certificate: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(inStream);
        }

    }


}
