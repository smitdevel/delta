package ee.smit.users;

public class UnitInfo {
    private Long orgId;
    private Long superUnitId;
    private Long unitId;
    private String name;
    private String email;
    private String unitPath;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getSuperUnitId() {
        return superUnitId;
    }

    public void setSuperUnitId(Long superUnitId) {
        this.superUnitId = superUnitId;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUnitPath() {
        return unitPath;
    }

    public void setUnitPath(String unitPath) {
        this.unitPath = unitPath;
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(orgId, superUnitId, unitId, name, email, unitPath);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class " + this.getClass().getName() + " {\n");

        sb.append("    orgId: ").append(toIndentedString(orgId)).append("\n");
        sb.append("    superUnitId: ").append(toIndentedString(superUnitId)).append("\n");
        sb.append("    unitId: ").append(toIndentedString(unitId)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    email: ").append(toIndentedString(email)).append("\n");
        sb.append("    unitPath: ").append(toIndentedString(unitPath)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
