package ee.smit.tera;

import ee.smit.tera.model.TeraFilesEntry;
import ee.webmedia.alfresco.app.AppConstants;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.log.service.LogService;
import ee.webmedia.alfresco.signature.exception.SignatureException;
import ee.webmedia.alfresco.signature.model.SignatureItem;
import ee.webmedia.alfresco.signature.model.SignatureItemsAndDataItems;
import ee.webmedia.alfresco.signature.service.SignatureService;
import ee.webmedia.alfresco.utils.FilenameUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.auth.AuthenticationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static ee.webmedia.alfresco.common.web.BeanHelper.*;
import static ee.webmedia.alfresco.document.file.model.FileModel.Props.DISPLAY_NAME;


public class TeraServiceImpl implements TeraService {

    private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(TeraServiceImpl.class);

    private JdbcTemplate jdbcTemplate;

    private final FileFolderService fileFolderService = BeanHelper.getFileFolderService();
    private final NodeService nodeService = BeanHelper.getNodeService();
    private final LogService logService = BeanHelper.getLogService();

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private LogEntry getNewLogEntry(){
        LogEntry logEntry = new LogEntry();
        logEntry.setComputerIp("127.0.0.1");
        logEntry.setComputerName("localhost");
        logEntry.setEventDescription("SHA-1 ületembeldamine");
        logEntry.setLevel(LogObject.DOCUMENT.getLevel());
        logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
        logEntry.setCreatorId("IMPORT");
        logEntry.setCreatorName("IMPORT");
        return logEntry;
    }

    public void addFileEntry(String nodeRef, String filename, String filetype, String crypt){
        Connection conn = getConnection();
        try{
            Statement stmt = conn.createStatement();

            String SQL = "INSERT INTO tera_files (filename, filetype, node_ref) VALUES ('"+filename+"', '"+filetype+"', '"+nodeRef+"')";

            int flag = stmt.executeUpdate(SQL);
            log.info("FLAG = {"+flag+"}");
            conn.commit();
            conn.close();
        } catch (Exception e){
            log.error(e.getMessage(), e);
        } finally {
            closeConnection(conn);
        }
    }

    public boolean checkFileEntryByNodeRef(String nodeRef){
        String q = "SELECT count(*) FROM tera_files WHERE node_ref=?";
        log.trace("QUERY: " + q + " :: nodeRef: " + nodeRef);
        int found = this.jdbcTemplate.queryForInt(q, nodeRef);
        log.trace("FOUND NODE_REF-s: " + found);
        return found != 0;
    }

    public int countAllFiles(){
        String q = "SELECT count(*) FROM tera_files;";
        log.trace("QUERY: " + q);
        return this.jdbcTemplate.queryForInt(q);
    }
    public List<TeraFilesEntry> getTeraFilesEntrys(int limit, int offset){
        String baseQuery = "SELECT id, created_date_time, filename, filetype, node_ref, crypt, asics, checked, status_info FROM tera_files WHERE asics=FALSE AND checked=FALSE ORDER By filetype DESC";

        StringBuilder q = new StringBuilder(baseQuery);

        if (limit > 0) {
            q.append(" LIMIT ").append(limit);
        }
        if (offset > 0) {
            q.append(" OFFSET ").append(offset);
        }

        String query = q.toString();
        //log.trace("QUERY: " + query);
        return queryEntries(query, new TeraFilesEntryMapper());
    }

    private <E> List<E> queryEntries(String query, RowMapper<E> rowMapper){
        log.trace("QUERY: " + query);
        List<E> results = this.jdbcTemplate.query(query, rowMapper);
        return results;
    }

    private Connection getConnection(){
        Connection conn = null;
        try {
            conn = jdbcTemplate.getDataSource().getConnection();
            return conn;
        } catch (Exception e){
            log.error(e.getMessage());
        }
        return conn;
    }

    private void closeConnection(Connection conn){
        if(conn!= null){
            try{
                conn.close();
            } catch (Exception e1){ }

        }
    }
    public void updateCryptAndAsics(Long id, String crypt, boolean asics) {
        if (id == null) {
            return;
        }
        Connection conn = getConnection();
        try{

            String sql = "UPDATE tera_files SET created_date_time = ?, crypt = ?, asics = ? WHERE id = ?";
            log.trace("QUERY: " + sql);

            this.jdbcTemplate.update(sql, new Object[] { new Date(), id, crypt, asics });
            conn.commit();
            conn.close();
        } catch (Exception e){

            log.error(e.getMessage(), e);

        } finally {
        }
        closeConnection(conn);
    }

    public void updateTeraFilesRow(String nodeRef, String fileName, String fileType, String crypt, String statusInfo, boolean fileChecked, boolean asicsCreated){
        if(nodeRef == null){
            return;
        }

        String SQL = "UPDATE tera_files SET created_date_time = CURRENT_TIMESTAMP";
        if(fileName != null && !fileName.isEmpty()){
            SQL += ", filename = '" + fileName + "'";
        }

        if(fileType != null && !fileType.isEmpty()){
            SQL += ", filetype = '" + fileType + "'";
        }

        if(crypt != null && !crypt.isEmpty()){
            SQL += ", crypt = '" + crypt + "'";
        }

        if(statusInfo != null && !statusInfo.isEmpty()){
            SQL += ", status_info = '" + statusInfo + "'";
        }

        if(fileChecked){
            SQL += ", checked=TRUE";
        }
        if(asicsCreated){
            SQL += ", asics=TRUE";
        }

        SQL += " WHERE node_ref='"+nodeRef+"'";
        log.trace(SQL);

        Connection conn = getConnection();
        try{

            Statement stmt = conn.createStatement();
            int flag = stmt.executeUpdate(SQL);
            log.info("FLAG = {"+flag+"}");
            conn.commit();
            conn.close();

        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        closeConnection(conn);
    }

    public void updateTeraFilesProcessStatus(String nodeRef, String crypt, boolean asics){
        if(nodeRef == null){
            return;
        }

        String sql = "UPDATE tera_files SET created_date_time = CURRENT_TIMESTAMP, crypt = ?, asics = ? WHERE node_ref = ?";
        log.trace("QUERY: " + sql + "; nodeRef value: " + nodeRef);
        this.jdbcTemplate.update(sql, new Object[] { nodeRef, crypt, asics }, new Object[] {});
    }

    public List<Map<String, Object>> findAllDigidocfiles(){
        String SQL = "SELECT "
                +" concat(alf_store.protocol, '://', alf_store.identifier, '/', alf_node.uuid) AS node_ref, "
                +" public.alf_node_properties.string_value AS filename, "
                +" substring(public.alf_node_properties.string_value from '\\.([^\\.]*)$') as ext "
                +" FROM "
                +"        alf_store "
                +" INNER JOIN alf_node ON (alf_node.store_id = alf_store.id) "
                +" INNER JOIN public.alf_node_properties ON (alf_node.id = public.alf_node_properties.node_id) "
                +" INNER JOIN public.alf_qname ON (public.alf_node_properties.qname_id = public.alf_qname.id) "
                +" WHERE "
                +" alf_node.id IN (SELECT public.alf_node_properties.node_id FROM public.alf_node_properties "
                +" INNER JOIN public.alf_qname ON (public.alf_node_properties.qname_id = public.alf_qname.id) "
                +" WHERE public.alf_node_properties.node_id IN (SELECT public.alf_node_properties.node_id FROM "
                +" public.alf_node_properties INNER JOIN public.alf_qname ON (public.alf_node_properties.qname_id = public.alf_qname.id) "
                +" WHERE (LOWER(public.alf_node_properties.string_value) LIKE '%.ddoc' OR LOWER(public.alf_node_properties.string_value) "
                +" LIKE '%.bdoc') AND public.alf_qname.local_name = 'name') AND public.alf_qname.local_name = 'content') "
                +" AND  public.alf_qname.local_name = 'name'";

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(SQL);

        return rows;
    }

    private class TeraFilesEntryMapper implements ParameterizedRowMapper<TeraFilesEntry> {

        public TeraFilesEntry mapRow(ResultSet rs, int i) throws SQLException {
            TeraFilesEntry entry = new TeraFilesEntry();
            entry.setId(rs.getLong(1));
            entry.setCreatedDateTime(rs.getTimestamp(2));
            entry.setFilename(rs.getString(3));
            entry.setFileype(rs.getString(4));
            entry.setNodeRef(rs.getString(5));
            entry.setCrypt(rs.getString(6));
            entry.setAsics(rs.getBoolean(7));
            entry.setChecked(rs.getBoolean(8));
            entry.setStatusInfo(rs.getString(9));
            return entry;
        }
    }

    public String checkFileNameSymbols(String filename){
        // Not working with Russian and other not latin chars
        filename = FilenameUtil.makeSafeFilename(
                FilenameUtil.replaceApostropheMark(filename)
        );

        /*
        filename = FilenameUtil.limitFileNameLength(
                FilenameUtil.replaceAmpersand(
                        FilenameUtil.trimDotsAndSpaces(
                                FilenameUtil.stripForbiddenWindowsCharactersAndRedundantWhitespaces(
                                        FilenameUtil.replaceApostropheMark(filename)
                                )
                        )
                )
        );*/
        return filename;
    }

    public void fixModifier(NodeService nodeService, FileFolderService fileFolderService, NodeRef nodeRef, String CREATOR_MODIFIER) {
        log.info("fixModifier()...");
        FileInfo fileInfo = fileFolderService.getFileInfo(nodeRef);
        Map<QName, Serializable> properties = fileInfo.getProperties();

        String fileModifier = (String) properties.get(ContentModel.PROP_MODIFIER);
        log.debug("Filemodifier name: " + fileModifier);

        String userFullName = getUserService().getUserFullName(fileModifier);
        log.debug("Filemodifier userfullname: " + userFullName);

        String displayName = (String) properties.get(DISPLAY_NAME);

        /*
        if(displayName == null || displayName.isEmpty() || displayName.equals("null")){
            displayName = fileInfo.getName();
            properties.put(DISPLAY_NAME, displayName);
        }
        */

        properties.put(ContentModel.PROP_MODIFIER, CREATOR_MODIFIER);
        log.debug("CREATOR_MODIFIER: " + properties.get(ContentModel.PROP_MODIFIER));
        log.debug("UPDATE FILE Properties...");
        nodeService.setNodePropertiesBySystem(fileInfo.getNodeRef(), properties, CREATOR_MODIFIER);
        log.debug("UPDATE FILE Properties...Done!");
    }

    public Map<QName, Serializable> renameFile(NodeService nodeService, FileInfo fileInfo, String fileBaseName, String fileExt, String fileType, String CREATOR_MODIFIER) {
        String asicsFilename = fileBaseName + fileExt;
        log.debug("New filename: " + asicsFilename);
        boolean loop = true;
        int i = 0;
        while (loop) {
            if (i > 1000) {
                break;
            }

            if (i > 0) {
                asicsFilename = fileBaseName + "-" + i + fileExt;
                log.debug("New LOOP filename: " + asicsFilename);
            }

            try {

                //FileInfo asicSFileInfo = fileFolderService.rename(fileInfo.getNodeRef(), asicsFilename);

                Map<QName, Serializable> properties = fileInfo.getProperties();
                String displayName = (String) properties.get(DISPLAY_NAME);

                String fileModifier = (String) properties.get(ContentModel.PROP_MODIFIER);
                log.debug("Filemodifier name: " + fileModifier);

                String userFullName = getUserService().getUserFullName(fileModifier);
                log.debug("Filemodifier userfullname: " + userFullName);

                log.info("displayName: " + displayName);
                if(displayName == null || displayName.isEmpty() || displayName.equals("null")){
                    displayName = asicsFilename;
                    log.info("displayName in NULL! Using filename: " + displayName);
                }

                String displayNameBaseName = FilenameUtils.getBaseName(displayName);
                if(displayNameBaseName == null || displayNameBaseName.isEmpty() || displayNameBaseName.equals("null")){
                    displayNameBaseName = asicsFilename;
                    log.info("displayName in NULL! Using filename: " + displayNameBaseName);
                }

                String newDisplayName = displayNameBaseName + fileExt;
                log.info("CURRENT DISPLAY NAME: " + displayName + ". UPDATE TO NEW NAME: " + newDisplayName);

                properties.put(DISPLAY_NAME, newDisplayName);
                properties.put(ContentModel.PROP_NAME, asicsFilename);
                properties.put(ContentModel.PROP_MODIFIER, CREATOR_MODIFIER);
                properties.put(ContentModel.PROP_MODIFIED, new Date());
                //properties.put(FileModel.Props.ACTIVE, true);
                log.debug("CREATOR_MODIFIER: " + properties.get(ContentModel.PROP_MODIFIER));
                log.debug("UPDATE FILE Properties...");
                //nodeService.addProperties(fileInfo.getNodeRef(), properties);
                //for(Map.Entry entry: properties.entrySet()){
                //    log.trace(entry.getKey() + " ==> " + entry.getValue());
                //}
                log.trace("SET NODE PROPERTIES By SYSTEM USER...");
                nodeService.setNodePropertiesBySystem(fileInfo.getNodeRef(), properties, CREATOR_MODIFIER);

                //getNodeProperties(fileInfo.getNodeRef());

                loop = false;
                log.debug("File rename DONE!");

                //FileInfo newFileInfo = fileFolderService.getFileInfo(fileInfo.getNodeRef());

                return properties;
            } catch (Exception e) {
                log.error(e.getMessage());
                i++;
                log.debug("NEW i value: " + i);
            }

        }
        log.info("File rename FAILED!");

        updateTeraFilesRow(fileInfo.getNodeRef().toString(), fileBaseName + "." + fileType.toLowerCase(), fileType, "SHA-1", "FILE RENAME FAILED!", true, false);

        return null;
    }

    public String getAndFixFilename(FileInfo fileInfo){
        String fileName = fileInfo.getName();
        if (fileName != null && !fileName.isEmpty()) {
            fileName = checkFileNameSymbols(fileName);
        }
        return fileName;
    }

    public void checkFileEntris(List<NodeRef> nodeRefs, boolean saveTeraFilesInfo, LogEntry logEntry){
        for(NodeRef nodeRef: nodeRefs){
            checkFileEntry(nodeRef.toString(), saveTeraFilesInfo, logEntry);
        }
    }

    /**
     * Check Digidoc files and put them into ASICS container
     * @param nodeRefStr
     * @param saveTeraFilesInfo
     * @param logEntry
     * @return
     */
    public String checkFileEntry(String nodeRefStr, boolean saveTeraFilesInfo, LogEntry logEntry){
        return checkFileEntry(new NodeRef(nodeRefStr), saveTeraFilesInfo, logEntry);
    }

    /**
     * Check Digidoc files and put them into ASICS container
     * @param nodeRef
     * @param saveTeraFilesInfo
     * @param logEntry
     * @return
     */
    public String checkFileEntry(NodeRef nodeRef, boolean saveTeraFilesInfo, LogEntry logEntry){
        log.info("checkFileEntry()...");
        if(logEntry == null){
            logEntry = getNewLogEntry();
        }


        FileInfo fileInfo = fileFolderService.getFileInfo(nodeRef);

        String contentPath = fileInfo.getContentData().getContentUrl();
        String fileName = getAndFixFilename(fileInfo);

        String fileExt = FilenameUtil.getDigiDocExt(fileName);

        Map<QName, Serializable> properties = fileInfo.getProperties();
        String displayName = (String) properties.get(DISPLAY_NAME);

        log.info("File nodeRef: "+nodeRef+", filename: "+fileName+", displayName: "+ displayName);

        if(saveTeraFilesInfo){
            checkAndAddTeraFiles(nodeRef.toString(), fileName, fileExt, "");
        }

        String statusInfo;

        if (!checkFileContent(fileInfo)) {
            statusInfo = "File check failed! NO DATA FOUND!";
            log.warn(statusInfo);
            if(saveTeraFilesInfo){
                updateTeraFilesRow(nodeRef.toString(), fileName, fileExt, "", statusInfo, true, false);
            }
            return null;
        }

        String processedNodeRef = fileInfo.getNodeRef().toString();

        if (fileExt.equals("DDOC")) {
            processedNodeRef = makeAsics(fileInfo, fileName, fileExt, saveTeraFilesInfo);
            logEntry.setObjectId(processedNodeRef);
            logService.addLogEntry(logEntry);
        } else if (fileExt.equals("BDOC")) {
            //processedNodeRef = checkDigiDocFileAndMakeAsics(fileInfo, fileName, fileExt);
            SignatureItemsAndDataItems items = getDigidocDataItems(nodeRef);
            if (items == null) {
                statusInfo = "Digidoc file parse failed! No ITEMS found!";
                log.warn(statusInfo);
                if(saveTeraFilesInfo){
                    updateTeraFilesRow(nodeRef.toString(), fileName, fileExt, "", statusInfo, true, false);
                }
                return null;
            }
            boolean needsOverstamping = DigidocFileContainsSHA1(items);

            if (needsOverstamping) {
                processedNodeRef = makeAsics(fileInfo, fileName, fileExt, saveTeraFilesInfo);
                logEntry.setObjectId(processedNodeRef);
                logService.addLogEntry(logEntry);
            } else {
                if(saveTeraFilesInfo){
                    updateTeraFilesRow(nodeRef.toString(), fileName, fileExt, "", "No SHA-1 found!", true, false);
                }
            }

        } else if (fileExt.equals("ASICS")) {
            if(saveTeraFilesInfo){
                updateTeraFilesRow(nodeRef.toString(), fileName, fileExt, "", "ASICS already done!", true, true);
            }
            logEntry.setObjectId(processedNodeRef);
            logService.addLogEntry(logEntry);
        } else {
            if(saveTeraFilesInfo){
                updateTeraFilesRow(nodeRef.toString(), fileName, fileExt, "", "Overstamping not needed!", true, false);
            }
        }

        return processedNodeRef;
    }

    private String makeAsics(FileInfo fileInfo, String fileName, String fileExt, boolean saveTeraFilesInfo) {
        log.info("CONTAINER CONTAINS SHA-1 ENCRYPTION AND NEEDS TIMESTAMPING (ASIC-S CONTAINER)!");
        if (getAsicsAndSave(fileInfo, fileName, fileExt, saveTeraFilesInfo)) {
            if(saveTeraFilesInfo){
                updateTeraFilesRow(fileInfo.getNodeRef().toString(), fileName, fileExt, "SHA-1", "", true, true);
            }
        } else {
            log.warn("File not processed to timestamping!");
            return null;
        }
        return fileInfo.getNodeRef().toString();
    }

    private boolean getAsicsAndSave(FileInfo fileInfo, String filename, String fileType, boolean saveTeraFilesInfo) {
        try {

            log.debug("Make ASIC-S container from file: " + filename);
            byte[] asicSContainer = getAsicSContainer(filename, fileInfo.getNodeRef(), fileType, saveTeraFilesInfo);
            if (asicSContainer == null) {
                log.error("MAKING ASIC-S FAILED! NO FILEDATA! continue...");
                return false;
            }

            String fileBaseName = FilenameUtils.getBaseName(filename);
            String fileExt = ".asics";

            log.debug("UPDATING fileinfo ...");

            String CURRENT_USER = "DELTA";

            Map<QName, Serializable> properties = renameFile(nodeService, fileInfo, fileBaseName, fileExt, fileType, CURRENT_USER);
            if (properties == null) {
                log.error("FILE RENAME FAILED!");
            } else {
                log.trace("File properties updated!");
            }

            try {
                writeSignedContainer(fileInfo.getNodeRef(), asicSContainer, CURRENT_USER);
            } catch (Exception e) {
                log.error(e.getMessage());
                if(saveTeraFilesInfo){
                    updateTeraFilesRow(fileInfo.getNodeRef().toString(), filename, fileExt, "SHA-1", "", true, false);
                }
                return false;
            }

            fixModifier(nodeService, fileFolderService, fileInfo.getNodeRef(), CURRENT_USER);

            log.debug("File is updated to ASIC-S....");
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    private byte[] getAsicSContainer(String filename, NodeRef nodeRef, String fileExt, boolean saveTeraFilesInfo) {
        byte[] fileBytes = BeanHelper.getDigiSignSearches().getContent(nodeRef);

        if(fileBytes == null || fileBytes.length == 0){
            String statusInfo = "NO FILE DATA FOUND! NodeRef: " + nodeRef.toString() + ", Filename: " + filename;
            log.info(statusInfo);
            if(saveTeraFilesInfo){
                updateTeraFilesRow(nodeRef.toString(), filename, fileExt, "", statusInfo, true, false);
            }
            return null;
        } else {
            log.info("DATA FOUND! NodeRef: " + nodeRef.toString() + ", Filename: " + filename + ", filesize: " + fileBytes.length + " bytes");
        }

        try {
            log.debug("Make ASIC-S....");
            byte[] out = BeanHelper.getDigiSignSearches().makeAsicS(filename, fileBytes);
            if(out == null) {
                log.debug("Response is NULL!");
            } else {
                log.debug("RESPONSE file size: " + out.length + " bytes");
            }
            return out;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private String getContentPath(NodeRef nodeRef){
        try{
            FileInfo fileInfo = fileFolderService.getFileInfo(nodeRef);
            String contentPath = fileInfo.getContentData().getContentUrl();
            return contentPath;
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return null;
    }


    private byte[] parseJsonObject(JSONObject json) {
        try {
            //String filename = (String) json.get("filename");
            //log.debug("ASIC-S filename: " + filename);
            int statusCode = (int) json.get("statusCode");
            log.trace("REQUEST statusCore: " + statusCode);

            if (statusCode == 400) {
                String status = (String) json.get("status");
                log.error("REQUEST status: " + status);
                return null;
            }
            String filedata = (String) json.get("filedata");
            if (filedata == null || filedata.isEmpty()) {

                log.error("MAKING ASIC-S CONTAINER FAILED! NO RESPONSE FILEDATA!!");

                return null;
            }
            return Base64.decodeBase64(filedata);

        } catch (JSONException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private byte[] parseJsonMap(Object asicSFileObj) {
        Map<String, Object> json = (Map<String, Object>) asicSFileObj;
        String filename = (String) json.get("filename");
        log.debug("ASIC-S filename: " + filename);
        String filedata = (String) json.get("filedata");
        if (filedata == null || filedata.isEmpty()) {
            log.error("MAKING ASIC-S CONTAINER FAILED! NO RESPONSE FILEDATA!!");
            return null;
        }
        return Base64.decodeBase64(filedata);
    }




    private boolean DigidocFileContainsSHA1(SignatureItemsAndDataItems items) {
        List<SignatureItem> signatureItems = items.getSignatureItems();

        for (SignatureItem signatureItem : signatureItems) {
            String encrytionType = signatureItem.getEncrytionType();
            log.debug("Signature encryption type: " + encrytionType);
            if (encrytionType.startsWith("SHA-1")) {
                return true;
            }
        }

        return false;
    }

    private SignatureItemsAndDataItems getDigidocDataItems(NodeRef fileNodeRef) {
        try {
            SignatureItemsAndDataItems items = BeanHelper.getDigiSignSearches().getDataItemsAndSignatureItems(fileNodeRef, true);
            return items;
        } catch (SignatureException ex) {
            log.error("getDigidocDataItems file nodeRef: " + fileNodeRef + " Exception error: " + ex.getMessage(), ex);
        }
        return null;
    }

    private void writeSignedContainer(NodeRef nodeRef, byte[] filedata, String CURRENT_USER) throws IOException {
        log.trace("writeSignedContainer()...fileFolderService.getWriter(): CURRENT_USER: " + CURRENT_USER);
        ContentWriter writer = fileFolderService.getWriter(nodeRef, CURRENT_USER);
        log.trace("writeSignedContainer(): writer.setMimetype()...");
        writer.setMimetype(SignatureService.DIGIDOC_MIMETYPE);
        log.trace("writeSignedContainer(): writer.setEncoding()...");
        writer.setEncoding(AppConstants.CHARSET);
        log.trace("writeSignedContainer(): writer.putContent()...");
        InputStream is = new ByteArrayInputStream(filedata);
        writer.putContent(is);
        IOUtils.closeQuietly(is);
        log.trace("writeSignedContainer()...DONE!");
    }

    private boolean checkFileContent(FileInfo fileInfo) {
        byte[] fileBytes = BeanHelper.getDigiSignSearches().getContent(fileInfo.getNodeRef());
        if (fileBytes == null ) {
            log.error("FILE CONTENTDATA IS NULL!...");
            return false;
        }

        if(fileBytes.length == 0)
        {
            log.error("FILE CONTENTDATA IS 0!...");
            return false;
        }
        //log.debug("FILE contentUrl: " + fileContentData.getContentUrl());
        //log.debug("FILE encoding: " + fileContentData.getEncoding());
        //log.debug("FILE mimetype: " + fileContentData.getMimetype());
        //log.debug("FILE size: " + fileContentData.getSize());

        return true;
    }

    public File timeStampDigidocFiles(File file) throws IOException, AuthenticationException {
        log.debug("timeStampDigidocFiles()... filename: " + file.getName() + "; length: " + file.length());
        return createFile(new ByteArrayInputStream(BeanHelper.getDigiSignSearches().makeAsicS(file)));
    }

    public File timeStampDigidocFiles(File file, String filename) throws IOException, AuthenticationException{
        log.debug("timeStampDigidocFiles()... filename: " + filename + "; length: " + file.length());
        return createFile(new ByteArrayInputStream(BeanHelper.getDigiSignSearches().makeAsicS(filename, FileUtils.readFileToByteArray(file))));
    }

    private File timeStampDigidocFiles(NodeRef nodeRef) throws IOException {
        if(!getDigiSignSearches().isAsicsActive()){
            return null;
        }
        FileInfo fileInfo = fileFolderService.getFileInfo(nodeRef);
        String fileName = getAndFixFilename(fileInfo);
        String fileExt = FilenameUtil.getDigiDocExt(fileName);

        Map<QName, Serializable> properties = fileInfo.getProperties();
        String displayName = (String) properties.get(DISPLAY_NAME);
        log.info("File nodeRef: "+nodeRef+", filename: "+fileName+", displayName: "+ displayName);
        return createFile(new ByteArrayInputStream(getAsicSContainer(fileName, fileInfo.getNodeRef(), fileExt, false)));
    }

    private File createFile(InputStream source) throws IOException {
        final File tempFile = TempFileProvider.createTempFile("alfresco", ".upload");
        FileUtils.copyInputStreamToFile(source, tempFile);
        return tempFile;
    }
    public void checkAndAddTeraFiles(String fileNodeRef, String fileName, String fileExt, String crypt) {
        if (!checkFileEntryByNodeRef(fileNodeRef)) {
            log.trace("Adding fileinfo to tera table...");
            addFileEntry(fileNodeRef, fileName, fileExt, crypt);
        } else {
            log.trace("File is in TERA table...next..");
        }

    }

    public void timestampWeakFilesByNodeRefs(List<Integer> timestampFiles, List<NodeRef> selectedFileNodeRef){
        if(!getDigiSignService().isTimestamptingEnabled()){
            return;
        }
        if(timestampFiles.size() > 0){
            for(Integer fileIndex: timestampFiles){
                try{
                    timeStampDigidocFiles(selectedFileNodeRef.get(fileIndex));
                } catch (IOException e){
                    log.error(e.getMessage(), e);
                }
            }
        }
    }
}
