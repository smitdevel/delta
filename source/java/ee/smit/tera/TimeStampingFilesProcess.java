package ee.smit.tera;

import ee.smit.tera.model.TeraFilesEntry;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogObject;
import ee.webmedia.alfresco.sharepoint.ImportStatus;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

import static ee.webmedia.alfresco.common.web.BeanHelper.*;

public class TimeStampingFilesProcess {
    private static final Log log = LogFactory.getLog(TimeStampingFilesProcess.class);
    private final ImportStatus status;
    private final LogEntry logEntry = new LogEntry();
    private final ProcessSettings processSettings;


    public TimeStampingFilesProcess(ProcessSettings processSettings, ImportStatus status) {
        this.status = status;
        this.processSettings = processSettings;

        logEntry.setComputerIp("127.0.0.1");
        logEntry.setComputerName("localhost");
        logEntry.setEventDescription("SHA-1 ületembeldamine");
        logEntry.setLevel(LogObject.DOCUMENT.getLevel());
        logEntry.setObjectName(LogObject.DOCUMENT.getObjectName());
        logEntry.setCreatorId("IMPORT");
        logEntry.setCreatorName("IMPORT");
    }

    public boolean doBatch() {
        List<String> processedFiles = new ArrayList<>();


        final List<TeraFilesEntry> entries = getTeraService().getTeraFilesEntrys(processSettings.getBatchSize(), 0);

        try {
            processedFiles = getTransactionService().getRetryingTransactionHelper().doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<List<String>>() {
                @Override
                public List<String> execute() throws Throwable {
                    return processFiles(status, entries);
                }
            });
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
        } finally {
            updateTeraFilesListStatus(processedFiles);
        }
        return processedFiles == null || !processedFiles.isEmpty();
    }

    private void updateTeraFilesListStatus(List<String> processedFiles) {
        for (String nodeRef : processedFiles) {
            log.info("UPDATED NODE_REF: " + nodeRef);
            //getTeraService().updateTeraFilesProcessStatus(nodeRef,"SHA-1", false);
        }

    }


    private List<String> processFiles(ImportStatus status, List<TeraFilesEntry> entries) throws IOException {
        if (entries.isEmpty()) {
            log.info("FILES TO PROCESS... 0");
            return Collections.emptyList();
        }

        log.info("FILES TO PROCESS... " + entries.size());

        final List<String> filesProcessed = new ArrayList<>();
        final List<Callable<String>> tasks = new ArrayList<>();


        int threadCount = Math.min(
                Runtime.getRuntime().availableProcessors(),
                entries.size()
        );

        if (getDigiSignService().getMaxThreads() > 0) {
            threadCount = Math.min(getDigiSignService().getMaxThreads(), entries.size());
        }

        log.info("THREAD COUNT: " + threadCount);

        final ExecutorService executor = Executors.newFixedThreadPool(threadCount);

        try {
            for (final TeraFilesEntry entry : entries) {
                tasks.add(
                        new Callable<String>() {
                            @Override
                            public String call() throws Exception {
                                return checkFileEntry(entry);
                            }
                        }
                );
            }

            for (final Future<String> f : executor.invokeAll(tasks/*, timeout? */)) {
                try {
                    // If you added a timeout, you can check f.isCancelled().
                    // If timed out, get() will throw a CancellationException.
                    if (f != null) {
                        filesProcessed.add(f.get());
                    } else {

                    }
                } catch (final ExecutionException e) {
                    // This request failed.  Handle it however you like.
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            executor.shutdown();
        }
        return filesProcessed;
    }

    /**
     * Check Digidoc files and put them into ASICS container
     *
     * @param entry
     * @return
     */
    private String checkFileEntry(TeraFilesEntry entry) {
        return getTeraService().checkFileEntry(entry.getNodeRef(), true, logEntry);
    }
}
