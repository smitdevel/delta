FROM centos:centos6
MAINTAINER lreeder
 
#Helpful utils, but only sudo is required
RUN yum -y install tar
RUN yum -y install vim
RUN yum -y install nc
RUN yum -y install sudo
 
######## JDK7
 #Note that ADD uncompresses this tarball automatically
ADD docker/jdk-7u80-linux-x64.tar.gz /app/java
#WORKDIR /app/java/jdk1.7.0_80
#RUN alternatives --install /usr/bin/java java /app/java/jdk1.7.0_80/bin/java 1
#RUN alternatives --install /usr/bin/jar jar /app/java/jdk1.7.0_80/bin/jar 1
#RUN alternatives --install /usr/bin/javac javac /app/java/jdk1.7.0_80/bin/javac 1
#RUN echo "JAVA_HOME=/app/java/jdk1.7.0_80" >> /etc/environment

RUN mkdir -p /var/run/tomcat
RUN mkdir -p /app/delta

######## TOMCAT
 
#Note that ADD uncompresses this tarball automatically
ADD docker/apache-tomcat-7.0.94.tar.gz /app/tomcat
WORKDIR /app/tomcat/
RUN mv  apache-tomcat-7.0.94 tomcat7
RUN echo "JAVA_HOME=/app/java/jdk1.7.0_80/" >> /etc/default/tomcat7
RUN groupadd tomcat
RUN useradd -s /bin/bash -g tomcat tomcat
RUN chown -Rf tomcat.tomcat /app/tomcat/tomcat7
RUN chmod 775 /app/tomcat/tomcat7/bin/*.sh
RUN ln -s /app/tomcat/tomcat7 /app/tomcat/current

######## Upload contentstore files
ADD docker/smit-test/contentstore.tgz /app/delta
RUN chown -Rf tomcat.tomcat /app/delta
######## COPY CONFS AND LIBS

COPY ./docker/utils/bin/setenv.sh /app/tomcat/tomcat7/bin/
COPY ./docker/utils/lib/log4j.properties /app/tomcat/tomcat7/lib/
COPY ./docker/utils/lib/alfresco-global.properties /app/tomcat/tomcat7/lib/
COPY ./docker/utils/lib/logo.png /app/tomcat/tomcat7/lib/
COPY ./docker/utils/lib/postgresql-42.1.4.jre7.jar /app/tomcat/tomcat7/lib/
COPY ./docker/utils/lib/xroad.properties /app/tomcat/tomcat7/lib/
COPY ./docker/utils/lib/xroad-adit.properties /app/tomcat/tomcat7/lib/
COPY ./docker/utils/lib/xroad-dhl.properties /app/tomcat/tomcat7/lib/

######## COPY APPLICATION
COPY ./build/delta-5.10.11-default-tomcat.war /app/tomcat/tomcat7/webapps/ROOT.war

######## RUN BASH
WORKDIR /usr/share/tomcat7/bin/
EXPOSE 8080
#ENTRYPOINT ["./catalina.sh", "start"]
