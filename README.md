# README #

Dokumendihaldussüsteem DELTA.

### Versioonid ###
------------------------------------------------------------------------------------------
DELTA version: 5.12.81
------------------------------------------------------------------------------------------
* DELTA-1930 Asjatoimikut ümber liigitades, ei muutu asjatoimikus olevate dokumentide metaandmetes "Funktsioon" ning "Sari" andmeväljad
* DELTA-2100 Dokumentide aruande tulemusfaili täiendamine dokumendi URL-ga
* DELTA-2110 Uue klassifikaatori lisamisel Salvesta/Tagasi nupule klikates tuleb süsteemi viga

------------------------------------------------------------------------------------------
DELTA version: 5.12.80
------------------------------------------------------------------------------------------
* DELTA-2103 Iseseisva terviktöövoo infoks edastamisel kandub "Teema" väljale ITTV pealkiri
* DELTA-2108 TTV eelseadistamisel määrates tähtaeg päevades ja lisades TÜ kasutajagrupile, siis väärtustakse TÜ tähtaeg päevades ainult kasutajagrupi esimese liikme jaoks
* DELTA-1614 AK andmete muutmisel põhjuse kuvamine ADRi

  Delta 1614 vajab ka ADR uuendust versioonile 1.4.16

------------------------------------------------------------------------------------------
DELTA version: 5.12.79
------------------------------------------------------------------------------------------
* DELTA-2082 Baaskonfis vale info, mis teatud olukordades põhjustab Deltas probleeme (tuleneb arendusest DELTA-2019)
* DELTA-2097 Peale käsitsi ADR-i süngi käivitamist ei teki enam „Avalikusta“ nuppu (seotud piletiga DELTA-2019)
* DELTA-2101 Logisse tuleb Nullpointer viga
* DELTA-2102 DTTV: "Täitmiseks" TÜ saaja muutub TTV vastutajaks ainult incomingLetteriga seotud TTV-s
* DELTA-2105 TTV eelseadistamisel kasutajagrupile antud TÜ vaade on paigast ära

------------------------------------------------------------------------------------------
DELTA version: 5.12.78
------------------------------------------------------------------------------------------
* Digisign-sing parse V4 support
* In Document in signing block signing date format changed to 24h clock 

------------------------------------------------------------------------------------------
DELTA version: 5.12.77
------------------------------------------------------------------------------------------
* DELTA-2092 Kaasallkirjastaja andmevälja väärtus kandub allkirjastamisel dokumendi metaandmetes Allkirjastaja andmevälja väärtuseks
* DELTA-2094 Pildiga e-kirja malli kasutades enne välja saatmist href kustutamine muudab dokumendi kasutamatuks
* DELTA-2098 Terviktöövoo eelseadistamisel lisades kooskõlastamiseks töövoos tähtaeg päevades, siis TTV salvestamine ei ole võimalik

------------------------------------------------------------------------------------------
DELTA version: 5.12.76
------------------------------------------------------------------------------------------
* DELTA-1517 Tööülesannete tähtaegade arvutamine tööpäevades ja süsteemi poolt automaatselt (RTIP funktsionaalsus)

------------------------------------------------------------------------------------------
DELTA version: 5.12.75
------------------------------------------------------------------------------------------
* DELTA-2087 TTV ekraanivormil klikates "Lisa töövoog" ja valides eelseadistaud TTV, siis tuleb viga HTTP Status 500
* DELTA-2088 Käivitatud TTV-sse lisades allkirjastamiseks TV, siis signerName ei kandu TÜ saaja väljale
* DELTA-2085 fix
* Hotfix: fixed a case where opening a document would throw a NullPointerException
* DELTA-2086 Dokumendi metaandmetes määratud signerName (ja coSignerTwoName) ei kandu allkirjastamiseks töövoo täitjaks

------------------------------------------------------------------------------------------
DELTA version: 5.12.74
------------------------------------------------------------------------------------------
* DELTA-2085 Alusdokumendile lisatud järje/vastuse registreerimisel tuleb süsteemi viga

------------------------------------------------------------------------------------------
DELTA version: 5.12.73
------------------------------------------------------------------------------------------
* DELTA-2083 Kasutajatele saadetakse asendamisega seotud teavitusi valesti

------------------------------------------------------------------------------------------
DELTA version: 5.12.72
------------------------------------------------------------------------------------------
* DELTA-2069 Kui sarja andmetes on määratud JPP Avalik ja JPP alus on ka määratud, siis dokumendil sarja valides tuleb viga
* DELTA-2072 Kustutatud objektide halduses suure hulga dokumentide korraga lõplikult kustutamine ei toimi
* DELTA-2074 Kui sarjas on määratud newNumberForEveryDoc = false ning sarja mustris on sidekriips, siis sidekriipsu järel olevat tähist registreerimisel ei väärtustata
* DELTA-2075 Ümberliigitatud dokumendile järg/vastusdokumendi registreerimisel lisatakse dokumendi viida mustri viimasele tähise väärtusele +1
* DELTA-2077 Täitmiseks tööülesande täitmismärkel ei ole <task.completedDateTime> kuvatud
* DELTA-2078 Uue dokumendi registreerimisel juurdepääsupiiranguga "Avalik", tekivad üleliigsed logikirjed
* DELTA-2079 Juurdepääsupiirangu Kehtiv kuni väärtus kustutatakse kui dokumendi asukohaks määratakse sari, mille andmetes on juurdepääsupiirang Avalik
* DELTA-2080 Uue dokumendi loomisel, kas vaikimisi või ise määratud JPP-ga "Avalik", tekivad üleliigsed logikirjed

------------------------------------------------------------------------------------------
DELTA version: 5.12.71.1
------------------------------------------------------------------------------------------
* hotfix: token cache timetolive set to 4h (14400s)

------------------------------------------------------------------------------------------
DELTA version: 5.12.71
------------------------------------------------------------------------------------------
* hotfix: old groups not deleted when syncUsersGroupsDeletedAfterDays value is 0 or smaller
* TEST e-mail support - replaces all to and bcc e-mail addresses with test e-mail value
* OAuth2 token store support
* AMR-REST ametnikList request - remove unnecessary parameters duplication

------------------------------------------------------------------------------------------
DELTA version: 5.12.70
------------------------------------------------------------------------------------------
* DELTA-2039 Kui peale restarti muuta parameetreid, käivitab Delta "Salvesta" nuppu vajutades süngi ning saadab välja teavitused
* DELTA-2048 Tööülesande täitmise ajal uute tööülesannete lisandumisel saadetud teavitused ei ole rakenduse logis kuvatud
* DELTA-2049 Arvamuse andmiseks TV teavitused ei liigu korrektselt
* DELTA-2050 Tööülesande teostatuks märkimisel ei ole teavituste saatmine korrektne
* DELTA-2067 Sarjas, kus individualizingNumbers = true, ei lisata alus-, vastus- ja järgdokumentide viida lõppu individualiseerivat numbrit õigesti
* DELTA-2068 Kui dokumentide uues asukohas on sarjas määratud individualizingNumbers = true, siis regNumber väärtustamisel individualiseerivaid numbreid ei väärtustata korrektselt
* DELTA-2070 Dokumendi ümberliigitamisel sarja, mille viida mustris on asenduse tähis {DA}, siis seda ei asendata dokumendi registreerimise aasta 2-kohalise väärtusega
* DELTA-2073 Liigitades dokumendid koos järgede ja vastustega ümber sarja, mille seadistus on algse asukoha sarjaga identne, siis uues asukohas ei muutu dokumentide viit korrektselt

------------------------------------------------------------------------------------------
DELTA version: 5.12.69
------------------------------------------------------------------------------------------
* DELTA-2019 Kui Delta ja ADR-i vaheline sünk teha tühjale ADR-ile, siis ei jõua need dokumendid ADR-i, millele on rakendatud adrSkipUpdate = true
* HotFix: IndexBackup integrated with NightlyDatafix job
* AMR userAwayCodes fix
* Added indexLookBackMinutes parameter: Index holes search parameter in minutes. Look back default value 1500 minutes (25h)
* Moved Index backup job in to NightlyDataDix job. That job can schedule by parameter: nightlyDatafixTime. Default value: 00:30
* TEMPORALLY FIX: Changed nightlyDataFixTime param. Reads value from alfresco-global.properties param: job.nightlyDatafixTime.value=00:30

------------------------------------------------------------------------------------------
DELTA version: 5.12.68
------------------------------------------------------------------------------------------
* DELTA-1995 Asutuse kontaktisik peab olema kuvatud andmeväljal recipientPersonName/additionalRecipientName
* DELTA-2059 Sarja sees toimikute vahel dokumente ümber liigitades muutuvad individualiseerivad numbrid valeks
* DELTA-2060 Sarjas, kus individualizingNumbers = true, ei lisata alus-, vastus- ja järgdokumentide viida lõppu individualiseerivat numbrit õigesti
* DELTA-2061 Dokumentide ümberliigitamisel sarjast, kus Individualizing Numbers = true, sarja, kus newNumberForEveryDoc = true/false, eemaldatakse vastus- ja järgdokumentide individualiseerivad numbrid
* DELTA-2062 Kui sarjas on määratud newNumberForEveryDoc = false või individualizingNumbers = true, siis dokumendi registreerimisel registrist tulenev shortRegNumber väärtusekse registri loenduri väärtus +2
* DELTA-2064 Sarjas on määratud newNumberForEveryDoc = true, tõste dokumendi koos vastuste/järgedega sarja sees ühest toimikust teise, siis dokumendi järgede/vastuste viidas shortRegNumber saab registrist uue väärtuse
* DELTA-2065 Kui sarjas on määratud newNumberForEveryDoc=false ja tõsta alusdokument (koos vastuse ja järjega) samas sarjas teise toimikusse, saab alusdokumendi (koos vastuse ja järjega) shortRegNumber registrist uue väärtuse
* DELTA-2066 Dokumentide ümberliigitamisel sarjast, kus Individualizing Numbers = true, sarja, kus pole individualiseerivaid numbreid, lisatakse uue vastus- ja järgdokumendi lõppu individualiseerivad numbrid
* DELTA-2034 parandus

------------------------------------------------------------------------------------------
DELTA version: 5.12.67
------------------------------------------------------------------------------------------
* DELTA-1993 showing document metadata in mDELTA (mobile view)
* DELTA-1994 displaying digicontainer content in mDELTA
* DELTA-1996 An entry must appear in the log about files being moved
* DELTA-1704 Vigase dokumendi/toimiku hävitamise vahele jätmine

------------------------------------------------------------------------------------------
DELTA version: 5.12.66
------------------------------------------------------------------------------------------
* DELTA-1968 Kui Deltas on docName väljale on jäänud reavahetused, siis kuvatakse UAM-is pealkirjas tühje ridu
* DELTA-2020 Klikates ikoonile "Tühjenda väljad" on võimalik delegeerimisel täitja "Nimi" andmeväljale lisada suvalist teksti.
* DELTA-2023 Dokumendi ümberliigitamisel sarja, kus individualizingNumbers = true, lisatakse vastus- ja/või järgdokumentidele registreerimisnumbri lõppu „-<individualNumber>“ väärtuseks „-null“
* DELTA-2034 Parameetri syncUsersGroupsStructUnitsTime väärtuse muutmisel käivitub sünk koheselt peale "Salvesta" nupu vajutamist
* DELTA-2045 Dokumendi "Kehtiv kuni" väli täidetakse vastavalt JP klassifikaatorile, mille kehtivusaeg on klassifikaatorite nimekirjas kõige pikem
* DELTA-2046 Dokumendist koopia loomisel määratakse JPP kehtiv kuni klassifikaatoritest kõige pikema väärtuse järgi
* DELTA-2047 Terviktöövoo vastutaja muutumisel tekivad rakenduse logisse hüperlinkidega lisaread
* DELTA-2053 Dokumendi asukoha muutmisel ei muutu järg- ja vastusdokumentide viit

------------------------------------------------------------------------------------------
DELTA version: 5.12.65
------------------------------------------------------------------------------------------
* DELTA-2055 KUM migratsioon

------------------------------------------------------------------------------------------
DELTA version: 5.12.64
------------------------------------------------------------------------------------------
* AMR-REST Fix: checks user RS status if conf.deltaWithRestrictedAccess=false
* AMR-REST FAKE Auth for RS DELTA
* AMR-REST Fix: RS check
* AMR-REST Removing KasAK param from AMR yksus request
* AMR-REST ametnikList request - set Kvalifikatsioonid from true -> false and set kasKoolitused from true --> false
* AMR-REST User away remove support

------------------------------------------------------------------------------------------
DELTA version: 5.12.63
------------------------------------------------------------------------------------------
* DELTA-1970 UAM-i jaoks Deltast eksporditud XML failis ei ole dokumendil toimiku seost, dokumendi asukohaks (parent) on märgitud ainult sari
* DELTA-1969 Kui dokument kuulub Delta toimikus teema alla, siis ei ekspordita seda dokumenti UAM-i

------------------------------------------------------------------------------------------
DELTA version: 5.12.62
------------------------------------------------------------------------------------------
* DELTA-1704 Skipping documents/files in destruction process
* DELTA-2033 Process of canceling "For information" user tasks cancels and tasks cannot be re-sent to canceling

------------------------------------------------------------------------------------------
DELTA version: 5.12.61
------------------------------------------------------------------------------------------
* DELTA-2003 Kui puudub mall „AK piirangu lõppemine vastutajale.html“, siis teavitust „AK piirangu lõppemine dokumendihaldurile.html“ ei saadeta välja
* DELTA-1981 Asenduste kellaaeg muutub asenduse sorteerimise järel 00:00-ks
* AMR-REST support
* Added parameter for "Nightly Data Fix" process - nightlyDatafixTime. Default time for starting: 00:30
* Get social sec nr from every serial that contains "-". Example: PNOEE-12345678901, PNOLT-12345678911
* DELTA-1997 (FIX) Struktuuriüksusi kuvatakse Delta "Kasutajagruppide haldus" vaates kustutatud staatusega, kui toimub korraga sama nimega üksuse eemaldamine ning lisamine
* Digisign client classes update
* Digisign returns separate signing statuses

------------------------------------------------------------------------------------------
DELTA version: 5.12.60
------------------------------------------------------------------------------------------
* DELTA-2015 DTTV: TTV vastutaja muutmisel ei edastata süsteemseid teavitusi korrektselt

------------------------------------------------------------------------------------------
DELTA version: 5.12.59
------------------------------------------------------------------------------------------
* DELTA-1932 Klikates ikoonil "Määra kõigile sama kuupäev" on võimalik töövoos "Nimi" andmeväljale lisada suvalist teksti.
* DELTA-1960 Dokumendist koopia loomisel määratakse JPP kehtivus koopia loomise kuupäeva alusel
* DELTA-1971 Dokumendi "Kehtiv kuni" väli täidetakse vastavalt JP klassifikaatorile, mille kehtivusaeg on kõige pikem
* DELTA-2029 Avalikustamisel dokumendid menüüpunkti täiendamine veeruga juurdepääsupiirangu alus
* DELTA-2031 Parameetrid salvestuvad "Salvesta" nuppu vajutamata
* DELTA-2035 Tööülesande täitja väljale on käsitsi võimalik sisestada suvalist teksti
* DELTA-2036 Terviktöövoo vaates töövoo plokis kuvatakse töövoo resolutsiooni asemel esimese tööülesande resolutsioon
* DELTA-2040 Vastutuse delegeerimisel kasutajate otsing ei toimi korrektselt
* DELTA-2041 Tööülesande vastutuse üle andmisel ei ole uuel vastutajal võimalik terviktöövooga seotud dokumenti avada
* DELTA-2043 Eelseadistatud terviktöövoo vaates pole võimalik lisada allkirjastamiseks töövoole uut tööülesannet

------------------------------------------------------------------------------------------
DELTA version: 5.12.58.1
------------------------------------------------------------------------------------------
* Revert "DELTA-1942 Outlookist kirja hõlmamise muudatus"

------------------------------------------------------------------------------------------
DELTA version: 5.12.58
------------------------------------------------------------------------------------------
* DELTA-2038 Tööülesannete aruanne jääb "RUNNING" staatusesse
* DELTA-2037 Vastutuse üleandmisel ei kuvata neid dokumente, asjatoimikuid, TV ja TTV, kelle kasutaja profiililt vastutuse üleandmine algatatakse.

------------------------------------------------------------------------------------------
DELTA version: 5.12.57
------------------------------------------------------------------------------------------
* Revert "DELTA-1863"
* Revert "DELTA-1863-1"
* Revert "DELTA-1991"
* Revert "DELTA-1991-1"

------------------------------------------------------------------------------------------
DELTA version: 5.12.56
------------------------------------------------------------------------------------------
* DELTA-1992 Kui eelseadistatud terviktöövoole lisatakse tööülesande täitjaks grupp, siis kaastäitja lisamisel läheb vaade paigast ära
* DELTA-2016 DTTV: Kui conf.casefile.enabled=true, siis sissetuleva kirjaga seotud käivitatud TTV, milles on täitmiseks TV, ei kuvata TTV vastutaja "Minu vastutusel" menüüpunktis
* DELTA-2021 Kiirotsingut tehes ei kuvata otsingutulemusi "Kokku" arvuna korrektselt
* DELTA-2024 Ümberliigitamisel väärtustatakse alusdokumendi „-<individualNumber>“ sama väärtusega, mis on vastus/järgdokumendi suurim individualiseeriva numbri väärtus.
* DELTA-2026 "Ebaõnnestunud saatmised" pealkiri sisaldab sümboleid
* DELTA-2027 Jutumärkide kuvamine tööülesande täitmismärke veerus
* DELTA-2028 Kui sarja andmetes on individualizingNumbers = true, siis vastus/järgdokumendi registreerimisel lisatakse <individualNumber> ette 2 sidekriipsu
* DELTA-2032 Eelseadistatud terviktöövoole uue töövoo lisamisel tekib süsteemi viga

------------------------------------------------------------------------------------------
DELTA version: 5.12.55
------------------------------------------------------------------------------------------
* DELTA-1671 Dora-Delta migratsiooni skripti loomine
* DELTA-1771 TJA-TKA GoPro-Delta andmete migreerimine

------------------------------------------------------------------------------------------
DELTA version: 5.12.54
------------------------------------------------------------------------------------------
* DELTA-1797-3 Sarja seadistuste muutmisel ei jookse registreerimisnumbrite individualiseerivad numbrid õigesti
* DELTA-1991-1 Kui on rakendatud muudatus pealkirjades DVK -> DHX, siis ei saadeta välja teavitust Dokumendi välja saatmine üle DHX-i on katkestatud
* DELTA-1999-1 Vastutuse üle andmisel ei asendu asjatoimiku endine vastutaja toimikus asuvate dokumentide õiguste halduses uue vastutajaga
* DELTA-2010 Terviktöövoo vastuse üle andmisel kaovad dokumendilt õigused
* DELTA-2011 Kui faili pealkirjas on 1'dest koosneb number, millele järgneb punkt, tekib arvamuse andmisel ja kooskõlastamisel süsteemi viga
* DELTA-2014 DTTV: signerName ei kandu korrektselt allkirjastamiseks töövoo täitjaks
* DELTA-2018 Kasutajagrupile edastatud tööülesande täitmismärke ja staatuse kuvamise viga
* DELTA-2022 Töös staatusega dokumendile lisatud fail(id) (pdf, txt, png jpt) laetakse alla, mitte ei avata brauseri vahelehel.

------------------------------------------------------------------------------------------
DELTA version: 5.12.53
------------------------------------------------------------------------------------------
* DELTA-2009 Dokumendi malli lisades tuleb süsteemi viga

------------------------------------------------------------------------------------------
DELTA version: 5.12.52
------------------------------------------------------------------------------------------
* DELTA-2013-1 Jõudlusprobleemid kui conf.myDocumentsMenu.finishedIncomingLettersAreNotShown = false

------------------------------------------------------------------------------------------
DELTA version: 5.12.51
------------------------------------------------------------------------------------------
* DELTA-1912 Üles laaditud faili käivitatakse HTML/JavaScript koodina

------------------------------------------------------------------------------------------
DELTA version: 5.12.50
------------------------------------------------------------------------------------------
* DELTA-2013 Jõudlusprobleemid kui conf.myDocumentsMenu.finishedIncomingLettersAreNotShown = false

------------------------------------------------------------------------------------------
DELTA version: 5.12.49
------------------------------------------------------------------------------------------
* DELTA-1797-2 Sarja seadistuste muutmisel ei jookse registreerimisnumbrite individualiseerivad numbrid õigesti
* DELTA-1985 "Tööülesanded" ploki vaade ei ole korrektne
* DELTA-2005 Kui meiliaadressis on peidetud sümbol, siis dokumendi väljasaatmisel Delta töös/toimimises tekib katkestus

------------------------------------------------------------------------------------------
DELTA version: 5.12.48
------------------------------------------------------------------------------------------
* DELTA-1979 Peale uue andmevälja lisamist tekib varasemalt loodud dokumendist koopia loomisel süsteemi viga
* DELTA-1982 DTTV: incomingLetter-iga seotud "Täitmiseks" TÜ saaja muutub ttv vastutajaks, aga teavitusi ei edastata ttv vastutajale
* DELTA-1988 Minu vastutusel dokumentide arv on ebakorrektne kui konfis on parameeter conf.myDocumentsMenu.finishedIncomingLettersAreNotShown = false ning sissetulev kiri on lõpetatud staatusega
* DELTA-1989 Kui sissetuleva kirjaga on seotud TTV, mille startedDateTime on väärtustatud ja conf.myDocumentsMenu.finishedIncomingLettersAreNotShown = false, siis sissetulev kiri ei kao menüüpunktist Minu vastutusel dokumendid
* DELTA-1991 Kui on rakendatud muudatus pealkirjades DVK -> DHX, siis ei saadeta välja teavitust Dokumendi välja saatmine üle DHX-i on katkestatud
* DELTA-1998 Terviktöövoo vastutuse üle andmisel ei asendu selle endine vastutaja terviktöövooga seotud dokumendi õiguste halduses uue vastutajaga
* DELTA-1999 Vastutuse üle andmisel ei asendu asjatoimiku endine vastutaja toimikus asuvate dokumentide õiguste halduses uue vastutajaga
* DELTA-2004 Sümbolid terviktöövoo tekstides
* DELTA-2006 Dokumendilt, mille metaandmetes on täidetud signerName ja coSignerTwoName, eelseadistatud TTV algatamisel, milles on vaid 1 allkirjastamiseks tööülesanne, tuleb süsteemi viga
* DELTA-2007 Kui dokumendi metaandmetes on täidetud coSignerTwoName, siis kaasallkirjastaja (coSignerTwoName) ei liigu automaatselt TV teise ülesande täitja väljale.
* DELTA-2008 ITTV ja DTTV Deltad ei toimi sarnaselt kui TTV lisada rohkem kui 1 allkirjastamiseks TV

------------------------------------------------------------------------------------------
DELTA version: 5.12.47
------------------------------------------------------------------------------------------
* DELTA-1850 DocLogix-Delta migratsioon (TalTech) täiendused

------------------------------------------------------------------------------------------
DELTA version: 5.12.46
------------------------------------------------------------------------------------------
* DELTA-1870 Jõudlusprobleemid alates versioonist 5.12.7

------------------------------------------------------------------------------------------
DELTA version: 5.12.45
------------------------------------------------------------------------------------------
* DELTA-1032 Vastutusega üle andmise funktsionaalsuse täiendamine
* DELTA-1598 E-kirja malli valimisel peab malli tekst meiliteate sisusse tekkima automaatselt
* DELTA-1797 Sarja seadistuste muutmisel ei jookse registreerimisnumbrite individualiseerivad numbrid õigesti
* DELTA-1816 Tööülesannete otsingutulemustes peab kuvama "Kokku" arvu vastavalt nimekirjas kuvatud tööülesannete arvule
* DELTA-1948 Arhiveeritud toimikut dokumentide loetelu alla tagasi tõstes, ei muudeta toimiku ega dokumentide asukohta
* DELTA-1972 Töös dokumente ei ole võimalik CSV failina eksportida
* DELTA-1975 Parameetri contractDueDateNotificationDays väärtuse eemaldamisel läheb Delta katki
* DELTA-1976 Kui faili pealkirjas on numbrid, komad, tähed ja kirjavahemärk, tekib arvamuse andmisel ja kooskõlastamisel süsteemi viga
* DELTA-1977 Kui faili pealkirjas on numbrid, komad ja tähed, siis täitmismärke all olev faili nimi poolitatakse kahele reale

------------------------------------------------------------------------------------------
DELTA version: 5.12.44
------------------------------------------------------------------------------------------
* DELTA-1990 ui dokumendi liik vacationApplication registreeritakse, siis asenduse lõppemise kuupäeva kellaajaks ei määrata 23:59

------------------------------------------------------------------------------------------
DELTA version: 5.12.43
------------------------------------------------------------------------------------------
* DELTA-1825 Asenduste kellaajaline määramine
* DELTA-869 Dokumendi metaandmetesse tekstiväljale URL-i lisamise võimalus

------------------------------------------------------------------------------------------
DELTA version: 5.12.42
------------------------------------------------------------------------------------------
* DELTA-1987 - .ddoc digikonteineris asuvat faili ei ole võimalik Delta rakendusest

------------------------------------------------------------------------------------------
DELTA version: 5.12.41
------------------------------------------------------------------------------------------
* DELTA-1612 Failide avalikustamise valik Failide plokis

------------------------------------------------------------------------------------------
DELTA version: 5.12.40
------------------------------------------------------------------------------------------
* DELTA-1943 Funktsionaalsuse „Lugemisõiguseta kasutajad näevad dokumentide olemasolu“ seadistuse muutmise logimine
* DELTA-1942 Outlookist kirja hõlmamise muudatus
* DELTA-1941 Konfidentsiaalse struktuuriüksuse funktsionaalsuse laiendamine kasutaja(te)le
* DELTA-1788 Muudatused AK piirangu lõppemise meiliteavituste osas
* DELTA-856 (Kaas)allkirjastaja kandumine dokumendi ekraanivormilt TV-sse

NB! Palun edastada administraatorile juhis seoses haruga DELTA-1788: enne tarne paigaldamist tuleb failis alfresco-global.properties määrata: jobs.enabled=false ning teha Delta projektile restart. Peale uuendamist tuleb määrata parameetri väärtuseks jobs.enabled=true, et piletiga seotud muudatused rakenduksid.

------------------------------------------------------------------------------------------
DELTA version: 5.12.39
------------------------------------------------------------------------------------------
* DELTA-1850 DocLogix-Delta migratsioon (TalTech) 

------------------------------------------------------------------------------------------
DELTA version: 5.12.38
------------------------------------------------------------------------------------------
* DELTA-1597-1 E-kirja malli lisades peab saama määrata, kas e-kirja malli tüübiks on "Saada välja", "Edasta infoks" või "Universaalne"
* DELTA-1785 Probleem "Grupitäitmiseks" TÜ katkestamisega
* DELTA-1863-1 Muudatus pealkirjades: DVK -> DHX

------------------------------------------------------------------------------------------
DELTA version: 5.12.37
------------------------------------------------------------------------------------------
* DELTA-1958 DTTV Töövood "Kooskõlastamiseks" ja "Arvamuse andmiseks" ei tööta kui dokumendi failide plokis on Deltas allkirjastatud fail
* DELTA-1804-3 "Kooskõlastamiseks" ja "Arvamuse andmiseks" TÜ täitmisel lisatud failid kuvatakse TÜ-ga ühel real
* DELTA-1965 "Kooskõlastamiseks" ja "Arvamuse andmiseks" TÜ täitmisel kuvatakse taustainfo faile
* DELTA-1959 Ebakorrektse isikukoodiga kasutaja Deltasse tekkimise tõttu läks AD sünk katki
* DELTA-1681-2 DTTV: incomingLetter-iga seotud "Täitmiseks" TÜ saaja peab muutuma TTV vastutajaks
* DELTA-1861-2 TTV eelseadistamine: võimalus igale täitjale eraldi resolutsioon määrata

------------------------------------------------------------------------------------------
DELTA version: 5.12.36
------------------------------------------------------------------------------------------
* DELTA-1032 	Vastutusega üle andmise funktsionaalsuse täiendamine
* DELTA-1850 DocLogix-Delta migratsioon (TalTech)
* Removing Digidoc service build from ant script  and adding libraries into common/lib folder

------------------------------------------------------------------------------------------
DELTA version: 5.12.35
------------------------------------------------------------------------------------------
* DELTA-1032 	Vastutusega üle andmise funktsionaalsuse täiendamine
* DELTA-1804-2 	"Kooskõlastamiseks" ja "Arvamuse andmiseks" TÜ täitmisel lisatud failid kuvatakse TÜ-ga ühel real
* DELTA-1948	Arhiveeritud toimikut dokumentide loetelu alla tagasi tõstes, ei muudeta toimiku ega dokumentide asukohta
* DELTA-1949-1 	Kui dokumendile on lisatud fail, mille nimes on punkt koos numbritega, siis arvamuse andmiseks tööülesande täitmisel kuvatakse süsteemiviga
* DELTA-1954 	Minu vastutusel dokumendid menüüpunktis ei ole kuvatud dokumendid (Sissetulev kiri) korrektselt kui konfiparameetri conf.myDocumentsMenu.finishedIncomingLettersAreNotShown väärtus on false
* Sünkroniseerimise fix TalTechile

------------------------------------------------------------------------------------------
DELTA version: 5.12.34
------------------------------------------------------------------------------------------
* DELTA-1743-1 Arhiveerimise tõttu toimuvad muudatused ei peaks olema ADR-i sünkroniseeritud

------------------------------------------------------------------------------------------
DELTA version: 5.12.33
------------------------------------------------------------------------------------------
* DELTA-1804 "Kooskõlastamiseks" ja "Arvamuse andmiseks" TÜ täitmisel lisatud failid kuvatakse TÜ-ga ühel real
* DELTA-1875 Kontaktile ei ole võimalik TÜ delegeerida
* DELTA-1934 DTTV: TTV-st tehtud koopia käivitamisel tuleb veateade "Muudatuste salvestamine ebaõnnestus. Dokument on vahepeal kustutatud."
* DELTA-1939 Esimene parameetri muutmine ei salvestu rakenduse logisse
* DELTA-1949 Kui dokumendile on lisatud fail, mille nimes on punkt koos numbritega, siis arvamuse andmiseks tööülesande täitmisel kuvatakse süsteemiviga

------------------------------------------------------------------------------------------
DELTA version: 5.12.32
------------------------------------------------------------------------------------------
* DELTA-1284 Dokumendile järg- ja vastusseost tehes ei tohi süsteem ekraanivormist koopiat tehes kopeerida alusdokumendi malli andmed uuele ekraanivormile
* DELTA-1797 Sarja seadistuste muutmisel ei jookse registreerimisnumbrite individualiseerivad numbrid õigesti
* DELTA-1866 Õiguste halduse loogika muudatus
* DELTA-1950 "Lisa uus arhiveerimistegevuse mall" nuppu ei kuvata menüüpunktis "Mallid"

------------------------------------------------------------------------------------------
DELTA version: 5.12.31
------------------------------------------------------------------------------------------
* DELTA-869 Dokumendi metaandmetesse tekstiväljale URL-i lisamise võimalus
* DELTA-1944 Sissetulevale kirjale vastusdokumendi registreerimise järel ei kuvata alusdokumenti plokis "Minu vastutusel"
* DELTA-1924 Sarja sees toimikute vahel dokumente ümber liigitades muutuvad individualiseerivad numbrid

------------------------------------------------------------------------------------------
DELTA version: 5.12.30
------------------------------------------------------------------------------------------
* DELTA-1682 Allkirjastaja automaatne kandumine dokumendi ekraanivormilt "Allkirjastamiseks" töövoo plokki
* DELTA-1863 Muudatus pealkirjades: DVK -> DHX

------------------------------------------------------------------------------------------
DELTA version: 5.12.29
------------------------------------------------------------------------------------------
* DELTA-1681-1 DTTV: incomingLetter-iga seotud "Täitmiseks" TÜ saaja peab muutuma TTV vastutajaks
* DELTA-1716-2 DHX dokumentide saatmine: kui dokumendi metaandmetes on täidetud nii docName kui docNameAdr, siis edastatakse saajale ainult docNameAdr
* DELTA-1804-1 "Kooskõlastamiseks" ja "Arvamuse andmiseks" TÜ täitmisel lisatud failid kuvatakse TÜ-ga ühel real
* DELTA-1861-1 TTV eelseadistamine: võimalus igale täitjale eraldi resolutsioon määrata
* DELTA-1887-1 Kontakti valimine andmeväljalt - kontakti nimes sisalduvad täpitähed on asendatud sümbolitega
* DELTA-1923-1 Sarja seadistustest kaovad salvestatud asjatoimiku liigid ära

------------------------------------------------------------------------------------------
DELTA version: 5.12.28
------------------------------------------------------------------------------------------
* DELTA-1935 Menüüpunkt "Organistasiooni struktuur" ei avane ning struktuuriüksused kuvatud ebakorrektselt.
struktuuriüksused kuvatud ebakorrektselt - fix

------------------------------------------------------------------------------------------
DELTA version: 5.12.27
------------------------------------------------------------------------------------------
* DELTA-1935 Menüüpunkt "Organistasiooni struktuur" ei avane ning struktuuriüksused kuvatud ebakorrektselt.
Menüüpunkt "Organistasiooni struktuur" - fix

------------------------------------------------------------------------------------------
DELTA version: 5.12.26
------------------------------------------------------------------------------------------
* DELTA-1928 DTTV: Vastusdokumendi registreerimisel/allkirjastamisel tuleb süsteemi viga kui alusdokumendis kaastäitja TÜ on teostamisel.
* DELTA-1870 Jõudlusprobleemid alates versioonist 5.12.7

------------------------------------------------------------------------------------------
DELTA version: 5.12.25
------------------------------------------------------------------------------------------
* DELTA-1677 Dokument edastatakse üle DHX'i edukalt, aga Deltas kuvatakse saatmise staatus "katkestatud"
* DELTA-1716-1 DHX dokumentide saatmine: kui dokumendi metaandmetes on täidetud nii docName kui docNameAdr, siis edastatakse saajale ainult docNameAdr
* DELTA-1803-2 Töös olevate dokumentide korraga lõpetamine
* DELTA-1931 Sarja seadistustest on kadunud newNumberForEveryDoc ja individualizingNumbers märkeruudud

------------------------------------------------------------------------------------------
DELTA version: 5.12.24
------------------------------------------------------------------------------------------
* DELTA-1486-1 Kui töös oleva TTV-ga on seotud dokument, mis arhiveeritakse, läheb TTV katki
* DELTA-1923 Sarja seadistustest kaovad salvestatud asjatoimiku liigid ära
* DELTA-1925 Kooskõlastamiseks tööülesande juures kuvatakse vale tööülesande staatus, kui tööülesanne on peale algatamist edastatud grupile
* DELTA-1926 TÜ "Registreerimine" käivitamisel loodud PDF failid saavad järjekorranumbrid väärtusega 9223372036854775807
* DELTA-1927 Dokumenti kustutades kuvatakse süsteemiviga

------------------------------------------------------------------------------------------
DELTA version: 5.12.23
------------------------------------------------------------------------------------------
* DELTA-1612-5 Failide avalikustamise valik Failide plokis

------------------------------------------------------------------------------------------
DELTA version: 5.12.22
------------------------------------------------------------------------------------------
* DELTA-1888 AD sünk katki

------------------------------------------------------------------------------------------
DELTA version: 5.12.21
------------------------------------------------------------------------------------------
* DELTA-1869 Dora skripti parandus - kohustuslike andmeväljade kontroll

------------------------------------------------------------------------------------------
DELTA version: 5.12.20
------------------------------------------------------------------------------------------
* DELTA-1855 mDeltas võimalik pikendada tööülesannet tühja tähtaja pikendaja lahtriga
* DELTA-1861 TTV eelseadistamine: võimalus igale täitjale eraldi resolutsioon määrata
* DELTA-1875 Kontaktile ei ole võimalik TÜ delegeerida
* DELTA-1881 Toimikute nimekirjas oleva asjatoimiku "Kehtiv kuni" veergu ei täideta validTo väärtusega
* DELTA-1886 "Saada välja" vaates kustub adressaadi nime väljale käsitsi kirjutatud nimi ära
* DELTA-1887 Kontakti valimine andmeväljalt - kontakti nimes sisalduvad täpitähed on asendatud sümbolitega

------------------------------------------------------------------------------------------
DELTA version: 5.12.19
------------------------------------------------------------------------------------------
* DELTA-1612-4 Failide avalikustamise valik Failide plokis

------------------------------------------------------------------------------------------
DELTA version: 5.12.18
------------------------------------------------------------------------------------------
*   DELTA-1486 Kui töös oleva TTV-ga on seotud dokument, mis arhiveeritakse, läheb TTV katki
*	DELTA-1597 E-kirja malli lisades peab saama määrata, kas e-kirja malli tüübiks on "Saada välja", "Edasta infoks" või "Universaalne"
*	DELTA-1598 E-kirja malli valimisel peab malli tekst meiliteate sisusse tekkima automaatselt
*	DELTA-1681 DTTV: incomingLetter-iga seotud "Täitmiseks" TÜ saaja peab muutuma TTV vastutajaks
*	DELTA-1716 DHX dokumentide saatmine: kui dokumendi metaandmetes on täidetud nii docName kui docNameAdr, siis edastatakse saajale ainult docNameAdr
*	DELTA-1787 Kui dokumendile lisatud fail on suurem kui parameetri maxAttachedFilesSize väärtus, siis ei saadeta TÜ saabumise teavitust välja
*	DELTA-1804 "Kooskõlastamiseks" ja "Arvamuse andmiseks" TÜ täitmisel lisatud failid kuvatakse TÜ-ga ühel real

------------------------------------------------------------------------------------------
DELTA version: 5.12.17
------------------------------------------------------------------------------------------
*   DELTA-1612 Failide avalikustamise valik Failide plokis

------------------------------------------------------------------------------------------
DELTA version: 5.12.16.1
------------------------------------------------------------------------------------------
*   CAS login fix

------------------------------------------------------------------------------------------
DELTA version: 5.12.16
------------------------------------------------------------------------------------------
*   DELTA-1612 Failide avalikustamise valik Failide plokis
*   DELTA-1876 - Delta välise konfiguratsiooni parameeter e-Templi kasutuselevõtuks
*   DELTA-1877 - Kinnitamiseks fn täiendamine - dokumendi liigi seadistus
*   Groups sync - when group is removed from the source, then it will be marked in DELTA as deleted. After defined days (default value: 2) it will be removed permanently.
*   UAA authentication update/fix
*   Digisign update

------------------------------------------------------------------------------------------
DELTA version: 5.12.15
------------------------------------------------------------------------------------------
*   DELTA-464-2 Otsingu tulemusel kuvatud nimekirjades (ka nt mallide loetelus) oleks aktiivseks tehtud see rida, mida parasjagu vaadatakse.
*   DELTA-1752-1 "Kinnitamiseks", "Grupitäitmiseks" ja "Otsuse täitmiseks" TÜ teostamise võimalus mDeltas
*   DELTA-1803-1 Töös olevate dokumentide korraga lõpetamine
*   DELTA-1865 Outlookis valesse kausta lohistatud kirja üritatakse korduvalt Deltasse importida, Delta kettamaht suureneb
*   DELTA-1868 Dokumendi välja saatmise vaates on puudu "Saatmisviis" andmeväli

------------------------------------------------------------------------------------------
DELTA version: 5.12.14
------------------------------------------------------------------------------------------
*   GoPro import täiendus
*   DELTA-1825 Asenduste kellaajaline määramine
*   DELTA-1870 Jõudlusprobleemid alates versioonist 5.12.7 (fix: DELTA-1827, DELTA-1828)

------------------------------------------------------------------------------------------
DELTA version: 5.12.13
------------------------------------------------------------------------------------------
*   DELTA-1612 Failide avalikustamise valik Failide plokis
*   DELTA-1724 Ekraanivormi plokk "Tööülesanded" kasutaja poolt seadistatav, kas on vaikimisi avatud või kinni
*   DELTA-1759 Automaatse välja saatmise funktsionaalsuse testimine ja parandused

------------------------------------------------------------------------------------------
DELTA version: 5.12.12
------------------------------------------------------------------------------------------
*   DELTA-1724 Ekraanivormi plokk "Tööülesanded" kasutaja poolt seadistatav, kas on vaikimisi avatud või kinni

------------------------------------------------------------------------------------------
DELTA version: 5.12.11
------------------------------------------------------------------------------------------
*   DELTA-1019-1 Uue töövoo "Dokumendi lõpetamine" lisamine ja töövoo "Registreerimine" täiendamine
*   DELTA-1654-2 Kui dokumendi liigi vormil on mõni andmeväljade gruppi kuuluv andmeväli eraldiseisvana kasutusel, siis dokumendi väljasaatmine ebaõnnestub
*   DELTA-1752 "Kinnitamiseks", "Grupitäitmiseks" ja "Otsuse täitmiseks" TÜ teostamise võimalus mDeltas
*   DELTA-1803 Töös olevate dokumentide korraga lõpetamine
*   DELTA-1851 Terviktöövoo objektide plokis kuvada AK dokumentide puhul viida juures märget [AK]

------------------------------------------------------------------------------------------
DELTA version: 5.12.10
------------------------------------------------------------------------------------------
*   DELTA-1825-1 Asenduste kellaajaline määramine

------------------------------------------------------------------------------------------
DELTA version: 5.12.9
------------------------------------------------------------------------------------------
*   DELTA-1743 Arhiveerimise tõttu toimuvad muudatused ei peaks olema ADR-i sünkroniseeritud
*   DELTA-1824 Ümberliigitamisel ei muutu järg- ja alusdokumentide viit

------------------------------------------------------------------------------------------
DELTA version: 5.12.8
------------------------------------------------------------------------------------------
*   DELTA-1769 GoPro-Delta import: täiendavate kooskõlastuste migreerimine terviktöövoogudeks + staatuse ja teabekandja vaikimisi väärtuse määramine

------------------------------------------------------------------------------------------
DELTA version: 5.12.7
------------------------------------------------------------------------------------------
*   DELTA-1744 Kinnitamiseks tööülesannete nimekirjas peab olema võimalik kinnitada korraga mitut dokumenti
*	DELTA-1825 Asenduste kellaajaline määramine
*	DELTA-1827 Eraldi menüüpunkt, kus kuvatakse avalikustamisele kuuluvad dokumendid
*	DELTA-1828 Dokumentide kohene avalikustamine

------------------------------------------------------------------------------------------
DELTA version: 5.12.6
------------------------------------------------------------------------------------------
*	DELTA-1019 Uue töövoo "Dokumendi lõpetamine" lisamine ja töövoo "Registreerimine" täiendamine
*	DELTA-1371-2 Eelseadistatud terviktöövoo valimise võimalus juba alustatud terviktöövoos
*   DELTA-1594-2 Deltas menüü 'arhivaalide loetellu liigutamist ootavad toimikud' annab süsteemi vea

------------------------------------------------------------------------------------------
DELTA version: 5.12.5
------------------------------------------------------------------------------------------
*	DELTA-1815 Jõudlusprobleemid seoses automaatse väljasaatmisega (DELTA-1680 fix)
*	Node-Browser children info fix
*   Deltas menüü 'arhivaalide loetellu liigutamist ootavad toimikud' annab süsteemi vea

------------------------------------------------------------------------------------------
DELTA version: 5.12.4
------------------------------------------------------------------------------------------
*	DELTA-1654 Kui dokumendi liigi vormil on mõni andmeväljade gruppi kuuluv andmeväli eraldiseisvana kasutusel, siis dokumendi väljasaatmine ebaõnnestub
*	DELTA-1849 Delegeerimisel on võimalik luua ülesanne suvalisele nimele või sõnale, ei kontrolli isiku olemasolu

------------------------------------------------------------------------------------------
DELTA version: 5.12.3
------------------------------------------------------------------------------------------
*	DELTA-677 "Liigita ümber" vaates dokumendi ümberliigitamisel tuleb välja kuvada ka suletud funktsioonid, sarjad, toimikud ja teemad ning dokumente peab saama neisse ümber liigitada
*	DELTA-1792 Tööülesande plokis peab "Näita ajalugu" lingile vajutades avanema modaal-aken
*	DELTA-1799 Faili üleslaadimine ebaõnnestub
*	DELTA-1845 Süsteemiviga dokumendi avamisel, mis on seotud dokumendi töövooga

------------------------------------------------------------------------------------------
DELTA version: 5.12.2
------------------------------------------------------------------------------------------
*	DELTA-1815 Jõudlusprobleemid seoses automaatse väljasaatmisega

------------------------------------------------------------------------------------------
DELTA version: 5.12.1
------------------------------------------------------------------------------------------
*	DELTA-1785 Probleem "Grupitäitmiseks" TÜ katkestamisega
*	DELTA-1817-1 Turvaauk Deltas
*	DELTA-1819 "Saatmata dokumendid" menüü punkti alt ei kao DHXi kaudu edastatud dokumendid ära
*	DELTA-1820 Esimese TÜ täitmismärge kirjutatakse üle

------------------------------------------------------------------------------------------
DELTA version: 5.12.0
------------------------------------------------------------------------------------------
*	DELTA-1821 site.workflow.files plokk MDetla "Täitmiseks" tööülesande vaates
*	DELTA-1834 DELTAsse ei ole võimalik DDOC faile laadida
*	DELTA-1835 UAA tuvastamise toetus
*	DELTA-1837 ASIC-S kontaineri puhul ei kuvata digidoc ikooni
*	DELTA-1838 ASIC-S kontaineri sisu ei kuvata

Lisaparameetrid:
# Default active auth system is CAS.
active.auth.system=CAS
#active.auth.system=UAA
# CAS
cas.casServerUrl=https://localhost/cas/
# UAA
uaa.serverUrl=https://localhost/uaa
uaa.clientId=localdelta
uaa.clientSecret=localdeltasecret


------------------------------------------------------------------------------------------
DELTA version: 5.11.4 
------------------------------------------------------------------------------------------
*	DELTA-1830 Digisign-sign allkirjastamisel eemaldatakse täpitähed. (Needs Digisign-sign 2.2.3)
*	MID signing use v2 requests.
*	DELTA-1745 Teadmiseks tööülesandeid peab olema võimalik katkestada korraga mitmel dokumendil
*	DELTA-965 Süsteem peab vőimaldama otsida dokumente/tööülesandeid. kasutajagruppide/kontaktgruppide järgi

------------------------------------------------------------------------------------------
DELTA version: 5.11.3
------------------------------------------------------------------------------------------
*	DELTA-1822 .bdoc formaadis digikonteineril peab kuvama allkirjastamise aega

------------------------------------------------------------------------------------------
DELTA version: 5.11.2
------------------------------------------------------------------------------------------
*	DELTA-1371-1 - Eelseadistatud terviktöövoo valimise võimalus juba alustatud terviktöövoos
*	DELTA-1611-1 - Asjatoimiku liigi sidumine konkreetse funktsiooni või sarjaga
*	DELTA-1654 - Kui dokumendi liigi vormil on mõni andmeväljade gruppi kuuluv andmeväli eraldiseisvana kasutusel, siis dokumendi väljasaatmine ebaõnnestub
*	DELTA-1816 - Tööülesannete otsingutulemustes peab kuvama "Kokku" arvu vastavalt nimekirjas kuvatud tööülesannete arvule
*	DELTA-1817 - Turvaauk Deltas
*   Revert "DELTA-1792 - Tööülesande plokis peab "Näita ajalugu" lingile vajutades avanema modaal-aken"

------------------------------------------------------------------------------------------
DELTA version: 5.11.1
------------------------------------------------------------------------------------------
*	Inside digidoc file reading fix.

------------------------------------------------------------------------------------------
DELTA version: 5.11.0
------------------------------------------------------------------------------------------
*	Digisign-sign support for signing

------------------------------------------------------------------------------------------
DELTA version: 5.10.46
------------------------------------------------------------------------------------------
*	DELTA-1749 - Lisaarendus dokumentide vahetamise jaoks Delta<-->PRIS vahel
*	DELTA-1792 - Tööülesande plokis peab "Näita ajalugu" lingile vajutades avanema modaal-aken
*	DELTA-1811 - Asjatoimiku ekraanivormil kuvatakse teksti "showAsGroupMember"

------------------------------------------------------------------------------------------
DELTA version: 5.10.45
------------------------------------------------------------------------------------------
*	DELTA-1784 - Tavakasutaja tohib saada kustutada ainult "töös" staatusega registreerimata dokumente
*	DELTA-1795 - Süsteemiviga tööülesande katkestamisel
*	DELTA-1802 - Probleem failis muudatuste salvestamise ja faili versiooni tekkimisega
*	DELTA-1808 - Allkirjastamise tööülesannet on võimalik ilma "Allkirjasta" välja väärtuseta käivitada (kohustuslik väli)

------------------------------------------------------------------------------------------
DELTA version: 5.10.44
------------------------------------------------------------------------------------------
*   Revert "DELTA-1660 - Winmail.dat text encoding fix."

------------------------------------------------------------------------------------------
DELTA version: 5.10.43
------------------------------------------------------------------------------------------
*	DELTA-1588-4 Grupitäitmise ülesande juures kuvada täitmismärge ja staatus ilma, et gruppi lahti teeks
*	DELTA-1794 Kui eelseadistatud TTV-s on "Kuva TTV rippmenüüs" linnutatud ja pealkirjas sisalduvad sümbolid, tekib TTV käivitamisel süsteemi viga
*	DELTA-1795 Süsteemiviga tööülesande katkestamisel
*	DELTA-1799 Faili üleslaadimine ebaõnnestub

------------------------------------------------------------------------------------------
DELTA version: 5.10.42
------------------------------------------------------------------------------------------
*	DELTA-1783 Simi lives kaastäitja väljale täitja lisamise probleem (DELTA-1001 täiendus)

------------------------------------------------------------------------------------------
DELTA version: 5.10.41
------------------------------------------------------------------------------------------
*	DELTA-1759 Automaatse välja saatmise funktsionaalsuse testimine ja parandused

------------------------------------------------------------------------------------------
DELTA version: 5.10.40
------------------------------------------------------------------------------------------
*	DELTA-1660 Winmail.dat text encoding fix
*	DELTA-1793 mDeltas ei saa delegeerimisel lisada kaastäitjaid, arvamuse andjaid, teadmiseks võtjaid
*	DELTA-1367 Dokumendi digiallkirjastamisel ei tohi tekkida kustutatud objektide halduse alla faili ning failide automaatne kustutamine
*	DELTA-1001 (fix) Kasutajat peab olema võimalik otsida ka tööülesande täitja andmeväljalt

------------------------------------------------------------------------------------------
DELTA version: 5.10.39
------------------------------------------------------------------------------------------
*	DELTA-1124-3 Süsteem ei tohi arvestada saatmine plokki tekkinud kirjetega, mis on tekkinud enne dokumendi lõpetamist - konfitav
*	DELTA-1782 Dokumentide nimekirjas ebakorrektne infotekst dokumentide arvu kohta
*	CAS client update

------------------------------------------------------------------------------------------
DELTA version: 5.10.38
------------------------------------------------------------------------------------------
*	DELTA-1145 Arhiveeritud toimikut peab saama tõsta tagasi dokumentide loetelu alla
*	DELTA-1588-3 Grupitäitmise ülesande juures kuvada täitmismärge ja staatus ilma, et gruppi lahti teeks
*	DELTA-1596-2 Dokumendi allkirjastamise käigus resolutsiooni ja rolli lisamise võimalus digikonteinerisse
*	DELTA-1776 Süsteem peab saatma asendajale teavituse, kui asendavata dokumenti ei allkirjastatud
*	DELTA-1780 Viga recipientPersonName andmevälja täitmisel
*	DELTA-1781 Tööülesannete aruande tulemused ebakorrektsed

------------------------------------------------------------------------------------------
DELTA version: 5.10.37
------------------------------------------------------------------------------------------
*	DELTA-1179 Osade sendMode väärtuste puhul ei teki "Saatmine" plokki kirjet

------------------------------------------------------------------------------------------
DELTA version: 5.10.36
------------------------------------------------------------------------------------------
*	DELTA-1510 fix: Document collection sort by access restriction end date fix.

------------------------------------------------------------------------------------------
DELTA version: 5.10.35
------------------------------------------------------------------------------------------
*	DELTA-1596-1 Dokumendi allkirjastamise käigus resolutsiooni ja rolli lisamise võimalus digikonteinerisse
*	DELTA-1611 Asjatoimiku liigi sidumine konkreetse funktsiooni või sarjaga
*	DELTA-1768 Osadele klassifikaatoritele ei saa uusi väärtusi juurde lisada
*	DELTA-1775 ADR-i dokumendi URL-ile ümber suunamise viga

------------------------------------------------------------------------------------------
DELTA version: 5.10.34
------------------------------------------------------------------------------------------
*	DELTA-1292 Delta poolt loodud XMLis olevate failid teisendamine korrektselt Base64 koodiks
*	DELTA-1722 SKA Delta X-tee teenuse dhsAddDocument arendus
*	DELTA-1774 Dora ja GoPro XML impordi skripti parandus - osad toimikud ei avane

------------------------------------------------------------------------------------------
DELTA version: 5.10.33
------------------------------------------------------------------------------------------
*	DELTA-1109 Registreerimisnumbrite individualiseerivad numbrid peavad jooksma järjest ja loogiliselt
*	DELTA-1766 - Kasutaja teekonna raja funktsionaalsus ei tööta korrektselt

------------------------------------------------------------------------------------------
DELTA version: 5.10.32
------------------------------------------------------------------------------------------
*	Kadunud e-postide parandus taski küljes.

------------------------------------------------------------------------------------------
DELTA version: 5.10.31
------------------------------------------------------------------------------------------
*	DELTA-1371 Eelseadistatud terviktöövoo valimise võimalus juba alustatud terviktöövoos
*	DELTA-1588-2 Grupitäitmise ülesande juures kuvada täitmismärge ja staatus ilma, et gruppi lahti teeks
*	DELTA-1757-1 Allkirjastamise tööülesannet on võimalik ilma tüübita käivitada (kohustuslik väli)
*	DELTA-1767 Täitmiseks tööülesande lisamisel on võimalik lisada ainult kaastäitja

------------------------------------------------------------------------------------------
DELTA version: 5.10.30
------------------------------------------------------------------------------------------
*	DELTA-1765 SkaTeenus projekti jaoks dokumentide eksport nodeide sirvijas.
*	DELTA-217 Dokumendile vastusdokumenti registreerides või kui peatäitja täidab oma tööülesande, peavad kaastäitjate tööülesannete staatusteks jääma "teostamisel". Konfigureeritav.

------------------------------------------------------------------------------------------
DELTA version: 5.10.29
------------------------------------------------------------------------------------------
*	DELTA-986-1 - Süsteem peab saatma asendajale asendavata dokumentide kohta teavitused tema algatatud tööülesannete täitmiste kohta
*	DELTA-1501 - Dokumentide nimekirjad vaikides sorditud reg kpv järgi kahanavealt
*	DELTA-1588-1 - Grupitäitmise ülesande juures kuvada täitmismärge ja staatus ilma, et gruppi lahti teeks
*	DELTA-1593 - Asjatoimiku sidumine kasutajatega, asjatoimikute menüüpunkti kuvamine vasakmenüüs
*	DELTA-1596 - Dokumendi allkirjastamise käigus resolutsiooni ja rolli lisamise võimalus digikonteinerisse
*	DELTA-1713 - Tööülesannete aruande tulemusi ei saa vaadata
*	DELTA-1757 - Dokumendi ekraanivormil "Allkirjasta" nupule vajutades tekitatakse allkirjastamise TÜ ilma tüübita (kohustuslik väli)

------------------------------------------------------------------------------------------
DELTA version: 5.10.28
------------------------------------------------------------------------------------------
*	DELTA-1762 - Teavitused meilile on teavituse nimes kirjaviga

------------------------------------------------------------------------------------------
DELTA version: 5.10.27
------------------------------------------------------------------------------------------
*	DELTA-1760 GoPro-Delta impordi skripti täiendus: failide import
*	DELTA-1680 Automaatse väljasaatmise põhifunktsionaalsus
*	DELTA-1018 Teostamisel oleva töövoo peatamisel peab saama muuta tööülesande täitja nime ja tähtaega

------------------------------------------------------------------------------------------
DELTA version: 5.10.26
------------------------------------------------------------------------------------------
*	DELTA-538 Teavituse "Lepingu lõpptähtaja saabumine" funktsionaalsuse muutmine
*	DELTA-986 Süsteem peab saatma asendajale asendavata dokumentide kohta teavitused tema algatatud tööülesannete täitmiste kohta
*	DELTA-1573 ADR-ist dokumentide eemaldamise võimalus (alates ADR 1.4.3)
*	DELTA-1588 Grupitäitmise ülesande juures kuvada täitmismärge ja staatus ilma, et gruppi lahti teeks
*	DELTA-1625-1 Toimikute elukäikude andmetes on märgistatud väli "Märgitud hävitamiseks"
*	DELTA-1730 TTV-s kuvatakse kaks täitja lahtrit, millest üks on tühi
*	DELTA-1746 Toimikut on võimalik salvestada nii, et tüüpi pole valitud
*	DELTA-1751 "Asjatoimiku liigid" menüüpunkti avamisel süsteemiviga

------------------------------------------------------------------------------------------
DELTA version: 5.10.25
------------------------------------------------------------------------------------------
*	DELTA-1729 Dokumendi ekraanivormil "Allkirjasta" nupule vajutades kuvatakse süsteemi viga
*	DELTA-1750 Delta-AMR-i sünk - tühja vastuse korral ei tohi Delta eemaldada kasutajaid

------------------------------------------------------------------------------------------
DELTA version: 5.10.24
------------------------------------------------------------------------------------------
*	DELTA-1741 - Uue tööülesande teavitusse mDelta urli lisamine
*	DELTA-1109 Registreerimisnumbrite individualiseerivad numbrid peavad jooksma järjest ja loogiliselt
*	DELTA-1724 Ekraanivormi plokk "Tööülesanded" kasutaja poolt seadistatav kas on vaikides avatud või kinni

------------------------------------------------------------------------------------------
DELTA version: 5.10.23
------------------------------------------------------------------------------------------
*	DELTA-1740 - mDeltas dokumendi TV vaates tähtaja pikendamine - probleem, kui pikendust küsitakse kelleltki teiselt kui tööülesande andjalt

------------------------------------------------------------------------------------------
DELTA version: 5.10.22
------------------------------------------------------------------------------------------
*	DELTA-1739 Dokumendist koopia tegemisel kandub alusdokumendilt kaasa juurdepääsupiirangu muutmise põhjus

------------------------------------------------------------------------------------------
DELTA version: 5.10.21
------------------------------------------------------------------------------------------
*	DELTA-1590 - Eemaldada tööülesannete valikust „Registreerimine“  ja  töövoo  saaja otsingu võimaluste hulgast  asutusevälise kontakti valiku võimalus
*	DELTA-1663 - "Kuupäeva" tüübiga andmevälja puhul on dokumendi metaandmete täitmisel kalender vaikimisi avatud
*	DELTA-1710 - Terviktöövoogude vaade statistikas
*	DELTA-1728 - Staatuslehe fix - teenuste timeoutid konfigureeritavaks
*	DELTA-1731 - Dokumentide otsingus ja aruannetes ei ole võimalik suletud toimiku alusel otsingut/aruannet teostada
*	DELTA-1734 - Toimikute nimekirja ei kuvata kasvavalt "Toimiku tähis" alusel
*	DELTA-1735 - Sarjast kaovad toimikud ära

------------------------------------------------------------------------------------------
DELTA version: 5.10.20
------------------------------------------------------------------------------------------
*	DELTA-1738 mDeltas dokumendi TV vaates kui kasutaja taotleb tähtaja pikendamist, tekib ülesande saajal süsteemiviga

------------------------------------------------------------------------------------------
DELTA version: 5.10.19
------------------------------------------------------------------------------------------
*	DELTA-1600 Avalikku registrisse väärtuse muutmine migreeritud dokumentidel
*	DELTA-871 Kuupäeva väljadele, üle süsteemi, panna kuupäeva kirjutamise őigsuse kontroll

------------------------------------------------------------------------------------------
DELTA version: 5.10.18
------------------------------------------------------------------------------------------
*	DELTA-1732 mdeltas dokumendi terviktöövoo vaates taustainfo failide kuvamine
*	DELTA-1726 Dora-Delta migratsiooni skripti täiendamine: kontaktid

------------------------------------------------------------------------------------------
DELTA version: 5.10.17
------------------------------------------------------------------------------------------
*	DELTA-1720 - Failde kuvamine mDeltas mitte iseseisvatöövoo korral

------------------------------------------------------------------------------------------
DELTA version: 5.10.16
------------------------------------------------------------------------------------------
*	DELTA-1727 skaneerimise liidese täiendus

------------------------------------------------------------------------------------------
DELTA version: 5.10.15
------------------------------------------------------------------------------------------
*	DELTA-1606 Kasutajagruppide ja kontaktgruppide koosseis peab uuenema automaatselt kui tehakse ekraanivormist koopia
*	DELTA-217 Dokumendile vastusdokumenti registreerides või kui peatäitja täidab oma tööülesande, ei tohi teiste taskide staatused minna "teostamata". Konfigureeritav

------------------------------------------------------------------------------------------
DELTA version: 5.10.14
------------------------------------------------------------------------------------------
*	DELTA-914 Administraatori tegevuste logimine ja kuvamine
*	DELTA-1390 "Lisa töövoog" valikus kuvada Registreerimine kui dokumendi staatus on töös ning kasutaja kuulub kasutajagruppi Dokumendihaldurid
*	DELTA-1572 Terviktöövoo vastutaja muutmine
*	DELTA-1594-1 Dokumendi ja ITTV-s töövoogude kaudu saadavate õiguste muutmine
*	DELTA-1717 TÜ täitmisel kuvatakse põhjuseta veateade
*	DELTA-1718 TTV objekti puudumisel ITTV käivitamine ebaõnnestub

	alfresco-gobal.properties file new params:
	
	conf.registerTaskAllowed=true/false		[DELTA-1390]
	
------------------------------------------------------------------------------------------
DELTA version: 5.10.13
------------------------------------------------------------------------------------------
*	DELTA-217 Dokumendile vastusdokumenti registreerides või kui peatäitja täidab oma tööülesande, peavad kaastäitjate tööülesannete staatusteks jääma "teostamisel". Konfigureeritav.
*	DELTA-1606 Kasutajagruppide ja kontaktgruppide koosseis peab uuenema automaatselt kui tehakse ekraanivormist koopia
*	DELTA-1693 luua süsteemne andmeväli-klassifikaator deliveryType
*	DELTA-1709 Dokumendi liigi ja sarja automaatse väljasaatmise seadistamine

------------------------------------------------------------------------------------------
DELTA version: 5.10.12
------------------------------------------------------------------------------------------
*	DELTA-1721 viga GoPro impordi skriptis

------------------------------------------------------------------------------------------
DELTA version: 5.10.11
------------------------------------------------------------------------------------------
*	DELTA-457 Minu vastutusel dokumendid, asjatoimikud menüüs peaks olema veerg "loomise kuupäev" 
*	DELTA-981 AK piirangu lőppemise teavitustest vőtta välja dokumendi liik "Sissetulev kiri".
*	DELTA-1501 Dokumentide nimekirjad vaikides sorditud reg kpv järgi kahanavealt
*	DELTA-1578 Töövoost koopia tegemisel peab süsteem kopeerima ka resolutsiooni
*	DELTA-1594 Dokumendi ja ITTV-s töövoogude kaudu saadavate õiguste muutmine

------------------------------------------------------------------------------------------
DELTA version: 5.10.10
------------------------------------------------------------------------------------------
*	DELTA-1714 mDeltas dokumendi terviktöövoo vaates mobiil-idga allkirjastamisel süsteemi viga SIM haldusalas
*	DELTA-1715 DVK custom fields tugi

------------------------------------------------------------------------------------------
DELTA version: 5.10.9
------------------------------------------------------------------------------------------
*	DELTA-464-1 Otsingu tulemusel kuvatud nimekirjades (ka nt mallide loetelus) oleks aktiivseks tehtud see rida, mida parasjagu vaadatakse.
*	DELTA-1698 Sarja andmetest ei kandu JP alus dokumendile üle
*	DELTA-1705 Dokumendile asukohta valides kuvatakse nimekirjas ka suletud toimikuid
*	DELTA-1707 Terviktöövoo käivitamine ebaõnnestub

------------------------------------------------------------------------------------------
DELTA version: 5.10.8
------------------------------------------------------------------------------------------
*	DELTA-1001 (fix 6)

------------------------------------------------------------------------------------------
DELTA version: 5.10.7
------------------------------------------------------------------------------------------
*	DELTA-1703 Paketi serveri juur kataloogi paigaldamisel contect pathi viga mDeltas (tekib topelt /m/m/).

------------------------------------------------------------------------------------------
DELTA version: 5.10.6
------------------------------------------------------------------------------------------
*	DELTA-1702 Dora ja GoPro XML impordi skripti parandus - kasutajanime otsing.

------------------------------------------------------------------------------------------
DELTA version: 5.10.5
------------------------------------------------------------------------------------------
*	DELTA-1701 - mDelta avamisel süsteemi viga.

------------------------------------------------------------------------------------------
DELTA version: 5.10.4
------------------------------------------------------------------------------------------
*	DELTA-1700 - ITTV salvestamisel tuleb süsteemi viga.

------------------------------------------------------------------------------------------
DELTA version: 5.10.3 (TEST)
------------------------------------------------------------------------------------------
*	DELTA-1699 - Lisada konfiparameeter mobiilse keskkonna peale suunamise väljalülitamine kasutades telefoni või tahvlit.

------------------------------------------------------------------------------------------
DELTA version: 5.10.2 RC5
------------------------------------------------------------------------------------------
*	DELTA-1001-5 Kasutajat peab olema võimalik otsida ka tööülesande täitja andmeväljalt
*	DELTA-1662-1 Dokumendi töövoog ei tööta mDeltas
*	DELTA-1673-1 „Ei kinnita“; "Ei allkirjasta"; "Ei kooskõlasta" töövoo täitmisviisid ei tööta
*	DELTA-1677 Dokument edastatakse üle DHX'i edukalt, aga Deltas kuvatakse saatmise staatus "katkestatud"
*	DELTA-1688 Sitesope monitooring annab erroreid, kuigi tegelikult on Deltadega kõik korras
*	DELTA-1691 Terviktöövoo koopia "Salvesta" nupp tekitab tehnilse vea
*	DELTA-1692 Enne lõpetamist välja saadetud dokumentide eemaldamine "Saatmata dokumendid" alt
*	DELTA-1696 Faili avamisel tekib brauserisse tühi vaheleht
*	DELTA-1697 Uuendused/parendused Nodeide sirvijasse

------------------------------------------------------------------------------------------
DELTA version: 5.10.2 RC4
------------------------------------------------------------------------------------------
*	DELTA-1001-4 Kasutajat peab olema võimalik otsida ka tööülesande täitja andmeväljalt
*	DELTA-1349 Lisada uus süsteemne andmeväli "Krüpteerimine"
*	DELTA-1592 Otsingutulemuste kuvamine ka koguarvuna,  nt: „Tööülesannete otsingu tulemused : 124“
*	DELTA-1656-2 SKA Delta mahukate toimikutega seotud tegevuste kiiruse parandamine
*	DELTA-1668 Dokumente ei ole võimalik "Saatmisel dokumendid" alt eemaldada
*	DELTA-1673 „Ei kinnita“; "Ei allkirjasta"; "Ei kooskõlasta" töövoo täitmisviisid ei tööta
*	DELTA-1674 Õiguste halduse funktsionaalsus statistika vaates

------------------------------------------------------------------------------------------
DELTA version: 5.10.2 RC3
------------------------------------------------------------------------------------------
*	DELTA-1664 Maavalitsuste arhiveerimise probleemi lahendamine.
*	DELTA-1018 Teostamisel oleva töövoo peatamisel peab saama muuta tööülesande täitja nime ja tähtaega. Parandatud probleem: Meiliteavitust "Tööülesande andmeid on muudetud" ei saadetud, kui kasutaja muutis tööülesande andmeid ja vajutas „Jätka“ nuppu. Paranduse tulemusena saadetakse meiliteavitus nii "Salvesta" kui ka "Jätka" nuppude vajutamise tulemusena, kui tööülesande andmed on muutunud.

------------------------------------------------------------------------------------------
DELTA version: 5.10.2 RC2
------------------------------------------------------------------------------------------
*	DELTA-768-fix Failid ja taustainfo failide plokis peab saama mitut faili korraga panna taustainfo failide alla ja vastupidi ning kustutada.
*	DELTA-1124-1 Süsteem ei tohi arvestada saatmine plokki tekkinud kirjetega, mis on tekkinud enne dokumendi LÕPETAMIST (registreerimise asemel)
*	DELTA-1671 Sotsiaalministreeriumi Dora-Delta migratsiooni skripti loomine.

------------------------------------------------------------------------------------------
DELTA version: 5.10.2 RC1
------------------------------------------------------------------------------------------
*	DELTA-464 Otsingu tulemusel kuvatud nimekirjades (ka nt mallide loetelus) oleks aktiivseks tehtud see rida, mida parasjagu vaadatakse.
*	DELTA-448 Delta otsing "sõnaühendi" järgi
*	DELTA-682 Sarja andmetest eemaldada juurdepääsupiirangu kehtiv alates ja kuni väljad.
*	DELTA-1001 fix3 Kasutajat peab olema võimalik otsida ka tööülesande täitja andmeväljalt
*	DELTA-1003 Arvamuse andmiseks töövoog peab andma dokumendi töövoos töövoo täitjale faili muutmise őiguse
*	DELTA-1007 Teadmiseks ja kinnitamiseks töövoog peab andma dokumendi töövoos automaatselt dokumendi muutmise õiguse
*	DELTA-1417 Iseseisvas terviktöövoos ja dokumendi töövoos paralleelne allkirjastamine
*	DELTA-1644 ADR-is kuvatakse kõik failid PDF formaadis, kuid peab kuvama nii nagu Deltas failide plokis
*	DELTA-1656 SKA Delta mahukate toimikutega seotud tegevuste kiiruse parandamine
*	DELTA-1658 Chrome'i tugi

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RELEASE - 14.06.2019
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC13
------------------------------------------------------------------------------------------
*	DELTA-1666 Statistika tööülesannete ja dokumentide kohta

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC12
------------------------------------------------------------------------------------------
*	RAHMIN fix3 arhiveerimis probleemid

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC11
------------------------------------------------------------------------------------------
*	DELTA-1018 Teostamisel oleva töövoo peatamisel peab saama muuta tööülesande täitja nime ja tähtaega ning saatma teavituse taski andmete muutmisest
*	DELTA-987 Kooskőlastaja peab saama lisada tööülesande täitmise plokki faili/faile ja neid sealt kustutada. Failid kuvada "Kooskőlastusmärked" plokis
*	DELTA-1124 Vol 2 Süsteem ei tohi arvestada saatmine plokki tekkinud kirjetega, mis on tekkinud enne dokumendi esmakordset lõpetamist

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC10
------------------------------------------------------------------------------------------
*	DELTA-1659 Dokumendi vaatest ITTV algatamisel pole terviktöövoo objekti automaatselt lisatud
*	DELTA-1001 fix2

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC9
------------------------------------------------------------------------------------------
*	DELTA-1365 fix - restoring lost params.
*	Admin code fix.
*	Rahmin arhiveerimis vea parandus: Integrity violations

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC8
------------------------------------------------------------------------------------------
*	DELTA-1655 Vastuseid ja järgesid saab salvestada suletud toimikutesse metaandmete vaates
*	DELTA-1650 tavakasutaja ei saa metaandmeid salvestada kui teema valimine on kohustuslik
*	DELTA-1643 (update) Digiallkirjastamisel lõpetatakse dokumenti igal juhul, ei arvestata 'ei' väärtusega dokumendi liigi seadistuse 'Dok lõpetatakse registreerimisel' juures
*	DELTA-1366 Rakenduse logikirjete otsingusse lisada otsing isikukoodi järgi

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC7
------------------------------------------------------------------------------------------
*	DELTA-1645 probleem asendaja lisamisega (updated)
*	DELTA-1365 Süsteemi rakenduse logisse lisada dokumendi pealkiri, viit ning link
*	DELTA-1652 Delta-AMR-i sünk on katki
*	Digisign-crypt separate service support.
	
	alfresco-gobal.properties file new params:
	
	digisign.crypt.service.url=https://digisign-crypt.smit.ee
	digisign.crypt.service.client.id=[digisign-crypt id name]
	digisign.crypt.service.client.secure=[digisign-crypt secure code]
	digisign.crypt.service.appname=[registred application username]
	digisign.crypt.service.apppass=[registred application password]
	digisign.crypt.service.active=true/false
	
------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC6
------------------------------------------------------------------------------------------
*	DELTA-1645 probleem asendaja lisamisega
*	Digisign date convert change
*	DELTA paigaldusjuhend update

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC5
------------------------------------------------------------------------------------------
*	1001 mesting fix

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC4 - hotfix
------------------------------------------------------------------------------------------
*	DELTA-1566 - Probleemid meilide lohistamisega

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC3
------------------------------------------------------------------------------------------
*	rahmin-fix1
*	DELTA-1642 Saatmise plokis ei kuvata saatmisviisi "riigiportaal eesti.ee" puhul välja saadetavate failide nimesid
*	DELTA-1547 Süsteem peab kontrollima sissetuleva kirja täitmise tähtaega vastu loodava vastus-/järgikirja tööülesannete tähtaegu
*	DELTA-974 Teema välja otsing peab otsima ka teema peakirja seest ja teemade nimekirjadesse lisada otsingu väli
*	DELTA-768 Failid ja taustainfo failide plokis peab saama mitut faili korraga panna taustainfo failide alla ja vastupidi ning kustutada.

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC2
------------------------------------------------------------------------------------------
*	DELTA-1430 fix - Iseseisvas terviktöövoos peab saama siduda dokumendi liiki konkreetsete töövoogudega

------------------------------------------------------------------------------------------
DELTA version: 5.10.1 RC1
------------------------------------------------------------------------------------------
*	DELTA-1001 - Kasutajat peab olema võimalik otsida ka tööülesande täitja andmeväljalt
*	DELTA-1430 - Iseseisvas terviktöövoos peab saama siduda dokumendi liiki konkreetsete töövoogudega
*	DELTA-1569 - Teatud tingimusel ei kuva menüüs "Dokumendi otsing" välja ikooni "Lisa otsetee"
*	DELTA-1625 - Toimikute elukäikude andmetes on märgistatud väli "Märgitud hävitamiseks"
*	DELTA-1637 - Teadmiseks TÜ edastamisel tähtaja lahtri täitmise kontroll ebakorrektne
*	DELTA-1639 - Päästeameti Deltas lühimenetluse otsuse vormil tekkib "Saada välja" nupu vajutusel süsteemi viga

------------------------------------------------------------------------------------------
DELTA version: 5.10.0 RELEASE - 14.02.2019
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
DELTA version: 5.10.0 RC2
------------------------------------------------------------------------------------------
Added fix:
*	DELTA-982 Luua uus süsteemne teavitus, mille saadab süsteem siis, kui töövoog peatub

------------------------------------------------------------------------------------------
DELTA version: 5.10.0 RC1
------------------------------------------------------------------------------------------
Added ADIT-adapter 1.0.0 support (https://bitbucket.org/smitdevel/adit-adapter/wiki/Home)
Deploys as jar.

Add alfresco-gobal.properties NEW params (if not exists):
x-tee.institution=[Reg.Nr]
adit.service.url=https://adit-adapter.develop:8090
adit.service.active=false|true

------------------------------------------------------------------------------------------
DELTA version: 5.9.3 RC2
------------------------------------------------------------------------------------------
*	PERH parandus, kontrollitakse kas kasutaja on olemas deltas või mitte

------------------------------------------------------------------------------------------
DELTA version: 5.9.3 RC1
------------------------------------------------------------------------------------------
*	DELTA-1548 Menüü "Dokumendid", "Terviktöövood", "Asjatoimikud" veergude muudatus: veerus "Töövoo seis" tööülesande täitja nime järel kuvada ka TÜ tähtaeg + kuvada pealkiri pikemalt
*	DELTA-1541 Veerus "Töövoo seis" kuvada staatuseks "Peatatud" kui töövoog peatub
*	DELTA-1633
*	DELTA-1554
*	DELTA-1279 Digidoc Client konteineri sees oleva faili avamise logimine
*	DELTA-1356_fix
*	DELTA-1356_fix
*	DELTA-1515
*	DELTA-1516

* Correcting javadoc.
* Adding adit param: x-tee.adit.infosystem=TEST|DELTA
* Updating bcpkix-jdk15on-1.45.jar ==> 1.58

*	PERH arendus

P.S. alfresco-gobal.properties failis PERH puhul (teiste ldap sünk omadusega) on vaja lisada:
oracle.db.name=
oracle.db.username=
oracle.db.password=
oracle.db.host=

oracle.db.port=
oracle.db.driver=oracle.jdbc.OracleDriver

oracle.db.url=jdbc:oracle:thin:@${oracle.db.host}:${oracle.db.port}:${oracle.db.name}

orgstruct.fromdatabase=true

------------------------------------------------------------------------------------------
DELTA version: 5.9.0
------------------------------------------------------------------------------------------
corrected branch

------------------------------------------------------------------------------------------
DELTA version: 5.2.3.184.1
------------------------------------------------------------------------------------------

* CDOC crypting fix: Added support for new ID-Card sertificate - KeyUsage change.

------------------------------------------------------------------------------------------
DELTA version: 5.2.3.184
------------------------------------------------------------------------------------------
* DELTA-1472 Asutusele saadetud dokumendid jäävad osaliselt DVK-sse rippuma ja neid ei laeta DELTA-sse.
