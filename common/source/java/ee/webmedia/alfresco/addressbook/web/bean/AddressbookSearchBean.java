package ee.webmedia.alfresco.addressbook.web.bean;

import static ee.webmedia.alfresco.addressbook.util.AddressbookUtil.transformAddressbookNodesToSelectItems;
import static ee.webmedia.alfresco.common.web.BeanHelper.getAddressbookService;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.model.SelectItem;

import ee.webmedia.alfresco.common.web.UserContactGroupSearchFilters;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.component.PickerSearchParams;

import ee.webmedia.alfresco.addressbook.model.AddressbookModel;
import ee.webmedia.alfresco.addressbook.util.AddressbookUtil;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.common.web.UserContactGroupSearchBean;

public class AddressbookSearchBean {

    public static final String BEAN_NAME = "AddressbookSearchBean";

    /**
     * Query callback method executed by the Generic Picker component.
     * This method is part of the contract to the Generic Picker, it is up to the backing bean
     * to execute whatever query is appropriate and return the results.
     * 
     * @param params Search parameters
     * @return An array of SelectItem objects containing the results to display in the picker.
     */
    public SelectItem[] searchContacts(PickerSearchParams params) {
        Integer filter = params.isIncludeFilterIndex() ? UserContactGroupSearchFilters.CONTACTS_FILTER.getId() : null;
        return transformAddressbookNodesToSelectItems(getAddressbookService().search(params.getSearchString(), params.getLimit()), filter);
    }

    /**
     * Query callback method executed by the Generic Picker component.
     * This method is part of the contract to the Generic Picker, it is up to the backing bean
     * to execute whatever query is appropriate and return the results.
     * 
     * @param params Search parameters
     * @return An array of SelectItem objects containing the results to display in the picker.
     */
    public SelectItem[] searchOrgContacts(PickerSearchParams params) {
        Integer filter = params.isIncludeFilterIndex() ? UserContactGroupSearchFilters.CONTACTS_FILTER.getId() : null;
        return transformAddressbookNodesToSelectItems(getAddressbookService().searchOrgContacts(params.getSearchString(), params.getLimit()), filter);
    }

    /**
     * Query callback method executed by the Generic Picker component.
     * This method is part of the contract to the Generic Picker, it is up to the backing bean
     * to execute whatever query is appropriate and return the results.
     * 
     * @param params Search parameters
     * @return An array of SelectItem objects containing the results to display in the picker.
     */
    public SelectItem[] searchContactGroups(PickerSearchParams params) {
        Integer filter = params.isIncludeFilterIndex() ? UserContactGroupSearchFilters.CONTACT_GROUPS_FILTER.getId() : null;
        return transformAddressbookNodesToSelectItems(getAddressbookService().searchContactGroups(params.getSearchString(), true, false, params.getLimit()), filter);
    }
    
    public SelectItem[] searchTaskCapableContactGroups(PickerSearchParams params) {
        Integer filter = params.isIncludeFilterIndex() ? UserContactGroupSearchFilters.CONTACT_GROUPS_FILTER.getId() : null;
        List<Node> nodes = getAddressbookService().searchContactGroups(params.getSearchString(), true, false, params.getLimit());
        return transformAddressbookNodesToSelectItems(filterTaskCapable(nodes), filter);
    }
    
    private List<Node> filterTaskCapable(List<Node> nodes){
    	List<Node> filteredNodes = new ArrayList<Node>();
    	for(Node node : nodes){
    		Boolean taskCapable = (Boolean) node.getProperties().get(AddressbookModel.Props.TASK_CAPABLE);
    		if(taskCapable){
    			filteredNodes.add(node);
    		}
    	}
    	return filteredNodes;
    }

    public List<String> getContactData(String nodeRef) {
        return AddressbookUtil.getContactData(nodeRef);
    }

    public boolean isUserHasPermission() {
        return getAddressbookService().hasManagePermission();
    }

    public void setupViewEntry(String refString) {
        NodeRef contactRef = new NodeRef(refString);
        FacesContext context = FacesContext.getCurrentInstance();
        QName type = BeanHelper.getNodeService().getType(contactRef);
        String callback;
        if (AddressbookModel.Types.ORGANIZATION.equals(type)) {
            callback = "AddressbookOrgDetailsDialog.setupViewEntry";
        } else {
            callback = "AddressbookPersonDetailsDialog.setupViewEntry";
        }
        MethodBinding b = context.getApplication().createMethodBinding("#{" + callback + "}", new Class[] { String.class });
        b.invoke(context, new Object[] { refString });
    }
}
