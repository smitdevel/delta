package ee.webmedia.alfresco.signature.model;

import ee.smit.digisign.sign.model.certificates.timestamp.TimeStampTokenInfo;
import ee.smit.digisign.sign.model.certificates.timestamp.TimeStampTokenShort;

import java.io.Serializable;
import java.util.List;

public class SignatureItemsAndDataItems implements Serializable {
    private static final long serialVersionUID = 1L;

    private final List<SignatureItem> signatureItems;
    private final List<DataItem> dataItems;
    private final List<TimeStampTokenShort> timeStampTokens;

    public SignatureItemsAndDataItems(List<SignatureItem> signatureItems, List<DataItem> dataItems) {
        this(signatureItems, dataItems, null);
    }

    public SignatureItemsAndDataItems(List<SignatureItem> signatureItems, List<DataItem> dataItems, List<TimeStampTokenShort> timeStampTokens) {
        this.signatureItems = signatureItems;
        this.dataItems = dataItems;
        this.timeStampTokens = timeStampTokens;
    }
    public List<SignatureItem> getSignatureItems() {
        return signatureItems;
    }

    public List<DataItem> getDataItems() {
        return dataItems;
    }

    public List<TimeStampTokenShort> getTimeStampTokens() {
        return timeStampTokens;
    }
}
