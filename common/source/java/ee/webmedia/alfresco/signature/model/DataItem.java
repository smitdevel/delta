package ee.webmedia.alfresco.signature.model;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.document.file.model.FileModel;
import ee.webmedia.alfresco.signature.exception.SignatureException;
import ee.webmedia.alfresco.signature.servlet.DownloadDigiDocContentServlet;
import org.alfresco.repo.web.scripts.FileTypeImageUtils;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.digidoc4j.DataFile;
import org.digidoc4j.exceptions.DigiDoc4JException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class DataItem implements Serializable {
    protected final static Log log = LogFactory.getLog(DataItem.class);
    private static final FastDateFormat simpleDateFormat = FastDateFormat.getInstance("dd.MM.yyyy");
    private static final long serialVersionUID = 1L;
    protected String id;
    protected String name;
    protected String mimeType;
    protected String encoding;
    protected long size;
    protected DataFile dataFile;
    protected String downloadUrl;
    protected int orderNr;
    protected byte[] dataFileBytes;
    private String accessRestriction;
    private String accessRestrictionReason;
    private String accessRestrictionEndDesc;
    private Date accessRestrictionBeginDate;
    private Date accessRestrictionEndDate;

    public DataItem(NodeRef nodeRef, String id, String name, String mimeType, String encoding, long size, DataFile dataFile, int orderNr, byte[] dataFileBytes) {
        this.id = id;
        this.name = name;
        this.mimeType = mimeType;
        this.encoding = encoding;
        this.size = size;
        this.dataFile = dataFile;
        if (nodeRef != null && name != null) {
            downloadUrl = DownloadDigiDocContentServlet.generateUrl(nodeRef, orderNr, name);
        }
        this.orderNr = orderNr;
        this.dataFileBytes = dataFileBytes;
        if (nodeRef != null) {
            Map<QName, Serializable> props = BeanHelper.getNodeService().getProperties(nodeRef);
            ArrayList<String> accessRestrictions = (ArrayList<String>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTIONS);
            this.accessRestriction = CollectionUtils.isNotEmpty(accessRestrictions) && accessRestrictions.size() > orderNr ? accessRestrictions.get(orderNr) : null;
            ArrayList<String> accessRestrictionReasons = (ArrayList<String>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_REASONS);
            this.accessRestrictionReason = CollectionUtils.isNotEmpty(accessRestrictionReasons) && accessRestrictionReasons.size() > orderNr ? accessRestrictionReasons.get(orderNr) : null;
            ArrayList<String> accessRestrictionEndDescs = (ArrayList<String>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DESCS);
            this.accessRestrictionEndDesc = CollectionUtils.isNotEmpty(accessRestrictionEndDescs) && accessRestrictionEndDescs.size() > orderNr ? accessRestrictionEndDescs.get(orderNr) : null;
            ArrayList<Date> accessRestrictionBeginDates = (ArrayList<Date>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_BEGIN_DATES);
            this.accessRestrictionBeginDate = CollectionUtils.isNotEmpty(accessRestrictionBeginDates) && accessRestrictionBeginDates.size() > orderNr ? accessRestrictionBeginDates.get(orderNr) : null;
            ArrayList<Date> accessRestrictionEndDates = (ArrayList<Date>) props.get(FileModel.Props.DIGIDOC_FILES_ACCESS_RESTRICTION_END_DATES);
            this.accessRestrictionEndDate = CollectionUtils.isNotEmpty(accessRestrictionEndDates) && accessRestrictionEndDates.size() > orderNr ? accessRestrictionEndDates.get(orderNr) : null;
        }
    }

    public DataItem(NodeRef nodeRef, String id, String name, String mimeType, long size, DataFile dataFile, int orderNr) {
        this(nodeRef, id, name, mimeType, null, size, dataFile, orderNr, null);
    }

    public DataItem(NodeRef nodeRef, String id, String name, String mimeType, String encoding, long size, int orderNr) {
        this(nodeRef, id, name, mimeType, encoding, size, null, orderNr, null);
    }

    public DataItem(NodeRef nodeRef, String id, String name, String mimeType, long size, int orderNr) {
        this(nodeRef, id, name, mimeType, null, size, null, orderNr, null);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        if (FilenameUtils.getBaseName(name).length() < 51) {
            return name;
        }

        return FilenameUtils.getBaseName(name).substring(0, 50) + "...." + FilenameUtils.getExtension(name);
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getEncoding() {
        return encoding;
    }

    public long getSize() {
        return size;
    }

    /**
     * Returns a new stream each time. Caller must close the stream!
     */
    public InputStream getData() throws SignatureException {
        log.debug("getData()...");
        try {
            if (this.dataFileBytes == null) {
                log.debug("dataFileBytes is NULL! getData(): get a dataFile stream...");
                return dataFile.getStream();
            } else {
                log.debug("DIGISIGN IS ACTIVE: DATAFILE bytes: length: " + this.dataFileBytes.length);
                return new ByteArrayInputStream(this.dataFileBytes);
            }
        } catch (DigiDoc4JException e) {
            throw new SignatureException("Error getting data, " + toString(), e);
        }
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }

    /**
     * Used in JSP to determine the file icon.
     */
    public String getFileType16() {
        return FileTypeImageUtils.getFileTypeImage(getName(), true);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("DataItem[");
        sb.append("id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", mimeType=").append(mimeType);
        sb.append(", encoding=").append(encoding);
        sb.append(", size=").append(size);
        sb.append(", downloadUrl=").append(downloadUrl);
        sb.append(", dataFile=").append(ObjectUtils.identityToString(dataFile));
        sb.append("]");
        return sb.toString();
    }

    public String getAccessRestriction() {
        return accessRestriction;
    }

    public String getAccessRestrictionReason() {
        return accessRestrictionReason;
    }

    public String getAccessRestrictionEndDesc() {
        return accessRestrictionEndDesc;
    }

    public Date getAccessRestrictionBeginDate() {
        return accessRestrictionBeginDate;
    }

    public Date getAccessRestrictionEndDate() {
        return accessRestrictionEndDate;
    }

    public String getTitleString() {
        List<String> values = new ArrayList<>();
        if (StringUtils.isNotBlank(getAccessRestrictionReason())) {
            values.add(getAccessRestrictionReason());
        }
        if (getAccessRestrictionEndDate() != null) {
            values.add(simpleDateFormat.format(getAccessRestrictionEndDate()));
        }
        if (StringUtils.isNotBlank(getAccessRestrictionEndDesc())) {
            values.add(getAccessRestrictionEndDesc());
        }
        return StringUtils.join(values, ", ");
    }
}
