package ee.webmedia.alfresco.signature.model;

import java.io.Serializable;
import java.util.Date;
import org.digidoc4j.DataToSign;

public class SignatureDigest implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private DataToSign dataToSign;
    private final String digestHex;
    private final String certHex;
    private final Date date;
    private final String requestUid;
    private final String regCode;

    public SignatureDigest(String digestHex, String certHex, Date date, String requestUid, String regCode) {
        this.digestHex = digestHex;
        this.certHex = certHex;
        this.date = date;
        this.requestUid = requestUid;
        this.regCode = regCode;
    }
    
    public SignatureDigest(String digestHex, String certHex, Date date, DataToSign dataToSign, String requestUid, String regCode) {
        this.digestHex = digestHex;
        this.certHex = certHex;
        this.date = date;
        this.dataToSign = dataToSign;
        this.requestUid = requestUid;
        this.regCode = regCode;
    }

    public String getDigestHex() {
        return digestHex;
    }

    public String getCertHex() {
        return certHex;
    }

    public Date getDate() {
        return date;
    }

    public String getRequestUid() {
        return requestUid;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setDataToSign(DataToSign dataToSign) {
    	this.dataToSign = dataToSign;
    }
    
    public DataToSign getDataToSign() {
    	return dataToSign;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("SignatureDigest[");
        sb.append("digestHex=").append(digestHex);
        sb.append(", certHex=").append(certHex);
        sb.append(", date=").append(date);
        if (date != null) {
            sb.append(" ").append(date.getTime());
        }
        sb.append(", requestUid=").append(requestUid);
        sb.append("]");
        return sb.toString();
    }

}
