package ee.webmedia.alfresco.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.Assert;

import ee.webmedia.alfresco.classificator.model.ClassificatorValue;
import ee.webmedia.alfresco.classificator.service.ClassificatorService;

/**
 * Utility class to deal with date and time
 */

public class CalendarUtil {
    private static org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(CalendarUtil.class);

    public static LocalDate addWorkingDaysToDate(LocalDate date, int workingDaysToAdd) {
        Assert.notNull(date);
        int i = 0;
        while (i < workingDaysToAdd) {
            date = date.plusDays(1);
            if (isWorkingDay(date)) {
                i++;
            }
        }
        // correct number of working days added, check if achieved date is working day and add additional days if needed
        //date = addDaysIfNotWorkingDay(date);
        return date;
    }

    public static LocalDate addDaysIfNotWorkingDay(LocalDate date) {
        while (!isWorkingDay(date)) {
            date = date.plusDays(1);
        }
        return date;
    }

    public static boolean isWorkingDay(LocalDate date) {
        if (date.getDayOfWeek() == DateTimeConstants.SATURDAY || date.getDayOfWeek() == DateTimeConstants.SUNDAY) {
            return false;
        }
        return !isHoliday(date);
    }

    private static boolean isHoliday(LocalDate date) {
    	int day = date.getDayOfMonth();
    	int month = date.getMonthOfYear();
    	int year = date.getYear();
    	LocalDate goodFriday = getEasterDate(year).minusDays(2);
        if ((day == 1 && month == 1) 
        		|| (day == 24 && month == 2)
        		|| (day == 1 && month == 5)
        		|| (day == 23 && month == 6)
        		|| (day == 24 && month == 6)
        		|| (day == 20 && month == 8)
        		|| (day == 24 && month == 12)
        		|| (day == 25 && month == 12)
        		|| (day == 26 && month == 12)
        		|| date.equals(goodFriday)) {
        	return true;
        }
        return false;
    }
    
    private static LocalDate getEasterDate(int year) {
        int a = year % 19;
        int b = year % 4;
        int c = year % 7;
        int k = year / 100;
        int p = (13 + 8 * k) / 25;
        int q = k / 4;
        int M = (15 - p + k - q) % 30;
        int N = (4 + k - q) % 7;
        int d = (19 * a + M) % 30;
        int e = (2 * b + 4 * c + 6 * d + N) % 7;

        LocalDate newDueDate = new LocalDate(year, 3, 22);
        if (d == 29 && e == 6) {
            return newDueDate.plusDays(d + e).minusDays(7);
        } else
            return newDueDate.plusDays(d + e);
    }

    private static Set<LocalDate> getNationalHolidays(ClassificatorService classificatorService) {
        List<ClassificatorValue> classificatorValues = classificatorService.getActiveClassificatorValues(classificatorService.getClassificatorByName("nationalHolidays"));
        Set<LocalDate> holidays = new HashSet<LocalDate>();
        DateTimeFormatter parser = DateTimeFormat.forPattern("dd.MM.yyyy");
        for (ClassificatorValue classificatorValue : classificatorValues) {
            String dateStr = classificatorValue.getValueName();
            try {
                DateTime time = parser.parseDateTime(dateStr);
                holidays.add(time.toLocalDate());
            } catch (Exception e) {
                LOG.error("nationalHolidays classificator value is not in correct format 'dd.MM.yyyy', value='" + dateStr + "', skipping.", e);
            }
        }
        return holidays;
    }

    public static int getDaysBetween(LocalDate beginDate, LocalDate endDate) {
        return Math.abs(Days.daysBetween(beginDate, endDate).getDays()) + 1;
    }

    public static int getDaysBetweenSigned(Date beginDay, Date endDay) {
        return Days.daysBetween(new LocalDate(beginDay), new LocalDate(endDay)).getDays();
    }

    public static int getNationalHolidaysBetween(LocalDate beginDate, LocalDate endDate, ClassificatorService classificatorService) {
        int days = 0;
        Set<LocalDate> nationalHolidays = getNationalHolidays(classificatorService);
        for (LocalDate nationalHoliday : nationalHolidays) {
            if (!nationalHoliday.isBefore(beginDate) && !nationalHoliday.isAfter(endDate)) {
                days++;
            }
        }
        return days;
    }

    public static int getDaysBetween(LocalDate beginDate, LocalDate endDate, boolean subtractNationalHolidays, ClassificatorService classificatorService) {
        int days = getDaysBetween(beginDate, endDate);
        if (subtractNationalHolidays) {
            days -= getNationalHolidaysBetween(beginDate, endDate, classificatorService);
        }
        return days;
    }

    public static long duration(long startTime) {
        return duration(startTime, System.nanoTime());
    }

    public static long duration(long startTime, long stopTime) {
        return (stopTime - startTime) / 1000000L;
    }

    public static Date getDateOrNull(Calendar source) {
        return source != null ? source.getTime() : null;
    }

}
