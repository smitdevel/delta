package ee.webmedia.alfresco.substitute.model;

import static ee.webmedia.alfresco.document.model.Document.dateTimeFormat;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import ee.webmedia.alfresco.utils.MessageUtil;
import org.apache.commons.lang.time.DateUtils;

public class UnmodifiableSubstitute implements Serializable {
    private static final long serialVersionUID = 0L;

    private final String substituteName;
    private final String substituteId;
    private final String label;
    private final Date substitutionStartDate;
    private final Date substitutionEndDate;

    public UnmodifiableSubstitute(Substitute sub) {
        substituteName = sub.getSubstituteName();
        substituteId = sub.getSubstituteId();
        substitutionStartDate = sub.getSubstitutionStartDate();
        substitutionEndDate = sub.getSubstitutionEndDate();
        label = isActive() ? MessageUtil.getMessage("user_away_has_substitute",
                dateTimeFormat.format(substitutionStartDate), dateTimeFormat.format(substitutionEndDate), getSubstituteName()) : null;
    }

    public boolean isActive() {
        if (substitutionEndDate == null || substitutionStartDate == null) {
            return false;
        }
        Date currentDate = DateUtils.truncate(new Date(), Calendar.MINUTE);
        return currentDate.compareTo(substitutionStartDate) >= 0
                && currentDate.compareTo(substitutionEndDate) <= 0;
    }

    public String getLabel() {
        return label;
    }

    public String getSubstituteName() {
        return substituteName;
    }

    public String getSubstituteId() {
        return substituteId;
    }

    public Date getSubstitutionStartDate() {
        return substitutionStartDate;
    }

    public Date getSubstitutionEndDate() {
        return substitutionEndDate;
    }

}
