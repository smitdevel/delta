package ee.webmedia.alfresco.common.rest;

import ee.webmedia.alfresco.adr.ws.DokumentDetailidegaV2;
import org.alfresco.util.Pair;
import org.springframework.http.HttpStatus;

public interface AdrRestApiService {
    String BEAN_NAME = "AdrRestApiService";

    Pair<String, HttpStatus> getDocumentFromAdr(String adrDocNodeRef);
    
    HttpStatus deleteDocumentFromAdr(String adrDocNodeRef);

    HttpStatus addDocumentToAdr(DokumentDetailidegaV2 adrDocNodeRef);

    boolean isAdrBlockEnabled();
}
