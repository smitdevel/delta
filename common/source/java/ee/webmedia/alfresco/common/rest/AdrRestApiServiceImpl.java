package ee.webmedia.alfresco.common.rest;

import java.io.UnsupportedEncodingException;

import javax.xml.bind.DatatypeConverter;

import ee.webmedia.alfresco.adr.ws.DokumentDetailidegaV2;
import org.alfresco.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import ee.webmedia.alfresco.common.web.BeanHelper;

public class AdrRestApiServiceImpl implements AdrRestApiService {
    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(AdrRestApiServiceImpl.class);
    private RestTemplate restTemplate = setupRestTemplate();

	public Pair<String, HttpStatus> getDocumentFromAdr(String adrDocNodeRef) {
    	String encodedNodeRef = encodeNodeRef(adrDocNodeRef);
    	try {
    		String url = String.format("%s/api/organizations/%s/documents/%s", getAdrUrl(), getAdrOrgName(), encodedNodeRef);
    		String dokumentJSON = restTemplate.getForObject(url, String.class);
    		return new Pair<String, HttpStatus>(dokumentJSON, HttpStatus.OK);
    	} catch (HttpClientErrorException | HttpServerErrorException clientOrServerException) {
    		log.debug("Unable to get document :" + clientOrServerException.getMessage());
    		return new Pair<String, HttpStatus>(null, clientOrServerException.getStatusCode());
    	}
	}
	
	public HttpStatus deleteDocumentFromAdr(String adrDocNodeRef) {
    	String encodedNodeRef = encodeNodeRef(adrDocNodeRef);
    	try {
    		String url = String.format("%s/api/organizations/%s/documents/%s", getAdrUrl(), getAdrOrgName(), encodedNodeRef);
    		restTemplate.delete(url);
    		return HttpStatus.OK;
    	} catch (HttpClientErrorException | HttpServerErrorException clientOrServerException) {
    		log.debug("Unable to delete document :" + clientOrServerException.getMessage());
			return clientOrServerException.getStatusCode();
    	}
	}

	public HttpStatus addDocumentToAdr(DokumentDetailidegaV2 document) {
		try {
			String url = String.format("%s/api/organizations/%s/documents/single", getAdrUrl(), getAdrOrgName());
			ResponseEntity result = restTemplate.postForEntity(url, document, ResponseEntity.class);
			return result.getStatusCode();
		} catch (HttpClientErrorException | HttpServerErrorException clientOrServerException) {
			log.debug("Unable to send document :" + clientOrServerException.getMessage());
			return clientOrServerException.getStatusCode();
		}
	}
	
	private String encodeNodeRef(String nodeRef) {
		String base64NodeRef = null;
		try {
			byte[] message = nodeRef.getBytes("UTF-8");
			base64NodeRef = DatatypeConverter.printBase64Binary(message);
		} catch (UnsupportedEncodingException e) {
			log.error(String.format("Unable to encode base64 (UTF-8) nodeRef %s : %s", nodeRef, e.getMessage()));
		}
		return base64NodeRef;
	}
	
	private RestTemplate setupRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		int timout = BeanHelper.getApplicationConstantsBean().getAdrRestTimeout();
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
			rf.setReadTimeout(timout);
			rf.setConnectTimeout(timout);
		return restTemplate;
	}
	
	private String getAdrUrl() {
		return BeanHelper.getApplicationConstantsBean().getAdrUrl();
	}
	
	private String getAdrOrgName() {
		return BeanHelper.getApplicationConstantsBean().getAdrOrgName();
	}
	
	public boolean isAdrBlockEnabled() {
		return BeanHelper.getApplicationConstantsBean().isAdrBlockView();
	}
}
