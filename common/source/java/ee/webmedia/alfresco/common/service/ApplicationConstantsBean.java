package ee.webmedia.alfresco.common.service;

import ee.webmedia.alfresco.utils.MessageUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * Bean for holding application configuration data that is not going to change during uptime.
 */
public class ApplicationConstantsBean implements InitializingBean {
    public static final String BEAN_NAME = "applicationConstantsBean";

    private static Log log = LogFactory.getLog(ApplicationConstantsBean.class);

    /**
     * If document list contains more than given number of rows, then only initial sorting is performed when loading list
     * and other sorting functionalities are disabled.
     */
    public static final int SORT_ALLOWED_LIMIT = 10000;

    /**
     * Seoses asutuseülese töövoo testimisega meie testis, kus asutus peab saama saata ülesandeid ka endale:
     * dokumendi vastuvõtmisel ja olemasoleva dokumendi otsimisele kontrollitakse
     * lisaks originalDvkId-le ka seda, et dokumendil oleks olemas aspekt notEditable, property notEditable=true.
     * Kui ei ole, siis tehakse uus dok. (Max peaks saama ühes süsteemis olla kaks dokumenti
     * sama originalDvkId-ga taskiga ja üks on alati notEditable sel juhul).
     * Ülesande teostamise vastuvõtmisel eelistatakse sellise dokumendi küljes olevat ülesannet,
     * millel ei ole notEditable aspekti. (Võib olla, et dokument on korduvalt edasi saadetud,
     * sel juhul ei ole ilma notEditable aspektita dokumenti olemas).
     * Testis tekib probleem sellise edasisaatmise korral, kui saata endale ja siis
     * edasisaadetud dokument uuesti endale, seda varianti ei saa testida.
     * Et asutus saaks tööülesannet saata iseendale, tuleb INTERNAL_TESTING väärtustada true,
     * sel juhul kuvatakse tööülesande täitja otsingus kontaktide nimekirjas ka
     * asutuse enda regitrikoodiga kontakt.
     * NB! Live keskkonnas PEAB INTERNAL_TESTING väärtus olema false!!!
     */
    private boolean INTERNAL_TESTING;
    private boolean caseVolumeEnabled;
    private String defaultVolumeSortingField;
    private boolean groupsEditingAllowed;
    private boolean createOrgStructGroups;
    private boolean substitutionTaskEndDateRestricted;
    private String messageNo;
    private String messageYes;

    private boolean generateNewRegNumberInReregistration;
    private boolean finishUnregisteredDocumentEnabled;
    private boolean volumeColumnEnabled;
    private boolean myTasksAndDocumentsMenuClosed;
    private boolean syncActiveStatus;
    private boolean showAssociationSenderReceiver;
    private boolean associationShortTitle;
    private boolean showAssociationFiles;
    private boolean documentAutoSendingEnabled;
    private boolean insertSignatureResolution;

    private int statusCasTimeout;
    private int statusDbTimeout;
    private int statusLdapTimeout;
    private int statusMsoTimeout;
    private int statusOodTimeout;
    private int statusXroadTimeout;
    private int statusHttpGetTimeout;

    private String adrUrl;
    private String adrOrgName;
    private boolean adrBlockView;
    private int adrRestTimeout;
    private boolean adrBlockViewForDocMangers;

    private boolean showFinishedUnsentDocuments;
    private boolean orgPris;

    private boolean testEmailActive;
    private String testEmailSendToValue;

    public boolean isGenerateNewRegNumberInReregistration() {
        return generateNewRegNumberInReregistration;
    }

    public void setGenerateNewRegNumberInReregistration(boolean generateNewRegNumberInReregistration) {
        this.generateNewRegNumberInReregistration = generateNewRegNumberInReregistration;
    }

    public boolean isFinishUnregisteredDocumentEnabled() {
        return finishUnregisteredDocumentEnabled;
    }

    public void setFinishUnregisteredDocumentEnabled(boolean finishUnregisteredDocumentEnabled) {
        this.finishUnregisteredDocumentEnabled = finishUnregisteredDocumentEnabled;
    }

    public boolean isVolumeColumnEnabled() {
        return volumeColumnEnabled;
    }

    public void setVolumeColumnEnabled(boolean volumeColumnEnabled) {
        this.volumeColumnEnabled = volumeColumnEnabled;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        messageYes = MessageUtil.getMessage("yes");
        messageNo = MessageUtil.getMessage("no");
    }

    public boolean isInternalTesting() {
        return INTERNAL_TESTING;
    }

    public void setInternalTesting(boolean internalTesting) {
        INTERNAL_TESTING = internalTesting;
    }

    public boolean isCaseVolumeEnabled() {
        return caseVolumeEnabled;
    }

    public void setCaseVolumeEnabled(boolean caseVolumeEnabled) {
        this.caseVolumeEnabled = caseVolumeEnabled;
    }

    public String getDefaultVolumeSortingField() {
        return defaultVolumeSortingField;
    }

    public void setDefaultVolumeSortingField(String defaultVolumeSortingField) {
        this.defaultVolumeSortingField = defaultVolumeSortingField;
    }

    public boolean isGroupsEditingAllowed() {
        return groupsEditingAllowed;
    }

    public void setGroupsEditingAllowed(boolean groupsEditingAllowed) {
        this.groupsEditingAllowed = groupsEditingAllowed;
    }

    public boolean isCreateOrgStructGroups() {
        return createOrgStructGroups;
    }

    public void setCreateOrgStructGroups(boolean createOrgStructGroups) {
        this.createOrgStructGroups = createOrgStructGroups;
    }

    public boolean isSubstitutionTaskEndDateRestricted() {
        return substitutionTaskEndDateRestricted;
    }

    public void setSubstitutionTaskEndDateRestricted(boolean substitutionTaskEndDateRestricted) {
        this.substitutionTaskEndDateRestricted = substitutionTaskEndDateRestricted;
    }

    public boolean isEinvoiceEnabled() {
        return false;
    }

    public String getMessageNo() {
        return messageNo;
    }

    public String getMessageYes() {
        return messageYes;
    }

    public boolean isMyTasksAndDocumentsMenuClosed() {
        return myTasksAndDocumentsMenuClosed;
    }

    public void setMyTasksAndDocumentsMenuClosed(boolean myTasksAndDocumentsMenuClosed) {
        this.myTasksAndDocumentsMenuClosed = myTasksAndDocumentsMenuClosed;
    }

    public void setSyncActiveStatus(boolean syncActiveStatus) {
        log.debug("---------------------------------------------------------");
        log.debug("setSyncActiveStatus(): Sync active status: " + syncActiveStatus);
        log.debug("---------------------------------------------------------");
        this.syncActiveStatus = syncActiveStatus; //syncActiveStatus;
    }

    public boolean isSyncActiveStatus() {
        log.debug("---------------------------------------------------------");
        log.debug("getSyncActiveStatus(): Sync active status: " + this.syncActiveStatus);
        log.debug("---------------------------------------------------------");
        return this.syncActiveStatus;
    }

    public void setShowAssociationSenderReceiver(boolean showAssociationSenderReceiver) {
        this.showAssociationSenderReceiver = showAssociationSenderReceiver;
    }

    public boolean isShowAssociationSenderReceiver() {
        return showAssociationSenderReceiver;
    }

    public void setAssociationShortTitle(boolean associationShortTitle) {
        this.associationShortTitle = associationShortTitle;
    }

    public boolean isAssociationShortTitle() {
        return associationShortTitle;
    }

    public void setShowAssociationFiles(boolean showAssociationFiles) {
        this.showAssociationFiles = showAssociationFiles;
    }

    public boolean isShowAssociationFiles() {
        return showAssociationFiles;
    }

    public int getStatusCasTimeout() {
        return statusCasTimeout;
    }

    public void setStatusCasTimeout(int statusCasTimeout) {
        this.statusCasTimeout = statusCasTimeout;
    }

    public int getStatusDbTimeout() {
        return statusDbTimeout;
    }

    public void setStatusDbTimeout(int statusDbTimeout) {
        this.statusDbTimeout = statusDbTimeout;
    }

    public int getStatusLdapTimeout() {
        return statusLdapTimeout;
    }

    public void setStatusLdapTimeout(int statusLdapTimeout) {
        this.statusLdapTimeout = statusLdapTimeout;
    }

    public int getStatusMsoTimeout() {
        return statusMsoTimeout;
    }

    public void setStatusMsoTimeout(int statusMsoTimeout) {
        this.statusMsoTimeout = statusMsoTimeout;
    }

    public int getStatusOodTimeout() {
        return statusOodTimeout;
    }

    public void setStatusOodTimeout(int statusOodTimeout) {
        this.statusOodTimeout = statusOodTimeout;
    }

    public int getStatusXroadTimeout() {
        return statusXroadTimeout;
    }

    public void setStatusXroadTimeout(int statusXroadTimeout) {
        this.statusXroadTimeout = statusXroadTimeout;
    }

    public int getStatusHttpGetTimeout() {
        return statusHttpGetTimeout;
    }

    public void setStatusHttpGetTimeout(int statusHttpGetTimeout) {
        this.statusHttpGetTimeout = statusHttpGetTimeout;
    }

    public boolean isDocumentAutoSendingEnabled() {
        return documentAutoSendingEnabled;
    }

    public void setDocumentAutoSendingEnabled(boolean documentAutoSendingEnabled) {
        this.documentAutoSendingEnabled = documentAutoSendingEnabled;
    }

    public boolean isInsertSignatureResolution() {
        return insertSignatureResolution;
    }

    public void setInsertSignatureResolution(boolean insertSignatureResolution) {
        this.insertSignatureResolution = insertSignatureResolution;
    }

    public String getAdrUrl() {
        return adrUrl;
    }

    public void setAdrUrl(String adrUrl) {
        this.adrUrl = adrUrl;
    }

    public String getAdrOrgName() {
        return adrOrgName;
    }

    public void setAdrOrgName(String adrOrgName) {
        this.adrOrgName = adrOrgName;
    }

    public boolean isAdrBlockView() {
        return adrBlockView;
    }

    public void setAdrBlockView(boolean adrBlockView) {
        this.adrBlockView = adrBlockView;
    }

    public int getAdrRestTimeout() {
        return adrRestTimeout;
    }

    public void setAdrRestTimeout(int adrRestTimeout) {
        this.adrRestTimeout = adrRestTimeout;
    }

    public boolean isAdrBlockViewForDocMangers() {
        return adrBlockViewForDocMangers;
    }

    public void setAdrBlockViewForDocMangers(boolean adrBlockViewForDocMangers) {
        this.adrBlockViewForDocMangers = adrBlockViewForDocMangers;
    }

    public boolean isShowFinishedUnsentDocuments() {
        return showFinishedUnsentDocuments;
    }

    public void setShowFinishedUnsentDocuments(boolean showFinishedUnsentDocuments) {
        this.showFinishedUnsentDocuments = showFinishedUnsentDocuments;
    }

    public boolean isOrgPris() {
        return orgPris;
    }

    public void setOrgPris(boolean orgPris) {
        this.orgPris = orgPris;
    }

    public void setTestEmailActive(boolean testEmailActive) {
        this.testEmailActive = testEmailActive;
    }

    public void setTestEmailSendToValue(String testEmailSendToValue) {
        this.testEmailSendToValue = testEmailSendToValue;
    }

    public boolean isTestEmailActive() {
        return testEmailActive;
    }

    public String getTestEmailSendToValue() {
        return testEmailSendToValue;
    }
}
