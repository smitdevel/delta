package ee.webmedia.alfresco.common.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public enum UserContactGroupSearchFilters {
    // Level 1
    USERS_FILTER(1, "Users"),
    USER_GROUPS_FILTER(2, "UserGroups"),
    CONTACTS_FILTER(4, "Contacts"),
    CONTACT_GROUPS_FILTER(8, "ContactGroups"),

    // Level 2
    USERS_AND_USER_GROUPS_FILTER(200, "UsersAndUserGroups"),
    USERS_AND_CONTACTS_FILTER(210, "UsersAndContacts"),
    USERS_AND_CONTACT_GROUPS_FILTER(220, "UsersAndContactGroups"),

    USER_GROUPS_AND_CONTACTS_FILTER(230, "UserGroupsAndContacts"),
    USER_GROUPS_AND_CONTACT_GROUPS_FILTER(240, "UserGroupsAndContactGroups"),

    CONTACTS_AND_CONTACT_GROUPS_FILTER(250, "ContactsAndContactGroups"),

    // Level 3
    USERS_AND_USER_GROUPS_AND_CONTACTS_FILTER(300, "UsersAndUserGroupsAndContacts"),
    USERS_AND_USER_GROUPS_AND_CONTACT_GROUPS_FILTER(310, "UsersAndUserGroupsAndContactGroups"),
    USERS_AND_CONTACTS_AND_CONTACT_GROUPS_FILTER(320, "UsersAndContactsAndContactGroups"),

    USER_GROUPS_AND_CONTACTS_AND_CONTACT_GROUPS_FILTER(330, "UserGroupsAndContactsAndContactGroups"),

    // Level 4
    ALL_FILTERS(400, "All");

    private int id;
    private String value;


    private static final Map<Integer, UserContactGroupSearchFilters> byId = new HashMap<>();
    static {
        for (UserContactGroupSearchFilters e : UserContactGroupSearchFilters.values()) {
            if (byId.put(e.getId(), e) != null) {
                throw new IllegalArgumentException("duplicate id: " + e.getId());
            }
        }
    }

    public static UserContactGroupSearchFilters getById(Integer id) {
        return byId.get(id);
    }

    public static String getValueById(Integer id) {
        return byId.get(id).getValue();
    }

    public static String getValueByIdList(ArrayList<Integer> ids){
        if(ids.size() == 1){
            return getValueById(ids.get(0));
        } else if(ids.size() == 2){
            if(ids.contains(1) && ids.contains(2)){
                return USERS_AND_USER_GROUPS_FILTER.getValue();
            } else if(ids.contains(1) && ids.contains(4)){
                return USERS_AND_CONTACTS_FILTER.getValue();
            } else if(ids.contains(1) && ids.contains(8)){
                return USERS_AND_CONTACT_GROUPS_FILTER.getValue();
            } else if(ids.contains(2) && ids.contains(4)){
                return USER_GROUPS_AND_CONTACTS_FILTER.getValue();
            } else if(ids.contains(2) && ids.contains(8)){
                return USER_GROUPS_AND_CONTACT_GROUPS_FILTER.getValue();
            } else if(ids.contains(4) && ids.contains(8)){
                return CONTACTS_AND_CONTACT_GROUPS_FILTER.getValue();
            } else {
                return "Null";
            }
        } else if(ids.size() == 3){
            if(ids.contains(1) && ids.contains(2) && ids.contains(4)){
                return USERS_AND_USER_GROUPS_AND_CONTACTS_FILTER.getValue();
            } else if(ids.contains(1) && ids.contains(2) && ids.contains(8)){
                return USERS_AND_USER_GROUPS_AND_CONTACT_GROUPS_FILTER.getValue();
            } else if(ids.contains(1) && ids.contains(4) && ids.contains(8)){
                return USERS_AND_CONTACTS_AND_CONTACT_GROUPS_FILTER.getValue();
            } else if(ids.contains(2) && ids.contains(4) && ids.contains(8)){
                return USER_GROUPS_AND_CONTACTS_AND_CONTACT_GROUPS_FILTER.getValue();
            } else {
                return "Null";
            }
        } else if(ids.size() == 4){
            return ALL_FILTERS.getValue();
        } else {
            return "Null";
        }
    }
    UserContactGroupSearchFilters(int id, String value){
        this.id = id;
        this.value = value;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
