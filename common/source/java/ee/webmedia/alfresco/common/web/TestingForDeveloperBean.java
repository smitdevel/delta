package ee.webmedia.alfresco.common.web;

import ee.webmedia.alfresco.app.AppConstants;
import ee.webmedia.alfresco.archivals.model.ArchivalsStoreVO;
import ee.webmedia.alfresco.classificator.enums.DocumentStatus;
import ee.webmedia.alfresco.classificator.enums.TemplateType;
import ee.webmedia.alfresco.classificator.model.Classificator;
import ee.webmedia.alfresco.classificator.model.ClassificatorValue;
import ee.webmedia.alfresco.classificator.service.ClassificatorService;
import ee.webmedia.alfresco.common.job.NightlyDataFixJob;
import ee.webmedia.alfresco.common.rest.AdrRestApiService;
import ee.webmedia.alfresco.common.service.CustomReindexComponent;
import ee.webmedia.alfresco.common.service.GeneralService;
import ee.webmedia.alfresco.docdynamic.bootstrap.DocumentUpdater;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.bootstrap.SearchableSendInfoUpdater;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
import ee.webmedia.alfresco.document.model.DocumentNumbering;
import ee.webmedia.alfresco.document.sendout.model.SendInfo;
import ee.webmedia.alfresco.dvk.service.DvkService;
import ee.webmedia.alfresco.eventplan.model.EventPlanModel;
import ee.webmedia.alfresco.functions.model.UnmodifiableFunction;
import ee.webmedia.alfresco.log.model.LogEntry;
import ee.webmedia.alfresco.log.model.LogFilter;
import ee.webmedia.alfresco.parameters.model.Parameter;
import ee.webmedia.alfresco.parameters.model.ParametersModel;
import ee.webmedia.alfresco.series.model.SeriesModel;
import ee.webmedia.alfresco.series.model.UnmodifiableSeries;
import ee.webmedia.alfresco.template.model.DocumentTemplateModel;
import ee.webmedia.alfresco.utils.ActionUtil;
import ee.webmedia.alfresco.utils.SearchUtil;
import ee.webmedia.alfresco.utils.UnableToPerformException;
import ee.webmedia.alfresco.versions.model.VersionsModel;
import ee.webmedia.alfresco.volume.VolumeDispositionReportGenerator;
import ee.webmedia.alfresco.volume.model.Volume;
import ee.webmedia.alfresco.volume.model.VolumeModel;
import ee.webmedia.alfresco.volume.search.model.VolumeSearchModel;
import ee.webmedia.alfresco.workflow.bootstrap.CompoundWorkflowOwnerPropsUpdater;
import ee.webmedia.alfresco.workflow.bootstrap.TaskUpdater;
import ee.webmedia.xtee.client.dhl.DhlFSStubXTeeServiceImpl;

import org.alfresco.i18n.I18NUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.cache.EhCacheTracerJob;
import org.alfresco.repo.search.Indexer;
import org.alfresco.repo.search.impl.lucene.ADMLuceneTest;
import org.alfresco.repo.search.impl.lucene.AbstractLuceneBase;
import org.alfresco.repo.search.impl.lucene.AbstractLuceneIndexerAndSearcherFactory.LuceneIndexBackupComponent;
import org.alfresco.repo.search.impl.lucene.AbstractLuceneIndexerAndSearcherFactory.LuceneIndexBackupJob;
import org.alfresco.repo.search.impl.lucene.LuceneIndexerAndSearcher;
import org.alfresco.repo.search.impl.lucene.index.IndexInfo;
import org.alfresco.repo.security.sync.UserRegistrySynchronizer;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.TransientNode;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.myfaces.application.jsp.JspStateManagerImpl;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.quartz.JobExecutionException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import com.nortal.jroad.client.dhl.DhlXTeeService.SendStatus;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static ee.webmedia.alfresco.common.web.BeanHelper.*;
import static ee.webmedia.alfresco.utils.SearchUtil.*;

/**
 * Bean with method {@link #handleTestEvent(ActionEvent)} that developers can use to test arbitrary code
 */
public class TestingForDeveloperBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory.getLog(TestingForDeveloperBean.class);
    private static final String CHARSET = AppConstants.CHARSET;
    private transient NodeService nodeService;
    private transient GeneralService generalService;
    private transient SearchService searchService;
    private transient TransactionService transactionService;

    private static final FastDateFormat dateTimeFormat = FastDateFormat.getInstance("dd.MM.yyyy HH:mm:ss.SSSZ");

    private String fileName;
    private String dispositionFileName;
    private String indexInfoText;
    private String sendingStatusDateStr;
    private String adrDocNodeRef;

	public String getAdrDocNodeRef() {
		return adrDocNodeRef;
	}

	public void setAdrDocNodeRef(String adrDocNodeRef) {
		this.adrDocNodeRef = adrDocNodeRef;
	}

	public String getSendingStatusDateStr() {
		return sendingStatusDateStr;
	}

	public void setSendingStatusDateStr(String sendingStatusDateStr) {
		this.sendingStatusDateStr = sendingStatusDateStr;
	}

	public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getIndexInfoText() {
        return indexInfoText;
    }

    private String missingOwnerId;

    public String getMissingOwnerId() {
        return missingOwnerId;
    }

    public void setMissingOwnerId(String missingOwnerId) {
        this.missingOwnerId = missingOwnerId;
    }

    public String getDispositionFileName() {
        return dispositionFileName;
    }

    public void setDispositionFileName(String dispositionFileName) {
        this.dispositionFileName = dispositionFileName;
    }

    public void searchMissingOwnerId(@SuppressWarnings("unused") ActionEvent event) {
        List<String> queryParts = new ArrayList<String>();
        queryParts.add(generateTypeQuery(DocumentCommonModel.Types.DOCUMENT));
        queryParts.add(generateAspectQuery(DocumentCommonModel.Aspects.SEARCHABLE));
        queryParts.add(SearchUtil.generatePropertyNullQuery(DocumentCommonModel.Props.OWNER_ID));
        queryParts.add(SearchUtil.generatePropertyNotNullQuery(DocumentCommonModel.Props.OWNER_NAME));
        queryParts.add(generateStringExactQuery(DocumentStatus.WORKING.getValueName(), DocumentCommonModel.Props.DOC_STATUS));
        String query = joinQueryPartsAnd(queryParts);
        String result = StringUtils.join(BeanHelper.getDocumentSearchService().searchNodes(query, 0, "missingOwnerId"), '\n');
        if (StringUtils.isBlank(result)) {
            result = "0";
        }

        setMissingOwnerId(result);
    }

    public void addNonSerializableObjectToSession(@SuppressWarnings("unused") ActionEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("NonSerializableObject", new Object());
    }

    public void receiveDocStub(@SuppressWarnings("unused") ActionEvent event) {
        DvkService stubDvkService = BeanHelper.getStubDvkService();
        DhlFSStubXTeeServiceImpl dhlXTeeServiceImplFSStub = BeanHelper.getDhlXTeeServiceImplFSStub();
        dhlXTeeServiceImplFSStub.setHasDocuments(true);
        String xmlFile = fileName;
        dhlXTeeServiceImplFSStub.setDvkXmlFile(xmlFile);
        Collection<String> receiveResults = stubDvkService.receiveDocuments();
        LOG.info("created " + receiveResults.size() + " documents based on given xml file");
    }

    public void doStuff(ActionEvent event) {
        LOG.info("doStuff()...");
        // DhlXTeeServiceImpl dhlXTeeService = BeanHelper.getSpringBean(DhlXTeeServiceImpl.class, "dhlXTeeService");
        // dhlXTeeService.markDocumentsReceived(Arrays.asList("10113"));

        Collection<String> receiveDocuments = BeanHelper.getDvkService().receiveDocuments();
        LOG.info("Received following documents:" + receiveDocuments);
    }

    /** Event handler for link "TestingForDeveloper" in /simdhs/faces/jsp/admin/store-browser.jsp */
    public void handleTestEvent(ActionEvent event) throws Exception {
        int testParamValue = ActionUtil.getParam(event, "testP", Integer.class);
        LOG.debug("Received event with testP=" + testParamValue);
        // Developers can use this method for testing, but shouldn't commit changes
        atsTestib(event);
    }

    public void runADMLuceneTestTestMaskDeletes(ActionEvent event) throws Exception {
        LOG.info("Starting to run ADMLuceneTest.testMaskDeletes");
        ADMLuceneTest test = new ADMLuceneTest("admLuceneTest");
        test.setUp(BeanHelper.getApplicationService().getApplicationContext());
        test.testMaskDeletes();
        test.tearDown();
        LOG.info("Completed running ADMLuceneTest.testMaskDeletes");
    }

    public void deleteFieldAndFieldGroupsAndBootstrapInfo(@SuppressWarnings("unused") ActionEvent event) {
        String simdhsModule = "simdhs";
        deleteBootstrap(simdhsModule, "systematicFieldDefinitionsBootstrap1");
        deleteBootstrap(simdhsModule, "systematicFieldDefinitionsBootstrap2");
        deleteBootstrap(simdhsModule, "systematicFieldDefinitionsBootstrap3");
        deleteBootstrap(simdhsModule, "systematicFieldDefinitionsBootstrap4");
        deleteBootstrap(simdhsModule, "systematicFieldGroupDefinitionsBootstrap1");
        deleteBootstrap(simdhsModule, "systematicFieldGroupDefinitionsBootstrap2");
        deleteBootstrap(simdhsModule, "systematicFieldGroupDefinitionsBootstrap3");
        deleteBootstrap(simdhsModule, "systematicFieldGroupDefinitions1FixBootstrap");
        deleteBootstrap(simdhsModule, "systematicDocumentTypesBootstrap");
        deleteChildren(getNodeRef("/docadmin:fieldDefinitions"));
        deleteChildren(getNodeRef("/docadmin:fieldGroupDefinitions"));
        deleteChildren(getNodeRef("/docadmin:documentTypes"));
    }

    public void deleteTestTemplatesBootstrapAndTemplates(@SuppressWarnings("unused") ActionEvent event) {
        deleteBootstrap("simdhs", "testWorkflowTemplatesBootstrap");
        List<FileInfo> templateFiles = BeanHelper.getFileFolderService().listFiles(BeanHelper.getConstantNodeRefsBean().getTemplateRoot());
        LOG.info("Found total " + templateFiles.size() + " templates");
        for (FileInfo fi : templateFiles) {
            if (getNodeService().hasAspect(fi.getNodeRef(), DocumentTemplateModel.Aspects.TEMPLATE_NOTIFICATION)
                    || TemplateType.NOTIFICATION_TEMPLATE.toString().equals(getNodeService().getProperty(fi.getNodeRef(), DocumentTemplateModel.Prop.TEMPLATE_TYPE))
                    || getNodeService().hasAspect(fi.getNodeRef(), QName.createQName(DocumentTemplateModel.URI, "systemTemplate"))
                    || "Süsteemne mall".equals(getNodeService().getProperty(fi.getNodeRef(), DocumentTemplateModel.Prop.DOCTYPE_ID))) {
                LOG.info("Deleting template \"" + fi.getName() + "\"");
                getNodeService().deleteNode(fi.getNodeRef());
            }
        }
        LOG.info("Completed deleting templates");
    }

    public void generateDispositionReport(@SuppressWarnings("unused") ActionEvent event) {
        LOG.info("generateDispositionReport " + getDispositionFileName());
        String csvFileName = getDispositionFileName();
        if (StringUtils.isBlank(getDispositionFileName())) {
            throw new UnableToPerformException("Input csv not defined, aborting updater");
        }
        File file = new File(csvFileName);
        if (!file.exists()) {
            throw new UnableToPerformException("Input csv " + csvFileName + " does not exist, aborting updater");
        }

        HSSFWorkbook workbook = VolumeDispositionReportGenerator.generate(file);
        writeDispositionReportToResponse(workbook);
        // hack for incorrect view id in the next request
        JspStateManagerImpl.ignoreCurrentViewSequenceHack();
    }

    public void resetMarkedForDestruction(@SuppressWarnings("unused") ActionEvent event) {
        LOG.info("resetMarkedForDestruction ");
        List<NodeRef> storeNodeRefs = getArchivalStores();
        List<Volume> volumes = BeanHelper.getDocumentSearchService().searchVolumesForArchiveList(
                new TransientNode(VolumeSearchModel.Types.ARCHIVE_LIST_FILTER, null, null), false, false, storeNodeRefs);
        resetMarkedForDestruction(volumes);
        // hack for incorrect view id in the next request
        JspStateManagerImpl.ignoreCurrentViewSequenceHack();
    }
    
    public void deleteCorruptedUsers(@SuppressWarnings("unused") ActionEvent event) {
    	List<NodeRef> userRefs = getPersonService().getAllUserRefs();
    	for (NodeRef userRef : userRefs) {
    		String userName = (String) getNodeService().getProperty(userRef, ContentModel.PROP_USERNAME);
            if (userName != null && userName.length() > 11 && userName.substring(0, 11).matches("[0-9]+")) {
                getNodeService().deleteNode(userRef);
            }
    	}
    }
    
    public void updateSeriesDocNumbering(@SuppressWarnings("unused") ActionEvent event) {
    	for (UnmodifiableFunction function : getFunctionsService().getAllFunctions()) {
    		for (UnmodifiableSeries series : getSeriesService().getAllSeriesByFunction(function.getNodeRef(), false)) {
    	    	Boolean newNumberForEveryDoc = (Boolean) nodeService.getProperty(series.getSeriesRef(), SeriesModel.Props.NEW_NUMBER_FOR_EVERY_DOC);
    	    	Boolean individualizingNumbers = (Boolean) nodeService.getProperty(series.getSeriesRef(), SeriesModel.Props.INDIVIDUALIZING_NUMBERS);
    	    	nodeService.setProperty(series.getSeriesRef(), SeriesModel.Props.DOC_NUMBERING, (Serializable) getDocNumbering(newNumberForEveryDoc, individualizingNumbers));
        	}
    	}
    }
    
	private Integer getDocNumbering(Boolean newNumberForEveryDoc, Boolean individualizingNumbers) {
		if (newNumberForEveryDoc == null || individualizingNumbers == null) {
			return Integer.valueOf(DocumentNumbering.NEW_NUMBER_FOR_EVERY_DOCUMENT.ordinal());
		}
		if (!newNumberForEveryDoc && !individualizingNumbers) {
			return Integer.valueOf(DocumentNumbering.NO_INDIVIDUALIZING_NUMBER.ordinal());
		} else if (newNumberForEveryDoc && !individualizingNumbers) {
			return Integer.valueOf(DocumentNumbering.NEW_NUMBER_FOR_EVERY_DOCUMENT.ordinal());
		} else if (!newNumberForEveryDoc && individualizingNumbers) {
			return Integer.valueOf(DocumentNumbering.INDIVIDUALIZING_NUMBER.ordinal());
		}
		return Integer.valueOf(DocumentNumbering.NEW_NUMBER_FOR_EVERY_DOCUMENT.ordinal());
	}

    private List<NodeRef> getArchivalStores() {
        LOG.info("getArchivalStores()...");
        List<NodeRef> storeNodeRefs = new ArrayList<>();
        LOG.info("getGeneralService().getArchivalsStoreVOs() size: " + getGeneralService().getArchivalsStoreVOs().size());
        for (ArchivalsStoreVO archivalsStoreVO : getGeneralService().getArchivalsStoreVOs()) {
            LOG.info("TITLE: " + archivalsStoreVO.getTitle() + "; NodeRef: " + archivalsStoreVO.getNodeRef());
            storeNodeRefs.add(archivalsStoreVO.getNodeRef());
        }
        return storeNodeRefs;
    }

    private void resetMarkedForDestruction(List<Volume> volumes) {
        LOG.info("resetMarkedForDestruction()...");
        List<NodeRef> jobList = BeanHelper.getArchivalsService().getAllInQueueJobsForDesruction();
        for (Volume volume : volumes) {
            if (!jobList.contains(volume.getNodeRef())) {
                nodeService.setProperty(volume.getNodeRef(), EventPlanModel.Props.MARKED_FOR_DESTRUCTION, Boolean.FALSE);
            }
        }
    }

    private void writeDispositionReportToResponse(HSSFWorkbook workbook) {
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            response.reset();
            response.setCharacterEncoding(CHARSET);
            response.setContentType("text/csv; charset=" + CHARSET);
            response.setContentType("application/octet-stream");
            response.addHeader("Content-Disposition", "attachment; filename=\"Havitamisakt.xls\"");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            OutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
            fc.responseComplete();
            fc.renderResponse();
            LOG.info("Your excel file has been generated!");
        } catch (IOException e) {
            throw new UnableToPerformException("Error generating Havitamisakt.xls!", e);
        }
    }

    private void deleteBootstrap(String moduleName, String bootstrapName) {
        final NodeRef nodeRef = getGeneralService().deleteBootstrapNodeRef(moduleName, bootstrapName);
        if (nodeRef == null) {
            LOG.info("from module '" + moduleName + "' bootstrap '" + bootstrapName + "' does not exist");
        } else {
            LOG.info("from module '" + moduleName + "' deleted bootstrap '" + bootstrapName + "' (noderef=" + nodeRef + ")");
        }
    }

    private NodeRef getNodeRef(String xpath) {
        LOG.info("getNodeRef()...");
        return getNodeRef(xpath, getGeneralService().getStore());
    }

    private NodeRef getNodeRef(String xpath, StoreRef store) {
        LOG.info("getNodeRef()...");
        return getGeneralService().getNodeRef(xpath, store);
    }

    private void deleteChildren(NodeRef deletableNodeRef) {
        for (ChildAssociationRef childAssociationRef : getNodeService().getChildAssocs(deletableNodeRef)) {
            NodeRef childRef = childAssociationRef.getChildRef();
            getNodeService().deleteNode(childRef);
            LOG.info("deleted node " + childRef);
        }
    }

    private List<String> storeRefs = null;

    public List<String> getStoreRefs() {
        LOG.info("getStoreRefs()...");
        if (storeRefs == null) {
            LOG.info("getNodeService().getStores() size: " + getNodeService().getStores().size());
            List<StoreRef> stores = getNodeService().getStores();
            storeRefs = new ArrayList<String>(stores.size());
            for (StoreRef storeRef : stores) {
                LOG.info("StoreRef: " + storeRef);
                storeRefs.add(storeRef.toString());
            }
        }
        return storeRefs;
    }

    public void runNightly0230DataMaintenanceJobNow(@SuppressWarnings("unused") ActionEvent event) throws JobExecutionException {
        new NightlyDataFixJob().execute(null);
    }

    public void runNightly0300IndexMaintenanceJobNow(@SuppressWarnings("unused") ActionEvent event) {
        LuceneIndexBackupComponent luceneIndexBackupComponent = BeanHelper.getSpringBean(LuceneIndexBackupComponent.class, "luceneIndexBackupComponent");
        new LuceneIndexBackupJob().executeInternal(luceneIndexBackupComponent);
    }

    public void runMergeNow(ActionEvent event) {
        final StoreRef storeRef = new StoreRef(ActionUtil.getParam(event, "storeRef"));
        final LuceneIndexerAndSearcher indexerAndSearcher = BeanHelper.getSpringBean(LuceneIndexerAndSearcher.class, "admLuceneIndexerAndSearcherFactory");
        getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Void>() {
            @Override
            public Void execute() throws Throwable {
                Indexer indexer = indexerAndSearcher.getIndexer(storeRef);
                IndexInfo indexInfo = getIndexInfo(indexer);
                runMergeNow(indexInfo);
                printIndexInfo(indexInfo);
                return null;
            }
        });
    }

    private void runMergeNow(IndexInfo indexInfo) {
        LOG.info("Scheduling special merge to run on indexInfo: " + indexInfo);
        indexInfo.runMergeNow();

        // XXX sõltuvalt mis selgub öise indekseerimisaktiivsuse kohta võib olla vajalik ka protsessi jooksmise ajal määrata mergerTargetOverlaysBlockingFactor väärtus suuremaks et
        // kasutajaid mitte blokeerida ja võibolla lubada Lucenel rohkem mälu kasutada et indeksite ümber kirjutamise effektiivust tõsta.
    }

    private IndexInfo getIndexInfo(Indexer indexer) throws NoSuchFieldException, IllegalAccessException {
        Field indexInfoField = AbstractLuceneBase.class.getDeclaredField("indexInfo");
        indexInfoField.setAccessible(true);
        return (IndexInfo) indexInfoField.get(indexer);
    }

    public void executeDocumentUpdater(ActionEvent event) throws Throwable {
        final DocumentUpdater documentUpdater = BeanHelper.getSpringBean(DocumentUpdater.class, "documentUpdater9");
        documentUpdater.executeUpdaterInBackground();
    }

    public void executeSearchableSendInfoUpdater(ActionEvent event) throws Throwable {
        final SearchableSendInfoUpdater searchableSendInfoUpdater = BeanHelper.getSpringBean(SearchableSendInfoUpdater.class, "searchableSendInfoUpdater");
        searchableSendInfoUpdater.executeUpdaterInBackground();
    }

    public void executeCompoundWorkflowOwnerPropsUpdater(ActionEvent event) throws Throwable {
        final CompoundWorkflowOwnerPropsUpdater compoundWorkflowOwnerPropsUpdater = BeanHelper.getSpringBean(CompoundWorkflowOwnerPropsUpdater.class,
                "compoundWorkflowOwnerPropsUpdater6");
        compoundWorkflowOwnerPropsUpdater.executeUpdaterInBackground();
    }

    public void executeTaskUpdater(ActionEvent event) throws Throwable {
        final TaskUpdater taskUpdater = BeanHelper.getSpringBean(TaskUpdater.class, "taskUpdater");
        taskUpdater.executeUpdaterInBackground();
    }

    public void printIndexInfo(ActionEvent event) {
        LOG.info("printIndexInfo()...");
        final StoreRef storeRef = new StoreRef(ActionUtil.getParam(event, "storeRef"));
        final LuceneIndexerAndSearcher indexerAndSearcher = BeanHelper.getSpringBean(LuceneIndexerAndSearcher.class, "admLuceneIndexerAndSearcherFactory");
        getTransactionHelper().doInTransaction(new RetryingTransactionCallback<Void>() {
            @Override
            public Void execute() throws Throwable {
                Indexer indexer = indexerAndSearcher.getIndexer(storeRef);
                IndexInfo indexInfo = getIndexInfo(indexer);
                printIndexInfo(indexInfo);
                return null;
            }
        });
    }
    
    public void removeSendingStatus(ActionEvent event) {
    	String query = BeanHelper.getDocumentSearchService().getDvkOutboxQuery();
    	List<NodeRef> sendInfos = BeanHelper.getDocumentSearchService().searchSendInfo(query, 3, /* queryName */"documentsInOutbox");
    	for(NodeRef sendInfo : sendInfos) {
        	Map<QName, Serializable> properties = new HashMap<QName, Serializable>(1);
        	properties.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendInfo.SENT);
        	BeanHelper.getNodeService().addProperties(sendInfo, properties);
    	}
    }

    public void removeUnsentStatus(ActionEvent event) {
        List<NodeRef> pendingDocs = BeanHelper.getDocumentSearchService().searchRecipientFinishedDocuments();
        Map<QName, Serializable> properties = new HashMap<>(2);
        properties.put(DocumentCommonModel.Props.SEND_INFO_SEND_STATUS, SendInfo.SENT);
        properties.put(DocumentCommonModel.Props.SENT_OUT_AUTOMATICALLY, Boolean.TRUE);
        for (NodeRef pendingDoc : pendingDocs) {
            Node document = getDocumentService().getDocument(pendingDoc);
            List<Node> sendAssocs = document.getAllChildAssociations(DocumentCommonModel.Assocs.SEND_INFO);
            if (sendAssocs != null && !sendAssocs.isEmpty()) {
                for (Node assoc : sendAssocs) {
                    BeanHelper.getNodeService().addProperties(assoc.getNodeRef(), properties);
                }
            }
            BeanHelper.getNodeService().addProperties(pendingDoc, properties);
        }
    }
    
    public void removeSendingStatusBeforeDate(ActionEvent event) {
        Date sendingStatusDate = null;
        Date veryOldDate = null;
        try {
        	sendingStatusDateStr = StringUtils.trimToEmpty(sendingStatusDateStr);
        	sendingStatusDate = new SimpleDateFormat("dd.MM.yyyy").parse(sendingStatusDateStr);
        	veryOldDate = new SimpleDateFormat("dd.MM.yyyy").parse("01.01.2000");
        } catch (ParseException e) {
        	LOG.error("Cannot start updater because expected end date in format 'dd.MM.yyyy' but got '" + sendingStatusDate + "'");
        	return;
        }
        List<NodeRef> finishedDocs = BeanHelper.getDocumentSearchService().searchRecipientFinishedDocuments();
    	for(NodeRef docRef : finishedDocs) {
    		Date created = (Date) nodeService.getProperty(docRef, ContentModel.PROP_CREATED);
    		if(created.before(sendingStatusDate)) {
    			nodeService.setProperty(docRef, DocumentCommonModel.Props.FIRST_FINISHED_DATE, veryOldDate);
    		}
    	}
    }

    public void resetAllMarkedForDestruction(ActionEvent event) {
        LOG.info("resetAllMarkedForDestruction");
        List<NodeRef> storeNodeRefs = getArchivalStores();
        final List<NodeRef> volumes = new ArrayList<>();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(BeanHelper.getDataSource());
        String sql = "SELECT protocol, identifier, uuid FROM public.alf_node_properties AS prop JOIN public.alf_qname AS qname ON prop.qname_id=qname.id JOIN public.alf_node AS node ON node.id = node_id JOIN public.alf_store AS store ON store_id = store.id WHERE local_name = 'markedForDestruction' AND boolean_value = true;";
        jdbcTemplate.query(sql, new ParameterizedRowMapper<NodeRef>() {
            @Override
            public NodeRef mapRow(ResultSet rs, int rowNum) throws SQLException {
            	String protocol = rs.getString("protocol");
            	String identifier = rs.getString("identifier");
            	String uuid = rs.getString("uuid");
            	NodeRef ref = new NodeRef(protocol, identifier, uuid);
            	volumes.add(ref);
            	return null;
            }});
        
        for (NodeRef volume : volumes) {
        	nodeService.setProperty(volume, EventPlanModel.Props.MARKED_FOR_DESTRUCTION, Boolean.FALSE);
        }
        // hack for incorrect view id in the next request
        JspStateManagerImpl.ignoreCurrentViewSequenceHack();
    }
    
    private void printIndexInfo(IndexInfo indexInfo) {
        LOG.info("printIndexInfo()...");
        String indexInfoTextWithoutDate = indexInfo.toString() + indexInfo.dumpInfoAsString();
        indexInfoText = dateTimeFormat.format(System.currentTimeMillis()) + "\n" + indexInfoTextWithoutDate;
        LOG.info(indexInfoTextWithoutDate);
    }

    public void updateUsersAndGroups(ActionEvent event) {
        LOG.debug("Starting to update users and usergroups");
        UserRegistrySynchronizer userRegistrySynchronizer = getSpringBean(UserRegistrySynchronizer.class, "UserRegistrySynchronizer");
        userRegistrySynchronizer.synchronize(true);
        LOG.debug("Finished updating users and usergroups");
    }

    public void searchHolesAndIndex(ActionEvent event) {
        CustomReindexComponent customReindexComponent = BeanHelper.getSpringBean(CustomReindexComponent.class, "customReindexComponent");
        customReindexComponent.reindex();
    }

    public void executeCacheStatistics(ActionEvent event) throws JobExecutionException {
        (new EhCacheTracerJob()).execute(null);
    }
    
    public String getDocumentFromAdr(ActionEvent event) {
    	AdrRestApiService adrRestApiService = BeanHelper.getAdrRestApiService();
    	Pair<String, HttpStatus> documentJSON = adrRestApiService.getDocumentFromAdr(adrDocNodeRef);
    	return documentJSON.getFirst();
    }
    
    public String deleteDocumentFromAdr(ActionEvent event) {
    	AdrRestApiService adrRestApiService = BeanHelper.getAdrRestApiService();
    	return "";
    }

    protected RetryingTransactionHelper getTransactionHelper() {
        RetryingTransactionHelper helper = BeanHelper.getTransactionService().getRetryingTransactionHelper();
        helper.setMaxRetries(1);
        return helper;
    }

    protected TransactionService getTransactionService() {
        if (transactionService == null) {
            transactionService = BeanHelper.getTransactionService();
        }
        return transactionService;
    }

    protected SearchService getSearchService() {
        if (searchService == null) {
            searchService = BeanHelper.getSearchService();
        }
        return searchService;
    }

    protected NodeService getNodeService() {
        if (nodeService == null) {
            nodeService = BeanHelper.getNodeService();
        }
        return nodeService;
    }

    protected GeneralService getGeneralService() {
        if (generalService == null) {
            generalService = BeanHelper.getGeneralService();
        }
        return generalService;
    }

    private void atsTestib(@SuppressWarnings("unused") ActionEvent e) {
        // ära puutu seda meetodit!
    }

}
