package ee.webmedia.alfresco.common.web;

import static ee.webmedia.alfresco.common.web.BeanHelper.*;
import static ee.webmedia.alfresco.common.web.BeanHelper.getNodeService;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import javax.faces.model.SelectItem;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.ui.common.component.PickerSearchParams;
import org.apache.commons.lang.ArrayUtils;

import ee.webmedia.alfresco.addressbook.model.AddressbookModel;
import ee.webmedia.alfresco.user.model.Authority;
import ee.webmedia.alfresco.utils.MessageUtil;
import ee.webmedia.alfresco.utils.WebUtil;

public class UserContactGroupSearchBean implements Serializable {
    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(UserContactGroupSearchBean.class);
    private static final long serialVersionUID = 1L;

    public static final String BEAN_NAME = "UserContactGroupSearchBean";
    public static final String FILTER_INDEX_SEPARATOR = "¤";

    private SelectItem[] usersGroupsFilters;
    private SelectItem[] contactsGroupsFilters;
    private SelectItem[] usersContactsFilters;
    private SelectItem[] usersGroupsContactsGroupsFilters;

    /**
     * Property accessed by the Generic Picker component.
     * 
     * @return the array of filter options to show in the users/groups picker
     */
    public SelectItem[] getUsersGroupsFilters() {
        if (usersGroupsFilters == null) {
            usersGroupsFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.USERS_FILTER.getId(), MessageUtil.getMessage("task_owner_users")),
                    new SelectItem(UserContactGroupSearchFilters.USER_GROUPS_FILTER.getId(), MessageUtil.getMessage("task_owner_usergroups")),
            };
        }
        return usersGroupsFilters;
    }

    /**
     * Property accessed by the Generic Picker component.
     * 
     * @return the array of filter options to show in the users/groups picker
     */
    public SelectItem[] getContactsGroupsFilters() {
        if (contactsGroupsFilters == null) {
            contactsGroupsFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.CONTACTS_FILTER.getId(), MessageUtil.getMessage("task_owner_contacts")),
                    new SelectItem(UserContactGroupSearchFilters.CONTACT_GROUPS_FILTER.getId(), MessageUtil.getMessage("task_owner_contactgroups")),
            };
        }
        return contactsGroupsFilters;
    }

    /**
     * Property accessed by the Generic Picker component.
     * 
     * @return the array of filter options to show in the users/groups picker
     */
    public SelectItem[] getUsersContactsFilters() {
        if (usersContactsFilters == null) {
            usersContactsFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.USERS_FILTER.getId(), MessageUtil.getMessage("task_owner_users")),
                    new SelectItem(UserContactGroupSearchFilters.CONTACTS_FILTER.getId(), MessageUtil.getMessage("task_owner_contacts")),
            };
        }
        return usersContactsFilters;
    }

    /**
     * Property accessed by the Generic Picker component.
     * 
     * @return the array of filter options to show in the users/groups picker
     */
    public SelectItem[] getUsersGroupsContactsGroupsFilters() {
        if (usersGroupsContactsGroupsFilters == null) {
            usersGroupsContactsGroupsFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.USERS_FILTER.getId(), MessageUtil.getMessage("task_owner_users")),
                    new SelectItem(UserContactGroupSearchFilters.USER_GROUPS_FILTER.getId(), MessageUtil.getMessage("task_owner_usergroups")),
                    new SelectItem(UserContactGroupSearchFilters.CONTACTS_FILTER.getId(), MessageUtil.getMessage("task_owner_contacts")),
                    new SelectItem(UserContactGroupSearchFilters.CONTACT_GROUPS_FILTER.getId(), MessageUtil.getMessage("task_owner_contactgroups")),
            };
        }
        return usersGroupsContactsGroupsFilters;
    }
    
    public SelectItem[] getContactsFilters() {
        if (usersGroupsContactsGroupsFilters == null) {
            usersGroupsContactsGroupsFilters = new SelectItem[] {
                    new SelectItem(UserContactGroupSearchFilters.CONTACTS_FILTER.getId(), MessageUtil.getMessage("task_owner_contacts")),
            };
        }
        return usersGroupsContactsGroupsFilters;
    }

    /*
     * pickerCallback methods
     */

    /**
     *
     * @param params
     * @return
     */
    public SelectItem[] searchAll(PickerSearchParams params) {
        return searchAll(params, false);
    }

    public SelectItem[] searchAllWithAdminsAndDocManagers(PickerSearchParams params) {
        return searchAll(params, true);
    }

    public SelectItem[] searchGroupsWithAdminsAndDocManagers(PickerSearchParams params) {
        return searchGroups(params, true);
    }

    public SelectItem[] searchAllWithoutLogOnUser(PickerSearchParams params) {
        SelectItem[] results = BeanHelper.getUserListDialog().searchOtherUsers(params);
        return results;
    }

    /*
     * Methods that can be used programmatically
     */

    // TODO merge CompoundWorkflowDefinitionDialog#executeOwnerSearch to here
    private SelectItem[] searchAll(PickerSearchParams params, boolean withAdminsAndDocManagers) {
        SelectItem[] results = new SelectItem[0];
        if (params.isFilterIndex(UserContactGroupSearchFilters.USERS_FILTER.getId())) {
            results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserListDialog().searchUsers(params));
        }
        if (params.isFilterIndex(UserContactGroupSearchFilters.USER_GROUPS_FILTER.getId())) {
            results = (SelectItem[]) ArrayUtils.addAll(results, searchGroups(params, withAdminsAndDocManagers));
        }
        if (params.isFilterIndex(UserContactGroupSearchFilters.CONTACTS_FILTER.getId())) {
            results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchContacts(params));
        }
        if (params.isFilterIndex(UserContactGroupSearchFilters.CONTACT_GROUPS_FILTER.getId())) {
            results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchContactGroups(params));
        }

        return results;
    }

    /**
     * Search groups
     * @param params
     * @param withAdminsAndDocManagers
     * @return
     */
    public SelectItem[] searchGroups(PickerSearchParams params, boolean withAdminsAndDocManagers) {
        List<Authority> results = BeanHelper.getDocumentSearchService().searchAuthorityGroups(params.getSearchString(), true, withAdminsAndDocManagers, params.getLimit());
        SelectItem[] selectItems = new SelectItem[results.size()];
        int i = 0;
        for (Authority authority : results) {
            String auth = params.isIncludeFilterIndex() ? (authority.getAuthority() + FILTER_INDEX_SEPARATOR + UserContactGroupSearchFilters.USER_GROUPS_FILTER.getId()) : authority.getAuthority();
            selectItems[i++] = new SelectItem(auth, authority.getName());
        }
        WebUtil.sort(selectItems);
        return selectItems;
    }

    /**
     * Search groups
     * @param param
     * @param limit
     * @return
     */
    public List<Pair<String, String>> searchGroups(String param, int limit) {
        List<Authority> authorities = BeanHelper.getDocumentSearchService().searchAuthorityGroups(param, true, true, limit);
        List<Pair<String, String>> result = new ArrayList<>(authorities.size());
        for (Authority a : authorities) {
            result.add(Pair.newInstance(a.getName(), a.getAuthority()));
        }
        return result;
    }

    /*
     * preprocessCallback methods
     */
    public List<Pair<String, String>> preprocessResultsToNodeRefs(int filterIndex, String[] results) {
        List<Pair<String, String>> processedResult = new ArrayList<Pair<String, String>>();
        if (results != null) {
            for (String result : results) {
                if (filterIndex == UserContactGroupSearchFilters.USERS_FILTER.getId()) {
                    // Replace user name with reference to the person node
                    NodeRef nodeRef = getUserService().getPerson(result);
                    if (nodeRef != null) {
                        processedResult.add(new Pair<String, String>(null, nodeRef.toString()));
                    }
                } else if (filterIndex == UserContactGroupSearchFilters.USER_GROUPS_FILTER.getId()) {
                    // Add all users contained in user group and replace user names with reference to the person node
                    Set<String> auths = getUserService().getUserNamesInGroup(result);
                    String groupName = null;
                    Authority group = getUserService().getAuthorityOrNull(result);
                    if (group != null) {
                        groupName = group.getName();
                    }
                    for (String auth : auths) {
                        NodeRef nodeRef = getUserService().getPerson(auth);
                        if (nodeRef != null) {
                            processedResult.add(new Pair<String, String>(groupName, nodeRef.toString()));
                        }
                    }
                } else if (filterIndex == UserContactGroupSearchFilters.CONTACTS_FILTER.getId()) {
                    // Add contact
                    processedResult.add(new Pair<String, String>(null, result));
                } else if (filterIndex == UserContactGroupSearchFilters.CONTACT_GROUPS_FILTER.getId()) {
                    // Add all contacts contained in contact group
                    NodeRef contactGroupRef = new NodeRef(result);
                    List<NodeRef> contacts = getAddressbookService().getContactGroupContents(contactGroupRef);
                    String groupName = (String) BeanHelper.getNodeService().getProperty(contactGroupRef, AddressbookModel.Props.GROUP_NAME);
                    for (NodeRef contact : contacts) {
                        processedResult.add(new Pair<String, String>(groupName, contact.toString()));
                    }
                } else {
                    throw new RuntimeException("filterIndex out of range: " + filterIndex);
                }
            }
        }
        return processedResult;
    }

    public List<Pair<String, String>> preprocessResultsToNames(int filterIndex, String[] results) {
        List<Pair<String, String>> processedResult = preprocessResultsToNodeRefs(filterIndex, results);
        for (Iterator<Pair<String, String>> iterator = processedResult.iterator(); iterator.hasNext();) {
            Pair<String, String> pair = iterator.next();
            Object nameObj = getUserContactMappingService().getMappedNameValue(new NodeRef(pair.getSecond()));
            if (nameObj != null && nameObj instanceof String) {
                NodeRef userOrContactRef = new NodeRef(pair.getSecond());
                QName type = getNodeService().getType(userOrContactRef);

                boolean isPerson = ContentModel.TYPE_PERSON.equals(type);
                boolean isOrganization = AddressbookModel.Types.ORGANIZATION.equals(type);
                boolean isOrgPerson = AddressbookModel.Types.ORGPERSON.equals(type);
                boolean isPrivPerson = AddressbookModel.Types.PRIV_PERSON.equals(type);
                log.debug(String.format("preprocessResultsToNames :: isPerson=%s, isOrganization=%s, isOrgPerson=%s, isPrivPerson=%s, value=%s", isPerson,
                        isOrganization, isOrgPerson, isPrivPerson, nameObj));

                if (isPerson || isPrivPerson) {
                    pair.setFirst("UserOrPrivPerson");
                    pair.setSecond(null);
                } else if (isOrganization) {
                    pair.setFirst("OrgType");
                    pair.setSecond((String) nameObj);
                } else if (isOrgPerson) {
                    try {
                        Map<String, String> map = new ObjectMapper().readValue((String) nameObj, new TypeReference<HashMap<String, String>>() {});
                        pair.setFirst("OrgPerson");
                        pair.setSecond(map.get("recipientName"));
                    } catch (IOException exp) {
                        log.error(exp);
                    }
                }
            } else {
                iterator.remove();
            }
        }
        return processedResult;
    }

}
