<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ page buffer="32kb" contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false"%>

<f:verbatim>
	<script type="text/javascript">

	setupCheckBoxes(".selectAllDocumentHeaders", ".documentHeaderSelectable");
	setupCheckBoxes(".selectAllTaskHeaders", ".taskHeaderSelectable");
	setupCheckBoxes(".selectAllWorkflowHeaders", ".wrokflowHeaderSelectable");
	setupCheckBoxes(".selectAllCaseHeaders", ".caseHeaderSelectable");
	
	function setupCheckBoxes(checkboxHeaderClassName, headerSelectableClassName) {
		$jQ(document).ready(function(){
				var checkHeader = true;
				$jQ(headerSelectableClassName).each(function(i, obj) {
					var selectable = $jQ(obj);
					selectable.change(function() {
						var allItemsChecked = true;
						$jQ(headerSelectableClassName).each(function(i2, obj2) {
							if (!$jQ(obj2).prop('checked')) {
								allItemsChecked = false;
							}
						});
						if (allItemsChecked) {
							$jQ(checkboxHeaderClassName).prop('checked', true);
						} else {
							$jQ(checkboxHeaderClassName).prop('checked', false);
						}
					});
					if (!selectable.prop('checked')) {
						checkHeader = false;
					}
				});
			   $jQ(checkboxHeaderClassName).prop('checked', checkHeader);
			   $jQ(checkboxHeaderClassName).change(function() {
			      $jQ(headerSelectableClassName).prop('checked',$jQ(this).prop('checked'));
			   });
			   var aTags = document.getElementsByTagName("a");
				for (var i = 0; i < aTags.length; i++) {
					  var text = aTags[i].textContent;
					  if (text == "export_document_csv" || text == "export_task_csv" || text == "export_workflow_csv" || text == "export_casefile_csv") {
					    aTags[i].style.display = 'none';
					    aTags[i].id = text;
					  }
				}
			});
	}
	</script>
</f:verbatim>
