package ee.webmedia.alfresco.common.propertysheet.renderer;

import com.ibatis.common.logging.Log;
import com.ibatis.common.logging.LogFactory;
import ee.webmedia.alfresco.utils.WebUtil;
import org.apache.myfaces.shared_impl.component.EscapeCapable;
import org.apache.myfaces.shared_impl.renderkit.JSFAttr;
import org.apache.myfaces.shared_impl.renderkit.RendererUtils;
import org.apache.myfaces.shared_impl.renderkit.html.HTML;
import org.apache.myfaces.shared_impl.renderkit.html.HtmlRenderer;
import org.apache.myfaces.shared_impl.renderkit.html.HtmlRendererUtils;
import org.apache.xalan.templates.Constants;

import javax.faces.component.*;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.convert.ConverterException;
import java.io.IOException;

public class HtmlTextRendererBase extends HtmlRenderer {
    private static final Log log = LogFactory.getLog(HtmlTextRendererBase.class);

    @Override
    public void encodeEnd(FacesContext facesContext, UIComponent component)
            throws IOException {
        org.apache.myfaces.shared_impl.renderkit.RendererUtils.checkParamValidity(facesContext, component, null);

        if (component instanceof EditableValueHolder) {
            renderInput(facesContext, component);
        } else if (component instanceof ValueHolder) {
            renderOutput(facesContext, component);
        } else {
            throw new IllegalArgumentException("Unsupported component class " + component.getClass().getName());
        }
    }

    protected static void renderOutput(FacesContext facesContext, UIComponent component)
            throws IOException {
        String text = org.apache.myfaces.shared_impl.renderkit.RendererUtils.getStringValue(facesContext, component);
        boolean escape;
        if (component instanceof HtmlOutputText || component instanceof EscapeCapable) {
            escape = ((HtmlOutputText) component).isEscape();
        } else {
            escape = RendererUtils.getBooleanAttribute(component, org.apache.myfaces.shared_impl.renderkit.JSFAttr.ESCAPE_ATTR,
                    true); //default is to escape
        }

        if (text != null && text.length() > 0 && !(text.contains(Constants.ATTRNAME_HREF))) {
            text = WebUtil.processLinks(text);
        }

        renderOutputText(facesContext, component, text, escape);
    }


    public static void renderOutputText(FacesContext facesContext,
                                        UIComponent component,
                                        String text,
                                        boolean escape)
            throws IOException {
        if (text != null) {
            ResponseWriter writer = facesContext.getResponseWriter();
            boolean span = false;

            if (component.getId() != null && !component.getId().startsWith(UIViewRoot.UNIQUE_ID_PREFIX)) {
                span = true;

                writer.startElement(HTML.SPAN_ELEM, component);

                HtmlRendererUtils.writeIdIfNecessary(writer, component, facesContext);

                HtmlRendererUtils.renderHTMLAttributes(writer, component, HTML.COMMON_PASSTROUGH_ATTRIBUTES);

            } else {
                span = HtmlRendererUtils.renderHTMLAttributesWithOptionalStartElement(writer, component,
                        HTML.SPAN_ELEM, HTML.COMMON_PASSTROUGH_ATTRIBUTES);
            }

            writer.write(text);

            if (span) {
                writer.endElement(org.apache.myfaces.shared_impl.renderkit.html.HTML.SPAN_ELEM);
            }
        }
    }


    protected void renderInput(FacesContext facesContext, UIComponent component)
            throws IOException {
        ResponseWriter writer = facesContext.getResponseWriter();

        String clientId = component.getClientId(facesContext);
        String value = org.apache.myfaces.shared_impl.renderkit.RendererUtils.getStringValue(facesContext, component);

        writer.startElement(HTML.INPUT_ELEM, component);
        writer.writeAttribute(HTML.ID_ATTR, clientId, null);
        writer.writeAttribute(HTML.NAME_ATTR, clientId, null);
        writer.writeAttribute(HTML.TYPE_ATTR, HTML.INPUT_TYPE_TEXT, null);
        if (value != null) {
            writer.writeAttribute(HTML.VALUE_ATTR, value, JSFAttr.VALUE_ATTR);
        }

        HtmlRendererUtils.renderHTMLAttributes(writer, component, HTML.INPUT_PASSTHROUGH_ATTRIBUTES_WITHOUT_DISABLED);
        if (isDisabled(facesContext, component)) {
            writer.writeAttribute(HTML.DISABLED_ATTR, Boolean.TRUE, null);
        }

        writer.endElement(HTML.INPUT_ELEM);
    }

    protected boolean isDisabled(FacesContext facesContext, UIComponent component) {
        //TODO: overwrite in extended HtmlTextRenderer and check for enabledOnUserRole
        if (component instanceof HtmlInputText) {
            return ((HtmlInputText) component).isDisabled();
        } else {
            return org.apache.myfaces.shared_impl.renderkit.RendererUtils.getBooleanAttribute(component, HTML.DISABLED_ATTR, false);
        }
    }


    public void decode(FacesContext facesContext, UIComponent component) {
        RendererUtils.checkParamValidity(facesContext, component, null);

        if (component instanceof EditableValueHolder) {
            HtmlRendererUtils.decodeUIInput(facesContext, component);
        } else if (component instanceof ValueHolder) {
            //nothing to decode
        } else {
            throw new IllegalArgumentException("Unsupported component class " + component.getClass().getName());
        }
    }


    public Object getConvertedValue(FacesContext facesContext, UIComponent component, Object submittedValue) throws ConverterException {
        org.apache.myfaces.shared_impl.renderkit.RendererUtils.checkParamValidity(facesContext, component, UIOutput.class);
        return RendererUtils.getConvertedUIOutputValue(facesContext,
                (UIOutput) component,
                submittedValue);
    }

}
