package ee.webmedia.alfresco.common.propertysheet.datepicker;

import javax.faces.convert.Converter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateTimePickerConverter extends DatePickerConverter implements Converter {

    public static final String CONVERTER_ID = DateTimePickerConverter.class.getCanonicalName();
    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm";
    private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(DateTimePickerConverter.class);
    public static SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat(DATE_TIME_FORMAT);

    static {
        simpleDateTimeFormat.setLenient(false);
    }

    @Override
    public DateFormat getDateFormat() {
        return simpleDateTimeFormat;
    }

}
