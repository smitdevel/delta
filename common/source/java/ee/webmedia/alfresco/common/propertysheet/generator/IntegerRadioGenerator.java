package ee.webmedia.alfresco.common.propertysheet.generator;

import static ee.webmedia.alfresco.common.web.BeanHelper.getApplicationConstantsBean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItem;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.convert.IntegerConverter;

import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.generator.BaseComponentGenerator;
import org.alfresco.web.ui.repo.component.property.PropertySheetItem;
import org.alfresco.web.ui.repo.component.property.UIPropertySheet;
import org.apache.commons.lang.StringUtils;

import ee.webmedia.alfresco.common.propertysheet.multivalueeditor.PropsBuilder;
import ee.webmedia.alfresco.utils.ComponentUtil;
import ee.webmedia.alfresco.utils.MessageUtil;

/**
 * Generator for integer properties that generates radiobuttons instead of checkbox. <br>
 * Follwing show-property configuration element attributes are used to display labels radiobuttons:<br>
 * 1) {@link IntegerRadioGenerator#ATTR_LABELS}
 */
public class IntegerRadioGenerator extends BaseComponentGenerator {

    private static final String ATTR_LABELS = "labels";

    @Override
    public UIComponent generate(FacesContext context, String id) {
        throw new RuntimeException("Not called");
    }

    @Override
    protected UIComponent createComponent(FacesContext context, UIPropertySheet propertySheet, final PropertySheetItem item) {
    	String separator = PropsBuilder.DEFAULT_OPTIONS_SEPARATOR;
        Integer value = getValue(propertySheet, item);

    	List<String> displayValues = new ArrayList<>();
    	
        String labels = getCustomAttributes().get(ATTR_LABELS);
        if (labels.contains(separator)) {
        	for (String label : labels.split(separator)) {
            	displayValues.add(MessageUtil.getMessage(label));
        	}
        } else {
        	displayValues.add(MessageUtil.getMessage(labels));
        }
        
        final HtmlSelectOneRadio selectComponent = new HtmlSelectOneRadio();
        final List<UIComponent> radio = new ArrayList<>();
        
        for (int i = 0; i < displayValues.size(); i++) {
        	UISelectItem select = (UISelectItem) context.getApplication().createComponent("javax.faces.SelectItem");
            select.setItemValue(i);
            select.setItemLabel(displayValues.get(i));
            radio.add(select);
        }

        ComponentUtil.addChildren(selectComponent, radio);

        selectComponent.setValue(value != null ? value : 0);

        FacesHelper.setupComponentId(context, selectComponent, item.getName());
        selectComponent.setConverter(new IntegerConverter());
        selectComponent.setLayout("pageDirection");// radiobuttons positioned under each-other
        return selectComponent;
    }

    private UIComponent createReadOnlyComponent(FacesContext context, Integer value, String label) {
        HtmlOutputText outputText = (HtmlOutputText) context.getApplication().createComponent("javax.faces.HtmlOutputText");
        // FacesHelper.setupComponentId(context, outputText, id);
        // outputText.setEscape(false);
        String displayValue;
        if (value == null) {
            displayValue = "null";
        } else {
            displayValue = label;
        }
        outputText.setValue(displayValue);
        return outputText;
    }

}
