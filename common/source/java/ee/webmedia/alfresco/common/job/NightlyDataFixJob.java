package ee.webmedia.alfresco.common.job;

import ee.webmedia.alfresco.common.bootstrap.InvalidNodeFixerBootstrap;
import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.docdynamic.bootstrap.DeleteDraftsBootstrap;
import org.alfresco.repo.search.impl.lucene.AbstractLuceneIndexerAndSearcherFactory;
import org.alfresco.repo.search.impl.lucene.AbstractLuceneIndexerAndSearcherFactory.LuceneIndexBackupJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Finds and fixes problems in data. Runs before {@link LuceneIndexBackupJob}, and prevents {@link LuceneIndexBackupJob} from starting before this job completes.
 */
public class NightlyDataFixJob implements StatefulJob {
    private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(NightlyDataFixJob.class);
    public static Lock nightlyMaintenanceJobLock = new ReentrantLock();

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        log.info("Scheduled job NightlyDataFixJob STARTED!");
        nightlyMaintenanceJobLock.lock();
        try {
            log.info("DeleteDraftsBootstrap... START!");
            DeleteDraftsBootstrap deleteDraftsBootstrap = BeanHelper.getSpringBean(DeleteDraftsBootstrap.class, "deleteDraftsBootstrap");
            deleteDraftsBootstrap.executeInternal(false);
            log.info("DeleteDraftsBootstrap... DONE!");
            log.info("InvalidNodeFixerBootstrap... START!");
            InvalidNodeFixerBootstrap invalidNodeFixerBootstrap = BeanHelper.getSpringBean(InvalidNodeFixerBootstrap.class, "invalidNodeFixerBootstrap");
            invalidNodeFixerBootstrap.execute();
            log.info("InvalidNodeFixerBootstrap... DONE!");
            log.info("LuceneIndexBackupJob... START!");
            new LuceneIndexBackupJob().executeInternal(BeanHelper.getSpringBean(AbstractLuceneIndexerAndSearcherFactory.LuceneIndexBackupComponent.class, "luceneIndexBackupComponent"));
            log.info("LuceneIndexBackupJob... DONE!");
        }catch (Exception e){
            log.error(e.getMessage(), e);
        } finally {
            nightlyMaintenanceJobLock.unlock();
        }
        log.info("Scheduled job NightlyDataFixJob ENDED!");
    }

}
