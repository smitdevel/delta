package ee.webmedia.alfresco.adr.bootstrap;

import static ee.webmedia.alfresco.utils.SearchUtil.generateDatePropertyRangeQuery;
import static ee.webmedia.alfresco.utils.SearchUtil.generatePropertyExactNotQuery;
import static ee.webmedia.alfresco.utils.SearchUtil.generatePropertyExactQuery;
import static ee.webmedia.alfresco.utils.SearchUtil.generateTypeQuery;
import static ee.webmedia.alfresco.utils.SearchUtil.joinQueryPartsAnd;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.model.SelectItem;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Node;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.csvreader.CsvWriter;

import ee.webmedia.alfresco.classificator.enums.PublishToAdr;
import ee.webmedia.alfresco.common.bootstrap.AbstractNodeUpdater;
import ee.webmedia.alfresco.common.service.BulkLoadNodeService;
import ee.webmedia.alfresco.docadmin.model.DocumentAdminModel;
import ee.webmedia.alfresco.docadmin.service.DocumentAdminService;
import ee.webmedia.alfresco.docadmin.service.DocumentType;
import ee.webmedia.alfresco.docadmin.service.DocumentTypeVersion;
import ee.webmedia.alfresco.docadmin.service.Field;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.document.model.DocumentCommonModel;
 
public class PublishToAdrUpdater extends AbstractNodeUpdater {

	private static final String  PUBLISH_TO_ADR_TO_ADR = "toAdr";
	private static final String  PUBLISH_TO_ADR_NOT_TO_ADR = "notToAdr";
	private static final String  PUBLISH_TO_ADR_REQ_FOR_INFO = "reqForInfo";
	
    private DocumentAdminService documentAdminService;
    private BulkLoadNodeService bulkLoadNodeService;

    private Date startDate;
    private Date endDate;
    private String publishToAdrValue;
    private Map<String, Boolean> docTypesCache;
    private Map<NodeRef, Node> batchNodes;
    private List<String> docTypes;
    private List<SelectItem> foundDocTypes;
    private String inputFolderPath;
    
    private Set<NodeRef> nodesUpdated = new HashSet<NodeRef>();
    private Set<NodeRef> nodesFailed = new HashSet<NodeRef>();
    private File nodesUpdatedFile;
    private File nodesFailedFile;

    private static final Set<QName> DOC_TYPE_PROPS = new HashSet<>(Arrays.asList(DocumentAdminModel.Props.OBJECT_TYPE_ID, DocumentAdminModel.Props.OBJECT_TYPE_VERSION_NR));

    private void initData() {
    	nodesUpdatedFile = loadNodesUpdatedFile();
        nodesFailedFile = loadNodesFailedFile();
    }
    
    public File loadNodesUpdatedFile() {
        return new File(inputFolder, getBaseFileName() + "Updated.csv");
    }

    public File loadNodesFailedFile() {
        return new File(inputFolder, getBaseFileName() + "Failed.csv");
    }
    
    @Override
    protected List<ResultSet> getNodeLoadingResultSet() throws Exception {

        String query = joinQueryPartsAnd(
                generateTypeQuery(DocumentCommonModel.Types.DOCUMENT),
                generateDatePropertyRangeQuery(startDate, addOneDay(endDate), DocumentCommonModel.Props.REG_DATE_TIME)
                );
        if (docTypes != null && !docTypes.isEmpty()) {
        	query = joinQueryPartsAnd(query,
        			generatePropertyExactQuery(DocumentAdminModel.Props.OBJECT_TYPE_ID, docTypes));
        }
        query = query + " AND " + generatePropertyExactNotQuery(DocumentDynamicModel.Props.PUBLISH_TO_ADR, mapPublishToAdr());
        
        List<ResultSet> result = new ArrayList<>();
        for (StoreRef storeRef : generalService.getAllStoreRefsWithTrashCan()) {
            result.add(searchService.query(storeRef, SearchService.LANGUAGE_LUCENE, query));
        }
        return result;
    }
    
    private Date addOneDay(Date date) {
    	Calendar c = Calendar.getInstance(); 
    	c.setTime(date); 
    	c.add(Calendar.DATE, 1);
    	return c.getTime();
    	
    }

    @Override
    protected void initializeBeforeUpdating() throws Exception {
        super.initializeBeforeUpdating();
        if (CollectionUtils.isEmpty(nodes)) {
            return;
        }
        docTypesCache = new HashMap<>();
    }

    @Override
    protected void doBeforeBatchUpdate(List<NodeRef> batchList) {
        if (CollectionUtils.isEmpty(batchList)) {
            return;
        }
        batchNodes = bulkLoadNodeService.loadNodes(batchList, DOC_TYPE_PROPS);
    }

    @Override
    protected String[] updateNode(NodeRef docRef) throws Exception {
        Node docNode = batchNodes.get(docRef);
        if (docNode == null) {
            log.warn("Did not find node from database: " + docRef);
            return null;
        }
        String docPublishToAdr = (String)nodeService.getProperty(docRef, DocumentDynamicModel.Props.PUBLISH_TO_ADR);
        if (mapPublishToAdr().equals(docPublishToAdr)) {
        	log.debug("Document " + docRef + " already has publishToAdr = " + docPublishToAdr);
            return null;
        }
        		
        Map<String, Object> props = docNode.getProperties();
        String typeId = (String) props.get(DocumentAdminModel.Props.OBJECT_TYPE_ID);
        Integer versionNr = (Integer) props.get(DocumentAdminModel.Props.OBJECT_TYPE_VERSION_NR);
        if (typeId == null || versionNr == null) {
            log.warn("Unable to update document " + docRef);
            nodesFailed.add(docRef);
            return new String[] { docRef.toString(), "typeId orversionNr are missing" };
        }
        Boolean hasPublishToAdrField = docTypesCache.get(typeId + "-" + versionNr);
        if (hasPublishToAdrField == null) {
            Pair<DocumentType, DocumentTypeVersion> typeAndVersion = documentAdminService.getDocumentTypeAndVersion(typeId, versionNr, false);
            DocumentTypeVersion version = typeAndVersion.getSecond();
            Collection<Field> fields = version.getFieldsById(Collections.singleton(DocumentDynamicModel.Props.PUBLISH_TO_ADR.getLocalName()));
            hasPublishToAdrField = CollectionUtils.isNotEmpty(fields) ? true : false;
            docTypesCache.put(typeId + "-" + versionNr, hasPublishToAdrField);
        }
        if (!hasPublishToAdrField) {
            log.info("Document " + docRef + " (type: " + typeId + ", ver: " + versionNr + ") does not have publishToAdr field, skipping");
            nodesFailed.add(docRef);
            return new String[] { docRef.toString(), "docType does not have publishToAdr" };
        }
        nodeService.setProperty(docRef, DocumentDynamicModel.Props.PUBLISH_TO_ADR, mapPublishToAdr());

        nodesUpdated.add(docRef);
        return new String[] { docRef.toString(), publishToAdrValue };
    }
    
    @Override
    protected void executeAfterCommit(final File completedFile, final CsvWriterClosure closure) {
        super.executeAfterCommit(completedFile, closure);
        writeNodeRefsToFile(nodesUpdatedFile, nodesUpdated);
        writeNodeRefsToFile(nodesFailedFile, nodesFailed);
    }

    public void writeNodeRefsToFile(File file, Set<NodeRef> nodeRefs) {
        try {
            // Mark nodes for deleting
            CsvWriter writer = new CsvWriter(new FileWriter(file, true), CSV_SEPARATOR);
            try {
                for (NodeRef nodeToDelete : nodeRefs) {
                    writer.writeRecord(new String[] { nodeToDelete.toString() });
                }
                nodeRefs.clear();
            } finally {
                writer.close();
            }
        } catch (IOException e) {
            throw new RuntimeException("Error writing file '" + file + "': " + e.getMessage(), e);
        }
    }
    
    @Override
    protected String[] getCsvFileHeaders() {
        return new String[] {
                "documentNodeRef",
                "Message"
                };
    }

    @Override
    protected void executeUpdater() throws Exception {
    	Assert.notNull(startDate, "Start date must not be blank");
        Assert.notNull(endDate, "End date must not be blank");
        Assert.hasText(publishToAdrValue, "PublishToAdr value must not be blank");
        Assert.notNull(inputFolder, "Input folder path must not be blank");
        initData();
        super.executeUpdater();
        resetFields();
    }

    @Override
    protected boolean usePreviousInputState() {
        return false;
    }

    private void resetFields() {
    	nodesUpdated = new HashSet<NodeRef>();
        nodesFailed = new HashSet<NodeRef>();
        
    	startDate = null;
    	endDate = null;
        docTypesCache = null;
        batchNodes = null;
    }
    
    private String mapPublishToAdr() {
    	if (PUBLISH_TO_ADR_TO_ADR.equals(publishToAdrValue)) {
    		return PublishToAdr.TO_ADR.getValueName();
    	} else if (PUBLISH_TO_ADR_NOT_TO_ADR.equals(publishToAdrValue)) {
    		return PublishToAdr.NOT_TO_ADR.getValueName();
    	} else if (PUBLISH_TO_ADR_REQ_FOR_INFO.equals(publishToAdrValue)) {
    		return PublishToAdr.REQUEST_FOR_INFORMATION.getValueName();
    	}
    	return publishToAdrValue;
    }
    
    public void setDocumentAdminService(DocumentAdminService documentAdminService) {
        this.documentAdminService = documentAdminService;
    }

    public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPublishToAdrValue() {
		return publishToAdrValue;
	}

	public void setPublishToAdrValue(String publishToAdrValue) {
		this.publishToAdrValue = publishToAdrValue;
	}

	public List<String> getDocTypes() {
		return docTypes;
	}

	public void setDocTypes(List<String> docTypes) {
		this.docTypes = docTypes;
	}

	public List<SelectItem> getFoundDocTypes() {
		foundDocTypes = new ArrayList<>();
        Map <String, String> adrDocTypes = documentAdminService.getAdrDocumentTypeIdsAndNames();
        if (adrDocTypes != null) {
        	for (String key: adrDocTypes.keySet()) {
        		final SelectItem selectItem = new SelectItem(key, adrDocTypes.get(key));
                foundDocTypes.add(selectItem);
        	}
        }
		return foundDocTypes;
	}

	public void setBulkLoadNodeService(BulkLoadNodeService bulkLoadNodeService) {
        this.bulkLoadNodeService = bulkLoadNodeService;
    }
	
	
	
	public String getInputFolderPath() {
		return inputFolderPath;
	}

	@Override
	public void setInputFolderPath(String inputFolderPath) {
		this.inputFolderPath = inputFolderPath;
        inputFolder = new File(inputFolderPath);
    }

}
