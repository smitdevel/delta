package ee.webmedia.alfresco.orgstructure.amr;

import ee.smit.amr.AmrRestService;
import ee.smit.users.UnitInfo;
import ee.smit.users.UserInfo;
import ee.webmedia.alfresco.common.service.ApplicationService;
import ee.webmedia.alfresco.orgstructure.amr.service.AMRService;
import ee.webmedia.alfresco.orgstructure.amr.service.AmrException;
import ee.webmedia.alfresco.orgstructure.amr.service.RSService;
import ee.webmedia.alfresco.orgstructure.model.OrganizationStructureModel;
import ee.webmedia.alfresco.user.service.UserService;
import ee.webmedia.alfresco.utils.UserUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.management.subsystems.ActivateableBean;
import org.alfresco.repo.security.sync.NodeDescription;
import org.alfresco.repo.security.sync.UserRegistry;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A {@link UserRegistry} implementation with the ability to query Alfresco-like descriptions of users and groups from a SIM "Ametnikeregister".
 */
public class AMRUserRegistry implements UserRegistry, ActivateableBean {
    private static final Log log = LogFactory.getLog(AMRUserRegistry.class);

    private UserService userService;
    private AMRService amrService;
    private RSService rsService;
    private AmrRestService amrRestService;
    private ApplicationService applicationService;
    private String testEmail;

    /**
     * Is this bean active? I.e. should this part of the subsystem be used?
     */
    private boolean active = true;

    @Override
    public Iterator<NodeDescription> getPersons(Date modifiedSince) {
        log.info("Get Persons...");
        UserInfo[] infos;
        if (amrRestService.isActive()) {
            infos = amrRestService.convertAmetnikListV2Resp(amrRestService.getJobsByCompanyId());
        } else {
            infos = amrService.convertAmetnikExtList(amrService.getAmetnikByAsutusId());
        }
        if (infos == null) {
            throw new AmrException("USER Sync failed! AMR getAmetnikByAsutusId service response is NULL! ");
        }

        if (infos.length <= 0) {
            throw new AmrException("USER Sync failed! AMR getAmetnikByAsutusId service response contains no persons (ZERO)!");
        }
        log.info("Found AMR persons: " + infos.length);

        ArrayList<NodeDescription> persons = new ArrayList<>();

        boolean isRestrictedDelta = rsService.isRestrictedDelta();

        log.info("Is Restricted DELTA?: " + isRestrictedDelta);

        List<String> restrictedDeltaUsers = new ArrayList<String>();
        if (isRestrictedDelta) {
            // avoid retrieving restricted delta users if not in restricted delta
            if (amrRestService.isActive()) {
                restrictedDeltaUsers = amrRestService.getRS();
            } else {
                restrictedDeltaUsers = Arrays.asList(rsService.getIsikukoodByAsutusIdAndHasRsLubaRequest());
            }
        }
        for (UserInfo info : infos) {
            if (info == null) {
                log.error("UserInfo info object is NULL!!");
                continue;
            }
            if (checkRestrictedDeltaUsers(isRestrictedDelta, restrictedDeltaUsers, info)) {
                continue;
            }
            NodeDescription person = mergePersonDescription(info);
            person.setLastModified(new Date());// actually should be when modified in remote system
            persons.add(person);
            if (log.isDebugEnabled()) {
                log.debug("firstName=" + info.getFirstname() + "; lastName=" + info.getLastname() + "; id=" + info.getSocialSecNr() + "; unitId=" + info.getUnitId() + "; jobTitle=" + info.getJobTitle() + "; phone=" + info.getPhoneNr() + "; email=" + info.getEmail());
            }
        }
        return persons.iterator();
    }

    private boolean checkRestrictedDeltaUsers(boolean isRestrictedDelta, List<String> restrictedDeltaUsers, UserInfo user) {
        return isRestrictedDelta && !restrictedDeltaUsers.contains(user.getSocialSecNr());
    }

    @Override
    public Iterator<NodeDescription> getPersonByIdCode(String idCode) {
        UserInfo user = null;
        if (amrRestService.isActive()) {
            user = amrRestService.getJobBySocialSecNr(idCode);
        } else {
            user = amrService.convertAmetnikExt(amrService.getAmetnikByIsikukood(idCode));
        }
        if (user == null) {
            return Collections.emptyIterator();
        }
        // TODO merge logic from AMRSimpleAuthenticationImpl to here (RsAccessStatusBean and other...)
        if (rsService.isRestrictedDelta() && !rsService.hasRsLubaByIsikukood(idCode)) {
            return Collections.emptyIterator();
        }
        return Collections.singleton(mergePersonDescription(user)).iterator();
    }

    @Override
    public Iterator<NodeDescription> getPersonByUsername(String username) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<NodeDescription> getGroups(Date modifiedSince) {
        return Collections.emptyIterator();
    }

    /**
     * @param info UserInfo object
     * @return NodeDescription with properties from given <code>ametnik</code><br>
     * (merged with propertis that person with the same userName has - if such person exists)
     */
    private NodeDescription mergePersonDescription(UserInfo info) {
        Map<QName, Serializable> properties;
        NodeDescription person = new NodeDescription();
        properties = person.getProperties();
        Map<QName, Serializable> existingProperties = userService.getUserProperties(info.getSocialSecNr());
        if (existingProperties != null) {
            properties.putAll(existingProperties);
        }
        fillPropertiesFromAmetnik(info, properties);
        return person;
    }

    /**
     * Reads properties from <code>AmetnikExt</code> object and puts them into <code>properties</code>
     *
     * @param info       UserInfo object
     * @param properties User properties
     */
    public void fillPropertiesFromAmetnik(UserInfo info, Map<QName, Serializable> properties) {
        String email = info.getEmail();

        // This is actually implemented in ChainingUserRegistrySynchronizer and the correct thing to do would be to
        // switch from AMRSimpleAuthenticationImpl to SimpleUpdatingAuthenticationComponentImpl - this has been the idea all along, but needs testing
        if (applicationService.isTest()) {
            email = testEmail;
        }

        Long yksusId = info.getUnitId();
        if (Long.valueOf(-1).equals(yksusId)) {
            yksusId = null;
        }
        properties.put(ContentModel.PROP_ORGID, yksusId != null ? yksusId.toString() : null);
        properties.put(ContentModel.PROP_EMAIL, email);
        properties.put(ContentModel.PROP_USERNAME, info.getSocialSecNr());
        properties.put(ContentModel.PROP_FIRSTNAME, info.getFirstname());
        properties.put(ContentModel.PROP_LASTNAME, info.getLastname());
        properties.put(ContentModel.PROP_TELEPHONE, info.getPhoneNr());
        properties.put(ContentModel.PROP_JOBTITLE, info.getJobTitle());
        properties.put(ContentModel.PROP_SERVICE_RANK, info.getServiceRank());
        properties.put(ContentModel.PROP_ORGANIZATION_PATH, (ArrayList<String>) UserUtil.formatYksusRadaToOrganizationPath(info.getUnitPath()));

        properties.put(ContentModel.PROP_COUNTY, info.getCounty());
        properties.put(ContentModel.PROP_MUNICIPALITY, info.getMunicipality());
        properties.put(ContentModel.PROP_VILLAGE, info.getCityRegion());
        properties.put(ContentModel.PROP_STREET_HOUSE, info.getStreet());
        properties.put(ContentModel.PROP_POSTAL_CODE, info.getPostalCode());
    }

    @Override
    public Iterator<NodeDescription> getOrganizationStructures() {
        log.info("Get organization structures...");
        UnitInfo[] infos;
        if (amrRestService.isActive()) {
            infos = amrRestService.convertYksusListV2Resp(amrRestService.getUnitsByCompanyId());
        } else {
            infos = amrService.convertYksusExtList(amrService.getYksusByAsutusId());
        }
        if (infos == null) {
            throw new AmrException("ORGANIZATION STRUCTURE sync failed! AMR getYksusByAsutusId service response is NULL! ");
        }

        if (infos.length <= 0) {
            throw new AmrException("ORGANIZATION STRUCTURE sync failed! AMR getYksusByAsutusId response contains no objects (ZERO)!");
        }
        log.info("Found organization structure units: " + infos.length);

        List<NodeDescription> orgStructures = new ArrayList<>();
        for (UnitInfo info : infos) {
            if (info == null) {
                log.error("UnitInfo info object is NULL!!");
                continue;
            }
            log.info("UNIT id: [" + info.getUnitId() + "] fullpath: [" + info.getUnitPath() + "]");
            orgStructures.add(yksusToOrganizationStructure(info));
        }

        log.debug("AMR orgStructures: " + orgStructures.size());
        return orgStructures.iterator();
    }

    private NodeDescription yksusToOrganizationStructure(UnitInfo info) {
        NodeDescription org = new NodeDescription();
        org.getProperties().put(OrganizationStructureModel.Props.UNIT_ID, info.getUnitId().toString());
        org.getProperties().put(OrganizationStructureModel.Props.NAME, info.getName());

        org.getProperties().put(OrganizationStructureModel.Props.DELETED_STATUS, false);
        org.getProperties().put(OrganizationStructureModel.Props.DELETED_STATUS_DATE, null);

        if (info.getSuperUnitId() != null) {
            org.getProperties().put(OrganizationStructureModel.Props.SUPER_UNIT_ID, info.getSuperUnitId().toString());
        }
        String email = info.getEmail();

        // -----------------------------------------------------------------------------
        if (amrRestService.getRemoveGroupsEmail()) {
            log.debug("RemoveGroupsEmail is TRUE! Set email to NULL: value war: " + email);
            email = null;
        }
        // -----------------------------------------------------------------------------

        if (email != null) {
            log.debug("Units name: [" + info.getName() + "]; e-mail: [" + info.getEmail() + "]");
            org.getProperties().put(OrganizationStructureModel.Props.GROUP_EMAIL, email);
        } else {
            log.debug("Units name: [" + info.getName() + "]; e-mail: NULL!! Remove GROUP_EMAIL");
            try {
                org.getProperties().remove(OrganizationStructureModel.Props.GROUP_EMAIL);
            } catch (Exception ex) {
                log.error("Remove GROUP_EMAIL failed! ERROR: " + ex.getMessage(), ex);
            }
        }


        Serializable organizationPath = (Serializable) UserUtil.formatYksusRadaToOrganizationPath(info.getUnitPath());
        org.getProperties().put(OrganizationStructureModel.Props.ORGANIZATION_PATH, organizationPath);
        return org;
    }

    @Override
    public Iterator<NodeDescription> getPersonByEmail(String email) {
        throw new UnsupportedOperationException();
    }

    // START: getters / setters

    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * Indicates whether this bean is active. I.e. should this part of the subsystem be used?
     *
     * @param active <code>true</code> if this bean is active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setAmrService(AMRService amrService) {
        this.amrService = amrService;
    }

    public void setApplicationService(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    public void setTestEmail(String testEmail) {
        this.testEmail = testEmail;
    }

    public void setRsService(RSService rsService) {
        this.rsService = rsService;
    }

    public void setAmrRestService(AmrRestService amrRestService) {
        this.amrRestService = amrRestService;
    }

    // END: getters / setters

}