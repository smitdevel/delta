package ee.webmedia.alfresco.orgstructure.amr.service;

import ee.smit.users.UnitInfo;
import ee.smit.users.UserInfo;
import smit.ametnik.services.AmetnikExt;
import smit.ametnik.services.YksusExt;

/**
 * Web service, to communicate with AmetnikeRegister
 */
public interface AMRService {
    String BEAN_NAME = "AmrService";

    YksusExt[] getYksusByAsutusId();

    AmetnikExt[] getAmetnikByAsutusId();

    AmetnikExt getAmetnikByIsikukood(String socialSecurityNr);

    UserInfo convertAmetnikExt(AmetnikExt ametnikExt);

    UserInfo[] convertAmetnikExtList(AmetnikExt[] ametnikExt);

    UnitInfo convertYksusExt(YksusExt yksusExt);

    UnitInfo[] convertYksusExtList(YksusExt[] yksusExts);

}
