package ee.webmedia.alfresco.orgstructure.amr.service;

import org.alfresco.error.AlfrescoRuntimeException;

public class AmrException extends AlfrescoRuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 3546647620128092466L;

    public AmrException(String msg)
    {
        super(msg);
    }

    public AmrException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
}
