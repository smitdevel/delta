package ee.webmedia.alfresco.orgstructure.amr.service;

import ee.smit.amr.model.AmetnikV2Resp;
import ee.smit.users.UnitInfo;
import ee.smit.users.UserInfo;
import org.springframework.ws.client.core.WebServiceTemplate;
import smit.ametnik.services.AmetnikByAsutusId2RequestDocument;
import smit.ametnik.services.AmetnikByAsutusId2RequestDocument.AmetnikByAsutusId2Request;
import smit.ametnik.services.AmetnikByAsutusId2ResponseDocument;
import smit.ametnik.services.AmetnikByAsutusId2ResponseDocument.AmetnikByAsutusId2Response;
import smit.ametnik.services.AmetnikByIsikukood2RequestDocument;
import smit.ametnik.services.AmetnikByIsikukood2RequestDocument.AmetnikByIsikukood2Request;
import smit.ametnik.services.AmetnikByIsikukood2ResponseDocument;
import smit.ametnik.services.AmetnikByIsikukood2ResponseDocument.AmetnikByIsikukood2Response;
import smit.ametnik.services.AmetnikExt;
import smit.ametnik.services.YksusByAsutusId2RequestDocument;
import smit.ametnik.services.YksusByAsutusId2RequestDocument.YksusByAsutusId2Request;
import smit.ametnik.services.YksusByAsutusId2ResponseDocument;
import smit.ametnik.services.YksusByAsutusId2ResponseDocument.YksusByAsutusId2Response;
import smit.ametnik.services.YksusExt;

import java.util.ArrayList;
import java.util.List;

import static ee.smit.utils.MathUtils.fromBigInteger;
import static ee.smit.utils.MathUtils.fromLong;

/**
 * Web service, to communicate with AmetnikeRegister
 */
public class AMRServiceImpl extends WebServiceTemplate implements AMRService {
    private static final org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(AMRServiceImpl.class);

    private Long asutusId;

    @Override
    public YksusExt[] getYksusByAsutusId() {
        long startTime = System.currentTimeMillis();
        YksusByAsutusId2Request request = YksusByAsutusId2RequestDocument.Factory.newInstance().addNewYksusByAsutusId2Request();
        request.setAsutusId(fromLong(asutusId));
        YksusByAsutusId2ResponseDocument response = (YksusByAsutusId2ResponseDocument) marshalSendAndReceive(request);
        log.info("YksusByAsutusId2 Request asutusId '" + asutusId + "', time " + (System.currentTimeMillis() - startTime) + " ms, responseDoc:\n" + response);

        YksusByAsutusId2Response yksusByAsutusId2Response = response.getYksusByAsutusId2Response();
        return yksusByAsutusId2Response.getYksusArray();
    }

    @Override
    public AmetnikExt[] getAmetnikByAsutusId() {
        long startTime = System.currentTimeMillis();
        AmetnikByAsutusId2Request request = AmetnikByAsutusId2RequestDocument.Factory.newInstance().addNewAmetnikByAsutusId2Request();
        request.setAsutusId(fromLong(asutusId));
        request.setYksusetaAmetnikudOnly(false);
        AmetnikByAsutusId2ResponseDocument responseDoc = (AmetnikByAsutusId2ResponseDocument) marshalSendAndReceive(request);
        log.info("AmetnikByAsutusId2 Request: asutusId '" + asutusId + "', time " + (System.currentTimeMillis() - startTime) + " ms, responseDoc:\n"
                + responseDoc);

        AmetnikByAsutusId2Response response = responseDoc.getAmetnikByAsutusId2Response();
        return response.getAmetnikArray();
    }

    @Override
    public AmetnikExt getAmetnikByIsikukood(String socialSecurityNr) {
        long startTime = System.currentTimeMillis();
        AmetnikByIsikukood2Request request = AmetnikByIsikukood2RequestDocument.Factory.newInstance().addNewAmetnikByIsikukood2Request();
        request.setIsikukood(socialSecurityNr);
        request.setAsutusId(fromLong(asutusId));
        AmetnikByIsikukood2ResponseDocument responseDoc = (AmetnikByIsikukood2ResponseDocument) marshalSendAndReceive(request);
        if (log.isDebugEnabled()) {
            log.debug("AmetnikByIsikukood2 Request socialSecurityNr '" + socialSecurityNr + "', time " + (System.currentTimeMillis() - startTime)
                    + " ms, responseDoc:\n" + responseDoc);
        }
        AmetnikByIsikukood2Response response = responseDoc.getAmetnikByIsikukood2Response();
        return response.getAmetnik();
    }

    /**
     * properties.put(ContentModel.PROP_ORGID, yksusId != null ? yksusId.toString() : null);
     *         properties.put(ContentModel.PROP_EMAIL, email);
     *         properties.put(ContentModel.PROP_USERNAME, ametnik.getIsikukood());
     *         properties.put(ContentModel.PROP_FIRSTNAME, ametnik.getEesnimi());
     *         properties.put(ContentModel.PROP_LASTNAME, ametnik.getPerekonnanimi());
     *         properties.put(ContentModel.PROP_TELEPHONE, ametnik.getKontakttelefon());
     *         properties.put(ContentModel.PROP_JOBTITLE, ametnik.getAmetikoht());
     *         properties.put(ContentModel.PROP_ORGANIZATION_PATH, (ArrayList<String>) UserUtil.formatYksusRadaToOrganizationPath(ametnik.getYksusRada()));
     *         Aadress aadress = ametnik.getAadress();
     *         if (aadress != null) {
     *             properties.put(ContentModel.PROP_COUNTY, aadress.getMaakond());
     *             properties.put(ContentModel.PROP_MUNICIPALITY, aadress.getOmavalitsus());
     *             properties.put(ContentModel.PROP_VILLAGE, aadress.getAsustusYksus());
     *             properties.put(ContentModel.PROP_STREET_HOUSE, aadress.getKohanimi());
     *             properties.put(ContentModel.PROP_POSTAL_CODE, aadress.getSihtkood());
     *         }
     *         properties.put(ContentModel.PROP_SERVICE_RANK, ametnik.getTeenistusaste());
     *
     * @param ametnikExt
     * @return
     */
    @Override
    public UserInfo convertAmetnikExt(AmetnikExt ametnikExt){
        if(ametnikExt == null){
            return new UserInfo();
        }

        UserInfo info = new UserInfo();

        info.setSocialSecNr(ametnikExt.getIsikukood());
        info.setFirstname(ametnikExt.getEesnimi());
        info.setLastname(ametnikExt.getPerekonnanimi());
        info.setEmail(ametnikExt.getEmail());
        info.setPhoneNr(ametnikExt.getKontakttelefon());
        info.setJobTitle(ametnikExt.getAmetikoht());
        info.setServiceRank(ametnikExt.getTeenistusaste());
        info.setOrgId(asutusId);
        info.setUnitId(fromBigInteger(ametnikExt.getYksusId()));
        info.setUnitPath(ametnikExt.getYksusRada());
        if(ametnikExt.getAadress() != null){
            info.setCounty(ametnikExt.getAadress().getMaakond());
            info.setMunicipality(ametnikExt.getAadress().getOmavalitsus());
            info.setCityRegion(ametnikExt.getAadress().getAsustusYksus());
            info.setStreet(ametnikExt.getAadress().getKohanimi());
            info.setPostalCode(ametnikExt.getAadress().getSihtkood());
        }

        return info;
    }

    @Override
    public UserInfo[] convertAmetnikExtList(AmetnikExt[] ametnikExt) {
        if(ametnikExt == null){
            return new UserInfo[0];
        }

        List<UserInfo> infos = new ArrayList<>();

        for (AmetnikExt user : ametnikExt) {
            UserInfo userInfo = convertAmetnikExt(user);
            if(userInfo != null){
                infos.add(userInfo);
            } else {
                log.error("Failed to convert AmetnikExt user object to UserInfo object!!! username: " + user.getIsikukood() + "; " + user.getEesnimi() + "; " + user.getPerekonnanimi());
            }
        }

        return infos.toArray(new UserInfo[0]);
    }

    @Override
    public UnitInfo convertYksusExt(YksusExt yksusExt){
        if (yksusExt == null){
            return new UnitInfo();
        }

        UnitInfo info = new UnitInfo();
        info.setOrgId(fromBigInteger(yksusExt.getAsutusId()));
        info.setSuperUnitId(fromBigInteger(yksusExt.getYlemYksusId()));
        info.setUnitId(fromBigInteger(yksusExt.getId()));
        info.setName(yksusExt.getNimetus());
        info.setUnitPath(yksusExt.getYksusRada());
        info.setEmail(yksusExt.getEmail());

        return info;
    }

    @Override
    public UnitInfo[] convertYksusExtList(YksusExt[] yksusExts){
        if(yksusExts == null){
            return new UnitInfo[0];
        }

        UnitInfo[] infos = new UnitInfo[yksusExts.length];
        int i = 0;
        for(YksusExt yksus : yksusExts){
            infos[i] = convertYksusExt(yksus);
            i++;
        }
        return infos;
    }

    // START: getters / setters
    public void setAsutusId(String asutusId) {
        this.asutusId = Long.valueOf(asutusId);
    }
    // END: getters / setters
}