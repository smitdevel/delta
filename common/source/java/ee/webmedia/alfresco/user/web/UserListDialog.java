package ee.webmedia.alfresco.user.web;

import static ee.webmedia.alfresco.common.web.BeanHelper.getParametersService;
import static ee.webmedia.alfresco.common.web.UserContactGroupSearchBean.FILTER_INDEX_SEPARATOR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import ee.webmedia.alfresco.common.web.UserContactGroupSearchFilters;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.users.UsersBeanProperties;
import org.alfresco.web.ui.common.component.PickerSearchParams;
import org.alfresco.web.ui.common.component.data.UIRichList;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.jsf.FacesContextUtils;

import ee.webmedia.alfresco.common.web.BeanHelper;
import ee.webmedia.alfresco.orgstructure.service.OrganizationStructureService;
import ee.webmedia.alfresco.parameters.model.Parameters;
import ee.webmedia.alfresco.user.model.UserListRowVO;
import ee.webmedia.alfresco.user.model.UserModel;
import ee.webmedia.alfresco.user.service.UserService;
import ee.webmedia.alfresco.utils.TextUtil;
import ee.webmedia.alfresco.utils.UserUtil;
import ee.webmedia.alfresco.utils.WebUtil;

public class UserListDialog extends BaseDialogBean {
    private static final Log log = LogFactory.getLog(UserListDialog.class);

    private static final long serialVersionUID = 1L;
    public static final String BEAN_NAME = "UserListDialog";

    private transient UserService userService;
    private transient OrganizationStructureService organizationStructureService;
    private UsersBeanProperties properties;

    private transient UIRichList usersList;
    private List<UserListRowVO> users = Collections.<UserListRowVO> emptyList();

    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Throwable {
        // finish button not shown or used
        return null;
    }

    @Override
    public void init(Map<String, String> params) {
        super.init(params);
        if (usersList != null) {
            usersList.setValue(null);
        }
        if (properties != null) {
            properties.setSearchCriteria(null);
        }
        users = new ArrayList<UserListRowVO>();
    }

    @Override
    public String cancel() {
        clean();
        return super.cancel();
    }

    @Override
    public void clean() {
        userService = null;
        organizationStructureService = null;
        usersList.setValue(null);
        users.clear();
    }

    /**
     * @return the list of user Nodes to display
     * @throws Exception
     */
    public List<UserListRowVO> getUsers() throws Exception {
        if (users == null) {
            search();
        }
        return users;
    }

    /**
     * Event handler called when the user wishes to search for a user
     *
     * @return The outcome
     */
    public String search() {
        if (usersList != null) {
            usersList.setValue(null);
        }
        users = getUserListVOs();
        // return null to stay on the same page
        return null;
    }

    /**
     * Action handler to show all the users currently in the system
     *
     * @return The outcome
     */
    public String showAll() {
        if (usersList != null) {
            usersList.setValue(null);
        }
        if (properties != null) {
            properties.setSearchCriteria(null);
        }
        users = getUserListVOs();

        // return null to stay on the same page
        return null;
    }

    private List<UserListRowVO> getUserListVOs() {
        List<UserListRowVO> userListVOs = new ArrayList<>();
        List<Node> userNodes;
        if (StringUtils.isNotBlank(properties.getSearchCriteria())) {
            userNodes = getOrganizationStructureService().setUsersUnit(getUserService().searchUsers(properties.getSearchCriteria(), false, -1));
        } else {
            userNodes = getOrganizationStructureService().setUsersUnit(getUserService().getPersonsList());
        }
        if (userNodes != null) {
            for (Node userNode : userNodes) {
                userListVOs.add(new UserListRowVO(userNode));
            }
        }
        return userListVOs;
    }

    /**
     * Query callback method executed by the Generic Picker component.
     * This method is part of the contract to the Generic Picker, it is up to the backing bean
     * to execute whatever query is appropriate and return the results.
     *
     * @param params Search parameters
     * @return An array of SelectItem objects containing the results to display in the picker.
     */
    public SelectItem[] searchUsers(PickerSearchParams params) {
        return escapeEncoding(params.getSearchString(), searchUsers(params, false, true));
    }

    public SelectItem[] searchUsersWithoutSubstitutionInfoShown(PickerSearchParams params) {
        return searchUsers(params, false, false);
    }

    public SelectItem[] searchUsersWithoutCurrentUser(PickerSearchParams params) {
        return searchUsers(params, true, false);
    }

    /**
     * @return SelectItems representing users. Current user is excluded.
     */
    public SelectItem[] searchOtherUsers(PickerSearchParams params) {
        return searchUsers(params, true, true);
    }

    public String getCallbackByPickerFilterValues(SelectItem[] items){
        log.debug("getCallbackByPickerFilterValues()...");
    	final Integer contactGroupsFilterIndex = 8;
    	ArrayList<Integer> values = new ArrayList<Integer>();
    	if(items == null) {
    	    log.debug("ITEMS is NULL!");
        } else {
    	    log.debug("ITEMS: " + items.length);
        }
    	for(SelectItem item : items){
    	    log.trace("LABEL: " + item.getLabel() + "; DESCRIPTION: " + item.getDescription() + "; VALUE: " + (int) item.getValue());
    		values.add((int) item.getValue());
    	}
        log.debug("VALUES size: " + values.size());
        return "UserListDialog.search" + UserContactGroupSearchFilters.getValueByIdList(values);
    }

    // Level 1 search

    /**
     * Search User Groups
     * @param params
     * @return
     */
    public SelectItem[] searchUserGroups(PickerSearchParams params){
        return escapeEncoding(params.getSearchString(), BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true));
    }

    /**
     * Search Contacts
     * @param params
     * @return
     */
    public SelectItem[] searchContacts(PickerSearchParams params){
        return escapeEncoding(params.getSearchString(), BeanHelper.getAddressbookSearchBean().searchContacts(params));
    }

    /**
     * Search Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchContactGroups(PickerSearchParams params){
        return escapeEncoding(params.getSearchString(), BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params));
    }

    // Level 2 search
    /**
     * Search Users and UserGroups
     * @param params
     * @return
     */
    public SelectItem[] searchUsersAndUserGroups(PickerSearchParams params){

        return escapeEncoding(params.getSearchString(), (SelectItem[]) ArrayUtils.addAll(
                BeanHelper.getUserListDialog().searchUsers(params),
                BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true)));
    }

    /**
     * Search Users and Contacts
     * @param params
     * @return
     */
    public SelectItem[] searchUsersAndContacts(PickerSearchParams params){

        return escapeEncoding(params.getSearchString(), (SelectItem[]) ArrayUtils.addAll(
                searchUsers(params, false, true),
                BeanHelper.getAddressbookSearchBean().searchContacts(params)));
    }

    /**
     * Search Users and Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchUsersAndContactGroups(PickerSearchParams params){
        return escapeEncoding(params.getSearchString(), (SelectItem[]) ArrayUtils.addAll(
                searchUsers(params, false, true),
                BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params)));
    }

    /**
     * Search User Groups and Contacts
     * @param params
     * @return
     */
    public SelectItem[] searchUserGroupsAndContacts(PickerSearchParams params){
        return escapeEncoding(params.getSearchString(), (SelectItem[]) ArrayUtils.addAll(
                BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true),
                BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params)));
    }

    /**
     * Search User Groups and Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchUserGroupsAndContactGroups(PickerSearchParams params){
        return escapeEncoding(params.getSearchString(), (SelectItem[]) ArrayUtils.addAll(
                BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true),
                BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params)));
    }

    /**
     * Search Contacts and Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchContactsAndContactGroups(PickerSearchParams params){
        return escapeEncoding(params.getSearchString(), (SelectItem[]) ArrayUtils.addAll(
                BeanHelper.getAddressbookSearchBean().searchContacts(params),
                BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params)));
    }

    // Level 3 search
    /**
     * Search Users, User Groups and Contacts
     * @param params
     * @return
     */
    public SelectItem[] searchUsersAndUserGroupsAndContacts(PickerSearchParams params){
        SelectItem[] results = new SelectItem[0];
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserListDialog().searchUsers(params));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchContacts(params));
        return escapeEncoding(params.getSearchString(), results);
    }

    /**
     * Search Users, User Groups amd Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchUsersAndUserGroupsAndContactGroups(PickerSearchParams params){
        SelectItem[] results = new SelectItem[0];
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserListDialog().searchUsers(params));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params));
        return escapeEncoding(params.getSearchString(), results);
    }

    /**
     * Search Users, Contacts and Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchUsersAndContactsAndContactGroups(PickerSearchParams params){
        SelectItem[] results = new SelectItem[0];
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserListDialog().searchUsers(params));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchContacts(params));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params));
        return escapeEncoding(params.getSearchString(), results);
    }

    /**
     * Search User Groups, Contacts and Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchUserGroupsAndContactsAndContactGroups(PickerSearchParams params){
        SelectItem[] results = new SelectItem[0];
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchContacts(params));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params));
        return escapeEncoding(params.getSearchString(), results);
    }

    // Level 4 search
    /**
     * Search Users, User Groups, Contacts and Contact Groups
     * @param params
     * @return
     */
    public SelectItem[] searchAll(PickerSearchParams params){
    	SelectItem[] results = new SelectItem[0];
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserListDialog().searchUsers(params));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getUserContactGroupSearchBean().searchGroups(params, true));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchContacts(params));
        results = (SelectItem[]) ArrayUtils.addAll(results, BeanHelper.getAddressbookSearchBean().searchTaskCapableContactGroups(params));
    	return escapeEncoding(params.getSearchString(), results);
    }

    public SelectItem[] searchNull(PickerSearchParams params){
        log.warn("Search option is not defind!");
        return new SelectItem[0];
    }

    /**
     * Search Users
     * @param params
     * @param excludeCurrentUser
     * @param showSubstitutionInfo
     * @return
     */
    private SelectItem[] searchUsers(PickerSearchParams params, boolean excludeCurrentUser, boolean showSubstitutionInfo) {
        String selectedGroup = params.getGroupSelectLimitation();
        if (StringUtils.isBlank(selectedGroup)) {
            selectedGroup = null;
        }
        String exactGroup = null;
        if (params.getFilterByStructUnitParam()) {
            String taskOwnerStructUnitParam = getParametersService().getStringParameter(Parameters.TASK_OWNER_STRUCT_UNIT);
            if (StringUtils.isNotBlank(taskOwnerStructUnitParam)) {
                exactGroup = taskOwnerStructUnitParam;
            }
        }
        List<Node> nodes = getOrganizationStructureService().setUsersUnit(
                getUserService().searchUsers(params.getSearchString(), true, selectedGroup, params.getLimit(), exactGroup));
        int nodesSize = nodes.size();
        List<SelectItem> results = new ArrayList<SelectItem>(nodesSize);

        String currentUser = null;
        if (excludeCurrentUser) {
            Node user = BeanHelper.getUserDetailsDialog().getUser();
            if (user != null) {
                currentUser = (String) user.getProperties().get(ContentModel.PROP_USERNAME);
            } else {
                currentUser = AuthenticationUtil.getRunAsUser();
            }
        }

        for (Node node : nodes) {
            String userName = (String) node.getProperties().get(ContentModel.PROP_USERNAME);
            if (excludeCurrentUser && StringUtils.equals(userName, currentUser) || node.hasAspect(UserModel.Aspects.LEAVING)) {
                continue;
            }
            String label = UserUtil.getPersonFullNameWithUnitNameAndJobTitle(node.getProperties());
            if (showSubstitutionInfo) {
                String substitute = BeanHelper.getSubstituteService().getSubstituteLabel(userName);
                label = TextUtil.joinStringAndStringWithSpace(label, substitute);
            }

            String value = userName;
            if (params.isIncludeFilterIndex()) {
                value += (FILTER_INDEX_SEPARATOR + UserContactGroupSearchFilters.USERS_FILTER.getId());
            }
            results.add(new SelectItem(value, label, label));
        }

        WebUtil.sort(results);
        return results.toArray(new SelectItem[results.size()]);
    }

    public void setProperties(UsersBeanProperties properties) {
        this.properties = properties;
    }

    public UIRichList getUsersList() {
        return usersList;
    }

    public void setUsersList(UIRichList usersList) {
        this.usersList = usersList;
    }

    protected UserService getUserService() {
        if (userService == null) {
            userService = (UserService) FacesContextUtils.getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
                    .getBean(UserService.BEAN_NAME);
        }
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    protected OrganizationStructureService getOrganizationStructureService() {
        if (organizationStructureService == null) {
            organizationStructureService = (OrganizationStructureService) FacesContextUtils.getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
                    .getBean(OrganizationStructureService.BEAN_NAME);
        }
        return organizationStructureService;
    }

    public void setOrganizationStructureService(OrganizationStructureService organizationStructureService) {
        this.organizationStructureService = organizationStructureService;
    }

    private SelectItem[] escapeEncoding(String searchWord, SelectItem[] selectItems) {
    	List<SelectItem> items = new ArrayList<SelectItem>();
    	for(SelectItem item : selectItems) {
    		if(item.getLabel().toLowerCase().contains(searchWord.toLowerCase())) items.add(item);
    	}
    	return (SelectItem[]) items.toArray(new SelectItem[items.size()]);
    }
}
