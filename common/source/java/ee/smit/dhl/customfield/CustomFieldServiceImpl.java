package ee.smit.dhl.customfield;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.smit.dhl.customfield.model.CustomField;
import ee.webmedia.alfresco.docdynamic.model.DocumentDynamicModel;
import ee.webmedia.alfresco.utils.DvkUtil;
import org.alfresco.service.namespace.QName;
import com.nortal.jroad.client.dhl.types.ee.riik.schemas.deccontainer.vers21.DecContainerDocument;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.Base64DecodingException;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CustomFieldServiceImpl implements CustomFieldService {

    private static Log log = LogFactory.getLog(CustomFieldServiceImpl.class);

    private static final String CUSTOM_FIELD_FILE_NAME = "delta_custom_fields.json";

    public Map<QName, Serializable> getCustomFieldProps(DecContainerDocument.DecContainer.File dataFile) {
        ArrayList<CustomField> customFields = getCustomFields(dataFile);
        Map<QName, Serializable> props = new HashMap<>();

        for (CustomField customField : customFields) {

            String fieldName = customField.getName();
            String fieldType = customField.getType();
            String fieldStringValue = customField.getValue();

            Object fieldValue = null;

            try {
                fieldValue = getFieldValue(fieldType, fieldStringValue);
                if (fieldValue != null) {
                    props.put(QName.createQName(DocumentDynamicModel.URI, fieldName), (Serializable) fieldValue);
                }
            } catch (ParseException e) {
                log.error("Failed to parse custom field date, date has to be in yyyy-MM-dd'T'HH:mm:ss format");
            } catch (IOException e) {
                log.error("Failed to parse custom field value");
            }
        }

        return props;
    }

    public ArrayList<CustomField> getCustomFields(DecContainerDocument.DecContainer.File dataFile) {
        ArrayList<CustomField> customFieldsList = null;

        String jsonCustomFields = null;
        try {
            jsonCustomFields = IOUtils.toString(DvkUtil.getFileContents(dataFile));
        } catch (IOException e) {
            log.error("Failed to read custom fields file contents");
        } catch (Base64DecodingException e) {
            log.error("Failed to decode custom fields file contents");
        }

        ObjectMapper mapper = new ObjectMapper();

        try {
            customFieldsList = mapper.readValue(jsonCustomFields, new TypeReference<ArrayList<CustomField>>(){});
        } catch (IOException e) {
            log.error("Failed to convert custom fields data to an object");
        }

        return customFieldsList;
    }

    public <T> T getFieldValue(String fieldType, String fieldValue) throws ParseException, IOException {
        switch(fieldType) {
            case "string":
                return (T) fieldValue;
            case "long":
                return (T) Long.valueOf(fieldValue);
            case "double":
                return (T) Double.valueOf(fieldValue);
            case "boolean":
                return (T) Boolean.valueOf(fieldValue);
            case "list":
                ObjectMapper mapper = new ObjectMapper();
                List<String> values = mapper.readValue(fieldValue, new TypeReference<List<String>>() {});
                return (T) values;
            case "date":
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(fieldValue);
                return (T) date;
            case "timestamp":
                return (T) new Date(Long.valueOf(fieldValue));
            default:
                log.error("Unknown custom field data type: " + fieldType);
                return null;
        }
    }

    public String getCustomFieldFileName() {
        return CUSTOM_FIELD_FILE_NAME;
    }

}
