package ee.smit.dhl.customfield;

import ee.smit.dhl.customfield.model.CustomField;
import com.nortal.jroad.client.dhl.types.ee.riik.schemas.deccontainer.vers21.DecContainerDocument;
import org.alfresco.service.namespace.QName;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

public interface CustomFieldService {
    String BEAN_NAME = "CustomFieldService";

    Map<QName, Serializable> getCustomFieldProps(DecContainerDocument.DecContainer.File dataFile);

    ArrayList<CustomField> getCustomFields(DecContainerDocument.DecContainer.File dataFile);

    <T> T getFieldValue(String fieldType, String fieldValue) throws ParseException, IOException;

    String getCustomFieldFileName();
}