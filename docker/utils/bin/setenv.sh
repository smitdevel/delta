# Set all necessary environment variables
JAVA_HOME="/app/java/jdk1.7.0_80"

JAVA_OPTS="-d64 -server -Djava.awt.headless=true -Duser.timezone=Europe/Tallinn -Dfile.encoding=UTF8 -Djavax.net.ssl.trustStore=/etc/pki/java/cacerts -Dhttps.protocols=TLSv1.2,SSLv3 -Djdk.tls.client.protocols=TLSv1.2"
#JAVA_OPTS="$JAVA_OPTS -Dhttp.proxyHost=mwg.smit.sise -Dhttp.proxyPort=3148 -Dhttp.proxyUser=37503294229 -Dhttp.proxyPass=K22biklane78+QWER -Dhttp.proxyPassword=K22biklane78+QWER"

CATALINA_OPTS="-Xms2g -Xmx2g -XX:MaxPermSize=512m -XX:ReservedCodeCacheSize=100M -XX:+UseCodeCacheFlushing -XX:NewRatio=1 -XX:+UseStringCache -XX:+OptimizeStringConcat -XX:+DisableExplicitGC -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9900 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dlog4j.configuration=file:/app/tomcat/tomcat7/lib/log4j.properties"

PROXY_OPTS="-Dhttp.proxySet=true -Dhttp.proxyHost=localhost -Dhttp.proxyPort=3128 -Dhttps.proxyHost=localhost -Dhttps.proxyPort=3128 -Dhttp.proxyUser=37503294229 -Dhttp.proxyPassword=K22biklane9078+ZXCV -Dhttps.proxyUser=37503294229 -Dhttps.proxyPassword=K22biklane9078+ZXCV"

PROXY_OPTS_EXCLUDE="-Dhttp.nonProxyHosts='localhost|*.dev|*.test|*.sise|192.168.56.1|192.*|172.*'"

JAVA_OPTS="$JAVA_OPTS $PROXY_OPTS $PROXY_OPTS_EXCLUDE"

CATALINA_PID="/var/run/tomcat/tomcat.pid"

CATALINA_OPTS="$CATALINA_OPTS $PROXY_OPTS $PROXY_OPTS_EXCLUDE -Dhttps.protocols=TLSv1.2 -Djdk.tls.client.protocols=TLSv1.2"
