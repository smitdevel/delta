<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib tagdir="/WEB-INF/tags" prefix="tag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tag:html>

    Dokumentide home.

    <c:set var="wrapperOpen" value="false" />
    <c:forEach items="${tasks}" var="task" varStatus="loop">
        <c:if test="${loop.index % 2 == 0}">
            <div class="articlewrap">
            <c:set var="wrapperOpen" value="true" />
        </c:if>

        <tag:taskItem task="${task}" />

        <c:if test="${loop.index % 2 == 1}">
            </div>
            <c:set var="wrapperOpen" value="false" />
        </c:if>
    </c:forEach>

    <c:if test="${wrapperOpen}">
        </div>
    </c:if>

</tag:html>