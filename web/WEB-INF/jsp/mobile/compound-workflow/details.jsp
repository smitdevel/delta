<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<tag:html>

    <c:if test="${ !empty compoundWorkflowTitle }">
        <h1><c:out value="${ compoundWorkflowTitle }"/></h1>
    </c:if>

    <%-- Delegation history block --%>
    <tag:expanderBlock blockId="workflow-delegation-history-block" titleId="delegation.history.block.title"
                       titleDetails="${delegationHistoryTaskCount}" expanded="false" independent="true"
                       rendered="${not empty delegationHistoryTaskCount}">
        <jsp:include page="/WEB-INF/jsp/mobile/compound-workflow/delegation-history-block-content.jsp"/>
    </tag:expanderBlock>

    <jsp:include page="/WEB-INF/jsp/mobile/compound-workflow/in-progress-tasks.jsp"/>

    <%-- START: General block --%>
    <tag:expanderBlock blockId="workflow-general" titleId="site.workflow.general" expanded="true" independent="true">
        <tag:valueRow labelId="workflow.ownerName" value="${workflowOwnerName}"/>
        <tag:valueRow labelId="workflow.status" value="${workflowStatus}"/>

        <c:if test="${ isDocumentWorkflowType }">
            <tag:valueRow labelId="workflow.accessRestriction" value="${document.accessRestriction}"/>

            <c:if test="${ !empty document.accessRestrictionReason }">
                <tag:readMore content="${document.accessRestrictionReason}" threshold="150" useTagContent="true" >
                    <tag:valueRow labelId="workflow.accessRestrictionReason" value="${document.accessRestrictionReason}" hideIfEmpty="true" escape="false" />
                </tag:readMore>
            </c:if>

            <c:if test="${ !empty accessRestrictionDate }">
                <tag:valueRow labelId="workflow.accessRestrictionDate" value="${accessRestrictionDate}"/>
            </c:if>

            <c:if test="${ !empty regInfo }">
                <tag:valueRow labelId="workflow.regDateTime" value="${regInfo}"/>
            </c:if>

            <c:if test="${ !empty document.senderOrRecipient }">
                <tag:readMore content="${document.senderOrRecipient}" threshold="150" useTagContent="true" >
                    <tag:valueRow labelId="workflow.SenderRecipientName" value="${document.senderOrRecipient}" hideIfEmpty="true" escape="false" />
                </tag:readMore>
            </c:if>

            <c:if test="${ !empty document.senderOrRecipientEmail }">
                <tag:readMore content="${document.senderOrRecipientEmail}" threshold="150" useTagContent="true" >
                    <tag:valueRow labelId="workflow.SenderRecipientEmail" value="${document.senderOrRecipientEmail}" hideIfEmpty="true" escape="false" />
                </tag:readMore>
            </c:if>

            <c:if test="${ !empty additionalInfo }">
                <tag:readMore content="${additionalInfo}" threshold="150" useTagContent="true" >
                    <tag:valueRow labelId="workflow.comment" value="${additionalInfo}" hideIfEmpty="true" escape="false" />
                </tag:readMore>
            </c:if>
        </c:if>
    </tag:expanderBlock>
    <%-- END: General block --%>

    <c:if test="${ isIndependent }">
        <%-- START: Files block --%>
        <tag:expanderBlock blockId="workflow-files" titleId="site.workflow.files" expanded="false" independent="true">
            <tag:fileBlock files="${loop.current.files}" signable="${loop.current.documentToSign}"
                           titleId="${loop.current.documentToSign ? 'site.document.signableFiles' : ''}"/>
        </tag:expanderBlock>
        <%-- END: Files block --%>
        <%-- START: Comments block --%>
        <tag:pagedList listId="workflow-comments" titleId="site.workflow.comments" items="${comments}" expanded="true"
                       independent="true">
            <h3>
                <c:out value="${loop.current.creatorName}"/>
                <c:out value="&nbsp;" escapeXml="false"/>
                <fmt:formatDate value="${loop.current.created}" pattern="dd.MM.yyyy HH:mm"/>
            </h3>
            <tag:readMore content="${loop.current.commentText}" threshold="150"/>

            <c:if test="${not loop.last}">
                <hr/>
            </c:if>
        </tag:pagedList>
        <%-- END: Comments block --%>

        <%-- START: Objects block --%>
        <tag:pagedList listId="workflow-objects" titleId="site.workflow.objects" items="${objects}" expanded="true"
                       independent="true">
            <h3><a href="#"> <%-- NB! loop.current.showLink --%>
                <c:out value="${loop.current.akString} ${loop.current.docName}"/>
            </a></h3>
            <tag:valueRow labelId="document.type" value="${loop.current.documentTypeName}"/>

            <tag:fileBlock files="${loop.current.files}" signable="${loop.current.documentToSign}"
                           titleId="${loop.current.documentToSign ? 'site.document.signableFiles' : ''}"/>
            <tag:fileBlock files="${loop.current.inactiveFiles}" titleId="site.document.inactiveFiles"/>
        </tag:pagedList>
        <%-- END: Objects block --%>
    </c:if>

    <%-- START: Opinions block --%>
    <tag:pagedList listId="workflow-opinions" titleId="site.workflow.opinions" items="${opinions}" expanded="true"
                   independent="true" rendered="${not empty opinions}">
        <h3>
            <c:out value="${loop.current.ownerNameWithSubstitute}"/>
            <c:out value="&nbsp;" escapeXml="false"/>
            <fmt:formatDate value="${loop.current.completedDateTime}" pattern="dd.MM.yyyy"/>
        </h3>
        <tag:fileBlock files="${loop.current.files}"/>

        <tag:readMore content="${loop.current.commentAndLinks}" threshold="200" useTagContent="true">
            <tag:text value="${loop.current.commentAndLinks}"></tag:text>
        </tag:readMore>
    </tag:pagedList>
    <%-- END: Opinions block --%>

    <%-- START: Information block --%>
    <tag:pagedList listId="workflow-information" titleId="site.workflow.information" items="${relatedUrls}"
                   expanded="true" rendered="${not empty relatedUrls}" independent="true">
        <p><a href="<c:url value="${loop.current.url}" />" target="${loop.current.target}"><tag:limitMessage
                message="${loop.current.url}"/></a></p>
        <tag:readMore content="${loop.current.urlComment}"/>

        <c:if test="${not loop.last}">
            <hr/>
        </c:if>
    </tag:pagedList>
    <%-- END: Information block --%>

    <tag:expanderBlock blockId="workflow-block" titleId="workflow.block.title" titleDetails="${taskCount}"
                       expanded="true" independent="true">
        <jsp:include page="/WEB-INF/jsp/mobile/compound-workflow/workflow-block-content.jsp"/>
    </tag:expanderBlock>

    <c:if test="${ !isIndependent }">
        <tag:expanderBlock blockId="workflow-files" titleId="workflow.files.title" titleDetails="${files.size()}"
                           expanded="true" independent="true">
            <jsp:include page="/WEB-INF/jsp/mobile/compound-workflow/workflow-block-files.jsp"/>
        </tag:expanderBlock>

        <tag:expanderBlock blockId="workflow-files-inactive" titleId="workflow.files.inactive.title"
                           titleDetails="${inActiveFiles.size()}" expanded="true" independent="true">
            <jsp:include page="/WEB-INF/jsp/mobile/compound-workflow/workflow-block-files-inactive.jsp"/>
        </tag:expanderBlock>
    </c:if>
    <%-- 			<tag:list list="dokumentList" /> --%>
</tag:html>