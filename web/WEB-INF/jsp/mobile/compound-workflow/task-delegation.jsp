<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tag"%>

<tag:html>

  <form:form modelAttribute="taskDelegationForm" method="POST">

    <h2>
      <fmt:message key="${taskDelegationForm.translations['pageTitle']}" />
    </h2>
    
    <div id="assignees" >
       <h3><fmt:message key="${taskDelegationForm.translations['assigneesTitle']}" /></h3>
    </div>
    
    <h2>
      <fmt:message key="${taskDelegationForm.translations['formTitle']}" />
    </h2>
    
    <input id="compoundWorkflowRef" name="compoundWorkflowRef" value="${ taskDelegationForm.compoundWorkflowRef }" type="hidden" />
    <input id="taskType" name="taskType" value="${ taskDelegationForm.taskType }" type="hidden" />
    <input id="choice" name="choice" value="${ taskDelegationForm.choice }" type="hidden" />
    
    <%-- START: Addition form --%>
    <tag:suggester labelId="${taskDelegationForm.translations['assignee']}" value="" />

    <tag:formrow labelId="workflow.task.resolution.long">
      <tag:textarea id="taskResolution" value="" />
    </tag:formrow>

    <fmt:formatDate value="${ taskDelegationForm.dueDate }" pattern="dd MMMM, yyyy" var="dueDateStr" />
    <tag:datepicker labelId="workflow.task.dueDate" value="${ dueDateStr }" initialValue="${ taskDelegationForm.taskDueDate }"/>

    <div class="buttongroup">
      <button id="addTaskButton" type="button" onclick="validateAndAdd()" >
        <fmt:message key="workflow.task.delegation.button.add.to.list" />
      </button>
      <button id="okButton" type="submit" name="ok" value="Submit">
        <fmt:message key="${taskDelegationForm.translations['okButtonTitle']}" />
      </button>
    </div>
    <%-- END: Addition form --%>

  </form:form>
  
  <script src="<c:url value="/mobile/js/delegation.js" />" type="text/javascript"></script>  
  <script src="<c:url value="/mobile/js/task-delegation.js" />" type="text/javascript"></script>

</tag:html>