<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<tag:html>

    <%-- Delegation history block --%>
    <tag:expanderBlock blockId="workflow-delegation-history-block" titleId="delegation.history.block.title"
                       titleDetails="${delegationHistoryTaskCount}" expanded="false" independent="true"
                       rendered="${not empty delegationHistoryTaskCount}">
        <jsp:include page="/WEB-INF/jsp/mobile/compound-workflow/delegation-history-block-content.jsp"/>
    </tag:expanderBlock>

    <form:form modelAttribute="taskDelegationForm" method="POST">

        <h2>
            <fmt:message key="workflow.task.delegation.title"/>
        </h2>

        <%-- START: Tasks --%>
        <div id="owner" class="hidden">
            <h3><fmt:message key="workflow.task.delegation.subblock.owner"/></h3>
        </div>

        <div id="coOwners" class="hidden">
            <h3><fmt:message key="workflow.task.delegation.subblock.coOwner"/></h3>
        </div>

        <div id="informationTasks" class="hidden">
            <h3><fmt:message key="workflow.task.delegation.subblock.informationTasks"/></h3>
        </div>

        <div id="opinionTasks" class="hidden">
            <h3><fmt:message key="workflow.task.delegation.subblock.opinionTasks"/></h3>
        </div>
        <%-- END: Tasks --%>

        <h2>
            <fmt:message key="workflow.task.delegation.addition.substitle"/>
        </h2>

        <%-- START: Addition form --%>
        <input id="compoundWorkflowRef" name="compoundWorkflowRef" value="${ taskDelegationForm.compoundWorkflowRef }"
               type="hidden"/>
        <input id="taskType" name="taskType" value="${ taskDelegationForm.taskType }" type="hidden" />
        <tag:formrow labelId="workflow.task.type">
            <form:select path="choice" items="${ taskDelegationForm.choices }"/>
        </tag:formrow>

        <tag:suggester labelId="workflow.task.owner" value=""/>

        <tag:formrow labelId="workflow.task.resolution.long">
            <tag:textarea id="taskResolution" value=""/>
        </tag:formrow>

        <fmt:formatDate value="${ taskDelegationForm.dueDate }" pattern="dd MMMM, yyyy" var="dueDateStr"/>
        <tag:datepicker labelId="workflow.task.dueDate" value="${ dueDateStr }"
                        initialValue="${ taskDelegationForm.taskDueDate }"/>

        <div class="buttongroup">
            <button type="button" id="addTaskButton" onclick="validateAndAdd()">
                <fmt:message key="workflow.task.delegation.button.add.to.list"/>
            </button>
            <button id="okButton" type="button" name="ok" value="Submit" onclick="getConfirmationMessages(true)">
                <fmt:message key="workflow.task.delegation.button.delegate"/>
            </button>
        </div>
        <%-- END: Addition form --%>

    </form:form>

    <script src="<c:url value="/mobile/js/delegation.js" />" type="text/javascript"></script>
    <script src="<c:url value="/mobile/js/assignment-task-delegation.js" />" type="text/javascript"></script>

</tag:html>