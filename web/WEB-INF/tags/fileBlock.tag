<%@ tag body-content="scriptless" isELIgnored="false" pageEncoding="UTF-8" description="Renders list of files and an optional header."%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/wm.tld" prefix="wm" %>
<%@ taglib uri="/WEB-INF/mDelta.tld" prefix="mDelta"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ attribute name="title" required="false" rtexprvalue="true" type="java.lang.String" description="Title for the file block."%>
<%@ attribute name="titleId" required="false" rtexprvalue="true" type="java.lang.String" description="Translation key for file block title."%>
<%@ attribute name="files" required="true" rtexprvalue="true" type="java.util.Collection" description="List of files (ee.webmedia.alfresco.document.file.model.SimpleFile) to display."%>
<%@ attribute name="signable" required="false" rtexprvalue="true" type="java.lang.Boolean" description="Are displayed files added to BDOC when document is signed?"%>
<%@ attribute name="maxLength" required="false" rtexprvalue="true" type="java.lang.Integer" description="Maximum length of the message to be displayed. Defaults to 150."%>

<c:if test="${empty title and not empty titleId}">
    <fmt:message key="${titleId}" var="title" />
</c:if>

<c:if test="${not empty files}">
    <c:if test="${not empty title}">
        <h4>
            <c:if test="${signable}">
                <span class="unsigned"><fmt:message key="document.documentToSign" /></span>
            </c:if>
            <c:out value="${title}" />
        </h4>
    </c:if>

    <ul class="filethumbnails${signable ? ' signable' : ''}">
        <c:forEach items="${files}" var="file">

            <li>
                <c:set var="fileSize" value="${mDelta:fileSize(file.size)}" />

                <c:if test = "${fn:contains(fileSize, 'bytes')}">
                    <fmt:formatNumber var="fileSize" type = "number" maxFractionDigits = "2" value="${(file.size / 1024)}" />
                    <c:set var = "fileSize" value = "${fn:replace(fileSize, ',', '.')} KB" />
                </c:if>
                
                <c:set value="${file.displayName} (${fileSize})" var="fullFileTitle" />

                <c:choose>
                    <c:when test="${not empty file.fileType && file.fileType eq 'digidoc'}">
                        <div id="withoutcomponent${file.nodeRef.id}" class="withoutcomponent display">
                                <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 5%" class="sign-tbl-td"><span class="img arrow-down-img" onclick="toggleComponent('${file.nodeRef.id}')">&nbsp;</span></td>
                                    <td style="width: 2%"><span class="unsigned">&nbsp;</span> <!-- sign_icon--></td>

                                    <c:choose>

                                        <c:when test="${ file.viewDocumentFilesPermission }" >
                                            <td class="sign-tbl-td-middle" >
                                                <div class="sign-tbl-td-middle">
                                                    <a class="txt-overflow-mobile-sign" onclick="toggleComponent('${file.nodeRef.id}')" ><c:out value="${file.displayName}" /></a>
                                                </div>
                                            <td>
                                            <td align="center" width="5%" class="sign-tbl-td"><a href="<c:url value="${file.readOnlyUrl}" />" class="img download-img">&nbsp;</a></td>
                                        </c:when>

                                        <c:otherwise>
                                            <td colspan="2" class="sign-tbl-td-middle">
                                                <div class="txt-overflow-mobile-sign" onclick="toggleComponent('${file.nodeRef.id}')"><c:out value="${file.displayName}" /></div>
                                                <div onclick="toggleComponent('${file.nodeRef.id}')" class="txt-overflow-mobile-sign"><c:out  value=" (${fileSize})"/></div>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>

                                </tr>
                            </table>
                        </div>

                        <div id="withcomponent${file.nodeRef.id}" class="withcomponent">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 5%" class="sign-tbl-td"><span class="img arrow-up-img" onclick="toggleComponent('${file.nodeRef.id}')">&nbsp;</span></td>
                                    <td style="width: 2%"><span class="unsigned">&nbsp;</span></td>


                                    <c:choose>
                                        <c:when test="${ file.viewDocumentFilesPermission }" >
                                            <td class="sign-tbl-td-middle">
                                                <div class="sign-tbl-td-middle">
                                                    <a onclick="toggleComponent('${file.nodeRef.id}')"><c:out value="${fullFileTitle}" /></a>
                                                </div>
                                            </td>
                                            <td align="center" width="5%" class="sign-tbl-td"><a href="<c:url value="${file.readOnlyUrl}" />" class="img download-img">&nbsp;</a></td>
                                        </c:when>

                                        <c:otherwise>
                                            <td colspan="2" class="sign-tbl-td-middle">
                                                <div class="sign-tbl-td-middle" onclick="toggleComponent('${file.nodeRef.id}')"><c:out value="${fullFileTitle}" /></div>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>

                                </tr>


                                <c:if test="${file.signatureInfos != null}">
                                    <tr style="padding-top: 15px;">
                                        <td colspan="2">&nbsp;</td>
                                        <td>
                                            <table cellspacing="0" cellpadding="0"

                                                <c:forEach items="${file.signatureInfos}" var="signInfo" varStatus="loop">
                                                    <tr>
                                                        <c:choose>
                                                            <c:when test="${loop.index == 0}" >
                                                                <td width="30px"><span class="grey-text"><fmt:message key="file.signer" /></span></td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td width="30px">&nbsp</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <td style="padding-left: 4px">
                                                        <span class="grey-text">
                                                            <c:out value="${signInfo.signerName}" />
                                                            <c:out value=", " />
                                                            <fmt:formatDate value="${signInfo.signDate}" pattern="dd.MM.yyyy HH:mm"/>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                </c:forEach>

                                            </table>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </c:if>

                                <c:if test="${file.signatureData != null}">

                                    <c:forEach items="${file.signatureData}" var="signData" varStatus="loop">

                                        <c:set var="signfileSize" value="${mDelta:fileSize(signData.size)}" />

                                        <c:if test = "${fn:contains(signfileSize, 'bytes')}">
                                            <fmt:formatNumber var="signfileSize" type = "number" maxFractionDigits = "2" value="${(signData.size / 1024)}" />
                                            <c:set var = "signfileSize" value = "${fn:replace(signfileSize, ',', '.')} KB" />
                                        </c:if>

                                        <tr class="sign-tbl-tr">
                                            <td>&nbsp;</td>
                                            <td style="padding: 15px 0 15px 0; top: 15px;"><span class="img ${signData.type}-img">&nbsp;</span></td>

                                            <c:choose>
                                                <c:when test="${ file.viewDocumentFilesPermission }" >
                                                    <td class="sign-tbl-td-middle">
                                                        <div class="file-main-div">
                                                            <div class="file-name-div dotted-txt">
                                                                <a class="file-name-div-a dotted-txt " href="<c:url value="${signData.downloadUrl}"/>"><c:out value="${signData.name}" /></a>
                                                            </div>
                                                            <div class="file-size-div dotted-txt ">
                                                                <a href="<c:url value="${signData.downloadUrl}"/>"><c:out  value=" (${signfileSize})"/></a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center" width="5%" class="sign-tbl-td"><a href="<c:url value="${signData.downloadUrl}" />"><span class="img download-img">&nbsp;</span></a></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="sign-tbl-td-middle" colspan="2">
                                                        <div class="file-main-div">
                                                            <div class="file-name-div dotted-txt"><c:out value="${signData.name}" /></div>
                                                            <div class="file-size-div dotted-txt "><c:out  value=" (${signfileSize})"/></div>
                                                        </div>
                                                    </td>
                                                </c:otherwise>
                                            </c:choose>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </table>
                        </div>
                    </c:when>

                    <c:otherwise>
                        <table>
                            <tr>
                                <td style="width: 5%" class="filetype-tbl-td"><span class="img ${file.fileType}-img">&nbsp;</span></td>

                                <c:choose>
                                    <c:when test="${ file.viewDocumentFilesPermission }" >
                                        <td class="sign-tbl-td-middle">
                                            <div class="file-main-div">
                                                <div class="file-name-div dotted-txt">
                                                    <a class="file-name-div-a dotted-txt " href="<c:url value="${file.readOnlyUrl}"/>"><c:out value="${file.displayName}" /></a>
                                                </div>
                                                <div class="file-size-div dotted-txt ">
                                                    <a href="<c:url value="${file.readOnlyUrl}"/>"><c:out value="(${fileSize})"/></a>
                                                </div>
                                            </div>
                                        </td>
                                        <td align="center" width="5%" class="sign-tbl-td"><a href="<c:url value="${file.readOnlyUrl}" />" class="img download-img">&nbsp;</a></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td colspan="2" class="sign-tbl-td-middle">
                                            <div class="file-main-div">
                                                <div class="file-name-div dotted-txt">
                                                    <c:out value="${file.displayName}" />
                                                </div>
                                                <div class="file-size-div dotted-txt ">
                                                    <c:out value="(${fileSize})"/>
                                                </div>
                                            </div>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </table>
                    </c:otherwise>

                </c:choose>
            </li>
        </c:forEach>
    </ul>
</c:if>