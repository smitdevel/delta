        addTranslation("workflow.task.owner", "<fmt:message key='workflow.task.owner' />");
        addTranslation("workflow.task.ownerGroup", "<fmt:message key='workflow.task.ownerGroup' />");
        addTranslation("workflow.task.prop.resolution", "<fmt:message key='workflow.task.prop.resolution' />");
        addTranslation("workflow.task.dueDate", "<fmt:message key='workflow.task.dueDate' />");
        addTranslation("workflow.task.delegation.missing.coOwner", "<fmt:message key='workflow.task.delegation.missing.coOwner' />");
        addTranslation("workflow.task.delegation.missing.owner", "<fmt:message key='workflow.task.delegation.missing.owner' />");
        addTranslation("empty.groups", "<fmt:message key='workflow.task.delegation.found.empty.groups' />");
        addTranslation("workflow.task.delegation.missing.allSubOwners", "<fmt:message key='workflow.task.delegation.missing.allSubOwners' />");

        function hasOwner(choiches) {
            if ($("#owner .rowCounter").length == 0 && $("#coOwners .rowCounter").length == 0 && $("#informationTasks .rowCounter").length == 0 && $("#opinionTasks .rowCounter").length == 0) {
                    var msg = translate("workflow.task.delegation.missing.owner");
                    addMessage(msg, "warning", true);
                    return false;
            }

            clearMessages();
            return true;
        }

        var choiches = [];

        function validateAndAdd() {
            delegation.validateAndAdd();
        }

        $(document).ready(function () {

            delegation.init({
                chooseTaskTypeAndSetChoice: function () {
                    var choiceToTaskTypeBlock = {
                        "ASSIGNMENT_RESPONSIBLE": "owner",
                        "ASSIGNMENT_NOT_RESPONSIBLE": "coOwners",
                        "INFORMATION": "informationTasks",
                        "OPINION": "opinionTasks"
                    };
                    choice = $("#choice").val();
                    delegation.setChoice(choice);
                    if (choice === "ASSIGNMENT_RESPONSIBLE") {
                        $("#owner span").remove();
                    }
                    return $("#" + choiceToTaskTypeBlock[choice]);
                },
                validateDueDate: function () {
                    var datePicker = $(".datepicker");
                    if ($("#choice").val() != "INFORMATION" && datePicker.val().length == 0) {
                        datePicker.addClass("invalid");
                    }
                }
            });

            var choice = $("#choice").val();
            var s = $(".autocomplete");
            if (choice == "ASSIGNMENT_RESPONSIBLE") {
                s.addClass("singleEntry");
            }
            setupSuggester(s, "/m/ajax/search/users");
            $("#choice").data("previous", $("#choice").val());
            $("#choice").change(function () {
                choice = $("#choice").val();
                var prev = $(this).data("previous");
                $(this).data("previous", choice);
                var suggester = $(".autocomplete");
                suggester.attr("value", "");
                suggester.siblings("ul").remove();
                if (choice === "ASSIGNMENT_RESPONSIBLE") {
                    suggester.addClass("singleEntry");
                    setupSuggester(suggester, "/m/ajax/search/users");
                } else {
                    suggester.removeClass("singleEntry");
                    if (choice === "INFORMATION" || choice === "OPINION") {
                        setupSuggester(suggester, "/m/ajax/search/all");
                    } else {
                        setupSuggester(suggester, "/m/ajax/search/users");
                    }
                }
            });

            $("#choice option").each(function () {
                choiches.push($(this).val());
            });

            $("#taskDelegationForm").on("submit", function (event) {
                var cwfRef = $("#compoundWorkflowRef").val();
                var lockResult = false;
                $.ajaxq('lock', {
                    type: 'POST',
                    queue: true,
                    url: getContextPath() + '/m/ajax/cwf/lockdelegate',
                    async: false,
                    dataType: 'json',
                    data: JSON.stringify({compoundWorkflowRef: cwfRef}),
                    contentType: 'application/json',
                    success: function (result) {
                        if (result.messages.length > 0) {
                            var combinedMessage = '';
                            $.each(result.messages, function (i, message) {
                                combinedMessage += message + '\n';
                            });
                            addMessage(combinedMessage, "error", true);
                            lockResult = false;
                        } else {
                            lockResult = true;
                        }
                    },
                    error: function () {
                        addMessage("Failed to get response from server", "error", true);
                        lockResult = false;
                    }
                });

                if (!lockResult) {
                    return false;
                }
                if (delegation.isConfirmed()) {
                    return true;
                }
                var valid = true;
                if (isOwnerOrResolutionFieldFilled()) {
                    valid = delegation.validateAndAdd();
                }
                valid = valid && hasOwner(choiches);

                if (valid) {
                    if ($(".spinner").length > 0) {
                        event.preventDefault(); // wait for group members to load
                        var form = $(this);
                            if (valid) {
                                if (delegation.hasOnlyEmptyGroups()) {
                                    return false;
                                }
                                delegation.markConfirmed();
                                form.submit();
                            }
                    } else {
                        return true;
                    }
                }

                return false;
            });

        });
        
  function coverFormWithSpinner() {
     var form = $("#taskDelegationForm");
     form.addClass("covered");
     form.spin("large", "#ff9000");
     var spinner = form.find(".spinner");
     var top = (form.height() / 2) + "px";
     spinner.css({
        "position" : "relative",
        "top" : top
     });
  }
  
   function getAllDueDates() {
     var dueDates = [];
     $(".visibleDueDate .value").each(function() {
        var dueDate = $(this).text();
        if(dueDate) {
           dueDates.push(dueDate);
        }
     });
     if (dueDates.length == 0) {
        $(".datepicker").each(function() {
        var dueDateEl = $(this).parent().children().eq(2);
        var dueDate = dueDateEl.val();
        if(dueDate) {
           dueDates.push(dueDate + " 23:59");
        }
     });
     }
     return dueDates;
  }
  
  function getAllTaskOwners() {
     var owners = [];
     $(".autocomplete input.singleEntry").each(function() {
        owners.push($(this).val());
     });
     return owners;
  }
  
  function getConfirmationMessages(setupSpinner) {
     if(!!setupSpinner) {
        coverFormWithSpinner();
     }
     if($(".spinner").not("#taskDelegationForm > .spinner").length) { // waiting for groups to load
        $("#taskDelegationForm").on("allGroupsLoaded", function(){
           if(delegation.hasOnlyEmptyGroups()) {
              removeFormSpinner();
              delegation.addEmptyGroupsMessage();
              return;
           }
           getConfirmationMessages(false);
        });
        return;
     }
     
     var cwfRef = $("#compoundWorkflowRef").val();
     var taskType = $("#taskType").val();
     var dueDates = getAllDueDates();
     var taskOwners = getAllTaskOwners();
     
     $.ajaxq('validate', {
        type: 'POST',
        queue: true,
        url: getContextPath() + '/m/ajax/delegation/confirmation',
        dataType: 'json',
        data: JSON.stringify ({compoundWorkflowRef: cwfRef,
                              'taskType': taskType,
                              'dueDates': dueDates,
                              'taskOwners': taskOwners }),
        contentType: 'application/json',
        success: function(result) {
           if(result.messages.length > 0) {
              var combinedMessage = '';
              $.each(result.messages, function(i, message) {
                 combinedMessage += message + '\n';
              });
              if(confirm(combinedMessage)) {
                 $("#taskDelegationForm").trigger("delegationConditionsAccepted");
                 return;
              } 
              return;
           }
           $("#taskDelegationForm").trigger("delegationConditionsAccepted"); // no messages
        },
        error: function() {
           addMessage("Failed to get response from server", "error", true);
           removeFormSpinner();
        }
     }).done(removeFormSpinner);
  }
  
  function removeFormSpinner() {
     $("#taskDelegationForm .spinner").remove();
     $("#taskDelegationForm").removeClass("covered");
  }
  
  $("#taskDelegationForm").on("delegationConditionsAccepted", function (event) {
       var form = $("#taskDelegationForm");
  	   form.submit();
  });